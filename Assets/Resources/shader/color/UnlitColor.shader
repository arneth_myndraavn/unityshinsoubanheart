Shader "QO/Unlit/Color" {
Properties {
 _Color ("Color & Transparency", Color) = (0,0,0,0.5)
}
SubShader { 
 Tags { "QUEUE"="Transparent" }
 Pass {
  Tags { "QUEUE"="Transparent" }
  Color [_Color]
  ZWrite Off
  Blend SrcAlpha OneMinusSrcAlpha
 }
}
Fallback "Unlit/Transparent"
}