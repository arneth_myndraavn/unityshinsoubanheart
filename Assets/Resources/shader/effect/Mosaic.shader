Shader "QO/Effect/Mosaic"
{
	Properties
	{
		_tex0("_tex0  : Original Image (RGBA)", 2D) = "white" {}
		_tex1("_tex1  : Transform Image (RGBA)", 2D) = "white" {}
		_X("x pixels", Float) = 0
		_Y("y pixels", Float) = 0
		_MX("min x pixels", Float) = 16
		_MY("min y pixels", Float) = 9
		_time("time rate", Range(0,1)) = 0
	}

		SubShader
		{
			LOD 200
			Tags { "QUEUE" = "Transparent" "RenderType" = "Transparent" }
			Blend SrcAlpha OneMinusSrcAlpha
			Lighting Off

			Pass
			{
				ZTest Always  ZWrite Off
				Fog { Mode off }


			CGPROGRAM
				#pragma vertex vert_img
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_fastest
				#include "UnityCG.cginc"


				uniform sampler2D _tex0;
				uniform sampler2D _tex1;
				uniform float _X;
				uniform float _Y;
				uniform float _MX;
				uniform float _MY;
				uniform float _time;


				fixed4 frag(v2f_img i) :COLOR
				{
					//Setup for UV
					float tmpvar_1 = (1.0 - sin(mul(_time,3.14)));
					float tmpvar_2 = mul(tmpvar_1, _X) + _MX;
					float tmpvar_3 = mul(tmpvar_1, _Y) + _MY;
					float tmpvar_4_x = float(max(0.0, min(1.0, floor(mul(i.uv.x, tmpvar_2)) / tmpvar_2)));
					float tmpvar_4_y = float(max(0.0, min(1.0, floor(mul(i.uv.y, tmpvar_3)) / tmpvar_3)));
					float2 tmpvar_4 = float2(tmpvar_4_x, tmpvar_4_y);

					//Textures
					float4 tmpvar_5 = tex2D(_tex0, tmpvar_4);
					float4 tmpvar_9 = tex2D(_tex1, tmpvar_4);

					//Setup for 6
					float t_7 = max(min((((1 - _time) - 0.45) / 0.11), 1.0), 0.0);
					float tmpvar_6 = mul(t_7 , mul(t_7 , (3.0 - mul(2.0 , t_7))));

					//Setup for 8
					float4 tmpvar_8 = float4(tmpvar_5.x, tmpvar_5.y, tmpvar_5.z, tmpvar_6);

					//Setup for 10
					float4 tmpvar_10 = float4(tmpvar_9.x, tmpvar_9.y, tmpvar_9.z, (1.0 - tmpvar_6));

					//Final output
					float3 tmpvar_11_xyz = ((float3(tmpvar_8.x, tmpvar_8.y, tmpvar_8.z) * tmpvar_6) + (float3(tmpvar_10.x, tmpvar_10.y, tmpvar_10.z) * tmpvar_10.w));
					float tmpvar_11_w = (tmpvar_6 + tmpvar_10.w);
					float4 tmpvar_11 = float4(tmpvar_11_xyz.x, tmpvar_11_xyz.y, tmpvar_11_xyz.z, tmpvar_11_w);

					//Return output
					return tmpvar_11;
				}

			ENDCG
			}
		}

FallBack off
}