Shader "QO/Effect/Wave"
{
	Properties
	{
		_tex0("_tex0  : Original Image (RGBA)", 2D) = "white" {}
		_tex1("_tex1  : Transform Image (RGBA)", 2D) = "white" {}
		_power("_power : Wave Power", Float) = 0
		_count("_count : Go Return Count", Float) = 0
		_time("_time  : Time Rate (0 to 1)", Range(0,1)) = 0

	}

		SubShader
		{
			LOD 200
			Tags { "QUEUE" = "Transparent" "RenderType" = "Transparent" }
			Blend SrcAlpha OneMinusSrcAlpha
			Lighting Off

			Pass
			{
				ZTest Always  ZWrite Off
				Fog { Mode off }


			CGPROGRAM
				#pragma vertex vert_img
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_fastest
				#include "UnityCG.cginc"


				uniform sampler2D _tex0;
				uniform sampler2D _tex1;
				uniform float _count;
				uniform float _power;
				uniform float _time;
				
				fixed4 frag(v2f_img i) :COLOR
				{
				float sin_omega_6 = sin((_time * 3.14159));
				float theta_5 = (((sin_omega_6 * 3.14159) * (_count * 2.0)) * (0.5 - i.uv.y));
				float tmpvar_9 = cos(theta_5);
				float wave_4 = ((tmpvar_9 * sin_omega_6) * _power);
				float tmpvar_11 = (i.uv.x + wave_4);
				float2 tmpvar_12 = float2(tmpvar_11, i.uv.y);
				float check1_3 = float((-0.01 >= tmpvar_11));
				float check2_2 = float((-0.01 >= (1.0 - tmpvar_11)));
				float tmpvar_15 = float((0.0 >= (check1_3 + check2_2)));
				float4 tmpvar_16 = tex2D (_tex0, tmpvar_12);

				float4 tmpvar_17 = float4(tmpvar_16.x, tmpvar_16.y, tmpvar_16.z, (1.0 - _time));

				float4 t0_1 = tmpvar_17;
				float4 tmpvar_18 = tex2D (_tex1, tmpvar_12);

				float4 tmpvar_19 = float4(tmpvar_18.x, tmpvar_18.y, tmpvar_18.z, (1.0 - t0_1.w));

				float3 tmpvar_20_xyz = (((t0_1.xyz * t0_1.w) + (tmpvar_19.xyz * tmpvar_19.w)) * tmpvar_15);
				float tmpvar_20_w = (t0_1.w + tmpvar_19.w);
				float4 tmpvar_20 = float4(tmpvar_20_xyz.x, tmpvar_20_xyz.y, tmpvar_20_xyz.z, tmpvar_20_w);

				return tmpvar_20;
				}

			ENDCG
			}
		}

FallBack off
}