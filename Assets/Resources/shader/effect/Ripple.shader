Shader "QO/Effect/Ripple"
{
	Properties
	{
		_tex0("_tex0  : Original Image (RGBA)", 2D) = "white" {}
		_tex1("_tex1  : Transform Image (RGBA)", 2D) = "white" {}
		_time("time rate", Range(0,1)) = 0
		_pow("_pow   : screen wave power", Float) = 2
		_w("_w     : ripple wave width", Float) = 1.5
		_h("_h     : ripple wave height", Float) = 5
		_speed("_speed : ripple wave speed", Float) = 2
		
	}

		SubShader
		{
			LOD 200
			Tags { "QUEUE" = "Transparent" "RenderType" = "Transparent" }
			Blend SrcAlpha OneMinusSrcAlpha
			Lighting Off

			Pass
			{
				ZTest Always  ZWrite Off
				Fog { Mode off }


			CGPROGRAM
				#pragma vertex vert_img
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_fastest
				#include "UnityCG.cginc"


				uniform sampler2D _tex0;
				uniform sampler2D _tex1;
				uniform float _time;
				uniform float _w;
				uniform float _h;
				uniform float _pow;
				uniform float _speed;
				


				fixed4 frag(v2f_img i) :COLOR
				{

				float2 tmpvar_3 = float2((i.uv.x - 0.5), ((i.uv.y - 0.5) * 0.5625));

				float tmpvar_4 = (_time * _speed);
				float tmpvar_5 = sqrt(dot(tmpvar_3, tmpvar_3));
				float tmpvar_6 = (tmpvar_4 - (_w * 0.01));
				float tmpvar_7 = ((_time - 0.25) * _speed);
				float tmpvar_8 = sqrt(dot(tmpvar_3, tmpvar_3));
				float tmpvar_9 = (tmpvar_7 - (_w * 0.01));
				float tmpvar_10 = ((_time - 0.5) * _speed);
				float tmpvar_11 = sqrt(dot(tmpvar_3, tmpvar_3));
				float tmpvar_12 = (tmpvar_10 - (_w * 0.01));
				float tmpvar_13 = ((_time - 0.75) * _speed);
				float tmpvar_14 = sqrt(dot(tmpvar_3, tmpvar_3));
				float tmpvar_15 = (tmpvar_13 - (_w * 0.01));
				float tmpvar_16 = (1.0 - _time);
				float2 tmpvar_17 = ((((((tmpvar_3 * ((sin((((tmpvar_4 - tmpvar_5) / (tmpvar_4 - tmpvar_6)) * 3.14159)) * _h) * 0.02)) * float(((float(((tmpvar_5 - tmpvar_6) >= 0.0)) + float(((tmpvar_4 - tmpvar_5) >= 0.0))) >= 2.0))) + ((tmpvar_3 * ((sin((((tmpvar_7 - tmpvar_8) / (tmpvar_7 - tmpvar_9)) * 3.14159)) * _h) * 0.02)) * float(((float(((tmpvar_8 - tmpvar_9) >= 0.0)) + float(((tmpvar_7 - tmpvar_8) >= 0.0))) >= 2.0)))) + ((tmpvar_3 * ((sin((((tmpvar_10 - tmpvar_11) / (tmpvar_10 - tmpvar_12)) * 3.14159)) * _h) * 0.02)) * float(((float(((tmpvar_11 - tmpvar_12) >= 0.0)) + float(((tmpvar_10 - tmpvar_11) >= 0.0))) >= 2.0)))) + ((tmpvar_3 * ((sin((((tmpvar_13 - tmpvar_14) / (tmpvar_13 - tmpvar_15)) * 3.14159)) * _h) * 0.02)) * float(((float(((tmpvar_14 - tmpvar_15) >= 0.0)) + float(((tmpvar_13 - tmpvar_14) >= 0.0))) >= 2.0)))) * tmpvar_16);
				float2 tmpvar_18 = (i.uv + (((tmpvar_3 * (sin((50.2654 * (sqrt(dot(tmpvar_3, tmpvar_3)) * _time))) * (_pow * 0.01))) * tmpvar_16) + tmpvar_17));
				float tmpvar_19 = float((-0.01 >= (i.uv.y + tmpvar_17.y)));
				float check1_2 = tmpvar_19;
				float tmpvar_20 = float((-0.01 >= (1.0 - (i.uv.y + tmpvar_17.y))));
				float check2_1 = tmpvar_20;
				float tmpvar_21 = float((0.0 >= (check1_2 + check2_1)));
				float4 tmpvar_22 = tex2D (_tex0, tmpvar_18);

				float4 tmpvar_23 = float4(tmpvar_22.x, tmpvar_22.y, tmpvar_22.z, 1.0 - _time);

				float4 tmpvar_24 = tex2D (_tex1, tmpvar_18);

				float4 tmpvar_25 = float4(tmpvar_24.x, tmpvar_24.y, tmpvar_24.z, 1.0 - tmpvar_23.w);

				float3 tmpvar_26_xyz = (((tmpvar_23.xyz * tmpvar_23.w) + (tmpvar_25.xyz * tmpvar_25.w)) * tmpvar_21);
				float tmpvar_26_w = (tmpvar_23.w + tmpvar_25.w);
				float4 tmpvar_26 = float4(tmpvar_26_xyz.x, tmpvar_26_xyz.y, tmpvar_26_xyz.z, tmpvar_26_w);

				return tmpvar_26;
				}

			ENDCG
			}
		}

			FallBack off
}