﻿Shader "QO/Effect/Transition"
{
	Properties
	{
		_tex0 ("_tex0 : Original  Image (RGBA)", 2D) = "white" {}
		_tex1 ("_tex1 : Transform Image (RGBA)", 2D) = "white" {}
		_tex2 ("_tex2 : Pattern (GrayScale)", 2D) = "white" {}
		_time ("_time : TimeRate", Range(0,1)) = 0
		_grad ("_grad : Gradation Level", Float) = 0.1
	}

	SubShader
	{
		LOD 200
		Tags { "QUEUE"="Transparent" "RenderType"="Transparent" }
		Blend SrcAlpha OneMinusSrcAlpha
		Lighting Off

		Pass
		{
			ZTest Always  ZWrite Off
			Fog { Mode off }


		CGPROGRAM
			#pragma vertex vert_img
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest
			#include "UnityCG.cginc"


			uniform sampler2D _tex0;
			uniform sampler2D _tex1;
			uniform sampler2D _tex2;
			uniform float _time;
			uniform float _grad;


			fixed4 frag( v2f_img i ):COLOR
			{
				fixed4 texColor = tex2D( _tex0, i.uv );
			    fixed4 texColor1 = tex2D( _tex1, i.uv );
			    fixed4 texColor2 = tex2D( _tex2, i.uv );
			    float addition = _time + _grad;
			    float4 smoothstep1 = smoothstep(_time , addition, texColor2);
			    return lerp( texColor1, texColor, smoothstep1 );
			}

		ENDCG
				}
			}

	FallBack off
}