Shader "QO/Effect/CrossFade"
{
	Properties
	{
		_tex0 ("_tex0 : Original  Image (RGBA)", 2D) = "white" {}
		_tex1 ("_tex1 : Transform Image (RGBA)", 2D) = "white" {}
		_time ("_time : TimeRate", Range(0,1)) = 0
	}

	SubShader
	{
		LOD 200
		Tags { "QUEUE"="Transparent" "RenderType"="Transparent" }
		Blend SrcAlpha OneMinusSrcAlpha
		Lighting Off

		Pass
		{
			ZTest Always  ZWrite Off
			Fog { Mode off }


		CGPROGRAM
			#pragma vertex vert_img
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest
			#include "UnityCG.cginc"


			uniform sampler2D _tex0;
			uniform sampler2D _tex1;
			uniform float _time;


			fixed4 frag( v2f_img i ):COLOR
			{
			    fixed4 texColor = tex2D( _tex0, i.uv );
			    fixed4 texColor2 = tex2D( _tex1, i.uv );
			    return lerp( texColor, texColor2, _time );
			}

		ENDCG
				}
			}

	FallBack off
}