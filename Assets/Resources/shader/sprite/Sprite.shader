﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "QO/Sprite" 
{
	Properties
	{
		_MainTex ("Base (RGBA)", 2D) = "white" {}
		_Color ("Color", Color) = (1,1,1,1)
		_UVWH ("UVWH", Vector) = (0,0,1,1)
	}

	SubShader
	{
		LOD 200

		Tags { "QUEUE"="Transparent" "RenderType"="Transparent" }
		Pass {
		  	Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			sampler2D _MainTex;
			half4 _Color;
			float4 _UVWH;

			struct appdata_t
			{
				float4 vertex : POSITION;
				half4 color : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex : POSITION;
				half4 color : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			v2f vert (appdata_t v)
			{
				v2f o;
				float2 uv_2 = _UVWH.xy;
				float2 wh_1 = _UVWH.zw;
				o.color = v.color;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.texcoord = ((v.texcoord * wh_1) + uv_2);
				return o;
			}

			half4 frag (v2f IN) : COLOR
			{
				  float4 tex = tex2D(_MainTex, IN.texcoord);
				  float4 tmpvar_1 = (tex * _Color);
				return tmpvar_1;
			}
			ENDCG
		}
	}
	Fallback Off
}