﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prime31
{
	// Token: 0x02000016 RID: 22
	public class JsonFormatter
	{
		// Token: 0x06000084 RID: 132 RVA: 0x00005F30 File Offset: 0x00004130
		public static string prettyPrint(string input)
		{
			string result;
			try
			{
				result = new JsonFormatter().print(input);
			}
			catch (Exception)
			{
				result = null;
			}
			return result;
		}

		// Token: 0x06000085 RID: 133 RVA: 0x00005F6C File Offset: 0x0000416C
		private static void buildIndents(int indents, StringBuilder output)
		{
			for (indents = indents; indents > 0; indents--)
			{
				output.Append("\t");
			}
		}

		// Token: 0x06000086 RID: 134 RVA: 0x00005F90 File Offset: 0x00004190
		private bool inString()
		{
			return this.inDoubleString || this.inSingleString;
		}

		// Token: 0x06000087 RID: 135 RVA: 0x00005FBC File Offset: 0x000041BC
		public string print(string input)
		{
			StringBuilder stringBuilder = new StringBuilder(input.Length * 2);
			foreach (char c in input)
			{
				switch (c)
				{
				case ' ':
					if (this.inString())
					{
						stringBuilder.Append(c);
					}
					break;
				default:
					switch (c)
					{
					case ':':
						if (!this.inString())
						{
							this.inVariableAssignment = true;
							stringBuilder.Append(c);
							stringBuilder.Append(" ");
						}
						else
						{
							stringBuilder.Append(c);
						}
						break;
					default:
						switch (c)
						{
						case '[':
							stringBuilder.Append(c);
							if (!this.inString())
							{
								this.context.Push(JsonFormatter.JsonContextType.Array);
							}
							break;
						default:
							switch (c)
							{
							case '{':
								if (!this.inString())
								{
									if (this.inVariableAssignment || (this.context.Count > 0 && this.context.Peek() != JsonFormatter.JsonContextType.Array))
									{
										stringBuilder.Append(Environment.NewLine);
										JsonFormatter.buildIndents(this.context.Count, stringBuilder);
									}
									stringBuilder.Append(c);
									this.context.Push(JsonFormatter.JsonContextType.Object);
									stringBuilder.Append(Environment.NewLine);
									JsonFormatter.buildIndents(this.context.Count, stringBuilder);
								}
								else
								{
									stringBuilder.Append(c);
								}
								break;
							default:
								if (c != '\'')
								{
									if (c != ',')
									{
										stringBuilder.Append(c);
									}
									else
									{
										stringBuilder.Append(c);
										if (!this.inString())
										{
											stringBuilder.Append(" ");
										}
										if (!this.inString() && this.context.Peek() != JsonFormatter.JsonContextType.Array)
										{
											JsonFormatter.buildIndents(this.context.Count, stringBuilder);
											stringBuilder.Append(Environment.NewLine);
											JsonFormatter.buildIndents(this.context.Count, stringBuilder);
											this.inVariableAssignment = false;
										}
									}
								}
								else
								{
									if (!this.inDoubleString && this.prevChar != '\\')
									{
										this.inSingleString = !this.inSingleString;
									}
									stringBuilder.Append(c);
								}
								break;
							case '}':
								if (!this.inString())
								{
									stringBuilder.Append(Environment.NewLine);
									this.context.Pop();
									JsonFormatter.buildIndents(this.context.Count, stringBuilder);
									stringBuilder.Append(c);
								}
								else
								{
									stringBuilder.Append(c);
								}
								break;
							}
							break;
						case ']':
							if (!this.inString())
							{
								stringBuilder.Append(c);
								this.context.Pop();
							}
							else
							{
								stringBuilder.Append(c);
							}
							break;
						}
						break;
					case '=':
						stringBuilder.Append(c);
						break;
					}
					break;
				case '"':
					if (!this.inSingleString && this.prevChar != '\\')
					{
						this.inDoubleString = !this.inDoubleString;
					}
					stringBuilder.Append(c);
					break;
				}
				this.prevChar = c;
			}
			return stringBuilder.ToString();
		}

		// Token: 0x04000035 RID: 53
		private const int defaultIndent = 0;

		// Token: 0x04000036 RID: 54
		private const string indent = "\t";

		// Token: 0x04000037 RID: 55
		private const string space = " ";

		// Token: 0x04000038 RID: 56
		private bool inDoubleString = false;

		// Token: 0x04000039 RID: 57
		private bool inSingleString = false;

		// Token: 0x0400003A RID: 58
		private bool inVariableAssignment = false;

		// Token: 0x0400003B RID: 59
		private char prevChar = '\0';

		// Token: 0x0400003C RID: 60
		private Stack<JsonFormatter.JsonContextType> context = new Stack<JsonFormatter.JsonContextType>();

		// Token: 0x02000017 RID: 23
		private enum JsonContextType
		{
			// Token: 0x0400003E RID: 62
			Object,
			// Token: 0x0400003F RID: 63
			Array
		}
	}
}
