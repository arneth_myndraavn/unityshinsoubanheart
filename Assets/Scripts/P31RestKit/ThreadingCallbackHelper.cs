﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Prime31
{
	// Token: 0x0200000E RID: 14
	public class ThreadingCallbackHelper : MonoBehaviour
	{
		// Token: 0x06000054 RID: 84 RVA: 0x00004578 File Offset: 0x00002778
		public void addActionToQueue(Action action)
		{
			object actions = this._actions;
			lock (actions)
			{
				this._actions.Add(action);
			}
		}

		// Token: 0x06000055 RID: 85 RVA: 0x000045BC File Offset: 0x000027BC
		private void Update()
		{
			object actions = this._actions;
			lock (actions)
			{
				this._currentActions.AddRange(this._actions);
				this._actions.Clear();
			}
			for (int i = 0; i < this._currentActions.Count; i++)
			{
				this._currentActions[i]();
			}
			this._currentActions.Clear();
		}

		// Token: 0x06000056 RID: 86 RVA: 0x0000464C File Offset: 0x0000284C
		public void disableIfEmpty()
		{
			object actions = this._actions;
			lock (actions)
			{
				if (this._actions.Count == 0)
				{
					base.enabled = false;
				}
			}
		}

		// Token: 0x04000022 RID: 34
		private List<Action> _actions = new List<Action>();

		// Token: 0x04000023 RID: 35
		private List<Action> _currentActions = new List<Action>();
	}
}
