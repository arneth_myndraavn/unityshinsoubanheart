﻿using System;

namespace Prime31.Reflection
{
	// Token: 0x02000020 RID: 32
	// (Invoke) Token: 0x060000C2 RID: 194
	public delegate void MemberMapLoader(Type type, SafeDictionary<string, CacheResolver.MemberMap> memberMaps);
}
