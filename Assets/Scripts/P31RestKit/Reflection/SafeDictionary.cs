﻿using System;
using System.Collections.Generic;

namespace Prime31.Reflection
{
	// Token: 0x02000024 RID: 36
	public class SafeDictionary<TKey, TValue>
	{
		// Token: 0x1700000A RID: 10
		public TValue this[TKey key]
		{
			get
			{
				return this._dictionary[key];
			}
		}

		// Token: 0x060000D5 RID: 213 RVA: 0x000080D0 File Offset: 0x000062D0
		public bool tryGetValue(TKey key, out TValue value)
		{
			return this._dictionary.TryGetValue(key, out value);
		}

		// Token: 0x060000D6 RID: 214 RVA: 0x000080F4 File Offset: 0x000062F4
		public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
		{
			return ((IEnumerable<KeyValuePair<TKey, TValue>>)this._dictionary).GetEnumerator();
		}

		// Token: 0x060000D7 RID: 215 RVA: 0x00008114 File Offset: 0x00006314
		public void add(TKey key, TValue value)
		{
			object padlock = this._padlock;
			lock (padlock)
			{
				if (!this._dictionary.ContainsKey(key))
				{
					this._dictionary.Add(key, value);
				}
			}
		}

		// Token: 0x04000058 RID: 88
		private readonly object _padlock = new object();

		// Token: 0x04000059 RID: 89
		private readonly Dictionary<TKey, TValue> _dictionary = new Dictionary<TKey, TValue>();
	}
}
