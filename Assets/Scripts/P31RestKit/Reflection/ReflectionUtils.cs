﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;

namespace Prime31.Reflection
{
	// Token: 0x0200001D RID: 29
	public class ReflectionUtils
	{
		// Token: 0x060000B3 RID: 179 RVA: 0x00007BAC File Offset: 0x00005DAC
		public static Attribute getAttribute(MemberInfo info, Type type)
		{
			Attribute result;
			if (info == null || type == null || !Attribute.IsDefined(info, type))
			{
				result = null;
			}
			else
			{
				result = Attribute.GetCustomAttribute(info, type);
			}
			return result;
		}

		// Token: 0x060000B4 RID: 180 RVA: 0x00007BE8 File Offset: 0x00005DE8
		public static Attribute getAttribute(Type objectType, Type attributeType)
		{
			Attribute result;
			if (objectType == null || attributeType == null || !Attribute.IsDefined(objectType, attributeType))
			{
				result = null;
			}
			else
			{
				result = Attribute.GetCustomAttribute(objectType, attributeType);
			}
			return result;
		}

		// Token: 0x060000B5 RID: 181 RVA: 0x00007C24 File Offset: 0x00005E24
		public static bool isTypeGenericeCollectionInterface(Type type)
		{
			bool result;
			if (!type.IsGenericType)
			{
				result = false;
			}
			else
			{
				Type genericTypeDefinition = type.GetGenericTypeDefinition();
				result = (genericTypeDefinition == typeof(IList<>) || genericTypeDefinition == typeof(ICollection<>) || genericTypeDefinition == typeof(IEnumerable<>));
			}
			return result;
		}

		// Token: 0x060000B6 RID: 182 RVA: 0x00007C84 File Offset: 0x00005E84
		public static bool isTypeDictionary(Type type)
		{
			bool result;
			if (typeof(IDictionary).IsAssignableFrom(type))
			{
				result = true;
			}
			else if (!type.IsGenericType)
			{
				result = false;
			}
			else
			{
				Type genericTypeDefinition = type.GetGenericTypeDefinition();
				result = (genericTypeDefinition == typeof(IDictionary<, >));
			}
			return result;
		}

		// Token: 0x060000B7 RID: 183 RVA: 0x00007CDC File Offset: 0x00005EDC
		public static bool isNullableType(Type type)
		{
			return type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>);
		}

		// Token: 0x060000B8 RID: 184 RVA: 0x00007D14 File Offset: 0x00005F14
		public static object toNullableType(object obj, Type nullableType)
		{
			return (obj != null) ? Convert.ChangeType(obj, Nullable.GetUnderlyingType(nullableType), CultureInfo.InvariantCulture) : null;
		}
	}
}
