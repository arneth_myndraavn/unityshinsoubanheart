﻿using System;
using System.Reflection;

namespace Prime31.Reflection
{
	// Token: 0x02000021 RID: 33
	public class CacheResolver
	{
		// Token: 0x060000C5 RID: 197 RVA: 0x00007D46 File Offset: 0x00005F46
		public CacheResolver(MemberMapLoader memberMapLoader)
		{
			this._memberMapLoader = memberMapLoader;
		}

		// Token: 0x060000C6 RID: 198 RVA: 0x00007D64 File Offset: 0x00005F64
		public static object getNewInstance(Type type)
		{
			CacheResolver.CtorDelegate ctorDelegate;
			object result;
			if (CacheResolver.constructorCache.tryGetValue(type, out ctorDelegate))
			{
				result = ctorDelegate();
			}
			else
			{
				ConstructorInfo constructorInfo = type.GetConstructor(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic, null, Type.EmptyTypes, null);
				ctorDelegate = (() => constructorInfo.Invoke(null));
				CacheResolver.constructorCache.add(type, ctorDelegate);
				result = ctorDelegate();
			}
			return result;
		}

		// Token: 0x060000C7 RID: 199 RVA: 0x00007DD4 File Offset: 0x00005FD4
		public SafeDictionary<string, CacheResolver.MemberMap> loadMaps(Type type)
		{
			SafeDictionary<string, CacheResolver.MemberMap> result;
			SafeDictionary<string, CacheResolver.MemberMap> safeDictionary;
			if (type == null || type == typeof(object))
			{
				result = null;
			}
			else if (this._memberMapsCache.tryGetValue(type, out safeDictionary))
			{
				result = safeDictionary;
			}
			else
			{
				safeDictionary = new SafeDictionary<string, CacheResolver.MemberMap>();
				this._memberMapLoader(type, safeDictionary);
				this._memberMapsCache.add(type, safeDictionary);
				result = safeDictionary;
			}
			return result;
		}

		// Token: 0x060000C8 RID: 200 RVA: 0x00007E44 File Offset: 0x00006044
		private static GetHandler createGetHandler(FieldInfo fieldInfo)
		{
			return (object instance) => fieldInfo.GetValue(instance);
		}

		// Token: 0x060000C9 RID: 201 RVA: 0x00007E74 File Offset: 0x00006074
		private static SetHandler createSetHandler(FieldInfo fieldInfo)
		{
			SetHandler result;
			if (fieldInfo.IsInitOnly || fieldInfo.IsLiteral)
			{
				result = null;
			}
			else
			{
				result = delegate(object instance, object value)
				{
					fieldInfo.SetValue(instance, value);
				};
			}
			return result;
		}

		// Token: 0x060000CA RID: 202 RVA: 0x00007ECC File Offset: 0x000060CC
		private static GetHandler createGetHandler(PropertyInfo propertyInfo)
		{
			MethodInfo getMethodInfo = propertyInfo.GetGetMethod(true);
			GetHandler result;
			if (getMethodInfo == null)
			{
				result = null;
			}
			else
			{
				result = ((object instance) => getMethodInfo.Invoke(instance, Type.EmptyTypes));
			}
			return result;
		}

		// Token: 0x060000CB RID: 203 RVA: 0x00007F14 File Offset: 0x00006114
		private static SetHandler createSetHandler(PropertyInfo propertyInfo)
		{
			MethodInfo setMethodInfo = propertyInfo.GetSetMethod(true);
			SetHandler result;
			if (setMethodInfo == null)
			{
				result = null;
			}
			else
			{
				result = delegate(object instance, object value)
				{
					setMethodInfo.Invoke(instance, new object[]
					{
						value
					});
				};
			}
			return result;
		}

		// Token: 0x04000051 RID: 81
		private readonly MemberMapLoader _memberMapLoader;

		// Token: 0x04000052 RID: 82
		private readonly SafeDictionary<Type, SafeDictionary<string, CacheResolver.MemberMap>> _memberMapsCache = new SafeDictionary<Type, SafeDictionary<string, CacheResolver.MemberMap>>();

		// Token: 0x04000053 RID: 83
		private static readonly SafeDictionary<Type, CacheResolver.CtorDelegate> constructorCache = new SafeDictionary<Type, CacheResolver.CtorDelegate>();

		// Token: 0x02000022 RID: 34
		// (Invoke) Token: 0x060000CE RID: 206
		private delegate object CtorDelegate();

		// Token: 0x02000023 RID: 35
		public sealed class MemberMap
		{
			// Token: 0x060000D1 RID: 209 RVA: 0x00007F66 File Offset: 0x00006166
			public MemberMap(PropertyInfo propertyInfo)
			{
				this.MemberInfo = propertyInfo;
				this.Type = propertyInfo.PropertyType;
				this.Getter = CacheResolver.createGetHandler(propertyInfo);
				this.Setter = CacheResolver.createSetHandler(propertyInfo);
			}

			// Token: 0x060000D2 RID: 210 RVA: 0x00007F9A File Offset: 0x0000619A
			public MemberMap(FieldInfo fieldInfo)
			{
				this.MemberInfo = fieldInfo;
				this.Type = fieldInfo.FieldType;
				this.Getter = CacheResolver.createGetHandler(fieldInfo);
				this.Setter = CacheResolver.createSetHandler(fieldInfo);
			}

			// Token: 0x04000054 RID: 84
			public readonly MemberInfo MemberInfo;

			// Token: 0x04000055 RID: 85
			public readonly Type Type;

			// Token: 0x04000056 RID: 86
			public readonly GetHandler Getter;

			// Token: 0x04000057 RID: 87
			public readonly SetHandler Setter;
		}
	}
}
