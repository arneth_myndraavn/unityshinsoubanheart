﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Prime31
{
	// Token: 0x02000015 RID: 21
	public static class JsonExtensions
	{
		// Token: 0x0600007F RID: 127 RVA: 0x00005E88 File Offset: 0x00004088
		public static string toJson(this IList obj)
		{
			return Json.encode(obj);
		}

		// Token: 0x06000080 RID: 128 RVA: 0x00005EA4 File Offset: 0x000040A4
		public static string toJson(this IDictionary obj)
		{
			return Json.encode(obj);
		}

		// Token: 0x06000081 RID: 129 RVA: 0x00005EC0 File Offset: 0x000040C0
		public static List<object> listFromJson(this string json)
		{
			return Json.decode(json) as List<object>;
		}

		// Token: 0x06000082 RID: 130 RVA: 0x00005EE0 File Offset: 0x000040E0
		public static Dictionary<string, object> dictionaryFromJson(this string json)
		{
			return Json.decode(json) as Dictionary<string, object>;
		}
	}
}
