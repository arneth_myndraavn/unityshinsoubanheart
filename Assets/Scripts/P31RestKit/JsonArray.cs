﻿using System;
using System.Collections.Generic;

namespace Prime31
{
	// Token: 0x02000018 RID: 24
	public class JsonArray : List<object>
	{
		// Token: 0x06000088 RID: 136 RVA: 0x0000630E File Offset: 0x0000450E
		public JsonArray()
		{
		}

		// Token: 0x06000089 RID: 137 RVA: 0x00006317 File Offset: 0x00004517
		public JsonArray(int capacity) : base(capacity)
		{
		}

		// Token: 0x0600008A RID: 138 RVA: 0x00006324 File Offset: 0x00004524
		public override string ToString()
		{
			return JsonFormatter.prettyPrint(SimpleJson.encode(this)) ?? string.Empty;
		}
	}
}
