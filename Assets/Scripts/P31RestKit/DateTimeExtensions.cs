﻿using System;

namespace Prime31
{
	// Token: 0x02000008 RID: 8
	public static class DateTimeExtensions
	{
		// Token: 0x06000021 RID: 33 RVA: 0x00002F8C File Offset: 0x0000118C
		public static long toEpochTime(this DateTime self)
		{
			DateTime d = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
			return Convert.ToInt64((self - d).TotalSeconds);
		}
	}
}
