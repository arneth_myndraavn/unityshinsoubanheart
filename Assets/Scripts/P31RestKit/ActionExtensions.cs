﻿using System;
using UnityEngine;

namespace Prime31
{
	// Token: 0x02000007 RID: 7
	public static class ActionExtensions
	{
		// Token: 0x0600001C RID: 28 RVA: 0x00002DC0 File Offset: 0x00000FC0
		private static void invoke(Delegate listener, object[] args)
		{
			if (!listener.Method.IsStatic && (listener.Target == null || listener.Target.Equals(null)))
			{
				Debug.LogError("an event listener is still subscribed to an event with the method " + listener.Method.Name + " even though it is null. Be sure to balance your event subscriptions.");
			}
			else
			{
				listener.Method.Invoke(listener.Target, args);
			}
		}

		// Token: 0x0600001D RID: 29 RVA: 0x00002E34 File Offset: 0x00001034
		public static void fire(this Action handler)
		{
			if (handler != null)
			{
				object[] args = new object[0];
				foreach (Delegate listener in handler.GetInvocationList())
				{
					ActionExtensions.invoke(listener, args);
				}
			}
		}

		// Token: 0x0600001E RID: 30 RVA: 0x00002E7C File Offset: 0x0000107C
		public static void fire<T>(this Action<T> handler, T param)
		{
			if (handler != null)
			{
				object[] args = new object[]
				{
					param
				};
				foreach (Delegate listener in handler.GetInvocationList())
				{
					ActionExtensions.invoke(listener, args);
				}
			}
		}

		// Token: 0x0600001F RID: 31 RVA: 0x00002ECC File Offset: 0x000010CC
		public static void fire<T, U>(this Action<T, U> handler, T param1, U param2)
		{
			if (handler != null)
			{
				object[] args = new object[]
				{
					param1,
					param2
				};
				foreach (Delegate listener in handler.GetInvocationList())
				{
					ActionExtensions.invoke(listener, args);
				}
			}
		}

		// Token: 0x06000020 RID: 32 RVA: 0x00002F28 File Offset: 0x00001128
		public static void fire<T, U, V>(this Action<T, U, V> handler, T param1, U param2, V param3)
		{
			if (handler != null)
			{
				object[] args = new object[]
				{
					param1,
					param2,
					param3
				};
				foreach (Delegate listener in handler.GetInvocationList())
				{
					ActionExtensions.invoke(listener, args);
				}
			}
		}
	}
}
