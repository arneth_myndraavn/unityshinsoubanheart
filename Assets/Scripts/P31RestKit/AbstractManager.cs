﻿using System;
using UnityEngine;

namespace Prime31
{
	// Token: 0x0200000D RID: 13
	public abstract class AbstractManager : MonoBehaviour
	{
		// Token: 0x17000007 RID: 7
		// (get) Token: 0x0600004C RID: 76 RVA: 0x00004354 File Offset: 0x00002554
		public static MonoBehaviour coroutineSurrogate
		{
			get
			{
				if (AbstractManager._prime31GameObjectMonobehaviourRef == null)
				{
					GameObject prime31ManagerGameObject = AbstractManager.getPrime31ManagerGameObject();
					AbstractManager._prime31GameObjectMonobehaviourRef = prime31ManagerGameObject.AddComponent<MonoBehaviour>();
				}
				return AbstractManager._prime31GameObjectMonobehaviourRef;
			}
		}

		// Token: 0x0600004E RID: 78 RVA: 0x0000439C File Offset: 0x0000259C
		public static ThreadingCallbackHelper getThreadingCallbackHelper()
		{
			return AbstractManager._threadingCallbackHelper;
		}

		// Token: 0x0600004F RID: 79 RVA: 0x000043B8 File Offset: 0x000025B8
		public static void createThreadingCallbackHelper()
		{
			if (!(AbstractManager._threadingCallbackHelper != null))
			{
				AbstractManager._threadingCallbackHelper = (UnityEngine.Object.FindObjectOfType(typeof(ThreadingCallbackHelper)) as ThreadingCallbackHelper);
				if (!(AbstractManager._threadingCallbackHelper != null))
				{
					GameObject prime31ManagerGameObject = AbstractManager.getPrime31ManagerGameObject();
					AbstractManager._threadingCallbackHelper = prime31ManagerGameObject.AddComponent<ThreadingCallbackHelper>();
				}
			}
		}

		// Token: 0x06000050 RID: 80 RVA: 0x0000441C File Offset: 0x0000261C
		public static GameObject getPrime31ManagerGameObject()
		{
			GameObject prime31GameObject;
			if (AbstractManager._prime31GameObject != null)
			{
				prime31GameObject = AbstractManager._prime31GameObject;
			}
			else
			{
				AbstractManager._prime31GameObject = GameObject.Find("prime[31]");
				if (AbstractManager._prime31GameObject == null)
				{
					AbstractManager._prime31GameObject = new GameObject("prime[31]");
					UnityEngine.Object.DontDestroyOnLoad(AbstractManager._prime31GameObject);
				}
				prime31GameObject = AbstractManager._prime31GameObject;
			}
			return prime31GameObject;
		}

		// Token: 0x06000051 RID: 81 RVA: 0x0000448C File Offset: 0x0000268C
		public static void initialize(Type type)
		{
			try
			{
				MonoBehaviour x = UnityEngine.Object.FindObjectOfType(type) as MonoBehaviour;
				if (!(x != null))
				{
					GameObject prime31ManagerGameObject = AbstractManager.getPrime31ManagerGameObject();
					GameObject gameObject = new GameObject(type.ToString());
					gameObject.AddComponent(type);
					gameObject.transform.parent = prime31ManagerGameObject.transform;
					//UnityEngine.Object.DontDestroyOnLoad(gameObject);
				}
			}
			catch (UnityException)
			{
				Debug.LogWarning(string.Concat(new object[]
				{
					"It looks like you have the ",
					type,
					" on a GameObject in your scene. Our new prefab-less manager system does not require the ",
					type,
					" to be on a GameObject.\nIt will be added to your scene at runtime automatically for you. Please remove the script from your scene."
				}));
			}
		}

		// Token: 0x06000052 RID: 82 RVA: 0x00004538 File Offset: 0x00002738
		private void Awake()
		{
			base.gameObject.name = base.GetType().ToString();
			UnityEngine.Object.DontDestroyOnLoad(this);
		}

		// Token: 0x0400001F RID: 31
		private static MonoBehaviour _prime31GameObjectMonobehaviourRef;

		// Token: 0x04000020 RID: 32
		private static ThreadingCallbackHelper _threadingCallbackHelper;

		// Token: 0x04000021 RID: 33
		private static GameObject _prime31GameObject;
	}
}
