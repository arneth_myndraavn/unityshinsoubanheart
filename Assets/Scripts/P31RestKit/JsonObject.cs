﻿using System;
using System.Collections.Generic;

namespace Prime31
{
	// Token: 0x02000019 RID: 25
	public class JsonObject : Dictionary<string, object>
	{
		// Token: 0x0600008C RID: 140 RVA: 0x00006358 File Offset: 0x00004558
		public override string ToString()
		{
			return JsonFormatter.prettyPrint(SimpleJson.encode(this)) ?? string.Empty;
		}
	}
}
