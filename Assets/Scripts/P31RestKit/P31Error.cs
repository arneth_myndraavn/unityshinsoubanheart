﻿using System;
using System.Collections.Generic;

namespace Prime31
{
	// Token: 0x02000025 RID: 37
	public sealed class P31Error
	{
		// Token: 0x1700000B RID: 11
		// (get) Token: 0x060000D8 RID: 216 RVA: 0x0000816C File Offset: 0x0000636C
		// (set) Token: 0x060000D9 RID: 217 RVA: 0x00008186 File Offset: 0x00006386
		public string message { get; private set; }

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x060000DA RID: 218 RVA: 0x00008190 File Offset: 0x00006390
		// (set) Token: 0x060000DB RID: 219 RVA: 0x000081AA File Offset: 0x000063AA
		public string domain { get; private set; }

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x060000DC RID: 220 RVA: 0x000081B4 File Offset: 0x000063B4
		// (set) Token: 0x060000DD RID: 221 RVA: 0x000081CE File Offset: 0x000063CE
		public int code { get; private set; }

		// Token: 0x1700000E RID: 14
		// (get) Token: 0x060000DE RID: 222 RVA: 0x000081D8 File Offset: 0x000063D8
		// (set) Token: 0x060000DF RID: 223 RVA: 0x000081F2 File Offset: 0x000063F2
		public Dictionary<string, object> userInfo { get; private set; }

		// Token: 0x060000E1 RID: 225 RVA: 0x00008204 File Offset: 0x00006404
		public static P31Error errorFromJson(string json)
		{
			P31Error p31Error = new P31Error();
			P31Error result;
			if (!json.StartsWith("{"))
			{
				p31Error.message = json;
				p31Error._containsOnlyMessage = true;
				result = p31Error;
			}
			else
			{
				Dictionary<string, object> dictionary = Json.decode(json) as Dictionary<string, object>;
				if (dictionary == null)
				{
					p31Error.message = "Unknown error";
				}
				else
				{
					p31Error.message = ((!dictionary.ContainsKey("message")) ? null : dictionary["message"].ToString());
					p31Error.domain = ((!dictionary.ContainsKey("domain")) ? null : dictionary["domain"].ToString());
					p31Error.code = ((!dictionary.ContainsKey("code")) ? -1 : int.Parse(dictionary["code"].ToString()));
					p31Error.userInfo = ((!dictionary.ContainsKey("userInfo")) ? null : (dictionary["userInfo"] as Dictionary<string, object>));
				}
				result = p31Error;
			}
			return result;
		}

		// Token: 0x060000E2 RID: 226 RVA: 0x00008324 File Offset: 0x00006524
		public override string ToString()
		{
			string result;
			if (this._containsOnlyMessage)
			{
				result = string.Format("[P31Error]: {0}", this.message);
			}
			else
			{
				try
				{
					string input = Json.encode(this);
					result = string.Format("[P31Error]: {0}", JsonFormatter.prettyPrint(input));
				}
				catch (Exception)
				{
					result = string.Format("[P31Error]: {0}", this.message);
				}
			}
			return result;
		}

		// Token: 0x0400005E RID: 94
		private bool _containsOnlyMessage;
	}
}
