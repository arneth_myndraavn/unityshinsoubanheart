﻿using System;

namespace Prime31
{
	// Token: 0x0200001B RID: 27
	public interface IJsonSerializerStrategy
	{
		// Token: 0x060000A8 RID: 168
		bool serializeNonPrimitiveObject(object input, out object output);

		// Token: 0x060000A9 RID: 169
		object deserializeObject(object value, Type type);
	}
}
