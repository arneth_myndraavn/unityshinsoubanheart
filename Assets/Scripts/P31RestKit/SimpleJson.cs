﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Prime31
{
	// Token: 0x0200001A RID: 26
	public class SimpleJson
	{
		// Token: 0x17000008 RID: 8
		// (get) Token: 0x0600008D RID: 141 RVA: 0x00006384 File Offset: 0x00004584
		// (set) Token: 0x0600008E RID: 142 RVA: 0x000063B0 File Offset: 0x000045B0
		public static IJsonSerializerStrategy currentJsonSerializerStrategy
		{
			get
			{
				IJsonSerializerStrategy result;
				if ((result = SimpleJson._currentJsonSerializerStrategy) == null)
				{
					result = (SimpleJson._currentJsonSerializerStrategy = SimpleJson.pocoJsonSerializerStrategy);
				}
				return result;
			}
			set
			{
				SimpleJson.currentJsonSerializerStrategy = value;
			}
		}

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x0600008F RID: 143 RVA: 0x000063BC File Offset: 0x000045BC
		public static PocoJsonSerializerStrategy pocoJsonSerializerStrategy
		{
			get
			{
				PocoJsonSerializerStrategy result;
				if ((result = SimpleJson._pocoJsonSerializerStrategy) == null)
				{
					result = (SimpleJson._pocoJsonSerializerStrategy = new PocoJsonSerializerStrategy());
				}
				return result;
			}
		}

		// Token: 0x06000091 RID: 145 RVA: 0x000063F0 File Offset: 0x000045F0
		public static string encode(object obj)
		{
			StringBuilder stringBuilder = new StringBuilder(2000);
			bool flag = SimpleJson.serializeValue(SimpleJson.currentJsonSerializerStrategy, obj, stringBuilder);
			return (!flag) ? null : stringBuilder.ToString();
		}

		// Token: 0x06000092 RID: 146 RVA: 0x00006430 File Offset: 0x00004630
		public static bool tryDeserializeObject(string json, out object obj)
		{
			bool result = true;
			if (json != null)
			{
				char[] json2 = json.ToCharArray();
				int num = 0;
				obj = SimpleJson.parseValue(json2, ref num, ref result);
			}
			else
			{
				obj = null;
			}
			return result;
		}

		// Token: 0x06000093 RID: 147 RVA: 0x00006470 File Offset: 0x00004670
		public static object decode(string json)
		{
			object obj;
			object result;
			if (SimpleJson.tryDeserializeObject(json, out obj))
			{
				result = obj;
			}
			else
			{
				Utils.logObject("Something went wrong deserializing the json. We got a null return. Here is the json we tried to deserialize: " + json);
				result = null;
			}
			return result;
		}

		// Token: 0x06000094 RID: 148 RVA: 0x000064B0 File Offset: 0x000046B0
		private static object decode(string json, Type type)
		{
			return SimpleJson.decode(json, type, null);
		}

		// Token: 0x06000095 RID: 149 RVA: 0x000064D0 File Offset: 0x000046D0
		public static T decode<T>(string json)
		{
			return (T)((object)SimpleJson.decode(json, typeof(T), null));
		}

		// Token: 0x06000096 RID: 150 RVA: 0x000064FC File Offset: 0x000046FC
		public static T decode<T>(string json, string rootElement) where T : new()
		{
			return (T)((object)SimpleJson.decode(json, typeof(T), rootElement));
		}

		// Token: 0x06000097 RID: 151 RVA: 0x00006528 File Offset: 0x00004728
		private static object decode(string json, Type type, string rootElement = null)
		{
			object obj = SimpleJson.decode(json);
			object result;
			if (type == null || (obj != null && obj.GetType().IsAssignableFrom(type)))
			{
				result = obj;
			}
			else
			{
				if (rootElement != null)
				{
					if (obj is JsonObject)
					{
						JsonObject jsonObject = obj as JsonObject;
						if (jsonObject.ContainsKey(rootElement))
						{
							obj = jsonObject[rootElement];
						}
						else
						{
							Utils.logObject(string.Format("A rootElement was requested ({0})  but does not exist in the decoded Dictionary", rootElement));
						}
					}
					else
					{
						Utils.logObject(string.Format("A rootElement was requested ({0}) but the decoded object is not a Dictionary. It is a {1}", rootElement, obj.GetType()));
					}
				}
				result = SimpleJson.currentJsonSerializerStrategy.deserializeObject(obj, type);
			}
			return result;
		}

		// Token: 0x06000098 RID: 152 RVA: 0x000065D8 File Offset: 0x000047D8
		public static T decodeObject<T>(object jsonObject, string rootElement = null)
		{
			Type typeFromHandle = typeof(T);
			T result;
			if (typeFromHandle == null || (jsonObject != null && jsonObject.GetType().IsAssignableFrom(typeFromHandle)))
			{
				result = (T)((object)jsonObject);
			}
			else
			{
				if (rootElement != null)
				{
					if (jsonObject is Dictionary<string, object>)
					{
						Dictionary<string, object> dictionary = jsonObject as Dictionary<string, object>;
						if (dictionary.ContainsKey(rootElement))
						{
							jsonObject = dictionary[rootElement];
						}
						else
						{
							Utils.logObject(string.Format("A rootElement was requested ({0})  but does not exist in the decoded Dictionary", rootElement));
						}
					}
					else
					{
						Utils.logObject(string.Format("A rootElement was requested ({0}) but the decoded object is not a Dictionary. It is a {1}", rootElement, jsonObject.GetType()));
					}
				}
				result = (T)((object)SimpleJson.currentJsonSerializerStrategy.deserializeObject(jsonObject, typeFromHandle));
			}
			return result;
		}

		// Token: 0x06000099 RID: 153 RVA: 0x00006698 File Offset: 0x00004898
		protected static IDictionary<string, object> parseObject(char[] json, ref int index, ref bool success)
		{
			IDictionary<string, object> dictionary = new JsonObject();
			SimpleJson.nextToken(json, ref index);
			bool flag = false;
			while (!flag)
			{
				int num = SimpleJson.lookAhead(json, index);
				if (num != 0)
				{
					if (num == 6)
					{
						SimpleJson.nextToken(json, ref index);
					}
					else
					{
						if (num == 2)
						{
							SimpleJson.nextToken(json, ref index);
							return dictionary;
						}
						string key = SimpleJson.parseString(json, ref index, ref success);
						if (!success)
						{
							success = false;
							return null;
						}
						num = SimpleJson.nextToken(json, ref index);
						if (num != 5)
						{
							success = false;
							return null;
						}
						object value = SimpleJson.parseValue(json, ref index, ref success);
						if (!success)
						{
							success = false;
							return null;
						}
						dictionary[key] = value;
					}
					continue;
				}
				success = false;
				return null;
			}
			return dictionary;
		}

		// Token: 0x0600009A RID: 154 RVA: 0x00006770 File Offset: 0x00004970
		protected static JsonArray parseArray(char[] json, ref int index, ref bool success)
		{
			JsonArray jsonArray = new JsonArray();
			SimpleJson.nextToken(json, ref index);
			bool flag = false;
			while (!flag)
			{
				int num = SimpleJson.lookAhead(json, index);
				if (num != 0)
				{
					if (num == 6)
					{
						SimpleJson.nextToken(json, ref index);
					}
					else
					{
						if (num == 4)
						{
							SimpleJson.nextToken(json, ref index);
							break;
						}
						object item = SimpleJson.parseValue(json, ref index, ref success);
						if (!success)
						{
							return null;
						}
						jsonArray.Add(item);
					}
					continue;
				}
				success = false;
				return null;
			}
			return jsonArray;
		}

		// Token: 0x0600009B RID: 155 RVA: 0x00006808 File Offset: 0x00004A08
		protected static object parseValue(char[] json, ref int index, ref bool success)
		{
			switch (SimpleJson.lookAhead(json, index))
			{
			case 1:
				return SimpleJson.parseObject(json, ref index, ref success);
			case 3:
				return SimpleJson.parseArray(json, ref index, ref success);
			case 7:
				return SimpleJson.parseString(json, ref index, ref success);
			case 8:
				return SimpleJson.parseNumber(json, ref index, ref success);
			case 9:
				SimpleJson.nextToken(json, ref index);
				return true;
			case 10:
				SimpleJson.nextToken(json, ref index);
				return false;
			case 11:
				SimpleJson.nextToken(json, ref index);
				return null;
			}
			success = false;
			return null;
		}

		// Token: 0x0600009C RID: 156 RVA: 0x000068DC File Offset: 0x00004ADC
		protected static string parseString(char[] json, ref int index, ref bool success)
		{
			StringBuilder stringBuilder = new StringBuilder(2000);
			SimpleJson.eatWhitespace(json, ref index);
			char c = json[index++];
			bool flag = false;
			while (!flag)
			{
				if (index == json.Length)
				{
					break;
				}
				c = json[index++];
				if (c == '"')
				{
					flag = true;
					break;
				}
				if (c == '\\')
				{
					if (index == json.Length)
					{
						break;
					}
					c = json[index++];
					if (c == '"')
					{
						stringBuilder.Append('"');
					}
					else if (c == '\\')
					{
						stringBuilder.Append('\\');
					}
					else if (c == '/')
					{
						stringBuilder.Append('/');
					}
					else if (c == 'b')
					{
						stringBuilder.Append('\b');
					}
					else if (c == 'f')
					{
						stringBuilder.Append('\f');
					}
					else if (c == 'n')
					{
						stringBuilder.Append('\n');
					}
					else if (c == 'r')
					{
						stringBuilder.Append('\r');
					}
					else if (c == 't')
					{
						stringBuilder.Append('\t');
					}
					else if (c == 'u')
					{
						int num = json.Length - index;
						if (num >= 4)
						{
							uint num2;
							string result;
							if (!(success = uint.TryParse(new string(json, index, 4), NumberStyles.HexNumber, CultureInfo.InvariantCulture, out num2)))
							{
								result = "";
							}
							else
							{
								if (55296U > num2 || num2 > 56319U)
								{
									stringBuilder.Append(char.ConvertFromUtf32((int)num2));
									index += 4;
									continue;
								}
								index += 4;
								num = json.Length - index;
								if (num >= 6)
								{
									uint num3;
									if (new string(json, index, 2) == "\\u" && uint.TryParse(new string(json, index + 2, 4), NumberStyles.HexNumber, CultureInfo.InvariantCulture, out num3))
									{
										if (56320U <= num3 && num3 <= 57343U)
										{
											stringBuilder.Append((char)num2);
											stringBuilder.Append((char)num3);
											index += 6;
											continue;
										}
									}
								}
								success = false;
								result = "";
							}
							return result;
						}
						break;
					}
				}
				else
				{
					stringBuilder.Append(c);
				}
			}
			if (!flag)
			{
				success = false;
				return null;
			}
			return stringBuilder.ToString();
		}

		// Token: 0x0600009D RID: 157 RVA: 0x00006B64 File Offset: 0x00004D64
		protected static object parseNumber(char[] json, ref int index, ref bool success)
		{
			SimpleJson.eatWhitespace(json, ref index);
			int lastIndexOfNumber = SimpleJson.getLastIndexOfNumber(json, index);
			int length = lastIndexOfNumber - index + 1;
			string text = new string(json, index, length);
			object result;
			if (text.IndexOf(".", StringComparison.OrdinalIgnoreCase) != -1 || text.IndexOf("e", StringComparison.OrdinalIgnoreCase) != -1)
			{
				double num;
				success = double.TryParse(new string(json, index, length), NumberStyles.Any, CultureInfo.InvariantCulture, out num);
				result = num;
			}
			else
			{
				long num2;
				success = long.TryParse(new string(json, index, length), NumberStyles.Any, CultureInfo.InvariantCulture, out num2);
				result = num2;
			}
			index = lastIndexOfNumber + 1;
			return result;
		}

		// Token: 0x0600009E RID: 158 RVA: 0x00006C18 File Offset: 0x00004E18
		protected static int getLastIndexOfNumber(char[] json, int index)
		{
			int i;
			for (i = index; i < json.Length; i++)
			{
				if ("0123456789+-.eE".IndexOf(json[i]) == -1)
				{
					break;
				}
			}
			return i - 1;
		}

		// Token: 0x0600009F RID: 159 RVA: 0x00006C5C File Offset: 0x00004E5C
		protected static void eatWhitespace(char[] json, ref int index)
		{
			while (index < json.Length)
			{
				if (" \t\n\r\b\f".IndexOf(json[index]) == -1)
				{
					break;
				}
				index++;
			}
		}

		// Token: 0x060000A0 RID: 160 RVA: 0x00006C90 File Offset: 0x00004E90
		protected static int lookAhead(char[] json, int index)
		{
			int num = index;
			return SimpleJson.nextToken(json, ref num);
		}

		// Token: 0x060000A1 RID: 161 RVA: 0x00006CB0 File Offset: 0x00004EB0
		protected static int nextToken(char[] json, ref int index)
		{
			SimpleJson.eatWhitespace(json, ref index);
			int result;
			if (index == json.Length)
			{
				result = 0;
			}
			else
			{
				char c = json[index];
				index++;
				switch (c)
				{
				case ',':
					result = 6;
					break;
				case '-':
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9':
					result = 8;
					break;
				default:
					switch (c)
					{
					case '[':
						result = 3;
						break;
					default:
						switch (c)
						{
						case '{':
							result = 1;
							break;
						default:
							if (c != '"')
							{
								index--;
								int num = json.Length - index;
								if (num >= 5)
								{
									if (json[index] == 'f' && json[index + 1] == 'a' && json[index + 2] == 'l' && json[index + 3] == 's' && json[index + 4] == 'e')
									{
										index += 5;
										result = 10;
										break;
									}
								}
								if (num >= 4)
								{
									if (json[index] == 't' && json[index + 1] == 'r' && json[index + 2] == 'u' && json[index + 3] == 'e')
									{
										index += 4;
										result = 9;
										break;
									}
								}
								if (num >= 4)
								{
									if (json[index] == 'n' && json[index + 1] == 'u' && json[index + 2] == 'l' && json[index + 3] == 'l')
									{
										index += 4;
										result = 11;
										break;
									}
								}
								result = 0;
							}
							else
							{
								result = 7;
							}
							break;
						case '}':
							result = 2;
							break;
						}
						break;
					case ']':
						result = 4;
						break;
					}
					break;
				case ':':
					result = 5;
					break;
				}
			}
			return result;
		}

		// Token: 0x060000A2 RID: 162 RVA: 0x00006E98 File Offset: 0x00005098
		protected static bool serializeValue(IJsonSerializerStrategy jsonSerializerStrategy, object value, StringBuilder builder)
		{
			bool flag = true;
			if (value is string)
			{
				flag = SimpleJson.serializeString((string)value, builder);
			}
			else if (value is IDictionary<string, object>)
			{
				IDictionary<string, object> dictionary = (IDictionary<string, object>)value;
				flag = SimpleJson.serializeObject(jsonSerializerStrategy, dictionary.Keys, dictionary.Values, builder);
			}
			else if (value is IDictionary<string, string>)
			{
				IDictionary<string, string> dictionary2 = (IDictionary<string, string>)value;
				flag = SimpleJson.serializeObject(jsonSerializerStrategy, dictionary2.Keys, dictionary2.Values, builder);
			}
			else if (value is IDictionary)
			{
				IDictionary dictionary3 = (IDictionary)value;
				flag = SimpleJson.serializeObject(jsonSerializerStrategy, dictionary3.Keys, dictionary3.Values, builder);
			}
			else if (value is IEnumerable)
			{
				flag = SimpleJson.serializeArray(jsonSerializerStrategy, (IEnumerable)value, builder);
			}
			else if (SimpleJson.isNumeric(value))
			{
				flag = SimpleJson.serializeNumber(value, builder);
			}
			else if (value is bool)
			{
				builder.Append((!(bool)value) ? "false" : "true");
			}
			else if (value == null)
			{
				builder.Append("null");
			}
			else
			{
				object value2;
				flag = jsonSerializerStrategy.serializeNonPrimitiveObject(value, out value2);
				if (flag)
				{
					SimpleJson.serializeValue(jsonSerializerStrategy, value2, builder);
				}
			}
			return flag;
		}

		// Token: 0x060000A3 RID: 163 RVA: 0x00006FF0 File Offset: 0x000051F0
		protected static bool serializeObject(IJsonSerializerStrategy jsonSerializerStrategy, IEnumerable keys, IEnumerable values, StringBuilder builder)
		{
			builder.Append("{");
			IEnumerator enumerator = keys.GetEnumerator();
			IEnumerator enumerator2 = values.GetEnumerator();
			bool flag = true;
			while (enumerator.MoveNext() && enumerator2.MoveNext())
			{
				object obj = enumerator.Current;
				object value = enumerator2.Current;
				if (!flag)
				{
					builder.Append(",");
				}
				if (obj is string)
				{
					SimpleJson.serializeString((string)obj, builder);
				}
				else if (!SimpleJson.serializeValue(jsonSerializerStrategy, value, builder))
				{
					return false;
				}
				builder.Append(":");
				if (SimpleJson.serializeValue(jsonSerializerStrategy, value, builder))
				{
					flag = false;
					continue;
				}
				return false;
			}
			builder.Append("}");
			return true;
		}

		// Token: 0x060000A4 RID: 164 RVA: 0x000070C8 File Offset: 0x000052C8
		protected static bool serializeArray(IJsonSerializerStrategy jsonSerializerStrategy, IEnumerable anArray, StringBuilder builder)
		{
			builder.Append("[");
			bool flag = true;
			IEnumerator enumerator = anArray.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					object value = enumerator.Current;
					if (!flag)
					{
						builder.Append(",");
					}
					if (!SimpleJson.serializeValue(jsonSerializerStrategy, value, builder))
					{
						return false;
					}
					flag = false;
				}
			}
			finally
			{
				IDisposable disposable;
				if ((disposable = (enumerator as IDisposable)) != null)
				{
					disposable.Dispose();
				}
			}
			builder.Append("]");
			return true;
		}

		// Token: 0x060000A5 RID: 165 RVA: 0x0000716C File Offset: 0x0000536C
		protected static bool serializeString(string aString, StringBuilder builder)
		{
			builder.Append("\"");
			foreach (char c in aString.ToCharArray())
			{
				if (c == '"')
				{
					builder.Append("\\\"");
				}
				else if (c == '\\')
				{
					builder.Append("\\\\");
				}
				else if (c == '\b')
				{
					builder.Append("\\b");
				}
				else if (c == '\f')
				{
					builder.Append("\\f");
				}
				else if (c == '\n')
				{
					builder.Append("\\n");
				}
				else if (c == '\r')
				{
					builder.Append("\\r");
				}
				else if (c == '\t')
				{
					builder.Append("\\t");
				}
				else
				{
					builder.Append(c);
				}
			}
			builder.Append("\"");
			return true;
		}

		// Token: 0x060000A6 RID: 166 RVA: 0x00007274 File Offset: 0x00005474
		protected static bool serializeNumber(object number, StringBuilder builder)
		{
			if (number is long)
			{
				builder.Append(((long)number).ToString(CultureInfo.InvariantCulture));
			}
			else if (number is ulong)
			{
				builder.Append(((ulong)number).ToString(CultureInfo.InvariantCulture));
			}
			else if (number is int)
			{
				builder.Append(((int)number).ToString(CultureInfo.InvariantCulture));
			}
			else if (number is uint)
			{
				builder.Append(((uint)number).ToString(CultureInfo.InvariantCulture));
			}
			else if (number is decimal)
			{
				builder.Append(((decimal)number).ToString(CultureInfo.InvariantCulture));
			}
			else if (number is float)
			{
				builder.Append(((float)number).ToString(CultureInfo.InvariantCulture));
			}
			else
			{
				builder.Append(Convert.ToDouble(number, CultureInfo.InvariantCulture).ToString("r", CultureInfo.InvariantCulture));
			}
			return true;
		}

		// Token: 0x060000A7 RID: 167 RVA: 0x000073C0 File Offset: 0x000055C0
		protected static bool isNumeric(object value)
		{
			return value is sbyte || value is byte || value is short || value is ushort || value is int || value is uint || value is long || value is ulong || value is float || value is double || value is decimal;
		}

		// Token: 0x04000040 RID: 64
		private const int TOKEN_NONE = 0;

		// Token: 0x04000041 RID: 65
		private const int TOKEN_CURLY_OPEN = 1;

		// Token: 0x04000042 RID: 66
		private const int TOKEN_CURLY_CLOSE = 2;

		// Token: 0x04000043 RID: 67
		private const int TOKEN_SQUARED_OPEN = 3;

		// Token: 0x04000044 RID: 68
		private const int TOKEN_SQUARED_CLOSE = 4;

		// Token: 0x04000045 RID: 69
		private const int TOKEN_COLON = 5;

		// Token: 0x04000046 RID: 70
		private const int TOKEN_COMMA = 6;

		// Token: 0x04000047 RID: 71
		private const int TOKEN_STRING = 7;

		// Token: 0x04000048 RID: 72
		private const int TOKEN_NUMBER = 8;

		// Token: 0x04000049 RID: 73
		private const int TOKEN_TRUE = 9;

		// Token: 0x0400004A RID: 74
		private const int TOKEN_FALSE = 10;

		// Token: 0x0400004B RID: 75
		private const int TOKEN_NULL = 11;

		// Token: 0x0400004C RID: 76
		private const int BUILDER_CAPACITY = 2000;

		// Token: 0x0400004D RID: 77
		private static IJsonSerializerStrategy _currentJsonSerializerStrategy;

		// Token: 0x0400004E RID: 78
		private static PocoJsonSerializerStrategy _pocoJsonSerializerStrategy;
	}
}
