﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace Prime31
{
	// Token: 0x0200000C RID: 12
	public class MonoBehaviourGUI : MonoBehaviour
	{
		// Token: 0x0600003E RID: 62 RVA: 0x00003DF8 File Offset: 0x00001FF8
		private bool isRetinaOrLargeScreen()
		{
			return this._isWindowsPhone || Screen.width >= 960 || Screen.height >= 960;
		}

		// Token: 0x0600003F RID: 63 RVA: 0x00003E3C File Offset: 0x0000203C
		private void paintWindow(int id)
		{
			GUI.skin.label.alignment = TextAnchor.UpperLeft;
			this._logScrollPosition = GUILayout.BeginScrollView(this._logScrollPosition, new GUILayoutOption[0]);
			if (GUILayout.Button("Clear Console", new GUILayoutOption[0]))
			{
				this._logBuilder.Remove(0, this._logBuilder.Length);
			}
			GUILayout.Label(this._logBuilder.ToString(), new GUILayoutOption[0]);
			GUILayout.EndScrollView();
		}

		// Token: 0x06000040 RID: 64 RVA: 0x00003EB9 File Offset: 0x000020B9
		private void handleLog(string logString, string stackTrace, LogType type)
		{
			this._logBuilder.AppendFormat("{0}\n", logString);
		}

		// Token: 0x06000041 RID: 65 RVA: 0x00003ECE File Offset: 0x000020CE
		private void OnDestroy()
		{
			Application.RegisterLogCallback(null);
		}

		// Token: 0x06000042 RID: 66 RVA: 0x00003ED8 File Offset: 0x000020D8
		private void Update()
		{
			if (!this._logRegistered)
			{
				Application.RegisterLogCallback(new Application.LogCallback(this.handleLog));
				this._logRegistered = true;
				this._isWindowsPhone = Application.platform.ToString().ToLower().Contains("wp8");
			}
			bool flag = false;
			if (Input.GetMouseButtonDown(0))
			{
				float num = Time.time - this._previousClickTime;
				if (num < this._doubleClickDelay)
				{
					flag = true;
				}
				else
				{
					this._previousClickTime = Time.time;
				}
			}
			if (Input.touchCount == 2 || flag)
			{
				this._isShowingLogConsole = !this._isShowingLogConsole;
			}
		}

		// Token: 0x06000043 RID: 67 RVA: 0x00003F8C File Offset: 0x0000218C
		protected void beginColumn()
		{
			this._width = (float)(Screen.width / 2 - 15);
			this._buttonHeight = (float)((!this.isRetinaOrLargeScreen()) ? 30 : 70);
			GUI.skin.button.margin = new RectOffset(0, 0, 10, 0);
			GUI.skin.button.stretchWidth = true;
			GUI.skin.button.fixedHeight = this._buttonHeight;
			GUI.skin.button.wordWrap = false;
			if (this._isShowingLogConsole)
			{
				GUILayout.BeginArea(new Rect(0f, 0f, 0f, 0f));
			}
			else
			{
				GUILayout.BeginArea(new Rect(10f, 10f, this._width, (float)Screen.height));
			}
			GUILayout.BeginVertical(new GUILayoutOption[0]);
		}

		// Token: 0x06000044 RID: 68 RVA: 0x0000406D File Offset: 0x0000226D
		protected void endColumn()
		{
			this.endColumn(false);
		}

		// Token: 0x06000045 RID: 69 RVA: 0x00004078 File Offset: 0x00002278
		protected void endColumn(bool hasSecondColumn)
		{
			GUILayout.EndVertical();
			GUILayout.EndArea();
			if (this._isShowingLogConsole)
			{
				GUILayout.Window(1, new Rect(0f, 0f, (float)Screen.width, (float)Screen.height), new GUI.WindowFunction(this.paintWindow), "prime[31] Log Console - double tap to dismiss", new GUILayoutOption[0]);
			}
			if (hasSecondColumn)
			{
				this.beginRightColumn();
			}
		}

		// Token: 0x06000046 RID: 70 RVA: 0x000040E0 File Offset: 0x000022E0
		private void beginRightColumn()
		{
			if (this._isShowingLogConsole)
			{
				GUILayout.BeginArea(new Rect(0f, 0f, 0f, 0f));
			}
			else
			{
				GUILayout.BeginArea(new Rect((float)Screen.width - this._width - 10f, 10f, this._width, (float)Screen.height));
			}
			GUILayout.BeginVertical(new GUILayoutOption[0]);
		}

		// Token: 0x06000047 RID: 71 RVA: 0x00004158 File Offset: 0x00002358
		protected bool bottomRightButton(string text, float width = 150f)
		{
			return GUI.Button(new Rect((float)Screen.width - width - 10f, (float)Screen.height - this._buttonHeight - 10f, width, this._buttonHeight), text);
		}

		// Token: 0x06000048 RID: 72 RVA: 0x000041A0 File Offset: 0x000023A0
		protected bool bottomLeftButton(string text, float width = 150f)
		{
			return GUI.Button(new Rect(10f, (float)Screen.height - this._buttonHeight - 10f, width, this._buttonHeight), text);
		}

		// Token: 0x06000049 RID: 73 RVA: 0x000041E0 File Offset: 0x000023E0
		protected bool bottomCenterButton(string text, float width = 150f)
		{
			float left = (float)(Screen.width / 2) - width / 2f;
			return GUI.Button(new Rect(left, (float)Screen.height - this._buttonHeight - 10f, width, this._buttonHeight), text);
		}

		// Token: 0x0600004A RID: 74 RVA: 0x0000422C File Offset: 0x0000242C
		protected bool toggleButton(string defaultText, string selectedText)
		{
			if (!this._toggleButtons.ContainsKey(defaultText))
			{
				this._toggleButtons[defaultText] = true;
			}
			string text = (!this._toggleButtons[defaultText]) ? selectedText : defaultText;
			if (!this._toggleButtons[defaultText])
			{
				GUI.skin.button.fontStyle = FontStyle.BoldAndItalic;
				GUI.contentColor = Color.yellow;
			}
			else
			{
				GUI.skin.button.fontStyle = FontStyle.Bold;
				GUI.contentColor = Color.red;
			}
			if (GUILayout.Button(text, new GUILayoutOption[0]))
			{
				this._toggleButtons[defaultText] = (text != defaultText);
			}
			GUI.skin.button.fontStyle = FontStyle.Normal;
			GUI.contentColor = Color.white;
			return this._toggleButtons[defaultText];
		}

		// Token: 0x0600004B RID: 75 RVA: 0x00004314 File Offset: 0x00002514
		protected bool toggleButtonState(string defaultText)
		{
			if (!this._toggleButtons.ContainsKey(defaultText))
			{
				this._toggleButtons[defaultText] = true;
			}
			return this._toggleButtons[defaultText];
		}

		// Token: 0x04000015 RID: 21
		protected float _width;

		// Token: 0x04000016 RID: 22
		protected float _buttonHeight;

		// Token: 0x04000017 RID: 23
		protected Dictionary<string, bool> _toggleButtons = new Dictionary<string, bool>();

		// Token: 0x04000018 RID: 24
		protected StringBuilder _logBuilder = new StringBuilder();

		// Token: 0x04000019 RID: 25
		private bool _logRegistered;

		// Token: 0x0400001A RID: 26
		private Vector2 _logScrollPosition;

		// Token: 0x0400001B RID: 27
		private bool _isShowingLogConsole;

		// Token: 0x0400001C RID: 28
		private float _doubleClickDelay = 0.15f;

		// Token: 0x0400001D RID: 29
		private float _previousClickTime;

		// Token: 0x0400001E RID: 30
		private bool _isWindowsPhone;
	}
}
