﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Prime31
{
	// Token: 0x0200000F RID: 15
	public class DTOBase
	{
		// Token: 0x06000058 RID: 88 RVA: 0x000046A4 File Offset: 0x000028A4
		public static List<T> listFromJson<T>(string json) where T : DTOBase
		{
			List<object> list = json.listFromJson();
			List<T> list2 = new List<T>();
			foreach (object obj in list)
			{
				T item = Activator.CreateInstance<T>();
				item.setDataFromDictionary(obj as Dictionary<string, object>);
				list2.Add(item);
			}
			return list2;
		}

		// Token: 0x06000059 RID: 89 RVA: 0x00004734 File Offset: 0x00002934
		public void setDataFromJson(string json)
		{
			this.setDataFromDictionary(json.dictionaryFromJson());
		}

		// Token: 0x0600005A RID: 90 RVA: 0x00004744 File Offset: 0x00002944
		public void setDataFromDictionary(Dictionary<string, object> dict)
		{
			Dictionary<string, Action<object>> membersWithSetters = this.getMembersWithSetters();
			foreach (KeyValuePair<string, object> keyValuePair in dict)
			{
				if (membersWithSetters.ContainsKey(keyValuePair.Key))
				{
					try
					{
						membersWithSetters[keyValuePair.Key](keyValuePair.Value);
					}
					catch (Exception obj)
					{
						Utils.logObject(obj);
					}
				}
			}
		}

		// Token: 0x0600005B RID: 91 RVA: 0x000047F0 File Offset: 0x000029F0
		private bool shouldIncludeTypeWithSetters(Type type)
		{
			return !type.IsGenericType && type.Namespace.StartsWith("System");
		}

		// Token: 0x0600005C RID: 92 RVA: 0x00004834 File Offset: 0x00002A34
		protected Dictionary<string, Action<object>> getMembersWithSetters()
		{
			Dictionary<string, Action<object>> dictionary = new Dictionary<string, Action<object>>();
			FieldInfo[] fields = base.GetType().GetFields();
			for (int i = 0; i < fields.Length; i++)
			{
				FieldInfo fieldInfo = fields[i];
				if (this.shouldIncludeTypeWithSetters(fieldInfo.FieldType))
				{
					FieldInfo theInfo = fieldInfo;
					dictionary[fieldInfo.Name] = delegate(object val)
					{
						theInfo.SetValue(this, val);
					};
				}
			}
			foreach (PropertyInfo propertyInfo in base.GetType().GetProperties())
			{
				if (this.shouldIncludeTypeWithSetters(propertyInfo.PropertyType))
				{
					if (propertyInfo.CanWrite && propertyInfo.GetSetMethod() != null)
					{
						PropertyInfo theInfo = propertyInfo;
						dictionary[propertyInfo.Name] = delegate(object val)
						{
							theInfo.SetValue(this, val, null);
						};
					}
				}
			}
			return dictionary;
		}

		// Token: 0x0600005D RID: 93 RVA: 0x00004954 File Offset: 0x00002B54
		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.AppendFormat("[{0}]:", base.GetType());
			foreach (FieldInfo fieldInfo in base.GetType().GetFields())
			{
				stringBuilder.AppendFormat(", {0}: {1}", fieldInfo.Name, fieldInfo.GetValue(this));
			}
			foreach (PropertyInfo propertyInfo in base.GetType().GetProperties())
			{
				stringBuilder.AppendFormat(", {0}: {1}", propertyInfo.Name, propertyInfo.GetValue(this, null));
			}
			return stringBuilder.ToString();
		}
	}
}
