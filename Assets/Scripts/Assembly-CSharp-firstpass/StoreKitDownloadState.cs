﻿using System;

// Token: 0x02000004 RID: 4
public enum StoreKitDownloadState
{
	// Token: 0x04000002 RID: 2
	Waiting,
	// Token: 0x04000003 RID: 3
	Active,
	// Token: 0x04000004 RID: 4
	Paused,
	// Token: 0x04000005 RID: 5
	Finished,
	// Token: 0x04000006 RID: 6
	Failed,
	// Token: 0x04000007 RID: 7
	Cancelled
}
