﻿using System;
using System.Collections.Generic;
using Prime31;

// Token: 0x02000008 RID: 8
public class StoreKitTransaction
{
	// Token: 0x0600004F RID: 79 RVA: 0x000029F0 File Offset: 0x00000BF0
	public static List<StoreKitTransaction> transactionsFromJson(string json)
	{
		List<StoreKitTransaction> list = new List<StoreKitTransaction>();
		List<object> list2 = json.listFromJson();
		if (list2 == null)
		{
			return list;
		}
		foreach (object obj in list2)
		{
			Dictionary<string, object> dict = (Dictionary<string, object>)obj;
			list.Add(StoreKitTransaction.transactionFromDictionary(dict));
		}
		return list;
	}

	// Token: 0x06000050 RID: 80 RVA: 0x00002A74 File Offset: 0x00000C74
	public static StoreKitTransaction transactionFromJson(string json)
	{
		if (json.dictionaryFromJson() == null)
		{
			return new StoreKitTransaction();
		}
		return StoreKitTransaction.transactionFromDictionary(json.dictionaryFromJson());
	}

	// Token: 0x06000051 RID: 81 RVA: 0x00002AA0 File Offset: 0x00000CA0
	public static StoreKitTransaction transactionFromDictionary(Dictionary<string, object> dict)
	{
		StoreKitTransaction storeKitTransaction = new StoreKitTransaction();
		if (dict.ContainsKey("productIdentifier"))
		{
			storeKitTransaction.productIdentifier = dict["productIdentifier"].ToString();
		}
		if (dict.ContainsKey("transactionIdentifier"))
		{
			storeKitTransaction.transactionIdentifier = dict["transactionIdentifier"].ToString();
		}
		if (dict.ContainsKey("base64EncodedReceipt"))
		{
			storeKitTransaction.base64EncodedTransactionReceipt = dict["base64EncodedReceipt"].ToString();
		}
		if (dict.ContainsKey("quantity"))
		{
			storeKitTransaction.quantity = int.Parse(dict["quantity"].ToString());
		}
		return storeKitTransaction;
	}

	// Token: 0x06000052 RID: 82 RVA: 0x00002B54 File Offset: 0x00000D54
	public override string ToString()
	{
		return string.Format("<StoreKitTransaction> ID: {0}, quantity: {1}, transactionIdentifier: {2}", this.productIdentifier, this.quantity, this.transactionIdentifier);
	}

	// Token: 0x04000022 RID: 34
	public string productIdentifier;

	// Token: 0x04000023 RID: 35
	public string transactionIdentifier;

	// Token: 0x04000024 RID: 36
	public string base64EncodedTransactionReceipt;

	// Token: 0x04000025 RID: 37
	public int quantity;
}
