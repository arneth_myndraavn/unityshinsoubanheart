﻿using System;
using System.Collections.Generic;
using Prime31;

// Token: 0x02000007 RID: 7
public class StoreKitProduct
{
	// Token: 0x17000001 RID: 1
	// (get) Token: 0x0600003D RID: 61 RVA: 0x00002764 File Offset: 0x00000964
	// (set) Token: 0x0600003E RID: 62 RVA: 0x0000276C File Offset: 0x0000096C
	public string productIdentifier { get; private set; }

	// Token: 0x17000002 RID: 2
	// (get) Token: 0x0600003F RID: 63 RVA: 0x00002778 File Offset: 0x00000978
	// (set) Token: 0x06000040 RID: 64 RVA: 0x00002780 File Offset: 0x00000980
	public string title { get; private set; }

	// Token: 0x17000003 RID: 3
	// (get) Token: 0x06000041 RID: 65 RVA: 0x0000278C File Offset: 0x0000098C
	// (set) Token: 0x06000042 RID: 66 RVA: 0x00002794 File Offset: 0x00000994
	public string description { get; private set; }

	// Token: 0x17000004 RID: 4
	// (get) Token: 0x06000043 RID: 67 RVA: 0x000027A0 File Offset: 0x000009A0
	// (set) Token: 0x06000044 RID: 68 RVA: 0x000027A8 File Offset: 0x000009A8
	public string price { get; private set; }

	// Token: 0x17000005 RID: 5
	// (get) Token: 0x06000045 RID: 69 RVA: 0x000027B4 File Offset: 0x000009B4
	// (set) Token: 0x06000046 RID: 70 RVA: 0x000027BC File Offset: 0x000009BC
	public string currencySymbol { get; private set; }

	// Token: 0x17000006 RID: 6
	// (get) Token: 0x06000047 RID: 71 RVA: 0x000027C8 File Offset: 0x000009C8
	// (set) Token: 0x06000048 RID: 72 RVA: 0x000027D0 File Offset: 0x000009D0
	public string currencyCode { get; private set; }

	// Token: 0x17000007 RID: 7
	// (get) Token: 0x06000049 RID: 73 RVA: 0x000027DC File Offset: 0x000009DC
	// (set) Token: 0x0600004A RID: 74 RVA: 0x000027E4 File Offset: 0x000009E4
	public string formattedPrice { get; private set; }

	// Token: 0x0600004B RID: 75 RVA: 0x000027F0 File Offset: 0x000009F0
	public static List<StoreKitProduct> productsFromJson(string json)
	{
		List<StoreKitProduct> list = new List<StoreKitProduct>();
		List<object> list2 = json.listFromJson();
		foreach (object obj in list2)
		{
			Dictionary<string, object> ht = (Dictionary<string, object>)obj;
			list.Add(StoreKitProduct.productFromDictionary(ht));
		}
		return list;
	}

	// Token: 0x0600004C RID: 76 RVA: 0x0000286C File Offset: 0x00000A6C
	public static StoreKitProduct productFromDictionary(Dictionary<string, object> ht)
	{
		StoreKitProduct storeKitProduct = new StoreKitProduct();
		if (ht.ContainsKey("productIdentifier"))
		{
			storeKitProduct.productIdentifier = ht["productIdentifier"].ToString();
		}
		if (ht.ContainsKey("localizedTitle"))
		{
			storeKitProduct.title = ht["localizedTitle"].ToString();
		}
		if (ht.ContainsKey("localizedDescription"))
		{
			storeKitProduct.description = ht["localizedDescription"].ToString();
		}
		if (ht.ContainsKey("price"))
		{
			storeKitProduct.price = ht["price"].ToString();
		}
		if (ht.ContainsKey("currencySymbol"))
		{
			storeKitProduct.currencySymbol = ht["currencySymbol"].ToString();
		}
		if (ht.ContainsKey("currencyCode"))
		{
			storeKitProduct.currencyCode = ht["currencyCode"].ToString();
		}
		if (ht.ContainsKey("formattedPrice"))
		{
			storeKitProduct.formattedPrice = ht["formattedPrice"].ToString();
		}
		return storeKitProduct;
	}

	// Token: 0x0600004D RID: 77 RVA: 0x0000298C File Offset: 0x00000B8C
	public override string ToString()
	{
		return string.Format("<StoreKitProduct>\nID: {0}\nTitle: {1}\nDescription: {2}\nPrice: {3}\nCurrency Symbol: {4}\nFormatted Price: {5}\nCurrency Code: {6}", new object[]
		{
			this.productIdentifier,
			this.title,
			this.description,
			this.price,
			this.currencySymbol,
			this.formattedPrice,
			this.currencyCode
		});
	}
}
