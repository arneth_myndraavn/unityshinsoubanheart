﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

// Token: 0x02000003 RID: 3
public class StoreKitBinding
{
	// Token: 0x06000003 RID: 3
	[DllImport("__Internal")]
	private static extern bool _storeKitCanMakePayments();

	// Token: 0x06000004 RID: 4 RVA: 0x000020FC File Offset: 0x000002FC
	public static bool canMakePayments()
	{
		return Application.platform == RuntimePlatform.IPhonePlayer && StoreKitBinding._storeKitCanMakePayments();
	}

	// Token: 0x06000005 RID: 5
	[DllImport("__Internal")]
	private static extern string _storeKitGetAppStoreReceiptUrl();

	// Token: 0x06000006 RID: 6 RVA: 0x00002110 File Offset: 0x00000310
	public static string getAppStoreReceiptLocation()
	{
		if (Application.platform == RuntimePlatform.IPhonePlayer)
		{
			return StoreKitBinding._storeKitGetAppStoreReceiptUrl();
		}
		return null;
	}

	// Token: 0x06000007 RID: 7
	[DllImport("__Internal")]
	private static extern void _storeKitRequestProductData(string productIdentifier);

	// Token: 0x06000008 RID: 8 RVA: 0x00002124 File Offset: 0x00000324
	public static void requestProductData(string[] productIdentifiers)
	{
		if (Application.platform == RuntimePlatform.IPhonePlayer)
		{
			StoreKitBinding._storeKitRequestProductData(string.Join(",", productIdentifiers));
		}
	}

	// Token: 0x06000009 RID: 9
	[DllImport("__Internal")]
	private static extern void _storeKitPurchaseProduct(string productIdentifier, int quantity);

	// Token: 0x0600000A RID: 10 RVA: 0x00002144 File Offset: 0x00000344
	public static void purchaseProduct(string productIdentifier, int quantity)
	{
		if (Application.platform == RuntimePlatform.IPhonePlayer)
		{
			StoreKitBinding._storeKitPurchaseProduct(productIdentifier, quantity);
		}
	}

	// Token: 0x0600000B RID: 11
	[DllImport("__Internal")]
	private static extern void _storeKitFinishPendingTransactions();

	// Token: 0x0600000C RID: 12 RVA: 0x00002158 File Offset: 0x00000358
	public static void finishPendingTransactions()
	{
		if (Application.platform == RuntimePlatform.IPhonePlayer)
		{
			StoreKitBinding._storeKitFinishPendingTransactions();
		}
	}

	// Token: 0x0600000D RID: 13
	[DllImport("__Internal")]
	private static extern void _storeKitFinishPendingTransaction(string transactionIdentifier);

	// Token: 0x0600000E RID: 14 RVA: 0x0000216C File Offset: 0x0000036C
	public static void finishPendingTransaction(string transactionIdentifier)
	{
		if (Application.platform == RuntimePlatform.IPhonePlayer)
		{
			StoreKitBinding._storeKitFinishPendingTransaction(transactionIdentifier);
		}
	}

	// Token: 0x0600000F RID: 15
	[DllImport("__Internal")]
	private static extern void _storeKitPauseDownloads();

	// Token: 0x06000010 RID: 16 RVA: 0x00002180 File Offset: 0x00000380
	public static void pauseDownloads()
	{
		if (Application.platform == RuntimePlatform.IPhonePlayer)
		{
			StoreKitBinding._storeKitPauseDownloads();
		}
	}

	// Token: 0x06000011 RID: 17
	[DllImport("__Internal")]
	private static extern void _storeKitResumeDownloads();

	// Token: 0x06000012 RID: 18 RVA: 0x00002194 File Offset: 0x00000394
	public static void resumeDownloads()
	{
		if (Application.platform == RuntimePlatform.IPhonePlayer)
		{
			StoreKitBinding._storeKitResumeDownloads();
		}
	}

	// Token: 0x06000013 RID: 19
	[DllImport("__Internal")]
	private static extern void _storeKitCancelDownloads();

	// Token: 0x06000014 RID: 20 RVA: 0x000021A8 File Offset: 0x000003A8
	public static void cancelDownloads()
	{
		if (Application.platform == RuntimePlatform.IPhonePlayer)
		{
			StoreKitBinding._storeKitCancelDownloads();
		}
	}

	// Token: 0x06000015 RID: 21
	[DllImport("__Internal")]
	private static extern void _storeKitRestoreCompletedTransactions();

	// Token: 0x06000016 RID: 22 RVA: 0x000021BC File Offset: 0x000003BC
	public static void restoreCompletedTransactions()
	{
		if (Application.platform == RuntimePlatform.IPhonePlayer)
		{
			StoreKitBinding._storeKitRestoreCompletedTransactions();
		}
	}

	// Token: 0x06000017 RID: 23
	[DllImport("__Internal")]
	private static extern string _storeKitGetAllSavedTransactions();

	// Token: 0x06000018 RID: 24 RVA: 0x000021D0 File Offset: 0x000003D0
	public static List<StoreKitTransaction> getAllSavedTransactions()
	{
		if (Application.platform == RuntimePlatform.IPhonePlayer)
		{
			string json = StoreKitBinding._storeKitGetAllSavedTransactions();
			return StoreKitTransaction.transactionsFromJson(json);
		}
		return new List<StoreKitTransaction>();
	}

	// Token: 0x06000019 RID: 25
	[DllImport("__Internal")]
	private static extern void _storeKitDisplayStoreWithProductId(string productId);

	// Token: 0x0600001A RID: 26 RVA: 0x000021FC File Offset: 0x000003FC
	public static void displayStoreWithProductId(string productId)
	{
		if (Application.platform == RuntimePlatform.IPhonePlayer)
		{
			StoreKitBinding._storeKitDisplayStoreWithProductId(productId);
		}
	}
}
