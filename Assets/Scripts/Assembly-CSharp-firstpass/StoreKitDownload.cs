﻿using System;
using System.Collections.Generic;
using Prime31;

// Token: 0x02000005 RID: 5
public class StoreKitDownload
{
	// Token: 0x0600001C RID: 28 RVA: 0x00002218 File Offset: 0x00000418
	public static List<StoreKitDownload> downloadsFromJson(string json)
	{
		List<StoreKitDownload> list = new List<StoreKitDownload>();
		List<object> list2 = json.listFromJson();
		if (list2 == null)
		{
			return list;
		}
		foreach (object obj in list2)
		{
			Dictionary<string, object> dict = (Dictionary<string, object>)obj;
			list.Add(StoreKitDownload.downloadFromDictionary(dict));
		}
		return list;
	}

	// Token: 0x0600001D RID: 29 RVA: 0x0000229C File Offset: 0x0000049C
	public static StoreKitDownload downloadFromDictionary(Dictionary<string, object> dict)
	{
		StoreKitDownload storeKitDownload = new StoreKitDownload();
		if (dict.ContainsKey("downloadState"))
		{
			storeKitDownload.downloadState = (StoreKitDownloadState)int.Parse(dict["downloadState"].ToString());
		}
		if (dict.ContainsKey("contentLength"))
		{
			storeKitDownload.contentLength = double.Parse(dict["contentLength"].ToString());
		}
		if (dict.ContainsKey("contentIdentifier"))
		{
			storeKitDownload.contentIdentifier = dict["contentIdentifier"].ToString();
		}
		if (dict.ContainsKey("contentURL"))
		{
			storeKitDownload.contentURL = dict["contentURL"].ToString();
		}
		if (dict.ContainsKey("contentVersion"))
		{
			storeKitDownload.contentVersion = dict["contentVersion"].ToString();
		}
		if (dict.ContainsKey("error"))
		{
			storeKitDownload.error = dict["error"].ToString();
		}
		if (dict.ContainsKey("progress"))
		{
			storeKitDownload.progress = float.Parse(dict["progress"].ToString());
		}
		if (dict.ContainsKey("timeRemaining"))
		{
			storeKitDownload.timeRemaining = double.Parse(dict["timeRemaining"].ToString());
		}
		if (dict.ContainsKey("transaction"))
		{
			storeKitDownload.transaction = StoreKitTransaction.transactionFromDictionary(dict["transaction"] as Dictionary<string, object>);
		}
		return storeKitDownload;
	}

	// Token: 0x0600001E RID: 30 RVA: 0x00002420 File Offset: 0x00000620
	public override string ToString()
	{
		return string.Format("<StoreKitDownload> downloadState: {0}\n contentLength: {1}\n contentIdentifier: {2}\n contentURL: {3}\n contentVersion: {4}\n error: {5}\n progress: {6}\n transaction: {7}", new object[]
		{
			this.downloadState,
			this.contentLength,
			this.contentIdentifier,
			this.contentURL,
			this.contentVersion,
			this.error,
			this.progress,
			this.transaction
		});
	}

	// Token: 0x04000008 RID: 8
	public StoreKitDownloadState downloadState;

	// Token: 0x04000009 RID: 9
	public double contentLength;

	// Token: 0x0400000A RID: 10
	public string contentIdentifier;

	// Token: 0x0400000B RID: 11
	public string contentURL;

	// Token: 0x0400000C RID: 12
	public string contentVersion;

	// Token: 0x0400000D RID: 13
	public string error;

	// Token: 0x0400000E RID: 14
	public float progress;

	// Token: 0x0400000F RID: 15
	public double timeRemaining;

	// Token: 0x04000010 RID: 16
	public StoreKitTransaction transaction;
}
