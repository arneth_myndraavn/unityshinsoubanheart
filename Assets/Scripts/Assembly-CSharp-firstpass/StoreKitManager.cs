﻿using System;
using System.Collections.Generic;
using Prime31;

// Token: 0x02000006 RID: 6
public class StoreKitManager : AbstractManager
{
	// Token: 0x06000020 RID: 32 RVA: 0x0000249C File Offset: 0x0000069C
	static StoreKitManager()
	{
		AbstractManager.initialize(typeof(StoreKitManager));
	}

	// Token: 0x14000001 RID: 1
	// (add) Token: 0x06000021 RID: 33 RVA: 0x000024B4 File Offset: 0x000006B4
	// (remove) Token: 0x06000022 RID: 34 RVA: 0x000024CC File Offset: 0x000006CC
	public static event Action<List<StoreKitProduct>> productListReceivedEvent;

	// Token: 0x14000002 RID: 2
	// (add) Token: 0x06000023 RID: 35 RVA: 0x000024E4 File Offset: 0x000006E4
	// (remove) Token: 0x06000024 RID: 36 RVA: 0x000024FC File Offset: 0x000006FC
	public static event Action<string> productListRequestFailedEvent;

	// Token: 0x14000003 RID: 3
	// (add) Token: 0x06000025 RID: 37 RVA: 0x00002514 File Offset: 0x00000714
	// (remove) Token: 0x06000026 RID: 38 RVA: 0x0000252C File Offset: 0x0000072C
	public static event Action<StoreKitTransaction> productPurchaseAwaitingConfirmationEvent;

	// Token: 0x14000004 RID: 4
	// (add) Token: 0x06000027 RID: 39 RVA: 0x00002544 File Offset: 0x00000744
	// (remove) Token: 0x06000028 RID: 40 RVA: 0x0000255C File Offset: 0x0000075C
	public static event Action<StoreKitTransaction> purchaseSuccessfulEvent;

	// Token: 0x14000005 RID: 5
	// (add) Token: 0x06000029 RID: 41 RVA: 0x00002574 File Offset: 0x00000774
	// (remove) Token: 0x0600002A RID: 42 RVA: 0x0000258C File Offset: 0x0000078C
	public static event Action<string> purchaseFailedEvent;

	// Token: 0x14000006 RID: 6
	// (add) Token: 0x0600002B RID: 43 RVA: 0x000025A4 File Offset: 0x000007A4
	// (remove) Token: 0x0600002C RID: 44 RVA: 0x000025BC File Offset: 0x000007BC
	public static event Action<string> purchaseCancelledEvent;

	// Token: 0x14000007 RID: 7
	// (add) Token: 0x0600002D RID: 45 RVA: 0x000025D4 File Offset: 0x000007D4
	// (remove) Token: 0x0600002E RID: 46 RVA: 0x000025EC File Offset: 0x000007EC
	public static event Action<string> restoreTransactionsFailedEvent;

	// Token: 0x14000008 RID: 8
	// (add) Token: 0x0600002F RID: 47 RVA: 0x00002604 File Offset: 0x00000804
	// (remove) Token: 0x06000030 RID: 48 RVA: 0x0000261C File Offset: 0x0000081C
	public static event Action restoreTransactionsFinishedEvent;

	// Token: 0x14000009 RID: 9
	// (add) Token: 0x06000031 RID: 49 RVA: 0x00002634 File Offset: 0x00000834
	// (remove) Token: 0x06000032 RID: 50 RVA: 0x0000264C File Offset: 0x0000084C
	public static event Action<List<StoreKitDownload>> paymentQueueUpdatedDownloadsEvent;

	// Token: 0x06000033 RID: 51 RVA: 0x00002664 File Offset: 0x00000864
	public void productPurchaseAwaitingConfirmation(string json)
	{
		if (StoreKitManager.productPurchaseAwaitingConfirmationEvent != null)
		{
			StoreKitManager.productPurchaseAwaitingConfirmationEvent(StoreKitTransaction.transactionFromJson(json));
		}
		if (StoreKitManager.autoConfirmTransactions)
		{
			StoreKitBinding.finishPendingTransactions();
		}
	}

	// Token: 0x06000034 RID: 52 RVA: 0x00002690 File Offset: 0x00000890
	public void productPurchased(string json)
	{
		if (StoreKitManager.purchaseSuccessfulEvent != null)
		{
			StoreKitManager.purchaseSuccessfulEvent(StoreKitTransaction.transactionFromJson(json));
		}
	}

	// Token: 0x06000035 RID: 53 RVA: 0x000026AC File Offset: 0x000008AC
	public void productPurchaseFailed(string error)
	{
		if (StoreKitManager.purchaseFailedEvent != null)
		{
			StoreKitManager.purchaseFailedEvent(error);
		}
	}

	// Token: 0x06000036 RID: 54 RVA: 0x000026C4 File Offset: 0x000008C4
	public void productPurchaseCancelled(string error)
	{
		if (StoreKitManager.purchaseCancelledEvent != null)
		{
			StoreKitManager.purchaseCancelledEvent(error);
		}
	}

	// Token: 0x06000037 RID: 55 RVA: 0x000026DC File Offset: 0x000008DC
	public void productsReceived(string json)
	{
		if (StoreKitManager.productListReceivedEvent != null)
		{
			StoreKitManager.productListReceivedEvent(StoreKitProduct.productsFromJson(json));
		}
	}

	// Token: 0x06000038 RID: 56 RVA: 0x000026F8 File Offset: 0x000008F8
	public void productsRequestDidFail(string error)
	{
		if (StoreKitManager.productListRequestFailedEvent != null)
		{
			StoreKitManager.productListRequestFailedEvent(error);
		}
	}

	// Token: 0x06000039 RID: 57 RVA: 0x00002710 File Offset: 0x00000910
	public void restoreCompletedTransactionsFailed(string error)
	{
		if (StoreKitManager.restoreTransactionsFailedEvent != null)
		{
			StoreKitManager.restoreTransactionsFailedEvent(error);
		}
	}

	// Token: 0x0600003A RID: 58 RVA: 0x00002728 File Offset: 0x00000928
	public void restoreCompletedTransactionsFinished(string empty)
	{
		if (StoreKitManager.restoreTransactionsFinishedEvent != null)
		{
			StoreKitManager.restoreTransactionsFinishedEvent();
		}
	}

	// Token: 0x0600003B RID: 59 RVA: 0x00002740 File Offset: 0x00000940
	public void paymentQueueUpdatedDownloads(string json)
	{
		if (StoreKitManager.paymentQueueUpdatedDownloadsEvent != null)
		{
			StoreKitManager.paymentQueueUpdatedDownloadsEvent(StoreKitDownload.downloadsFromJson(json));
		}
	}

	// Token: 0x04000011 RID: 17
	public static bool autoConfirmTransactions = true;
}
