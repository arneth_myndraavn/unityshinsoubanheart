﻿using System;
using UnityEngine;

// Token: 0x020001AE RID: 430
public class Singleton<T> : UserBaseClass where T : UserBaseClass
{
	// Token: 0x170001AE RID: 430
	// (get) Token: 0x06000C65 RID: 3173 RVA: 0x00032D94 File Offset: 0x00030F94
	public static T Instance
	{
		get
		{
			if (Singleton<T>.instance == null)
			{
				Singleton<T>.instance = (T)((object)UnityEngine.Object.FindObjectOfType(typeof(T)));
				if (Singleton<T>.instance == null)
				{
					Debug.LogError("An instance of " + typeof(T) + " is needed in the scene, but there is none.");
				}
			}
			return Singleton<T>.instance;
		}
	}

	// Token: 0x170001AF RID: 431
	// (get) Token: 0x06000C66 RID: 3174 RVA: 0x00032E08 File Offset: 0x00031008
	public static bool IsReady
	{
		get
		{
			if (Singleton<T>.instance == null)
			{
				Singleton<T>.instance = (T)((object)UnityEngine.Object.FindObjectOfType(typeof(T)));
			}
			return Singleton<T>.instance != null;
		}
	}

	// Token: 0x04000968 RID: 2408
	protected static T instance;
}
