﻿using System;

// Token: 0x020000C0 RID: 192
public struct LB_INIT
{
	// Token: 0x060005D1 RID: 1489 RVA: 0x000173A4 File Offset: 0x000155A4
	public LB_INIT(string caption, DEBUG_ID id, DEBUG_ID parentid)
	{
		this.szCaption = caption;
		this.nId = (int)id;
		this.nParentId = (int)parentid;
	}

	// Token: 0x04000482 RID: 1154
	public string szCaption;

	// Token: 0x04000483 RID: 1155
	public int nId;

	// Token: 0x04000484 RID: 1156
	public int nParentId;
}
