﻿using System;
using System.Collections;
using System.Collections.Generic;
using Qoo.File;
using Qoo.Table;
using UnityEngine;

// Token: 0x02000156 RID: 342
public class ManTexture : MonoBehaviour
{
	// Token: 0x060009A1 RID: 2465 RVA: 0x0002A914 File Offset: 0x00028B14
	public UnityTexture Add(string name)
	{
		name = ManTexture.NormalizeName(name);
		if (this.array.ContainsKey(name))
		{
			this.array[name].IncCount();
			return this.array[name];
		}
		Texture2D texture_ = new Texture2D(0, 0, TextureFormat.ARGB32, false, false);
		UnityFile file = Singleton<UnityFileLoader>.Instance.LoadImageFile(name);
		SizeF size = DirScaleTable.GetSize(Nmb.GetFileInfo(name).DirName);
		this.array.Add(name, new UnityTexture(name, texture_, file, size));
		return this.array[name];
	}

	// Token: 0x060009A2 RID: 2466 RVA: 0x0002A9A4 File Offset: 0x00028BA4
	public UnityTexture Add(string name, byte[] data)
	{
		name = ManTexture.NormalizeName(name);
		if (this.array.ContainsKey(name))
		{
			this.array[name].IncCount();
			return this.array[name];
		}
		Texture2D texture_ = new Texture2D(0, 0, TextureFormat.ARGB32, false, false);
		this.array.Add(name, new UnityTexture(name, texture_, data, null));
		return this.array[name];
	}

	// Token: 0x060009A3 RID: 2467 RVA: 0x0002AA14 File Offset: 0x00028C14
	public UnityTexture Add(string name, Texture2D tex)
	{
		name = ManTexture.NormalizeName(name);
		if (this.array.ContainsKey(name))
		{
			this.array[name].IncCount();
			return this.array[name];
		}
		this.array.Add(name, new UnityTexture(name, tex));
		return this.array[name];
	}

	// Token: 0x060009A4 RID: 2468 RVA: 0x0002AA78 File Offset: 0x00028C78
	public bool Remove(UnityTexture tex, bool IsForce)
	{
		string name = tex.m_Name;
		if (this.array.ContainsKey(tex.m_Name))
		{
			if (IsForce)
			{
				this.m_RemoveList.Add(tex);
				this.array.Remove(name);
				return true;
			}
			this.array[name].DecCount();
			if (this.array[name].IsRelease())
			{
				this.m_RemoveList.Add(tex);
				this.array.Remove(name);
				return true;
			}
		}
		return false;
	}

	// Token: 0x060009A5 RID: 2469 RVA: 0x0002AB0C File Offset: 0x00028D0C
	public bool Remove(string name, bool IsForce)
	{
		name = ManTexture.NormalizeName(name);
		if (this.array.ContainsKey(name))
		{
			if (IsForce)
			{
				this.m_RemoveList.Add(this.array[name]);
				this.array.Remove(name);
				return true;
			}
			this.array[name].DecCount();
			if (this.array[name].IsRelease())
			{
				this.m_RemoveList.Add(this.array[name]);
				this.array.Remove(name);
				return true;
			}
		}
		return false;
	}

	// Token: 0x060009A6 RID: 2470 RVA: 0x0002ABB4 File Offset: 0x00028DB4
	private void Awake()
	{
		base.StartCoroutine("EndFrame");
	}

	// Token: 0x060009A7 RID: 2471 RVA: 0x0002ABC4 File Offset: 0x00028DC4
	private void Update()
	{
		foreach (UnityTexture unityTexture in this.array.Values)
		{
			unityTexture.Update();
		}
	}

	// Token: 0x060009A8 RID: 2472 RVA: 0x0002AC30 File Offset: 0x00028E30
	private IEnumerator EndFrame()
	{
		for (;;)
		{
			yield return new WaitForEndOfFrame();
			if (this.m_RemoveList == null)
			{
				break;
			}
			bool bDelete = this.m_RemoveList.Count > 0;
			foreach (UnityTexture item in this.m_RemoveList)
			{
				UnityEngine.Object.Destroy(item.m_Texture);
				item.m_Texture = null;
			}
			this.m_RemoveList.Clear();
			if (bDelete)
			{
				yield return Resources.UnloadUnusedAssets();
				GC.Collect();
			}
		}
		yield break;
	}

	// Token: 0x060009A9 RID: 2473 RVA: 0x0002AC4C File Offset: 0x00028E4C
	public bool IsFind(string name)
	{
		name = ManTexture.NormalizeName(name);
		return this.array.ContainsKey(name);
	}

	// Token: 0x060009AA RID: 2474 RVA: 0x0002AC64 File Offset: 0x00028E64
	internal bool IsUse(string name)
	{
		name = ManTexture.NormalizeName(name);
		return this.array.ContainsKey(name) && this.array[name].IsInit;
	}

	// Token: 0x060009AB RID: 2475 RVA: 0x0002ACA0 File Offset: 0x00028EA0
	public static string NormalizeName(string name)
	{
		name = name.ToLower();
		int num = name.IndexOf('.');
		if (num != -1)
		{
			name = name.Substring(0, num);
		}
		return name;
	}

	// Token: 0x060009AC RID: 2476 RVA: 0x0002ACD0 File Offset: 0x00028ED0
	internal void Ready(string name, bool IsDec = true)
	{
		UnityTexture unityTexture = this.Add(name);
		if (IsDec)
		{
			unityTexture.DecCount();
		}
		else
		{
			unityTexture.IncCount();
		}
	}

	// Token: 0x040007E1 RID: 2017
	private Dictionary<string, UnityTexture> array = new Dictionary<string, UnityTexture>();

	// Token: 0x040007E2 RID: 2018
	private List<UnityTexture> m_RemoveList = new List<UnityTexture>();
}
