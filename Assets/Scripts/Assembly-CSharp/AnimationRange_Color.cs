﻿using System;
using UnityEngine;

// Token: 0x02000196 RID: 406
public class AnimationRange_Color
{
	// Token: 0x06000BA3 RID: 2979 RVA: 0x00031398 File Offset: 0x0002F598
	public AnimationRange_Color(int repeat, float speed, float delay, Color begin, Color end)
	{
		this.animationRange = new AnimationRange(repeat, speed, delay, MoveType.OneshotLiner);
		this.offsetBegin = begin;
		this.offsetEnd = end;
		this.offset = this.offsetBegin;
	}

	// Token: 0x17000191 RID: 401
	// (get) Token: 0x06000BA4 RID: 2980 RVA: 0x000313CC File Offset: 0x0002F5CC
	public Color Offset
	{
		get
		{
			return this.offset;
		}
	}

	// Token: 0x17000192 RID: 402
	// (get) Token: 0x06000BA5 RID: 2981 RVA: 0x000313D4 File Offset: 0x0002F5D4
	public bool Active
	{
		get
		{
			return this.animationRange.Active;
		}
	}

	// Token: 0x06000BA6 RID: 2982 RVA: 0x000313E4 File Offset: 0x0002F5E4
	public void Update()
	{
		this.animationRange.Update();
		if (this.animationRange.Active)
		{
			float rate = this.animationRange.Rate;
			this.offset = this.offsetBegin * (1f - rate) + this.offsetEnd * rate;
		}
		else
		{
			this.offset = this.offsetEnd;
		}
	}

	// Token: 0x06000BA7 RID: 2983 RVA: 0x00031454 File Offset: 0x0002F654
	public void Restart()
	{
		this.animationRange.Restart();
	}

	// Token: 0x0400091D RID: 2333
	private AnimationRange animationRange;

	// Token: 0x0400091E RID: 2334
	private Color offsetBegin;

	// Token: 0x0400091F RID: 2335
	private Color offsetEnd;

	// Token: 0x04000920 RID: 2336
	private Color offset;
}
