﻿using System;
using System.Collections.Generic;
using Game;
using PaymentGameApi;
using Qoo;
using Qoo.Game;
using UnityEngine;

// Token: 0x0200012B RID: 299
public class PaymentWindow : SubTaskBaseWindow
{
	// Token: 0x060007FD RID: 2045 RVA: 0x00022E14 File Offset: 0x00021014
	protected sealed override void OnAwake()
	{
		Qoo.Debug.Print("m_chara = " + this.GetChara());
		UIValue.Payment_Exit = PaymentExitType.INVALID;
		this.callType = UIValue.Payment_Call;
		this.state = PaymentWindow.STATE.INIT;
		this.m_chara = this.GetChara();
		this.ProductDB = new DatabaseProductID();
		this.buttonStatus = new PaymentWindow.CheckButtonStatus(this.ProductDB);
		this.buttonStatus.Check(this.GetCharaId());
	}

	// Token: 0x060007FE RID: 2046 RVA: 0x00022E8C File Offset: 0x0002108C
	protected sealed override string[] newSceneTextureNameArray()
	{
		string str = this.GetFilenameId(this.m_chara).ToString("00");
		return new string[]
		{
			"screen/title/kyara_" + str,
			"screen/title/buy01",
			"screen/title/buy02",
			"screen/title/buy03",
			"screen/title/buy04",
			"screen/title/buy06",
			"screen/title/buy07",
			"screen/title/buy08",
			"screen/title/buy10",
			"screen/title/buy11",
			"screen/title/buy12",
			"screen/common/back"
		};
	}

	// Token: 0x060007FF RID: 2047 RVA: 0x00022F28 File Offset: 0x00021128
	protected sealed override BaseWindow.UIComponent[] newComponentArray()
	{
		string str = this.GetFilenameId(this.m_chara).ToString("00");
		return new BaseWindow.UIComponent[]
		{
			new BaseWindow.UIImage("BackGround", 0, 0, this.wndz, "screen/title/kyara_" + str, true, true),
			new BaseWindow.UIImage("Plate1", 351, 160, this.wndz + 1, (this.GetCharaId() != CHAR_ID.MOB) ? "screen/title/buy04" : "screen/title/buy12", false, true),
			new BaseWindow.UIButton("ButtonFull_ALL", 314, 320, this.wndz + 1, this.GetButtonFPath(PaymentWindow.CheckButtonStatus.ButtonType.FULLPACKAGE_ROUTE_VOICE), true, false, 1, 2, 0),
			new BaseWindow.UIButton("ButtonFull_VOICE", 314, 320, this.wndz + 1, this.GetButtonFPath(PaymentWindow.CheckButtonStatus.ButtonType.FULLPACKAGE_VOICE), true, false, 1, 2, 0),
			new BaseWindow.UIImage("ButtonFull_OFF", 314, 320, this.wndz + 1, this.GetButtonFPath(PaymentWindow.CheckButtonStatus.ButtonType.PURCHASED), false, false),
			new BaseWindow.UIText("ButtonFull_PRICE", this.btn_fp_x, this.btn_fp_y, this.wndz + 2, " ", 22, UnityTextSprite.PositionType.BottomRight, UnityTextSprite.PositionType.Center, byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue),
			new BaseWindow.UIButton("ButtonFullRoute", 633, 320, this.wndz + 1, this.GetButtonFPath(PaymentWindow.CheckButtonStatus.ButtonType.FULLPACKAGE_ROUTE), true, false, 1, 2, 0),
			new BaseWindow.UIImage("ButtonFullRoute_OFF", 633, 320, this.wndz + 1, this.GetButtonFPath(PaymentWindow.CheckButtonStatus.ButtonType.PURCHASED), false, false),
			new BaseWindow.UIText("ButtonFullRoute_PRICE", this.btn_frp_x, this.btn_frp_y, this.wndz + 2, " ", 22, UnityTextSprite.PositionType.BottomRight, UnityTextSprite.PositionType.Center, byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue),
			new BaseWindow.UIButton("ButtonChar_ALL", 314, 398, this.wndz + 1, this.GetButtonFPath(PaymentWindow.CheckButtonStatus.ButtonType.CHAR_ROUTE_VOICE), true, false, 1, 2, 0),
			new BaseWindow.UIButton("ButtonChar_VOICE", 314, 398, this.wndz + 1, this.GetButtonFPath(PaymentWindow.CheckButtonStatus.ButtonType.CHAR_VOICE), true, false, 1, 2, 0),
			new BaseWindow.UIImage("ButtonChar_OFF", 314, 398, this.wndz + 1, this.GetButtonFPath(PaymentWindow.CheckButtonStatus.ButtonType.PURCHASED), false, false),
			new BaseWindow.UIText("ButtonChar_PRICE", this.btn_cp_x, this.btn_cp_y, this.wndz + 2, " ", 22, UnityTextSprite.PositionType.BottomRight, UnityTextSprite.PositionType.Center, byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue),
			new BaseWindow.UIButton("ButtonCharRoute", 633, 398, this.wndz + 1, this.GetButtonFPath(PaymentWindow.CheckButtonStatus.ButtonType.CHAR_ROUTE), true, false, 1, 2, 0),
			new BaseWindow.UIImage("ButoonCharRoute_OFF", 633, 398, this.wndz + 1, this.GetButtonFPath(PaymentWindow.CheckButtonStatus.ButtonType.PURCHASED), false, false),
			new BaseWindow.UIText("ButtonCharRoute_PRICE", this.btn_crp_x, this.btn_crp_y, this.wndz + 2, " ", 22, UnityTextSprite.PositionType.BottomRight, UnityTextSprite.PositionType.Center, byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue),
			new BaseWindow.UIButton("Cancel", 486, 479, this.wndz + 1, "screen/title/buy07", true, true, 1, 2, 0),
			new BaseWindow.UIButton("Back", 887, 6, this.wndz + 1, "screen/common/back", true, this.callType == PaymentCallType.TITLE, 1, 2, 0)
		};
	}

	// Token: 0x06000800 RID: 2048 RVA: 0x000232B0 File Offset: 0x000214B0
	protected sealed override void OnBaseWindowOnButton(string obj)
	{
		switch (obj)
		{
		case "ButtonFull_ALL":
		case "ButtonFull_VOICE":
		case "ButtonFull_OFF":
		case "ButtonFullRoute":
		case "ButtonFullRoute_OFF":
		case "ButtonChar_ALL":
		case "ButtonChar_VOICE":
		case "ButtonChar_OFF":
		case "ButtonCharRoute":
		case "ButoonCharRoute_OFF":
			base.PlaySE_Ok();
			this.OnPayment(this.ButtonNameToPaymentType(obj));
			break;
		case "Cancel":
			base.PlaySE_Cancel();
			this.OnCancel();
			break;
		case "Back":
			base.PlaySE_Cancel();
			this.OnBack();
			break;
		}
	}

	// Token: 0x06000801 RID: 2049 RVA: 0x000233D0 File Offset: 0x000215D0
	private int GetChara()
	{
		return UIValue.GalleryCharactor;
	}

	// Token: 0x06000802 RID: 2050 RVA: 0x000233D8 File Offset: 0x000215D8
	private CHAR_ID GetCharaId()
	{
		return (CHAR_ID)this.m_chara;
	}

	// Token: 0x06000803 RID: 2051 RVA: 0x000233E0 File Offset: 0x000215E0
	private int GetFilenameId(int i)
	{
		return i + 1;
	}

	// Token: 0x06000804 RID: 2052 RVA: 0x000233E8 File Offset: 0x000215E8
	private string GetButtonFPath(PaymentWindow.CheckButtonStatus.ButtonType buttonType)
	{
		switch (buttonType)
		{
		case PaymentWindow.CheckButtonStatus.ButtonType.PURCHASED:
			return "screen/title/buy08";
		case PaymentWindow.CheckButtonStatus.ButtonType.FULLPACKAGE_ROUTE_VOICE:
			return "screen/title/buy01";
		case PaymentWindow.CheckButtonStatus.ButtonType.FULLPACKAGE_ROUTE:
			return "screen/title/buy03";
		case PaymentWindow.CheckButtonStatus.ButtonType.FULLPACKAGE_VOICE:
			return "screen/title/buy11";
		case PaymentWindow.CheckButtonStatus.ButtonType.CHAR_ROUTE_VOICE:
			return "screen/title/buy02";
		case PaymentWindow.CheckButtonStatus.ButtonType.CHAR_ROUTE:
			return "screen/title/buy06";
		case PaymentWindow.CheckButtonStatus.ButtonType.CHAR_VOICE:
			return "screen/title/buy10";
		default:
			return string.Empty;
		}
	}

	// Token: 0x06000805 RID: 2053 RVA: 0x00023450 File Offset: 0x00021650
	private void RenewalButtonStatus()
	{
		base.GameObjectShow("ButtonFull_ALL", false);
		base.GameObjectShow("ButtonFull_VOICE", false);
		base.GameObjectShow("ButtonFull_OFF", false);
		base.GameObjectShow("ButtonFullRoute", false);
		base.GameObjectShow("ButtonFullRoute_OFF", false);
		base.GameObjectShow("ButtonChar_ALL", false);
		base.GameObjectShow("ButtonChar_VOICE", false);
		base.GameObjectShow("ButtonChar_OFF", false);
		base.GameObjectShow("ButtonCharRoute", false);
		base.GameObjectShow("ButoonCharRoute_OFF", false);
		string str_ = " ";
		UnityTextSprite textSprite = base.GetTextSprite("ButtonFull_PRICE");
		textSprite.ClearText();
		textSprite.SetTextColor(new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue));
		switch (this.buttonStatus.ButtonFull)
		{
		case PaymentWindow.CheckButtonStatus.ButtonType.PURCHASED:
			base.GameObjectShow("ButtonFull_OFF", true);
			str_ = " ";
			break;
		case PaymentWindow.CheckButtonStatus.ButtonType.FULLPACKAGE_ROUTE_VOICE:
			base.GameObjectShow("ButtonFull_ALL", true);
			str_ = AMAppStore.GetPrice(this.ProductDB.GetAddonName(CHAR_ID.NIGHTMARE, DatabaseProductID.CheckType.ROUTE_VOICE));
			break;
		case PaymentWindow.CheckButtonStatus.ButtonType.FULLPACKAGE_VOICE:
			base.GameObjectShow("ButtonFull_VOICE", true);
			str_ = AMAppStore.GetPrice(this.ProductDB.GetAddonName(CHAR_ID.NIGHTMARE, DatabaseProductID.CheckType.VOICE));
			break;
		}
		Qoo.Debug.Print(this.ProductDB.GetAddonName(CHAR_ID.NIGHTMARE, DatabaseProductID.CheckType.ROUTE_VOICE));
		textSprite.AddText(str_, 22);
		textSprite.SetPosition(this.btn_fp_x, this.btn_fp_y, UnityTextSprite.PositionType.BottomRight, UnityTextSprite.PositionType.Center);
		textSprite.Update(960, 544, (float)(-(float)(this.wndz + 2)));
		textSprite = base.GetTextSprite("ButtonFullRoute_PRICE");
		textSprite.ClearText();
		textSprite.SetTextColor(new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue));
		switch (this.buttonStatus.ButtonFullRoute)
		{
		case PaymentWindow.CheckButtonStatus.ButtonType.PURCHASED:
			base.GameObjectShow("ButtonFullRoute_OFF", true);
			str_ = " ";
			break;
		case PaymentWindow.CheckButtonStatus.ButtonType.FULLPACKAGE_ROUTE:
			base.GameObjectShow("ButtonFullRoute", true);
			str_ = AMAppStore.GetPrice(this.ProductDB.GetAddonName(CHAR_ID.NIGHTMARE, DatabaseProductID.CheckType.ROUTE));
			break;
		}
		Qoo.Debug.Print(this.ProductDB.GetAddonName(CHAR_ID.NIGHTMARE, DatabaseProductID.CheckType.ROUTE));
		textSprite.AddText(str_, 22);
		textSprite.SetPosition(this.btn_frp_x, this.btn_frp_y, UnityTextSprite.PositionType.BottomRight, UnityTextSprite.PositionType.Center);
		textSprite.Update(960, 544, (float)(-(float)(this.wndz + 2)));
		if (this.GetCharaId() == CHAR_ID.MOB)
		{
			return;
		}
		textSprite = base.GetTextSprite("ButtonChar_PRICE");
		textSprite.ClearText();
		textSprite.SetTextColor(new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue));
		PaymentWindow.CheckButtonStatus.ButtonType buttonType = this.buttonStatus.ButtonChar;
		switch (buttonType)
		{
		case PaymentWindow.CheckButtonStatus.ButtonType.CHAR_ROUTE_VOICE:
			base.GameObjectShow("ButtonChar_ALL", true);
			str_ = AMAppStore.GetPrice(this.ProductDB.GetAddonName(this.GetCharaId(), DatabaseProductID.CheckType.ROUTE_VOICE));
			break;
		default:
			if (buttonType == PaymentWindow.CheckButtonStatus.ButtonType.PURCHASED)
			{
				base.GameObjectShow("ButtonChar_OFF", true);
				str_ = " ";
			}
			break;
		case PaymentWindow.CheckButtonStatus.ButtonType.CHAR_VOICE:
			base.GameObjectShow("ButtonChar_VOICE", true);
			str_ = AMAppStore.GetPrice(this.ProductDB.GetAddonName(this.GetCharaId(), DatabaseProductID.CheckType.VOICE));
			break;
		}
		textSprite.AddText(str_, 22);
		textSprite.SetPosition(this.btn_cp_x, this.btn_cp_y, UnityTextSprite.PositionType.BottomRight, UnityTextSprite.PositionType.Center);
		textSprite.Update(960, 544, (float)(-(float)(this.wndz + 2)));
		textSprite = base.GetTextSprite("ButtonCharRoute_PRICE");
		textSprite.ClearText();
		textSprite.SetTextColor(new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue));
		buttonType = this.buttonStatus.ButtonCharRoute;
		if (buttonType != PaymentWindow.CheckButtonStatus.ButtonType.PURCHASED)
		{
			if (buttonType == PaymentWindow.CheckButtonStatus.ButtonType.CHAR_ROUTE)
			{
				base.GameObjectShow("ButtonCharRoute", true);
				str_ = AMAppStore.GetPrice(this.ProductDB.GetAddonName(this.GetCharaId(), DatabaseProductID.CheckType.ROUTE));
			}
		}
		else
		{
			base.GameObjectShow("ButoonCharRoute_OFF", true);
			str_ = " ";
		}
		textSprite.AddText(str_, 22);
		textSprite.SetPosition(this.btn_crp_x, this.btn_crp_y, UnityTextSprite.PositionType.BottomRight, UnityTextSprite.PositionType.Center);
		textSprite.Update(960, 544, (float)(-(float)(this.wndz + 2)));
	}

	// Token: 0x06000806 RID: 2054 RVA: 0x00023894 File Offset: 0x00021A94
	protected override void OnBaseWindowUpdate()
	{
		switch (this.state)
		{
		case PaymentWindow.STATE.INIT:
			base.AddScene(UIValue.SCENE_PAYMENTLIST);
			UIValue.Payment_Exit = PaymentExitType.INVALID;
			this.state = PaymentWindow.STATE.LISTNEW;
			break;
		case PaymentWindow.STATE.LISTNEW:
			switch (UIValue.Payment_Exit)
			{
			case PaymentExitType.SUCCESS:
				this.buttonStatus.Check(this.GetCharaId());
				UIValue.Payment_Exit = PaymentExitType.INVALID;
				this.RenewalButtonStatus();
				this.UpdateSysdata();
				this.state = PaymentWindow.STATE.WAIT;
				break;
			case PaymentExitType.FAILD:
				this.buttonStatus.Check(this.GetCharaId());
				UIValue.Payment_Exit = PaymentExitType.INVALID;
				this.RenewalButtonStatus();
				this.state = PaymentWindow.STATE.FAILD;
				break;
			}
			break;
		case PaymentWindow.STATE.EXEC:
			switch (UIValue.Payment_Exit)
			{
			case PaymentExitType.CANCEL:
				UIValue.Payment_Exit = PaymentExitType.INVALID;
				this.state = PaymentWindow.STATE.WAIT;
				break;
			case PaymentExitType.SUCCESS:
				UIValue.Payment_Exit = PaymentExitType.INVALID;
				this.state = PaymentWindow.STATE.SUCCESS;
				break;
			case PaymentExitType.FAILD:
				UIValue.Payment_Exit = PaymentExitType.INVALID;
				this.state = PaymentWindow.STATE.FAILD;
				break;
			}
			break;
		case PaymentWindow.STATE.SUCCESS:
			if (this.callType == PaymentCallType.TITLE)
			{
				SceneManager.ChangeScene(UIValue.SCENE_TITLE);
			}
			else
			{
				base.IsTaskEnd = true;
			}
			this.state = PaymentWindow.STATE.EXIT;
			break;
		case PaymentWindow.STATE.FAILD:
			if (this.callType == PaymentCallType.TITLE)
			{
				SceneManager.ChangeScene(UIValue.SCENE_TITLE);
			}
			else
			{
				base.IsTaskEnd = true;
			}
			this.state = PaymentWindow.STATE.EXIT;
			break;
		case PaymentWindow.STATE.EXIT:
			this.state = PaymentWindow.STATE.AFTER_EXIT;
			break;
		}
	}

	// Token: 0x06000807 RID: 2055 RVA: 0x00023A48 File Offset: 0x00021C48
	private void OnPayment(int index)
	{
		PaymentWindow.STATE state = this.state;
		if (state != PaymentWindow.STATE.WAIT)
		{
			return;
		}
		string productId = this.GetProductId(index);
		if (productId.Length != 0)
		{
			this.state = PaymentWindow.STATE.EXEC;
			UIValue.Payment_ProductId = productId;
			base.AddScene(UIValue.SCENE_PAYMENTDIALOG);
		}
	}

	// Token: 0x06000808 RID: 2056 RVA: 0x00023A9C File Offset: 0x00021C9C
	private int ButtonNameToPaymentType(string obj)
	{
		switch (obj)
		{
		case "ButtonFull_ALL":
			return 0;
		case "ButtonFull_VOICE":
			return 0;
		case "ButtonFull_OFF":
			return -1;
		case "ButtonFullRoute":
			return 1;
		case "ButtonFullRoute_OFF":
			return -1;
		case "ButtonChar_ALL":
			return 2;
		case "ButtonChar_VOICE":
			return 2;
		case "ButtonChar_OFF":
			return -1;
		case "ButtonCharRoute":
			return 3;
		case "ButoonCharRoute_OFF":
			return -1;
		}
		return -1;
	}

	// Token: 0x06000809 RID: 2057 RVA: 0x00023B9C File Offset: 0x00021D9C
	private string GetProductId(int index)
	{
		switch (index)
		{
		case 0:
			switch (this.buttonStatus.ButtonFull)
			{
			case PaymentWindow.CheckButtonStatus.ButtonType.FULLPACKAGE_ROUTE_VOICE:
				return this.ProductDB.GetAddonName(CHAR_ID.NOTHING, DatabaseProductID.CheckType.ROUTE_VOICE);
			case PaymentWindow.CheckButtonStatus.ButtonType.FULLPACKAGE_VOICE:
				return this.ProductDB.GetAddonName(CHAR_ID.NOTHING, DatabaseProductID.CheckType.VOICE);
			}
			break;
		case 1:
		{
			PaymentWindow.CheckButtonStatus.ButtonType buttonType = this.buttonStatus.ButtonFullRoute;
			if (buttonType == PaymentWindow.CheckButtonStatus.ButtonType.FULLPACKAGE_ROUTE)
			{
				return this.ProductDB.GetAddonName(CHAR_ID.NOTHING, DatabaseProductID.CheckType.ROUTE);
			}
			break;
		}
		case 2:
			switch (this.buttonStatus.ButtonChar)
			{
			case PaymentWindow.CheckButtonStatus.ButtonType.CHAR_ROUTE_VOICE:
				return this.ProductDB.GetAddonName(this.GetCharaId(), DatabaseProductID.CheckType.ROUTE_VOICE);
			case PaymentWindow.CheckButtonStatus.ButtonType.CHAR_VOICE:
				return this.ProductDB.GetAddonName(this.GetCharaId(), DatabaseProductID.CheckType.VOICE);
			}
			break;
		case 3:
		{
			PaymentWindow.CheckButtonStatus.ButtonType buttonType = this.buttonStatus.ButtonCharRoute;
			if (buttonType == PaymentWindow.CheckButtonStatus.ButtonType.CHAR_ROUTE)
			{
				return this.ProductDB.GetAddonName(this.GetCharaId(), DatabaseProductID.CheckType.ROUTE);
			}
			break;
		}
		}
		return string.Empty;
	}

	// Token: 0x0600080A RID: 2058 RVA: 0x00023CBC File Offset: 0x00021EBC
	private void OnCancel()
	{
		Qoo.Debug.Print("state = " + this.state);
		PaymentWindow.STATE state = this.state;
		if (state != PaymentWindow.STATE.WAIT)
		{
			return;
		}
		if (this.callType == PaymentCallType.TITLE)
		{
			SceneManager.ChangeScene(UIValue.SCENE_TITLE);
		}
		else
		{
			base.IsTaskEnd = true;
		}
	}

	// Token: 0x0600080B RID: 2059 RVA: 0x00023D20 File Offset: 0x00021F20
	private void OnBack()
	{
		Qoo.Debug.Print("state = " + this.state);
		PaymentWindow.STATE state = this.state;
		if (state != PaymentWindow.STATE.WAIT)
		{
			return;
		}
		SceneManager.BackScene();
	}

	// Token: 0x0600080C RID: 2060 RVA: 0x00023D68 File Offset: 0x00021F68
	private void UpdateSysdata()
	{
		bool flag;
		bool flag2;
		for (CHAR_ID char_ID = CHAR_ID.BLOOD; char_ID < CHAR_ID.NIGHTMARE; char_ID++)
		{
			flag = false;
			flag2 = false;
			if (AMAppStore.CheckPurchased(this.ProductDB.GetAddonName(char_ID, DatabaseProductID.CheckType.ROUTE_VOICE)))
			{
				flag = true;
				flag2 = true;
			}
			if (AMAppStore.CheckPurchased(this.ProductDB.GetAddonName(char_ID, DatabaseProductID.CheckType.ROUTE)))
			{
				flag = true;
			}
			if (AMAppStore.CheckPurchased(this.ProductDB.GetAddonName(char_ID, DatabaseProductID.CheckType.VOICE)))
			{
				flag2 = true;
			}
			Qoo.Debug.Print(string.Concat(new object[]
			{
				"セーブデータ：Chara ",
				char_ID,
				"ルート ",
				flag,
				"ボイス ",
				flag2
			}));
			SysData.SetPay(char_ID, flag, flag2);
		}
		flag = false;
		flag2 = false;
		if (AMAppStore.CheckPurchased(this.ProductDB.GetAddonName(CHAR_ID.NIGHTMARE, DatabaseProductID.CheckType.ROUTE_VOICE)))
		{
			flag = true;
			flag2 = true;
		}
		if (AMAppStore.CheckPurchased(this.ProductDB.GetAddonName(CHAR_ID.NIGHTMARE, DatabaseProductID.CheckType.ROUTE)))
		{
			flag = true;
		}
		if (AMAppStore.CheckPurchased(this.ProductDB.GetAddonName(CHAR_ID.NIGHTMARE, DatabaseProductID.CheckType.VOICE)))
		{
			flag2 = true;
		}
		Qoo.Debug.Print(string.Concat(new object[]
		{
			"セーブデータ：フルパック ルート ",
			flag,
			"ボイス ",
			flag2
		}));
		SysData.SetPayFull(flag, flag2);
		SaveLoadManager.SaveSystem();
	}

	// Token: 0x040006FC RID: 1788
	private PaymentWindow.STATE state;

	// Token: 0x040006FD RID: 1789
	private int m_chara;

	// Token: 0x040006FE RID: 1790
	private PaymentWindow.CheckButtonStatus buttonStatus;

	// Token: 0x040006FF RID: 1791
	private PaymentCallType callType;

	// Token: 0x04000700 RID: 1792
	private readonly int wndz = 5;

	// Token: 0x04000701 RID: 1793
	private DatabaseProductID ProductDB;

	// Token: 0x04000702 RID: 1794
	private int btn_fp_x = 575;

	// Token: 0x04000703 RID: 1795
	private int btn_fp_y = 349;

	// Token: 0x04000704 RID: 1796
	private int btn_frp_x = 894;

	// Token: 0x04000705 RID: 1797
	private int btn_frp_y = 349;

	// Token: 0x04000706 RID: 1798
	private int btn_cp_x = 575;

	// Token: 0x04000707 RID: 1799
	private int btn_cp_y = 427;

	// Token: 0x04000708 RID: 1800
	private int btn_crp_x = 894;

	// Token: 0x04000709 RID: 1801
	private int btn_crp_y = 427;

	// Token: 0x0200012C RID: 300
	private class PurchasedStatus
	{
		// Token: 0x0600080D RID: 2061 RVA: 0x00023EB8 File Offset: 0x000220B8
		public PurchasedStatus(CHAR_ID charId_, DatabaseProductID db_)
		{
			this.charId = charId_;
			this.db = db_;
			this.routeVoice = false;
			this.route = false;
			this.voice = false;
		}

		// Token: 0x170000DF RID: 223
		// (get) Token: 0x0600080E RID: 2062 RVA: 0x00023EE4 File Offset: 0x000220E4
		public bool RouteVoice
		{
			get
			{
				return this.routeVoice;
			}
		}

		// Token: 0x170000E0 RID: 224
		// (get) Token: 0x0600080F RID: 2063 RVA: 0x00023EEC File Offset: 0x000220EC
		public bool Route
		{
			get
			{
				return this.route;
			}
		}

		// Token: 0x170000E1 RID: 225
		// (get) Token: 0x06000810 RID: 2064 RVA: 0x00023EF4 File Offset: 0x000220F4
		public bool Voice
		{
			get
			{
				return this.voice;
			}
		}

		// Token: 0x170000E2 RID: 226
		// (get) Token: 0x06000811 RID: 2065 RVA: 0x00023EFC File Offset: 0x000220FC
		public bool RouteMix
		{
			get
			{
				return this.routeVoice | this.route;
			}
		}

		// Token: 0x170000E3 RID: 227
		// (get) Token: 0x06000812 RID: 2066 RVA: 0x00023F0C File Offset: 0x0002210C
		public bool VoiceMix
		{
			get
			{
				return this.routeVoice | this.voice;
			}
		}

		// Token: 0x06000813 RID: 2067 RVA: 0x00023F1C File Offset: 0x0002211C
		public void Check()
		{
			Qoo.Debug.Print("check Id =" + this.charId);
			this.routeVoice = this.Check(this.db.GetAddonName(this.charId, DatabaseProductID.CheckType.ROUTE_VOICE));
			this.route = this.Check(this.db.GetAddonName(this.charId, DatabaseProductID.CheckType.ROUTE));
			this.voice = this.Check(this.db.GetAddonName(this.charId, DatabaseProductID.CheckType.VOICE));
			Qoo.Debug.Print(string.Concat(new object[]
			{
				"RV  = ",
				this.routeVoice,
				" R = ",
				this.route,
				" V = ",
				this.voice
			}));
		}

		// Token: 0x06000814 RID: 2068 RVA: 0x00023FF0 File Offset: 0x000221F0
		public void Check(Dictionary<CHAR_ID, PaymentWindow.PurchasedStatus> dic)
		{
			bool flag = true;
			bool flag2 = true;
			bool flag3 = true;
			foreach (KeyValuePair<CHAR_ID, PaymentWindow.PurchasedStatus> keyValuePair in dic)
			{
				if (!keyValuePair.Value.RouteVoice)
				{
					flag = false;
				}
				if (!keyValuePair.Value.Route)
				{
					flag2 = false;
				}
				if (!keyValuePair.Value.Voice)
				{
					flag3 = false;
				}
			}
			this.routeVoice = flag;
			this.route = flag2;
			this.voice = flag3;
		}

		// Token: 0x06000815 RID: 2069 RVA: 0x000240A0 File Offset: 0x000222A0
		public bool Check(string productId)
		{
			return AMAppStore.CheckPurchased(productId);
		}

		// Token: 0x0400070C RID: 1804
		private CHAR_ID charId;

		// Token: 0x0400070D RID: 1805
		private DatabaseProductID db;

		// Token: 0x0400070E RID: 1806
		private bool routeVoice;

		// Token: 0x0400070F RID: 1807
		private bool route;

		// Token: 0x04000710 RID: 1808
		private bool voice;
	}

	// Token: 0x0200012D RID: 301
	private class CheckButtonStatus
	{
		// Token: 0x06000816 RID: 2070 RVA: 0x000240A8 File Offset: 0x000222A8
		public CheckButtonStatus(DatabaseProductID db)
		{
			this.char2PurchasedStatus = new Dictionary<CHAR_ID, PaymentWindow.PurchasedStatus>();
			CHAR_ID[] array = new CHAR_ID[]
			{
				CHAR_ID.BLOOD,
				CHAR_ID.ELLIOT,
				CHAR_ID.DEEDUM,
				CHAR_ID.VIVALDI,
				CHAR_ID.PETER,
				CHAR_ID.ACE,
				CHAR_ID.GOWLAND,
				CHAR_ID.BORIS,
				CHAR_ID.JULIUS
			};
			foreach (CHAR_ID char_ID in array)
			{
				this.char2PurchasedStatus.Add(char_ID, new PaymentWindow.PurchasedStatus(char_ID, db));
			}
			this.fullPurchasedStatus = new PaymentWindow.PurchasedStatus(CHAR_ID.NOTHING, db);
			this.totalPurchasedStatus = new PaymentWindow.PurchasedStatus(CHAR_ID.NOTHING, db);
		}

		// Token: 0x170000E4 RID: 228
		// (get) Token: 0x06000817 RID: 2071 RVA: 0x00024138 File Offset: 0x00022338
		public PaymentWindow.CheckButtonStatus.ButtonType ButtonFull
		{
			get
			{
				return this.buttonFull;
			}
		}

		// Token: 0x170000E5 RID: 229
		// (get) Token: 0x06000818 RID: 2072 RVA: 0x00024140 File Offset: 0x00022340
		public PaymentWindow.CheckButtonStatus.ButtonType ButtonFullRoute
		{
			get
			{
				return this.buttonFullRoute;
			}
		}

		// Token: 0x170000E6 RID: 230
		// (get) Token: 0x06000819 RID: 2073 RVA: 0x00024148 File Offset: 0x00022348
		public PaymentWindow.CheckButtonStatus.ButtonType ButtonChar
		{
			get
			{
				return this.buttonChar;
			}
		}

		// Token: 0x170000E7 RID: 231
		// (get) Token: 0x0600081A RID: 2074 RVA: 0x00024150 File Offset: 0x00022350
		public PaymentWindow.CheckButtonStatus.ButtonType ButtonCharRoute
		{
			get
			{
				return this.buttonCharRoute;
			}
		}

		// Token: 0x170000E8 RID: 232
		// (get) Token: 0x0600081B RID: 2075 RVA: 0x00024158 File Offset: 0x00022358
		private PaymentWindow.CheckButtonStatus.ButtonType CheckButtonFull
		{
			get
			{
				if (this.fullPurchasedStatus.VoiceMix)
				{
					return PaymentWindow.CheckButtonStatus.ButtonType.PURCHASED;
				}
				if (this.fullPurchasedStatus.RouteMix && this.totalPurchasedStatus.VoiceMix)
				{
					return PaymentWindow.CheckButtonStatus.ButtonType.PURCHASED;
				}
				if (this.fullPurchasedStatus.RouteMix)
				{
					return PaymentWindow.CheckButtonStatus.ButtonType.FULLPACKAGE_VOICE;
				}
				return PaymentWindow.CheckButtonStatus.ButtonType.FULLPACKAGE_ROUTE_VOICE;
			}
		}

		// Token: 0x170000E9 RID: 233
		// (get) Token: 0x0600081C RID: 2076 RVA: 0x000241AC File Offset: 0x000223AC
		private PaymentWindow.CheckButtonStatus.ButtonType CheckButtonFullRoute
		{
			get
			{
				if (this.fullPurchasedStatus.RouteMix)
				{
					return PaymentWindow.CheckButtonStatus.ButtonType.PURCHASED;
				}
				return PaymentWindow.CheckButtonStatus.ButtonType.FULLPACKAGE_ROUTE;
			}
		}

		// Token: 0x170000EA RID: 234
		// (get) Token: 0x0600081D RID: 2077 RVA: 0x000241C4 File Offset: 0x000223C4
		private PaymentWindow.CheckButtonStatus.ButtonType CheckButtonChar
		{
			get
			{
				PaymentWindow.CheckButtonStatus.ButtonType buttonType = this.ButtonFull;
				PaymentWindow.PurchasedStatus purchasedStatus = this.char2PurchasedStatus[this.charId];
				if (buttonType == PaymentWindow.CheckButtonStatus.ButtonType.PURCHASED || purchasedStatus.VoiceMix)
				{
					return PaymentWindow.CheckButtonStatus.ButtonType.PURCHASED;
				}
				if (buttonType == PaymentWindow.CheckButtonStatus.ButtonType.FULLPACKAGE_VOICE || purchasedStatus.RouteMix)
				{
					return PaymentWindow.CheckButtonStatus.ButtonType.CHAR_VOICE;
				}
				return PaymentWindow.CheckButtonStatus.ButtonType.CHAR_ROUTE_VOICE;
			}
		}

		// Token: 0x170000EB RID: 235
		// (get) Token: 0x0600081E RID: 2078 RVA: 0x00024214 File Offset: 0x00022414
		private PaymentWindow.CheckButtonStatus.ButtonType CheckButtonCharRoute
		{
			get
			{
				PaymentWindow.CheckButtonStatus.ButtonType buttonType = this.ButtonFullRoute;
				PaymentWindow.PurchasedStatus purchasedStatus = this.char2PurchasedStatus[this.charId];
				if (buttonType == PaymentWindow.CheckButtonStatus.ButtonType.PURCHASED || purchasedStatus.RouteMix)
				{
					return PaymentWindow.CheckButtonStatus.ButtonType.PURCHASED;
				}
				return PaymentWindow.CheckButtonStatus.ButtonType.CHAR_ROUTE;
			}
		}

		// Token: 0x0600081F RID: 2079 RVA: 0x00024250 File Offset: 0x00022450
		public void Check(CHAR_ID charId_)
		{
			foreach (KeyValuePair<CHAR_ID, PaymentWindow.PurchasedStatus> keyValuePair in this.char2PurchasedStatus)
			{
				keyValuePair.Value.Check();
			}
			this.fullPurchasedStatus.Check();
			this.totalPurchasedStatus.Check(this.char2PurchasedStatus);
			if (charId_ == CHAR_ID.MOB)
			{
				this.charId = CHAR_ID.BLOOD;
			}
			else
			{
				this.charId = charId_;
			}
			this.buttonFull = this.CheckButtonFull;
			this.buttonFullRoute = this.CheckButtonFullRoute;
			this.buttonChar = this.CheckButtonChar;
			this.buttonCharRoute = this.CheckButtonCharRoute;
		}

		// Token: 0x04000711 RID: 1809
		private Dictionary<CHAR_ID, PaymentWindow.PurchasedStatus> char2PurchasedStatus;

		// Token: 0x04000712 RID: 1810
		private PaymentWindow.PurchasedStatus fullPurchasedStatus;

		// Token: 0x04000713 RID: 1811
		private PaymentWindow.PurchasedStatus totalPurchasedStatus;

		// Token: 0x04000714 RID: 1812
		private CHAR_ID charId;

		// Token: 0x04000715 RID: 1813
		private PaymentWindow.CheckButtonStatus.ButtonType buttonFull;

		// Token: 0x04000716 RID: 1814
		private PaymentWindow.CheckButtonStatus.ButtonType buttonFullRoute;

		// Token: 0x04000717 RID: 1815
		private PaymentWindow.CheckButtonStatus.ButtonType buttonChar;

		// Token: 0x04000718 RID: 1816
		private PaymentWindow.CheckButtonStatus.ButtonType buttonCharRoute;

		// Token: 0x0200012E RID: 302
		public enum ButtonType
		{
			// Token: 0x0400071A RID: 1818
			INVALID,
			// Token: 0x0400071B RID: 1819
			PURCHASED,
			// Token: 0x0400071C RID: 1820
			FULLPACKAGE_ROUTE_VOICE,
			// Token: 0x0400071D RID: 1821
			FULLPACKAGE_ROUTE,
			// Token: 0x0400071E RID: 1822
			FULLPACKAGE_VOICE,
			// Token: 0x0400071F RID: 1823
			CHAR_ROUTE_VOICE,
			// Token: 0x04000720 RID: 1824
			CHAR_ROUTE,
			// Token: 0x04000721 RID: 1825
			CHAR_VOICE
		}
	}

	// Token: 0x0200012F RID: 303
	private enum STATE
	{
		// Token: 0x04000723 RID: 1827
		INIT,
		// Token: 0x04000724 RID: 1828
		LISTNEW,
		// Token: 0x04000725 RID: 1829
		WAIT,
		// Token: 0x04000726 RID: 1830
		EXEC,
		// Token: 0x04000727 RID: 1831
		SUCCESS,
		// Token: 0x04000728 RID: 1832
		FAILD,
		// Token: 0x04000729 RID: 1833
		EXIT,
		// Token: 0x0400072A RID: 1834
		AFTER_EXIT
	}
}
