﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

// Token: 0x02000150 RID: 336
public class UIFont : MonoBehaviour
{
	// Token: 0x1700011F RID: 287
	// (get) Token: 0x06000952 RID: 2386 RVA: 0x00028A90 File Offset: 0x00026C90
	// (set) Token: 0x06000953 RID: 2387 RVA: 0x00028A98 File Offset: 0x00026C98
	public BMFont Font
	{
		get
		{
			return this.mFont;
		}
		set
		{
			this.mFont = value;
		}
	}

	// Token: 0x17000120 RID: 288
	// (get) Token: 0x06000954 RID: 2388 RVA: 0x00028AA4 File Offset: 0x00026CA4
	public int Width
	{
		get
		{
			return (this.mFont == null) ? 1 : this.mFont.texWidth;
		}
	}

	// Token: 0x17000121 RID: 289
	// (get) Token: 0x06000955 RID: 2389 RVA: 0x00028AC4 File Offset: 0x00026CC4
	public int Height
	{
		get
		{
			return (this.mFont == null) ? 1 : this.mFont.texHeight;
		}
	}

	// Token: 0x17000122 RID: 290
	// (get) Token: 0x06000956 RID: 2390 RVA: 0x00028AE4 File Offset: 0x00026CE4
	// (set) Token: 0x06000957 RID: 2391 RVA: 0x00028AEC File Offset: 0x00026CEC
	public Material material
	{
		get
		{
			return this.mMat;
		}
		set
		{
			this.mMat = value;
		}
	}

	// Token: 0x17000123 RID: 291
	// (get) Token: 0x06000958 RID: 2392 RVA: 0x00028AF8 File Offset: 0x00026CF8
	// (set) Token: 0x06000959 RID: 2393 RVA: 0x00028B50 File Offset: 0x00026D50
	public Rect uvRect
	{
		get
		{
			if (this.texture != null && this.sprite != null)
			{
				this.mUVRect.width = (float)this.sprite.w;
				this.mUVRect.height = (float)this.sprite.h;
			}
			return this.mUVRect;
		}
		set
		{
			this.mUVRect = value;
		}
	}

	// Token: 0x17000124 RID: 292
	// (get) Token: 0x0600095A RID: 2394 RVA: 0x00028B5C File Offset: 0x00026D5C
	// (set) Token: 0x0600095B RID: 2395 RVA: 0x00028B64 File Offset: 0x00026D64
	public int horizontalSpacing
	{
		get
		{
			return this.mSpacingX;
		}
		set
		{
			this.mSpacingX = value;
		}
	}

	// Token: 0x17000125 RID: 293
	// (get) Token: 0x0600095C RID: 2396 RVA: 0x00028B70 File Offset: 0x00026D70
	// (set) Token: 0x0600095D RID: 2397 RVA: 0x00028B78 File Offset: 0x00026D78
	public int verticalSpacing
	{
		get
		{
			return this.mSpacingY;
		}
		set
		{
			this.mSpacingY = value;
		}
	}

	// Token: 0x17000126 RID: 294
	// (get) Token: 0x0600095E RID: 2398 RVA: 0x00028B84 File Offset: 0x00026D84
	public int size
	{
		get
		{
			return this.mFont.charSize;
		}
	}

	// Token: 0x0600095F RID: 2399 RVA: 0x00028B94 File Offset: 0x00026D94
	private void Trim()
	{
	}

	// Token: 0x06000960 RID: 2400 RVA: 0x00028B98 File Offset: 0x00026D98
	public Vector2 CalculatePrintedSize(string text)
	{
		Vector2 zero = Vector2.zero;
		if (this.mFont != null && this.mFont.isValid && !string.IsNullOrEmpty(text))
		{
			int length = text.Length;
			int num = 0;
			int num2 = 0;
			int num3 = 0;
			int num4 = 0;
			int num5 = this.mFont.charSize + this.mSpacingY;
			for (int i = 0; i < length; i++)
			{
				char c = text[i];
				if (c == '\n')
				{
					if (num2 > num)
					{
						num = num2;
					}
					num2 = 0;
					num3 += num5;
					num4 = 0;
				}
				else if (c < ' ')
				{
					num4 = 0;
				}
				else
				{
					BMGlyph glyph = this.mFont.GetGlyph((int)c);
					if (glyph != null)
					{
						num2 += this.mSpacingX + ((num4 == 0) ? glyph.advance : (glyph.advance + glyph.GetKerning(num4, true)));
						num4 = (int)c;
					}
				}
			}
			float num6 = (this.mFont.charSize <= 0) ? 1f : (1f / (float)this.mFont.charSize);
			zero.x = num6 * (float)((num2 <= num) ? num : num2);
			zero.y = num6 * (float)(num3 + num5);
		}
		return zero;
	}

	// Token: 0x06000961 RID: 2401 RVA: 0x00028CEC File Offset: 0x00026EEC
	private static void EndLine(ref StringBuilder s)
	{
		int num = s.Length - 1;
		if (num > 0 && s[num] == ' ')
		{
			s[num] = '\n';
		}
		else
		{
			s.Append('\n');
		}
	}

	// Token: 0x06000962 RID: 2402 RVA: 0x00028D34 File Offset: 0x00026F34
	public string GetEndOfLineThatFits(string text, float maxWidth)
	{
		int num = Mathf.RoundToInt(maxWidth * (float)this.size);
		if (num < 1)
		{
			return text;
		}
		int length = text.Length;
		int num2 = num;
		BMGlyph bmglyph = null;
		int num3 = length;
		while (num3 > 0 && num2 > 0)
		{
			char c = text[--num3];
			BMGlyph glyph = this.mFont.GetGlyph((int)c);
			int num4 = this.mSpacingX;
			if (glyph != null)
			{
				num4 += glyph.advance + ((bmglyph != null) ? bmglyph.GetKerning((int)c, true) : 0);
				bmglyph = glyph;
				num2 -= num4;
			}
			else
			{
				bmglyph = null;
			}
		}
		if (num2 < 0)
		{
			num3++;
		}
		return text.Substring(num3, length - num3);
	}

	// Token: 0x06000963 RID: 2403 RVA: 0x00028DF8 File Offset: 0x00026FF8
	public string WrapText(string text, float maxWidth, int maxLineCount)
	{
		int num = Mathf.RoundToInt(maxWidth * (float)this.size);
		if (num < 1)
		{
			return text;
		}
		StringBuilder stringBuilder = new StringBuilder();
		int length = text.Length;
		int num2 = num;
		int num3 = 0;
		int num4 = 0;
		int i = 0;
		bool flag = true;
		bool flag2 = maxLineCount != 1;
		int num5 = 1;
		while (i < length)
		{
			char c = text[i];
			if (c == '\n')
			{
				if (!flag2 || num5 == maxLineCount)
				{
					break;
				}
				num2 = num;
				if (num4 < i)
				{
					stringBuilder.Append(text.Substring(num4, i - num4 + 1));
				}
				else
				{
					stringBuilder.Append(c);
				}
				flag = true;
				num5++;
				num4 = i + 1;
				num3 = 0;
			}
			else
			{
				if (c == ' ' && num3 != 32 && num4 < i)
				{
					stringBuilder.Append(text.Substring(num4, i - num4 + 1));
					flag = false;
					num4 = i + 1;
					num3 = (int)c;
				}
				BMGlyph glyph = this.mFont.GetGlyph((int)c);
				int num6 = this.mSpacingX;
				if (glyph != null)
				{
					num6 += ((num3 == 0) ? glyph.advance : (glyph.advance + glyph.GetKerning(num3, true)));
					num2 -= num6;
					if (num2 < 0)
					{
						if (flag || !flag2 || num5 == maxLineCount)
						{
							stringBuilder.Append(text.Substring(num4, Mathf.Max(0, i - num4)));
							if (!flag2 || num5 == maxLineCount)
							{
								num4 = i;
								break;
							}
							UIFont.EndLine(ref stringBuilder);
							flag = true;
							num5++;
							if (c == ' ')
							{
								num4 = i + 1;
								num2 = num;
							}
							else
							{
								num4 = i;
								num2 = num - num6;
							}
							num3 = 0;
						}
						else
						{
							while (num4 < length && text[num4] == ' ')
							{
								num4++;
							}
							flag = true;
							num2 = num;
							i = num4 - 1;
							num3 = 0;
							if (!flag2 || num5 == maxLineCount)
							{
								break;
							}
							num5++;
							UIFont.EndLine(ref stringBuilder);
						}
					}
					else
					{
						num3 = (int)c;
					}
				}
			}
			i++;
		}
		if (num4 < i)
		{
			stringBuilder.Append(text.Substring(num4, i - num4));
		}
		return stringBuilder.ToString();
	}

	// Token: 0x06000964 RID: 2404 RVA: 0x00029064 File Offset: 0x00027264
	private void Align(List<Vector3> verts, int indexOffset, UIFont.Alignment alignment, int x, int lineWidth)
	{
		if (alignment != UIFont.Alignment.Left && this.mFont.charSize > 0)
		{
			float num = (alignment != UIFont.Alignment.Right) ? ((float)(lineWidth - x) * 0.5f) : ((float)(lineWidth - x));
			num = (float)Mathf.RoundToInt(num);
			if (num < 0f)
			{
				num = 0f;
			}
			num /= (float)this.mFont.charSize;
			for (int i = indexOffset; i < verts.Count; i++)
			{
				Vector3 value = verts[i];
				value.x += num;
				verts[i] = value;
			}
		}
	}

	// Token: 0x06000965 RID: 2405 RVA: 0x00029108 File Offset: 0x00027308
	public void Print(List<CharObject> text, List<Vector3> verts, List<Vector2> uvs, List<Color32> cols)
	{
		if (this.mFont != null && text != null)
		{
			if (!this.mFont.isValid)
			{
				Debug.LogError("Attempting to print using an invalid font!");
				return;
			}
			Vector3 zero = Vector3.zero;
			Vector3 zero2 = Vector3.zero;
			Vector2 zero3 = Vector2.zero;
			Vector2 zero4 = Vector2.zero;
			float num = this.uvRect.width / (float)this.mFont.texWidth;
			float num2 = this.mUVRect.height / (float)this.mFont.texHeight;
			int count = text.Count;
			for (int i = 0; i < count; i++)
			{
				char code = text[i].code;
				Color color = text[i].color;
				int x = text[i].X;
				int y = text[i].Y;
				float scale = text[i].scale;
				if (code != '\n')
				{
					if (code >= ' ')
					{
						BMGlyph glyph = this.mFont.GetGlyph((int)code);
						if (glyph != null)
						{
							if (code != ' ')
							{
								zero.x = (float)x + scale * (float)glyph.offsetX;
								zero.y = -((float)y + scale * (float)glyph.offsetY);
								zero2.x = zero.x + scale * (float)glyph.width;
								zero2.y = zero.y - scale * (float)glyph.height;
								zero3.x = this.mUVRect.xMin + num * (float)glyph.x;
								zero3.y = this.mUVRect.yMax - num2 * (float)glyph.y;
								zero4.x = zero3.x + num * (float)glyph.width;
								zero4.y = zero3.y - num2 * (float)glyph.height;
								if (glyph.channel == 0 || glyph.channel == 15)
								{
									for (int j = 0; j < 4; j++)
									{
										cols.Add(color);
									}
								}
								else
								{
									Color color2 = color;
									color2 *= 0.49f;
									switch (glyph.channel)
									{
									case 1:
										color2.b += 0.51f;
										break;
									case 2:
										color2.g += 0.51f;
										break;
									case 4:
										color2.r += 0.51f;
										break;
									case 8:
										color2.a += 0.51f;
										break;
									}
									for (int k = 0; k < 4; k++)
									{
										cols.Add(color2);
									}
								}
								verts.Add(new Vector3(zero2.x, zero.y));
								verts.Add(new Vector3(zero2.x, zero2.y));
								verts.Add(new Vector3(zero.x, zero2.y));
								verts.Add(new Vector3(zero.x, zero.y));
								uvs.Add(new Vector2(zero4.x, zero3.y));
								uvs.Add(new Vector2(zero4.x, zero4.y));
								uvs.Add(new Vector2(zero3.x, zero4.y));
								uvs.Add(new Vector2(zero3.x, zero3.y));
							}
						}
					}
				}
			}
		}
	}

	// Token: 0x06000966 RID: 2406 RVA: 0x000294F0 File Offset: 0x000276F0
	public void Print(string text, Color color, List<Vector3> verts, List<Vector2> uvs, List<Color> cols, UIFont.Alignment alignment, int lineWidth, bool premultiply)
	{
		if (this.mFont != null && text != null)
		{
			if (!this.mFont.isValid)
			{
				Debug.LogError("Attempting to print using an invalid font!");
				return;
			}
			this.mColors.Clear();
			this.mColors.Add(color);
			Vector2 vector = (this.mFont.charSize <= 0) ? Vector2.one : new Vector2(1f / (float)this.mFont.charSize, 1f / (float)this.mFont.charSize);
			int count = verts.Count;
			int num = 0;
			int num2 = 0;
			int num3 = 0;
			int num4 = 0;
			int num5 = this.mFont.charSize + this.mSpacingY;
			Vector3 zero = Vector3.zero;
			Vector3 zero2 = Vector3.zero;
			Vector2 zero3 = Vector2.zero;
			Vector2 zero4 = Vector2.zero;
			float num6 = this.uvRect.width / (float)this.mFont.texWidth;
			float num7 = this.mUVRect.height / (float)this.mFont.texHeight;
			int length = text.Length;
			for (int i = 0; i < length; i++)
			{
				char c = text[i];
				if (c == '\n')
				{
					if (num2 > num)
					{
						num = num2;
					}
					if (alignment != UIFont.Alignment.Left)
					{
						this.Align(verts, count, alignment, num2, lineWidth);
						count = verts.Count;
					}
					num2 = 0;
					num3 += num5;
					num4 = 0;
				}
				else if (c < ' ')
				{
					num4 = 0;
				}
				else
				{
					BMGlyph glyph = this.mFont.GetGlyph((int)c);
					if (glyph != null)
					{
						if (num4 != 0)
						{
							num2 += glyph.GetKerning(num4, true);
						}
						if (c == ' ')
						{
							num2 += this.mSpacingX + glyph.advance;
							num4 = (int)c;
						}
						else
						{
							zero.x = vector.x * (float)(num2 + glyph.offsetX);
							zero.y = -vector.y * (float)(num3 + glyph.offsetY);
							zero2.x = zero.x + vector.x * (float)glyph.width;
							zero2.y = zero.y - vector.y * (float)glyph.height;
							zero3.x = this.mUVRect.xMin + num6 * (float)glyph.x;
							zero3.y = this.mUVRect.yMax - num7 * (float)glyph.y;
							zero4.x = zero3.x + num6 * (float)glyph.width;
							zero4.y = zero3.y - num7 * (float)glyph.height;
							num2 += this.mSpacingX + glyph.advance;
							num4 = (int)c;
							if (glyph.channel == 0 || glyph.channel == 15)
							{
								for (int j = 0; j < 4; j++)
								{
									cols.Add(color);
								}
							}
							else
							{
								Color color2 = color;
								color2 *= 0.49f;
								switch (glyph.channel)
								{
								case 1:
									color2.b += 0.51f;
									break;
								case 2:
									color2.g += 0.51f;
									break;
								case 4:
									color2.r += 0.51f;
									break;
								case 8:
									color2.a += 0.51f;
									break;
								}
								for (int k = 0; k < 4; k++)
								{
									cols.Add(color2);
								}
							}
							verts.Add(new Vector3(zero2.x, zero.y));
							verts.Add(new Vector3(zero2.x, zero2.y));
							verts.Add(new Vector3(zero.x, zero2.y));
							verts.Add(new Vector3(zero.x, zero.y));
							uvs.Add(new Vector2(zero4.x, zero3.y));
							uvs.Add(new Vector2(zero4.x, zero4.y));
							uvs.Add(new Vector2(zero3.x, zero4.y));
							uvs.Add(new Vector2(zero3.x, zero3.y));
						}
					}
				}
			}
			if (alignment != UIFont.Alignment.Left && count < verts.Count)
			{
				this.Align(verts, count, alignment, num2, lineWidth);
				count = verts.Count;
			}
		}
	}

	// Token: 0x040007C2 RID: 1986
	private Material mMat;

	// Token: 0x040007C3 RID: 1987
	private Rect mUVRect = new Rect(0f, 0f, 1f, 1f);

	// Token: 0x040007C4 RID: 1988
	private BMFont mFont = new BMFont();

	// Token: 0x040007C5 RID: 1989
	private int mSpacingX;

	// Token: 0x040007C6 RID: 1990
	private int mSpacingY;

	// Token: 0x040007C7 RID: 1991
	private List<Color> mColors = new List<Color>();

	// Token: 0x040007C8 RID: 1992
	public UnityTexture texture;

	// Token: 0x040007C9 RID: 1993
	public UnitySprite sprite;

	// Token: 0x02000151 RID: 337
	public enum Alignment
	{
		// Token: 0x040007CB RID: 1995
		Left,
		// Token: 0x040007CC RID: 1996
		Center,
		// Token: 0x040007CD RID: 1997
		Right
	}
}
