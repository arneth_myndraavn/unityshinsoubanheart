﻿using Qoo.Game;
using System;
using System.Collections;
using UnityEngine;

// Token: 0x0200010C RID: 268
public class GalleryInputNameKeyboardWindow : BaseWindow
{
	// Token: 0x0600070E RID: 1806 RVA: 0x0001E298 File Offset: 0x0001C498
	protected sealed override void BeforeInit()
	{
		UnityTask.SetMainTask(this.Run());
	}

	// Token: 0x0600070F RID: 1807 RVA: 0x0001E2A8 File Offset: 0x0001C4A8
	public IEnumerator Run()
	{
		string initvalue = UIValue.GalleryInputNameDialog_Name;
        string initvalue2 = UIValue.GalleryInputNameDialog_LastName;
        yield return NameInputKeyboard.Open(initvalue, initvalue2, false);
		/*switch (Application.platform)
		{
		case RuntimePlatform.IPhonePlayer:
		case RuntimePlatform.Android:
			yield return NameInputKeyboard.Open(initvalue, false);
			goto IL_82;
		}*/
		//NameInputKeyboard.DebugInputText = initvalue;
		IL_82:
		UIValue.GalleryInputNameDialog_Name = NameInputKeyboard.InputText;
        UIValue.GalleryInputNameDialog_LastName = NameInputKeyboard.InputTextLastName;
        UIValue.GalleryInputNameDialog_Exit = GalleryInputNameDialogExitType.INPUT;
		base.DeleteLastAddScene();
		NameInputKeyboard.Close();
		yield break;
	}
}
