﻿using System;
using System.Collections.Generic;
using Qoo.Game;
using UnityEngine;

// Token: 0x02000121 RID: 289
public class OptionTextWindow : OptionBaseWindow
{
	private Dictionary<string,int> dict = null;

	// Token: 0x060007BF RID: 1983 RVA: 0x0002138C File Offset: 0x0001F58C
	private void SetTextWaitHandler(int value)
	{
		SysData.SetTextSpeed(value);
	}

	// Token: 0x060007C0 RID: 1984 RVA: 0x00021394 File Offset: 0x0001F594
	private void SetAutoWaitHandler(int value)
	{
		SysData.SetAutoPage(value);
	}

	// Token: 0x060007C1 RID: 1985 RVA: 0x0002139C File Offset: 0x0001F59C
	private void SetCursorPosition()
	{
		ImageObject imageObject = base.GetImageObject("SpeedCursor");
		ImageObject imageObject2 = base.GetImageObject("PageCursor");
		imageObject.UIObject.ScreenX = this.TextWaitCursorPositions[SysData.GetTextSpeed()];
		imageObject2.UIObject.ScreenX = this.AutoWaitCursorPositions[SysData.GetAutoPage()];
		imageObject.InitPosition();
		imageObject2.InitPosition();
	}

	// Token: 0x060007C2 RID: 1986 RVA: 0x000213FC File Offset: 0x0001F5FC
	protected override OptionBaseWindow.OptionType GetOptionType()
	{
		return OptionBaseWindow.OptionType.Text;
	}

	// Token: 0x060007C3 RID: 1987 RVA: 0x00021400 File Offset: 0x0001F600
	protected sealed override string[] newSceneTextureNameArray()
	{
		string[] collection = base.newSceneTextureNameArray();
		string[] collection2 = new string[]
		{
			"screen/sysmenu/sys_plate",
			"screen/sysmenu/sys_btn_10",
			"screen/sysmenu/sys_btn_11",
			"screen/sysmenu/sys_plate_speed",
			"screen/sysmenu/sys_plate_page",
			"screen/sysmenu/sys_check",
			"screen/common/cancel"
		};
		List<string> list = new List<string>();
		list.AddRange(collection);
		list.AddRange(collection2);
		return list.ToArray();
	}

	// Token: 0x060007C4 RID: 1988 RVA: 0x00021470 File Offset: 0x0001F670
	protected sealed override void BeforeInit()
	{
		Vector3 localPosition = base.transform.parent.localPosition;
		localPosition.z = -50f;
		base.transform.parent.localPosition = localPosition;
	}

	// Token: 0x060007C5 RID: 1989 RVA: 0x000214AC File Offset: 0x0001F6AC
	protected sealed override void AfterInit()
	{
		this.SetCursorPosition();
	}

	// Token: 0x060007C6 RID: 1990 RVA: 0x000214B4 File Offset: 0x0001F6B4
	protected override void OnGraphicLoadComplete()
	{
		base.OnGraphicLoadComplete();
	}

	// Token: 0x060007C7 RID: 1991 RVA: 0x000214BC File Offset: 0x0001F6BC
	protected sealed override BaseWindow.UIComponent[] newComponentArray()
	{
		BaseWindow.UIComponent[] collection = base.newComponentArray();
		BaseWindow.UIComponent[] collection2 = new BaseWindow.UIComponent[]
		{
			new BaseWindow.UICollision("Close", 0, 0, this.wndz, 960, 544),
			new BaseWindow.UICollision("TextWait0", 216, 178, this.wndz + 4, 88, 34),
			new BaseWindow.UICollision("TextWait1", 356, 178, this.wndz + 4, 88, 34),
			new BaseWindow.UICollision("TextWait2", 497, 178, this.wndz + 4, 88, 34),
			new BaseWindow.UICollision("TextWait3", 615, 178, this.wndz + 4, 132, 34),
			new BaseWindow.UICollision("AutoWait0", 223, 300, this.wndz + 4, 75, 34),
			new BaseWindow.UICollision("AutoWait1", 313, 300, this.wndz + 4, 75, 34),
			new BaseWindow.UICollision("AutoWait2", 403, 300, this.wndz + 4, 75, 34),
			new BaseWindow.UICollision("AutoWait3", 493, 300, this.wndz + 4, 75, 34),
			new BaseWindow.UICollision("AutoWait4", 583, 300, this.wndz + 4, 75, 34),
			new BaseWindow.UICollision("AutoWait5", 673, 300, this.wndz + 4, 75, 34),
			new BaseWindow.UIImage("BackGround", 153, 68, this.wndz + 1, "screen/sysmenu/sys_plate", true, true),
			new BaseWindow.UIButton("Clear", 208, 380, this.wndz + 2, "screen/sysmenu/sys_btn_10", true, true, 1, 2, 0),
			new BaseWindow.UIButton("Decide", 686, 380, this.wndz + 2, "screen/sysmenu/sys_btn_11", true, true, 1, 2, 0),
			new BaseWindow.UIButton("Cancel", 715, 73, this.wndz + 2, "screen/common/cancel", true, true, 1, 2, 0),
			new BaseWindow.UIImage("Speed", 225, 129, this.wndz + 2, "screen/sysmenu/sys_plate_speed", false, true),
			new BaseWindow.UIImage("Page", 225, 251, this.wndz + 2, "screen/sysmenu/sys_plate_page", false, true),
			new BaseWindow.UIImage("SpeedCursor", 216, 182, this.wndz + 3, "screen/sysmenu/sys_check", false, true),
			new BaseWindow.UIImage("PageCursor", 216, 304, this.wndz + 3, "screen/sysmenu/sys_check", false, true)
		};
		List<BaseWindow.UIComponent> list = new List<BaseWindow.UIComponent>();
		list.AddRange(collection);
		list.AddRange(collection2);
		return list.ToArray();
	}

	// Token: 0x060007C8 RID: 1992 RVA: 0x000217B0 File Offset: 0x0001F9B0
	protected sealed override void OnBaseWindowOnButton(string obj)
	{
		base.OnBaseWindowOnButton(obj);
		if (obj != null)
		{
			if (this.dict == null)
			{
				this.dict = new Dictionary<string, int>(10)
				{
					{
						"TextWait0",
						0
					},
					{
						"TextWait1",
						0
					},
					{
						"TextWait2",
						0
					},
					{
						"TextWait3",
						0
					},
					{
						"AutoWait0",
						1
					},
					{
						"AutoWait1",
						1
					},
					{
						"AutoWait2",
						1
					},
					{
						"AutoWait3",
						1
					},
					{
						"AutoWait4",
						1
					},
					{
						"AutoWait5",
						1
					}
				};
			}
			int num;
			if (this.dict.TryGetValue(obj, out num))
			{
				if (num != 0)
				{
					if (num == 1)
					{
						base.PlaySE_Select();
						this.SetAutoWaitHandler(this.AutoWaitToInt(obj));
					}
				}
				else
				{
					base.PlaySE_Select();
					this.SetTextWaitHandler(this.TextWaitToString(obj));
				}
			}
		}
		this.SetCursorPosition();
	}

	// Token: 0x060007C9 RID: 1993 RVA: 0x000218B8 File Offset: 0x0001FAB8
	private int TextWaitToString(string obj)
	{
		switch (obj)
		{
		case "TextWait0":
			return 0;
		case "TextWait1":
			return 1;
		case "TextWait2":
			return 2;
		case "TextWait3":
			return 3;
		}
		return -1;
	}

	// Token: 0x060007CA RID: 1994 RVA: 0x0002194C File Offset: 0x0001FB4C
	private int AutoWaitToInt(string obj)
	{
		switch (obj)
		{
		case "AutoWait0":
			return 0;
		case "AutoWait1":
			return 1;
		case "AutoWait2":
			return 2;
		case "AutoWait3":
			return 3;
		case "AutoWait4":
			return 4;
		case "AutoWait5":
			return 5;
		}
		return -1;
	}

	// Token: 0x040006CF RID: 1743
	private readonly int wndz;

	// Token: 0x040006D0 RID: 1744
	private int[] TextWaitCursorPositions = new int[]
	{
		216,
		356,
		497,
		615
	};

	// Token: 0x040006D1 RID: 1745
	private int[] AutoWaitCursorPositions = new int[]
	{
		216,
		306,
		396,
		486,
		576,
		666
	};
}
