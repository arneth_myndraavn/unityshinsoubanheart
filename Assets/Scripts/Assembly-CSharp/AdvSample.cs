﻿using System;
using System.Collections;
using Qoo.Game;

// Token: 0x02000002 RID: 2
public class AdvSample : UserBaseClass
{
	// Token: 0x06000002 RID: 2 RVA: 0x000020F4 File Offset: 0x000002F4
	private void Start()
	{
		UnityTask.SetMainTask(this.Run());
	}

	// Token: 0x06000003 RID: 3 RVA: 0x00002104 File Offset: 0x00000304
	private void Update()
	{
	}

	// Token: 0x06000004 RID: 4 RVA: 0x00002108 File Offset: 0x00000308
	public IEnumerator Run()
	{
		EventWnd wnd = base.gameObject.AddComponent<EventWnd>();
		UnityApp.Graph.Enable(false);
		SysData.SetReadAll();
		SysData.SetSkip(2);
		yield return wnd.Exec("prologue.ks", string.Empty, true);
		UnityTask.SetMainTask(this.Run());
		yield break;
	}
}
