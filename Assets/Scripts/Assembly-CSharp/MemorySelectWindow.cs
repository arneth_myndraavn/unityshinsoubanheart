﻿using System;

// Token: 0x0200011C RID: 284
public class MemorySelectWindow : GallerySelectWindow
{
	// Token: 0x06000789 RID: 1929 RVA: 0x000204FC File Offset: 0x0001E6FC
	protected sealed override void BeforeInitGallerySelect()
	{
		this.charaData = CSVManager.Instance.CsvSceneMemoryHolder.GetCharaIndex(base.Chara);
	}

	// Token: 0x0600078A RID: 1930 RVA: 0x00020524 File Offset: 0x0001E724
	protected sealed override int GetCollect()
	{
		return this.charaData.Collect;
	}

	// Token: 0x0600078B RID: 1931 RVA: 0x00020534 File Offset: 0x0001E734
	protected override string GetThumbnailName(int index)
	{
		return this.charaData.Thumbnail(index);
	}

	// Token: 0x0600078C RID: 1932 RVA: 0x00020544 File Offset: 0x0001E744
	protected override int GetThumbnailLength()
	{
		return this.charaData.Length;
	}

	// Token: 0x0600078D RID: 1933 RVA: 0x00020554 File Offset: 0x0001E754
	protected sealed override string GetOnArrowSceneName()
	{
		return UIValue.SCENE_MEMORYSELECT;
	}

	// Token: 0x0600078E RID: 1934 RVA: 0x0002055C File Offset: 0x0001E75C
	protected sealed override bool IsFont()
	{
		return false;
	}

	// Token: 0x0600078F RID: 1935 RVA: 0x00020560 File Offset: 0x0001E760
	protected sealed override string GetFontText(int index)
	{
		return string.Empty;
	}

	// Token: 0x06000790 RID: 1936 RVA: 0x00020568 File Offset: 0x0001E768
	protected sealed override void OnSelect(int index)
	{
		if (this.charaData.Read(index))
		{
			this.charaData.OnSelect(index);
			SceneManager.ChangeScene(UIValue.SCENE_ADVMODE);
		}
	}

	// Token: 0x06000791 RID: 1937 RVA: 0x00020594 File Offset: 0x0001E794
	protected sealed override bool IsSelectable(int index)
	{
		return this.charaData.Read(index);
	}

	// Token: 0x040006C7 RID: 1735
	private CSVSceneMemoryHolder.CharaData charaData;
}
