﻿using System;
using UnityEngine;

// Token: 0x020001AA RID: 426
public abstract class MonoBehaviourWrap : MonoBehaviour
{
	// Token: 0x06000C54 RID: 3156
	public abstract void Update();

	// Token: 0x06000C55 RID: 3157
	public abstract void Awake();

	// Token: 0x06000C56 RID: 3158
	public abstract void Start();

	// Token: 0x06000C57 RID: 3159
	public abstract void OnDestroy();

	// Token: 0x06000C58 RID: 3160
	public abstract void OnButton(string obj);
}
