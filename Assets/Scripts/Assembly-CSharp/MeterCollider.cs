﻿using System;
using System.Collections;
using UnityEngine;

// Token: 0x020000EA RID: 234
public class MeterCollider : MonoBehaviour
{
	// Token: 0x06000655 RID: 1621 RVA: 0x0001A5B8 File Offset: 0x000187B8
	public void Init(GameObject meterObject)
	{
		this.m_MeterObject = meterObject;
		this.m_MeterScript = this.m_MeterObject.GetComponent<Meter>();
		base.StartCoroutine(this.WaitLoadTextureCoroutine());
	}

	// Token: 0x06000656 RID: 1622 RVA: 0x0001A5E0 File Offset: 0x000187E0
	private IEnumerator WaitLoadTextureCoroutine()
	{
		while (this.m_MeterObject.GetComponent<Renderer>().material.mainTexture == null)
		{
			yield return 0;
		}
		base.gameObject.transform.parent = this.m_MeterObject.transform.parent;
		base.gameObject.transform.localPosition = this.m_MeterObject.GetComponent<ImageObject>().OriginalPosition;
		this.m_Collider = base.gameObject.GetComponent<BoxCollider>();
		if (this.m_Collider == null)
		{
			this.m_Collider = base.gameObject.AddComponent<BoxCollider>();
		}
		this.m_Collider.size = new Vector3((float)this.m_MeterObject.GetComponent<Renderer>().material.mainTexture.width, (float)this.m_MeterObject.GetComponent<Renderer>().material.mainTexture.height, 1f);
		this.m_Collider.center = new Vector3(0f, 0f, -3f);
		this.m_ImageObject = this.m_MeterObject.GetComponent<ImageObject>();
		yield break;
	}

	// Token: 0x06000657 RID: 1623 RVA: 0x0001A5FC File Offset: 0x000187FC
	private void Update()
	{
		if (this.m_Collider != null)
		{
			this.m_Collider.size = new Vector3(this.m_Collider.size.x, this.m_MeterObject.transform.localScale.y, 1f);
		}
	}

	// Token: 0x06000658 RID: 1624 RVA: 0x0001A65C File Offset: 0x0001885C
	private void OnMouseDown()
	{
		this.m_LastX = Input.mousePosition.x;
	}

	// Token: 0x06000659 RID: 1625 RVA: 0x0001A67C File Offset: 0x0001887C
	private void OnMouseDrag()
	{
		float num = this.m_LastX - Input.mousePosition.x;
		this.m_LastX = Input.mousePosition.x;
		Camera camera = SubPartCamera.GetCamera();
		Vector3 position = camera.WorldToScreenPoint(this.m_ImageObject.OnViewScale);
		position.x -= num;
		position = camera.ScreenToWorldPoint(position);
		this.m_MeterScript.UpdateMeter(position.x / this.m_ImageObject.OriginalScale.x);
	}

	// Token: 0x0600065A RID: 1626 RVA: 0x0001A704 File Offset: 0x00018904
	private void OnMouseUp()
	{
		this.m_MeterScript.OnEditEnd();
	}

	// Token: 0x040005C8 RID: 1480
	private BoxCollider m_Collider;

	// Token: 0x040005C9 RID: 1481
	private GameObject m_MeterObject;

	// Token: 0x040005CA RID: 1482
	private ImageObject m_ImageObject;

	// Token: 0x040005CB RID: 1483
	private Meter m_MeterScript;

	// Token: 0x040005CC RID: 1484
	private float m_LastX;
}
