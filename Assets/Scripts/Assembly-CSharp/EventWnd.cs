﻿using System;
using System.Collections;
using System.Collections.Generic;
using Qoo;
using Qoo.Application;
using Qoo.Def;
using Qoo.Game;
using Qoo.Graphics;
using Qoo.Input;
using Qoo.Ks;
using Qoo.SoundSystem;
using UnityEngine;

// Token: 0x020000A5 RID: 165
public class EventWnd : Singleton<EventWnd>
{
	// Token: 0x170000AE RID: 174
	// (get) Token: 0x060004C8 RID: 1224 RVA: 0x00011E9C File Offset: 0x0001009C
	public PLAY_MODE Mode
	{
		get
		{
			return this.player.Mode;
		}
	}

	// Token: 0x060004C9 RID: 1225 RVA: 0x00011EAC File Offset: 0x000100AC
	public void RedrawFace(bool isDraw)
	{
		this.player.Scene.RedrawFace(isDraw);
	}

	// Token: 0x170000AF RID: 175
	// (get) Token: 0x060004CA RID: 1226 RVA: 0x00011EC0 File Offset: 0x000100C0
	public EVENTBACKUPDATA SceneBackup
	{
		get
		{
			return this.player.Scene.BackupData;
		}
	}

	// Token: 0x170000B0 RID: 176
	// (get) Token: 0x060004CB RID: 1227 RVA: 0x00011ED4 File Offset: 0x000100D4
	public EventPlayer Player
	{
		get
		{
			return this.player;
		}
	}

	// Token: 0x170000B1 RID: 177
	// (get) Token: 0x060004CC RID: 1228 RVA: 0x00011EDC File Offset: 0x000100DC
	// (set) Token: 0x060004CD RID: 1229 RVA: 0x00011EE4 File Offset: 0x000100E4
	public bool IsWaitScreenChange { get; set; }

	// Token: 0x060004CE RID: 1230 RVA: 0x00011EF0 File Offset: 0x000100F0
	private void Awake()
	{
		this.IsWaitScreenChange = false;
	}

	// Token: 0x060004CF RID: 1231 RVA: 0x00011EFC File Offset: 0x000100FC
	private void Update()
	{
	}

	// Token: 0x060004D0 RID: 1232 RVA: 0x00011F00 File Offset: 0x00010100
	public void Init(PLAY_MODE mode)
	{
		this.player.Init(mode);
		KsInput.Clear();
		KsInput.ClearMenu();
		KsInput.ClearBacklog();
		this.IsWaitScreenChange = false;
	}

	// Token: 0x060004D1 RID: 1233 RVA: 0x00011F28 File Offset: 0x00010128
	public void Finish()
	{
		App.QooKsLog.Reset();
		this.player.Release();
		UnityEngine.Object.Destroy(base.GetComponent<MsgWnd>());
		UnityEngine.Object.Destroy(base.GetComponent<SelectWnd>());
		Sound.SeSlotStopAll();
		Sound.BgmStop(1000);
		Sound.VoiceStop();
	}

	// Token: 0x060004D2 RID: 1234 RVA: 0x00011F78 File Offset: 0x00010178
	public void Load(string nameKs, string nameLabel)
	{
		this.player.Load(nameKs, nameLabel);
	}

	// Token: 0x060004D3 RID: 1235 RVA: 0x00011F88 File Offset: 0x00010188
	public void Restore(GAME_SAVE_DATA data)
	{
		this.player.Restore(data);
	}

	// Token: 0x060004D4 RID: 1236 RVA: 0x00011F98 File Offset: 0x00010198
	public bool IsEnd()
	{
		return this.player.Status == PLAYSTAT.END;
	}

	// Token: 0x060004D5 RID: 1237 RVA: 0x00011FA8 File Offset: 0x000101A8
	public IEnumerator Exec(string nameKs, string nameLabel, bool IsNormalMode)
	{
		Graph.Fade_Out();
		yield return this.Ready();
		yield return App.QooKsData.LoadKs(nameKs);
		this.Init((!IsNormalMode) ? PLAY_MODE.MEMORY : PLAY_MODE.NORMAL);
		this.Load(nameKs, nameLabel);
		Graph.Fade_In();
		yield return 0;
		yield return this.RunPlayer();
		yield break;
	}

	// Token: 0x060004D6 RID: 1238 RVA: 0x00011FF0 File Offset: 0x000101F0
	public IEnumerator ExecLoad()
	{
		Graph.Fade_Out();
		yield return this.Ready();
		yield return this.ExecRestore();
		yield return this.RunPlayer();
		yield break;
	}

	// Token: 0x060004D7 RID: 1239 RVA: 0x0001200C File Offset: 0x0001020C
	private IEnumerator ExecRestore()
	{
		GAME_SAVE_DATA data = GameData.GetLoadData();
		data.Apply();
		foreach (string item in data.m_Log.m_KsNameArray)
		{
			yield return App.QooKsData.LoadKs(item);
		}
		yield return 0;
		foreach (string item2 in data.m_Log.m_KsNameArray)
		{
			yield return App.QooKsData.Add(item2);
		}
		List<string> array = new List<string>();
		foreach (string item3 in data.m_Log.m_KsNameArray)
		{
			TagReader reader = App.QooKsData.Get(item3);
			array.AddRange(reader.NextKsList);
		}
		foreach (string item4 in array)
		{
			yield return App.QooKsData.LoadKs(item4);
		}
		yield return 0;
		this.Init(PLAY_MODE.NORMAL);
		base.QooSound.BgmStop(1000);
		this.Restore(data);
		yield return 0;
		yield return 0;
		yield return 0;
		yield break;
	}

	// Token: 0x060004D8 RID: 1240 RVA: 0x00012028 File Offset: 0x00010228
	private IEnumerator Ready()
	{
		foreach (string item in KsDef.INIT_TEX_NAME_LIST)
		{
			Man2D.ReadyTexture(item, false);
		}
		yield return 0;
		yield return 0;
		base.gameObject.AddComponent<MsgWnd>();
		base.gameObject.AddComponent<SelectWnd>();
		App.QooBackLog.Init();
		yield return 0;
		yield break;
	}

	// Token: 0x060004D9 RID: 1241 RVA: 0x00012044 File Offset: 0x00010244
	private IEnumerator RunPlayer()
	{
		this.player.PlayStart();
		for (;;)
		{
			if (Qoo.Debug.IsAutoKsDebug)
			{
				KsInput.SetAuto();
			}
			if (!Singleton<UnityGraph>.Instance.enabled)
			{
				yield return Singleton<UnityGraph>.Instance.RestoreEnable(null);
			}
			if (GameData.IsLoadData())
			{
				Graph.Fade_Out();
				yield return 0;
				this.Finish();
				yield return 0;
				yield return Singleton<Man2D>.Instance.EraseFrameBuffer();
				yield return this.Ready();
				yield return this.ExecRestore();
				this.player.PlayStart();
			}
			if (GameData.IsMoveTitle)
			{
				break;
			}
			if (this.player.IsNextKs())
			{
				foreach (string item in this.player.GetNextKsArray())
				{
					yield return App.QooKsData.LoadKs(item);
				}
				this.player.ResetNextKs();
			}
			if (this.player.Exec() && this.player.IsPlayEnd())
			{
				break;
			}
			if (!this.IsWaitScreenChange && KsInput.IsBackLog)
			{
				yield return this.CallBacklog();
			}
			if (!this.IsWaitScreenChange && KsInput.IsMenu && SaveData.IsSave)
			{
				yield return this.ExecMenu();
			}
			if (GameData.IsMoveTitle)
			{
				break;
			}
			yield return base.QooApp.FrameUpdate();
		}
		this.Finish();
		foreach (string item2 in KsDef.INIT_TEX_NAME_LIST)
		{
			Singleton<Man2D>.Instance.ReleaseTexture(item2, true);
		}
		yield break;
	}

	// Token: 0x060004DA RID: 1242 RVA: 0x00012060 File Offset: 0x00010260
	private IEnumerator CallBacklog()
	{
		if (!App.QooBackLog.Empty)
		{
			bool IsSelect = Singleton<SelectWnd>.Instance.enabled;
			if (IsSelect)
			{
				Singleton<SelectWnd>.Instance.enabled = false;
			}
			CBackLogWnd wnd = base.gameObject.AddComponent<CBackLogWnd>();
			yield return base.QooApp.FrameUpdate();
			while (wnd != null && wnd.enabled)
			{
				yield return base.QooApp.FrameUpdate();
			}
			if (IsSelect)
			{
				Singleton<SelectWnd>.Instance.enabled = true;
			}
		}
		KsInput.ClearBacklog();
		yield break;
	}

	// Token: 0x060004DB RID: 1243 RVA: 0x0001207C File Offset: 0x0001027C
	public IEnumerator ExecMenu()
	{
		Sound.VoiceStop();
		bool IsMsg = Singleton<MsgWnd>.Instance.IsVisible;
		bool IsSelect = Singleton<SelectWnd>.Instance.enabled;
		if (IsMsg)
		{
			Singleton<MsgWnd>.Instance.Show(false);
		}
		if (IsSelect)
		{
			Singleton<SelectWnd>.Instance.enabled = false;
		}
		Singleton<UnityGraph>.Instance.RunEffect = true;
		yield return 0;
		yield return 0;
		Singleton<Man2D>.Instance.UpdateFrameBuffer();
		KsInput.Clear();
		yield return new AdvMenuTask().Open(Singleton<Man2D>.Instance.GetFrameBuffer(), this.player.Mode == PLAY_MODE.NORMAL);
		UnityApp.Input.Clear();
		UnityApp.Input.IsOnePushDelete = true;
		if (!GameData.IsMoveTitle)
		{
			if (IsMsg)
			{
				Singleton<MsgWnd>.Instance.Show(true);
			}
			SysData.Apply();
			base.QooGraph.Enable(false);
		}
		else if (Singleton<SelectWnd>.Instance.IsRun)
		{
			Singleton<SelectWnd>.Instance.End();
		}
		KsInput.ClearMenu();
		if (IsSelect)
		{
			Singleton<SelectWnd>.Instance.enabled = true;
		}
		yield return 0;
		yield break;
	}

	// Token: 0x0400037C RID: 892
	private EventPlayer player = new EventPlayer();
}
