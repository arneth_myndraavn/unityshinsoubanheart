﻿using System;
using System.Collections;
using Qoo;
using UnityEngine;

// Token: 0x02000119 RID: 281
public class LogoMovieWindow : BaseWindow
{
	// Token: 0x0600077F RID: 1919 RVA: 0x0002041C File Offset: 0x0001E61C
	protected sealed override void OnAwake()
	{
		this.state = LogoMovieWindow.STATE.INIT;
	}

	// Token: 0x06000780 RID: 1920 RVA: 0x00020428 File Offset: 0x0001E628
	protected override void OnBaseWindowUpdate()
	{
		switch (this.state)
		{
		case LogoMovieWindow.STATE.INIT:
			base.StartCoroutine(this.PlayMovie());
			this.state = LogoMovieWindow.STATE.PLAYING;
			break;
		case LogoMovieWindow.STATE.END:
			Qoo.Debug.Print("＊＊＊ムービー再生おわた＊＊＊");
			SceneManager.ChangeScene(UIValue.SCENE_TITLE);
			this.state = LogoMovieWindow.STATE.EXIT;
			break;
		}
	}

	// Token: 0x06000781 RID: 1921 RVA: 0x0002049C File Offset: 0x0001E69C
	private IEnumerator PlayMovie()
	{
		//TODO fix movie playing. either make platform specific versions OR make platform agnostic
		//yield return base.StartCoroutine(MovieManager.PlayMovie("mp4/normal/quinrose_logo_8.mp4", FullScreenMovieControlMode.CancelOnInput));
		yield return base.StartCoroutine(MovieManager.PlayMovie("mp4/normal/quinrose_logo_8.webm", this.gameObject));
		this.state = LogoMovieWindow.STATE.END;
		yield break;
	}

	// Token: 0x040006C1 RID: 1729
	private LogoMovieWindow.STATE state;

	// Token: 0x0200011A RID: 282
	private enum STATE
	{
		// Token: 0x040006C3 RID: 1731
		INIT,
		// Token: 0x040006C4 RID: 1732
		PLAYING,
		// Token: 0x040006C5 RID: 1733
		END,
		// Token: 0x040006C6 RID: 1734
		EXIT
	}
}
