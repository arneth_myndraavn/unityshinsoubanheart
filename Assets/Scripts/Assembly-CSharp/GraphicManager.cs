﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x0200016C RID: 364
public class GraphicManager : MonoBehaviour
{
	// Token: 0x17000155 RID: 341
	// (get) Token: 0x06000A72 RID: 2674 RVA: 0x0002E3B8 File Offset: 0x0002C5B8
	private static GraphicManager Instance
	{
		get
		{
			GameObject gameObject = GameObject.Find("__GraphicManager");
			if (gameObject == null)
			{
				gameObject = new GameObject("__GraphicManager");
				gameObject.AddComponent<GraphicManager>();
			}
			if (GraphicManager.instance == null)
			{
				GraphicManager.instance = gameObject.GetComponent<GraphicManager>();
			}
			return GraphicManager.instance;
		}
	}

	// Token: 0x06000A73 RID: 2675 RVA: 0x0002E410 File Offset: 0x0002C610
	public static bool CheckLoadImageComplete(string fpath)
	{
		string fileName = GraphicManager.getFileName(fpath);
		return Singleton<Man2D>.Instance.IsUseTextue(fileName);
	}

	// Token: 0x06000A74 RID: 2676 RVA: 0x0002E430 File Offset: 0x0002C630
	private static UnityTexture GetLoadedTexture(string fpath)
	{
		if (fpath == null || fpath == string.Empty)
		{
			return null;
		}
		string fileName = GraphicManager.getFileName(fpath);
		if (Singleton<Man2D>.Instance.IsUseTextue(fileName))
		{
			return Singleton<Man2D>.Instance.LoadTexture(fileName);
		}
		return null;
	}

	// Token: 0x06000A75 RID: 2677 RVA: 0x0002E47C File Offset: 0x0002C67C
	public static void LoadTexture(string fpath, Action<UnityTexture> handler)
	{
		UnityTexture loadedTexture = GraphicManager.GetLoadedTexture(fpath);
		if (loadedTexture != null && loadedTexture.m_Texture != null)
		{
			handler(loadedTexture);
		}
		else
		{
			GraphicManager.Instance.StartCoroutine(GraphicManager.WaitLoadingImage(fpath, handler));
		}
	}

	// Token: 0x06000A76 RID: 2678 RVA: 0x0002E4C8 File Offset: 0x0002C6C8
	private static IEnumerator WaitLoadingImage(string fpath, Action<UnityTexture> handler)
	{
		string fname = GraphicManager.getFileName(fpath);
		UnityTexture unityTexture = Singleton<Man2D>.Instance.LoadTexture(fname);
		while (!Singleton<Man2D>.Instance.IsUseTextue(fname))
		{
			yield return 0;
		}
		unityTexture.m_Texture.wrapMode = TextureWrapMode.Clamp;
		handler(unityTexture);
		yield break;
	}

	// Token: 0x06000A77 RID: 2679 RVA: 0x0002E4F8 File Offset: 0x0002C6F8
	public static void ReleaseTexture(UnityTexture unityTexture)
	{
		if (Singleton<Man2D>.IsReady)
		{
			Man2D man2D = Singleton<Man2D>.Instance;
			if (man2D != null)
			{
				man2D.ReleaseTexture(unityTexture, false);
			}
		}
	}

	// Token: 0x06000A78 RID: 2680 RVA: 0x0002E52C File Offset: 0x0002C72C
	private static string getFileName(string fpath)
	{
		string[] array = fpath.Split(new char[]
		{
			'/'
		});
		return array[array.Length - 1];
	}

	// Token: 0x04000875 RID: 2165
	private static GraphicManager instance;

	// Token: 0x0200016D RID: 365
	public class SceneGraphicHolder
	{
		// Token: 0x06000A79 RID: 2681 RVA: 0x0002E558 File Offset: 0x0002C758
		public SceneGraphicHolder(string[] path)
		{
			this.textureList = new List<GraphicManager.SceneGraphicHolder.UnityTextureHolder>();
			this.Create(path);
		}

		// Token: 0x06000A7A RID: 2682 RVA: 0x0002E574 File Offset: 0x0002C774
		public void Create(string[] path)
		{
			this.Release();
			foreach (string name_ in path)
			{
				this.textureList.Add(new GraphicManager.SceneGraphicHolder.UnityTextureHolder(name_));
			}
		}

		// Token: 0x06000A7B RID: 2683 RVA: 0x0002E5B4 File Offset: 0x0002C7B4
		public void Release()
		{
			foreach (GraphicManager.SceneGraphicHolder.UnityTextureHolder unityTextureHolder in this.textureList)
			{
				unityTextureHolder.Release();
			}
			this.textureList.Clear();
		}

		// Token: 0x06000A7C RID: 2684 RVA: 0x0002E624 File Offset: 0x0002C824
		public bool isDone()
		{
			GraphicManager.SceneGraphicHolder.UnityTextureHolder unityTextureHolder = this.textureList.Find((GraphicManager.SceneGraphicHolder.UnityTextureHolder obj) => !obj.isDone());
			return unityTextureHolder == null;
		}

		// Token: 0x06000A7D RID: 2685 RVA: 0x0002E660 File Offset: 0x0002C860
		public IEnumerator WaitLoadTexture()
		{
			while (!this.isDone())
			{
				yield return new WaitForSeconds(0.1f);
			}
			yield break;
		}

		// Token: 0x04000876 RID: 2166
		private List<GraphicManager.SceneGraphicHolder.UnityTextureHolder> textureList;

		// Token: 0x0200016E RID: 366
		public class UnityTextureHolder
		{
			// Token: 0x06000A7F RID: 2687 RVA: 0x0002E688 File Offset: 0x0002C888
			public UnityTextureHolder(string name_)
			{
				this.name = name_;
				this.unityTexture = null;
				GraphicManager.LoadTexture(this.name, new Action<UnityTexture>(this.LoadTextureHandler));
			}

			// Token: 0x06000A80 RID: 2688 RVA: 0x0002E6B8 File Offset: 0x0002C8B8
			public void LoadTextureHandler(UnityTexture unityTexture_)
			{
				this.unityTexture = unityTexture_;
			}

			// Token: 0x06000A81 RID: 2689 RVA: 0x0002E6C4 File Offset: 0x0002C8C4
			public void Release()
			{
				GraphicManager.ReleaseTexture(this.unityTexture);
			}

			// Token: 0x06000A82 RID: 2690 RVA: 0x0002E6D4 File Offset: 0x0002C8D4
			public bool isDone()
			{
				return this.unityTexture != null;
			}

			// Token: 0x04000878 RID: 2168
			private string name;

			// Token: 0x04000879 RID: 2169
			private UnityTexture unityTexture;
		}
	}
}
