﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

// Token: 0x0200013B RID: 315
public class SceneManager : MonoBehaviour
{
	// Token: 0x170000F4 RID: 244
	// (get) Token: 0x06000883 RID: 2179 RVA: 0x0002613C File Offset: 0x0002433C
	private static SceneManager Instance
	{
		get
		{
			GameObject gameObject = GameObject.Find("__SceneManager");
			if (gameObject == null)
			{
				gameObject = new GameObject("__SceneManager");
				SceneManager.instance = gameObject.AddComponent<SceneManager>();
			}
			if (SceneManager.instance == null)
			{
				SceneManager.instance = gameObject.GetComponent<SceneManager>();
			}
			return SceneManager.instance;
		}
	}

	// Token: 0x06000884 RID: 2180 RVA: 0x00026198 File Offset: 0x00024398
	public static void ClearSceneStack()
	{
		SceneManager.SceneStack.Clear();
		SceneManager.SceneStack.Add(SceneManager.CurrentSceneName);
		SceneManager.SceneStackByAdd.Clear();
	}

	// Token: 0x06000885 RID: 2181 RVA: 0x000261C0 File Offset: 0x000243C0
	public static void AddScene(string sceneName)
	{
		SceneManager.SceneStackByAdd.Add(sceneName);
		SceneManager.Instance.StartWaitForLoadScene(sceneName);
	}

	// Token: 0x06000886 RID: 2182 RVA: 0x000261D8 File Offset: 0x000243D8
	private static void DeleteScene(string sceneName)
	{
		SceneManager.UnloadScene(sceneName);
		if (SceneManager.SceneStackByAdd.Contains(sceneName))
		{
			SceneManager.SceneStackByAdd.Remove(sceneName);
		}
		GameObject gameObject = GameObject.Find(SceneManager.GetCurrentScene() + "/Wnd");
		if (gameObject != null)
		{
			BaseWindow component = gameObject.GetComponent<BaseWindow>();
			if (component != null)
			{
				component.OnCurrentWindow();
			}
		}
	}

	// Token: 0x06000887 RID: 2183 RVA: 0x00026244 File Offset: 0x00024444
	private static string GetCurrentScene()
	{
		int count = SceneManager.SceneStackByAdd.Count;
		return (count == 0) ? SceneManager.CurrentSceneName : SceneManager.SceneStackByAdd[count - 1];
	}

	// Token: 0x06000888 RID: 2184 RVA: 0x0002627C File Offset: 0x0002447C
	public static void DeleteLastAddScene()
	{
		if (SceneManager.SceneStackByAdd.Count == 0)
		{
			return;
		}
		string sceneName = SceneManager.SceneStackByAdd[SceneManager.SceneStackByAdd.Count - 1];
		SceneManager.DeleteScene(sceneName);
	}

	// Token: 0x06000889 RID: 2185 RVA: 0x000262B8 File Offset: 0x000244B8
	public static void DeleteAllAddScene()
	{
		if (SceneManager.SceneStackByAdd.Count == 0)
		{
			return;
		}
		while (SceneManager.SceneStackByAdd.Count > 0)
		{
			SceneManager.DeleteLastAddScene();
		}
	}

	// Token: 0x0600088A RID: 2186 RVA: 0x000262F0 File Offset: 0x000244F0
	public static void ChangeScene(string nextSceneName)
	{
		if (SceneManager.CurrentSceneName != null && SceneManager.CurrentSceneName != nextSceneName)
		{
			SceneManager.SceneStack.Add(SceneManager.CurrentSceneName);
		}
		SceneManager.Instance.StartChangeSceneCoroutine(SceneManager.CurrentSceneName, nextSceneName);
	}

	// Token: 0x0600088B RID: 2187 RVA: 0x0002632C File Offset: 0x0002452C
	public static void BackScene()
	{
		if (SceneManager.SceneStack.Count == 0)
		{
			return;
		}
		string nextSceneName = SceneManager.SceneStack[SceneManager.SceneStack.Count - 1];
		SceneManager.SceneStack.RemoveAt(SceneManager.SceneStack.Count - 1);
		SceneManager.Instance.StartChangeSceneCoroutine(SceneManager.CurrentSceneName, nextSceneName);
	}

	// Token: 0x0600088C RID: 2188 RVA: 0x00026388 File Offset: 0x00024588
	private static AsyncOperation LoadScene(string sceneName)
	{
		return UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
		//return Application.LoadLevelAdditiveAsync(sceneName);
	}

	// Token: 0x0600088D RID: 2189 RVA: 0x00026390 File Offset: 0x00024590
	private static void UnloadScene(string sceneName)
	{
		//UnityEngine.SceneManagement.SceneManager.UnloadScene(sceneName);
		GameObject gameObject = GameObject.Find(sceneName);
		if (gameObject != null)
		{
			UnityEngine.Object.Destroy(gameObject);
		}
		Application.UnloadLevel(sceneName);
		
		Resources.UnloadUnusedAssets();
	}

	// Token: 0x0600088E RID: 2190 RVA: 0x000263C0 File Offset: 0x000245C0
	private void SetCurrentSceneName(string nextSceneName)
	{
		SceneManager.CurrentSceneName = nextSceneName;
	}

	// Token: 0x0600088F RID: 2191 RVA: 0x000263C8 File Offset: 0x000245C8
	private void StartChangeSceneCoroutine(string currentSceneName, string nextSceneName)
	{
		base.StartCoroutine(this.ChangeSceneCoroutine(currentSceneName, nextSceneName, new Action<string>(this.SetCurrentSceneName)));
	}

	// Token: 0x06000890 RID: 2192 RVA: 0x000263E8 File Offset: 0x000245E8
	private void StartWaitForLoadScene(string nextSceneName)
	{
		base.StartCoroutine(this.WaitForLoadScene(null, nextSceneName, null));
	}

	// Token: 0x06000891 RID: 2193 RVA: 0x000263FC File Offset: 0x000245FC
	private IEnumerator WaitForLoadScene(string currentSceneName, string nextSceneName, Action<string> action)
	{
		AsyncOperation ao = SceneManager.LoadScene(nextSceneName);
		//SceneManager.LoadScene(nextSceneName);
		if (currentSceneName != null)
		{
			SceneManager.UnloadScene(currentSceneName);
		}
		if (action != null)
		{
			action(nextSceneName);
		}
		while (ao.progress < 1f)
		{
			yield return new WaitForSeconds(0.1f);
		}
		GameObject go = GameObject.Find(nextSceneName + "/Wnd");
		if (go != null)
		{
			BaseWindow w = go.GetComponent<BaseWindow>();
			if (w != null)
			{
				yield return base.StartCoroutine(w.WaitLoadTexture());
				w.OnCurrentWindow();
			}
		}
		yield break;
	}

	// Token: 0x06000892 RID: 2194 RVA: 0x00026444 File Offset: 0x00024644
	private IEnumerator ChangeSceneCoroutine(string currentSceneName, string nextSceneName, Action<string> action)
	{
		yield return base.StartCoroutine(ScreenEffect.FadeBlackOut(0.5f));
		yield return base.StartCoroutine(this.WaitForLoadScene(currentSceneName, nextSceneName, action));
		yield return base.StartCoroutine(ScreenEffect.FadeBlackIn(0.5f));
		ScreenEffect.Term();
		yield break;
	}

	// Token: 0x04000767 RID: 1895
	private static SceneManager instance;

	// Token: 0x04000768 RID: 1896
	private static string CurrentSceneName;

	// Token: 0x04000769 RID: 1897
	private static List<string> SceneStack = new List<string>();

	// Token: 0x0400076A RID: 1898
	private static List<string> SceneStackByAdd = new List<string>();
}
