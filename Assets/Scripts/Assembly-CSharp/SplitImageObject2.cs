﻿using System;
using UnityEngine;

// Token: 0x0200019B RID: 411
public class SplitImageObject2 : ImageObject2
{
	// Token: 0x06000BED RID: 3053 RVA: 0x000320E4 File Offset: 0x000302E4
	public SplitImageObject2(GameObject go, int x, int y, int z, UnityTexture uniTex, int imgCountX, int imgCountY) : base(go, x, y, z, uniTex)
	{
		this.SetImageCount(imgCountX, imgCountY);
	}

	// Token: 0x170001A3 RID: 419
	// (get) Token: 0x06000BEE RID: 3054 RVA: 0x00032100 File Offset: 0x00030300
	public int ImageCountX
	{
		get
		{
			return this.m_ImageCountX;
		}
	}

	// Token: 0x170001A4 RID: 420
	// (get) Token: 0x06000BEF RID: 3055 RVA: 0x00032108 File Offset: 0x00030308
	public int ImageCountY
	{
		get
		{
			return this.m_ImageCountY;
		}
	}

	// Token: 0x06000BF0 RID: 3056 RVA: 0x00032110 File Offset: 0x00030310
	public void SetImageCount(int countX, int countY)
	{
		this.m_ImageCountX = countX;
		this.m_ImageCountY = countY;
		this.FixedTexture();
	}

	// Token: 0x06000BF1 RID: 3057 RVA: 0x00032128 File Offset: 0x00030328
	public override void FixedTexture()
	{
		if (base.HasTexture)
		{
		}
	}

	// Token: 0x04000938 RID: 2360
	private int m_ImageCountX;

	// Token: 0x04000939 RID: 2361
	private int m_ImageCountY;
}
