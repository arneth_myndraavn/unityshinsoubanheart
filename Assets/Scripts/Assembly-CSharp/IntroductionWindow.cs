﻿using System;
using Game;
using Qoo;
using Qoo.Game;
using UnityEngine;

// Token: 0x02000118 RID: 280
public class IntroductionWindow : BaseWindow
{
	// Token: 0x06000767 RID: 1895 RVA: 0x0001FB6C File Offset: 0x0001DD6C
	protected sealed override string GetBGMName()
	{
		return "lovecheck2_ali";
	}

	// Token: 0x06000768 RID: 1896 RVA: 0x0001FB74 File Offset: 0x0001DD74
	protected sealed override void OnAwake()
	{
		this.m_chara = UIValue.GalleryCharactor;
		this.loading = false;
		this.checkFlick = true;
		this.checkButton = true;
	}

	// Token: 0x06000769 RID: 1897 RVA: 0x0001FBA4 File Offset: 0x0001DDA4
	protected sealed override string[] newSceneTextureNameArray()
	{
		return new string[]
		{
			this.GetFilenameBackGround(),
			this.GetFilenameMessage(),
			"screen/title/buy05",
			"screen/title/buy09",
			"screen/title/kyara_voice",
			"screen/sysmenu/sys_smv",
			"screen/cgmemory/cgm_arwl",
			"screen/cgmemory/cgm_arwr",
			"screen/common/cancel"
		};
	}

	// Token: 0x0600076A RID: 1898 RVA: 0x0001FC04 File Offset: 0x0001DE04
	protected sealed override void AfterInit()
	{
		float num = 0f;
		int num2 = 3;
		float num3 = 0.8f;
		ImageObject imageObject = base.GetImageObject("ArrowL");
		imageObject.AddImageAnimation(new ImageAnimationLocalPosition(num2, num3, num, MoveType.LoopSin000to180, new Vector3(0f, 0f, 0f), new Vector3(-25f, 0f, 0f)));
		imageObject.AddImageAnimation(new ImageAnimationMaterialColor(1, num3, num + (float)(num2 - 1) * num3, new Color(1f, 1f, 1f, 1f), new Color(1f, 1f, 1f, 0f)));
		ImageObject imageObject2 = base.GetImageObject("ArrowR");
		imageObject2.AddImageAnimation(new ImageAnimationLocalPosition(num2, num3, num, MoveType.LoopSin000to180, new Vector3(0f, 0f, 0f), new Vector3(25f, 0f, 0f)));
		imageObject2.AddImageAnimation(new ImageAnimationMaterialColor(1, num3, num + (float)(num2 - 1) * num3, new Color(1f, 1f, 1f, 1f), new Color(1f, 1f, 1f, 0f)));
		bool voiceButtonShow = this.GetVoiceButtonShow();
		base.GameObjectShow("SampleVoice", voiceButtonShow);
		base.GameObjectShow("SampleVoice1", voiceButtonShow);
		base.GameObjectShow("SampleVoice2", voiceButtonShow);
		base.GameObjectShow("SampleVoice3", voiceButtonShow);
	}

	// Token: 0x0600076B RID: 1899 RVA: 0x0001FD74 File Offset: 0x0001DF74
	protected sealed override BaseWindow.UIComponent[] newComponentArray()
	{
		string filenameBackGround = this.GetFilenameBackGround();
		string filenameMessage = this.GetFilenameMessage();
		string filenameButton = this.GetFilenameButton();
		bool voiceButtonShow = this.GetVoiceButtonShow();
		return new BaseWindow.UIComponent[]
		{
			new BaseWindow.UIImage("DummyBackGround", 0, 0, this.wndz, filenameBackGround, false, false),
			new BaseWindow.UIImage("DummyMessage", 304, 275, this.wndz + 1, filenameMessage, false, false),
			new BaseWindow.UIImage("BackGround", 0, 0, this.wndz, filenameBackGround, false, true),
			new BaseWindow.UIImage("Message", 304, 275, this.wndz + 1, filenameMessage, false, true),
			//new BaseWindow.UIButton("Start", 465, 172, this.wndz + 1, filenameButton, true, this.GetStartButtonShow(), 1, 2, 0),
			new BaseWindow.UIImage("SampleVoice", 50, 423, this.wndz + 1, "screen/title/kyara_voice", true, voiceButtonShow),
			new BaseWindow.UIButton("SampleVoice1", 45, 460, this.wndz + 1, "screen/sysmenu/sys_smv", true, voiceButtonShow, 1, 2, 0),
			new BaseWindow.UIButton("SampleVoice2", 127, 460, this.wndz + 1, "screen/sysmenu/sys_smv", true, voiceButtonShow, 1, 2, 0),
			new BaseWindow.UIButton("SampleVoice3", 208, 460, this.wndz + 1, "screen/sysmenu/sys_smv", true, voiceButtonShow, 1, 2, 0),
			new BaseWindow.UIImage("ArrowL", 20, 199, this.wndz + 1, "screen/cgmemory/cgm_arwl", false, true),
			new BaseWindow.UIImage("ArrowR", 861, 199, this.wndz + 1, "screen/cgmemory/cgm_arwr", false, true),
			new BaseWindow.UIButton("Cancel", 887, 6, this.wndz + 1, "screen/common/cancel", true, true, 1, 2, 0)
		};
	}

	// Token: 0x0600076C RID: 1900 RVA: 0x0001FF50 File Offset: 0x0001E150
	protected override void OnBaseWindowUpdate()
	{
		if (this.loading)
		{
			if (!this.LoadCompleted())
			{
				return;
			}
			this.SetCharaTexture(string.Empty);
			//base.SetTexture("Start", this.GetFilenameButton());
			this.SetButtonShow();
			base.RestartAnimation("ArrowL");
			base.RestartAnimation("ArrowR");
			this.loading = false;
			this.checkFlick = true;
			this.checkButton = true;
		}
		if (!this.checkFlick)
		{
			return;
		}
		INPUT_STATUS status = UnityApp.Input.Status;
		if (status == INPUT_STATUS.DRAG || status == INPUT_STATUS.FLICK)
		{
			this.OnFlick(UnityApp.Input.SlideDragVector);
		}
	}

	// Token: 0x0600076D RID: 1901 RVA: 0x00020000 File Offset: 0x0001E200
	protected sealed override void OnBaseWindowOnButton(string obj)
	{
		if (this.loading)
		{
			return;
		}
		if (!this.checkButton)
		{
			return;
		}
		switch (obj)
		{
		case "Start":
			this.checkFlick = false;
			base.StopVoice();
			base.PlaySE_Ok();
			UIValue.Payment_Call = PaymentCallType.TITLE;
			SceneManager.ChangeScene(UIValue.SCENE_PAYMENT);
			break;
		case "SampleVoice1":
		case "SampleVoice2":
		case "SampleVoice3":
			base.PlayVoice(Chara.GetVoiceFromId(this.GetCharaId()) + this.voice[this.m_chara, this.VoiceButtonToIndex(obj)].ToString("_0000"), false);
			break;
		case "Cancel":
			this.checkFlick = false;
			base.StopVoice();
			base.PlaySE_Cancel();
			SceneManager.BackScene();
			break;
		}
	}

	// Token: 0x0600076E RID: 1902 RVA: 0x0002012C File Offset: 0x0001E32C
	private void SetButtonShow()
	{
		bool voiceButtonShow = this.GetVoiceButtonShow();
		base.GameObjectShow("SampleVoice", voiceButtonShow);
		base.GameObjectShow("SampleVoice1", voiceButtonShow);
		base.GameObjectShow("SampleVoice2", voiceButtonShow);
		base.GameObjectShow("SampleVoice3", voiceButtonShow);
		//base.GameObjectShow("Start", this.GetStartButtonShow());
	}

	// Token: 0x0600076F RID: 1903 RVA: 0x00020184 File Offset: 0x0001E384
	private bool GetStartButtonShow()
	{
		return !base.IsTrial && this.GetCharaId() != CHAR_ID.NIGHTMARE;
	}

	// Token: 0x06000770 RID: 1904 RVA: 0x000201A4 File Offset: 0x0001E3A4
	private bool GetVoiceButtonShow()
	{
		return this.GetCharaId() != CHAR_ID.MOB;
	}

	// Token: 0x06000771 RID: 1905 RVA: 0x000201B4 File Offset: 0x0001E3B4
	private int VoiceButtonToIndex(string obj)
	{
		switch (obj)
		{
		case "SampleVoice1":
			return 0;
		case "SampleVoice2":
			return 1;
		case "SampleVoice3":
			return 2;
		}
		return 0;
	}

	// Token: 0x06000772 RID: 1906 RVA: 0x00020234 File Offset: 0x0001E434
	private void OnFlick(SLIDE_VECTOR slide)
	{
		if (slide == SLIDE_VECTOR.LEFT || slide == SLIDE_VECTOR.RIGHT)
		{
			this.OnCharaChange((slide != SLIDE_VECTOR.LEFT) ? this.GetRightChara() : this.GetLeftChara());
		}
	}

	// Token: 0x06000773 RID: 1907 RVA: 0x0002027C File Offset: 0x0001E47C
	private bool LoadCompleted()
	{
		return base.LoadCompleted("DummyBackGround") && base.LoadCompleted("DummyMessage");
	}

	// Token: 0x06000774 RID: 1908 RVA: 0x0002029C File Offset: 0x0001E49C
	private void SetCharaTexture(string prefix)
	{
		base.SetTexture(prefix + "BackGround", this.GetFilenameBackGround());
		base.SetTexture(prefix + "Message", this.GetFilenameMessage());
	}

	// Token: 0x06000775 RID: 1909 RVA: 0x000202D8 File Offset: 0x0001E4D8
	private void OnCharaChange(int chara)
	{
		this.loading = true;
		this.m_chara = chara;
		UIValue.GalleryCharactor = this.m_chara;
		this.SetCharaTexture("Dummy");
		this.checkFlick = false;
		this.checkButton = false;
		base.PlaySE_Select();
		base.StopVoice();
	}

	// Token: 0x06000776 RID: 1910 RVA: 0x00020324 File Offset: 0x0001E524
	private int GetRightChara()
	{
		int numberOfButton = this.GetNumberOfButton();
		return (this.m_chara - 1 + numberOfButton) % numberOfButton;
	}

	// Token: 0x06000777 RID: 1911 RVA: 0x00020344 File Offset: 0x0001E544
	private int GetLeftChara()
	{
		int numberOfButton = this.GetNumberOfButton();
		return (this.m_chara + 1 + numberOfButton) % numberOfButton;
	}

	// Token: 0x06000778 RID: 1912 RVA: 0x00020364 File Offset: 0x0001E564
	private int GetNumberOfButton()
	{
		return 11;
	}

	// Token: 0x06000779 RID: 1913 RVA: 0x00020368 File Offset: 0x0001E568
	private CHAR_ID GetCharaId()
	{
		return (CHAR_ID)this.m_chara;
	}

	// Token: 0x0600077A RID: 1914 RVA: 0x00020370 File Offset: 0x0001E570
	private string GetFilenameBackGround()
	{
		return this.GetFilenameId(this.m_chara).ToString("screen/title/kyara_00");
	}

	// Token: 0x0600077B RID: 1915 RVA: 0x00020398 File Offset: 0x0001E598
	private string GetFilenameMessage()
	{
		string text = this.GetFilenameId(this.m_chara).ToString("screen/title/kyara_m_00");
		if (base.IsTrial && this.GetCharaId() == CHAR_ID.MOB)
		{
			text += "_tri";
		}
		Qoo.Debug.Print(text);
		return text;
	}

	// Token: 0x0600077C RID: 1916 RVA: 0x000203EC File Offset: 0x0001E5EC
	private string GetFilenameButton()
	{
		return (this.GetCharaId() != CHAR_ID.MOB) ? "screen/title/buy05" : "screen/title/buy09";
	}

	// Token: 0x0600077D RID: 1917 RVA: 0x0002040C File Offset: 0x0001E60C
	private int GetFilenameId(int i)
	{
		return i + 1;
	}

	// Token: 0x040006B9 RID: 1721
	private readonly int wndz;

	// Token: 0x040006BA RID: 1722
	private int m_chara;

	// Token: 0x040006BB RID: 1723
	private bool loading;

	// Token: 0x040006BC RID: 1724
	private bool checkFlick;

	// Token: 0x040006BD RID: 1725
	private bool checkButton;

	// Token: 0x040006BE RID: 1726
	private int[,] voice = new int[,]
	{
		{
			642,
			735,
			1334
		},
		{
			15,
			468,
			737
		},
		{
			236,
			811,
			836
		},
		{
			743,
			769,
			1545
		},
		{
			6,
			326,
			742
		},
		{
			321,
			794,
			2510
		},
		{
			395,
			1209,
			1957
		},
		{
			542,
			585,
			1717
		},
		{
			86,
			214,
			656
		},
		{
			40,
			770,
			1454
		}
	};
}
