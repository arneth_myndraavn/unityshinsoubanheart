﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;

// Token: 0x02000139 RID: 313
public class ChannelTexture
{
	// Token: 0x06000857 RID: 2135 RVA: 0x00025928 File Offset: 0x00023B28
	public ChannelTexture(int width, int height, TextureFormat format)
	{
		this.tex = new Texture2D(width, height, format, false);
		this.tex.wrapMode = TextureWrapMode.Clamp;
		this.ReAlloc();
	}

	// Token: 0x06000858 RID: 2136
	[DllImport("__Internal")]
	private static extern void ReAllocTexture(int handle, int x, int y);

	// Token: 0x06000859 RID: 2137 RVA: 0x00025954 File Offset: 0x00023B54
	public void ReAlloc()
	{
		ChannelTexture.ReAllocTexture(this.tex.GetNativeTextureID(), this.tex.width, this.tex.height);
	}

	// Token: 0x0600085A RID: 2138 RVA: 0x00025988 File Offset: 0x00023B88
	public void Destroy()
	{
		UnityEngine.Object.Destroy(this.tex);
	}

	// Token: 0x0400074E RID: 1870
	private const string PLATFORM_DLL = "__Internal";

	// Token: 0x0400074F RID: 1871
	public Texture2D tex;
}
