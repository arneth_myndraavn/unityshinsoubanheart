﻿using System;
using System.Collections.Generic;

// Token: 0x02000109 RID: 265
public abstract class GalleryCharaSelectWindow : BaseWindow
{
	// Token: 0x060006FB RID: 1787
	protected abstract int GetCollect();

	// Token: 0x060006FC RID: 1788
	protected abstract void OnName();

	// Token: 0x060006FD RID: 1789
	protected abstract bool IsNameButton();

	// Token: 0x060006FE RID: 1790
	protected abstract string GetHeaderPath();

	// Token: 0x060006FF RID: 1791
	protected abstract string GetCharaButtonSceneName();

	// Token: 0x06000700 RID: 1792 RVA: 0x0001D82C File Offset: 0x0001BA2C
	protected sealed override string[] newSceneTextureNameArray()
	{
		return new string[]
		{
			"screen/cgmemory2/cgm_bg_1",
			this.GetHeaderPath(),
			"screen/cgmemory/cgm_kumi0",
			"screen/cgmemory/cgm_kumi1",
			"screen/cgmemory/cgm_kumi2",
			"screen/cgmemory/cgm_kumi3",
			"screen/cgmemory/cgm_kumi4",
			"screen/cgmemory/cgm_kumi5",
			"screen/cgmemory/cgm_kumi6",
			"screen/cgmemory/cgm_kumi7",
			"screen/cgmemory/cgm_kumi8",
			"screen/cgmemory/cgm_kumi9",
			"screen/cgmemory/cgm_btn_cha01",
			"screen/cgmemory/cgm_btn_cha02",
			"screen/cgmemory/cgm_btn_cha03",
			"screen/cgmemory/cgm_btn_cha04",
			"screen/cgmemory/cgm_btn_cha05",
			"screen/cgmemory/cgm_btn_cha06",
			"screen/cgmemory/cgm_btn_cha07",
			"screen/cgmemory/cgm_btn_cha08",
			"screen/cgmemory/cgm_btn_cha09",
			"screen/cgmemory/cgm_btn_cha10",
			"screen/cgmemory/cgm_btn_cha11",
			"screen/cgmemory/cgm_btn_name",
			"screen/common/cancel"
		};
	}

	// Token: 0x06000701 RID: 1793 RVA: 0x0001D91C File Offset: 0x0001BB1C
	protected sealed override BaseWindow.UIComponent[] newComponentArray()
	{
		List<BaseWindow.UIComponent> list = new List<BaseWindow.UIComponent>();
		list.Add(new BaseWindow.UIImage("BackGround", 0, 0, this.wndz, "screen/cgmemory2/cgm_bg_1", false, true));
		list.Add(new BaseWindow.UIImage("Header", 0, 0, this.wndz + 1, this.GetHeaderPath(), false, true));
		int collect = this.GetCollect();
		if (collect >= 0)
		{
			int num = collect / 100;
			int num2 = (collect - num * 100) / 10;
			int num3 = collect - num * 100 - num2 * 10;
			list.Add(new BaseWindow.UIImage("Per100", 728, 17, this.wndz + 2, num.ToString("screen/cgmemory/cgm_kumi0"), false, true));
			list.Add(new BaseWindow.UIImage("Per010", 758, 17, this.wndz + 2, num2.ToString("screen/cgmemory/cgm_kumi0"), false, true));
			list.Add(new BaseWindow.UIImage("Per001", 788, 17, this.wndz + 2, num3.ToString("screen/cgmemory/cgm_kumi0"), false, true));
		}
		int num4 = 0;
		for (int i = 0; i < 2; i++)
		{
			int y_ = (i != 0) ? 297 : 91;
			int num5 = (i != 0) ? 85 : 5;
			int num6 = (i != 0) ? 5 : 6;
			int j = 0;
			while (j < num6)
			{
				int x_ = num5 + 158 * j;
				string filenameString = this.GetFilenameString(num4);
				list.Add(new BaseWindow.UIButton("Chara" + filenameString, x_, y_, this.wndz + 2, "screen/cgmemory/cgm_btn_cha" + filenameString, true, true, 1, 2, 0));
				j++;
				num4++;
			}
		}
		if (this.IsNameButton())
		{
			list.Add(new BaseWindow.UIButton("BtnName", 734, 499, 2, "screen/cgmemory/cgm_btn_name", true, true, 1, 2, 0));
		}
		list.Add(new BaseWindow.UIButton("Cancel", 887, 6, 3, "screen/common/cancel", true, true, 1, 2, 0));
		return list.ToArray();
	}

	// Token: 0x06000702 RID: 1794 RVA: 0x0001DB30 File Offset: 0x0001BD30
	protected sealed override void OnBaseWindowOnButton(string obj)
	{
		switch (obj)
		{
		case "Chara01":
		case "Chara02":
		case "Chara03":
		case "Chara04":
		case "Chara05":
		case "Chara06":
		case "Chara07":
		case "Chara08":
		case "Chara09":
		case "Chara10":
		case "Chara11":
			UIValue.GalleryPage = 0;
			UIValue.GalleryCharactor = this.ToCharId(obj);
			base.PlaySE_Ok();
			SceneManager.ChangeScene(this.GetCharaButtonSceneName());
			break;
		case "BtnName":
			base.PlaySE_Ok();
			this.OnName();
			break;
		case "Cancel":
			base.PlaySE_Cancel();
			SceneManager.BackScene();
			break;
		}
	}

	// Token: 0x06000703 RID: 1795 RVA: 0x0001DC6C File Offset: 0x0001BE6C
	private string GetButtonName(int i)
	{
		return "Chara" + this.GetFilenameString(i);
	}

	// Token: 0x06000704 RID: 1796 RVA: 0x0001DC80 File Offset: 0x0001BE80
	private string GetFilenameString(int i)
	{
		return (i + 1).ToString("00");
	}

	// Token: 0x06000705 RID: 1797 RVA: 0x0001DCA0 File Offset: 0x0001BEA0
	private int ToCharId(string obj)
	{
		switch (obj)
		{
		case "Chara01":
			return 0;
		case "Chara02":
			return 1;
		case "Chara03":
			return 2;
		case "Chara04":
			return 3;
		case "Chara05":
			return 4;
		case "Chara06":
			return 5;
		case "Chara07":
			return 6;
		case "Chara08":
			return 7;
		case "Chara09":
			return 8;
		case "Chara10":
			return 9;
		case "Chara11":
			return 10;
		}
		return -1;
	}

	// Token: 0x0400067D RID: 1661
	private readonly int wndz;
}
