﻿using System;

// Token: 0x02000193 RID: 403
public enum MoveType
{
	// Token: 0x0400090E RID: 2318
	OneshotLiner,
	// Token: 0x0400090F RID: 2319
	LoopSin000to180,
	// Token: 0x04000910 RID: 2320
	OneshotCos180to360,
	// Token: 0x04000911 RID: 2321
	OneshotSin000to090
}
