﻿using System;
using System.Collections.Generic;
using Qoo.File;
using Qoo.Table;
using UnityEngine;

// Token: 0x02000199 RID: 409
public class ImageObject : MonoBehaviour
{
	// Token: 0x17000197 RID: 407
	// (get) Token: 0x06000BBA RID: 3002 RVA: 0x0003161C File Offset: 0x0002F81C
	public bool LoadCompleted
	{
		get
		{
			return this.loadCompleted;
		}
	}

	// Token: 0x17000198 RID: 408
	// (get) Token: 0x06000BBB RID: 3003 RVA: 0x00031624 File Offset: 0x0002F824
	// (set) Token: 0x06000BBC RID: 3004 RVA: 0x0003162C File Offset: 0x0002F82C
	public bool RendererEnableAfterLoad
	{
		get
		{
			return this.rendererEnableAfterLoad;
		}
		set
		{
			this.rendererEnableAfterLoad = value;
		}
	}

	// Token: 0x17000199 RID: 409
	// (get) Token: 0x06000BBD RID: 3005 RVA: 0x00031638 File Offset: 0x0002F838
	public Texture Texture
	{
		get
		{
			return (this.imageUnityTexture != null) ? this.imageUnityTexture.m_Texture : null;
		}
	}

	// Token: 0x06000BBE RID: 3006 RVA: 0x00031658 File Offset: 0x0002F858
	private void OnDestroy()
	{
		GraphicManager.ReleaseTexture(this.imageUnityTexture);
	}

	// Token: 0x06000BBF RID: 3007 RVA: 0x00031668 File Offset: 0x0002F868
	private void Update()
	{
		Vector3 vector = this.OnViewPosition;
		Vector3 vector2 = this.OnViewScale;
		Color color = this.OnViewColor;
		if (this.imageAnimations != null)
		{
			foreach (IImageAnimation imageAnimation in this.imageAnimations)
			{
				imageAnimation.Update();
				vector = imageAnimation.CalcPosition(vector);
				vector2 = imageAnimation.CalcScale(vector2);
				color = imageAnimation.CalcColor(color);
			}
		}
		if (base.transform.localPosition != vector)
		{
			base.transform.localPosition = vector;
		}
		if (base.transform.localScale != vector2)
		{
			base.transform.localScale = vector2;
		}
		// TODO Color
		/*
		if (base.renderer.material.color != color)
		{
		base.renderer.material.color = color;
		}*/
	}

	// Token: 0x06000BC0 RID: 3008 RVA: 0x00031778 File Offset: 0x0002F978
	public static ImageObject Create(UIObjectInfo uiObject, Transform parent = null, bool collisionEnable = false)
	{
		GameObject gameObject = GameObject.CreatePrimitive(PrimitiveType.Quad);
		//gameObject.renderer.material = new Material(Resources.Load("Shader/Sprite/Sprite") as Shader);
		gameObject.GetComponent<Renderer>().material = new Material(Shader.Find("QO/Sprite") as Shader);
		gameObject.transform.parent = parent;
		gameObject.GetComponent<Collider>().enabled = collisionEnable;
		ImageObject imageObject = gameObject.AddComponent<ImageObject>();
		imageObject.UIObject = uiObject;
		imageObject.InitObject(true, uiObject);
		return imageObject;
	}

	// Token: 0x06000BC1 RID: 3009 RVA: 0x000317DC File Offset: 0x0002F9DC
	public void InitObject(bool initTexture = true, UIObjectInfo uiObject = null)
	{
		this.UIObject = uiObject;
		base.gameObject.name = this.UIObject.ObjectName;
		this.ImageBlockIndex = this.UIObject.DefaultTextureIndex;
		if (initTexture)
		{
			this.InitTexture();
		}
		else
		{
			this.OnTextureLoadComplete2();
		}
		//not needed when using unity unlit. if custom shaders fixed, add back
		//this.OnViewColor = base.renderer.material.color;
	}

	// Token: 0x06000BC2 RID: 3010 RVA: 0x00031844 File Offset: 0x0002FA44
	public void SetTexture(string texturePath)
	{
		if (this.imageUnityTexture != null)
		{
			GraphicManager.ReleaseTexture(this.imageUnityTexture);
			this.imageUnityTexture = null;
			base.GetComponent<Renderer>().material.mainTexture = null;
		}
		this.changeTexture = true;
		this.UIObject.UITexture.TexturePath = texturePath;
		this.InitTexture();
	}

	// Token: 0x06000BC3 RID: 3011 RVA: 0x000318A0 File Offset: 0x0002FAA0
	public void InitTexture()
	{
		this.loadCompleted = false;
		this.InitImageScale();
		GraphicManager.LoadTexture(this.UIObject.UITexture.TexturePath, new Action<UnityTexture>(this.OnTextureLoadComplete));
	}

	// Token: 0x06000BC4 RID: 3012 RVA: 0x000318DC File Offset: 0x0002FADC
	private void InitImageScale()
	{
		string text = this.UIObject.UITexture.TexturePath;
		int num = text.LastIndexOf('/');
		if (num >= 0)
		{
			text = text.Substring(num + 1);
		}
		if (text.Length > 0)
		{
			this.ImageScale = DirScaleTable.GetSize(Nmb.GetFileInfo(text).DirName);
		}
	}

	// Token: 0x06000BC5 RID: 3013 RVA: 0x00031938 File Offset: 0x0002FB38
	public void InitScale()
	{
		if (this.imageUnityTexture == null)
		{
			return;
		}
		this.OriginalScale = new Vector3(0f, 0f, 1f)
		{
			x = (float)((int)((float)this.imageUnityTexture.m_Texture.width * this.ImageScale.w) / this.UIObject.UITexture.BlockX),
			y = (float)((int)((float)this.imageUnityTexture.m_Texture.height * this.ImageScale.h) / this.UIObject.UITexture.BlockY)
		} * (float)this.UIObject.ScaleRate;
	}

	// Token: 0x06000BC6 RID: 3014 RVA: 0x000319F0 File Offset: 0x0002FBF0
	public void InitPosition()
	{
		this.OriginalPosition.Set((float)(-480 + (int)(this.OriginalScale.x / 2f) + this.UIObject.ScreenX), (float)(272 - (int)(this.OriginalScale.y / 2f) - this.UIObject.ScreenY), (float)(-(float)this.UIObject.ScreenZ));
		base.transform.localPosition = this.OriginalPosition;
		base.transform.localScale = this.OriginalScale;
		this.OnViewPosition = this.OriginalPosition;
		this.OnViewScale = this.OriginalScale;
	}

	// Token: 0x06000BC7 RID: 3015 RVA: 0x00031A9C File Offset: 0x0002FC9C
	public void InitTextureUV()
	{
		int num = this.ImageBlockIndex / this.UIObject.UITexture.BlockNum + 1;
		int num2 = this.ImageBlockIndex % this.UIObject.UITexture.BlockNum + 1;
		Vector2 vector = new Vector2(1f / (float)this.UIObject.UITexture.BlockX, 1f / (float)this.UIObject.UITexture.BlockY);
		Vector2 vector2 = new Vector2(1f - vector.x * (float)num, 1f - vector.y * (float)num2);
		base.transform.GetComponent<Renderer>().material.SetVector("_UVWH", new Vector4(vector2.x, vector2.y, vector.x, vector.y));
	}

	// Token: 0x06000BC8 RID: 3016 RVA: 0x00031B74 File Offset: 0x0002FD74
	private void OnTextureLoadComplete(UnityTexture unityTexture)
	{
		this.loadCompleted = true;
		this.imageUnityTexture = unityTexture;
		base.GetComponent<Renderer>().material.mainTexture = unityTexture.m_Texture;
		// TODO issue with changeTexture, fix later
		//base.renderer.enabled = (this.changeTexture && this.rendererEnableAfterLoad);
		base.GetComponent<Renderer>().enabled = this.rendererEnableAfterLoad;
		this.OnTextureLoadComplete2();
	}

	// Token: 0x06000BC9 RID: 3017 RVA: 0x00031BD0 File Offset: 0x0002FDD0
	public void ApplyRendererEnableAfterLoad()
	{
		base.GetComponent<Renderer>().enabled = this.rendererEnableAfterLoad;
	}

	// Token: 0x06000BCA RID: 3018 RVA: 0x00031BE4 File Offset: 0x0002FDE4
	private void OnTextureLoadComplete2()
	{
		this.InitScale();
		this.InitPosition();
		this.InitTextureUV();
	}

	// Token: 0x06000BCB RID: 3019 RVA: 0x00031BF8 File Offset: 0x0002FDF8
	public void AddImageAnimation(IImageAnimation ia)
	{
		if (ia == null)
		{
			return;
		}
		if (this.imageAnimations == null)
		{
			this.imageAnimations = new List<IImageAnimation>();
		}
		this.imageAnimations.Add(ia);
	}

	// Token: 0x06000BCC RID: 3020 RVA: 0x00031C24 File Offset: 0x0002FE24
	public void RestartAnimation()
	{
		if (this.imageAnimations != null)
		{
			foreach (IImageAnimation imageAnimation in this.imageAnimations)
			{
				imageAnimation.Restart();
			}
		}
	}

	// Token: 0x06000BCD RID: 3021 RVA: 0x00031C94 File Offset: 0x0002FE94
	public void SetBoxCollider(Vector3 sizeRate)
	{
		MeshCollider component = base.gameObject.GetComponent<MeshCollider>();
		if (component != null)
		{
			component.enabled = false;
		}
		BoxCollider boxCollider = base.gameObject.GetComponent<BoxCollider>();
		if (boxCollider == null)
		{
			boxCollider = base.gameObject.AddComponent<BoxCollider>();
		}
		boxCollider.size = sizeRate;
	}

	// Token: 0x04000923 RID: 2339
	public Vector3 OriginalPosition;

	// Token: 0x04000924 RID: 2340
	public Vector3 OriginalScale;

	// Token: 0x04000925 RID: 2341
	public Vector3 OnViewPosition;

	// Token: 0x04000926 RID: 2342
	public Vector3 OnViewScale;

	// Token: 0x04000927 RID: 2343
	public Color OnViewColor;

	// Token: 0x04000928 RID: 2344
	private UnityTexture imageUnityTexture;

	// Token: 0x04000929 RID: 2345
	public SizeF ImageScale = new SizeF(1f, 1f);

	// Token: 0x0400092A RID: 2346
	public int ImageBlockIndex;

	// Token: 0x0400092B RID: 2347
	public UIObjectInfo UIObject;

	// Token: 0x0400092C RID: 2348
	private bool loadCompleted;

	// Token: 0x0400092D RID: 2349
	private bool rendererEnableAfterLoad = true;

	// Token: 0x0400092E RID: 2350
	private bool changeTexture;

	// Token: 0x0400092F RID: 2351
	private List<IImageAnimation> imageAnimations;
}
