﻿using System;

// Token: 0x0200017D RID: 381
public class SizeF
{
	// Token: 0x06000ACB RID: 2763 RVA: 0x0002F610 File Offset: 0x0002D810
	public SizeF(float w_ = 0f, float h_ = 0f)
	{
		this.w = w_;
		this.h = h_;
	}

	// Token: 0x040008BB RID: 2235
	public float w;

	// Token: 0x040008BC RID: 2236
	public float h;
}
