﻿using System;
using System.Collections;
using UnityEngine;

// Token: 0x020000F1 RID: 241
public class SubPartTask
{
	// Token: 0x06000683 RID: 1667 RVA: 0x0001B004 File Offset: 0x00019204
	protected IEnumerator Open(string sceneName, bool bFade)
	{
		SceneManager.AddScene(sceneName);
		GameObject go = null;
		while (go == null)
		{
			yield return 0;
			Singleton<TaskManager>.Instance.UpdateReadTask();
			go = GameObject.Find(sceneName + "/Wnd");
		}
		Singleton<UnityGraph>.Instance.Disable(false);
		if (bFade)
		{
			EffectManager.FadeBlackIn(0.5f);
			while (EffectManager.FadeIsEnd())
			{
				yield return 0;
			}
		}
		SubTaskBaseWindow baseTaskWindow = go.GetComponent<SubTaskBaseWindow>();
		while (!baseTaskWindow.IsTaskEnd)
		{
			yield return 0;
			Singleton<TaskManager>.Instance.UpdateReadTask();
		}
		if (bFade)
		{
			EffectManager.FadeBlackOut(0.5f);
			while (EffectManager.FadeIsEnd())
			{
				yield return 0;
			}
		}
		Singleton<UnityGraph>.Instance.Enable(false);
		yield return 0;
		SceneManager.DeleteLastAddScene();
		yield break;
	}
}
