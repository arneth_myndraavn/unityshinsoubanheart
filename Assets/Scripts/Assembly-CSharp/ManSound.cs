﻿using System;
using System.Collections.Generic;
using Qoo;

// Token: 0x02000189 RID: 393
public class ManSound : Singleton<ManSound>
{
	// Token: 0x17000173 RID: 371
	// (get) Token: 0x06000B18 RID: 2840 RVA: 0x00030064 File Offset: 0x0002E264
	// (set) Token: 0x06000B19 RID: 2841 RVA: 0x0003006C File Offset: 0x0002E26C
	public float BgmVolume
	{
		get
		{
			return this.volBgm;
		}
		set
		{
			this.volBgm = value;
		}
	}

	// Token: 0x17000174 RID: 372
	// (get) Token: 0x06000B1A RID: 2842 RVA: 0x00030078 File Offset: 0x0002E278
	// (set) Token: 0x06000B1B RID: 2843 RVA: 0x00030080 File Offset: 0x0002E280
	public float SeVolume
	{
		get
		{
			return this.volSe;
		}
		set
		{
			this.volSe = value;
		}
	}

	// Token: 0x17000175 RID: 373
	// (get) Token: 0x06000B1C RID: 2844 RVA: 0x0003008C File Offset: 0x0002E28C
	// (set) Token: 0x06000B1D RID: 2845 RVA: 0x00030094 File Offset: 0x0002E294
	public float SysSeVolume
	{
		get
		{
			return this.volSysSe;
		}
		set
		{
			this.volSysSe = value;
		}
	}

	// Token: 0x06000B1E RID: 2846 RVA: 0x000300A0 File Offset: 0x0002E2A0
	private void Awake()
	{
		this.bgm = SoundStream.CreateObject(base.gameObject, "Bgm");
		this.voice = SoundStream.CreateObject(base.gameObject, "Voice");
		this.se = SoundVoice.CreateObject(base.gameObject, "SysSe");
		this.player_se = SoundVoice.CreateObject(base.gameObject, "SysSe_Player");
		for (int num = 0; num != this.slotSe.Length; num++)
		{
			this.slotSe[num] = SoundStream.CreateObject(base.gameObject, "SlotSe" + num.ToString());
		}
		this.data.Clear();
	}

	// Token: 0x06000B1F RID: 2847 RVA: 0x00030150 File Offset: 0x0002E350
	private void Update()
	{
	}

	// Token: 0x06000B20 RID: 2848 RVA: 0x00030154 File Offset: 0x0002E354
	public void BgmPlay(string name, float fVol = 1f, int nInTime = 1000, int nOutTime = 1000)
	{
		this.bgm.Play("bgm", name, true, fVol * this.BgmVolume, nInTime, nOutTime);
	}

	// Token: 0x06000B21 RID: 2849 RVA: 0x00030180 File Offset: 0x0002E380
	public void BgmStop(int timeOut = 1000)
	{
		this.bgm.Stop(timeOut);
	}

	// Token: 0x06000B22 RID: 2850 RVA: 0x00030190 File Offset: 0x0002E390
	public bool BgmIsPlay()
	{
		return this.bgm.IsPlay;
	}

	// Token: 0x06000B23 RID: 2851 RVA: 0x000301A0 File Offset: 0x0002E3A0
	public void BgmSetVolume(float fVol)
	{
		this.bgm.Current.Volume = fVol;
	}

	// Token: 0x06000B24 RID: 2852 RVA: 0x000301B4 File Offset: 0x0002E3B4
	public void VoicePlay(string name, float fVol)
	{
		if (fVol == 0f)
		{
			return;
		}
		this.voice.Play("voice", name, false, fVol, 1000, 1000);
	}

	// Token: 0x06000B25 RID: 2853 RVA: 0x000301E0 File Offset: 0x0002E3E0
	public void VoiceStop()
	{
		this.voice.Stop(1000);
	}

	// Token: 0x06000B26 RID: 2854 RVA: 0x000301F4 File Offset: 0x0002E3F4
	public bool VoiceIsPlay()
	{
		return this.voice.IsPlay;
	}

	// Token: 0x06000B27 RID: 2855 RVA: 0x00030204 File Offset: 0x0002E404
	public void VoicePause(bool isPause)
	{
		this.voice.Pause(isPause);
	}

	// Token: 0x06000B28 RID: 2856 RVA: 0x00030214 File Offset: 0x0002E414
	public void VoiceSetVolume(float fVol)
	{
		this.voice.Current.Volume = fVol;
	}

	// Token: 0x06000B29 RID: 2857 RVA: 0x00030228 File Offset: 0x0002E428
	public void SlotSePlay(int slot, string name, float fVol, bool IsLoop)
	{
		Debug.Assert(slot < this.slotSe.Length && slot >= 0);
		if (this.SeVolume > 0f)
		{
			this.slotSe[slot].Play("se", name, IsLoop, fVol * this.SeVolume, 0, 0);
		}
	}

	// Token: 0x06000B2A RID: 2858 RVA: 0x00030284 File Offset: 0x0002E484
	public void SlotSeStop(int slot, int timeOut = 1000)
	{
		Debug.Assert(slot < this.slotSe.Length && slot >= 0);
		this.slotSe[slot].Stop(timeOut);
	}

	// Token: 0x06000B2B RID: 2859 RVA: 0x000302B4 File Offset: 0x0002E4B4
	public void SlotSeStopAll(int timeOut = 1000)
	{
		for (int num = 0; num != this.slotSe.Length; num++)
		{
			this.SlotSeStop(num, timeOut);
		}
	}

	// Token: 0x06000B2C RID: 2860 RVA: 0x000302E4 File Offset: 0x0002E4E4
	public void SlotSePauseAll(bool isPause)
	{
		foreach (SoundStream soundStream in this.slotSe)
		{
			soundStream.Pause(isPause);
		}
	}

	// Token: 0x06000B2D RID: 2861 RVA: 0x00030318 File Offset: 0x0002E518
	public void SlotSeSetVolume(int slot, float fVol)
	{
		this.slotSe[slot].Current.Volume = fVol;
	}

	// Token: 0x06000B2E RID: 2862 RVA: 0x00030330 File Offset: 0x0002E530
	public void SysPlayerSePlay(string name, float fVol = 1f)
	{
		this.player_se.FadeOutTime = 0;
		if (this.player_se.IsPlay)
		{
			this.player_se.Stop();
		}
		if (fVol > 0f)
		{
			this.player_se.SetSource("se", name);
			this.player_se.FadeInTime = 0;
			this.player_se.Loop = false;
			this.player_se.Volume = fVol;
			this.player_se.Play();
		}
	}

	// Token: 0x06000B2F RID: 2863 RVA: 0x000303B0 File Offset: 0x0002E5B0
	public bool SysPlayerSeIsPlay()
	{
		return this.player_se.IsPlay;
	}

	// Token: 0x06000B30 RID: 2864 RVA: 0x000303C0 File Offset: 0x0002E5C0
	public void SysSePlay(string name, float fVol = 1f)
	{
		if (this.SysSeVolume > 0f)
		{
			SoundData soundData;
			if (!this.data.TryGetValue(name, out soundData))
			{
				soundData = new SoundData("se", name);
			}
			this.se.Play(soundData, fVol * this.SysSeVolume);
		}
	}

	// Token: 0x06000B31 RID: 2865 RVA: 0x00030410 File Offset: 0x0002E610
	public void AddSeData(string name)
	{
		if (!this.data.ContainsKey(name))
		{
			this.data.Add(name, new SoundData("se", name));
		}
	}

	// Token: 0x06000B32 RID: 2866 RVA: 0x00030448 File Offset: 0x0002E648
	public void ClearSeData()
	{
		this.data.Clear();
	}

	// Token: 0x040008DD RID: 2269
	private SoundStream bgm;

	// Token: 0x040008DE RID: 2270
	private SoundStream voice;

	// Token: 0x040008DF RID: 2271
	private SoundVoice se;

	// Token: 0x040008E0 RID: 2272
	private SoundVoice player_se;

	// Token: 0x040008E1 RID: 2273
	private SoundStream[] slotSe = new SoundStream[2];

	// Token: 0x040008E2 RID: 2274
	private Dictionary<string, SoundData> data = new Dictionary<string, SoundData>();

	// Token: 0x040008E3 RID: 2275
	private float volBgm = 1f;

	// Token: 0x040008E4 RID: 2276
	private float volSe = 1f;

	// Token: 0x040008E5 RID: 2277
	private float volSysSe = 1f;
}
