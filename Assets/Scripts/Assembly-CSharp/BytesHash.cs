﻿using System;
using System.Security.Cryptography;
using System.Text;

// Token: 0x02000161 RID: 353
public class BytesHash
{
	// Token: 0x06000A29 RID: 2601 RVA: 0x0002D490 File Offset: 0x0002B690
	public static string CreateHashCode(byte[] data)
	{
		SHA1Managed sha1Managed = new SHA1Managed();
		byte[] array = sha1Managed.ComputeHash(data);
		StringBuilder stringBuilder = new StringBuilder();
		foreach (byte b in array)
		{
			stringBuilder.Append(b.ToString("X2"));
		}
		return stringBuilder.ToString();
	}
}
