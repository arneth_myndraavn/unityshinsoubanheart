﻿using System;
using System.Collections.Generic;
using Qoo.Game;
using UnityEngine;

// Token: 0x0200011E RID: 286
public class OptionScreenWindow : OptionBaseWindow
{
	// Token: 0x0600079C RID: 1948 RVA: 0x0002078C File Offset: 0x0001E98C
	private void SetLoveAnim(int n)
	{
		SysData.SetEnableLoveAnim(n == 1);
	}

	// Token: 0x0600079D RID: 1949 RVA: 0x00020798 File Offset: 0x0001E998
	private void SetDrawFace(int n)
	{
		SysData.SetDrawFace(n == 1);
	}

	// Token: 0x0600079E RID: 1950 RVA: 0x000207A4 File Offset: 0x0001E9A4
	private void UpdateCursor()
	{
		ImageObject imageObject = base.GetImageObject("LoveCursor");
		ImageObject imageObject2 = base.GetImageObject("FaceCursor");
		imageObject.UIObject.ScreenX = ((!SysData.IsEnableLoveAnim()) ? 526 : 314);
		imageObject2.UIObject.ScreenX = ((!SysData.IsDrawFace()) ? 526 : 314);
		imageObject.InitPosition();
		imageObject2.InitPosition();
	}

	// Token: 0x0600079F RID: 1951 RVA: 0x00020820 File Offset: 0x0001EA20
	protected override OptionBaseWindow.OptionType GetOptionType()
	{
		return OptionBaseWindow.OptionType.Screen;
	}

	// Token: 0x060007A0 RID: 1952 RVA: 0x00020824 File Offset: 0x0001EA24
	protected sealed override string[] newSceneTextureNameArray()
	{
		string[] collection = base.newSceneTextureNameArray();
		string[] collection2 = new string[]
		{
			"screen/sysmenu/sys_plate",
			"screen/sysmenu/sys_btn_10",
			"screen/sysmenu/sys_btn_11",
			"screen/sysmenu/sys_plate_love",
			"screen/sysmenu/sys_plate_face",
			"screen/sysmenu/sys_check",
			"screen/common/cancel"
		};
		List<string> list = new List<string>();
		list.AddRange(collection);
		list.AddRange(collection2);
		return list.ToArray();
	}

	// Token: 0x060007A1 RID: 1953 RVA: 0x00020894 File Offset: 0x0001EA94
	protected sealed override void BeforeInit()
	{
		Vector3 localPosition = base.transform.parent.localPosition;
		localPosition.z = -50f;
		base.transform.parent.localPosition = localPosition;
	}

	// Token: 0x060007A2 RID: 1954 RVA: 0x000208D0 File Offset: 0x0001EAD0
	protected sealed override void AfterInit()
	{
		this.UpdateCursor();
	}

	// Token: 0x060007A3 RID: 1955 RVA: 0x000208D8 File Offset: 0x0001EAD8
	protected override void OnGraphicLoadComplete()
	{
		base.OnGraphicLoadComplete();
	}

	// Token: 0x060007A4 RID: 1956 RVA: 0x000208E0 File Offset: 0x0001EAE0
	protected sealed override BaseWindow.UIComponent[] newComponentArray()
	{
		BaseWindow.UIComponent[] collection = base.newComponentArray();
		BaseWindow.UIComponent[] collection2 = new BaseWindow.UIComponent[]
		{
			new BaseWindow.UICollision("Close", 0, 0, this.wndz, 960, 544),
			new BaseWindow.UICollision("LoveON", 321, 178, this.wndz + 4, 75, 34),
			new BaseWindow.UICollision("LoveOFF", 533, 178, this.wndz + 4, 88, 34),
			new BaseWindow.UICollision("FaceON", 321, 300, this.wndz + 4, 75, 34),
			new BaseWindow.UICollision("FaceOFF", 533, 300, this.wndz + 4, 88, 34),
			new BaseWindow.UIImage("BackGround", 153, 68, this.wndz + 1, "screen/sysmenu/sys_plate", true, true),
			new BaseWindow.UIButton("Clear", 208, 380, this.wndz + 2, "screen/sysmenu/sys_btn_10", true, true, 1, 2, 0),
			new BaseWindow.UIButton("Decide", 686, 380, this.wndz + 2, "screen/sysmenu/sys_btn_11", true, true, 1, 2, 0),
			new BaseWindow.UIButton("Cancel", 715, 73, this.wndz + 2, "screen/common/cancel", true, true, 1, 2, 0),
			new BaseWindow.UIImage("Love", 225, 129, this.wndz + 2, "screen/sysmenu/sys_plate_love", false, true),
			new BaseWindow.UIImage("Face", 225, 251, this.wndz + 2, "screen/sysmenu/sys_plate_face", false, true),
			new BaseWindow.UIImage("LoveCursor", 314, 182, this.wndz + 3, "screen/sysmenu/sys_check", false, true),
			new BaseWindow.UIImage("FaceCursor", 314, 304, this.wndz + 3, "screen/sysmenu/sys_check", false, true)
		};
		List<BaseWindow.UIComponent> list = new List<BaseWindow.UIComponent>();
		list.AddRange(collection);
		list.AddRange(collection2);
		return list.ToArray();
	}

	// Token: 0x060007A5 RID: 1957 RVA: 0x00020AF8 File Offset: 0x0001ECF8
	protected sealed override void OnBaseWindowOnButton(string obj)
	{
		base.OnBaseWindowOnButton(obj);
		switch (obj)
		{
		case "LoveOFF":
			base.PlaySE_Select();
			this.SetLoveAnim(0);
			break;
		case "LoveON":
			base.PlaySE_Select();
			this.SetLoveAnim(1);
			break;
		case "FaceOFF":
			base.PlaySE_Select();
			this.SetDrawFace(0);
			break;
		case "FaceON":
			base.PlaySE_Select();
			this.SetDrawFace(1);
			break;
		}
		this.UpdateCursor();
	}

	// Token: 0x040006CA RID: 1738
	private readonly int wndz;
}
