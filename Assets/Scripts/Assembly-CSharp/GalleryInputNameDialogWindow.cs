﻿using System;
using Qoo.Game;
using UnityEngine;

// Token: 0x0200010A RID: 266
public class GalleryInputNameDialogWindow : BaseWindow
{
	// Token: 0x06000707 RID: 1799 RVA: 0x0001DE20 File Offset: 0x0001C020
	protected sealed override void OnAwake()
	{
		this.type = UIValue.GalleryInputNameDialog_Type;
		this.state = ((this.type != GalleryInputNameDialogType.CONFIRM) ? GalleryInputNameDialogWindow.STATE.INIT : GalleryInputNameDialogWindow.STATE.INVALID);
		this.elapsed = 0f;
	}

	// Token: 0x06000708 RID: 1800 RVA: 0x0001DE54 File Offset: 0x0001C054
	protected sealed override string[] newSceneTextureNameArray()
	{
		switch (this.type)
		{
		case GalleryInputNameDialogType.EMPTY:
		case GalleryInputNameDialogType.OVER:
		case GalleryInputNameDialogType.INVALIDCHAR:
			return new string[]
			{
				"screen/common/sys_dialog"
			};
		case GalleryInputNameDialogType.CONFIRM:
			return new string[]
			{
				"screen/common/sys_dialog",
				"screen/common/dlog_btn_y",
				"screen/common/dlog_btn_n"
			};
		default:
			return base.newSceneTextureNameArray();
		}
	}

	// Token: 0x06000709 RID: 1801 RVA: 0x0001DEC0 File Offset: 0x0001C0C0
	protected sealed override BaseWindow.UIComponent[] newComponentArray()
	{
		switch (this.type)
		{
		case GalleryInputNameDialogType.EMPTY:
		case GalleryInputNameDialogType.OVER:
		case GalleryInputNameDialogType.INVALIDCHAR:
			return new BaseWindow.UIComponent[]
			{
				new BaseWindow.UICollision("Block", 0, 0, this.wndz, 960, 544),
				new BaseWindow.UIImage("Dialog", this.msgwndx, this.msgwndy, this.wndz + 1, "screen/common/sys_dialog", false, true),
				new BaseWindow.UIText("Text", this.textx, this.texty, this.wndz + 2, this.GetText(0), 22, UnityTextSprite.PositionType.Center, UnityTextSprite.PositionType.TopLeft, byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue)
			};
		case GalleryInputNameDialogType.CONFIRM:
		{
			int num = 22;
			return new BaseWindow.UIComponent[]
			{
				new BaseWindow.UICollision("Block", 0, 0, this.wndz, 960, 544),
				new BaseWindow.UIImage("Dialog", this.msgwndx, this.msgwndy, this.wndz + 1, "screen/common/sys_dialog", false, true),
				new BaseWindow.UIButton("ButtonY", this.ybtnx, this.btny, this.wndz + 2, "screen/common/dlog_btn_y", true, true, 1, 2, 0),
				new BaseWindow.UIButton("ButtonN", this.nbtnx, this.btny, this.wndz + 2, "screen/common/dlog_btn_n", true, true, 1, 2, 0),
				new BaseWindow.UIText("Text0", this.textx, this.texty + num * 0, this.wndz + 2, this.GetText(0), 22, UnityTextSprite.PositionType.Center, UnityTextSprite.PositionType.TopLeft, byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue),
				new BaseWindow.UIText("Text1", this.textx, this.texty + num * 1, this.wndz + 2, this.GetText(1), 22, UnityTextSprite.PositionType.Center, UnityTextSprite.PositionType.TopLeft, byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue)
			};
		}
		default:
			return base.newComponentArray();
		}
	}

	// Token: 0x0600070A RID: 1802 RVA: 0x0001E0B8 File Offset: 0x0001C2B8
	protected sealed override void OnBaseWindowUpdate()
	{
		switch (this.state)
		{
		case GalleryInputNameDialogWindow.STATE.INIT:
			this.state = GalleryInputNameDialogWindow.STATE.WAIT;
			this.elapsed = 0f;
			break;
		case GalleryInputNameDialogWindow.STATE.WAIT:
			this.elapsed += Time.deltaTime;
			if (this.elapsed > 1f)
			{
				this.state = GalleryInputNameDialogWindow.STATE.EXIT;
				UIValue.GalleryInputNameDialog_Exit = GalleryInputNameDialogExitType.RETRY;
				base.DeleteLastAddScene();
			}
			break;
		}
	}

	// Token: 0x0600070B RID: 1803 RVA: 0x0001E144 File Offset: 0x0001C344
	protected sealed override void OnBaseWindowOnButton(string obj)
	{
		switch (obj)
		{
		case "ButtonY":
			base.PlaySE_Ok();
			UIValue.GalleryInputNameDialog_Exit = GalleryInputNameDialogExitType.YES;
			base.DeleteLastAddScene();
			break;
		case "ButtonN":
			base.PlaySE_Cancel();
			UIValue.GalleryInputNameDialog_Exit = GalleryInputNameDialogExitType.NO;
			base.DeleteLastAddScene();
			break;
		case "Close":
			base.PlaySE_Cancel();
			UIValue.GalleryInputNameDialog_Exit = GalleryInputNameDialogExitType.RETRY;
			base.DeleteLastAddScene();
			break;
		}
	}

	// Token: 0x0600070C RID: 1804 RVA: 0x0001E204 File Offset: 0x0001C404
	private string GetText(int line)
	{
		switch (this.type)
		{
		case GalleryInputNameDialogType.EMPTY:
			return UnityApp.Instance.isJapanese ? "名前が入力されていません。" : "No name entered";
		case GalleryInputNameDialogType.OVER:
			return UnityApp.Instance.isJapanese ? "入力できる名前は７文字までです。" : "You can enter up to 7 characters";
		case GalleryInputNameDialogType.INVALIDCHAR:
			return UnityApp.Instance.isJapanese ? "表示できない文字が含まれています。" : "Input contains characters that cannot be displayed";
		case GalleryInputNameDialogType.CONFIRM:
                if (UnityApp.Instance.isJapanese)
                {
					if (line == 0)
					{
						string galleryInputNameDialog_Name = UIValue.GalleryInputNameDialog_Name;
						string paramString = GameData.GetParamString("familyname", string.Empty);
						return string.Format("「{0}＝{1}」です。", galleryInputNameDialog_Name, paramString);
					}
					if (line != 1)
					{
						return string.Empty;
					}
					return "よろしいですか？";

				}
                else
                {
					if (line == 0)
					{
						string galleryInputNameDialog_Name = UIValue.GalleryInputNameDialog_Name;
                        string galleryInputNameDialog_LastName = UIValue.GalleryInputNameDialog_LastName;
                        //string paramString = GameData.GetParamString("familyname", string.Empty);
						return string.Format("Is {0} {1} correct?", galleryInputNameDialog_Name, galleryInputNameDialog_LastName);
					}
					if (line != 1)
					{
						return string.Empty;
					}
					return string.Empty;
				}
			
		default:
			return string.Empty;
		}
	}

	// Token: 0x04000680 RID: 1664
	private GalleryInputNameDialogType type;

	// Token: 0x04000681 RID: 1665
	private readonly int msgwndx = 136;

	// Token: 0x04000682 RID: 1666
	private readonly int msgwndy = 182;

	// Token: 0x04000683 RID: 1667
	private readonly int ybtnx = 312;

	// Token: 0x04000684 RID: 1668
	private readonly int nbtnx = 513;

	// Token: 0x04000685 RID: 1669
	private readonly int btny = 277;

	// Token: 0x04000686 RID: 1670
	private readonly int wndz = 10;

	// Token: 0x04000687 RID: 1671
	private readonly int textx = 480;

	// Token: 0x04000688 RID: 1672
	private readonly int texty = 222;

	// Token: 0x04000689 RID: 1673
	private GalleryInputNameDialogWindow.STATE state;

	// Token: 0x0400068A RID: 1674
	private float elapsed;

	// Token: 0x0200010B RID: 267
	private enum STATE
	{
		// Token: 0x0400068D RID: 1677
		INVALID,
		// Token: 0x0400068E RID: 1678
		INIT,
		// Token: 0x0400068F RID: 1679
		WAIT,
		// Token: 0x04000690 RID: 1680
		EXIT
	}
}
