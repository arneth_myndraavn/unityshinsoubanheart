﻿using System;

// Token: 0x020000F8 RID: 248
public enum SoundOptionType
{
	// Token: 0x0400060C RID: 1548
	BGM,
	// Token: 0x0400060D RID: 1549
	SE,
	// Token: 0x0400060E RID: 1550
	SYSTEM
}
