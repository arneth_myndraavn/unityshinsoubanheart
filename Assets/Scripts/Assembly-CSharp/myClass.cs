﻿using System;
using System.Collections.Generic;

// Token: 0x02000006 RID: 6
[Serializable]
public class myClass
{
	// Token: 0x04000009 RID: 9
	public myClassChild child;

	// Token: 0x0400000A RID: 10
	public int a;

	// Token: 0x0400000B RID: 11
	public float f;

	// Token: 0x0400000C RID: 12
	public string b;

	// Token: 0x0400000D RID: 13
	public char c;

	// Token: 0x0400000E RID: 14
	public string[] strArray = new string[]
	{
		"a",
		"b",
		"c"
	};

	// Token: 0x0400000F RID: 15
	public List<string> strList;
}
