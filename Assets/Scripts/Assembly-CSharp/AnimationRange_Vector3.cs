﻿using System;
using UnityEngine;

// Token: 0x02000195 RID: 405
public class AnimationRange_Vector3
{
	// Token: 0x06000B98 RID: 2968 RVA: 0x00031278 File Offset: 0x0002F478
	public AnimationRange_Vector3(int repeat, float speed, float delay, MoveType moveType, Vector3 begin, Vector3 end)
	{
		this.animationRange = new AnimationRange(repeat, speed, delay, moveType);
		this.offsetBegin = begin;
		this.offsetEnd = end;
		this.offset = this.offsetBegin;
	}

	// Token: 0x1700018C RID: 396
	// (get) Token: 0x06000B99 RID: 2969 RVA: 0x000312B8 File Offset: 0x0002F4B8
	public Vector3 Offset
	{
		get
		{
			return this.offset;
		}
	}

	// Token: 0x1700018D RID: 397
	// (get) Token: 0x06000B9A RID: 2970 RVA: 0x000312C0 File Offset: 0x0002F4C0
	public bool Active
	{
		get
		{
			return this.animationRange.Active;
		}
	}

	// Token: 0x1700018E RID: 398
	// (get) Token: 0x06000B9B RID: 2971 RVA: 0x000312D0 File Offset: 0x0002F4D0
	// (set) Token: 0x06000B9C RID: 2972 RVA: 0x000312E0 File Offset: 0x0002F4E0
	public float Speed
	{
		get
		{
			return this.animationRange.Speed;
		}
		set
		{
			this.animationRange.Speed = value;
		}
	}

	// Token: 0x1700018F RID: 399
	// (get) Token: 0x06000B9D RID: 2973 RVA: 0x000312F0 File Offset: 0x0002F4F0
	// (set) Token: 0x06000B9E RID: 2974 RVA: 0x000312F8 File Offset: 0x0002F4F8
	public Vector3 OffsetBegin
	{
		get
		{
			return this.offsetBegin;
		}
		set
		{
			this.offsetBegin = value;
		}
	}

	// Token: 0x17000190 RID: 400
	// (get) Token: 0x06000B9F RID: 2975 RVA: 0x00031304 File Offset: 0x0002F504
	// (set) Token: 0x06000BA0 RID: 2976 RVA: 0x0003130C File Offset: 0x0002F50C
	public Vector3 OffsetEnd
	{
		get
		{
			return this.offsetEnd;
		}
		set
		{
			this.offsetEnd = value;
		}
	}

	// Token: 0x06000BA1 RID: 2977 RVA: 0x00031318 File Offset: 0x0002F518
	public void Update()
	{
		this.animationRange.Update();
		if (this.animationRange.Active)
		{
			float rate = this.animationRange.Rate;
			this.offset = this.offsetBegin * (1f - rate) + this.offsetEnd * rate;
		}
		else
		{
			this.offset = this.offsetEnd;
		}
	}

	// Token: 0x06000BA2 RID: 2978 RVA: 0x00031388 File Offset: 0x0002F588
	public void Restart()
	{
		this.animationRange.Restart();
	}

	// Token: 0x04000919 RID: 2329
	private AnimationRange animationRange;

	// Token: 0x0400091A RID: 2330
	private Vector3 offsetBegin;

	// Token: 0x0400091B RID: 2331
	private Vector3 offsetEnd;

	// Token: 0x0400091C RID: 2332
	private Vector3 offset;
}
