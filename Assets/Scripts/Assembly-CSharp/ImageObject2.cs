﻿using System;
using UnityEngine;

// Token: 0x0200019A RID: 410
public class ImageObject2
{
	// Token: 0x06000BCE RID: 3022 RVA: 0x00031CEC File Offset: 0x0002FEEC
	public ImageObject2(GameObject go, int x, int y, int z, UnityTexture uniTex)
	{
		this.m_GameObject = go;
		this.SetTexture(uniTex, true);
		this.SetPosition(x, y, z);
	}

	// Token: 0x1700019A RID: 410
	// (get) Token: 0x06000BCF RID: 3023 RVA: 0x00031D1C File Offset: 0x0002FF1C
	public int X
	{
		get
		{
			return this.m_X;
		}
	}

	// Token: 0x1700019B RID: 411
	// (get) Token: 0x06000BD0 RID: 3024 RVA: 0x00031D24 File Offset: 0x0002FF24
	public int Y
	{
		get
		{
			return this.m_Y;
		}
	}

	// Token: 0x1700019C RID: 412
	// (get) Token: 0x06000BD1 RID: 3025 RVA: 0x00031D2C File Offset: 0x0002FF2C
	public int Z
	{
		get
		{
			return this.m_Z;
		}
	}

	// Token: 0x1700019D RID: 413
	// (get) Token: 0x06000BD2 RID: 3026 RVA: 0x00031D34 File Offset: 0x0002FF34
	public int Width
	{
		get
		{
			return this.m_Width;
		}
	}

	// Token: 0x1700019E RID: 414
	// (get) Token: 0x06000BD3 RID: 3027 RVA: 0x00031D3C File Offset: 0x0002FF3C
	public int Height
	{
		get
		{
			return this.m_Height;
		}
	}

	// Token: 0x1700019F RID: 415
	// (get) Token: 0x06000BD4 RID: 3028 RVA: 0x00031D44 File Offset: 0x0002FF44
	public Vector4 UVWH
	{
		get
		{
			return this.m_UVWH;
		}
	}

	// Token: 0x170001A0 RID: 416
	// (get) Token: 0x06000BD5 RID: 3029 RVA: 0x00031D4C File Offset: 0x0002FF4C
	// (set) Token: 0x06000BD6 RID: 3030 RVA: 0x00031D60 File Offset: 0x0002FF60
	public bool ColliderEnabled
	{
		get
		{
			return this.m_GameObject.GetComponent<Collider>().enabled;
		}
		set
		{
			this.m_GameObject.GetComponent<Collider>().enabled = value;
		}
	}

	// Token: 0x170001A1 RID: 417
	// (get) Token: 0x06000BD7 RID: 3031 RVA: 0x00031D74 File Offset: 0x0002FF74
	// (set) Token: 0x06000BD8 RID: 3032 RVA: 0x00031D88 File Offset: 0x0002FF88
	public bool RendererEnabled
	{
		get
		{
			return this.m_GameObject.GetComponent<Renderer>().enabled;
		}
		set
		{
			this.m_GameObject.GetComponent<Collider>().enabled = value;
		}
	}

	// Token: 0x170001A2 RID: 418
	// (get) Token: 0x06000BD9 RID: 3033 RVA: 0x00031D9C File Offset: 0x0002FF9C
	public bool HasTexture
	{
		get
		{
			return this.m_UniTex != null && this.m_UniTex.IsInit;
		}
	}

	// Token: 0x06000BDA RID: 3034 RVA: 0x00031DB8 File Offset: 0x0002FFB8
	public static ImageObject2 Create(Transform parent, string name, int x, int y, int z, UnityTexture uniTex)
	{
		GameObject gameObject = GameObject.CreatePrimitive(PrimitiveType.Quad);
		//gameObject.renderer.material = new Material(Resources.Load("Shader/Sprite/Sprite") as Shader);
		gameObject.GetComponent<Renderer>().material = new Material(Shader.Find("QO/Sprite") as Shader);
		gameObject.name = name;
		gameObject.transform.parent = parent;
		return new ImageObject2(gameObject, x, y, z, uniTex);
	}

	// Token: 0x06000BDB RID: 3035 RVA: 0x00031E0C File Offset: 0x0003000C
	public void Hidden(bool hidden)
	{
		this.m_GameObject.GetComponent<Renderer>().enabled = !hidden;
		this.m_GameObject.GetComponent<Collider>().enabled = !hidden;
	}

	// Token: 0x06000BDC RID: 3036 RVA: 0x00031E44 File Offset: 0x00030044
	public void Enabled(bool enabled)
	{
		this.m_GameObject.SetActive(enabled);
	}

	// Token: 0x06000BDD RID: 3037 RVA: 0x00031E54 File Offset: 0x00030054
	public T AddComponent<T>() where T : MonoBehaviour
	{
		return this.m_GameObject.AddComponent<T>();
	}

	// Token: 0x06000BDE RID: 3038 RVA: 0x00031E64 File Offset: 0x00030064
	public void RemoveComponent<T>() where T : MonoBehaviour
	{
		UnityEngine.Object.Destroy(this.m_GameObject.GetComponent<T>());
	}

	// Token: 0x06000BDF RID: 3039 RVA: 0x00031E7C File Offset: 0x0003007C
	public T GetComponent<T>() where T : MonoBehaviour
	{
		return this.m_GameObject.GetComponent<T>();
	}

	// Token: 0x06000BE0 RID: 3040 RVA: 0x00031E8C File Offset: 0x0003008C
	public void ReloadTexture(bool fixedTexture = true)
	{
		if (this.HasTexture)
		{
			this.SetTexture(this.m_UniTex, fixedTexture);
		}
	}

	// Token: 0x06000BE1 RID: 3041 RVA: 0x00031EA8 File Offset: 0x000300A8
	public void SetTexture(UnityTexture uniTex, bool fixedTexture = true)
	{
		this.m_UniTex = uniTex;
		if (uniTex == null)
		{
			return;
		}
		this.m_GameObject.GetComponent<Renderer>().material.mainTexture = this.m_UniTex.m_Texture;
		if (fixedTexture)
		{
			this.FixedTexture();
		}
	}

	// Token: 0x06000BE2 RID: 3042 RVA: 0x00031EF0 File Offset: 0x000300F0
	public void SetTextureUVWH(float u = 0f, float v = 0f, float w = 1f, float h = 1f)
	{
		this.SetTextureUVWH(new Vector4(u, v, w, h));
	}

	// Token: 0x06000BE3 RID: 3043 RVA: 0x00031F04 File Offset: 0x00030104
	public void SetTextureUVWH(Vector4 uvwh)
	{
		this.m_UVWH = uvwh;
		this.m_GameObject.GetComponent<Renderer>().material.SetVector("_UVWH", this.m_UVWH);
	}

	// Token: 0x06000BE4 RID: 3044 RVA: 0x00031F30 File Offset: 0x00030130
	public void Move(Vector3 vec)
	{
		this.Move((int)vec.x, (int)vec.y, (int)vec.z);
	}

	// Token: 0x06000BE5 RID: 3045 RVA: 0x00031F50 File Offset: 0x00030150
	public void Move(int x, int y, int z)
	{
		int x2 = this.m_X + x;
		int y2 = this.m_Y + y;
		int z2 = this.m_Z + z;
		this.SetPosition(x2, y2, z2);
	}

	// Token: 0x06000BE6 RID: 3046 RVA: 0x00031F84 File Offset: 0x00030184
	public void SetPosition(Vector3 pos)
	{
		this.SetPosition((int)pos.x, (int)pos.y, (int)pos.z);
	}

	// Token: 0x06000BE7 RID: 3047 RVA: 0x00031FA4 File Offset: 0x000301A4
	public void SetPosition(int x, int y, int z)
	{
		this.m_X = x;
		this.m_Y = y;
		this.m_Z = z;
		this.UpdatePosition();
	}

	// Token: 0x06000BE8 RID: 3048 RVA: 0x00031FC4 File Offset: 0x000301C4
	public void UpdatePosition()
	{
		int num = -480;
		int num2 = 272;
		Vector3 localPosition = new Vector3(0f, 0f, 0f);
		localPosition.x = (float)(num + this.Width / 2 - this.X);
		localPosition.y = (float)(num2 - this.Height / 2 - this.Y);
		localPosition.z = (float)this.Z;
		this.m_GameObject.transform.localPosition = localPosition;
	}

	// Token: 0x06000BE9 RID: 3049 RVA: 0x00032044 File Offset: 0x00030244
	public virtual void FixedTexture()
	{
		if (this.HasTexture)
		{
			this.SetSize(this.m_UniTex.m_Texture.width, this.m_UniTex.m_Texture.height);
		}
	}

	// Token: 0x06000BEA RID: 3050 RVA: 0x00032084 File Offset: 0x00030284
	public void SetSize(int width, int height)
	{
		this.m_Width = width;
		this.m_Height = height;
		this.UpdateScale();
	}

	// Token: 0x06000BEB RID: 3051 RVA: 0x0003209C File Offset: 0x0003029C
	public void UpdateScale()
	{
		Vector3 localScale = new Vector3((float)this.m_Width, (float)this.m_Height, 1f);
		this.m_GameObject.transform.localScale = localScale;
	}

	// Token: 0x06000BEC RID: 3052 RVA: 0x000320D4 File Offset: 0x000302D4
	public void Destroy()
	{
		UnityEngine.Object.Destroy(this.m_GameObject);
	}

	// Token: 0x04000930 RID: 2352
	private GameObject m_GameObject;

	// Token: 0x04000931 RID: 2353
	private int m_X;

	// Token: 0x04000932 RID: 2354
	private int m_Y;

	// Token: 0x04000933 RID: 2355
	private int m_Z;

	// Token: 0x04000934 RID: 2356
	private int m_Width;

	// Token: 0x04000935 RID: 2357
	private int m_Height;

	// Token: 0x04000936 RID: 2358
	private UnityTexture m_UniTex;

	// Token: 0x04000937 RID: 2359
	private Vector4 m_UVWH;
}
