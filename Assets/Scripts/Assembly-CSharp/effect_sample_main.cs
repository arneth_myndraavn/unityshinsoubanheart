﻿using System;
using System.Collections;
using UnityEngine;

// Token: 0x02000003 RID: 3
public class effect_sample_main : MonoBehaviour
{
	// Token: 0x06000006 RID: 6 RVA: 0x00002138 File Offset: 0x00000338
	private IEnumerator Start()
	{
		this.idx = 0;
		yield return base.StartCoroutine(this.loadAssetBundle());
		GameObject panel = GameObject.Find("Panel");
		panel.GetComponent<Renderer>().material.mainTexture = this.fromTex;
		for (;;)
		{
			EffectManager.Init(this.ManageEffects[this.idx], this.transTime, panel.GetComponent<Renderer>(), this.fromTex, this.toTex);
			yield return new WaitForSeconds(1f);
			yield return base.StartCoroutine(EffectManager.StartEffect());
			this.idx++;
			this.idx = (this.idx + this.ManageEffects.Length) % this.ManageEffects.Length;
		}
		yield break;
	}

	// Token: 0x06000007 RID: 7 RVA: 0x00002154 File Offset: 0x00000354
	private IEnumerator loadAssetBundle()
	{
		for (int i = 0; i < 2; i++)
		{
			string fname = string.Empty;
			if (i == 0)
			{
				fname = "bg_001_map_a";
			}
			else
			{
				fname = "bg_003_hmap_a";
			}
			string abname = Pathing.ToPlatformAssetBundleName(fname);
			WWW www = new WWW(Pathing.appContentDataPath + "/bytes/event/bgimage/" + abname);
			yield return www;
			AssetBundle ab = www.assetBundle;
			TextAsset ta = ab.mainAsset as TextAsset;
			Texture2D tex = new Texture2D(0, 0, TextureFormat.RGBA32, false, true);
			tex.LoadImage(ta.bytes);
			tex.wrapMode = TextureWrapMode.Clamp;
			ab.Unload(true);
			www.Dispose();
			if (i == 0)
			{
				this.fromTex = tex;
			}
			else
			{
				this.toTex = tex;
			}
		}
		yield break;
	}

	// Token: 0x04000001 RID: 1
	private int idx;

	// Token: 0x04000002 RID: 2
	public string[] ManageEffects;

	// Token: 0x04000003 RID: 3
	public int transTime = 2000;

	// Token: 0x04000004 RID: 4
	public TextMesh tm;

	// Token: 0x04000005 RID: 5
	private Texture2D fromTex;

	// Token: 0x04000006 RID: 6
	private Texture2D toTex;
}
