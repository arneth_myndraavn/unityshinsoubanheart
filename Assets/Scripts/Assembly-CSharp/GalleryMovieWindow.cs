﻿using System;
using System.Collections;
using Qoo;
using UnityEngine;

// Token: 0x0200010F RID: 271
public class GalleryMovieWindow : BaseWindow
{
	// Token: 0x06000720 RID: 1824 RVA: 0x0001E7D0 File Offset: 0x0001C9D0
	protected sealed override string GetBGMName()
	{
		return string.Empty;
	}

	// Token: 0x06000721 RID: 1825 RVA: 0x0001E7D8 File Offset: 0x0001C9D8
	protected sealed override void OnAwake()
	{
		this.state = GalleryMovieWindow.STATE.INIT;
	}

	// Token: 0x06000722 RID: 1826 RVA: 0x0001E7E4 File Offset: 0x0001C9E4
	protected override void OnBaseWindowUpdate()
	{
		switch (this.state)
		{
		case GalleryMovieWindow.STATE.INIT:
			base.StartCoroutine(this.PlayMovie());
			this.state = GalleryMovieWindow.STATE.PLAYING;
			break;
		case GalleryMovieWindow.STATE.END:
			Qoo.Debug.Print("＊＊＊ムービー再生おわた＊＊＊");
			SceneManager.BackScene();
			this.state = GalleryMovieWindow.STATE.EXIT;
			break;
		}
	}

	// Token: 0x06000723 RID: 1827 RVA: 0x0001E854 File Offset: 0x0001CA54
	private IEnumerator PlayMovie()
	{
		//TODO fix movie playing. either make platform specific versions OR make platform agnostic
		//yield return base.StartCoroutine(MovieManager.PlayMovie("mp4/high/op_movie.mp4", FullScreenMovieControlMode.CancelOnInput));
		yield return base.StartCoroutine(MovieManager.PlayMovie("mp4/high/op_movie.webm", this.gameObject));
		this.state = GalleryMovieWindow.STATE.END;
		yield break;
	}

	// Token: 0x04000697 RID: 1687
	private GalleryMovieWindow.STATE state;

	// Token: 0x02000110 RID: 272
	private enum STATE
	{
		// Token: 0x04000699 RID: 1689
		INIT,
		// Token: 0x0400069A RID: 1690
		PLAYING,
		// Token: 0x0400069B RID: 1691
		END,
		// Token: 0x0400069C RID: 1692
		EXIT
	}
}
