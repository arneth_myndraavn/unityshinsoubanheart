﻿
using System;
using System.Collections;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

// Token: 0x02000174 RID: 372
public class ScreenKeyboardManager : MonoBehaviour
{
	// Token: 0x1700015A RID: 346
	// (get) Token: 0x06000A93 RID: 2707 RVA: 0x0002EA38 File Offset: 0x0002CC38
	private static ScreenKeyboardManager Instance
	{
		get
		{
			if (ScreenKeyboardManager.m_instance == null)
			{
				GameObject gameObject = GameObject.Find("_ScreenKeyboardManager");
				if (gameObject == null)
				{
					gameObject = new GameObject("_ScreenKeyboardManager");
				}
				ScreenKeyboardManager.m_instance = gameObject.AddComponent<ScreenKeyboardManager>();
				gameObject.AddComponent<EventSystem>();
				gameObject.AddComponent<StandaloneInputModule>();
				ScreenKeyboardManager.m_instance.m_inputText = string.Empty;
                ScreenKeyboardManager.m_instance.m_inputTextLastName = string.Empty;
                ScreenKeyboardManager.m_instance.isKeyboardActive = true;
            }
			return ScreenKeyboardManager.m_instance;
		}
	}

	// Token: 0x1700015B RID: 347
	// (get) Token: 0x06000A94 RID: 2708 RVA: 0x0002EA98 File Offset: 0x0002CC98
	public static string InputText
	{
		get
		{
			return ScreenKeyboardManager.Instance.m_inputText;
		}
	}

	// Token: 0x1700015C RID: 348
	// (set) Token: 0x06000A95 RID: 2709 RVA: 0x0002EAA4 File Offset: 0x0002CCA4
	public static string DebugInputText
	{
		set
		{
			ScreenKeyboardManager.Instance.m_inputText = value;
		}
	}

    // Token: 0x1700015B RID: 347
    // (get) Token: 0x06000A94 RID: 2708 RVA: 0x0002EA98 File Offset: 0x0002CC98
    public static string InputTextLastName
    {
        get
        {
            return ScreenKeyboardManager.Instance.m_inputTextLastName;
        }
    }

    // Token: 0x1700015C RID: 348
    // (set) Token: 0x06000A95 RID: 2709 RVA: 0x0002EAA4 File Offset: 0x0002CCA4
    public static string DebugInputTextLastName
    {
        set
        {
            ScreenKeyboardManager.Instance.m_inputTextLastName = value;
        }
    }

    // Token: 0x1700015D RID: 349
    // (get) Token: 0x06000A96 RID: 2710 RVA: 0x0002EAB4 File Offset: 0x0002CCB4
    public static bool IsCancel
	{
		get
		{
			return false;
			//return ScreenKeyboardManager.Instance.m_keyboard.wasCanceled;
		}
	}

	// Token: 0x06000A97 RID: 2711 RVA: 0x0002EAC8 File Offset: 0x0002CCC8
	public static IEnumerator Open(string defaultString = "", string defaultString2 = "")
	{
        //ScreenKeyboardManager.m_instance.isKeyboardActive = true;
        //Debug.LogWarning("Name: " + defaultString + " = " + defaultString2);
		GameObject manager = ScreenKeyboardManager.Instance.gameObject;

		Canvas canvas = manager.AddComponent<Canvas>();
		CanvasScaler scaler = canvas.gameObject.AddComponent<CanvasScaler>();
		scaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
		canvas.gameObject.AddComponent<GraphicRaycaster>();
		canvas.renderMode = RenderMode.ScreenSpaceOverlay;

        InputField _inputFieldFirstName = createInputBox(canvas, "FirstName", defaultString, "Input First Name", new Vector3(-250, 50, 0));
        InputField _inputFieldLastName = createInputBox(canvas, "LastName", defaultString2, "Input Last Name", new Vector3(0, 50, 0));

        createEqualsBox(canvas, "Equals", new Vector3(-125, 50, 0));
        createButton(canvas, "SubmitButton", new Vector3(-125, -35, 0));

        string firstNameString = _inputFieldFirstName.text;
        string lastNameString = _inputFieldLastName.text;

        while (ScreenKeyboardManager.m_instance.isKeyboardActive)
		{
			yield return 0;
			/*if (Input.GetKeyDown(KeyCode.Return))
			{
                firstNameString = _inputFieldFirstName.text;
                lastNameString = _inputFieldLastName.text;
                ScreenKeyboardManager.Instance.m_inputText = firstNameString;
                ScreenKeyboardManager.Instance.m_inputTextLastName = lastNameString;
                GameObject.Destroy(canvas.gameObject);
				isKeyboardActive = false;

			}*/
		}
		yield break;
	}

	public static InputField createInputBox(Canvas canvas, string objectName, string defaultString, string placeholderString, Vector3 position)
    {
        int width = 200;
        int height = 50;
        int fontSize = 24;
        Color fontColor = Color.white;
        Color inputBoxColor = new Color(87f / 255f, 48f / 255f, 55f / 255f, 255f / 255f);

        GameObject container = new GameObject();
        container.name = objectName;
        container.transform.parent = canvas.transform;
        container.AddComponent<CanvasRenderer>();

        Image _image = container.AddComponent<Image>();
        //_image.sprite = UnityEditor.AssetDatabase.GetBuiltinExtraResource<Sprite>("UI/Skin/UISprite.psd");
        _image.type = Image.Type.Sliced;
        _image.color = inputBoxColor;

        Font font = (Font)Resources.Load("font/Cabin");

        GameObject textContainer = new GameObject();
        textContainer.name = "Text";
        textContainer.transform.parent = container.transform;
        container.transform.localScale = new Vector3(1, 1, 1);
        Text _text = textContainer.AddComponent<Text>();
        RectTransform _textTransform = _text.GetComponent<RectTransform>();
        _textTransform.sizeDelta = new Vector2(width, height);
        _text.supportRichText = false;
        _text.color = fontColor;
        _text.font = font;
        _text.fontSize = fontSize;
        _text.horizontalOverflow = HorizontalWrapMode.Overflow;
        _text.alignment = TextAnchor.MiddleCenter;

        GameObject placeholderContainer = new GameObject();
        placeholderContainer.name = "Placeholder";
        placeholderContainer.transform.parent = container.transform;
        Text _placeholder = placeholderContainer.AddComponent<Text>();
        RectTransform _placeholderTransform = _placeholder.GetComponent<RectTransform>();
        _placeholderTransform.sizeDelta = new Vector2(width, height);
        _placeholder.color = fontColor;
        _placeholder.font = font;
        _placeholder.fontSize = fontSize;
        _placeholder.fontStyle = FontStyle.Italic;
        _placeholder.supportRichText = false;
        _placeholder.horizontalOverflow = HorizontalWrapMode.Overflow;
        _placeholder.text = placeholderString;
        _placeholder.alignment = TextAnchor.MiddleCenter;

        InputField _inputField = container.AddComponent<InputField>();
        _inputField.targetGraphic = _image;
        _inputField.textComponent = _text;
        _inputField.placeholder = _placeholder;
        RectTransform _inputFieldTransform = _inputField.GetComponent<RectTransform>();
        _inputFieldTransform.localPosition = position;
        _inputFieldTransform.sizeDelta = new Vector2(width, height);
        _inputField.characterLimit = 7;
        _inputField.characterValidation = InputField.CharacterValidation.Name;
        _inputField.text = defaultString;
		return _inputField;
    }

    public static void createButton(Canvas canvas, string objectName, Vector3 position)
    {
        int width = 150;
        int height = 50;
        int fontSize = 24;
        Color fontColor = Color.white;
        Color inputBoxColor = new Color(242f / 255f, 151f / 255f, 161f / 255f, 255f / 255f);

        GameObject container = new GameObject();
        container.name = objectName;
        container.transform.parent = canvas.transform;
        container.transform.localPosition = position;
        container.AddComponent<CanvasRenderer>();
        

        Image _image = container.AddComponent<Image>();
        _image.type = Image.Type.Sliced;
        _image.color = inputBoxColor;
        Button _button = container.AddComponent<Button>();
        _button.onClick.AddListener(delegate { OnButtonClick(container.transform.parent.GetChild(0).gameObject.GetComponent<InputField>(), container.transform.parent.GetChild(1).gameObject.GetComponent<InputField>(), canvas); });

        RectTransform _containerTransform = container.GetComponent<RectTransform>();
        _containerTransform.sizeDelta = new Vector2(width, height);

        Font font = (Font)Resources.Load("font/Cabin");

        GameObject textContainer = new GameObject();
        textContainer.name = "Text";
        textContainer.transform.parent = container.transform;
        textContainer.transform.localPosition = new Vector3(0,0,0);
        container.transform.localScale = new Vector3(1, 1, 1);
        Text _text = textContainer.AddComponent<Text>();
        RectTransform _textTransform = _text.GetComponent<RectTransform>();
        _textTransform.sizeDelta = new Vector2(width, height);
        _text.supportRichText = false;
        _text.color = fontColor;
        _text.font = font;
        _text.fontSize = fontSize;
        _text.horizontalOverflow = HorizontalWrapMode.Overflow;
        _text.alignment = TextAnchor.MiddleCenter;
        _text.text = "Confirm";
    }

    public static void createEqualsBox(Canvas canvas, string objectName, Vector3 position)
    {
        int width = 50;
        int height = 50;
        int fontSize = 40;
        Color fontColor = Color.white;

        GameObject container = new GameObject();
        container.name = objectName;
        container.transform.parent = canvas.transform;
			
        container.AddComponent<CanvasRenderer>();

        Font font = (Font)Resources.Load("font/Cabin");

        container.transform.localScale = new Vector3(1, 1, 1);
        Text _text = container.AddComponent<Text>();
        RectTransform _textTransform = _text.GetComponent<RectTransform>();
		_textTransform.localPosition = position;
        _textTransform.sizeDelta = new Vector2(width, height);
        _text.supportRichText = false;
        _text.color = fontColor;
        _text.font = font;
        _text.fontSize = fontSize;
        _text.horizontalOverflow = HorizontalWrapMode.Overflow;
        _text.alignment = TextAnchor.MiddleCenter;
		_text.text = "=";
    }

    public static void OnButtonClick(InputField firstname, InputField lastname, Canvas canvas )
    {
        String firstNameString = firstname.text;
        String lastNameString = lastname.text;
        ScreenKeyboardManager.Instance.m_inputText = firstNameString;
        ScreenKeyboardManager.Instance.m_inputTextLastName = lastNameString;
        Debug.LogWarning("Name: " + firstNameString + " = " + lastNameString);
        ScreenKeyboardManager.m_instance.isKeyboardActive = false;
        GameObject.Destroy(canvas.gameObject);
    }

    // Token: 0x06000A98 RID: 2712 RVA: 0x0002EAEC File Offset: 0x0002CCEC
    public static void Close()
	{
		if (ScreenKeyboardManager.m_instance != null)
		{
			UnityEngine.Object.Destroy(ScreenKeyboardManager.m_instance.gameObject);
			ScreenKeyboardManager.m_instance = null;
		}
	}

	// Token: 0x04000885 RID: 2181
	private static ScreenKeyboardManager m_instance;

	// Token: 0x04000886 RID: 2182
	//private TouchScreenKeyboard m_keyboard;

	// Token: 0x04000887 RID: 2183
	private string m_inputText;

    // Token: 0x04000887 RID: 2183
    private string m_inputTextLastName;

    private bool isKeyboardActive;
}
