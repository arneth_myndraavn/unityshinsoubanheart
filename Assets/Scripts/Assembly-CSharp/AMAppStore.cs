﻿using System;
using System.Collections.Generic;
using PaymentStoreApi;
using Qoo;
using UnityEngine;

// Token: 0x02000181 RID: 385
public static class AMAppStore
{
	// Token: 0x06000AD6 RID: 2774 RVA: 0x0002F798 File Offset: 0x0002D998
	public static bool ResultWaitInit(ref AMAppStore.ResultStatus eResult)
	{
		if (AMAppStore.Wait(AMAppStore.CommandNo.CMD_INIT))
		{
			eResult = AMAppStore.GetResult(AMAppStore.CommandNo.CMD_INIT);
			return true;
		}
		return false;
	}

	// Token: 0x06000AD7 RID: 2775 RVA: 0x0002F7B0 File Offset: 0x0002D9B0
	public static bool ResultWaitPurchase(ref AMAppStore.ResultStatus eResult)
	{
		if (AMAppStore.Wait(AMAppStore.CommandNo.CMD_PURCHASE))
		{
			eResult = AMAppStore.GetResult(AMAppStore.CommandNo.CMD_PURCHASE);
			return true;
		}
		return false;
	}

	// Token: 0x06000AD8 RID: 2776 RVA: 0x0002F7C8 File Offset: 0x0002D9C8
	public static bool ResultWaitPurchased(ref AMAppStore.ResultStatus eResult)
	{
		if (AMAppStore.Wait(AMAppStore.CommandNo.CMD_PURCHASED))
		{
			eResult = AMAppStore.GetResult(AMAppStore.CommandNo.CMD_PURCHASED);
			return true;
		}
		return false;
	}

	// Token: 0x06000AD9 RID: 2777 RVA: 0x0002F7E0 File Offset: 0x0002D9E0
	public static bool ResultWaitMenu(ref AMAppStore.ResultStatus eResult)
	{
		if (AMAppStore.Wait(AMAppStore.CommandNo.CMD_MENU))
		{
			eResult = AMAppStore.GetResult(AMAppStore.CommandNo.CMD_MENU);
			return true;
		}
		return false;
	}

	// Token: 0x06000ADA RID: 2778 RVA: 0x0002F7F8 File Offset: 0x0002D9F8
	public static bool ResultWaitRestore(ref AMAppStore.ResultStatus eResult)
	{
		if (AMAppStore.Wait(AMAppStore.CommandNo.CMD_RESTORE))
		{
			eResult = AMAppStore.GetResult(AMAppStore.CommandNo.CMD_RESTORE);
			return true;
		}
		return false;
	}

	// Token: 0x06000ADB RID: 2779 RVA: 0x0002F810 File Offset: 0x0002DA10
	public static string GetErrorInit()
	{
		return AMAppStore.GetErrorMessage(AMAppStore.CommandNo.CMD_INIT);
	}

	// Token: 0x06000ADC RID: 2780 RVA: 0x0002F818 File Offset: 0x0002DA18
	public static string GetErrorPurchase()
	{
		return AMAppStore.GetErrorMessage(AMAppStore.CommandNo.CMD_PURCHASE);
	}

	// Token: 0x06000ADD RID: 2781 RVA: 0x0002F820 File Offset: 0x0002DA20
	public static string GetErrorPurchased()
	{
		return AMAppStore.GetErrorMessage(AMAppStore.CommandNo.CMD_PURCHASED);
	}

	// Token: 0x06000ADE RID: 2782 RVA: 0x0002F828 File Offset: 0x0002DA28
	public static string GetErrorMenu()
	{
		return AMAppStore.GetErrorMessage(AMAppStore.CommandNo.CMD_MENU);
	}

	// Token: 0x06000ADF RID: 2783 RVA: 0x0002F830 File Offset: 0x0002DA30
	public static string GetErrorRestore()
	{
		return AMAppStore.GetErrorMessage(AMAppStore.CommandNo.CMD_RESTORE);
	}

	// Token: 0x06000AE0 RID: 2784 RVA: 0x0002F838 File Offset: 0x0002DA38
	public static int GetPurchasesList(ref List<AMAppStore.OpenPurchasesList> PurchasesList)
	{
		List<StoreKitTransaction> purchasesList = AMAppStore.WrapStore.GetPurchasesList();
		int count = purchasesList.Count;
		if (count == 0)
		{
			PurchasesList.Clear();
		}
		else
		{
			AMAppStore.OpenPurchasesList item = default(AMAppStore.OpenPurchasesList);
			for (int i = 0; i < purchasesList.Count; i++)
			{
				item.Set(purchasesList[i].productIdentifier, true);
				if (!PurchasesList.Contains(item))
				{
					PurchasesList.Add(item);
				}
			}
		}
		return count;
	}

	// Token: 0x06000AE1 RID: 2785 RVA: 0x0002F8B0 File Offset: 0x0002DAB0
	public static int GetMenuList(ref List<AMAppStore.OpenMenuList> MenuList)
	{
		if (AMAppStore.m_MenuList == null)
		{
			MenuList.Clear();
			return 0;
		}
		int count = AMAppStore.m_MenuList.Count;
		if (count == 0)
		{
			MenuList.Clear();
			return 0;
		}
		AMAppStore.OpenMenuList item = default(AMAppStore.OpenMenuList);
		for (int i = 0; i < AMAppStore.m_MenuList.Count; i++)
		{
			item.Set(AMAppStore.m_MenuList[i].productIdentifier, AMAppStore.m_MenuList[i].price);
			if (!MenuList.Contains(item))
			{
				MenuList.Add(item);
			}
		}
		return count;
	}

	// Token: 0x06000AE2 RID: 2786 RVA: 0x0002F94C File Offset: 0x0002DB4C
	public static bool Init(string szKey)
	{
		AMAppStore.WrapStore.Init(szKey);
		return true;
	}

	// Token: 0x06000AE3 RID: 2787 RVA: 0x0002F958 File Offset: 0x0002DB58
	public static void End()
	{
	}

	// Token: 0x06000AE4 RID: 2788 RVA: 0x0002F95C File Offset: 0x0002DB5C
	public static bool CheckCondition()
	{
		return AMAppStore.WrapStore.CheckPayment();
	}

	// Token: 0x06000AE5 RID: 2789 RVA: 0x0002F964 File Offset: 0x0002DB64
	public static bool RequestPurchase(string szProduct)
	{
		if (AMAppStore.CheckReqBusy(AMAppStore.CommandNo.CMD_PURCHASE))
		{
			return false;
		}
		AMAppStore.SetReqBusy(AMAppStore.CommandNo.CMD_PURCHASE);
		AMAppStore.WrapStore.PurchaseProduct(szProduct, 1);
		return true;
	}

	// Token: 0x06000AE6 RID: 2790 RVA: 0x0002F984 File Offset: 0x0002DB84
	public static bool RequestAfterPurchase(string[] szProducts)
	{
		if (AMAppStore.CheckReqBusy(AMAppStore.CommandNo.CMD_PURCHASED))
		{
			return false;
		}
		AMAppStore.m_PurchasesList = AMAppStore.WrapStore.GetPurchasesList();
		AMAppStore.SetReqFinish(AMAppStore.CommandNo.CMD_PURCHASED);
		AMAppStore.SetSuccess(AMAppStore.CommandNo.CMD_PURCHASED);
		return true;
	}

	// Token: 0x06000AE7 RID: 2791 RVA: 0x0002F9B8 File Offset: 0x0002DBB8
	public static bool RequestMenuList(string[] szProducts)
	{
		if (AMAppStore.CheckReqBusy(AMAppStore.CommandNo.CMD_MENU))
		{
			return false;
		}
		AMAppStore.SetReqBusy(AMAppStore.CommandNo.CMD_MENU);
		AMAppStore.WrapStore.GetMenu(szProducts);
		return true;
	}

	// Token: 0x06000AE8 RID: 2792 RVA: 0x0002F9D4 File Offset: 0x0002DBD4
	public static bool RequestRestore(string[] szProducts)
	{
		if (AMAppStore.CheckReqBusy(AMAppStore.CommandNo.CMD_RESTORE))
		{
			return false;
		}
		AMAppStore.SetReqBusy(AMAppStore.CommandNo.CMD_RESTORE);
		AMAppStore.WrapStore.Restore();
		return true;
	}

	// Token: 0x06000AE9 RID: 2793 RVA: 0x0002F9F0 File Offset: 0x0002DBF0
	public static void RegistrationEvent()
	{
		StoreKitManager.productPurchaseAwaitingConfirmationEvent += AMAppStore.productPurchaseAwaitingConfirmationEvent;
		StoreKitManager.purchaseSuccessfulEvent += AMAppStore.purchaseSuccessful;
		StoreKitManager.purchaseCancelledEvent += AMAppStore.purchaseCancelled;
		StoreKitManager.purchaseFailedEvent += AMAppStore.purchaseFailed;
		StoreKitManager.productListReceivedEvent += AMAppStore.productListReceivedEvent;
		StoreKitManager.productListRequestFailedEvent += AMAppStore.productListRequestFailed;
		StoreKitManager.restoreTransactionsFailedEvent += AMAppStore.restoreTransactionsFailed;
		StoreKitManager.restoreTransactionsFinishedEvent += AMAppStore.restoreTransactionsFinished;
		StoreKitManager.paymentQueueUpdatedDownloadsEvent += AMAppStore.paymentQueueUpdatedDownloadsEvent;
	}

	// Token: 0x06000AEA RID: 2794 RVA: 0x0002FA98 File Offset: 0x0002DC98
	public static void DeleteEvent()
	{
		StoreKitManager.productPurchaseAwaitingConfirmationEvent -= AMAppStore.productPurchaseAwaitingConfirmationEvent;
		StoreKitManager.purchaseSuccessfulEvent -= AMAppStore.purchaseSuccessful;
		StoreKitManager.purchaseCancelledEvent -= AMAppStore.purchaseCancelled;
		StoreKitManager.purchaseFailedEvent -= AMAppStore.purchaseFailed;
		StoreKitManager.productListReceivedEvent -= AMAppStore.productListReceivedEvent;
		StoreKitManager.productListRequestFailedEvent -= AMAppStore.productListRequestFailed;
		StoreKitManager.restoreTransactionsFailedEvent -= AMAppStore.restoreTransactionsFailed;
		StoreKitManager.restoreTransactionsFinishedEvent -= AMAppStore.restoreTransactionsFinished;
		StoreKitManager.paymentQueueUpdatedDownloadsEvent -= AMAppStore.paymentQueueUpdatedDownloadsEvent;
	}

	// Token: 0x06000AEB RID: 2795 RVA: 0x0002FB40 File Offset: 0x0002DD40
	public static bool CheckPurchased(string ProductId)
	{
		return AMAppStore.m_PurchasesList != null && AMAppStore.m_PurchasesList.Count != 0 && AMAppStore.m_PurchasesList.Find((StoreKitTransaction i) => i.productIdentifier == ProductId) != null;
	}

	// Token: 0x06000AEC RID: 2796 RVA: 0x0002FB98 File Offset: 0x0002DD98
	public static string GetPrice(string ProductId)
	{
		if (AMAppStore.m_MenuList == null)
		{
			return "---";
		}
		if (AMAppStore.m_MenuList.Count == 0)
		{
			return "---";
		}
		StoreKitProduct storeKitProduct = AMAppStore.m_MenuList.Find((StoreKitProduct i) => i.productIdentifier == ProductId);
		if (storeKitProduct == null)
		{
			return "---";
		}
		return storeKitProduct.formattedPrice;
	}

	// Token: 0x06000AED RID: 2797 RVA: 0x0002FC00 File Offset: 0x0002DE00
	private static bool CheckRequestStatus(AMAppStore.CommandNo eCommandNo, AMAppStore.ReqStatus eStatus)
	{
		return AMAppStore.m_CmdSt[(int)eCommandNo].GetReqStatus() == eStatus;
	}

	// Token: 0x06000AEE RID: 2798 RVA: 0x0002FC1C File Offset: 0x0002DE1C
	private static void SetRequestStatus(AMAppStore.CommandNo eCommandNo, AMAppStore.ReqStatus eStatus)
	{
		AMAppStore.m_CmdSt[(int)eCommandNo].SetReqStatus(eStatus);
	}

	// Token: 0x06000AEF RID: 2799 RVA: 0x0002FC30 File Offset: 0x0002DE30
	private static AMAppStore.ResultStatus GetRequestResult(AMAppStore.CommandNo eCommandNo)
	{
		return AMAppStore.m_CmdSt[(int)eCommandNo].GetResultStatus();
	}

	// Token: 0x06000AF0 RID: 2800 RVA: 0x0002FC44 File Offset: 0x0002DE44
	private static void SetRequestResult(AMAppStore.CommandNo eCommandNo, AMAppStore.ResultStatus eResult)
	{
		AMAppStore.m_CmdSt[(int)eCommandNo].SetResultStatus(eResult);
	}

	// Token: 0x06000AF1 RID: 2801 RVA: 0x0002FC58 File Offset: 0x0002DE58
	private static string GetErrorMessage(AMAppStore.CommandNo eCommandNo)
	{
		return AMAppStore.m_CmdSt[(int)eCommandNo].GetErrorMess();
	}

	// Token: 0x06000AF2 RID: 2802 RVA: 0x0002FC6C File Offset: 0x0002DE6C
	private static void SetErrorMessage(AMAppStore.CommandNo eCommandNo, string szMess)
	{
		AMAppStore.m_CmdSt[(int)eCommandNo].SetErrorMess(szMess);
	}

	// Token: 0x06000AF3 RID: 2803 RVA: 0x0002FC80 File Offset: 0x0002DE80
	private static bool CheckReqNon(AMAppStore.CommandNo eCmd)
	{
		return AMAppStore.CheckRequestStatus(eCmd, AMAppStore.ReqStatus.REQST_NON);
	}

	// Token: 0x06000AF4 RID: 2804 RVA: 0x0002FC94 File Offset: 0x0002DE94
	private static bool CheckReqBusy(AMAppStore.CommandNo eCmd)
	{
		return AMAppStore.CheckRequestStatus(eCmd, AMAppStore.ReqStatus.REQST_BUSY);
	}

	// Token: 0x06000AF5 RID: 2805 RVA: 0x0002FCA8 File Offset: 0x0002DEA8
	private static bool CheckReqFinish(AMAppStore.CommandNo eCmd)
	{
		return AMAppStore.CheckRequestStatus(eCmd, AMAppStore.ReqStatus.REQST_FINISH);
	}

	// Token: 0x06000AF6 RID: 2806 RVA: 0x0002FCBC File Offset: 0x0002DEBC
	private static void SetReqNon(AMAppStore.CommandNo eCmd)
	{
		AMAppStore.SetRequestStatus(eCmd, AMAppStore.ReqStatus.REQST_NON);
	}

	// Token: 0x06000AF7 RID: 2807 RVA: 0x0002FCC8 File Offset: 0x0002DEC8
	private static void SetReqBusy(AMAppStore.CommandNo eCmd)
	{
		AMAppStore.SetRequestStatus(eCmd, AMAppStore.ReqStatus.REQST_BUSY);
	}

	// Token: 0x06000AF8 RID: 2808 RVA: 0x0002FCD4 File Offset: 0x0002DED4
	private static void SetReqFinish(AMAppStore.CommandNo eCmd)
	{
		AMAppStore.SetRequestStatus(eCmd, AMAppStore.ReqStatus.REQST_FINISH);
	}

	// Token: 0x06000AF9 RID: 2809 RVA: 0x0002FCE0 File Offset: 0x0002DEE0
	private static void SetSuccess(AMAppStore.CommandNo eCmd)
	{
		AMAppStore.SetRequestResult(eCmd, AMAppStore.ResultStatus.RESULT_SUCCESS);
	}

	// Token: 0x06000AFA RID: 2810 RVA: 0x0002FCEC File Offset: 0x0002DEEC
	private static void SetFailure(AMAppStore.CommandNo eCmd)
	{
		AMAppStore.SetRequestResult(eCmd, AMAppStore.ResultStatus.RESULT_FAILD);
	}

	// Token: 0x06000AFB RID: 2811 RVA: 0x0002FCF8 File Offset: 0x0002DEF8
	private static void SetCancel(AMAppStore.CommandNo eCmd)
	{
		AMAppStore.SetRequestResult(eCmd, AMAppStore.ResultStatus.RESULT_CANCEL);
	}

	// Token: 0x06000AFC RID: 2812 RVA: 0x0002FD04 File Offset: 0x0002DF04
	private static void SetError(AMAppStore.CommandNo eCmd, string szError)
	{
		AMAppStore.SetErrorMessage(eCmd, szError);
	}

	// Token: 0x06000AFD RID: 2813 RVA: 0x0002FD10 File Offset: 0x0002DF10
	public static bool Wait(AMAppStore.CommandNo eCmd)
	{
		return AMAppStore.CheckReqNon(eCmd) || AMAppStore.CheckReqFinish(eCmd);
	}

	// Token: 0x06000AFE RID: 2814 RVA: 0x0002FD28 File Offset: 0x0002DF28
	public static AMAppStore.ResultStatus GetResult(AMAppStore.CommandNo eCmd)
	{
		AMAppStore.SetReqNon(eCmd);
		return AMAppStore.GetRequestResult(eCmd);
	}

	// Token: 0x06000AFF RID: 2815 RVA: 0x0002FD38 File Offset: 0x0002DF38
	private static void productListReceivedEvent(List<StoreKitProduct> productList)
	{
		AMAppStore.m_MenuList = productList;
		AMAppStore.SetReqFinish(AMAppStore.CommandNo.CMD_MENU);
		AMAppStore.SetSuccess(AMAppStore.CommandNo.CMD_MENU);
		Qoo.Debug.Print("商品リスト取得成功");
	}

	// Token: 0x06000B00 RID: 2816 RVA: 0x0002FD58 File Offset: 0x0002DF58
	private static void productListRequestFailed(string error)
	{
		AMAppStore.SetReqFinish(AMAppStore.CommandNo.CMD_MENU);
		AMAppStore.SetFailure(AMAppStore.CommandNo.CMD_MENU);
		AMAppStore.SetError(AMAppStore.CommandNo.CMD_MENU, error);
		Qoo.Debug.Print("商品リスト取得失敗");
	}

	// Token: 0x06000B01 RID: 2817 RVA: 0x0002FD78 File Offset: 0x0002DF78
	private static void purchaseSuccessful(StoreKitTransaction transaction)
	{
		AMAppStore.SetReqFinish(AMAppStore.CommandNo.CMD_PURCHASE);
		AMAppStore.SetSuccess(AMAppStore.CommandNo.CMD_PURCHASE);
		PaymentSave paymentSave = new PaymentSave();
		paymentSave.Save(transaction.productIdentifier);
		Qoo.Debug.Print("購入成功");
	}

	// Token: 0x06000B02 RID: 2818 RVA: 0x0002FDB0 File Offset: 0x0002DFB0
	private static void purchaseFailed(string error)
	{
		AMAppStore.SetReqFinish(AMAppStore.CommandNo.CMD_PURCHASE);
		AMAppStore.SetFailure(AMAppStore.CommandNo.CMD_PURCHASE);
		AMAppStore.SetError(AMAppStore.CommandNo.CMD_PURCHASE, error);
		Qoo.Debug.Print("購入失敗");
	}

	// Token: 0x06000B03 RID: 2819 RVA: 0x0002FDD0 File Offset: 0x0002DFD0
	private static void purchaseCancelled(string error)
	{
		AMAppStore.SetReqFinish(AMAppStore.CommandNo.CMD_PURCHASE);
		AMAppStore.SetCancel(AMAppStore.CommandNo.CMD_PURCHASE);
		AMAppStore.SetError(AMAppStore.CommandNo.CMD_PURCHASE, error);
		Qoo.Debug.Print("購入キャンセル");
	}

	// Token: 0x06000B04 RID: 2820 RVA: 0x0002FDF0 File Offset: 0x0002DFF0
	private static void productPurchaseAwaitingConfirmationEvent(StoreKitTransaction transaction)
	{
		Qoo.Debug.Print("処理完了待ち");
	}

	// Token: 0x06000B05 RID: 2821 RVA: 0x0002FDFC File Offset: 0x0002DFFC
	private static void restoreTransactionsFinished()
	{
		AMAppStore.SetReqFinish(AMAppStore.CommandNo.CMD_RESTORE);
		AMAppStore.SetSuccess(AMAppStore.CommandNo.CMD_RESTORE);
		Qoo.Debug.Print("リストア成功");
	}

	// Token: 0x06000B06 RID: 2822 RVA: 0x0002FE14 File Offset: 0x0002E014
	private static void restoreTransactionsFailed(string error)
	{
		AMAppStore.SetReqFinish(AMAppStore.CommandNo.CMD_RESTORE);
		AMAppStore.SetFailure(AMAppStore.CommandNo.CMD_RESTORE);
		AMAppStore.SetError(AMAppStore.CommandNo.CMD_RESTORE, error);
		Qoo.Debug.Print("リストア失敗");
	}

	// Token: 0x06000B07 RID: 2823 RVA: 0x0002FE34 File Offset: 0x0002E034
	private static void paymentQueueUpdatedDownloadsEvent(List<StoreKitDownload> downloads)
	{
		Qoo.Debug.Print("ホスティングシステムDL状況");
	}

	// Token: 0x040008C1 RID: 2241
	private static AMAppStore.PaymentCommandStatus[] m_CmdSt = new AMAppStore.PaymentCommandStatus[5];

	// Token: 0x040008C2 RID: 2242
	private static bool m_bInit = false;

	// Token: 0x040008C3 RID: 2243
	private static List<StoreKitTransaction> m_PurchasesList = null;

	// Token: 0x040008C4 RID: 2244
	private static List<StoreKitProduct> m_MenuList = null;

	// Token: 0x02000182 RID: 386
	private static class WrapStore
	{
		// Token: 0x06000B09 RID: 2825 RVA: 0x0002FE44 File Offset: 0x0002E044
		public static void Init(string szKey)
		{
			if (!AMAppStore.m_bInit)
			{
				switch (Application.platform)
				{
				case RuntimePlatform.IPhonePlayer:
				case RuntimePlatform.Android:
					AMAppStore.WrapStore.m_bEmu = false;
					goto IL_43;
				}
				AMAppStore.WrapStore.m_bEmu = true;
				IL_43:
				if (AMAppStore.WrapStore.m_bEmu)
				{
					AMAppStore.WrapStore.m_PurchasesList = new List<StoreKitTransaction>();
				}
				AMAppStore.SetReqNon(AMAppStore.CommandNo.CMD_INIT);
				AMAppStore.SetSuccess(AMAppStore.CommandNo.CMD_INIT);
				AMAppStore.m_bInit = true;
			}
			else
			{
				AMAppStore.SetReqNon(AMAppStore.CommandNo.CMD_INIT);
				AMAppStore.SetSuccess(AMAppStore.CommandNo.CMD_INIT);
			}
		}

		// Token: 0x06000B0A RID: 2826 RVA: 0x0002FECC File Offset: 0x0002E0CC
		public static List<StoreKitTransaction> GetPurchasesList()
		{
			if (AMAppStore.WrapStore.m_bEmu)
			{
				return AMAppStore.WrapStore.m_PurchasesList;
			}
			return StoreKitBinding.getAllSavedTransactions();
		}

		// Token: 0x06000B0B RID: 2827 RVA: 0x0002FEE4 File Offset: 0x0002E0E4
		public static bool CheckPayment()
		{
			return AMAppStore.WrapStore.m_bEmu || StoreKitBinding.canMakePayments();
		}

		// Token: 0x06000B0C RID: 2828 RVA: 0x0002FEF8 File Offset: 0x0002E0F8
		public static bool PurchaseProduct(string szProduct, int nNum)
		{
			if (AMAppStore.WrapStore.m_bEmu)
			{
				StoreKitTransaction storeKitTransaction = new StoreKitTransaction();
				storeKitTransaction.base64EncodedTransactionReceipt = "Emu" + szProduct;
				storeKitTransaction.productIdentifier = szProduct;
				storeKitTransaction.transactionIdentifier = szProduct;
				storeKitTransaction.quantity = 0;
				if (!AMAppStore.WrapStore.m_PurchasesList.Contains(storeKitTransaction))
				{
					AMAppStore.WrapStore.m_PurchasesList.Add(storeKitTransaction);
				}
				PaymentSave paymentSave = new PaymentSave();
				paymentSave.Save(szProduct);
				AMAppStore.SetReqFinish(AMAppStore.CommandNo.CMD_PURCHASE);
				AMAppStore.SetSuccess(AMAppStore.CommandNo.CMD_PURCHASE);
				return true;
			}
			StoreKitBinding.purchaseProduct(szProduct, 1);
			return true;
		}

		// Token: 0x06000B0D RID: 2829 RVA: 0x0002FF7C File Offset: 0x0002E17C
		public static void GetMenu(string[] szProducts)
		{
			if (AMAppStore.WrapStore.m_bEmu)
			{
				AMAppStore.SetReqFinish(AMAppStore.CommandNo.CMD_MENU);
				AMAppStore.SetSuccess(AMAppStore.CommandNo.CMD_MENU);
				return;
			}
			StoreKitBinding.requestProductData(szProducts);
		}

		// Token: 0x06000B0E RID: 2830 RVA: 0x0002FF9C File Offset: 0x0002E19C
		public static void Restore()
		{
			if (AMAppStore.WrapStore.m_bEmu)
			{
				AMAppStore.SetReqFinish(AMAppStore.CommandNo.CMD_RESTORE);
				AMAppStore.SetSuccess(AMAppStore.CommandNo.CMD_RESTORE);
				return;
			}
			StoreKitBinding.restoreCompletedTransactions();
		}

		// Token: 0x040008C5 RID: 2245
		private static bool m_bEmu;

		// Token: 0x040008C6 RID: 2246
		private static List<StoreKitTransaction> m_PurchasesList;
	}

	// Token: 0x02000183 RID: 387
	public enum ReqStatus
	{
		// Token: 0x040008C8 RID: 2248
		REQST_NON,
		// Token: 0x040008C9 RID: 2249
		REQST_BUSY,
		// Token: 0x040008CA RID: 2250
		REQST_FINISH
	}

	// Token: 0x02000184 RID: 388
	public enum ResultStatus
	{
		// Token: 0x040008CC RID: 2252
		RESULT_SUCCESS,
		// Token: 0x040008CD RID: 2253
		RESULT_FAILD,
		// Token: 0x040008CE RID: 2254
		RESULT_CANCEL
	}

	// Token: 0x02000185 RID: 389
	public enum CommandNo
	{
		// Token: 0x040008D0 RID: 2256
		CMD_INIT,
		// Token: 0x040008D1 RID: 2257
		CMD_PURCHASE,
		// Token: 0x040008D2 RID: 2258
		CMD_PURCHASED,
		// Token: 0x040008D3 RID: 2259
		CMD_MENU,
		// Token: 0x040008D4 RID: 2260
		CMD_RESTORE,
		// Token: 0x040008D5 RID: 2261
		CMD_MAX
	}

	// Token: 0x02000186 RID: 390
	public struct PaymentCommandStatus
	{
		// Token: 0x06000B0F RID: 2831 RVA: 0x0002FFBC File Offset: 0x0002E1BC
		public void SetErrorMess(string szMess)
		{
			this.szError = szMess;
		}

		// Token: 0x06000B10 RID: 2832 RVA: 0x0002FFC8 File Offset: 0x0002E1C8
		public string GetErrorMess()
		{
			return this.szError;
		}

		// Token: 0x06000B11 RID: 2833 RVA: 0x0002FFD0 File Offset: 0x0002E1D0
		public void SetReqStatus(AMAppStore.ReqStatus eStatus)
		{
			this.eReqStatus = eStatus;
		}

		// Token: 0x06000B12 RID: 2834 RVA: 0x0002FFDC File Offset: 0x0002E1DC
		public AMAppStore.ReqStatus GetReqStatus()
		{
			return this.eReqStatus;
		}

		// Token: 0x06000B13 RID: 2835 RVA: 0x0002FFE4 File Offset: 0x0002E1E4
		public void SetResultStatus(AMAppStore.ResultStatus eStatus)
		{
			this.eResStatus = eStatus;
		}

		// Token: 0x06000B14 RID: 2836 RVA: 0x0002FFF0 File Offset: 0x0002E1F0
		public AMAppStore.ResultStatus GetResultStatus()
		{
			return this.eResStatus;
		}

		// Token: 0x040008D6 RID: 2262
		public string szError;

		// Token: 0x040008D7 RID: 2263
		public AMAppStore.ReqStatus eReqStatus;

		// Token: 0x040008D8 RID: 2264
		public AMAppStore.ResultStatus eResStatus;
	}

	// Token: 0x02000187 RID: 391
	public struct OpenMenuList
	{
		// Token: 0x06000B15 RID: 2837 RVA: 0x0002FFF8 File Offset: 0x0002E1F8
		public void Set(string id, string price)
		{
			this.ProductID = id;
			this.Price = price;
		}

		// Token: 0x040008D9 RID: 2265
		public string ProductID;

		// Token: 0x040008DA RID: 2266
		public string Price;
	}

	// Token: 0x02000188 RID: 392
	public struct OpenPurchasesList
	{
		// Token: 0x06000B16 RID: 2838 RVA: 0x00030008 File Offset: 0x0002E208
		public void Set(string id, bool bflag)
		{
			this.ProductID = id;
			this.Settled = bflag;
		}

		// Token: 0x040008DB RID: 2267
		public string ProductID;

		// Token: 0x040008DC RID: 2268
		public bool Settled;
	}
}
