﻿using System;

// Token: 0x02000132 RID: 306
public class StorySelectWindow : GalleryCharaSelectWindow
{
	// Token: 0x06000839 RID: 2105 RVA: 0x00024FF4 File Offset: 0x000231F4
	protected sealed override string GetBGMName()
	{
		return "lovecheck2_ali";
	}

	// Token: 0x0600083A RID: 2106 RVA: 0x00024FFC File Offset: 0x000231FC
	protected sealed override int GetCollect()
	{
		return -1;
	}

	// Token: 0x0600083B RID: 2107 RVA: 0x00025000 File Offset: 0x00023200
	protected sealed override void OnName()
	{
	}

	// Token: 0x0600083C RID: 2108 RVA: 0x00025004 File Offset: 0x00023204
	protected sealed override bool IsNameButton()
	{
		return false;
	}

	// Token: 0x0600083D RID: 2109 RVA: 0x00025008 File Offset: 0x00023208
	protected sealed override string GetHeaderPath()
	{
		return "screen/title/kyara_bnr";
	}

	// Token: 0x0600083E RID: 2110 RVA: 0x00025010 File Offset: 0x00023210
	protected sealed override string GetCharaButtonSceneName()
	{
		return UIValue.SCENE_INTRODUCTION;
	}
}
