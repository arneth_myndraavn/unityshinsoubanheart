﻿using System;
using UnityEngine;

// Token: 0x0200015B RID: 347
public class CharObject
{
	// Token: 0x060009F6 RID: 2550 RVA: 0x0002C2D8 File Offset: 0x0002A4D8
	public CharObject()
	{
	}

	// Token: 0x060009F7 RID: 2551 RVA: 0x0002C2FC File Offset: 0x0002A4FC
	public CharObject(char code_, int size_, int x_, int y_, BMGlyph glyph_, float scale_, Color32 col_)
	{
		this.X = x_;
		this.Y = y_;
		this.glyph = glyph_;
		this.size = size_;
		this.width = size_;
		this.height = size_;
		this.scale = scale_;
		this.code = code_;
		this.color = col_;
	}

	// Token: 0x060009F8 RID: 2552 RVA: 0x0002C364 File Offset: 0x0002A564
	public CharObject(char code_, int size_, int x_, int y_)
	{
		this.X = x_;
		this.Y = y_;
		this.glyph = null;
		this.size = size_;
		this.width = size_;
		this.height = size_;
		this.scale = 0f;
		this.code = code_;
		this.color = new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue);
	}

	// Token: 0x1700013A RID: 314
	// (get) Token: 0x060009F9 RID: 2553 RVA: 0x0002C3E4 File Offset: 0x0002A5E4
	public int Width
	{
		get
		{
			return this.width;
		}
	}

	// Token: 0x1700013B RID: 315
	// (get) Token: 0x060009FA RID: 2554 RVA: 0x0002C3EC File Offset: 0x0002A5EC
	public int Height
	{
		get
		{
			return this.height;
		}
	}

	// Token: 0x0400081A RID: 2074
	public int X;

	// Token: 0x0400081B RID: 2075
	public int Y;

	// Token: 0x0400081C RID: 2076
	public BMGlyph glyph;

	// Token: 0x0400081D RID: 2077
	public int width;

	// Token: 0x0400081E RID: 2078
	public int height;

	// Token: 0x0400081F RID: 2079
	public int size;

	// Token: 0x04000820 RID: 2080
	public char code;

	// Token: 0x04000821 RID: 2081
	public Color32 color = default(Color32);

	// Token: 0x04000822 RID: 2082
	public float scale;
}
