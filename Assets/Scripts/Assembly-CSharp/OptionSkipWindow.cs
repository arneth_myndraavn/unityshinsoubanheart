﻿using System;
using Qoo.Def;
using Qoo.Game;
using Qoo.Input;
using UnityEngine;

// Token: 0x0200011F RID: 287
public class OptionSkipWindow : OptionBaseWindow
{
	// Token: 0x060007A7 RID: 1959 RVA: 0x00020BE0 File Offset: 0x0001EDE0
	private bool IsSkipReadOnly()
	{
		return SysData.GetSkip() == SKIP_MODE.READ;
	}

	// Token: 0x060007A8 RID: 1960 RVA: 0x00020BEC File Offset: 0x0001EDEC
	private int GetSkipTypePosition()
	{
		return (!this.IsSkipReadOnly()) ? 540 : 318;
	}

	// Token: 0x060007A9 RID: 1961 RVA: 0x00020C08 File Offset: 0x0001EE08
	private void UpdateSkipTypeUI()
	{
		ImageObject imageObject = base.GetImageObject("Check");
		imageObject.UIObject.ScreenX = this.GetSkipTypePosition();
		imageObject.InitPosition();
	}

	// Token: 0x060007AA RID: 1962 RVA: 0x00020C38 File Offset: 0x0001EE38
	private void OnSkipTypeSwitch(bool isReadOnly)
	{
		if (isReadOnly)
		{
			SysData.SetSkip(1);
		}
		else
		{
			SysData.SetSkip(2);
		}
	}

	// Token: 0x060007AB RID: 1963 RVA: 0x00020C54 File Offset: 0x0001EE54
	private void OnClose()
	{
		GameObject gameObject = GameObject.Find(UIValue.SCENE_ADVMENU + "/Wnd");
		SubTaskBaseWindow component = gameObject.GetComponent<SubTaskBaseWindow>();
		component.IsTaskEnd = true;
		base.DeleteLastAddScene();
		ScreenEffect.Term();
	}

	// Token: 0x060007AC RID: 1964 RVA: 0x00020C90 File Offset: 0x0001EE90
	private void OnNextNodeSkip()
	{
		KsInput.SetJump(true);
	}

	// Token: 0x060007AD RID: 1965 RVA: 0x00020C98 File Offset: 0x0001EE98
	private void OnAutoSkip()
	{
		KsInput.SetSkip(true);
	}

	// Token: 0x060007AE RID: 1966 RVA: 0x00020CA0 File Offset: 0x0001EEA0
	protected override OptionBaseWindow.OptionType GetOptionType()
	{
		return OptionBaseWindow.OptionType.Skip;
	}

	// Token: 0x060007AF RID: 1967 RVA: 0x00020CA4 File Offset: 0x0001EEA4
	protected sealed override string[] newSceneTextureNameArray()
	{
		return new string[]
		{
			"screen/sysmenu/skip_1",
			"screen/sysmenu/skip_2",
			"screen/sysmenu/skip_5",
			"screen/sysmenu/sys_check"
		};
	}

	// Token: 0x060007B0 RID: 1968 RVA: 0x00020CDC File Offset: 0x0001EEDC
	protected sealed override void BeforeInit()
	{
		Vector3 localPosition = base.transform.parent.localPosition;
		localPosition.z = -50f;
		base.transform.parent.localPosition = localPosition;
	}

	// Token: 0x060007B1 RID: 1969 RVA: 0x00020D18 File Offset: 0x0001EF18
	protected sealed override void AfterInit()
	{
	}

	// Token: 0x060007B2 RID: 1970 RVA: 0x00020D1C File Offset: 0x0001EF1C
	protected override void OnGraphicLoadComplete()
	{
		base.OnGraphicLoadComplete();
		this.UpdateSkipTypeUI();
	}

	// Token: 0x060007B3 RID: 1971 RVA: 0x00020D2C File Offset: 0x0001EF2C
	protected sealed override BaseWindow.UIComponent[] newComponentArray()
	{
		int skipTypePosition = this.GetSkipTypePosition();
		return new BaseWindow.UIComponent[]
		{
			new BaseWindow.UICollision("Close", 0, 0, this.wndz, 960, 544),
			new BaseWindow.UICollision("READ", 350, 80, this.wndz + 2, 115, 45),
			new BaseWindow.UICollision("ALL", 570, 80, this.wndz + 2, 115, 45),
			new BaseWindow.UIButton("NextNodeSkip", 305, 139, this.wndz + 1, "screen/sysmenu/skip_1", true, true, 1, 2, 0),
			new BaseWindow.UIButton("AutoSkip", 305, 268, this.wndz + 1, "screen/sysmenu/skip_2", true, true, 1, 2, 0),
			new BaseWindow.UIImage("READ_ALL", 305, 15, this.wndz + 1, "screen/sysmenu/skip_5", true, true),
			new BaseWindow.UIImage("Check", skipTypePosition, 87, this.wndz + 1, "screen/sysmenu/sys_check", true, true)
		};
	}

	// Token: 0x060007B4 RID: 1972 RVA: 0x00020E3C File Offset: 0x0001F03C
	protected sealed override void OnBaseWindowOnButton(string obj)
	{
		switch (obj)
		{
		case "NextNodeSkip":
			base.PlaySE_Ok();
			this.OnNextNodeSkip();
			this.OnClose();
			break;
		case "AutoSkip":
			base.PlaySE_Ok();
			this.OnAutoSkip();
			this.OnClose();
			break;
		case "READ":
			base.PlaySE_Select();
			this.OnSkipTypeSwitch(true);
			this.UpdateSkipTypeUI();
			break;
		case "ALL":
			base.PlaySE_Select();
			this.OnSkipTypeSwitch(false);
			this.UpdateSkipTypeUI();
			break;
		}
		base.OnBaseWindowOnButton(obj);
	}

	// Token: 0x040006CC RID: 1740
	private readonly int wndz;
}
