﻿using System;
using Qoo;
using Qoo.Def;
using Qoo.Game;
using Qoo.Input;
using Qoo.Message;
using Qoo.SoundSystem;
using UnityEngine;

// Token: 0x020000B8 RID: 184
public class MsgWnd : Singleton<MsgWnd>
{
	// Token: 0x170000BE RID: 190
	// (get) Token: 0x0600055B RID: 1371 RVA: 0x00015200 File Offset: 0x00013400
	// (set) Token: 0x0600055C RID: 1372 RVA: 0x00015208 File Offset: 0x00013408
	public bool IsAutoSkip
	{
		get
		{
			return this.m_isAutoSkip;
		}
		set
		{
			this.m_isAutoSkip = value;
		}
	}

	// Token: 0x170000BF RID: 191
	// (get) Token: 0x0600055D RID: 1373 RVA: 0x00015214 File Offset: 0x00013414
	// (set) Token: 0x0600055E RID: 1374 RVA: 0x0001521C File Offset: 0x0001341C
	public bool IsSilentMode
	{
		get
		{
			return this.m_isSilentMode;
		}
		set
		{
			this.m_isSilentMode = value;
			if (this.m_isSilentMode)
			{
				this.m_MsgText.Show(false);
				this.Show(false);
			}
		}
	}

	// Token: 0x170000C0 RID: 192
	// (get) Token: 0x0600055F RID: 1375 RVA: 0x00015244 File Offset: 0x00013444
	// (set) Token: 0x06000560 RID: 1376 RVA: 0x0001524C File Offset: 0x0001344C
	public int KsNo { get; private set; }

	// Token: 0x170000C1 RID: 193
	// (get) Token: 0x06000561 RID: 1377 RVA: 0x00015258 File Offset: 0x00013458
	// (set) Token: 0x06000562 RID: 1378 RVA: 0x00015260 File Offset: 0x00013460
	public int LabelNo { get; private set; }

	// Token: 0x170000C2 RID: 194
	// (get) Token: 0x06000563 RID: 1379 RVA: 0x0001526C File Offset: 0x0001346C
	// (set) Token: 0x06000564 RID: 1380 RVA: 0x00015274 File Offset: 0x00013474
	public int MesNo { get; private set; }

	// Token: 0x170000C3 RID: 195
	// (get) Token: 0x06000565 RID: 1381 RVA: 0x00015280 File Offset: 0x00013480
	public string Message
	{
		get
		{
			return this.m_MsgText.GetMessage();
		}
	}

	// Token: 0x170000C4 RID: 196
	// (get) Token: 0x06000566 RID: 1382 RVA: 0x00015290 File Offset: 0x00013490
	// (set) Token: 0x06000567 RID: 1383 RVA: 0x00015298 File Offset: 0x00013498
	public MSGWND_STYLE Style
	{
		get
		{
			return this.m_Style;
		}
		private set
		{
			this.m_Style = value;
		}
	}

	// Token: 0x170000C5 RID: 197
	// (get) Token: 0x06000568 RID: 1384 RVA: 0x000152A4 File Offset: 0x000134A4
	public bool IsVisible
	{
		get
		{
			return this.m_isVisible;
		}
	}

	// Token: 0x170000C6 RID: 198
	// (get) Token: 0x06000569 RID: 1385 RVA: 0x000152AC File Offset: 0x000134AC
	// (set) Token: 0x0600056A RID: 1386 RVA: 0x000152B4 File Offset: 0x000134B4
	public UnitySprite FaceSprite
	{
		get
		{
			return this.m_spFace;
		}
		set
		{
			this.m_spFace = value;
		}
	}

	// Token: 0x0600056B RID: 1387 RVA: 0x000152C0 File Offset: 0x000134C0
	private void Awake()
	{
		this.m_tpMsg = Man2D.TextSprite(null);
		this.m_tpMsg.SetName("Message");
		for (int i = 0; i < this.m_tpName.Length; i++)
		{
			this.m_tpName[i] = Man2D.TextSprite(null);
			this.m_tpName[i].SetName("NameText:" + i.ToString());
			this.m_spName[i] = Man2D.Sprite(string.Empty);
			this.m_spName[i].SetName("NamePlate:" + i.ToString());
		}
		this.m_spFrame = Man2D.Sprite(string.Empty);
		this.m_spFrame.SetName("MsgFrame");
		this.m_spCursol = Man2D.Sprite(string.Empty);
		this.m_spCursol.SetName("Cursol");
		this.m_spHide = Man2D.Sprite("cancel");
		this.m_spHide.SetName("Hide Btn");
		this.m_spHide.SetPos(899, 318, 800);
		this.m_spHide.SetPtnNum(1, 2);
		this.m_spHide.SetPtnPos(0, 0);
		this.m_spHide.ResetUVAnim();
		this.m_spHide.SetUVAnimWait(1);
		this.m_spHide.Show = false;
		KsInput.HideSprite = this.m_spHide;
		this.m_MsgText = new CMessageText(base.QooGraph.Font, true);
		this.m_MsgText.Init(0, 0, 0, 100, 4, 0, 0, 0);
		this.m_Style = MSGWND_STYLE.NULL;
	}

	// Token: 0x0600056C RID: 1388 RVA: 0x00015454 File Offset: 0x00013654
	private void OnDestroy()
	{
		if (Singleton<Man2D>.IsReady)
		{
			Singleton<Man2D>.Instance.RemoveSprite(this.m_tpMsg);
			this.m_tpMsg = null;
			Singleton<Man2D>.Instance.RemoveSprite(this.m_spFrame);
			this.m_spFrame = null;
			Singleton<Man2D>.Instance.RemoveSprite(this.m_spCursol);
			this.m_spCursol = null;
			Singleton<Man2D>.Instance.RemoveSprite(this.m_spHide);
			this.m_spHide = null;
			KsInput.HideSprite = null;
			for (int num = 0; num != this.m_tpName.Length; num++)
			{
				Singleton<Man2D>.Instance.RemoveSprite(this.m_tpName[num]);
				this.m_tpName[num] = null;
			}
			for (int num2 = 0; num2 != this.m_spName.Length; num2++)
			{
				Singleton<Man2D>.Instance.RemoveSprite(this.m_spName[num2]);
				this.m_spName[num2] = null;
			}
			this.m_MsgText.Release();
		}
		this.m_spFace = null;
	}

	// Token: 0x0600056D RID: 1389 RVA: 0x00015550 File Offset: 0x00013750
	private void Update()
	{
		if (this.m_Style == MSGWND_STYLE.NULL)
		{
			return;
		}
		if (this.m_spCursol != null)
		{
			this.m_spCursol.Show = false;
		}
		if (!this.IsSilentMode && this.m_isHide)
		{
			if (!KsInput.IsHide)
			{
				this.m_isHide = false;
				this.Show(true);
			}
			return;
		}
		if (!this.m_isVisible && !this.IsSilentMode)
		{
			return;
		}
		if (KsInput.IsAuto != this.m_isAutoSkip)
		{
			Sound.SE_Ok();
		}
		this.m_isAutoSkip = KsInput.IsAuto;
		this.SetMsgSpeed();
		switch (this.m_Phase)
		{
		case MsgWnd.MSG_PHASE.MESANIM:
			if (this.IsFull())
			{
				this.Full();
				this.m_Phase = MsgWnd.MSG_PHASE.KEYWAIT;
			}
			else if (KsInput.IsTrig || GameData.CheckReadedSkip(this.KsNo, this.LabelNo, this.MesNo))
			{
				this.Full();
				this.m_Phase = MsgWnd.MSG_PHASE.KEYWAIT;
				KsInput.IsTrig = false;
			}
			this.CommonInputProc();
			this.m_MsgText.UpdateAnim();
			this.m_tpMsg.SetArray(this.m_MsgText.GetArray());
			break;
		case MsgWnd.MSG_PHASE.KEYWAIT:
			this.SetAutoSkipTime();
			this.UpdateCurAnim();
			if (KsInput.IsTrig || GameData.CheckReadedSkip(this.KsNo, this.LabelNo, this.MesNo) || this.CheckAutoSkip())
			{
				KsInput.IsTrig = false;
				this.m_spCursol.Show = false;
				this.m_Phase = MsgWnd.MSG_PHASE.NULL;
			}
			else
			{
				this.CommonInputProc();
			}
			break;
		case MsgWnd.MSG_PHASE.MESEND:
			this.m_Phase = MsgWnd.MSG_PHASE.NULL;
			break;
		}
		INPUT_STATUS status = UnityApp.Input.Status;
		if (status != INPUT_STATUS.PUSH)
		{
			this.m_spHide.SetPtnPos(0, 0);
			this.m_spHide.ResetUVAnim();
			this.m_spHide.EnableUVAnim(false);
		}
		else if (this.m_spHide.IsHit(UnityApp.Input.Start))
		{
			this.m_spHide.EnableUVAnim(true);
		}
	}

	// Token: 0x0600056E RID: 1390 RVA: 0x00015788 File Offset: 0x00013988
	private bool CommonInputProc()
	{
		if (this.m_isVisible && KsInput.IsHide)
		{
			this.Full();
			this.m_isHide = true;
			this.Show(false);
			return true;
		}
		if (KsInput.IsBackLog)
		{
			this.Full();
			return true;
		}
		return false;
	}

	// Token: 0x0600056F RID: 1391 RVA: 0x000157D4 File Offset: 0x000139D4
	public bool IsFull()
	{
		return this.m_MsgText.IsFull();
	}

	// Token: 0x06000570 RID: 1392 RVA: 0x000157E4 File Offset: 0x000139E4
	public void Full()
	{
		this.m_MsgText.Full();
		this.m_tpMsg.SetArray(this.m_MsgText.GetArray());
	}

	// Token: 0x06000571 RID: 1393 RVA: 0x00015814 File Offset: 0x00013A14
	private void UpdateCurAnim()
	{
		MSGWND_STYLE_DATA data = MessageStyle.GetData(this.Style);
		if (this.m_isAutoSkip)
		{
			this.m_spCursol.SetImage(base.QooSprite.LoadTexture(data.BrkAutoCg));
		}
		else
		{
			this.m_spCursol.SetImage(base.QooSprite.LoadTexture(data.BrkCg));
		}
		if (!MsgWnd.g_isAnimPosOuter)
		{
			this.m_spCursol.x = this.m_posBase.x + this.m_MsgText.GetCursorPosX();
			this.m_spCursol.y = this.m_posBase.y + this.m_MsgText.GetCursorPosY() - this.m_spCursol.h + 1;
		}
		else
		{
			this.m_spCursol.x = this.m_posBase.x + data.posBrk.x;
			this.m_spCursol.y = this.m_posBase.y + data.posBrk.y;
		}
		this.m_spCursol.z = data.posBrk.z;
		this.m_spCursol.A = byte.MaxValue;
		this.m_spCursol.SetPtnNum(data.sizeBrkSplit.w, data.sizeBrkSplit.h);
		this.m_spCursol.SetUVAnimWait(data.nBrkWait);
		this.m_spCursol.EnableUVAnim(true);
		this.m_spCursol.RepeatUVAnim(true);
		this.m_spCursol.Show = true;
	}

	// Token: 0x06000572 RID: 1394 RVA: 0x00015998 File Offset: 0x00013B98
	public void AddMessage(string msg)
	{
		this.m_MsgText.AddMessage(msg, this.m_Color, this.m_Size, -1);
		this.m_Phase = MsgWnd.MSG_PHASE.MESANIM;
		this.m_nAutoSkipTime = 0;
	}

	// Token: 0x06000573 RID: 1395 RVA: 0x000159C4 File Offset: 0x00013BC4
	public void ResetMessage()
	{
		this.m_MsgText.ResetMessage();
		this.m_tpMsg.SetArray(this.m_MsgText.GetArray());
	}

	// Token: 0x06000574 RID: 1396 RVA: 0x000159F4 File Offset: 0x00013BF4
	private void SetBasePos(int nX, int nY, bool bApply)
	{
		this.m_posBase.x = nX;
		this.m_posBase.y = nY;
		if (bApply)
		{
			MSGWND_STYLE style = this.Style;
			this.Style = MSGWND_STYLE.MAX;
			this.SetStyle(style);
		}
	}

	// Token: 0x06000575 RID: 1397 RVA: 0x00015A34 File Offset: 0x00013C34
	private void ResetBasePos(bool bApply)
	{
		this.SetBasePos(0, 0, bApply);
	}

	// Token: 0x06000576 RID: 1398 RVA: 0x00015A40 File Offset: 0x00013C40
	public void SetStyle(MSGWND_STYLE style_)
	{
		if (this.Style == style_)
		{
			return;
		}
		this.Style = style_;
		if (this.Style >= MSGWND_STYLE.MAX)
		{
			return;
		}
		MSGWND_STYLE_DATA data = MessageStyle.GetData(this.Style);
		this.m_posFrame.x = this.m_posBase.x + data.posFrm.x;
		this.m_posFrame.y = this.m_posBase.x + data.posFrm.y;
		this.m_spFrame.SetPos(this.m_posFrame.x, this.m_posFrame.y, data.posFrm.z);
		this.m_spFrame.SetImage(base.QooSprite.LoadTexture(MessageStyle.GetFrameCgName(this.Style)));
		this.m_tpMsg.z = data.posTxt.z;
		for (int num = 0; num != 2; num++)
		{
			this.m_spName[num].x = this.m_posBase.x + data.posName[num].x;
			this.m_spName[num].y = this.m_posBase.y + data.posName[num].y;
			this.m_spName[num].z = data.posName[num].z;
			this.m_spName[num].SetImage(base.QooSprite.LoadTexture("nam_common"));
			this.m_spName[num].A = 0;
			this.m_tpName[num].x = this.m_posBase.x + data.posName[num].x + 104;
			this.m_tpName[num].y = this.m_posBase.y + data.posName[num].y + 4;
			this.m_tpName[num].z = data.posName[num].z + 1;
		}
		if (CMessageText.EnabelLINETOP_PROC)
		{
			switch (this.Style)
			{
			case MSGWND_STYLE.NORMAL:
			case MSGWND_STYLE.KYARA:
			case MSGWND_STYLE.HEROINE:
			case MSGWND_STYLE.MONOLOGUE:
				this.m_MsgText.EnableLineTopProc(true);
				goto IL_236;
			}
			this.m_MsgText.EnableLineTopProc(false);
		}
		IL_236:
		this.m_MsgText.ChangePos(this.m_posBase.x + data.posTxt.x, this.m_posBase.y + data.posTxt.y, data.posTxt.z, false);
		this.m_MsgText.ChangeClipSize(data.sizeTxt.w, data.sizeTxt.h, data.nTxtWEx);
		this.m_MsgText.EnableAutoReturn(data.bTxtAutoRet);
		this.m_MsgText.SetColor(data.colTxt.r, data.colTxt.g, data.colTxt.b);
		this.m_MsgText.SetKeyTxtColor(data.colRefWord.r, data.colRefWord.g, data.colRefWord.b);
		this.m_MsgText.SetKeyBgColor(data.colRefBg.r, data.colRefBg.g, data.colRefBg.b, data.colRefBg.a);
		this.m_MsgText.SetKeyCurColor(data.colRefCur.r, data.colRefCur.g, data.colRefCur.b, data.colRefCur.a);
	}

	// Token: 0x06000577 RID: 1399 RVA: 0x00015DC8 File Offset: 0x00013FC8
	private void SetMsgSpeed()
	{
		switch (SysData.GetTextSpeed())
		{
		case 0:
			this.m_MsgText.SetAnimSpeed(1, 2);
			break;
		case 1:
			this.m_MsgText.SetAnimSpeed(1, 0);
			break;
		case 2:
			this.m_MsgText.SetAnimSpeed(2, 0);
			break;
		case 3:
			this.m_MsgText.SetAnimSpeed(255, 0);
			break;
		}
	}

	// Token: 0x06000578 RID: 1400 RVA: 0x00015E44 File Offset: 0x00014044
	public void Show(bool show)
	{
		if (!this.IsSilentMode)
		{
			this.m_tpMsg.Show = show;
			foreach (UnityTextSprite unityTextSprite in this.m_tpName)
			{
				unityTextSprite.Show = show;
			}
			foreach (UnitySprite unitySprite in this.m_spName)
			{
				unitySprite.Show = show;
			}
			this.m_spFrame.Show = show;
			this.m_spCursol.Show = show;
			this.m_spHide.Show = show;
			this.m_isVisible = show;
			this.m_spFace.Show = show;
		}
		else
		{
			show = false;
			this.m_tpMsg.Show = show;
			foreach (UnityTextSprite unityTextSprite2 in this.m_tpName)
			{
				unityTextSprite2.Show = show;
			}
			foreach (UnitySprite unitySprite2 in this.m_spName)
			{
				unitySprite2.Show = show;
			}
			this.m_spFrame.Show = show;
			this.m_spCursol.Show = show;
			this.m_spHide.Show = show;
			this.m_isVisible = show;
			this.m_spFace.Show = show;
		}
	}

	// Token: 0x06000579 RID: 1401 RVA: 0x00015FA0 File Offset: 0x000141A0
	public bool ShowHideBtn(bool isShow)
	{
		bool show = this.m_spHide.Show;
		this.m_spHide.Show = isShow;
		return show;
	}

	// Token: 0x0600057A RID: 1402 RVA: 0x00015FC8 File Offset: 0x000141C8
	public void SetName(string[] name)
	{
		Qoo.Debug.Assert(name.Length == this.m_tpName.Length);
		for (int i = 0; i < name.Length; i++)
		{
			this.m_tpName[i].ClearText();
			if (name[i].Length > 0)
			{
				this.m_tpName[i].AddText(name[i], 28);
				MSGWND_STYLE_DATA data = MessageStyle.GetData(this.Style);
				this.m_spName[i].x = this.m_posBase.x + data.posName[i].x;
				this.m_spName[i].y = this.m_posBase.y + data.posName[i].y;
				this.m_spName[i].z = data.posName[i].z;
				this.m_spName[i].SetImage(base.QooSprite.LoadTexture("nam_common"));
				this.m_spName[i].A = 0;
				int nX = this.m_spName[i].x + this.m_spName[i].w / 2;
				int nY = this.m_posBase.y + data.posName[i].y + 4;
				this.m_tpName[i].z = data.posName[i].z + 1;
				this.m_tpName[i].SetPosition(nX, nY, UnityTextSprite.PositionType.Center, UnityTextSprite.PositionType.TopLeft);
				this.m_tpName[i].A = byte.MaxValue;
				this.m_spName[i].A = byte.MaxValue;
				this.m_tpName[i].Show = true;
				this.m_spName[i].Show = true;
			}
			else
			{
				this.m_tpName[i].Clear();
				this.m_tpName[i].A = 0;
				this.m_spName[i].A = 0;
			}
		}
	}

	// Token: 0x0600057B RID: 1403 RVA: 0x0001619C File Offset: 0x0001439C
	public void SetSize(MSGFONTSIZE size_)
	{
		this.m_Size = size_;
	}

	// Token: 0x0600057C RID: 1404 RVA: 0x000161A8 File Offset: 0x000143A8
	internal void SetMesNo(int ks_, int label_, int mes_)
	{
		this.KsNo = ks_;
		this.LabelNo = label_;
		this.MesNo = mes_;
	}

	// Token: 0x0600057D RID: 1405 RVA: 0x000161C0 File Offset: 0x000143C0
	private void SetAutoSkipTime()
	{
		if (this.m_nAutoSkipTime != 0)
		{
			return;
		}
		if (Sound.VoiceIsPlay())
		{
			return;
		}
		this.m_nAutoSkipTime = base.QooApp.GetTimeMilli();
	}

	// Token: 0x0600057E RID: 1406 RVA: 0x000161F8 File Offset: 0x000143F8
	private bool CheckAutoSkip()
	{
		return this.m_isAutoSkip && this.m_nAutoSkipTime != 0 && this.m_nAutoSkipTime + SysData.GetAutoPage() * 1000 <= base.QooApp.GetTimeMilli();
	}

	// Token: 0x0600057F RID: 1407 RVA: 0x00016244 File Offset: 0x00014444
	public bool IsSkip()
	{
		return this.m_Phase == MsgWnd.MSG_PHASE.NULL;
	}

	// Token: 0x040003F9 RID: 1017
	private UnityTextSprite m_tpMsg;

	// Token: 0x040003FA RID: 1018
	private UnitySprite m_spFrame;

	// Token: 0x040003FB RID: 1019
	private UnitySprite m_spCursol;

	// Token: 0x040003FC RID: 1020
	private UnitySprite m_spHide;

	// Token: 0x040003FD RID: 1021
	private UnitySprite m_spFace;

	// Token: 0x040003FE RID: 1022
	private UnityTextSprite[] m_tpName = new UnityTextSprite[2];

	// Token: 0x040003FF RID: 1023
	private UnitySprite[] m_spName = new UnitySprite[2];

	// Token: 0x04000400 RID: 1024
	private CMessageText m_MsgText;

	// Token: 0x04000401 RID: 1025
	private bool m_isSilentMode;

	// Token: 0x04000402 RID: 1026
	private MSGFONTSIZE m_Size = MSGFONTSIZE.NORMAL;

	// Token: 0x04000403 RID: 1027
	private Color32 m_Color = new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue);

	// Token: 0x04000404 RID: 1028
	private MSGWND_STYLE m_Style = MSGWND_STYLE.NULL;

	// Token: 0x04000405 RID: 1029
	private Point2 m_posFrame = new Point2(0, 0);

	// Token: 0x04000406 RID: 1030
	private Point2 m_posBase = new Point2(0, 0);

	// Token: 0x04000407 RID: 1031
	private bool m_isHide;

	// Token: 0x04000408 RID: 1032
	private bool m_isVisible = true;

	// Token: 0x04000409 RID: 1033
	private bool m_isAutoSkip;

	// Token: 0x0400040A RID: 1034
	private int m_nAutoSkipTime;

	// Token: 0x0400040B RID: 1035
	private MsgWnd.MSG_PHASE m_Phase;

	// Token: 0x0400040C RID: 1036
	public static bool g_isAnimPosOuter = true;

	// Token: 0x020000B9 RID: 185
	private enum MSG_PHASE
	{
		// Token: 0x04000411 RID: 1041
		NULL,
		// Token: 0x04000412 RID: 1042
		MESANIM,
		// Token: 0x04000413 RID: 1043
		KEYWAIT,
		// Token: 0x04000414 RID: 1044
		MESEND,
		// Token: 0x04000415 RID: 1045
		BACKLOG,
		// Token: 0x04000416 RID: 1046
		SELWORD
	}
}
