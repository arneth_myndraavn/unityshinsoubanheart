﻿using System;

// Token: 0x020001AD RID: 429
public class UserBaseClass : UnityBaseClass
{
	// Token: 0x170001AA RID: 426
	// (get) Token: 0x06000C60 RID: 3168 RVA: 0x00032D3C File Offset: 0x00030F3C
	public UnityGraph QooGraph
	{
		get
		{
			if (Singleton<UnityGraph>.IsReady)
			{
				return Singleton<UnityGraph>.Instance;
			}
			return null;
		}
	}

	// Token: 0x170001AB RID: 427
	// (get) Token: 0x06000C61 RID: 3169 RVA: 0x00032D50 File Offset: 0x00030F50
	public UnityApp QooApp
	{
		get
		{
			if (Singleton<UnityApp>.IsReady)
			{
				return Singleton<UnityApp>.Instance;
			}
			return null;
		}
	}

	// Token: 0x170001AC RID: 428
	// (get) Token: 0x06000C62 RID: 3170 RVA: 0x00032D64 File Offset: 0x00030F64
	public Man2D QooSprite
	{
		get
		{
			if (Singleton<Man2D>.IsReady)
			{
				return Singleton<Man2D>.Instance;
			}
			return null;
		}
	}

	// Token: 0x170001AD RID: 429
	// (get) Token: 0x06000C63 RID: 3171 RVA: 0x00032D78 File Offset: 0x00030F78
	public ManSound QooSound
	{
		get
		{
			if (Singleton<ManSound>.IsReady)
			{
				return Singleton<ManSound>.Instance;
			}
			return null;
		}
	}
}
