﻿using System;

// Token: 0x0200017C RID: 380
public class Size
{
	// Token: 0x06000ACA RID: 2762 RVA: 0x0002F5F8 File Offset: 0x0002D7F8
	public Size(int w_ = 0, int h_ = 0)
	{
		this.w = w_;
		this.h = h_;
	}

	// Token: 0x040008B9 RID: 2233
	public int w;

	// Token: 0x040008BA RID: 2234
	public int h;
}
