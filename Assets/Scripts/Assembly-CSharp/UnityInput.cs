﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x02000179 RID: 377
public class UnityInput : MonoBehaviour
{
	// Token: 0x17000168 RID: 360
	// (get) Token: 0x06000AB2 RID: 2738 RVA: 0x0002F0C4 File Offset: 0x0002D2C4
	protected bool IsPush
	{
		get
		{
			return this.m_isPush;
		}
	}

	// Token: 0x17000169 RID: 361
	// (get) Token: 0x06000AB3 RID: 2739 RVA: 0x0002F0CC File Offset: 0x0002D2CC
	protected Vector2 PushPosition
	{
		get
		{
			return this.m_posNow;
		}
	}

	// Token: 0x1700016A RID: 362
	// (get) Token: 0x06000AB4 RID: 2740 RVA: 0x0002F0D4 File Offset: 0x0002D2D4
	protected Vector2 StartPosition
	{
		get
		{
			return this.m_posStart;
		}
	}

	// Token: 0x1700016B RID: 363
	// (get) Token: 0x06000AB5 RID: 2741 RVA: 0x0002F0DC File Offset: 0x0002D2DC
	protected Vector2 EndPosition
	{
		get
		{
			return this.m_posEnd;
		}
	}

	// Token: 0x1700016C RID: 364
	// (get) Token: 0x06000AB6 RID: 2742 RVA: 0x0002F0E4 File Offset: 0x0002D2E4
	protected Vector2 DeltaPosition
	{
		get
		{
			if (this.m_posOld.Count < 2)
			{
				return new Vector2(0f, 0f);
			}
			Queue<Vector2> queue = new Queue<Vector2>();
			bool flag = true;
			Vector2 b = new Vector2(0f, 0f);
			foreach (Vector2 vector in this.m_posOld)
			{
				if (flag)
				{
					flag = false;
				}
				else
				{
					queue.Enqueue(vector - b);
				}
				b = vector;
			}
			Vector2 a = new Vector2(0f, 0f);
			foreach (Vector2 b2 in queue)
			{
				a += b2;
			}
			return a / (float)queue.Count;
		}
	}

	// Token: 0x1700016D RID: 365
	// (get) Token: 0x06000AB7 RID: 2743 RVA: 0x0002F218 File Offset: 0x0002D418
	protected Vector2 StartDeltaPosition
	{
		get
		{
			return this.m_posNow - this.m_posStart;
		}
	}

	// Token: 0x1700016E RID: 366
	// (get) Token: 0x06000AB8 RID: 2744 RVA: 0x0002F22C File Offset: 0x0002D42C
	// (set) Token: 0x06000AB9 RID: 2745 RVA: 0x0002F234 File Offset: 0x0002D434
	//private 
	protected TOUCH_STATE State
	{
		//protected 
		get
		{
			return this.m_eState;
		}
		//private
		set
		{
			this.m_eState = value;
		}
	}

	// Token: 0x1700016F RID: 367
	// (get) Token: 0x06000ABA RID: 2746 RVA: 0x0002F240 File Offset: 0x0002D440
	public int Width
	{
		get
		{
			return this.m_Width;
		}
	}

	// Token: 0x17000170 RID: 368
	// (get) Token: 0x06000ABB RID: 2747 RVA: 0x0002F248 File Offset: 0x0002D448
	public int Height
	{
		get
		{
			return this.m_Height;
		}
	}

	// Token: 0x17000171 RID: 369
	// (get) Token: 0x06000ABC RID: 2748 RVA: 0x0002F250 File Offset: 0x0002D450
	// (set) Token: 0x06000ABD RID: 2749 RVA: 0x0002F258 File Offset: 0x0002D458
	public bool IsOnePushDelete { get; set; }

	// Token: 0x06000ABE RID: 2750 RVA: 0x0002F264 File Offset: 0x0002D464
	public void Awake()
	{
		this.m_Width = Screen.width;
		this.m_Height = Screen.height;
		Input.simulateMouseWithTouches = true;
		this.IsOnePushDelete = false;
	}

	// Token: 0x06000ABF RID: 2751 RVA: 0x0002F28C File Offset: 0x0002D48C
	public void Update()
	{
		if (this.IsOnePushDelete)
		{
			this.m_isPush = false;
			this.State = TOUCH_STATE.NULL;
			if (!Input.GetMouseButton(0))
			{
				this.IsOnePushDelete = false;
			}
			return;
		}
		Vector2 vector = new Vector2(Input.mousePosition.x, (float)this.m_Height - Input.mousePosition.y);
		if (Input.GetMouseButton(0))
		{
			this.UpdateNowPos(vector);
			this.m_isPush = true;
			if (Input.GetMouseButtonDown(0))
			{
				this.m_posStart = vector;
				this.ResetOld();
				this.State = TOUCH_STATE.PUSH;
				this.m_fMax = 0f;
			}
			else
			{
				float magnitude = this.DeltaPosition.magnitude;
				if (magnitude > this.m_fMax)
				{
					this.m_fMax = magnitude;
				}
				if (magnitude > 0f)
				{
					if (this.State != TOUCH_STATE.DRAG)
					{
						this.State = TOUCH_STATE.DRAG;
					}
				}
				else if (this.State != TOUCH_STATE.DRAG)
				{
					this.State = TOUCH_STATE.PUSH;
				}
			}
		}
		else if (Input.GetMouseButtonUp(0))
		{
			this.UpdateNowPos(vector);
			this.m_isPush = false;
			this.CalcEndState();
			this.m_fMax = 0f;
		}
		else
		{
			this.Clear();
		}
	}

	// Token: 0x06000AC0 RID: 2752 RVA: 0x0002F3D0 File Offset: 0x0002D5D0
	private void UpdateNowPos(Vector2 now)
	{
		this.m_posNow = now;
		if (this.m_posOld.Count > 4)
		{
			this.m_posOld.Dequeue();
		}
		this.m_posOld.Enqueue(now);
	}

	// Token: 0x06000AC1 RID: 2753 RVA: 0x0002F410 File Offset: 0x0002D610
	private void ResetOld()
	{
		this.m_posOld.Clear();
	}

	// Token: 0x06000AC2 RID: 2754 RVA: 0x0002F420 File Offset: 0x0002D620
	public void Clear()
	{
		this.m_posStart = new Vector2(0f, 0f);
		this.m_posOld.Clear();
		this.m_posNow = new Vector2(0f, 0f);
		this.m_posEnd = new Vector2(0f, 0f);
		this.m_isPush = false;
		this.m_eState = TOUCH_STATE.NULL;
	}

	// Token: 0x06000AC3 RID: 2755 RVA: 0x0002F488 File Offset: 0x0002D688
	private void CalcEndState()
	{
		float magnitude = this.DeltaPosition.magnitude;
		float magnitude2 = this.StartDeltaPosition.magnitude;
		if (magnitude > 32f)
		{
			this.State = TOUCH_STATE.FLICK;
			this.m_posEnd = this.m_posNow;
		}
		else if (magnitude2 < 16f && this.m_fMax < 16f)
		{
			this.State = TOUCH_STATE.CLICK;
			this.m_posEnd = this.m_posNow;
		}
		else
		{
			this.State = TOUCH_STATE.DRAG_END;
			this.m_posEnd = this.m_posNow;
		}
	}

	// Token: 0x06000AC4 RID: 2756 RVA: 0x0002F51C File Offset: 0x0002D71C
	public void ResetStartPosition()
	{
		if (this.IsPush)
		{
			this.m_posStart = this.m_posNow;
		}
	}

	// Token: 0x06000AC5 RID: 2757 RVA: 0x0002F538 File Offset: 0x0002D738
	private void SetLeaveEvent()
	{
		this.m_posEnd = this.m_posNow;
		this.State = TOUCH_STATE.NULL;
	}

	// Token: 0x040008A7 RID: 2215
	private const int DEF_OLD_BUF = 4;

	// Token: 0x040008A8 RID: 2216
	private const float DEF_FLICK_DELTA = 32f;

	// Token: 0x040008A9 RID: 2217
	private const float DEF_CLICK_DELTA = 16f;

	// Token: 0x040008AA RID: 2218
	private Vector2 m_posStart = default(Vector2);

	// Token: 0x040008AB RID: 2219
	private Queue<Vector2> m_posOld = new Queue<Vector2>();

	// Token: 0x040008AC RID: 2220
	private Vector2 m_posNow = default(Vector2);

	// Token: 0x040008AD RID: 2221
	private Vector2 m_posEnd = default(Vector2);

	// Token: 0x040008AE RID: 2222
	private float m_fMax;

	// Token: 0x040008AF RID: 2223
	private bool m_isPush;

	// Token: 0x040008B0 RID: 2224
	private TOUCH_STATE m_eState;

	// Token: 0x040008B1 RID: 2225
	private int m_Width;

	// Token: 0x040008B2 RID: 2226
	private int m_Height;
}
