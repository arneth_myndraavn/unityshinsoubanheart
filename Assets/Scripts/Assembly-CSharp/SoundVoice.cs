﻿using System;
using System.Collections;
using Qoo.Application;
using UnityEngine;

// Token: 0x0200018C RID: 396
public class SoundVoice : MonoBehaviour
{
	// Token: 0x1700017B RID: 379
	// (get) Token: 0x06000B48 RID: 2888 RVA: 0x00030778 File Offset: 0x0002E978
	// (set) Token: 0x06000B49 RID: 2889 RVA: 0x00030780 File Offset: 0x0002E980
	public string Name
	{
		get
		{
			return this.sourceName;
		}
		private set
		{
			this.sourceName = value;
		}
	}

	// Token: 0x1700017C RID: 380
	// (get) Token: 0x06000B4A RID: 2890 RVA: 0x0003078C File Offset: 0x0002E98C
	// (set) Token: 0x06000B4B RID: 2891 RVA: 0x00030794 File Offset: 0x0002E994
	public bool IsPause
	{
		get
		{
			return this.isPause;
		}
		private set
		{
			this.isPause = value;
		}
	}

	// Token: 0x1700017D RID: 381
	// (get) Token: 0x06000B4C RID: 2892 RVA: 0x000307A0 File Offset: 0x0002E9A0
	public bool IsPlay
	{
		get
		{
			return this.voice.isPlaying || this.isStart;
		}
	}

	// Token: 0x1700017E RID: 382
	// (get) Token: 0x06000B4D RID: 2893 RVA: 0x000307BC File Offset: 0x0002E9BC
	// (set) Token: 0x06000B4E RID: 2894 RVA: 0x000307C4 File Offset: 0x0002E9C4
	public int FadeInTime
	{
		get
		{
			return this.timeIn;
		}
		set
		{
			this.timeIn = value;
		}
	}

	// Token: 0x1700017F RID: 383
	// (get) Token: 0x06000B4F RID: 2895 RVA: 0x000307D0 File Offset: 0x0002E9D0
	// (set) Token: 0x06000B50 RID: 2896 RVA: 0x000307D8 File Offset: 0x0002E9D8
	public int FadeOutTime
	{
		get
		{
			return this.timeOut;
		}
		set
		{
			this.timeOut = value;
		}
	}

	// Token: 0x17000180 RID: 384
	// (get) Token: 0x06000B51 RID: 2897 RVA: 0x000307E4 File Offset: 0x0002E9E4
	// (set) Token: 0x06000B52 RID: 2898 RVA: 0x000307EC File Offset: 0x0002E9EC
	public float Volume
	{
		get
		{
			return this.volume;
		}
		set
		{
			this.volume = value;
		}
	}

	// Token: 0x17000181 RID: 385
	// (get) Token: 0x06000B53 RID: 2899 RVA: 0x000307F8 File Offset: 0x0002E9F8
	public float OutVolume
	{
		get
		{
			return this.voice.volume;
		}
	}

	// Token: 0x17000182 RID: 386
	// (get) Token: 0x06000B54 RID: 2900 RVA: 0x00030808 File Offset: 0x0002EA08
	public bool IsFade
	{
		get
		{
			return this.isFadeIn || this.isFadeOut;
		}
	}

	// Token: 0x17000183 RID: 387
	// (get) Token: 0x06000B55 RID: 2901 RVA: 0x00030820 File Offset: 0x0002EA20
	// (set) Token: 0x06000B56 RID: 2902 RVA: 0x00030830 File Offset: 0x0002EA30
	public bool Loop
	{
		get
		{
			return this.voice.loop;
		}
		set
		{
			this.voice.loop = value;
		}
	}

	// Token: 0x17000184 RID: 388
	// (get) Token: 0x06000B57 RID: 2903 RVA: 0x00030840 File Offset: 0x0002EA40
	public bool IsSource
	{
		get
		{
			return this.voice.clip != null;
		}
	}

	// Token: 0x06000B58 RID: 2904 RVA: 0x00030854 File Offset: 0x0002EA54
	public static SoundVoice CreateObject(GameObject parent, string name)
	{
		return new GameObject("SoundVoice." + name)
		{
			transform = 
			{
				parent = parent.transform
			}
		}.gameObject.AddComponent<SoundVoice>();
	}

	// Token: 0x06000B59 RID: 2905 RVA: 0x00030890 File Offset: 0x0002EA90
	private void Awake()
	{
		this.voice = base.gameObject.AddComponent<AudioSource>();
		this.voice.spatialBlend = 0f;
	}

	// Token: 0x06000B5A RID: 2906 RVA: 0x000308B4 File Offset: 0x0002EAB4
	private void Update()
	{
		if (this.m_data != null && this.voice.clip == null && this.m_data.Data != null)
		{
			this.voice.clip = this.m_data.Data;
			this.voice.spatialBlend = 0f;
			this.m_data = null;
		}
		if (this.isStart && this.voice.clip != null)
		{
			this.Play();
		}
		if (!this.IsFade)
		{
			this.voice.volume = this.Volume;
		}
	}

	// Token: 0x06000B5B RID: 2907 RVA: 0x00030968 File Offset: 0x0002EB68
	public bool SetSource(string category, string name_)
	{
		this.Name = name_;
		this.isStart = false;
		this.isFadeOut = false;
		this.isFadeIn = false;
		if (this.voice.clip != null)
		{
			this.voice.Stop();
			Resources.UnloadAsset(this.voice.clip);
		}
		this.voice.clip = null;
		this.voice.clip = SoundData.Load(category, name_);
		this.voice.spatialBlend = 0f;
		return this.voice.clip != null;
	}

	// Token: 0x06000B5C RID: 2908 RVA: 0x00030A04 File Offset: 0x0002EC04
	public void Play(SoundData data, float fVol = 1f)
	{
		if (data != null)
		{
			this.voice.PlayOneShot(data.Data, fVol);
		}
	}

	// Token: 0x06000B5D RID: 2909 RVA: 0x00030A20 File Offset: 0x0002EC20
	public void Play()
	{
		this.isStart = false;
		if (this.voice.clip == null)
		{
			this.isStart = true;
			this.isFadeOut = false;
			this.isFadeIn = false;
		}
		else
		{
			this.isFadeOut = false;
			this.voice.Play();
			if (this.FadeInTime > 0)
			{
				this.isFadeIn = true;
				base.StartCoroutine(this.FadeIn(0f, this.Volume));
			}
			this.IsPause = false;
		}
	}

	// Token: 0x06000B5E RID: 2910 RVA: 0x00030AA8 File Offset: 0x0002ECA8
	public void Stop()
	{
		if (this.isFadeOut && this.FadeOutTime > 0)
		{
			this.isFadeOut = true;
			this.isStart = false;
			this.isFadeIn = false;
			base.StartCoroutine(this.FadeOut(this.Volume, 0f));
		}
		else
		{
			this.isFadeOut = false;
			this.isFadeIn = false;
			this.isStart = false;
			this.voice.Stop();
			Resources.UnloadAsset(this.voice.clip);
			this.voice.clip = null;
		}
	}

	// Token: 0x06000B5F RID: 2911 RVA: 0x00030B3C File Offset: 0x0002ED3C
	public void Pause(bool bPause)
	{
		if (this.IsSource)
		{
			if (bPause && this.IsPlay)
			{
				this.voice.Pause();
			}
			else if (!bPause && this.IsPause)
			{
				this.voice.Play();
			}
			this.IsPause = bPause;
		}
	}

	// Token: 0x06000B60 RID: 2912 RVA: 0x00030B98 File Offset: 0x0002ED98
	private IEnumerator FadeOut(float volIn, float volOut)
	{
		if (!this.isFadeOut)
		{
			yield break;
		}
		int start = App.GetTimeMilli();
		int end = start + this.FadeInTime;
		for (int now = start; now < end; now = App.GetTimeMilli())
		{
			if (!this.isFadeOut)
			{
				yield break;
			}
			this.voice.volume = Mathf.Lerp(volOut, volIn, (float)(now / end));
			yield return 0;
		}
		if (this.isFadeOut)
		{
			this.voice.volume = volOut;
			if (volOut == 0f)
			{
				this.voice.Stop();
				Resources.UnloadAsset(this.voice.clip);
				this.voice.clip = null;
			}
			this.isFadeOut = false;
		}
		yield break;
	}

	// Token: 0x06000B61 RID: 2913 RVA: 0x00030BD0 File Offset: 0x0002EDD0
	private IEnumerator FadeIn(float volIn, float volOut)
	{
		if (!this.isFadeIn)
		{
			yield break;
		}
		int start = App.GetTimeMilli();
		int end = start + this.FadeInTime;
		for (int now = start; now < end; now = App.GetTimeMilli())
		{
			if (!this.isFadeIn)
			{
				yield break;
			}
			this.voice.volume = Mathf.Lerp(volOut, volIn, (float)(now / end));
			yield return 0;
		}
		if (this.isFadeIn)
		{
			this.voice.volume = volOut;
			this.isFadeIn = false;
		}
		yield break;
	}

	// Token: 0x040008EC RID: 2284
	private AudioSource voice;

	// Token: 0x040008ED RID: 2285
	private SoundData m_data;

	// Token: 0x040008EE RID: 2286
	private bool isPause;

	// Token: 0x040008EF RID: 2287
	private bool isFadeIn;

	// Token: 0x040008F0 RID: 2288
	private bool isFadeOut;

	// Token: 0x040008F1 RID: 2289
	private string sourceName;

	// Token: 0x040008F2 RID: 2290
	private int timeIn = 1000;

	// Token: 0x040008F3 RID: 2291
	private int timeOut = 1000;

	// Token: 0x040008F4 RID: 2292
	private float volume = 1f;

	// Token: 0x040008F5 RID: 2293
	private bool isStart;
}
