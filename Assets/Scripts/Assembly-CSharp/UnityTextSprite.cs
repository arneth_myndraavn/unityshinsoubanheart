﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x0200015C RID: 348
public class UnityTextSprite : UnitySprite
{
	// Token: 0x060009FB RID: 2555 RVA: 0x0002C3F4 File Offset: 0x0002A5F4
	public UnityTextSprite(bool IsSprite = true, GameObject parent = null) : base(IsSprite, parent, true)
	{
		this.mFilter = this.obj.GetComponent<MeshFilter>();
		this.mesh = this.mFilter.sharedMesh;
		if (this.mesh == null)
		{
			this.mesh = new Mesh();
			this.mesh.hideFlags = HideFlags.DontSave;
			this.mFilter.sharedMesh = this.mesh;
		}
		this.IsUseTex = true;
	}

	// Token: 0x1700013C RID: 316
	// (get) Token: 0x060009FC RID: 2556 RVA: 0x0002C4E0 File Offset: 0x0002A6E0
	// (set) Token: 0x060009FD RID: 2557 RVA: 0x0002C4F4 File Offset: 0x0002A6F4
	public override int w
	{
		get
		{
			return (int)((float)this.OrgW * this.fWidth);
		}
		set
		{
			this.fWidth = (float)(value / this.OrgW);
		}
	}

	// Token: 0x1700013D RID: 317
	// (get) Token: 0x060009FE RID: 2558 RVA: 0x0002C508 File Offset: 0x0002A708
	// (set) Token: 0x060009FF RID: 2559 RVA: 0x0002C51C File Offset: 0x0002A71C
	public override int h
	{
		get
		{
			return (int)((float)this.OrgH * this.fHeight);
		}
		set
		{
			this.fHeight = (float)(value / this.OrgH);
		}
	}

	// Token: 0x1700013E RID: 318
	// (get) Token: 0x06000A00 RID: 2560 RVA: 0x0002C530 File Offset: 0x0002A730
	// (set) Token: 0x06000A01 RID: 2561 RVA: 0x0002C538 File Offset: 0x0002A738
	public int MarginX
	{
		get
		{
			return this.marginX;
		}
		set
		{
			this.marginX = value;
		}
	}

	// Token: 0x1700013F RID: 319
	// (get) Token: 0x06000A02 RID: 2562 RVA: 0x0002C544 File Offset: 0x0002A744
	// (set) Token: 0x06000A03 RID: 2563 RVA: 0x0002C54C File Offset: 0x0002A74C
	public int MarginY
	{
		get
		{
			return this.marginY;
		}
		set
		{
			this.marginY = value;
		}
	}

	// Token: 0x06000A04 RID: 2564 RVA: 0x0002C558 File Offset: 0x0002A758
	public void SetPosition(int nX, int nY, UnityTextSprite.PositionType px = UnityTextSprite.PositionType.TopLeft, UnityTextSprite.PositionType py = UnityTextSprite.PositionType.TopLeft)
	{
		if (px != UnityTextSprite.PositionType.TopLeft || py != UnityTextSprite.PositionType.TopLeft)
		{
			this.CalcSize();
			this.x = nX + this.calcOfs(this.w, px);
			this.y = nY + this.calcOfs(this.h, py);
		}
		else
		{
			this.x = nX;
			this.y = nY;
		}
	}

	// Token: 0x06000A05 RID: 2565 RVA: 0x0002C5B8 File Offset: 0x0002A7B8
	private int calcOfs(int wh, UnityTextSprite.PositionType p)
	{
		switch (p)
		{
		case UnityTextSprite.PositionType.TopLeft:
			return 0;
		case UnityTextSprite.PositionType.Center:
			return -wh / 2;
		case UnityTextSprite.PositionType.BottomRight:
			return -wh;
		default:
			return 0;
		}
	}

	// Token: 0x17000140 RID: 320
	// (get) Token: 0x06000A06 RID: 2566 RVA: 0x0002C5EC File Offset: 0x0002A7EC
	// (set) Token: 0x06000A07 RID: 2567 RVA: 0x0002C5F4 File Offset: 0x0002A7F4
	public UIFont Font
	{
		get
		{
			return this.font;
		}
		set
		{
			this.font = value;
			if (this.font != null)
			{
				this.tex = this.font.texture;
			}
		}
	}

	// Token: 0x17000141 RID: 321
	// (get) Token: 0x06000A08 RID: 2568 RVA: 0x0002C620 File Offset: 0x0002A820
	public override bool IsTexRelease
	{
		get
		{
			return false;
		}
	}

	// Token: 0x06000A09 RID: 2569 RVA: 0x0002C624 File Offset: 0x0002A824
	public void ClearText()
	{
		this.text.Clear();
		this.linePos.Clear();
		this.charPosX = 0;
		this.charPosY = 0;
	}

	// Token: 0x06000A0A RID: 2570 RVA: 0x0002C658 File Offset: 0x0002A858
	public bool AddText(string str_, int size)
	{
		int charSize = this.font.Font.charSize;
		float scale_ = this.Scale(charSize, size);
		foreach (char c in str_)
		{
			if (c == '\n')
			{
				this.charPosX = 0;
				this.charPosY += size + this.MarginY;
				this.linePos.Add(this.text.Count);
			}
			else
			{
				BMGlyph glyph = this.font.Font.GetGlyph((int)c);
				int num = size;
				if (glyph != null)
				{
					this.text.Add(new CharObject(c, size, this.charPosX, this.charPosY, glyph, scale_, this.textColor));
					float num2 = (float)glyph.advance / (float)this.font.Font.charSize;
					num = (int)((float)size * num2);
				}
				this.charPosX += num + this.MarginX;
			}
		}
		this.isUpdate = true;
		return true;
	}

	// Token: 0x06000A0B RID: 2571 RVA: 0x0002C76C File Offset: 0x0002A96C
	public bool Normalize()
	{
		int top_y = 0;
		int num = 0;
		int num2;
		if (this.linePos.Count > 0)
		{
			for (int i = 0; i < this.linePos.Count; i++)
			{
				num2 = this.linePos[i];
				top_y = this.SortUnderLine(num, num2, top_y) + this.MarginY;
				num = num2;
			}
		}
		num2 = this.text.Count;
		if (num2 != num)
		{
			top_y = this.SortUnderLine(num, num2, top_y);
		}
		this.isUpdate = true;
		return true;
	}

	// Token: 0x06000A0C RID: 2572 RVA: 0x0002C7F4 File Offset: 0x0002A9F4
	public bool SetArray(CharObject[] array)
	{
		int charSize = this.font.Font.charSize;
		this.ClearText();
		foreach (CharObject charObject in array)
		{
			if (charObject.color.a != 0)
			{
				if (charObject.code != '\0')
				{
					if (charObject.size != 0)
					{
						if (charObject.glyph == null)
						{
							charObject.glyph = this.font.Font.GetGlyph((int)charObject.code);
						}
						if (charObject.glyph != null)
						{
							charObject.scale = this.Scale(charSize, charObject.size);
							this.text.Add(charObject);
						}
					}
				}
			}
		}
		this.isUpdate = true;
		return true;
	}

	// Token: 0x06000A0D RID: 2573 RVA: 0x0002C8C0 File Offset: 0x0002AAC0
	public void CalcSize()
	{
		if (this.isUpdate)
		{
			int num = 0;
			int num2 = 0;
			foreach (CharObject charObject in this.text)
			{
				num2 = Math.Max(charObject.X + charObject.Width, num2);
				num = Math.Max(charObject.Y + charObject.Height, num);
			}
			this.OrgW = (this.UW = num2);
			this.OrgH = (this.VH = num);
		}
	}

	// Token: 0x06000A0E RID: 2574 RVA: 0x0002C97C File Offset: 0x0002AB7C
	public void Clear()
	{
		this.text.Clear();
		this.linePos.Clear();
		this.mesh.Clear();
		this.verts.Clear();
		this.uv.Clear();
		this.color.Clear();
		int num = 0;
		this.y = num;
		this.x = num;
		this.fWidth = 1f;
		this.fHeight = 1f;
		this.isUpdate = true;
	}

	// Token: 0x06000A0F RID: 2575 RVA: 0x0002C9F8 File Offset: 0x0002ABF8
	public override void Update(int ScrW, int ScrH, float worldZ)
	{
		if (this.isUpdate)
		{
			this.CalcSize();
			this.verts.Clear();
			this.uv.Clear();
			this.color.Clear();
			this.font.Print(this.text, this.verts, this.uv, this.color);
			this.BuildMesh();
			this.isUpdate = false;
		}
		if (this.IsUseTex)
		{
			if (this.tex != null)
			{
				this.obj.GetComponent<Renderer>().material.mainTexture = this.tex.m_Texture;
			}
			else
			{
				this.obj.GetComponent<Renderer>().material.mainTexture = null;
			}
		}
		this.obj.transform.localPosition = new Vector3((float)(this.x - ScrW / 2), (float)(-(float)this.y + ScrH / 2), worldZ);
		this.obj.transform.localScale = new Vector3(this.fWidth, this.fHeight, 0f);
		this.obj.GetComponent<Renderer>().material.color = new Color((float)this.R / 255f, (float)this.G / 255f, (float)this.B / 255f, (float)this.A / 255f);
		this.obj.SetActive(this.Show);
	}

	// Token: 0x06000A10 RID: 2576 RVA: 0x0002CB6C File Offset: 0x0002AD6C
	private void BuildMesh()
	{
		int count = this.verts.Count;
		int num = (count >> 1) * 3;
		bool flag = this.mIndices == null || this.mIndices.Length != num;
		if (flag)
		{
			int num2 = 0;
			this.mIndices = new int[num];
			for (int i = 0; i < count; i += 4)
			{
				this.mIndices[num2++] = i;
				this.mIndices[num2++] = i + 1;
				this.mIndices[num2++] = i + 2;
				this.mIndices[num2++] = i + 2;
				this.mIndices[num2++] = i + 3;
				this.mIndices[num2++] = i;
			}
			this.mesh.Clear();
		}
		if (this.verts.Count > 0)
		{
			this.mesh.vertices = this.verts.ToArray();
		}
		if (this.uv.Count > 0)
		{
			this.mesh.uv = this.uv.ToArray();
		}
		if (this.color.Count > 0)
		{
			this.mesh.colors32 = this.color.ToArray();
		}
		if (flag)
		{
			this.mesh.triangles = this.mIndices;
		}
		this.mesh.RecalculateBounds();
        if (this.mFilter != null)
        {
			this.mFilter.mesh = this.mesh;
		}
		
	}

	// Token: 0x06000A11 RID: 2577 RVA: 0x0002CCE4 File Offset: 0x0002AEE4
	private float Scale(int size, int base_size)
	{
		return (float)base_size / (float)size;
	}

	// Token: 0x06000A12 RID: 2578 RVA: 0x0002CCEC File Offset: 0x0002AEEC
	private int SortUnderLine(int now, int end, int top_y)
	{
		int num = 0;
		foreach (CharObject charObject in this.text.GetRange(now, end - now))
		{
			num = Math.Max(num, Math.Max(charObject.Y, top_y) + charObject.Height);
		}
		foreach (CharObject charObject2 in this.text.GetRange(now, end - now))
		{
			charObject2.Y = num - charObject2.Height;
		}
		return num;
	}

	// Token: 0x06000A13 RID: 2579 RVA: 0x0002CDD8 File Offset: 0x0002AFD8
	public void SetTextColor(Color32 col_)
	{
		this.textColor = col_;
	}

	// Token: 0x04000823 RID: 2083
	private UIFont font;

	// Token: 0x04000824 RID: 2084
	private List<CharObject> text = new List<CharObject>();

	// Token: 0x04000825 RID: 2085
	private List<int> linePos = new List<int>();

	// Token: 0x04000826 RID: 2086
	private bool isUpdate;

	// Token: 0x04000827 RID: 2087
	private Mesh mesh;

	// Token: 0x04000828 RID: 2088
	private MeshFilter mFilter;

	// Token: 0x04000829 RID: 2089
	private List<Vector3> verts = new List<Vector3>();

	// Token: 0x0400082A RID: 2090
	private List<Vector2> uv = new List<Vector2>();

	// Token: 0x0400082B RID: 2091
	private List<Color32> color = new List<Color32>();

	// Token: 0x0400082C RID: 2092
	private Color32 textColor = new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue);

	// Token: 0x0400082D RID: 2093
	private int[] mIndices;

	// Token: 0x0400082E RID: 2094
	private int charPosX;

	// Token: 0x0400082F RID: 2095
	private int charPosY;

	// Token: 0x04000830 RID: 2096
	private float fWidth = 1f;

	// Token: 0x04000831 RID: 2097
	private float fHeight = 1f;

	// Token: 0x04000832 RID: 2098
	private int marginX;

	// Token: 0x04000833 RID: 2099
	private int marginY = 1;

	// Token: 0x0200015D RID: 349
	public enum PositionType
	{
		// Token: 0x04000835 RID: 2101
		TopLeft,
		// Token: 0x04000836 RID: 2102
		Center,
		// Token: 0x04000837 RID: 2103
		BottomRight
	}
}
