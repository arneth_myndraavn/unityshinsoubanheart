﻿using System;
using System.Collections.Generic;

// Token: 0x0200000D RID: 13
public class TextItem
{
	// Token: 0x1700000B RID: 11
	// (get) Token: 0x06000038 RID: 56 RVA: 0x00002604 File Offset: 0x00000804
	public int ID
	{
		get
		{
			return this.m_id;
		}
	}

	// Token: 0x1700000C RID: 12
	// (get) Token: 0x06000039 RID: 57 RVA: 0x0000260C File Offset: 0x0000080C
	public string Name
	{
		get
		{
			return this.m_Name;
		}
	}

	// Token: 0x1700000D RID: 13
	// (get) Token: 0x0600003A RID: 58 RVA: 0x00002614 File Offset: 0x00000814
	public TextItem Parent
	{
		get
		{
			return this.m_Parent;
		}
	}

	// Token: 0x1700000E RID: 14
	// (get) Token: 0x0600003B RID: 59 RVA: 0x0000261C File Offset: 0x0000081C
	public List<TextItem> Child
	{
		get
		{
			return this.m_Child;
		}
	}

	// Token: 0x0600003C RID: 60 RVA: 0x00002624 File Offset: 0x00000824
	public bool Create(int id, TextItem parent, string name)
	{
		this.m_id = id;
		this.m_Parent = parent;
		if (parent != null)
		{
			parent.AddChild(this);
		}
		this.m_Name = name;
		return true;
	}

	// Token: 0x0600003D RID: 61 RVA: 0x00002658 File Offset: 0x00000858
	public bool Release()
	{
		foreach (TextItem textItem in this.m_Child)
		{
			textItem.Release();
		}
		if (this.Parent != null)
		{
			this.Parent.RemoveChild(this);
		}
		this.m_Name = string.Empty;
		return true;
	}

	// Token: 0x0600003E RID: 62 RVA: 0x000026E4 File Offset: 0x000008E4
	private bool AddChild(TextItem item)
	{
		this.m_Child.Add(item);
		return true;
	}

	// Token: 0x0600003F RID: 63 RVA: 0x000026F4 File Offset: 0x000008F4
	private bool RemoveChild(TextItem item)
	{
		for (int num = 0; num != this.m_Child.Count; num++)
		{
			if (item.ID == this.m_Child[num].ID)
			{
				this.m_Child.RemoveAt(num);
			}
		}
		return true;
	}

	// Token: 0x06000040 RID: 64 RVA: 0x00002748 File Offset: 0x00000948
	public TextItem SearchItem(int id)
	{
		if (this.m_id == id)
		{
			return this;
		}
		foreach (TextItem textItem in this.m_Child)
		{
			TextItem textItem2 = textItem.SearchItem(id);
			if (textItem2 != null)
			{
				return textItem2;
			}
		}
		return null;
	}

	// Token: 0x0400001E RID: 30
	private int m_id = -1;

	// Token: 0x0400001F RID: 31
	private TextItem m_Parent;

	// Token: 0x04000020 RID: 32
	private string m_Name = string.Empty;

	// Token: 0x04000021 RID: 33
	private List<TextItem> m_Child = new List<TextItem>();
}
