﻿using System;
using System.Collections;
using Qoo.Game;
using UnityEngine;

// Token: 0x020000EF RID: 239
public class OptionSoundMeter : Meter
{
	// Token: 0x06000678 RID: 1656 RVA: 0x0001ADAC File Offset: 0x00018FAC
	public void Init(int width, int height, SoundOptionType type)
	{
		this.m_Type = type;
		switch (this.m_Type)
		{
		case SoundOptionType.BGM:
			this.m_Value = SysData.GetVolumeBgm();
			break;
		case SoundOptionType.SE:
			this.m_Value = SysData.GetVolumeSe();
			break;
		case SoundOptionType.SYSTEM:
			this.m_Value = SysData.GetVolumeSys();
			break;
		}
		base.Init();
	}

	// Token: 0x06000679 RID: 1657 RVA: 0x0001AE14 File Offset: 0x00019014
	protected override IEnumerator WaitLoadTextureCoroutine()
	{
		while (base.gameObject.GetComponent<Renderer>().material.mainTexture == null)
		{
			yield return 0;
		}
		this.UpdateMeter((float)this.m_Value / 5f);
		yield break;
	}

	// Token: 0x0600067A RID: 1658 RVA: 0x0001AE30 File Offset: 0x00019030
	public override void OnEdit(float value)
	{
		float num = value * 5f;
		if (num > 0f && num < 1f)
		{
			num = 1f;
		}
		else
		{
			num = Mathf.Round(num);
		}
		if (this.m_Value != (int)num)
		{
			this.UpdateVolume((int)num);
		}
	}

	// Token: 0x0600067B RID: 1659 RVA: 0x0001AE84 File Offset: 0x00019084
	private void UpdateVolume(int volume)
	{
		this.m_Value = volume;
		switch (this.m_Type)
		{
		case SoundOptionType.BGM:
			SysData.SetVolumeBgm(this.m_Value);
			break;
		case SoundOptionType.SE:
			SysData.SetVolumeSe(this.m_Value);
			break;
		case SoundOptionType.SYSTEM:
			SysData.SetVolumeSys(this.m_Value);
			break;
		}
		SysData.Apply();
	}

	// Token: 0x0600067C RID: 1660 RVA: 0x0001AEEC File Offset: 0x000190EC
	public override void OnEditEnd()
	{
		this.UpdateMeter((float)this.m_Value / 5f);
	}

	// Token: 0x040005E7 RID: 1511
	private SoundOptionType m_Type;
}
