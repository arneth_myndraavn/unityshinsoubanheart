﻿using System;
using System.Collections;
using UnityEngine;

// Token: 0x020000DD RID: 221
public class NameInputKeyboard : MonoBehaviour
{
	// Token: 0x170000D3 RID: 211
	// (get) Token: 0x0600060B RID: 1547 RVA: 0x00019294 File Offset: 0x00017494
	private static NameInputKeyboard Instance
	{
		get
		{
			if (NameInputKeyboard.m_instance == null)
			{
				GameObject gameObject = GameObject.Find("_NameInputKeyboard");
				if (gameObject == null)
				{
					gameObject = new GameObject("_NameInputKeyboard");
				}
				NameInputKeyboard.m_instance = gameObject.AddComponent<NameInputKeyboard>();
				NameInputKeyboard.m_instance.m_inputText = string.Empty;
                NameInputKeyboard.m_instance.m_inputTextLastName = string.Empty;
            }
			return NameInputKeyboard.m_instance;
		}
	}

	// Token: 0x170000D4 RID: 212
	// (get) Token: 0x0600060C RID: 1548 RVA: 0x000192F4 File Offset: 0x000174F4
	public static string InputText
	{
		get
		{
			return NameInputKeyboard.Instance.m_inputText;
		}
	}

	// Token: 0x170000D5 RID: 213
	// (set) Token: 0x0600060D RID: 1549 RVA: 0x00019300 File Offset: 0x00017500
	public static string DebugInputText
	{
		set
		{
			NameInputKeyboard.Instance.m_inputText = value;
		}
	}

    // Token: 0x170000D4 RID: 212
    // (get) Token: 0x0600060C RID: 1548 RVA: 0x000192F4 File Offset: 0x000174F4
    public static string InputTextLastName
    {
        get
        {
            return NameInputKeyboard.Instance.m_inputTextLastName;
        }
    }

    // Token: 0x170000D5 RID: 213
    // (set) Token: 0x0600060D RID: 1549 RVA: 0x00019300 File Offset: 0x00017500
    public static string DebugInputTextLastName
    {
        set
        {
            NameInputKeyboard.Instance.m_inputTextLastName = value;
        }
    }

    // Token: 0x0600060E RID: 1550 RVA: 0x00019310 File Offset: 0x00017510
    public static IEnumerator Open(string defaultString = "", string defaultString2 = "", bool isUnitySubTask = false)
	{
		if (isUnitySubTask)
		{
            //Debug.LogWarning("Name" + defaultString + " = " + defaultString2);
            yield return NameInputKeyboard.Instance.StartCoroutine(ScreenKeyboardManager.Open(defaultString, defaultString2));
		}
		else
		{
			yield return ScreenKeyboardManager.Open(defaultString, defaultString2);
		}
		if (ScreenKeyboardManager.IsCancel)
		{
            if (UnityApp.Instance.isJapanese)
            {
				NameInputKeyboard.Instance.m_inputText = "アリス";
                NameInputKeyboard.Instance.m_inputTextLastName = "リデル";
            }
            else
            {
				NameInputKeyboard.Instance.m_inputText = "Alice";
                NameInputKeyboard.Instance.m_inputTextLastName = "Liddell";
            }
			
		}
		else
		{
			NameInputKeyboard.Instance.m_inputText = ScreenKeyboardManager.InputText;
            NameInputKeyboard.Instance.m_inputTextLastName = ScreenKeyboardManager.InputTextLastName;
        }
		yield break;
	}

	// Token: 0x0600060F RID: 1551 RVA: 0x00019340 File Offset: 0x00017540
	public static void Close()
	{
		if (NameInputKeyboard.m_instance != null)
		{
			UnityEngine.Object.Destroy(NameInputKeyboard.m_instance.gameObject);
			NameInputKeyboard.m_instance = null;
		}
	}

	// Token: 0x040005A7 RID: 1447
	private static NameInputKeyboard m_instance;

	// Token: 0x040005A8 RID: 1448
	private string m_inputText;

    private string m_inputTextLastName;
}
