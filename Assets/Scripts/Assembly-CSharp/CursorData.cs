﻿using System;
using Qoo;
using UnityEngine;

// Token: 0x020000AF RID: 175
public class CursorData
{
	// Token: 0x170000B9 RID: 185
	// (get) Token: 0x0600050E RID: 1294 RVA: 0x000135C4 File Offset: 0x000117C4
	// (set) Token: 0x0600050F RID: 1295 RVA: 0x000135CC File Offset: 0x000117CC
	public int Num
	{
		get
		{
			return this.nKeyNum;
		}
		set
		{
			this.nKeyNum = value;
		}
	}

	// Token: 0x170000BA RID: 186
	// (get) Token: 0x06000510 RID: 1296 RVA: 0x000135D8 File Offset: 0x000117D8
	// (set) Token: 0x06000511 RID: 1297 RVA: 0x000135E0 File Offset: 0x000117E0
	public int Select
	{
		get
		{
			return this.nSelectKey;
		}
		set
		{
			this.nSelectKey = value;
		}
	}

	// Token: 0x170000BB RID: 187
	// (get) Token: 0x06000512 RID: 1298 RVA: 0x000135EC File Offset: 0x000117EC
	// (set) Token: 0x06000513 RID: 1299 RVA: 0x000135F4 File Offset: 0x000117F4
	public Color32 CursorColor
	{
		get
		{
			return this.colCur;
		}
		set
		{
			this.colCur = value;
		}
	}

	// Token: 0x170000BC RID: 188
	// (get) Token: 0x06000514 RID: 1300 RVA: 0x00013600 File Offset: 0x00011800
	// (set) Token: 0x06000515 RID: 1301 RVA: 0x00013608 File Offset: 0x00011808
	public Color32 BgColor
	{
		get
		{
			return this.colBg;
		}
		set
		{
			this.colBg = value;
		}
	}

	// Token: 0x06000516 RID: 1302 RVA: 0x00013614 File Offset: 0x00011814
	public void Init()
	{
		for (int i = 0; i < 8; i++)
		{
			this.ahSp[i] = Man2D.Sprite(string.Empty);
			this.ahSp[i].A = 0;
			this.ahSp[i].SetSize(0, 0);
			this.ahSp[i].Show = true;
			this.ahSp[i].SetName("CURSOR:" + i);
			this.anKey[i] = -1;
		}
		this.nKeyNum = 0;
	}

	// Token: 0x06000517 RID: 1303 RVA: 0x000136A0 File Offset: 0x000118A0
	public void Release()
	{
		if (Singleton<Man2D>.IsReady)
		{
			for (int i = 0; i < 8; i++)
			{
				Singleton<Man2D>.Instance.RemoveSprite(this.ahSp[i]);
				this.ahSp[i] = null;
			}
		}
	}

	// Token: 0x06000518 RID: 1304 RVA: 0x000136E8 File Offset: 0x000118E8
	public void Reset(bool isGroup = false)
	{
		foreach (UnitySprite unitySprite in this.ahSp)
		{
			unitySprite.A = 0;
		}
		this.nSpNum = 0;
		this.nKeyNum = 0;
		if (isGroup)
		{
			for (int num = 0; num != this.anKey.Length; num++)
			{
				this.anKey[num] = -1;
			}
			this.nSelectKey = -1;
		}
	}

	// Token: 0x06000519 RID: 1305 RVA: 0x00013758 File Offset: 0x00011958
	public void Add()
	{
		Qoo.Debug.Assert(this.nSpNum < this.ahSp.Length);
		this.nSpNum++;
	}

	// Token: 0x0600051A RID: 1306 RVA: 0x00013780 File Offset: 0x00011980
	public void SetCursorPos(int x, int y, int z)
	{
		Qoo.Debug.Assert(this.nSpNum < this.ahSp.Length);
		UnitySprite unitySprite = this.ahSp[this.nSpNum];
		unitySprite.x = x;
		unitySprite.y = y;
		unitySprite.z = z;
	}

	// Token: 0x0600051B RID: 1307 RVA: 0x000137C8 File Offset: 0x000119C8
	public void SetCursorSize(int w, int h, bool isSet = true)
	{
		Qoo.Debug.Assert(this.nSpNum < this.ahSp.Length);
		UnitySprite unitySprite = this.ahSp[this.nSpNum];
		if (isSet)
		{
			unitySprite.w = w - unitySprite.x;
		}
		else
		{
			unitySprite.w = w;
		}
		unitySprite.h = h + 2;
	}

	// Token: 0x0600051C RID: 1308 RVA: 0x00013824 File Offset: 0x00011A24
	public void SetCursorGroup(int nGroup)
	{
		Qoo.Debug.Assert(this.nSpNum < this.ahSp.Length);
		this.anKey[this.nSpNum] = nGroup;
	}

	// Token: 0x0600051D RID: 1309 RVA: 0x0001384C File Offset: 0x00011A4C
	public void UpdateKeyCursor()
	{
		for (int i = 0; i < this.nSpNum; i++)
		{
			UnitySprite unitySprite = this.ahSp[i];
			if (this.anKey[i] == this.nSelectKey)
			{
				unitySprite.SetColor(this.colCur);
			}
			else
			{
				unitySprite.SetColor(this.colBg);
			}
		}
	}

	// Token: 0x0600051E RID: 1310 RVA: 0x000138AC File Offset: 0x00011AAC
	public bool MoveKeyCursor(int nVec)
	{
		if (this.nKeyNum <= 0)
		{
			return false;
		}
		if (this.nSelectKey < 0)
		{
			this.nSelectKey = 0;
		}
		else if (nVec > 0)
		{
			this.nSelectKey++;
			if (this.nSelectKey >= this.nKeyNum)
			{
				this.nSelectKey = 0;
			}
		}
		else if (nVec < 0)
		{
			this.nSelectKey--;
			if (this.nSelectKey < 0)
			{
				this.nSelectKey = this.nKeyNum - 1;
			}
		}
		this.UpdateKeyCursor();
		return true;
	}

	// Token: 0x0600051F RID: 1311 RVA: 0x00013948 File Offset: 0x00011B48
	public void ResetKeySel()
	{
		this.nSelectKey = -1;
		this.UpdateKeyCursor();
	}

	// Token: 0x06000520 RID: 1312 RVA: 0x00013958 File Offset: 0x00011B58
	public void Show(bool isShow)
	{
		foreach (UnitySprite unitySprite in this.ahSp)
		{
			unitySprite.Show = isShow;
		}
	}

	// Token: 0x040003B9 RID: 953
	private const int CURSP_MAX = 8;

	// Token: 0x040003BA RID: 954
	private UnitySprite[] ahSp = new UnitySprite[8];

	// Token: 0x040003BB RID: 955
	private int[] anKey = new int[8];

	// Token: 0x040003BC RID: 956
	private int nSpNum;

	// Token: 0x040003BD RID: 957
	private int nKeyNum;

	// Token: 0x040003BE RID: 958
	private int nSelectKey = -1;

	// Token: 0x040003BF RID: 959
	private Color32 colBg = new Color32(0, 0, 0, 0);

	// Token: 0x040003C0 RID: 960
	private Color32 colCur = new Color32(0, 0, 0, 0);
}
