﻿using System;
using Qoo;
using UnityEngine;
using UnityEngine.Video;

// Token: 0x02000180 RID: 384
public class MovieTex : MonoBehaviour
{
	// Token: 0x17000172 RID: 370
	// (get) Token: 0x06000ACF RID: 2767 RVA: 0x0002F668 File Offset: 0x0002D868
	public bool IsPlay
	{
		get
		{
			return this.m_tex.isPlaying;
		}
	}

	// Token: 0x06000AD0 RID: 2768 RVA: 0x0002F678 File Offset: 0x0002D878
	private void Awake()
	{
	}

	// Token: 0x06000AD1 RID: 2769 RVA: 0x0002F67C File Offset: 0x0002D87C
	private void OnDestroy()
	{
		UnityEngine.Object.Destroy(this.m_tex);
		this.m_tex = null;
	}

	// Token: 0x06000AD2 RID: 2770 RVA: 0x0002F690 File Offset: 0x0002D890
	public Material Play(string path, MOVIE_TEX_TYPE eType, bool isLoop = false)
	{
		if (path.IndexOf('/') == 0)
		{
			path = path.Substring(1);
		}
		string pathExt = System.IO.Path.GetExtension(path);
		path = path.Substring(0, path.Length - pathExt.Length);
		
		//this.m_tex = base.gameObject.AddComponent<MobileMovieTexture>();

		this.m_tex = base.gameObject.AddComponent<VideoPlayer>();
		this.m_tex.playOnAwake = false;
		Material newMat = new Material(Shader.Find("QO/Sprite") as Shader);
		/*this.m_tex.url = "file:///" + Application.streamingAssetsPath + "/" + path;
		this.m_tex.isLooping = isLoop;*/
		//this.m_tex.m_path = path + ".mp4";
		this.m_tex.url = "file:///" + Application.streamingAssetsPath + "/" + path + ".webm";
		this.m_tex.isLooping = isLoop;
		this.m_tex.renderMode = UnityEngine.Video.VideoRenderMode.MaterialOverride;
		//this.m_tex.targetMaterialRenderer = newMat;
		this.m_tex.Play();
		//GameObject.Find();

		/*if (eType != MOVIE_TEX_TYPE.NORMAL)
		{
			if (eType != MOVIE_TEX_TYPE.ADD)
			{
				Qoo.Debug.Print("Error:MOVIE_TEX_TYPE:Unknown");
				UnityEngine.Debug.DebugBreak();
			}
			else
			{
				//this.m_tex.m_movieMaterial = new Material(Resources.Load("Shader/Movie/YCrCbtoRGB Add") as Shader);
				this.m_tex.m_movieMaterial = new Material(Shader.Find("QO/Sprite") as Shader);
			}
		}
		else
		{
			//this.m_tex.m_movieMaterial = new Material(Resources.Load("Shader/Movie/YCrCbtoRGB") as Shader);
			this.m_tex.m_movieMaterial = new Material(Shader.Find("QO/Sprite") as Shader);
		}*/

		//return gameObject.GetComponent<Renderer>().material;
		//return this.m_tex.MovieMaterial;
		return newMat;
	}

	// Token: 0x06000AD3 RID: 2771 RVA: 0x0002F764 File Offset: 0x0002D964
	private void Update()
	{
	}

	// Token: 0x06000AD4 RID: 2772 RVA: 0x0002F768 File Offset: 0x0002D968
	public void Stop()
	{
		this.m_tex.Stop();
	}

	// Token: 0x040008C0 RID: 2240
	//private MobileMovieTexture m_tex;
	private VideoPlayer m_tex;
}
