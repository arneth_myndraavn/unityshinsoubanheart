﻿using System;
using UnityEngine;

// Token: 0x020000F4 RID: 244
public class VoiceOptionCollider : MonoBehaviour
{
	// Token: 0x06000689 RID: 1673 RVA: 0x0001B0B0 File Offset: 0x000192B0
	private void Start()
	{
		base.gameObject.AddComponent<BoxCollider>();
	}

	// Token: 0x0600068A RID: 1674 RVA: 0x0001B0C0 File Offset: 0x000192C0
	private void Update()
	{
		if (this.m_Drag)
		{
			return;
		}
		Vector3 localPosition = this.ListObject.transform.localPosition;
		float basePositionY = this.BasePositionY;
		if (localPosition.y < basePositionY)
		{
			float num = localPosition.y - basePositionY;
			num /= 3f;
			if (Mathf.Abs(num) < 1f)
			{
				localPosition.y = basePositionY;
			}
			else
			{
				localPosition.y = basePositionY + num;
			}
		}
		else if (localPosition.y > this.Height)
		{
			float num2 = Mathf.Abs(localPosition.y - this.Height);
			num2 /= 3f;
			localPosition.y = this.Height + ((Mathf.Abs(num2) >= 1f) ? num2 : 0f);
			if (Mathf.Abs(num2) < 1f)
			{
				localPosition.y = this.Height;
			}
			else
			{
				localPosition.y = this.Height + num2;
			}
		}
		this.ListObject.transform.localPosition = localPosition;
	}

	// Token: 0x0600068B RID: 1675 RVA: 0x0001B1D8 File Offset: 0x000193D8
	public void SetInfo(GameObject dragObject, int height)
	{
		this.ListObject = dragObject;
		this.BasePositionY = this.ListObject.transform.localPosition.y;
		this.Height = (float)height;
	}

	// Token: 0x0600068C RID: 1676 RVA: 0x0001B214 File Offset: 0x00019414
	public void AddScrollBar(VoiceOptionScrollbar scrollbar)
	{
		this.m_Scrollbar = scrollbar;
	}

	// Token: 0x0600068D RID: 1677 RVA: 0x0001B220 File Offset: 0x00019420
	private void OnMouseDown()
	{
		this.m_Drag = true;
		this.OnInputStartObjectPosition = this.ListObject.transform.position;
		this.OnInputStartMousePosition = Input.mousePosition;
	}

	// Token: 0x0600068E RID: 1678 RVA: 0x0001B258 File Offset: 0x00019458
	private void OnMouseDrag()
	{
		Camera camera = SubPartCamera.GetCamera();
		Vector3 position = camera.WorldToScreenPoint(this.OnInputStartObjectPosition);
		Vector3 vector = Input.mousePosition - this.OnInputStartMousePosition;
		position.y += vector.y;
		position = camera.ScreenToWorldPoint(position);
		this.ListObject.transform.position = position;
		if (this.m_Scrollbar != null)
		{
			this.m_Scrollbar.SetValue((int)(this.BasePositionY - position.y));
		}
	}

	// Token: 0x0600068F RID: 1679 RVA: 0x0001B2E4 File Offset: 0x000194E4
	private void OnMouseUp()
	{
		this.m_Drag = false;
	}

	// Token: 0x040005E9 RID: 1513
	public GameObject ListObject;

	// Token: 0x040005EA RID: 1514
	public bool m_Drag;

	// Token: 0x040005EB RID: 1515
	public float BasePositionY;

	// Token: 0x040005EC RID: 1516
	public float Height;

	// Token: 0x040005ED RID: 1517
	private Vector3 OnInputStartMousePosition;

	// Token: 0x040005EE RID: 1518
	private Vector3 OnInputStartObjectPosition;

	// Token: 0x040005EF RID: 1519
	private VoiceOptionScrollbar m_Scrollbar;
}
