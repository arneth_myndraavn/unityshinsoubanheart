﻿using System;
using UnityEngine;

// Token: 0x020000E7 RID: 231
public class SubPartCamera : MonoBehaviour
{
	// Token: 0x170000D6 RID: 214
	// (get) Token: 0x06000638 RID: 1592 RVA: 0x00019D78 File Offset: 0x00017F78
	private static SubPartCamera Instance
	{
		get
		{
			GameObject gameObject = GameObject.Find("__SubPartCamera");
			if (gameObject == null)
			{
				gameObject = new GameObject("__SubPartCamera");
				SubPartCamera.instance = gameObject.AddComponent<SubPartCamera>();
			}
			if (SubPartCamera.instance == null)
			{
				SubPartCamera.instance = gameObject.GetComponent<SubPartCamera>();
			}
			return SubPartCamera.instance;
		}
	}

	// Token: 0x06000639 RID: 1593 RVA: 0x00019DD4 File Offset: 0x00017FD4
	public static void Init(float aspect_w = 16f, float aspect_h = 9f)
	{
		SubPartCamera.Instance.m_AspectWidth = aspect_w;
		SubPartCamera.Instance.m_AspectHeight = aspect_h;
		SubPartCamera.Instance.m_Camera = SubPartCamera.Instance.gameObject.AddComponent<Camera>();
		SubPartCamera.Instance.m_Camera.orthographic = true;
		SubPartCamera.Instance.m_Camera.orthographicSize = 272f;
		SubPartCamera.Instance.m_Camera.clearFlags = CameraClearFlags.Color;
		SubPartCamera.Instance.m_Camera.backgroundColor = Color.black;
		SubPartCamera.Instance.m_Camera.farClipPlane = 10000f;
		SubPartCamera.Instance.m_Camera.nearClipPlane = 0.3f;
		SubPartCamera.Instance.m_Camera.rect = SubPartCamera.Instance.calcAspect(SubPartCamera.Instance.m_AspectWidth, SubPartCamera.Instance.m_AspectHeight);
		SubPartCamera.Instance.m_Camera.enabled = true;
		//TODO Camera change
		//SubPartCamera.Instance.gameObject.transform.localPosition = new Vector3(2000f, 0f, -1000f);
		SubPartCamera.Instance.gameObject.transform.localPosition = new Vector3(0f, 0f, -1100f);
	}

	// Token: 0x0600063A RID: 1594 RVA: 0x00019EE8 File Offset: 0x000180E8
	public static Camera GetCamera()
	{
		return SubPartCamera.Instance.m_Camera;
	}

	// Token: 0x170000D7 RID: 215
	// (get) Token: 0x0600063B RID: 1595 RVA: 0x00019EF4 File Offset: 0x000180F4
	// (set) Token: 0x0600063C RID: 1596 RVA: 0x00019F08 File Offset: 0x00018108
	public static bool Enabled
	{
		get
		{
			return SubPartCamera.Instance.m_Camera.enabled;
		}
		set
		{
			SubPartCamera.Instance.m_Camera.enabled = value;
		}
	}

	// Token: 0x0600063D RID: 1597 RVA: 0x00019F1C File Offset: 0x0001811C
	private void Start()
	{
	}

	// Token: 0x0600063E RID: 1598 RVA: 0x00019F20 File Offset: 0x00018120
	private void Update()
	{
	}

	// Token: 0x0600063F RID: 1599 RVA: 0x00019F24 File Offset: 0x00018124
	private void OnDestory()
	{
		SubPartCamera.instance = null;
	}

	// Token: 0x06000640 RID: 1600 RVA: 0x00019F2C File Offset: 0x0001812C
	private Rect calcAspect(float width, float height)
	{
		float num = width / height;
		float num2 = (float)Screen.width / (float)Screen.height;
		float num3 = num2 / num;
		Rect result = new Rect(0f, 0f, 1f, 1f);
		if (1f > num3)
		{
			result.x = 0f;
			result.y = (1f - num3) / 2f;
			result.width = 1f;
			result.height = num3;
		}
		else
		{
			float num4 = 1f / num3;
			result.x = (1f - num4) / 2f;
			result.y = 0f;
			result.width = num4;
			result.height = 1f;
		}
		return result;
	}

	// Token: 0x040005B6 RID: 1462
	private static SubPartCamera instance;

	// Token: 0x040005B7 RID: 1463
	public float m_AspectWidth = 16f;

	// Token: 0x040005B8 RID: 1464
	public float m_AspectHeight = 9f;

	// Token: 0x040005B9 RID: 1465
	private Camera m_Camera;
}
