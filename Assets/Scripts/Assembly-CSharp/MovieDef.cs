﻿using System;

// Token: 0x020000D5 RID: 213
public class MovieDef
{
	// Token: 0x04000550 RID: 1360
	public const string MOVIE_EXT = ".webm";

	// Token: 0x04000551 RID: 1361
	public const string EFFECT_MOVIE_EXT = ".ogv";
}
