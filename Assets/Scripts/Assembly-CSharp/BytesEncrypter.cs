﻿using System;
using System.Security.Cryptography;
using System.Text;

// Token: 0x02000160 RID: 352
public class BytesEncrypter
{
	// Token: 0x06000A25 RID: 2597 RVA: 0x0002D3A0 File Offset: 0x0002B5A0
	public static byte[] EncryptRijndael(byte[] src, string saltString, string password)
	{
		RijndaelManaged rijndaelManaged = new RijndaelManaged();
		byte[] key;
		byte[] iv;
		BytesEncrypter.GenerateKeyFromPassword(saltString, password, rijndaelManaged.KeySize, out key, rijndaelManaged.BlockSize, out iv);
		rijndaelManaged.Key = key;
		rijndaelManaged.IV = iv;
		ICryptoTransform cryptoTransform = rijndaelManaged.CreateEncryptor();
		byte[] result = cryptoTransform.TransformFinalBlock(src, 0, src.Length);
		cryptoTransform.Dispose();
		return result;
	}

	// Token: 0x06000A26 RID: 2598 RVA: 0x0002D3F4 File Offset: 0x0002B5F4
	public static byte[] DecryptRijndael(byte[] src, string saltString, string password)
	{
		RijndaelManaged rijndaelManaged = new RijndaelManaged();
		byte[] key;
		byte[] iv;
		BytesEncrypter.GenerateKeyFromPassword(saltString, password, rijndaelManaged.KeySize, out key, rijndaelManaged.BlockSize, out iv);
		rijndaelManaged.Key = key;
		rijndaelManaged.IV = iv;
		ICryptoTransform cryptoTransform = rijndaelManaged.CreateDecryptor();
		byte[] result = cryptoTransform.TransformFinalBlock(src, 0, src.Length);
		cryptoTransform.Dispose();
		return result;
	}

	// Token: 0x06000A27 RID: 2599 RVA: 0x0002D448 File Offset: 0x0002B648
	private static void GenerateKeyFromPassword(string saltString, string password, int keySize, out byte[] key, int blockSize, out byte[] iv)
	{
		byte[] bytes = Encoding.UTF8.GetBytes(saltString);
		Rfc2898DeriveBytes rfc2898DeriveBytes = new Rfc2898DeriveBytes(password, bytes, 1000);
		key = rfc2898DeriveBytes.GetBytes(keySize / 8);
		iv = rfc2898DeriveBytes.GetBytes(blockSize / 8);
	}
}
