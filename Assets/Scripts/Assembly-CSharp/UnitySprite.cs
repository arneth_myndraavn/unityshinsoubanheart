﻿using System;
using Qoo;
using UnityEngine;

// Token: 0x0200015A RID: 346
public class UnitySprite
{
	// Token: 0x060009C5 RID: 2501 RVA: 0x0002B634 File Offset: 0x00029834
	public UnitySprite(bool IsSprite = true, GameObject parent = null, bool IsText = false)
	{
		this.id = (UnitySprite.id_base += 1U);
		if (!IsText)
		{
			this.obj = CreateSprite.Create();
		}
		else
		{
			this.obj = CreateSprite.CreateTextSprite();
		}
		this.obj.transform.parent = parent.transform;
		if (IsSprite)
		{
			this.obj.layer = LayerMask.NameToLayer("Sprite");
			this.IsUseTex = true;
		}
		else
		{
			this.obj.layer = 0;
			this.IsUseTex = false;
		}
	}

	// Token: 0x1700012F RID: 303
	// (get) Token: 0x060009C6 RID: 2502 RVA: 0x0002B700 File Offset: 0x00029900
	// (set) Token: 0x060009C7 RID: 2503 RVA: 0x0002B708 File Offset: 0x00029908
	public virtual int x
	{
		get
		{
			return this.m_nX;
		}
		set
		{
			this.m_nX = value;
		}
	}

	// Token: 0x17000130 RID: 304
	// (get) Token: 0x060009C8 RID: 2504 RVA: 0x0002B714 File Offset: 0x00029914
	// (set) Token: 0x060009C9 RID: 2505 RVA: 0x0002B71C File Offset: 0x0002991C
	public virtual int y
	{
		get
		{
			return this.m_nY;
		}
		set
		{
			this.m_nY = value;
		}
	}

	// Token: 0x17000131 RID: 305
	// (get) Token: 0x060009CA RID: 2506 RVA: 0x0002B728 File Offset: 0x00029928
	// (set) Token: 0x060009CB RID: 2507 RVA: 0x0002B730 File Offset: 0x00029930
	public virtual int z
	{
		get
		{
			return this.m_nZ;
		}
		set
		{
			this.m_nZ = value;
		}
	}

	// Token: 0x17000132 RID: 306
	// (get) Token: 0x060009CC RID: 2508 RVA: 0x0002B73C File Offset: 0x0002993C
	// (set) Token: 0x060009CD RID: 2509 RVA: 0x0002B744 File Offset: 0x00029944
	public virtual int w
	{
		get
		{
			return this.m_nW;
		}
		set
		{
			this.m_nW = value;
		}
	}

	// Token: 0x17000133 RID: 307
	// (get) Token: 0x060009CE RID: 2510 RVA: 0x0002B750 File Offset: 0x00029950
	// (set) Token: 0x060009CF RID: 2511 RVA: 0x0002B758 File Offset: 0x00029958
	public virtual int h
	{
		get
		{
			return this.m_nH;
		}
		set
		{
			this.m_nH = value;
		}
	}

	// Token: 0x17000134 RID: 308
	// (get) Token: 0x060009D0 RID: 2512 RVA: 0x0002B764 File Offset: 0x00029964
	// (set) Token: 0x060009D1 RID: 2513 RVA: 0x0002B76C File Offset: 0x0002996C
	public virtual int OrgW
	{
		get
		{
			return this.orgW;
		}
		protected set
		{
			this.orgW = value;
		}
	}

	// Token: 0x17000135 RID: 309
	// (get) Token: 0x060009D2 RID: 2514 RVA: 0x0002B778 File Offset: 0x00029978
	// (set) Token: 0x060009D3 RID: 2515 RVA: 0x0002B780 File Offset: 0x00029980
	public virtual int OrgH
	{
		get
		{
			return this.orgH;
		}
		protected set
		{
			this.orgH = value;
		}
	}

	// Token: 0x17000136 RID: 310
	// (get) Token: 0x060009D4 RID: 2516 RVA: 0x0002B78C File Offset: 0x0002998C
	// (set) Token: 0x060009D5 RID: 2517 RVA: 0x0002B794 File Offset: 0x00029994
	public virtual bool Show
	{
		get
		{
			return this.isShow;
		}
		set
		{
			this.isShow = value;
		}
	}

	// Token: 0x17000137 RID: 311
	// (get) Token: 0x060009D6 RID: 2518 RVA: 0x0002B7A0 File Offset: 0x000299A0
	// (set) Token: 0x060009D7 RID: 2519 RVA: 0x0002B7A8 File Offset: 0x000299A8
	public virtual int Effect
	{
		get
		{
			return this.m_nEffect;
		}
		set
		{
			this.m_nEffect = value;
		}
	}

	// Token: 0x17000138 RID: 312
	// (get) Token: 0x060009D8 RID: 2520 RVA: 0x0002B7B4 File Offset: 0x000299B4
	// (set) Token: 0x060009D9 RID: 2521 RVA: 0x0002B7BC File Offset: 0x000299BC
	public virtual Color32 FxColor
	{
		get
		{
			return this.m_colorFx;
		}
		set
		{
			this.m_colorFx = value;
		}
	}

	// Token: 0x17000139 RID: 313
	// (get) Token: 0x060009DA RID: 2522 RVA: 0x0002B7C8 File Offset: 0x000299C8
	public virtual bool IsTexRelease
	{
		get
		{
			return true;
		}
	}

	// Token: 0x060009DB RID: 2523 RVA: 0x0002B7CC File Offset: 0x000299CC
	public bool Reset()
	{
		this.DeleteMaterial();
		if (this.obj != null && this.obj.GetComponent<MeshFilter>().sharedMesh != null)
		{
			this.obj.GetComponent<MeshFilter>().sharedMesh.Clear();
			UnityEngine.Object.Destroy(this.obj.GetComponent<MeshFilter>().sharedMesh);
			this.obj.GetComponent<MeshFilter>().sharedMesh = null;
		}
		if (this.obj != null)
		{
			UnityEngine.Object.Destroy(this.obj);
		}
		this.obj = null;
		this.tex = null;
		return true;
	}

	// Token: 0x060009DC RID: 2524 RVA: 0x0002B874 File Offset: 0x00029A74
	public void DeleteMaterial()
	{
		if (this.obj != null && this.obj.GetComponent<Renderer>() != null && this.obj.GetComponent<Renderer>().material != null && this.obj.GetComponent<Renderer>().material.shader != null)
		{
			UnityEngine.Object.Destroy(this.obj.GetComponent<Renderer>().material);
			this.obj.GetComponent<Renderer>().material = null;
		}
	}

	// Token: 0x060009DD RID: 2525 RVA: 0x0002B90C File Offset: 0x00029B0C
	public void SetMaterial(string shader_name)
	{
		this.DeleteMaterial();
		this.obj.GetComponent<Renderer>().material = new Material(Resources.Load(shader_name) as Shader);
	}

	// Token: 0x060009DE RID: 2526 RVA: 0x0002B940 File Offset: 0x00029B40
	public virtual void Update(int ScrW, int ScrH, float worldZ)
	{
		if (this.OldBrend != this.Brend)
		{
			switch (this.Brend)
			{
			case SPRITE_DRAW_MODE.MUL:
				this.SetMaterial("Shader/Sprite/Sprite");
				break;
			case SPRITE_DRAW_MODE.MUL_NOTEXALPHA:
				this.SetMaterial("Shader/Sprite/Sprite NoTexAlpha");
				break;
			case SPRITE_DRAW_MODE.ADD:
				this.SetMaterial("Shader/Sprite/Sprite Add");
				break;
			case SPRITE_DRAW_MODE.SUB:
				this.SetMaterial("Shader/Sprite/Sprite Sub");
				break;
			case SPRITE_DRAW_MODE.TEX_ALPHA:
				this.SetMaterial("Shader/Sprite/Sprite Flash");
				break;
			case SPRITE_DRAW_MODE.BACK:
				this.SetMaterial("Shader/Sprite/Sprite Back");
				break;
			}
			this.OldBrend = this.Brend;
		}
		if (this.IsUseTex)
		{
			if (this.tex != null)
			{
				this.obj.GetComponent<Renderer>().material.mainTexture = this.tex.m_Texture;
			}
			else
			{
				this.obj.GetComponent<Renderer>().material.mainTexture = null;
			}
		}
		this.UpdateUVAnim();
		if (this.obj.activeSelf != this.Show)
		{
			this.obj.SetActive(this.Show);
		}
		this.obj.transform.localPosition = new Vector3((float)(this.x + this.m_nFx - ScrW / 2), (float)(-(float)this.y + this.m_nFy + ScrH / 2), worldZ);
		if (this.obj.transform.localScale != new Vector3((float)this.w, (float)this.h, 0f))
		{
			this.obj.transform.localScale = new Vector3((float)this.w, (float)this.h, 0f);
		}
		if (this.Brend != SPRITE_DRAW_MODE.OTHER)
		{
			this.obj.GetComponent<Renderer>().material.SetVector("_UVWH", new Vector4((float)this.U / (float)this.OrgW, (float)(this.OrgH - this.V) / (float)this.OrgH - (float)this.VH / (float)this.OrgH, (float)this.UW / (float)this.OrgW, (float)this.VH / (float)this.OrgH));
			SPRITE_DRAW_MODE brend = this.Brend;
			if (brend == SPRITE_DRAW_MODE.TEX_ALPHA)
			{
				this.obj.GetComponent<Renderer>().material.SetColor("_EffectColor", this.m_colorFx);
			}
		}
		this.obj.GetComponent<Renderer>().material.color = new Color((float)this.R / 255f, (float)this.G / 255f, (float)this.B / 255f, (float)this.A / 255f);
	}

	// Token: 0x060009DF RID: 2527 RVA: 0x0002BC1C File Offset: 0x00029E1C
	public void SetImage(UnityTexture tex_)
	{
		if (this.tex != null && this.tex != tex_)
		{
			Singleton<Man2D>.Instance.ReleaseTexture(this.tex, false);
		}
		this.tex = tex_;
		if (this.obj)
		{
			if (this.tex == null)
			{
				int num = 0;
				this.OrgW = num;
				this.w = (this.UW = num);
				num = 0;
				this.OrgH = num;
				this.h = (this.VH = num);
			}
			else
			{
				if (!this.tex.IsInit)
				{
					Qoo.Debug.Print("+++++間に合ってない。" + this.tex.m_Name);
				}
				int num = this.tex.m_Texture.width;
				this.OrgW = num;
				this.w = (this.UW = num);
				num = this.tex.m_Texture.height;
				this.OrgH = num;
				this.h = (this.VH = num);
				this.w = (int)((float)this.w * this.tex.m_Scale.w);
				this.h = (int)((float)this.h * this.tex.m_Scale.h);
			}
		}
	}

	// Token: 0x060009E0 RID: 2528 RVA: 0x0002BD64 File Offset: 0x00029F64
	public void SetRenderImage(Texture offscreen, int w_, int h_)
	{
		if (this.obj)
		{
			if (offscreen == null)
			{
				this.A = 0;
			}
			this.obj.GetComponent<Renderer>().material.mainTexture = offscreen;
			this.OrgW = w_;
			this.UW = w_;
			this.w = w_;
			this.OrgH = h_;
			this.VH = h_;
			this.h = h_;
		}
	}

	// Token: 0x060009E1 RID: 2529 RVA: 0x0002BDDC File Offset: 0x00029FDC
	public void SetMaterial(Material mat_, int screenW, int screenH)
	{
		this.IsUseTex = false;
		if (this.obj)
		{
			this.obj.GetComponent<Renderer>().material = mat_;
			this.OrgW = screenW;
			this.UW = screenW;
			this.w = screenW;
			this.OrgH = screenH;
			this.VH = screenH;
			this.h = screenH;
		}
	}

	// Token: 0x060009E2 RID: 2530 RVA: 0x0002BE44 File Offset: 0x0002A044
	public void ResetUVAnim()
	{
		this.UVAnimCnt = -1;
		this.UVAnimIndex = 0;
	}

	// Token: 0x060009E3 RID: 2531 RVA: 0x0002BE54 File Offset: 0x0002A054
	public void UpdateUVAnim()
	{
		if (!this.UVAnimOn)
		{
			return;
		}
		if (this.PtnNumX <= 1 && this.PtnNumY <= 1)
		{
			this.UVAnimOn = false;
			return;
		}
		if (this.UVAnimCnt >= this.UVAnimWait)
		{
			this.UVAnimCnt = 0;
			this.UVAnimIndex++;
			if (this.UVAnimIndex >= this.PtnNumX * this.PtnNumY)
			{
				if (this.UVAnimRepeat)
				{
					this.UVAnimIndex %= this.PtnNumX * this.PtnNumY;
				}
				else
				{
					this.UVAnimIndex = this.PtnNumX * this.PtnNumY - 1;
					this.UVAnimOn = false;
				}
			}
		}
		else
		{
			this.UVAnimCnt++;
		}
		if (this.UVAnimIndex >= 0 && this.UVAnimIndex < this.PtnNumX * this.PtnNumY)
		{
			this.SetPtnPos(this.UVAnimIndex % this.PtnNumX, this.UVAnimIndex / this.PtnNumX);
		}
	}

	// Token: 0x060009E4 RID: 2532 RVA: 0x0002BF68 File Offset: 0x0002A168
	public void SetPtnPos(int nPosX, int nPosY)
	{
		if (nPosX < this.PtnNumX && nPosY < this.PtnNumY)
		{
			this.w = (this.UW = this.OrgW / this.PtnNumX);
			this.h = (this.VH = this.OrgH / this.PtnNumY);
			this.U = this.UW * nPosX;
			this.V = this.VH * nPosY;
			if (this.tex != null)
			{
				this.w = (int)((float)this.w * this.tex.m_Scale.w);
				this.h = (int)((float)this.h * this.tex.m_Scale.h);
			}
		}
	}

	// Token: 0x060009E5 RID: 2533 RVA: 0x0002C02C File Offset: 0x0002A22C
	public void SetPtnNum(int nNumX, int nNumY)
	{
		this.PtnNumX = nNumX;
		this.PtnNumY = nNumY;
	}

	// Token: 0x060009E6 RID: 2534 RVA: 0x0002C03C File Offset: 0x0002A23C
	public void EnableUVAnim(bool bOn)
	{
		this.UVAnimOn = bOn;
	}

	// Token: 0x060009E7 RID: 2535 RVA: 0x0002C048 File Offset: 0x0002A248
	public bool IsEnableUVAnim()
	{
		return this.UVAnimOn;
	}

	// Token: 0x060009E8 RID: 2536 RVA: 0x0002C050 File Offset: 0x0002A250
	public void RepeatUVAnim(bool bOn)
	{
		this.UVAnimRepeat = bOn;
	}

	// Token: 0x060009E9 RID: 2537 RVA: 0x0002C05C File Offset: 0x0002A25C
	public void SetUVAnimWait(int nWait)
	{
		this.UVAnimWait = nWait;
	}

	// Token: 0x060009EA RID: 2538 RVA: 0x0002C068 File Offset: 0x0002A268
	public void SetCenterPosH()
	{
		this.x = (960 - this.w) / 2;
	}

	// Token: 0x060009EB RID: 2539 RVA: 0x0002C080 File Offset: 0x0002A280
	public void SetRightPosH()
	{
		this.x = 960 - this.w;
	}

	// Token: 0x060009EC RID: 2540 RVA: 0x0002C094 File Offset: 0x0002A294
	public void SetCenterPosV()
	{
		this.y = (544 - this.h) / 2;
	}

	// Token: 0x060009ED RID: 2541 RVA: 0x0002C0AC File Offset: 0x0002A2AC
	public void SetBottomPosV()
	{
		if (this.h < 544)
		{
			this.y = 544 - this.h;
		}
		else
		{
			this.y = 0;
		}
	}

	// Token: 0x060009EE RID: 2542 RVA: 0x0002C0E8 File Offset: 0x0002A2E8
	public void SetName(string name)
	{
		if (this.obj)
		{
			this.obj.transform.name = name;
		}
	}

	// Token: 0x060009EF RID: 2543 RVA: 0x0002C10C File Offset: 0x0002A30C
	public void SetColor(Color32 col)
	{
		this.R = col.r;
		this.G = col.g;
		this.B = col.b;
		this.A = col.a;
	}

	// Token: 0x060009F0 RID: 2544 RVA: 0x0002C150 File Offset: 0x0002A350
	public void SetSize(int w_, int h_)
	{
		this.w = w_;
		this.h = h_;
	}

	// Token: 0x060009F1 RID: 2545 RVA: 0x0002C160 File Offset: 0x0002A360
	public void SetFxPos(int x_, int y_)
	{
		this.m_nFx = x_;
		this.m_nFy = y_;
	}

	// Token: 0x060009F2 RID: 2546 RVA: 0x0002C170 File Offset: 0x0002A370
	public void CalcRenderImageOffset(int width, int height)
	{
		float num = (float)this.OrgW / (float)this.OrgH;
		float num2 = (float)width / (float)height;
		if (num2 < num)
		{
			float num3 = (float)width / (float)this.OrgW;
			float num4 = (float)this.OrgH * num3;
			this.w = width;
			this.h = (int)num4;
			this.y = (height - (int)num4) / 2;
		}
		else if (num2 > num)
		{
			float num5 = (float)height / (float)this.OrgH;
			float num6 = (float)this.OrgW * num5;
			this.w = (int)num6;
			this.h = height;
			this.y = 0;
			this.x = (width - (int)num6) / 2;
		}
		else
		{
			this.w = width;
			this.h = height;
			this.y = 0;
		}
	}

	// Token: 0x060009F3 RID: 2547 RVA: 0x0002C22C File Offset: 0x0002A42C
	internal void SetPos(int iX, int iY, int z_)
	{
		this.x = iX;
		this.y = iY;
		this.z = z_;
	}

	// Token: 0x060009F4 RID: 2548 RVA: 0x0002C244 File Offset: 0x0002A444
	internal bool IsEqualCg(string idCG)
	{
		return this.tex != null && this.tex.m_Name == ManTexture.NormalizeName(idCG);
	}

	// Token: 0x060009F5 RID: 2549 RVA: 0x0002C26C File Offset: 0x0002A46C
	public bool IsHit(Point2 pos_)
	{
		return this.Show && this.x <= pos_.x && pos_.x < this.x + this.w && this.y <= pos_.y && pos_.y < this.y + this.h;
	}

	// Token: 0x040007F6 RID: 2038
	public uint id;

	// Token: 0x040007F7 RID: 2039
	private int m_nX;

	// Token: 0x040007F8 RID: 2040
	private int m_nY;

	// Token: 0x040007F9 RID: 2041
	private int m_nZ;

	// Token: 0x040007FA RID: 2042
	private int m_nFx;

	// Token: 0x040007FB RID: 2043
	private int m_nFy;

	// Token: 0x040007FC RID: 2044
	private int m_nW;

	// Token: 0x040007FD RID: 2045
	private int m_nH;

	// Token: 0x040007FE RID: 2046
	public int U;

	// Token: 0x040007FF RID: 2047
	public int V;

	// Token: 0x04000800 RID: 2048
	public int UW;

	// Token: 0x04000801 RID: 2049
	public int VH;

	// Token: 0x04000802 RID: 2050
	private int orgW;

	// Token: 0x04000803 RID: 2051
	private int orgH;

	// Token: 0x04000804 RID: 2052
	private bool isShow = true;

	// Token: 0x04000805 RID: 2053
	public float Rotate;

	// Token: 0x04000806 RID: 2054
	public int PtnNumX;

	// Token: 0x04000807 RID: 2055
	public int PtnNumY;

	// Token: 0x04000808 RID: 2056
	public int UVAnimWait;

	// Token: 0x04000809 RID: 2057
	public int UVAnimCnt;

	// Token: 0x0400080A RID: 2058
	public int UVAnimIndex;

	// Token: 0x0400080B RID: 2059
	public bool UVAnimOn;

	// Token: 0x0400080C RID: 2060
	public bool UVAnimRepeat;

	// Token: 0x0400080D RID: 2061
	public byte R = byte.MaxValue;

	// Token: 0x0400080E RID: 2062
	public byte G = byte.MaxValue;

	// Token: 0x0400080F RID: 2063
	public byte B = byte.MaxValue;

	// Token: 0x04000810 RID: 2064
	public byte A = byte.MaxValue;

	// Token: 0x04000811 RID: 2065
	public byte Bright;

	// Token: 0x04000812 RID: 2066
	public Color32 m_colorFx;

	// Token: 0x04000813 RID: 2067
	public SPRITE_DRAW_MODE Brend;

	// Token: 0x04000814 RID: 2068
	public SPRITE_DRAW_MODE OldBrend;

	// Token: 0x04000815 RID: 2069
	private int m_nEffect;

	// Token: 0x04000816 RID: 2070
	public GameObject obj;

	// Token: 0x04000817 RID: 2071
	public UnityTexture tex;

	// Token: 0x04000818 RID: 2072
	protected bool IsUseTex;

	// Token: 0x04000819 RID: 2073
	private static uint id_base;
}
