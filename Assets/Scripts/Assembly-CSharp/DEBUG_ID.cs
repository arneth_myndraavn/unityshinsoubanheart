﻿using System;

// Token: 0x020000BF RID: 191
public enum DEBUG_ID
{
	// Token: 0x0400042A RID: 1066
	ID_ROOT = -1,
	// Token: 0x0400042B RID: 1067
	ID_ROOT_KSSELECT,
	// Token: 0x0400042C RID: 1068
	ID_ROOT_SYSDATA_LOAD,
	// Token: 0x0400042D RID: 1069
	ID_ROOT_FILEVIEW,
	// Token: 0x0400042E RID: 1070
	ID_ROOT_SCREEN,
	// Token: 0x0400042F RID: 1071
	ID_ROOT_FLAG,
	// Token: 0x04000430 RID: 1072
	ID_ROOT_SAVE,
	// Token: 0x04000431 RID: 1073
	ID_ROOT_DEBUG,
	// Token: 0x04000432 RID: 1074
	ID_ROOT_BOOT,
	// Token: 0x04000433 RID: 1075
	ID_ROOT_MAINMENU,
	// Token: 0x04000434 RID: 1076
	ID_ROOT_QODBG,
	// Token: 0x04000435 RID: 1077
	ID_FILEVIEW_BEGIN,
	// Token: 0x04000436 RID: 1078
	ID_FILEVIEW_CG,
	// Token: 0x04000437 RID: 1079
	ID_FILEVIEW_BGM,
	// Token: 0x04000438 RID: 1080
	ID_FILEVIEW_SE,
	// Token: 0x04000439 RID: 1081
	ID_FILEVIEW_VOICE,
	// Token: 0x0400043A RID: 1082
	ID_FILEVIEW_MOVIE,
	// Token: 0x0400043B RID: 1083
	ID_FILEVIEW_END,
	// Token: 0x0400043C RID: 1084
	ID_SCREEN_BEGIN,
	// Token: 0x0400043D RID: 1085
	ID_SCREEN_SYSMENU,
	// Token: 0x0400043E RID: 1086
	ID_SCREEN_SYSMENU2,
	// Token: 0x0400043F RID: 1087
	ID_SCREEN_NAMESET,
	// Token: 0x04000440 RID: 1088
	ID_SCREEN_SOUND,
	// Token: 0x04000441 RID: 1089
	ID_SCREEN_REF1,
	// Token: 0x04000442 RID: 1090
	ID_SCREEN_REF2,
	// Token: 0x04000443 RID: 1091
	ID_SCREEN_END,
	// Token: 0x04000444 RID: 1092
	ID_FLAG_BEGIN,
	// Token: 0x04000445 RID: 1093
	ID_FLAG_SYS_SKIP,
	// Token: 0x04000446 RID: 1094
	ID_FLAG_SYS_ALLSET,
	// Token: 0x04000447 RID: 1095
	ID_FLAG_SYS_ALLCLEAR,
	// Token: 0x04000448 RID: 1096
	ID_FLAG_CG,
	// Token: 0x04000449 RID: 1097
	ID_FLAG_MEMORY_RESET,
	// Token: 0x0400044A RID: 1098
	ID_FLAG_CG_RESET,
	// Token: 0x0400044B RID: 1099
	ID_FLAG_BGM_SET,
	// Token: 0x0400044C RID: 1100
	ID_FLAG_BGM_RESET,
	// Token: 0x0400044D RID: 1101
	ID_FLAG_REF_SET,
	// Token: 0x0400044E RID: 1102
	ID_FLAG_REF_RESET,
	// Token: 0x0400044F RID: 1103
	ID_FLAG_PAY_ON,
	// Token: 0x04000450 RID: 1104
	ID_FLAG_PAY_OFF,
	// Token: 0x04000451 RID: 1105
	ID_FLAG_INDEX,
	// Token: 0x04000452 RID: 1106
	ID_FLAG_ROUTE01,
	// Token: 0x04000453 RID: 1107
	ID_FLAG_ROUTE02,
	// Token: 0x04000454 RID: 1108
	ID_FLAG_ROUTE03,
	// Token: 0x04000455 RID: 1109
	ID_FLAG_ROUTE04,
	// Token: 0x04000456 RID: 1110
	ID_FLAG_ROUTE05,
	// Token: 0x04000457 RID: 1111
	ID_FLAG_ROUTE06,
	// Token: 0x04000458 RID: 1112
	ID_FLAG_ROUTE07,
	// Token: 0x04000459 RID: 1113
	ID_FLAG_ROUTE08,
	// Token: 0x0400045A RID: 1114
	ID_FLAG_ROUTE09,
	// Token: 0x0400045B RID: 1115
	ID_FLAG_ROUTE10,
	// Token: 0x0400045C RID: 1116
	ID_FLAG_ROUTE11,
	// Token: 0x0400045D RID: 1117
	ID_FLAG_ROUTE12,
	// Token: 0x0400045E RID: 1118
	ID_FLAG_ROUTE13,
	// Token: 0x0400045F RID: 1119
	ID_FLAG_ROUTE14,
	// Token: 0x04000460 RID: 1120
	ID_FLAG_ROUTE15,
	// Token: 0x04000461 RID: 1121
	ID_FLAG_END,
	// Token: 0x04000462 RID: 1122
	ID_SAVE_BEGIN,
	// Token: 0x04000463 RID: 1123
	ID_SAVE_DATADELETE,
	// Token: 0x04000464 RID: 1124
	ID_SAVE_SYSLOAD,
	// Token: 0x04000465 RID: 1125
	ID_SAVE_SYSSAVE,
	// Token: 0x04000466 RID: 1126
	ID_SAVE_LOAD,
	// Token: 0x04000467 RID: 1127
	ID_SAVE_END,
	// Token: 0x04000468 RID: 1128
	ID_MINIGAME_BEGIN,
	// Token: 0x04000469 RID: 1129
	ID_MINIGAME_1,
	// Token: 0x0400046A RID: 1130
	ID_MINIGAME_2,
	// Token: 0x0400046B RID: 1131
	ID_MINIGAME_3,
	// Token: 0x0400046C RID: 1132
	ID_MINIGAME_4,
	// Token: 0x0400046D RID: 1133
	ID_MINIGAME_5,
	// Token: 0x0400046E RID: 1134
	ID_MINIGAME_END,
	// Token: 0x0400046F RID: 1135
	ID_DEBUG_BEGIN,
	// Token: 0x04000470 RID: 1136
	ID_DEBUG_VERSION,
	// Token: 0x04000471 RID: 1137
	ID_DEBUG_KS_ALLCHECK,
	// Token: 0x04000472 RID: 1138
	ID_DEBUG_AUTO_KS,
	// Token: 0x04000473 RID: 1139
	ID_DEBUG_END,
	// Token: 0x04000474 RID: 1140
	ID_QODBG_QODBG,
	// Token: 0x04000475 RID: 1141
	ID_QODBG_BEGIN,
	// Token: 0x04000476 RID: 1142
	ID_QODBG_SOUNDDEBUG,
	// Token: 0x04000477 RID: 1143
	ID_QODBG_INST,
	// Token: 0x04000478 RID: 1144
	ID_QODBG_UNINST,
	// Token: 0x04000479 RID: 1145
	ID_QODBG_INSTON,
	// Token: 0x0400047A RID: 1146
	ID_QODBG_INSTOFF,
	// Token: 0x0400047B RID: 1147
	ID_QODBG_INSTSTAT,
	// Token: 0x0400047C RID: 1148
	ID_QODBG_BGM1,
	// Token: 0x0400047D RID: 1149
	ID_QODBG_BGM2,
	// Token: 0x0400047E RID: 1150
	ID_QODBG_BGM3,
	// Token: 0x0400047F RID: 1151
	ID_QODBG_SE_ALL,
	// Token: 0x04000480 RID: 1152
	ID_QODBG_END,
	// Token: 0x04000481 RID: 1153
	ID_ROOT_TEST_MOVIE
}
