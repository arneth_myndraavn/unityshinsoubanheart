﻿using System;
using System.Collections;
using Game;
using Qoo;
using Qoo.AM.Game;
using Qoo.Game;
using Qoo.Graphics;
using UnityEngine;

// Token: 0x020000C2 RID: 194
public class DebugMenuWnd : Singleton<DebugMenuWnd>
{
	// Token: 0x060005D5 RID: 1493 RVA: 0x000176FC File Offset: 0x000158FC
	private void Awake()
	{
		Singleton<UnityGraph>.Instance.Enable(false);
		this.m_Menu = base.gameObject.AddComponent<TextMenuWnd>();
		this.m_Menu.Init(new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue), new Color32(0, 0, 64, 128), 48, 5);
		foreach (LB_INIT lb_INIT in DebugDef.g_aLbInit)
		{
			this.m_Menu.AddMenu(lb_INIT.nParentId, lb_INIT.nId, lb_INIT.szCaption);
		}
		this.m_Menu.Begin();
		Singleton<UnityGraph>.Instance.SetEffect("FadeIn", 1f);
	}

	// Token: 0x060005D6 RID: 1494 RVA: 0x000177C4 File Offset: 0x000159C4
	private void Update()
	{
		if (this.m_Menu.IsSelect)
		{
			if (this.m_Menu != null)
			{
				this.m_Menu.End();
			}
			this.RunDeugMenu((DEBUG_ID)this.m_Menu.SelectID);
			if (this.m_Menu != null)
			{
				this.m_Menu.Begin();
			}
		}
	}

	// Token: 0x060005D7 RID: 1495 RVA: 0x0001782C File Offset: 0x00015A2C
	private void OnDestroy()
	{
		if (this.m_Menu != null)
		{
			this.m_Menu.Release();
			UnityEngine.Object.Destroy(this.m_Menu);
		}
		this.m_Menu = null;
		if (Singleton<UnityGraph>.IsReady)
		{
			Singleton<UnityGraph>.Instance.Disable(false);
		}
	}

	// Token: 0x060005D8 RID: 1496 RVA: 0x0001787C File Offset: 0x00015A7C
	private void RunDeugMenu(DEBUG_ID nMenuId)
	{
		if (nMenuId > DEBUG_ID.ID_FILEVIEW_BEGIN && nMenuId < DEBUG_ID.ID_FILEVIEW_END)
		{
			this.ExecFileView(nMenuId);
		}
		else if (nMenuId > DEBUG_ID.ID_SCREEN_BEGIN && nMenuId < DEBUG_ID.ID_SCREEN_END)
		{
			this.ExecScreen(nMenuId);
		}
		else if (nMenuId > DEBUG_ID.ID_FLAG_BEGIN && nMenuId < DEBUG_ID.ID_FLAG_END)
		{
			this.ExecFlag(nMenuId);
		}
		else if (nMenuId > DEBUG_ID.ID_SAVE_BEGIN && nMenuId < DEBUG_ID.ID_SAVE_END)
		{
			this.ExecSaveData(nMenuId);
		}
		else if (nMenuId > DEBUG_ID.ID_MINIGAME_BEGIN && nMenuId < DEBUG_ID.ID_MINIGAME_END)
		{
			this.ExecMiniGame(nMenuId);
		}
		else if (nMenuId > DEBUG_ID.ID_DEBUG_BEGIN && nMenuId < DEBUG_ID.ID_DEBUG_END)
		{
			this.ExecDebug(nMenuId);
		}
		else if (nMenuId > DEBUG_ID.ID_QODBG_BEGIN && nMenuId < DEBUG_ID.ID_QODBG_END)
		{
			this.ExecQoDebug(nMenuId);
		}
		else if (nMenuId != DEBUG_ID.ID_ROOT_KSSELECT)
		{
			if (nMenuId != DEBUG_ID.ID_ROOT_SYSDATA_LOAD)
			{
				if (nMenuId != DEBUG_ID.ID_ROOT_BOOT)
				{
					if (nMenuId != DEBUG_ID.ID_ROOT_MAINMENU)
					{
						if (nMenuId == DEBUG_ID.ID_ROOT_TEST_MOVIE)
						{
							base.enabled = false;
							base.gameObject.AddComponent<DebugMovieWnd>();
						}
					}
					else
					{
						Singleton<UnityGraph>.Instance.ResetFade(0);
						this.OnDestroy();
						SceneManager.ChangeScene(UIValue.SCENE_TITLE);
						base.enabled = false;
					}
				}
				else
				{
					Singleton<UnityGraph>.Instance.ResetFade(0);
					this.OnDestroy();
					SceneManager.ChangeScene(UIValue.SCENE_LOGOMOVIE);
					base.enabled = false;
				}
			}
			else
			{
				this.ExecSaveData(DEBUG_ID.ID_SAVE_SYSLOAD);
			}
		}
	}

	// Token: 0x060005D9 RID: 1497 RVA: 0x000179F0 File Offset: 0x00015BF0
	private void ExecQoDebug(DEBUG_ID nMenuId)
	{
	}

	// Token: 0x060005DA RID: 1498 RVA: 0x000179F4 File Offset: 0x00015BF4
	private void ExecDebug(DEBUG_ID nMenuId)
	{
		switch (nMenuId)
		{
		case DEBUG_ID.ID_DEBUG_AUTO_KS:
			Singleton<UnityGraph>.Instance.ResetFade(0);
			this.OnDestroy();
			Qoo.Debug.IsAutoKsDebug = true;
			SysData.SetDefaultSetting();
			SysData.SetVoiceEnable(false);
			SysData.SetTextSpeed(3);
			SysData.SetEnableLoveAnim(true);
			SysData.SetAutoPage(0);
			SceneManager.ChangeScene(UIValue.SCENE_ADVMODE);
			base.enabled = false;
			break;
		}
	}

	// Token: 0x060005DB RID: 1499 RVA: 0x00017A74 File Offset: 0x00015C74
	private void ExecMiniGame(DEBUG_ID nMenuId)
	{
		switch (nMenuId)
		{
		}
	}

	// Token: 0x060005DC RID: 1500 RVA: 0x00017AC0 File Offset: 0x00015CC0
	private void ExecSaveData(DEBUG_ID nMenuId)
	{
		switch (nMenuId)
		{
		case DEBUG_ID.ID_SAVE_SYSLOAD:
		{
			SysSaveData sysSaveData = new SysSaveData();
			sysSaveData.Load(SaveLoadManager.LoadSystem());
			break;
		}
		case DEBUG_ID.ID_SAVE_SYSSAVE:
			SaveLoadManager.SaveSystem();
			break;
		}
	}

	// Token: 0x060005DD RID: 1501 RVA: 0x00017B1C File Offset: 0x00015D1C
	private void ExecFlag(DEBUG_ID nMenuId)
	{
		if (nMenuId >= DEBUG_ID.ID_FLAG_ROUTE01 && nMenuId <= DEBUG_ID.ID_FLAG_ROUTE15)
		{
			CHAR_ID route = (CHAR_ID)(nMenuId - 39);
			GameData.SetRoute(route);
			return;
		}
		switch (nMenuId)
		{
		case DEBUG_ID.ID_FLAG_SYS_SKIP:
			SysData.SetSkip(2);
			break;
		case DEBUG_ID.ID_FLAG_SYS_ALLSET:
			SysData.SetReadAll();
			break;
		case DEBUG_ID.ID_FLAG_SYS_ALLCLEAR:
			SysData.ClearRead();
			break;
		case DEBUG_ID.ID_FLAG_CG:
			SysData.SetReadCGAll();
			break;
		case DEBUG_ID.ID_FLAG_MEMORY_RESET:
			SysData.ClearRead();
			break;
		case DEBUG_ID.ID_FLAG_CG_RESET:
			SysData.ResetReadCGAll();
			break;
		case DEBUG_ID.ID_FLAG_BGM_SET:
			SysData.SetPlayBgmFlagAll();
			break;
		case DEBUG_ID.ID_FLAG_BGM_RESET:
			SysData.ResetPlayBgmFlagAll();
			break;
		case DEBUG_ID.ID_FLAG_PAY_ON:
			SysData.SetPayFullRoute(true);
			SysData.SetPayFullVoice(true);
			break;
		case DEBUG_ID.ID_FLAG_PAY_OFF:
			SysData.SetPayFullRoute(false);
			SysData.SetPayFullVoice(false);
			break;
		}
	}

	// Token: 0x060005DE RID: 1502 RVA: 0x00017C0C File Offset: 0x00015E0C
	private void ExecScreen(DEBUG_ID nMenuId)
	{
		switch (nMenuId)
		{
		}
	}

	// Token: 0x060005DF RID: 1503 RVA: 0x00017C50 File Offset: 0x00015E50
	private void ExecFileView(DEBUG_ID nMenuId)
	{
		switch (nMenuId)
		{
		}
	}

	// Token: 0x060005E0 RID: 1504 RVA: 0x00017C9C File Offset: 0x00015E9C
	private IEnumerator MovieTest()
	{
		Singleton<UnityGraph>.Instance.Disable(false);
		yield return 0;
		for (int i = 0; i != 100; i++)
		{
			Qoo.Debug.Print("MovieTest:DisableVersion:Count=" + i);
			Movie.Play("quinrose_logo_8", true);
			yield return 0;
		}
		Singleton<UnityGraph>.Instance.Enable(false);
		for (int j = 0; j != 100; j++)
		{
			Qoo.Debug.Print("MovieTest:EnableVersion:Count=" + j);
			Movie.Play("quinrose_logo_8", true);
			yield return 0;
		}
		yield return 0;
		Singleton<UnityGraph>.Instance.Enable(false);
		base.enabled = true;
		yield break;
	}

	// Token: 0x04000486 RID: 1158
	private TextMenuWnd m_Menu;
}
