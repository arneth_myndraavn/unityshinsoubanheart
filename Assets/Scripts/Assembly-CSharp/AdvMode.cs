﻿using System;
using System.Collections;
using Qoo;
using Qoo.Application;
using Qoo.Game;
using Qoo.SoundSystem;
using UnityEngine;

// Token: 0x020000DF RID: 223
public class AdvMode : UserBaseClass
{
	// Token: 0x06000619 RID: 1561 RVA: 0x000197D8 File Offset: 0x000179D8
	private void Start()
	{
		UnityTask.SetMainTask(this.Run());
	}

	// Token: 0x0600061A RID: 1562 RVA: 0x000197E8 File Offset: 0x000179E8
	private void Update()
	{
	}

	// Token: 0x0600061B RID: 1563 RVA: 0x000197EC File Offset: 0x000179EC
	public IEnumerator Run()
	{
		if (GameData.MemoryMode_IsActive())
		{
			PaymentParam.Conv();
			yield return AdvMode.ExecKs(base.gameObject, GameData.MemoryKs, GameData.MemoryLabel, false);
			GameData.MemoryMode_Destroy();
			SceneManager.BackScene();
		}
		else
		{
			do
			{
				PaymentParam.Conv();
				if (GameData.IsLoadData())
				{
					yield return AdvMode.ExecLoad(base.gameObject);
				}
				else
				{
					GameData.Init();
					string InitKsName = (App.AdvMode != 0) ? "honpen.ks" : "prologue.ks";
					if (SysData.IsTrial())
					{
						InitKsName = "op00_trial.ks";
					}
					yield return AdvMode.ExecKs(base.gameObject, InitKsName, string.Empty, true);
				}
			}
			while (GameData.IsLoadData() || Qoo.Debug.IsAutoKsDebug);
			SceneManager.ChangeScene("SceneTitle");
		}
		yield break;
	}

	// Token: 0x0600061C RID: 1564 RVA: 0x00019808 File Offset: 0x00017A08
	private static IEnumerator ExecKs(GameObject obj, string nameKs, string nameLabel, bool IsNormalPlay)
	{
		Sound.SeSlotStopAll();
		Sound.BgmStop(1000);
		GameData.IsMoveTitle = false;
		GameData.LockGameData();
		yield return Singleton<Man2D>.Instance.EraseFrameBuffer();
		UnityApp.Graph.Enable(false);
		EventWnd wnd = obj.AddComponent<EventWnd>();
		ParamWnd paramwnd = obj.AddComponent<ParamWnd>();
		yield return wnd.Exec(nameKs, nameLabel, IsNormalPlay);
		UnityApp.Graph.Disable(false);
		UnityEngine.Object.Destroy(wnd);
		UnityEngine.Object.Destroy(paramwnd);
		GameData.IsMoveTitle = false;
		yield return Singleton<Man2D>.Instance.EraseFrameBuffer();
		GameData.UnlockGameData();
		yield break;
	}

	// Token: 0x0600061D RID: 1565 RVA: 0x00019854 File Offset: 0x00017A54
	private static IEnumerator ExecLoad(GameObject obj)
	{
		Sound.SeSlotStopAll();
		Sound.BgmStop(1000);
		GameData.IsMoveTitle = false;
		yield return Singleton<Man2D>.Instance.EraseFrameBuffer();
		UnityApp.Graph.Enable(false);
		EventWnd wnd = obj.AddComponent<EventWnd>();
		ParamWnd paramwnd = obj.AddComponent<ParamWnd>();
		yield return wnd.ExecLoad();
		UnityEngine.Object.Destroy(wnd);
		UnityEngine.Object.Destroy(paramwnd);
		GameData.IsMoveTitle = false;
		yield return Singleton<Man2D>.Instance.EraseFrameBuffer();
		UnityApp.Graph.Disable(false);
		yield break;
	}
}
