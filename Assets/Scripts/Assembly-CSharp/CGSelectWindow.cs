﻿using System;

// Token: 0x02000102 RID: 258
public class CGSelectWindow : GallerySelectWindow
{
	// Token: 0x060006BF RID: 1727 RVA: 0x0001C4D4 File Offset: 0x0001A6D4
	protected sealed override void BeforeInitGallerySelect()
	{
		this.charaData = CSVManager.Instance.CsvCGListHolder.GetCharaIndex(base.Chara);
	}

	// Token: 0x060006C0 RID: 1728 RVA: 0x0001C4FC File Offset: 0x0001A6FC
	protected sealed override int GetCollect()
	{
		return this.charaData.Collect;
	}

	// Token: 0x060006C1 RID: 1729 RVA: 0x0001C50C File Offset: 0x0001A70C
	protected override string GetThumbnailName(int index)
	{
		return this.charaData.Thumbnail(index);
	}

	// Token: 0x060006C2 RID: 1730 RVA: 0x0001C51C File Offset: 0x0001A71C
	protected override int GetThumbnailLength()
	{
		return this.charaData.Length;
	}

	// Token: 0x060006C3 RID: 1731 RVA: 0x0001C52C File Offset: 0x0001A72C
	protected sealed override string GetOnArrowSceneName()
	{
		return UIValue.SCENE_CGSELECT;
	}

	// Token: 0x060006C4 RID: 1732 RVA: 0x0001C534 File Offset: 0x0001A734
	protected sealed override bool IsFont()
	{
		return true;
	}

	// Token: 0x060006C5 RID: 1733 RVA: 0x0001C538 File Offset: 0x0001A738
	protected sealed override string GetFontText(int index)
	{
		return this.charaData.CollectString(index);
	}

	// Token: 0x060006C6 RID: 1734 RVA: 0x0001C548 File Offset: 0x0001A748
	protected sealed override void OnSelect(int index)
	{
		if (this.charaData.Read(index))
		{
			this.charaData.OnSelect(index);
			SceneManager.ChangeScene(UIValue.SCENE_GALLERYCG);
		}
	}

	// Token: 0x060006C7 RID: 1735 RVA: 0x0001C574 File Offset: 0x0001A774
	protected sealed override bool IsSelectable(int index)
	{
		return this.charaData.Read(index);
	}

	// Token: 0x04000660 RID: 1632
	private CSVCGListHolder.CharaData charaData;
}
