﻿using System;
using System.Text;
using Qoo.File;
using UnityEngine;

// Token: 0x0200018A RID: 394
public class SoundData
{
	// Token: 0x06000B33 RID: 2867 RVA: 0x00030458 File Offset: 0x0002E658
	public SoundData()
	{
	}

	// Token: 0x06000B34 RID: 2868 RVA: 0x00030460 File Offset: 0x0002E660
	public SoundData(string category, string name_)
	{
		this.Create(category, name_);
	}

	// Token: 0x17000176 RID: 374
	// (get) Token: 0x06000B35 RID: 2869 RVA: 0x00030474 File Offset: 0x0002E674
	// (set) Token: 0x06000B36 RID: 2870 RVA: 0x0003047C File Offset: 0x0002E67C
	public AudioClip Data
	{
		get
		{
			return this.data;
		}
		private set
		{
			this.data = value;
		}
	}

	// Token: 0x17000177 RID: 375
	// (get) Token: 0x06000B37 RID: 2871 RVA: 0x00030488 File Offset: 0x0002E688
	// (set) Token: 0x06000B38 RID: 2872 RVA: 0x00030490 File Offset: 0x0002E690
	public string Name
	{
		get
		{
			return this.name;
		}
		set
		{
			this.name = value;
		}
	}

	// Token: 0x17000178 RID: 376
	// (get) Token: 0x06000B39 RID: 2873 RVA: 0x0003049C File Offset: 0x0002E69C
	// (set) Token: 0x06000B3A RID: 2874 RVA: 0x000304A4 File Offset: 0x0002E6A4
	public bool IsAsync { get; private set; }

	// Token: 0x06000B3B RID: 2875 RVA: 0x000304B0 File Offset: 0x0002E6B0
	public bool Create(string category, string name_)
	{
		this.Name = name_;
		this.Data = SoundData.Load(category, name_);
		this.IsAsync = false;
		return this.Data != null;
	}

	// Token: 0x06000B3C RID: 2876 RVA: 0x000304E4 File Offset: 0x0002E6E4
	public static AudioClip Load(string category, string name)
	{
		FileId fileId = new FileId(name);
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.Append("mp3");
		stringBuilder.Append("/");
		stringBuilder.Append(category);
		stringBuilder.Append("/");
		stringBuilder.Append(fileId.Name);
		return Resources.Load(stringBuilder.ToString()) as AudioClip;
	}

	// Token: 0x040008E6 RID: 2278
	private string name;

	// Token: 0x040008E7 RID: 2279
	private AudioClip data;
}
