﻿using System;
using System.Collections.Generic;
using PaymentGameApi;

// Token: 0x02000133 RID: 307
public class TitleRestoreWindow : BaseWindow
{
	private Dictionary<string,int> dict = null;

	// Token: 0x06000840 RID: 2112 RVA: 0x00025028 File Offset: 0x00023228
	protected sealed override void OnAwake()
	{
		this.state = TitleRestoreWindow.STATE.PAYINIT;
		this.tm = new RestoreMain();
		this.pi = new PaymentInit();
	}

	// Token: 0x06000841 RID: 2113 RVA: 0x00025048 File Offset: 0x00023248
	protected sealed override string[] newSceneTextureNameArray()
	{
		return new string[]
		{
			"screen/common/sys_dialog"
		};
	}

	// Token: 0x06000842 RID: 2114 RVA: 0x00025068 File Offset: 0x00023268
	protected sealed override BaseWindow.UIComponent[] newComponentArray()
	{
		return new BaseWindow.UIComponent[]
		{
			new BaseWindow.UICollision("Close", 0, 0, this.wndz, 960, 544),
			new BaseWindow.UIImage("MsgWnd", 137, 182, this.wndz, "screen/common/sys_dialog", false, true),
			new BaseWindow.UIText("text", 480, 272, this.wndz + 1, "リストア処理中です。", 28, UnityTextSprite.PositionType.Center, UnityTextSprite.PositionType.Center, byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue)
		};
	}

	// Token: 0x06000843 RID: 2115 RVA: 0x000250FC File Offset: 0x000232FC
	protected sealed override void OnBaseWindowOnButton(string obj)
	{
		if (obj != null)
		{
			if (this.dict == null)
			{
				this.dict = new Dictionary<string, int>(1)
				{
					{
						"Close",
						0
					}
				};
			}
			int num;
			if (this.dict.TryGetValue(obj, out num))
			{
				if (num == 0)
				{
					base.PlaySE_Cancel();
					this.OnClose();
				}
			}
		}
	}

	// Token: 0x06000844 RID: 2116 RVA: 0x00025164 File Offset: 0x00023364
	protected override void OnBaseWindowUpdate()
	{
		switch (this.state)
		{
		case TitleRestoreWindow.STATE.PAYINIT:
			this.pi.Init();
			this.state = TitleRestoreWindow.STATE.PAYEXEC;
			break;
		case TitleRestoreWindow.STATE.PAYEXEC:
			if (this.pi.Exec())
			{
				if (this.pi.Result() == AMAppStore.ResultStatus.RESULT_SUCCESS)
				{
					this.state = TitleRestoreWindow.STATE.INIT;
				}
				else
				{
					base.StartTime();
					this.SetText(this.pi.ResultMessage());
					this.state = TitleRestoreWindow.STATE.FAILD;
				}
			}
			break;
		case TitleRestoreWindow.STATE.INIT:
			this.tm.Init();
			this.state = TitleRestoreWindow.STATE.EXEC;
			break;
		case TitleRestoreWindow.STATE.EXEC:
			if (this.tm.Exec())
			{
				base.StartTime();
				this.SetText(this.tm.ResultMessage());
				this.state = ((this.tm.Result() != AMAppStore.ResultStatus.RESULT_SUCCESS) ? TitleRestoreWindow.STATE.FAILD : TitleRestoreWindow.STATE.SUCCESS);
			}
			break;
		case TitleRestoreWindow.STATE.SUCCESS:
			if (base.CheckTime(1f))
			{
				this.OnClose();
			}
			break;
		case TitleRestoreWindow.STATE.FAILD:
			if (base.CheckTime(1f))
			{
				this.OnClose();
			}
			break;
		case TitleRestoreWindow.STATE.EXIT:
			base.DeleteLastAddScene();
			this.state = TitleRestoreWindow.STATE.AFTER_EXIT;
			break;
		}
	}

	// Token: 0x06000845 RID: 2117 RVA: 0x000252B8 File Offset: 0x000234B8
	private void OnClose()
	{
		TitleRestoreWindow.STATE state = this.state;
		if (state == TitleRestoreWindow.STATE.SUCCESS || state == TitleRestoreWindow.STATE.FAILD)
		{
			this.state = TitleRestoreWindow.STATE.EXIT;
		}
	}

	// Token: 0x06000846 RID: 2118 RVA: 0x000252EC File Offset: 0x000234EC
	private void SetText(string message)
	{
		UnityTextSprite textSprite = base.GetTextSprite("text");
		textSprite.ClearText();
		textSprite.AddText(message, 22);
		textSprite.SetPosition(480, 272, UnityTextSprite.PositionType.Center, UnityTextSprite.PositionType.Center);
		textSprite.Update(960, 544, (float)(-(float)(this.wndz + 1)));
	}

	// Token: 0x04000734 RID: 1844
	private TitleRestoreWindow.STATE state;

	// Token: 0x04000735 RID: 1845
	private RestoreMain tm;

	// Token: 0x04000736 RID: 1846
	private PaymentInit pi;

	// Token: 0x04000737 RID: 1847
	private readonly int wndz = 10;

	// Token: 0x02000134 RID: 308
	private enum STATE
	{
		// Token: 0x0400073A RID: 1850
		PAYINIT,
		// Token: 0x0400073B RID: 1851
		PAYEXEC,
		// Token: 0x0400073C RID: 1852
		INIT,
		// Token: 0x0400073D RID: 1853
		EXEC,
		// Token: 0x0400073E RID: 1854
		SUCCESS,
		// Token: 0x0400073F RID: 1855
		FAILD,
		// Token: 0x04000740 RID: 1856
		EXIT,
		// Token: 0x04000741 RID: 1857
		AFTER_EXIT
	}
}
