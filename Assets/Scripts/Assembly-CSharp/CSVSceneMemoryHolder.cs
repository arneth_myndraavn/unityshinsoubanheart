﻿using System;
using System.Collections.Generic;
using Qoo.Game;
using UnityEngine;

// Token: 0x0200016A RID: 362
public class CSVSceneMemoryHolder
{
	// Token: 0x06000A62 RID: 2658 RVA: 0x0002E070 File Offset: 0x0002C270
	public CSVSceneMemoryHolder()
	{
		this.charaData = CSVSceneMemoryHolder.SplitChara();
		this.allCharaCollect = 0;
	}

	// Token: 0x17000150 RID: 336
	// (get) Token: 0x06000A63 RID: 2659 RVA: 0x0002E08C File Offset: 0x0002C28C
	public int AllCharaCollect
	{
		get
		{
			return this.allCharaCollect;
		}
	}

	// Token: 0x06000A64 RID: 2660 RVA: 0x0002E094 File Offset: 0x0002C294
	public CSVSceneMemoryHolder.CharaData GetCharaIndex(int charaIndex)
	{
		return this.charaData[charaIndex];
	}

	// Token: 0x06000A65 RID: 2661 RVA: 0x0002E0A0 File Offset: 0x0002C2A0
	public void UpdateGallery()
	{
		int num = 0;
		int num2 = 0;
		foreach (CSVSceneMemoryHolder.CharaData charaData in this.charaData)
		{
			charaData.UpdateGallery();
			num += charaData.CollectCount;
			num2 += charaData.CollectTotal;
		}
		this.allCharaCollect = ((num2 == 0) ? 0 : (num * 100 / num2));
	}

	// Token: 0x06000A66 RID: 2662 RVA: 0x0002E108 File Offset: 0x0002C308
	private static List<CSVSceneMemory> Read()
	{
		List<CSVSceneMemory> list = new List<CSVSceneMemory>();
		TextAsset textAsset = Resources.Load("CSV/SCENE_MEMORY") as TextAsset;
		string[] array = textAsset.text.Split(new char[]
		{
			'\n'
		});
		for (int i = 1; i < array.Length; i++)
		{
			string text = array[i];
			string text2 = text.Replace("\"", string.Empty).Replace("\r", string.Empty);
			string[] array2 = text2.Split(new char[]
			{
				','
			});
			if (array2.Length >= 7)
			{
				list.Add(new CSVSceneMemory(array2));
			}
		}
		Resources.UnloadAsset(textAsset);
		return list;
	}

	// Token: 0x06000A67 RID: 2663 RVA: 0x0002E1B4 File Offset: 0x0002C3B4
	private static CSVSceneMemoryHolder.CharaData[] SplitChara()
	{
		List<CSVSceneMemory> list = CSVSceneMemoryHolder.Read();
		List<CSVSceneMemoryHolder.CharaData> list2 = new List<CSVSceneMemoryHolder.CharaData>();
		int charaIndex;
		for (charaIndex = 0; charaIndex < 11; charaIndex++)
		{
			list2.Add(new CSVSceneMemoryHolder.CharaData(list.FindAll((CSVSceneMemory obj) => obj.charaIndex == charaIndex)));
		}
		return list2.ToArray();
	}

	// Token: 0x0400086F RID: 2159
	private CSVSceneMemoryHolder.CharaData[] charaData;

	// Token: 0x04000870 RID: 2160
	private int allCharaCollect;

	// Token: 0x0200016B RID: 363
	public class CharaData
	{
		// Token: 0x06000A68 RID: 2664 RVA: 0x0002E220 File Offset: 0x0002C420
		public CharaData(List<CSVSceneMemory> dataList)
		{
			this.datas = dataList.ToArray();
			this.reads = new int[this.datas.Length];
			this.collectCount = 0;
			this.collectTotal = 1;
		}

		// Token: 0x17000151 RID: 337
		// (get) Token: 0x06000A69 RID: 2665 RVA: 0x0002E258 File Offset: 0x0002C458
		public int Collect
		{
			get
			{
				return this.collectCount * 100 / this.collectTotal;
			}
		}

		// Token: 0x17000152 RID: 338
		// (get) Token: 0x06000A6A RID: 2666 RVA: 0x0002E26C File Offset: 0x0002C46C
		public int CollectCount
		{
			get
			{
				return this.collectCount;
			}
		}

		// Token: 0x17000153 RID: 339
		// (get) Token: 0x06000A6B RID: 2667 RVA: 0x0002E274 File Offset: 0x0002C474
		public int CollectTotal
		{
			get
			{
				return this.collectTotal;
			}
		}

		// Token: 0x17000154 RID: 340
		// (get) Token: 0x06000A6C RID: 2668 RVA: 0x0002E27C File Offset: 0x0002C47C
		public int Length
		{
			get
			{
				return this.datas.Length;
			}
		}

		// Token: 0x06000A6D RID: 2669 RVA: 0x0002E288 File Offset: 0x0002C488
		public bool Read(int index)
		{
			return index < this.reads.Length && this.reads[index] != 0;
		}

		// Token: 0x06000A6E RID: 2670 RVA: 0x0002E2B8 File Offset: 0x0002C4B8
		public string Thumbnail(int index)
		{
			return (!this.Read(index)) ? "screen/cgmemory2/cgm_nothm" : ("viewer/memory/" + this.datas[index].thumbnail);
		}

		// Token: 0x06000A6F RID: 2671 RVA: 0x0002E2E8 File Offset: 0x0002C4E8
		public void UpdateGallery()
		{
			int num = 0;
			this.collectCount = 0;
			this.collectTotal = 0;
			foreach (CSVSceneMemory csvsceneMemory in this.datas)
			{
				csvsceneMemory.Update();
				this.collectCount += (this.reads[num] = csvsceneMemory.collectCount);
				this.collectTotal++;
				num++;
			}
			if (this.collectTotal == 0)
			{
				this.collectTotal = 1;
			}
		}

		// Token: 0x06000A70 RID: 2672 RVA: 0x0002E370 File Offset: 0x0002C570
		public void OnSelect(int index)
		{
			UIValue.GalleryIndex = index;
			CSVSceneMemory csvsceneMemory = this.datas[index];
			GameData.MemoryMode_Create(csvsceneMemory.route, csvsceneMemory.stay, csvsceneMemory.startKs, csvsceneMemory.startLabel, csvsceneMemory.eventName);
		}

		// Token: 0x04000871 RID: 2161
		private CSVSceneMemory[] datas;

		// Token: 0x04000872 RID: 2162
		private int[] reads;

		// Token: 0x04000873 RID: 2163
		private int collectCount;

		// Token: 0x04000874 RID: 2164
		private int collectTotal;
	}
}
