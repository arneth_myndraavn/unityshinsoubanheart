﻿using System;
using System.Collections.Generic;
using PaymentGameApi;
using Qoo;

// Token: 0x02000127 RID: 295
public class PaymentListWindow : BaseWindow
{
	private Dictionary<string,int> dict = null;

	// Token: 0x060007EB RID: 2027 RVA: 0x000228BC File Offset: 0x00020ABC
	protected sealed override void OnAwake()
	{
		this.state = PaymentListWindow.STATE.PAYINIT;
		this.menuList = new PaymentListWindow.MenuList();
		this.purchaseList = new PaymentListWindow.PurchaseList();
		this.pi = new PaymentInit();
	}

	// Token: 0x060007EC RID: 2028 RVA: 0x000228F4 File Offset: 0x00020AF4
	protected sealed override string[] newSceneTextureNameArray()
	{
		return new string[]
		{
			"screen/common/sys_dialog"
		};
	}

	// Token: 0x060007ED RID: 2029 RVA: 0x00022914 File Offset: 0x00020B14
	protected override BaseWindow.UIComponent[] newComponentArray()
	{
		return new BaseWindow.UIComponent[]
		{
			new BaseWindow.UICollision("Close", 0, 0, this.wndz, 960, 544),
			new BaseWindow.UIImage("MsgWnd", 137, 182, this.wndz + 1, "screen/common/sys_dialog", false, true),
			new BaseWindow.UIText("Text", 480, 272, this.wndz + 2, "アドオン購入履歴を更新中です。", 22, UnityTextSprite.PositionType.Center, UnityTextSprite.PositionType.Center, byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue)
		};
	}

	// Token: 0x060007EE RID: 2030 RVA: 0x000229AC File Offset: 0x00020BAC
	protected override void OnBaseWindowOnButton(string obj)
	{
		if (obj != null)
		{
			if (this.dict == null)
			{
				this.dict = new Dictionary<string, int>(1)
				{
					{
						"Close",
						0
					}
				};
			}
			int num;
			if (this.dict.TryGetValue(obj, out num))
			{
				if (num == 0)
				{
					base.PlaySE_Cancel();
					this.OnClose();
				}
			}
		}
	}

	// Token: 0x060007EF RID: 2031 RVA: 0x00022A14 File Offset: 0x00020C14
	protected override void OnBaseWindowUpdate()
	{
		switch (this.state)
		{
		case PaymentListWindow.STATE.PAYINIT:
			this.pi.Init();
			this.state = PaymentListWindow.STATE.PAYEXEC;
			break;
		case PaymentListWindow.STATE.PAYEXEC:
			if (this.pi.Exec())
			{
				if (this.pi.Result() == AMAppStore.ResultStatus.RESULT_SUCCESS)
				{
					this.state = PaymentListWindow.STATE.INIT;
				}
				else
				{
					base.StartTime();
					this.SetText(this.pi.ResultMessage());
					this.state = PaymentListWindow.STATE.FAILD;
				}
			}
			break;
		case PaymentListWindow.STATE.INIT:
			if (!AMAppStore.CheckCondition())
			{
				this.SetText("App内での購入が許可されていません。\n「機能制限」の設定画面から\n「App内での購入」をオンにしてください。");
				this.state = PaymentListWindow.STATE.FAILD;
				base.StartTime();
			}
			else
			{
				this.menuList.Init();
				this.state = PaymentListWindow.STATE.EXEC;
			}
			break;
		case PaymentListWindow.STATE.EXEC:
			if (this.menuList.Exec())
			{
				AMAppStore.ResultStatus resultStatus = this.menuList.Result();
				this.state = ((resultStatus != AMAppStore.ResultStatus.RESULT_SUCCESS) ? PaymentListWindow.STATE.FAILD : PaymentListWindow.STATE.PURCHASE_INIT);
				if (resultStatus == AMAppStore.ResultStatus.RESULT_FAILD)
				{
					base.StartTime();
					this.SetText(this.menuList.ResultMessage());
				}
			}
			break;
		case PaymentListWindow.STATE.PURCHASE_INIT:
			this.purchaseList.Init();
			this.state = PaymentListWindow.STATE.PURCHASE_EXEC;
			break;
		case PaymentListWindow.STATE.PURCHASE_EXEC:
			if (this.purchaseList.Exec())
			{
				base.StartTime();
				this.SetText(this.purchaseList.ResultMessage());
				this.state = ((this.purchaseList.Result() != AMAppStore.ResultStatus.RESULT_SUCCESS) ? PaymentListWindow.STATE.FAILD : PaymentListWindow.STATE.SUCCESS);
			}
			break;
		case PaymentListWindow.STATE.SUCCESS:
			if (base.CheckTime(1f))
			{
				this.OnClose();
			}
			break;
		case PaymentListWindow.STATE.FAILD:
			if (base.CheckTime(1f))
			{
				this.OnClose();
			}
			break;
		case PaymentListWindow.STATE.EXIT:
			base.DeleteLastAddScene();
			this.state = PaymentListWindow.STATE.AFTER_EXIT;
			break;
		}
	}

	// Token: 0x060007F0 RID: 2032 RVA: 0x00022C04 File Offset: 0x00020E04
	private void OnClose()
	{
		PaymentListWindow.STATE state = this.state;
		if (state != PaymentListWindow.STATE.SUCCESS)
		{
			if (state == PaymentListWindow.STATE.FAILD)
			{
				UIValue.Payment_Exit = PaymentExitType.FAILD;
				this.state = PaymentListWindow.STATE.EXIT;
			}
		}
		else
		{
			UIValue.Payment_Exit = PaymentExitType.SUCCESS;
			this.state = PaymentListWindow.STATE.EXIT;
		}
	}

	// Token: 0x060007F1 RID: 2033 RVA: 0x00022C50 File Offset: 0x00020E50
	private void SetText(string message)
	{
		UnityTextSprite textSprite = base.GetTextSprite("Text");
		textSprite.ClearText();
		textSprite.AddText(message, 22);
		textSprite.SetPosition(480, 272, UnityTextSprite.PositionType.Center, UnityTextSprite.PositionType.Center);
		textSprite.Update(960, 544, (float)(-(float)(this.wndz + 2)));
	}

	// Token: 0x040006E9 RID: 1769
	private PaymentListWindow.STATE state;

	// Token: 0x040006EA RID: 1770
	private PaymentListWindow.MenuList menuList;

	// Token: 0x040006EB RID: 1771
	private PaymentListWindow.PurchaseList purchaseList;

	// Token: 0x040006EC RID: 1772
	private PaymentInit pi;

	// Token: 0x040006ED RID: 1773
	private readonly int wndz = 10;

	// Token: 0x02000128 RID: 296
	private class MenuList
	{
		// Token: 0x060007F2 RID: 2034 RVA: 0x00022CA8 File Offset: 0x00020EA8
		public MenuList()
		{
			this.m_eResut = AMAppStore.ResultStatus.RESULT_FAILD;
		}

		// Token: 0x060007F3 RID: 2035 RVA: 0x00022CB8 File Offset: 0x00020EB8
		public void Init()
		{
			DatabaseProductID databaseProductID = new DatabaseProductID();
			AMAppStore.RequestMenuList(databaseProductID.GetAllProductId());
		}

		// Token: 0x060007F4 RID: 2036 RVA: 0x00022CD8 File Offset: 0x00020ED8
		public bool Exec()
		{
			return AMAppStore.ResultWaitMenu(ref this.m_eResut);
		}

		// Token: 0x060007F5 RID: 2037 RVA: 0x00022CE8 File Offset: 0x00020EE8
		public AMAppStore.ResultStatus Result()
		{
			return this.m_eResut;
		}

		// Token: 0x060007F6 RID: 2038 RVA: 0x00022CF0 File Offset: 0x00020EF0
		public string ResultMessage()
		{
			Debug.Print(AMAppStore.GetErrorMenu());
			return (this.m_eResut != AMAppStore.ResultStatus.RESULT_SUCCESS) ? "通信に失敗しました。\n通信状態の良いところで再度接続を試みてください。" : "\u3000";
		}

		// Token: 0x040006EF RID: 1775
		private AMAppStore.ResultStatus m_eResut;
	}

	// Token: 0x02000129 RID: 297
	private class PurchaseList
	{
		// Token: 0x060007F7 RID: 2039 RVA: 0x00022D24 File Offset: 0x00020F24
		public PurchaseList()
		{
			this.m_eResut = AMAppStore.ResultStatus.RESULT_FAILD;
		}

		// Token: 0x060007F8 RID: 2040 RVA: 0x00022D34 File Offset: 0x00020F34
		public void Init()
		{
			DatabaseProductID databaseProductID = new DatabaseProductID();
			AMAppStore.RequestAfterPurchase(databaseProductID.GetAllProductId());
		}

		// Token: 0x060007F9 RID: 2041 RVA: 0x00022D54 File Offset: 0x00020F54
		public bool Exec()
		{
			return AMAppStore.ResultWaitPurchased(ref this.m_eResut);
		}

		// Token: 0x060007FA RID: 2042 RVA: 0x00022D64 File Offset: 0x00020F64
		public AMAppStore.ResultStatus Result()
		{
			return this.m_eResut;
		}

		// Token: 0x060007FB RID: 2043 RVA: 0x00022D6C File Offset: 0x00020F6C
		public string ResultMessage()
		{
			Debug.Print(AMAppStore.GetErrorPurchased());
			return (this.m_eResut != AMAppStore.ResultStatus.RESULT_SUCCESS) ? "通信に失敗しました。\n通信状態の良いところで再度接続を試みてください。" : "アドオンの購入履歴を更新しました。";
		}

		// Token: 0x040006F0 RID: 1776
		private AMAppStore.ResultStatus m_eResut;
	}

	// Token: 0x0200012A RID: 298
	private enum STATE
	{
		// Token: 0x040006F2 RID: 1778
		PAYINIT,
		// Token: 0x040006F3 RID: 1779
		PAYEXEC,
		// Token: 0x040006F4 RID: 1780
		INIT,
		// Token: 0x040006F5 RID: 1781
		EXEC,
		// Token: 0x040006F6 RID: 1782
		PURCHASE_INIT,
		// Token: 0x040006F7 RID: 1783
		PURCHASE_EXEC,
		// Token: 0x040006F8 RID: 1784
		SUCCESS,
		// Token: 0x040006F9 RID: 1785
		FAILD,
		// Token: 0x040006FA RID: 1786
		EXIT,
		// Token: 0x040006FB RID: 1787
		AFTER_EXIT
	}
}
