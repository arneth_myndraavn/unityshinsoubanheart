﻿using System;
using System.Collections.Generic;
using Game;
using Qoo.Game;
using UnityEngine;

// Token: 0x02000122 RID: 290
public class OptionVoiceWindow : OptionBaseWindow
{
	private Dictionary<string,int> dict = null;

	// Token: 0x060007CC RID: 1996 RVA: 0x00021A0C File Offset: 0x0001FC0C
	private void OnVoiceFlag()
	{
		UIActionToggle actionToggle = base.GetActionToggle("VoiceFlag");
		actionToggle.SwitchToggle();
		SysData.SetVoiceEnable(actionToggle.IsSwitchOn());
	}

	// Token: 0x060007CD RID: 1997 RVA: 0x00021A38 File Offset: 0x0001FC38
	private void MeterInit()
	{
		for (int i = 0; i < 11; i++)
		{
			string name = "Meter" + (i + 1).ToString("00");
			if (base.ExistsGameObject(name))
			{
				OptionVoiceMeter component = base.GetGameObject(name).GetComponent<OptionVoiceMeter>();
				component.Init(462, 66, i);
			}
		}
	}

	// Token: 0x060007CE RID: 1998 RVA: 0x00021A9C File Offset: 0x0001FC9C
	private void GetSysData()
	{
		UIActionToggle actionToggle = base.GetActionToggle("VoiceFlag");
		if (SysData.IsVoiceEnable())
		{
			actionToggle.SwitchOn();
		}
		else
		{
			actionToggle.SwitchOff();
		}
		this.MeterInit();
	}

	// Token: 0x060007CF RID: 1999 RVA: 0x00021AD8 File Offset: 0x0001FCD8
	protected override OptionBaseWindow.OptionType GetOptionType()
	{
		return OptionBaseWindow.OptionType.Voice;
	}

	// Token: 0x060007D0 RID: 2000 RVA: 0x00021ADC File Offset: 0x0001FCDC
	protected sealed override string[] newSceneTextureNameArray()
	{
		string[] collection = base.newSceneTextureNameArray();
		string[] collection2 = new string[]
		{
			"screen/sysmenu/sys_plate",
			"screen/sysmenu/sys_btn_10",
			"screen/sysmenu/sys_btn_11",
			"screen/sysmenu/sys_plate_voice",
			"screen/common/cancel",
			"sys_vname_cha01",
			"sys_vname_cha02",
			"sys_vname_cha03",
			"sys_vname_cha04",
			"sys_vname_cha05",
			"sys_vname_cha06",
			"sys_vname_cha07",
			"sys_vname_cha08",
			"sys_vname_cha09",
			"sys_vname_cha10",
			"sys_vname_cha11",
			"sys_volume_bar2",
			"sys_volume_meter2",
			"voice_ber1",
			"voice_ber2"
		};
		List<string> list = new List<string>();
		list.AddRange(collection);
		list.AddRange(collection2);
		return list.ToArray();
	}

	// Token: 0x060007D1 RID: 2001 RVA: 0x00021BC0 File Offset: 0x0001FDC0
	protected sealed override void BeforeInit()
	{
		Vector3 localPosition = base.transform.parent.localPosition;
		localPosition.z = -50f;
		base.transform.parent.localPosition = localPosition;
		for (int i = 0; i < 11; i++)
		{
			UIValue.CharaVoiceVolume[i] = SysData.GetVoiceVolume((CHAR_ID)i);
		}
	}

	// Token: 0x060007D2 RID: 2002 RVA: 0x00021C1C File Offset: 0x0001FE1C
	protected sealed override void AfterInit()
	{
		VoiceOptionList voiceOptionList = VoiceOptionList.Create(250, 155, this.wndz + 3, 490, 216, 11, base.transform);
		for (int i = 0; i < 11; i++)
		{
			string name = "Chara" + (i + 1).ToString("00");
			string name2 = "Frame" + (i + 1).ToString("00");
			string name3 = "Meter" + (i + 1).ToString("00");
			voiceOptionList.AddListItem(base.GetImageObject(name));
			voiceOptionList.AddListItem(base.GetImageObject(name2));
			voiceOptionList.AddListItem(base.GetImageObject(name3));
			base.GetGameObject(name3).AddComponent<OptionVoiceMeter>();
		}
		voiceOptionList.SetScrollbar(base.GetGameObject("ScrollArea"), base.GetGameObject("ScrollBar"));
		base.AddGameObject("VoiceOptionList", voiceOptionList.gameObject);
		this.GetSysData();
	}

	// Token: 0x060007D3 RID: 2003 RVA: 0x00021D24 File Offset: 0x0001FF24
	protected override void OnGraphicLoadComplete()
	{
		base.OnGraphicLoadComplete();
	}

	// Token: 0x060007D4 RID: 2004 RVA: 0x00021D2C File Offset: 0x0001FF2C
	protected sealed override BaseWindow.UIComponent[] newComponentArray()
	{
		BaseWindow.UIComponent[] collection = base.newComponentArray();
		BaseWindow.UIComponent[] collection2 = new BaseWindow.UIComponent[]
		{
			new BaseWindow.UICollision("Close", 0, 0, this.wndz, 960, 544),
			new BaseWindow.UIImage("BackGround", 153, 68, this.wndz + 1, "screen/sysmenu/sys_plate", true, true),
			new BaseWindow.UIButton("Clear", 208, 380, this.wndz + 2, "screen/sysmenu/sys_btn_10", true, true, 1, 2, 0),
			new BaseWindow.UIButton("Decide", 686, 380, this.wndz + 2, "screen/sysmenu/sys_btn_11", true, true, 1, 2, 0),
			new BaseWindow.UIToggle("VoiceFlag", 366, 99, this.wndz + 2, "screen/sysmenu/sys_plate_voice", true, true, 1, 2, 1, 0, false),
			new BaseWindow.UIButton("Cancel", 715, 73, this.wndz + 2, "screen/common/cancel", true, true, 1, 2, 0),
			new BaseWindow.UIImage("Chara01", 250, 155, this.wndz + 3, "sys_vname_cha01", false, true),
			new BaseWindow.UIImage("Chara02", 250, 230, this.wndz + 3, "sys_vname_cha02", false, true),
			new BaseWindow.UIImage("Chara03", 250, 305, this.wndz + 3, "sys_vname_cha03", false, true),
			new BaseWindow.UIImage("Chara04", 250, 380, this.wndz + 3, "sys_vname_cha04", false, true),
			new BaseWindow.UIImage("Chara05", 250, 455, this.wndz + 3, "sys_vname_cha05", false, true),
			new BaseWindow.UIImage("Chara06", 250, 530, this.wndz + 3, "sys_vname_cha06", false, true),
			new BaseWindow.UIImage("Chara07", 250, 605, this.wndz + 3, "sys_vname_cha07", false, true),
			new BaseWindow.UIImage("Chara08", 250, 680, this.wndz + 3, "sys_vname_cha08", false, true),
			new BaseWindow.UIImage("Chara09", 250, 755, this.wndz + 3, "sys_vname_cha09", false, true),
			new BaseWindow.UIImage("Chara10", 250, 830, this.wndz + 3, "sys_vname_cha10", false, true),
			new BaseWindow.UIImage("Chara11", 250, 905, this.wndz + 3, "sys_vname_cha11", false, true),
			new BaseWindow.UIImage("Frame01", 434, 185, this.wndz + 4, "sys_volume_bar2", false, true),
			new BaseWindow.UIImage("Frame02", 434, 260, this.wndz + 4, "sys_volume_bar2", false, true),
			new BaseWindow.UIImage("Frame03", 434, 335, this.wndz + 4, "sys_volume_bar2", false, true),
			new BaseWindow.UIImage("Frame04", 434, 410, this.wndz + 4, "sys_volume_bar2", false, true),
			new BaseWindow.UIImage("Frame05", 434, 485, this.wndz + 4, "sys_volume_bar2", false, true),
			new BaseWindow.UIImage("Frame06", 434, 560, this.wndz + 4, "sys_volume_bar2", false, true),
			new BaseWindow.UIImage("Frame07", 434, 635, this.wndz + 4, "sys_volume_bar2", false, true),
			new BaseWindow.UIImage("Frame08", 434, 710, this.wndz + 4, "sys_volume_bar2", false, true),
			new BaseWindow.UIImage("Frame09", 434, 785, this.wndz + 4, "sys_volume_bar2", false, true),
			new BaseWindow.UIImage("Frame10", 434, 860, this.wndz + 4, "sys_volume_bar2", false, true),
			new BaseWindow.UIImage("Frame11", 434, 935, this.wndz + 4, "sys_volume_bar2", false, true),
			new BaseWindow.UIImage("Meter01", 436, 187, this.wndz + 5, "sys_volume_meter2", false, true),
			new BaseWindow.UIImage("Meter02", 436, 262, this.wndz + 5, "sys_volume_meter2", false, true),
			new BaseWindow.UIImage("Meter03", 436, 337, this.wndz + 5, "sys_volume_meter2", false, true),
			new BaseWindow.UIImage("Meter04", 436, 412, this.wndz + 5, "sys_volume_meter2", false, true),
			new BaseWindow.UIImage("Meter05", 436, 487, this.wndz + 5, "sys_volume_meter2", false, true),
			new BaseWindow.UIImage("Meter06", 436, 562, this.wndz + 5, "sys_volume_meter2", false, true),
			new BaseWindow.UIImage("Meter07", 436, 637, this.wndz + 5, "sys_volume_meter2", false, true),
			new BaseWindow.UIImage("Meter08", 436, 712, this.wndz + 5, "sys_volume_meter2", false, true),
			new BaseWindow.UIImage("Meter09", 436, 787, this.wndz + 5, "sys_volume_meter2", false, true),
			new BaseWindow.UIImage("Meter10", 436, 862, this.wndz + 5, "sys_volume_meter2", false, true),
			new BaseWindow.UIImage("Meter11", 436, 937, this.wndz + 5, "sys_volume_meter2", false, true),
			new BaseWindow.UIImage("ScrollArea", 740, 155, this.wndz + 6, "voice_ber1", false, true),
			new BaseWindow.UIImage("ScrollBar", 740, 155, this.wndz + 7, "voice_ber2", true, true)
		};
		List<BaseWindow.UIComponent> list = new List<BaseWindow.UIComponent>();
		list.AddRange(collection);
		list.AddRange(collection2);
		return list.ToArray();
	}

	// Token: 0x060007D5 RID: 2005 RVA: 0x00022398 File Offset: 0x00020598
	protected sealed override void OnBaseWindowOnButton(string obj)
	{
		base.OnBaseWindowOnButton(obj);
		if (obj != null)
		{
			if (this.dict == null)
			{
				this.dict = new Dictionary<string, int>(1)
				{
					{
						"VoiceFlag",
						0
					}
				};
			}
			int num;
			if (this.dict.TryGetValue(obj, out num))
			{
				if (num == 0)
				{
					base.PlaySE_Select();
					this.OnVoiceFlag();
				}
			}
		}
		this.GetSysData();
	}

	// Token: 0x040006D5 RID: 1749
	private readonly int wndz;
}
