﻿using System;
using UnityEngine;

// Token: 0x02000190 RID: 400
public class UnityTimer : Singleton<UnityTimer>
{
	// Token: 0x17000187 RID: 391
	// (get) Token: 0x06000B7D RID: 2941 RVA: 0x00030EB0 File Offset: 0x0002F0B0
	// (set) Token: 0x06000B7E RID: 2942 RVA: 0x00030EB8 File Offset: 0x0002F0B8
	public float Now
	{
		get
		{
			return this.now;
		}
		private set
		{
			this.now = value;
		}
	}

	// Token: 0x17000188 RID: 392
	// (get) Token: 0x06000B7F RID: 2943 RVA: 0x00030EC4 File Offset: 0x0002F0C4
	// (set) Token: 0x06000B80 RID: 2944 RVA: 0x00030ECC File Offset: 0x0002F0CC
	public bool IsPause { get; set; }

	// Token: 0x06000B81 RID: 2945 RVA: 0x00030ED8 File Offset: 0x0002F0D8
	private void Awake()
	{
		this.old = Time.time;
	}

	// Token: 0x06000B82 RID: 2946 RVA: 0x00030EE8 File Offset: 0x0002F0E8
	private void Update()
	{
		float num = Time.time - this.old;
		if (!this.IsPause)
		{
			this.Now += num;
		}
		this.old = Time.time;
	}

	// Token: 0x06000B83 RID: 2947 RVA: 0x00030F28 File Offset: 0x0002F128
	public int GetTimeMilli()
	{
		return (int)(this.Now * 1000f);
	}

	// Token: 0x04000907 RID: 2311
	private float now;

	// Token: 0x04000908 RID: 2312
	private float old;
}
