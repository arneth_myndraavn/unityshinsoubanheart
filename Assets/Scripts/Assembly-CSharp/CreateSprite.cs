﻿using System;
using UnityEngine;

// Token: 0x02000152 RID: 338
public class CreateSprite : MonoBehaviour
{
	// Token: 0x06000969 RID: 2409 RVA: 0x00029AE8 File Offset: 0x00027CE8
	public static GameObject Create()
	{
		GameObject gameObject = new GameObject("sprite");
		MeshRenderer meshRenderer = gameObject.AddComponent<MeshRenderer>();
		if (meshRenderer.material != null)
		{
			UnityEngine.Object.Destroy(meshRenderer.material);
		}
		//meshRenderer.material = new Material(Resources.Load("Shader/Sprite/Sprite") as Shader);
		meshRenderer.material = new Material(Shader.Find("QO/Sprite") as Shader);
		meshRenderer.castShadows = false;
		meshRenderer.receiveShadows = false;
		MeshFilter meshFilter = gameObject.AddComponent<MeshFilter>();
		Mesh mesh = CreateSprite.GetMesh();
		meshFilter.sharedMesh = mesh;
		return gameObject;
	}

	// Token: 0x0600096A RID: 2410 RVA: 0x00029B60 File Offset: 0x00027D60
	public static GameObject CreateTextSprite()
	{
		GameObject gameObject = new GameObject("sprite");
		MeshRenderer meshRenderer = gameObject.AddComponent<MeshRenderer>();
		if (meshRenderer.material != null)
		{
			UnityEngine.Object.Destroy(meshRenderer.material);
		}
		meshRenderer.material = new Material(Resources.Load("Shader/Sprite/TextSprite") as Shader);

		meshRenderer.castShadows = false;
		meshRenderer.receiveShadows = false;
		MeshFilter meshFilter = gameObject.AddComponent<MeshFilter>();
		meshFilter.sharedMesh = new Mesh
		{
			name = "Text Sprite"
		};
		return gameObject;
	}

	// Token: 0x0600096B RID: 2411 RVA: 0x00029BE4 File Offset: 0x00027DE4
	private static Mesh GetMesh()
	{
		Mesh mesh = CreateSprite.CreateMesh();
		mesh.RecalculateBounds();
		;
		return mesh;
	}

	// Token: 0x0600096C RID: 2412 RVA: 0x00029C04 File Offset: 0x00027E04
	private static Mesh CreateMesh()
	{
		Mesh mesh = new Mesh();
		mesh.name = "Sprite";
		mesh.vertices = CreateSprite.base_vertex;
		mesh.triangles = CreateSprite.triangles;
		mesh.uv = CreateSprite.uv;
		mesh.RecalculateNormals();
		return mesh;
	}

	// Token: 0x0600096D RID: 2413 RVA: 0x00029C4C File Offset: 0x00027E4C
	public static void CreateSpriteCamera(GameObject obj, int ScreenH, bool IsSprite = true)
	{
		obj.transform.localPosition = new Vector3(0f, 0f, -1000f);
		obj.transform.localScale = new Vector3(1f, 1f, 1f);
		obj.GetComponent<Camera>().orthographic = true;
		obj.GetComponent<Camera>().orthographicSize = (float)(ScreenH / 2);
		obj.GetComponent<Camera>().nearClipPlane = 0f;
		obj.GetComponent<Camera>().farClipPlane = 20000f;
		obj.GetComponent<Camera>().renderingPath = RenderingPath.VertexLit;
		obj.GetComponent<Camera>().clearFlags = CameraClearFlags.Color;
		obj.GetComponent<Camera>().backgroundColor = new Color(0f, 0f, 0f, 1f);
		if (IsSprite)
		{
			obj.GetComponent<Camera>().cullingMask &= 1 << LayerMask.NameToLayer("Sprite");
			obj.GetComponent<Camera>().gameObject.layer = LayerMask.NameToLayer("Sprite");
		}
		else
		{
			obj.GetComponent<Camera>().cullingMask &= ~(1 << LayerMask.NameToLayer("Sprite"));
		}
	}

	// Token: 0x040007CE RID: 1998
	private static Vector3[] base_vertex = new Vector3[]
	{
		new Vector3(0f, 0f, 0f),
		new Vector3(1f, 0f, 0f),
		new Vector3(1f, -1f, 0f),
		new Vector3(0f, -1f, 0f)
	};

	// Token: 0x040007CF RID: 1999
	private static int[] triangles = new int[]
	{
		0,
		1,
		2,
		2,
		3,
		0
	};

	// Token: 0x040007D0 RID: 2000
	private static Vector2[] uv = new Vector2[]
	{
		new Vector2(0f, 1f),
		new Vector2(1f, 1f),
		new Vector2(1f, 0f),
		new Vector2(0f, 0f)
	};
}
