﻿using System;

// Token: 0x0200017B RID: 379
public class Point2
{
	// Token: 0x06000AC8 RID: 2760 RVA: 0x0002F5B0 File Offset: 0x0002D7B0
	public Point2(int x_ = 0, int y_ = 0)
	{
		this.x = x_;
		this.y = y_;
	}

	// Token: 0x06000AC9 RID: 2761 RVA: 0x0002F5C8 File Offset: 0x0002D7C8
	public void Scale(float fX, float fY)
	{
		this.x = (int)((float)this.x * fX);
		this.y = (int)((float)this.y * fY);
	}

	// Token: 0x040008B7 RID: 2231
	public int x;

	// Token: 0x040008B8 RID: 2232
	public int y;
}
