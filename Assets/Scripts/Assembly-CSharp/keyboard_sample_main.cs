﻿using System;
using System.Collections;
using UnityEngine;

// Token: 0x02000004 RID: 4
public class keyboard_sample_main : MonoBehaviour
{
	// Token: 0x06000009 RID: 9 RVA: 0x00002178 File Offset: 0x00000378
	private void OnMouseDown()
	{
		Debug.Log("OnMouseDown");
		base.StartCoroutine(this.UserInput());
	}

	// Token: 0x0600000A RID: 10 RVA: 0x00002194 File Offset: 0x00000394
	private IEnumerator UserInput()
	{
		yield return base.StartCoroutine(ScreenKeyboardManager.Open(ScreenKeyboardManager.InputText, ScreenKeyboardManager.InputTextLastName));
		TextMesh tMesh = base.GetComponent<TextMesh>();
		tMesh.text = ScreenKeyboardManager.InputText;
		yield break;
	}
}
