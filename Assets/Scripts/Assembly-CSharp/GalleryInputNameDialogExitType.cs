﻿using System;

// Token: 0x020000FA RID: 250
public enum GalleryInputNameDialogExitType
{
	// Token: 0x04000616 RID: 1558
	INVALID,
	// Token: 0x04000617 RID: 1559
	INPUT,
	// Token: 0x04000618 RID: 1560
	YES,
	// Token: 0x04000619 RID: 1561
	NO,
	// Token: 0x0400061A RID: 1562
	RETRY
}
