﻿using System;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using QO.IO;
using Qoo;
using Qoo.AM.Game;
using Qoo.Def;
using Qoo.Game;
using UnityEngine;

// Token: 0x0200013E RID: 318
public class SaveLoadManager
{
	// Token: 0x060008BA RID: 2234 RVA: 0x000268C4 File Offset: 0x00024AC4
	public static void Delete(int index)
	{
		string str = SaveLoadManager.SAVEDATA_NAME + index.ToString();
		if (File.Exists(SaveLoadManager.SAVEDIR + "/" + str))
		{
			File.Delete(SaveLoadManager.SAVEDIR + "/" + str);
		}
		if (File.Exists(SaveLoadManager.SAVEDIR + "/H_" + str))
		{
			File.Delete(SaveLoadManager.SAVEDIR + "/H_" + str);
		}
	}

	// Token: 0x060008BB RID: 2235 RVA: 0x00026944 File Offset: 0x00024B44
	public static void DeleteAll()
	{
		SaveLoadManager.DeleteAllGameSaveData();
		SaveLoadManager.DeleteAutoSaveData();
		SaveLoadManager.DeleteSystemSaveData();
	}

	// Token: 0x060008BC RID: 2236 RVA: 0x00026958 File Offset: 0x00024B58
	public static void DeleteAllGameSaveData()
	{
		for (int i = 0; i < SaveLoadManager.AUTO_SAVE_INDEX; i++)
		{
			SaveLoadManager.Delete(i);
		}
	}

	// Token: 0x060008BD RID: 2237 RVA: 0x00026984 File Offset: 0x00024B84
	public static void DeleteSystemSaveData()
	{
		SaveLoadManager.Delete(SaveLoadManager.SYSTEM_DATA_INDEX);
	}

	// Token: 0x060008BE RID: 2238 RVA: 0x00026990 File Offset: 0x00024B90
	public static void DeleteAutoSaveData()
	{
		SaveLoadManager.Delete(SaveLoadManager.AUTO_SAVE_INDEX);
	}

	// Token: 0x060008BF RID: 2239 RVA: 0x0002699C File Offset: 0x00024B9C
	public static bool Save(byte[] bytes, SaveFileInfo info, int index)
	{
		string text = SaveLoadManager.SAVEDATA_NAME + index.ToString();
		byte[] array = null;
		for (int i = 0; i < SaveLoadManager.SAVE_RETRY_CNT; i++)
		{
			try
			{
				array = SaveLoadManager.SaveBytes(text + "_TMP", bytes, false);
				if (array != null)
				{
					break;
				}
			}
			catch (IOException ex)
			{
				Qoo.Debug.Print(ex.Message);
			}
			catch (Exception ex2)
			{
				Qoo.Debug.Print(ex2.Message);
				DispAssert.Show(ErrorDef.ASSERT_ERROR_CODE.FAIL_SAVE_CATCH_EXCEPTION, UnityApp.Instance.isJapanese ? "ゲームデータの保存を正常に終了できませんでした。" : "The game data could not be saved");
				break;
			}
		}
		if (array == null)
		{
			Qoo.Debug.Print("####Save file create error");
			DispAssert.Show(ErrorDef.ASSERT_ERROR_CODE.FAIL_SAVE_DATA_CREATE, UnityApp.Instance.isJapanese ? "ゲームデータの保存を正常に終了できませんでした。" : "The game data could not be saved");
			return false;
		}
		string text2 = BytesHash.CreateHashCode(array);
		if (text2 == null)
		{
			Qoo.Debug.Print("####hash = null death");
			DispAssert.Show(ErrorDef.ASSERT_ERROR_CODE.FAIL_SAVE_HASHCODE_CREATE, UnityApp.Instance.isJapanese ? "ゲームデータの保存を正常に終了できませんでした。" : "The game data could not be saved");
			return false;
		}
		info.HashCode = text2;
		if (SaveLoadManager.SaveToXML<SaveFileInfo>("H_" + text + "_TMP", info, Encoding.UTF8, false) == null)
		{
			Qoo.Debug.Print("header file save error");
			DispAssert.Show(ErrorDef.ASSERT_ERROR_CODE.FAIL_SAVE_HEADER_CREATE, UnityApp.Instance.isJapanese ? "ゲームデータの保存を正常に終了できませんでした。" : "The game data could not be saved");
			return false;
		}
		try
		{
			if (File.Exists(SaveLoadManager.SAVEDIR + "/" + text))
			{
				File.Delete(SaveLoadManager.SAVEDIR + "/" + text);
			}
			if (File.Exists(SaveLoadManager.SAVEDIR + "/H_" + text))
			{
				File.Delete(SaveLoadManager.SAVEDIR + "/H_" + text);
			}
			File.Move(SaveLoadManager.SAVEDIR + "/" + text + "_TMP", SaveLoadManager.SAVEDIR + "/" + text);
			File.Move(SaveLoadManager.SAVEDIR + "/H_" + text + "_TMP", SaveLoadManager.SAVEDIR + "/H_" + text);
		}
		catch (Exception ex3)
		{
			Qoo.Debug.Print(ex3.Message);
			DispAssert.Show(ErrorDef.ASSERT_ERROR_CODE.FAIL_SAVE_RENAME, "ゲームデータの保存を正常に終了できませんでした。");
		}
		return true;
	}

	// Token: 0x060008C0 RID: 2240 RVA: 0x00026BD0 File Offset: 0x00024DD0
	public static bool SaveSystem()
	{
		SysSaveData sysSaveData = new SysSaveData();
		byte[] bytes = sysSaveData.Save();
		SaveFileInfo info = new SaveFileInfo();
		bool result = SaveLoadManager.Save(bytes, info, SaveLoadManager.SYSTEM_DATA_INDEX);
		Qoo.Debug.Print("SaveSysData");
		return result;
	}

	// Token: 0x060008C1 RID: 2241 RVA: 0x00026C08 File Offset: 0x00024E08
	public static bool SaveAuto()
	{
		byte[] array = GameData.Save();
		if (array != null)
		{
			bool result = SaveLoadManager.Save(array, new SaveFileInfo
			{
				LastMessage = GameData.LastMessage,
				PlayCharaId = (int)GameData.GetRoute(),
				SceneName = GameData.GetSceneName(),
				TimeStamp = DateTime.Now.ToString("yyyy／MM／dd  HH：mm")
			}, SaveLoadManager.AUTO_SAVE_INDEX);
			Qoo.Debug.Print("SaveAutoSave");
			return result;
		}
		Qoo.Debug.Print("SaveAutoSave:Fail");
		return false;
	}

	// Token: 0x060008C2 RID: 2242 RVA: 0x00026C84 File Offset: 0x00024E84
	public static byte[] Load(int index)
	{
		string text = SaveLoadManager.SAVEDATA_NAME + index.ToString();
		if (!File.Exists(SaveLoadManager.SAVEDIR + "/" + text))
		{
			return null;
		}
		SaveFileInfo saveFileInfo = SaveLoadManager.LoadHeader(index);
		byte[] array = null;
		for (int i = 0; i < SaveLoadManager.SAVE_RETRY_CNT; i++)
		{
			try
			{
				array = SaveLoadManager.LoadBytes(text, false);
				if (array != null)
				{
					break;
				}
			}
			catch (IOException ex)
			{
				Qoo.Debug.Print(ex.Message);
			}
			catch (Exception ex2)
			{
				Qoo.Debug.Print(ex2.Message);
				DispAssert.Show(ErrorDef.ASSERT_ERROR_CODE.FAIL_LOAD_CATCH_EXCEPTION, UnityApp.Instance.isJapanese ? "ゲームデータの読込を正常に終了できませんでした。" : "The game data could not be loaded");
				break;
			}
		}
		if (array == null)
		{
			DispAssert.Show(ErrorDef.ASSERT_ERROR_CODE.FAIL_LOAD, UnityApp.Instance.isJapanese ? "ゲームデータの読込を正常に終了できませんでした。" : "The game data could not be loaded");
			return null;
		}
		string hashCode = BytesHash.CreateHashCode(array);
		if (!saveFileInfo.CheckHash(hashCode))
		{
			Qoo.Debug.Print("hash ga tigau yo");
			DispAssert.Show(ErrorDef.ASSERT_ERROR_CODE.UNMATCH_SAVE_HASH, UnityApp.Instance.isJapanese ? "ゲームデータの読込を正常に終了できませんでした。" : "The game data could not be loaded");
			return null;
		}
		return array;
	}

	// Token: 0x060008C3 RID: 2243 RVA: 0x00026DA0 File Offset: 0x00024FA0
	public static byte[] LoadSystem()
	{
		return SaveLoadManager.Load(SaveLoadManager.SYSTEM_DATA_INDEX);
	}

	// Token: 0x060008C4 RID: 2244 RVA: 0x00026DAC File Offset: 0x00024FAC
	public static byte[] LoadAuto()
	{
		return SaveLoadManager.Load(SaveLoadManager.AUTO_SAVE_INDEX);
	}

	// Token: 0x060008C5 RID: 2245 RVA: 0x00026DB8 File Offset: 0x00024FB8
	public static SaveFileInfo LoadHeader(int index)
	{
		string str = SaveLoadManager.SAVEDATA_NAME + index.ToString();
		return SaveLoadManager.LoadXML<SaveFileInfo>("H_" + str, false);
	}

	// Token: 0x060008C6 RID: 2246 RVA: 0x00026DEC File Offset: 0x00024FEC
	private static byte[] SaveBytes(string fname, byte[] savedata, bool encrypt = false)
	{
		string text = SaveLoadManager.SAVEDIR + "/" + fname;
		QODirectory.CreatePathToFileDirectory(text);
		byte[] array = savedata;
		if (encrypt)
		{
			array = BytesEncrypter.EncryptRijndael(array, SaveLoadManager.SALT_STRING, SaveLoadManager.PASSWORD);
		}
		byte[] result;
		try
		{
			File.WriteAllBytes(text, array);
			result = array;
		}
		catch (IOException ex)
		{
			Qoo.Debug.Print("####Catch IOException!!");
			throw ex;
		}
		catch (Exception ex2)
		{
			Qoo.Debug.Print("####Catch Other Exception!!");
			throw ex2;
		}
		return result;
	}

	// Token: 0x060008C7 RID: 2247 RVA: 0x00026E98 File Offset: 0x00025098
	private static byte[] LoadBytes(string fname, bool decrypt = false)
	{
		string path = SaveLoadManager.SAVEDIR + "/" + fname;
		if (!File.Exists(path))
		{
			return null;
		}
		byte[] result;
		try
		{
			byte[] array = File.ReadAllBytes(path);
			if (decrypt)
			{
				array = BytesEncrypter.DecryptRijndael(array, SaveLoadManager.SALT_STRING, SaveLoadManager.PASSWORD);
			}
			result = array;
		}
		catch (IOException ex)
		{
			Qoo.Debug.Print("####Catch IOException!!");
			throw ex;
		}
		catch (Exception ex2)
		{
			Qoo.Debug.Print("####Catch Other Exception!!");
			throw ex2;
		}
		return result;
	}

	// Token: 0x060008C8 RID: 2248 RVA: 0x00026F48 File Offset: 0x00025148
	private static byte[] SaveToXML<T>(string fname, object saveData, Encoding encode, bool encrypt = false) where T : class
	{
		byte[] savedata = SaveLoadManager.SerializeXML<T>(saveData, encode);
		return SaveLoadManager.SaveBytes(fname, savedata, encrypt);
	}

	// Token: 0x060008C9 RID: 2249 RVA: 0x00026F68 File Offset: 0x00025168
	private static T LoadXML<T>(string fname, bool decrypt = false) where T : class
	{
		byte[] array = SaveLoadManager.LoadBytes(fname, decrypt);
		if (array == null)
		{
			return (T)((object)null);
		}
		return SaveLoadManager.DeserializeXML<T>(array);
	}

	// Token: 0x060008CA RID: 2250 RVA: 0x00026F90 File Offset: 0x00025190
	private static byte[] SerializeXML<T>(object data, Encoding encode) where T : class
	{
		XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
		MemoryStream memoryStream = new MemoryStream();
		StreamWriter streamWriter = new StreamWriter(memoryStream, encode);
		xmlSerializer.Serialize(streamWriter, data);
		byte[] buffer = memoryStream.GetBuffer();
		streamWriter.Close();
		return buffer;
	}

	// Token: 0x060008CB RID: 2251 RVA: 0x00026FD4 File Offset: 0x000251D4
	private static T DeserializeXML<T>(byte[] bytes) where T : class
	{
		MemoryStream stream = new MemoryStream(bytes);
		XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
		object obj = xmlSerializer.Deserialize(stream);
		return (T)((object)obj);
	}

	// Token: 0x0400077A RID: 1914
	public static readonly int SYSTEM_DATA_INDEX = 99;

	// Token: 0x0400077B RID: 1915
	public static readonly int AUTO_SAVE_INDEX = 9;

	// Token: 0x0400077C RID: 1916
	public static readonly string SAVEDATA_NAME = "SAVEDATA";

	// Token: 0x0400077D RID: 1917
	public static readonly int SAVE_RETRY_CNT = 5;

	// Token: 0x0400077E RID: 1918
	private static readonly string SALT_STRING = "jp.co.qourier";

	// Token: 0x0400077F RID: 1919
	private static readonly string PASSWORD = "Qourier1045";

	// Token: 0x04000780 RID: 1920
	private static readonly string SAVEDIR = Application.persistentDataPath + "/save";
}
