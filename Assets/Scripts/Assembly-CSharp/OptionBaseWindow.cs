﻿using System;
using Game;
using Qoo.Game;
using UnityEngine;

// Token: 0x020000EB RID: 235
public abstract class OptionBaseWindow : BaseWindow
{
	// Token: 0x0600065C RID: 1628
	protected abstract OptionBaseWindow.OptionType GetOptionType();

	// Token: 0x0600065D RID: 1629 RVA: 0x0001A71C File Offset: 0x0001891C
	public void BackupOptionValue()
	{
		this.m_OptionValueBackUp.GetSysData();
	}

	// Token: 0x0600065E RID: 1630 RVA: 0x0001A72C File Offset: 0x0001892C
	public void RestoreOptionValue()
	{
		this.m_OptionValueBackUp.SetSysData();
	}

	// Token: 0x0600065F RID: 1631 RVA: 0x0001A73C File Offset: 0x0001893C
	public void ResetOptionValue()
	{
		switch (this.m_OptionType)
		{
		case OptionBaseWindow.OptionType.Text:
			this.ResetTextOptionValue();
			break;
		case OptionBaseWindow.OptionType.Screen:
			this.ResetScreenOptionValue();
			break;
		case OptionBaseWindow.OptionType.Sound:
			this.ResetSoundOptionValue();
			break;
		case OptionBaseWindow.OptionType.Voice:
			this.ResetVoiceOptionValue();
			break;
		}
		SysData.Apply();
	}

	// Token: 0x06000660 RID: 1632 RVA: 0x0001A79C File Offset: 0x0001899C
	public void ResetTextOptionValue()
	{
		SysData.SetTextSpeed(1);
		SysData.SetAutoPage(1);
	}

	// Token: 0x06000661 RID: 1633 RVA: 0x0001A7AC File Offset: 0x000189AC
	public void ResetScreenOptionValue()
	{
		SysData.SetEnableLoveAnim(false);
		SysData.SetDrawFace(true);
	}

	// Token: 0x06000662 RID: 1634 RVA: 0x0001A7BC File Offset: 0x000189BC
	public void ResetSoundOptionValue()
	{
		SysData.SetVolumeBgm(3);
		SysData.SetVolumeSe(3);
		SysData.SetVolumeSys(3);
	}

	// Token: 0x06000663 RID: 1635 RVA: 0x0001A7D0 File Offset: 0x000189D0
	public void ResetVoiceOptionValue()
	{
		SysData.SetVoiceEnable(true);
		for (int i = 0; i < 11; i++)
		{
			SysData.SetVoiceVolume((CHAR_ID)i, 5);
		}
	}

	// Token: 0x06000664 RID: 1636 RVA: 0x0001A800 File Offset: 0x00018A00
	private int Step(int value, int[] steps)
	{
		for (int i = steps.Length - 1; i >= 0; i--)
		{
			if (value >= steps[i])
			{
				return i + 1;
			}
		}
		return 0;
	}

	// Token: 0x06000665 RID: 1637 RVA: 0x0001A834 File Offset: 0x00018A34
	private int GetLoveStep(CHAR_ID chara, int love)
	{
		int result = 0;
		switch (chara)
		{
		case CHAR_ID.BLOOD:
		case CHAR_ID.ELLIOT:
		case CHAR_ID.DEEDUM:
			result = this.Step(love, new int[]
			{
				2,
				4,
				6,
				9,
				11
			});
			break;
		case CHAR_ID.VIVALDI:
		case CHAR_ID.PETER:
		case CHAR_ID.ACE:
		case CHAR_ID.GOWLAND:
		case CHAR_ID.BORIS:
		case CHAR_ID.JULIUS:
			result = this.Step(love, new int[]
			{
				2,
				4,
				6,
				9,
				12
			});
			break;
		case CHAR_ID.NIGHTMARE:
			result = this.Step(love, new int[]
			{
				3,
				4,
				5,
				6,
				7
			});
			break;
		}
		return result;
	}

	// Token: 0x06000666 RID: 1638 RVA: 0x0001A8D4 File Offset: 0x00018AD4
	private void GetLoveStatus()
	{
		this.m_Route = GameData.GetRoute();
		if (this.m_Route == CHAR_ID.NOTHING)
		{
			return;
		}
		this.m_Love = GameData.GetParamInt("love", Chara.GetNameFromId(this.m_Route));
		this.m_LoveStep = this.GetLoveStep(this.m_Route, this.m_Love);
	}

	// Token: 0x06000667 RID: 1639 RVA: 0x0001A92C File Offset: 0x00018B2C
	private void SaveSysData()
	{
		SaveLoadManager.SaveSystem();
	}

	// Token: 0x06000668 RID: 1640 RVA: 0x0001A934 File Offset: 0x00018B34
	protected override void OnAwake()
	{
		base.OnAwake();
		this.m_OptionType = this.GetOptionType();
		this.m_OptionValueBackUp = new OptionBaseWindow.OptionValue();
		this.m_OptionValueBackUp.GetSysData();
	}

	// Token: 0x06000669 RID: 1641 RVA: 0x0001A96C File Offset: 0x00018B6C
	protected override string[] newSceneTextureNameArray()
	{
		this.GetLoveStatus();
		if (this.m_Route == CHAR_ID.NOTHING)
		{
			return new string[0];
		}
		string text = this.m_LoveStep.ToString("sys_lovemark_0");
		string text2 = ((int)(this.m_Route + 1)).ToString("sys_love_cha00");
		return new string[]
		{
			text,
			text2
		};
	}

	// Token: 0x0600066A RID: 1642 RVA: 0x0001A9C8 File Offset: 0x00018BC8
	protected override BaseWindow.UIComponent[] newComponentArray()
	{
		this.GetLoveStatus();
		if (this.m_Route == CHAR_ID.NOTHING)
		{
			return new BaseWindow.UIComponent[0];
		}
		string fpath_ = this.m_LoveStep.ToString("sys_lovemark_0");
		string fpath_2 = ((int)(this.m_Route + 1)).ToString("sys_love_cha00");
		return new BaseWindow.UIComponent[]
		{
			new BaseWindow.UIImage("LoveStep", 124, 77, 5, fpath_, false, true),
			new BaseWindow.UIImage("Route", 71, 276, 5, fpath_2, false, true)
		};
	}

	// Token: 0x0600066B RID: 1643 RVA: 0x0001AA4C File Offset: 0x00018C4C
	protected void CreateScreenBackEffect()
	{
		Color black = Color.black;
		black.a = 0.5f;
		ScreenEffect.Init(0, black, base.transform);
	}

	// Token: 0x0600066C RID: 1644 RVA: 0x0001AA78 File Offset: 0x00018C78
	protected override void OnGraphicLoadComplete()
	{
		base.OnGraphicLoadComplete();
		this.CreateScreenBackEffect();
	}

	// Token: 0x0600066D RID: 1645 RVA: 0x0001AA88 File Offset: 0x00018C88
	protected override void OnBaseWindowOnButton(string obj)
	{
		switch (obj)
		{
		case "Decide":
			base.PlaySE_Ok();
			base.DeleteLastAddScene();
			break;
		case "Clear":
			base.PlaySE_Cancel();
			this.ResetOptionValue();
			break;
		case "Cancel":
		case "Close":
			base.PlaySE_Cancel();
			this.RestoreOptionValue();
			base.DeleteLastAddScene();
			break;
		}
	}

	// Token: 0x040005CD RID: 1485
	private OptionBaseWindow.OptionType m_OptionType;

	// Token: 0x040005CE RID: 1486
	private OptionBaseWindow.OptionValue m_OptionValueBackUp;

	// Token: 0x040005CF RID: 1487
	private int m_Love;

	// Token: 0x040005D0 RID: 1488
	private int m_LoveStep;

	// Token: 0x040005D1 RID: 1489
	private CHAR_ID m_Route;

	// Token: 0x020000EC RID: 236
	private class OptionValue
	{
		// Token: 0x0600066F RID: 1647 RVA: 0x0001AB60 File Offset: 0x00018D60
		public void GetSysData()
		{
			this.IsLoveAnim = SysData.IsEnableLoveAnim();
			this.IsDrawFace = SysData.IsDrawFace();
			this.TextWait = SysData.GetTextSpeed();
			this.AutoWait = SysData.GetAutoPage();
			this.VolumeBGM = SysData.GetVolumeBgm();
			this.VolumeSE = SysData.GetVolumeSe();
			this.VolumeSYS = SysData.GetVolumeSys();
			this.IsPlayVoice = SysData.IsVoiceEnable();
			for (int i = 0; i < 11; i++)
			{
				this.VolumeVoice[i] = SysData.GetVoiceVolume((CHAR_ID)i);
			}
		}

		// Token: 0x06000670 RID: 1648 RVA: 0x0001ABE8 File Offset: 0x00018DE8
		public void SetSysData()
		{
			SysData.SetEnableLoveAnim(this.IsLoveAnim);
			SysData.SetDrawFace(this.IsDrawFace);
			SysData.SetTextSpeed(this.TextWait);
			SysData.SetAutoPage(this.AutoWait);
			SysData.SetVolumeBgm(this.VolumeBGM);
			SysData.SetVolumeSe(this.VolumeSE);
			SysData.SetVolumeSys(this.VolumeSYS);
			SysData.SetVoiceEnable(this.IsPlayVoice);
			for (int i = 0; i < 11; i++)
			{
				SysData.SetVoiceVolume((CHAR_ID)i, this.VolumeVoice[i]);
			}
		}

		// Token: 0x040005D3 RID: 1491
		public bool IsLoveAnim;

		// Token: 0x040005D4 RID: 1492
		public bool IsDrawFace;

		// Token: 0x040005D5 RID: 1493
		public int TextWait;

		// Token: 0x040005D6 RID: 1494
		public int AutoWait;

		// Token: 0x040005D7 RID: 1495
		public int VolumeBGM;

		// Token: 0x040005D8 RID: 1496
		public int VolumeSE;

		// Token: 0x040005D9 RID: 1497
		public int VolumeSYS;

		// Token: 0x040005DA RID: 1498
		public bool IsPlayVoice;

		// Token: 0x040005DB RID: 1499
		public int[] VolumeVoice = new int[11];
	}

	// Token: 0x020000ED RID: 237
	public enum OptionType
	{
		// Token: 0x040005DD RID: 1501
		Text,
		// Token: 0x040005DE RID: 1502
		Screen,
		// Token: 0x040005DF RID: 1503
		Sound,
		// Token: 0x040005E0 RID: 1504
		Voice,
		// Token: 0x040005E1 RID: 1505
		Back,
		// Token: 0x040005E2 RID: 1506
		Skip
	}
}
