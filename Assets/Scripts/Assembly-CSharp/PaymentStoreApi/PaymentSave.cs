﻿using System;
using Game;
using PaymentGameApi;
using Qoo;
using Qoo.Game;

namespace PaymentStoreApi
{
	// Token: 0x020000E6 RID: 230
	public class PaymentSave
	{
		// Token: 0x06000636 RID: 1590 RVA: 0x00019C60 File Offset: 0x00017E60
		public void Save(string szProductId)
		{
			DatabaseProductID databaseProductID = new DatabaseProductID();
			CHAR_ID charaIdForProductId = databaseProductID.GetCharaIdForProductId(szProductId);
			DatabaseProductID.CheckType checkTypeForProductId = databaseProductID.GetCheckTypeForProductId(szProductId);
			if (charaIdForProductId == CHAR_ID.NIGHTMARE)
			{
				switch (checkTypeForProductId)
				{
				case DatabaseProductID.CheckType.ROUTE_VOICE:
					SysData.SetPayFull(true, true);
					break;
				case DatabaseProductID.CheckType.ROUTE:
					SysData.SetPayFullRoute(true);
					break;
				case DatabaseProductID.CheckType.VOICE:
					SysData.SetPayFullVoice(true);
					break;
				}
				Debug.Print("セーブデータ：フルパック " + checkTypeForProductId);
			}
			else
			{
				switch (checkTypeForProductId)
				{
				case DatabaseProductID.CheckType.ROUTE_VOICE:
					SysData.SetPay(charaIdForProductId, true, true);
					break;
				case DatabaseProductID.CheckType.ROUTE:
					SysData.SetPayRoute(charaIdForProductId, true);
					break;
				case DatabaseProductID.CheckType.VOICE:
					SysData.SetPayVoice(charaIdForProductId, true);
					break;
				}
				Debug.Print(string.Concat(new object[]
				{
					"セーブデータ:Chara:",
					charaIdForProductId,
					":",
					checkTypeForProductId
				}));
			}
			SaveLoadManager.SaveSystem();
		}
	}
}
