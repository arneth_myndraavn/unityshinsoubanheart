﻿using System;

// Token: 0x0200017A RID: 378
public class Point3
{
	// Token: 0x06000AC6 RID: 2758 RVA: 0x0002F550 File Offset: 0x0002D750
	public Point3(int x_ = 0, int y_ = 0, int z_ = 0)
	{
		this.x = x_;
		this.y = y_;
		this.z = z_;
	}

	// Token: 0x06000AC7 RID: 2759 RVA: 0x0002F570 File Offset: 0x0002D770
	public void Scale(float fX, float fY, float fZ)
	{
		this.x = (int)((float)this.x * fX);
		this.y = (int)((float)this.y * fY);
		this.z = (int)((float)this.z * fZ);
	}

	// Token: 0x040008B4 RID: 2228
	public int x;

	// Token: 0x040008B5 RID: 2229
	public int y;

	// Token: 0x040008B6 RID: 2230
	public int z;
}
