﻿using System;
using System.Collections.Generic;
using Qoo.Input;
using UnityEngine;

// Token: 0x0200011D RID: 285
public class OptionBackWindow : OptionBaseWindow
{
	private Dictionary<string,int> dict = null;

	// Token: 0x06000793 RID: 1939 RVA: 0x000205AC File Offset: 0x0001E7AC
	private void OnClose()
	{
		GameObject gameObject = GameObject.Find(UIValue.SCENE_ADVMENU + "/Wnd");
		SubTaskBaseWindow component = gameObject.GetComponent<SubTaskBaseWindow>();
		component.IsTaskEnd = true;
		base.DeleteLastAddScene();
		ScreenEffect.Term();
	}

	// Token: 0x06000794 RID: 1940 RVA: 0x000205E8 File Offset: 0x0001E7E8
	protected override OptionBaseWindow.OptionType GetOptionType()
	{
		return OptionBaseWindow.OptionType.Back;
	}

	// Token: 0x06000795 RID: 1941 RVA: 0x000205EC File Offset: 0x0001E7EC
	protected sealed override string[] newSceneTextureNameArray()
	{
		return new string[]
		{
			"screen/sysmenu/skip_3",
			"screen/sysmenu/skip_4"
		};
	}

	// Token: 0x06000796 RID: 1942 RVA: 0x00020614 File Offset: 0x0001E814
	protected sealed override void BeforeInit()
	{
		Vector3 localPosition = base.transform.parent.localPosition;
		localPosition.z = -50f;
		base.transform.parent.localPosition = localPosition;
	}

	// Token: 0x06000797 RID: 1943 RVA: 0x00020650 File Offset: 0x0001E850
	protected sealed override void AfterInit()
	{
	}

	// Token: 0x06000798 RID: 1944 RVA: 0x00020654 File Offset: 0x0001E854
	protected override void OnGraphicLoadComplete()
	{
		base.OnGraphicLoadComplete();
	}

	// Token: 0x06000799 RID: 1945 RVA: 0x0002065C File Offset: 0x0001E85C
	protected sealed override BaseWindow.UIComponent[] newComponentArray()
	{
		return new BaseWindow.UIComponent[]
		{
			new BaseWindow.UICollision("Close", 0, 0, this.wndz, 960, 544),
			new BaseWindow.UIButton("NextNodeSkip", 305, 139, this.wndz + 1, "screen/sysmenu/skip_3", true, true, 1, 2, 0),
			new BaseWindow.UIButton("AutoSkip", 305, 268, this.wndz + 1, "screen/sysmenu/skip_4", true, true, 1, 2, 0)
		};
	}

	// Token: 0x0600079A RID: 1946 RVA: 0x000206E4 File Offset: 0x0001E8E4
	protected sealed override void OnBaseWindowOnButton(string obj)
	{
		if (obj != null)
		{
			if (this.dict == null)
			{
				this.dict = new Dictionary<string, int>(2)
				{
					{
						"NextNodeSkip",
						0
					},
					{
						"AutoSkip",
						1
					}
				};
			}
			int num;
			if (this.dict.TryGetValue(obj, out num))
			{
				if (num != 0)
				{
					if (num == 1)
					{
						base.PlaySE_Ok();
						KsInput.SetSkip(false);
						this.OnClose();
					}
				}
				else
				{
					base.PlaySE_Ok();
					KsInput.SetJump(false);
					this.OnClose();
				}
			}
		}
		base.OnBaseWindowOnButton(obj);
	}

	// Token: 0x040006C8 RID: 1736
	private readonly int wndz;
}
