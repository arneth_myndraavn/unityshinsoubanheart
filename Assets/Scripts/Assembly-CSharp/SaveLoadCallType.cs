﻿using System;

// Token: 0x020000FE RID: 254
public enum SaveLoadCallType
{
	// Token: 0x04000629 RID: 1577
	INVALID,
	// Token: 0x0400062A RID: 1578
	TITLE,
	// Token: 0x0400062B RID: 1579
	ADVMENU
}
