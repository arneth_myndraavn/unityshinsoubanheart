﻿using System;

// Token: 0x0200013D RID: 317
public class SaveFileInfo
{
	// Token: 0x170000FD RID: 253
	// (get) Token: 0x060008AD RID: 2221 RVA: 0x000267EC File Offset: 0x000249EC
	// (set) Token: 0x060008AE RID: 2222 RVA: 0x000267F4 File Offset: 0x000249F4
	public string HashCode
	{
		get
		{
			return this.m_HashCode;
		}
		set
		{
			this.m_HashCode = value;
		}
	}

	// Token: 0x170000FE RID: 254
	// (get) Token: 0x060008AF RID: 2223 RVA: 0x00026800 File Offset: 0x00024A00
	// (set) Token: 0x060008B0 RID: 2224 RVA: 0x00026808 File Offset: 0x00024A08
	public string TimeStamp
	{
		get
		{
			return this.m_TimeStamp;
		}
		set
		{
			this.m_TimeStamp = value;
		}
	}

	// Token: 0x170000FF RID: 255
	// (get) Token: 0x060008B1 RID: 2225 RVA: 0x00026814 File Offset: 0x00024A14
	// (set) Token: 0x060008B2 RID: 2226 RVA: 0x0002681C File Offset: 0x00024A1C
	public string LastMessage
	{
		get
		{
			return this.m_LastMessage;
		}
		set
		{
			this.m_LastMessage = value;
		}
	}

	// Token: 0x17000100 RID: 256
	// (get) Token: 0x060008B3 RID: 2227 RVA: 0x00026828 File Offset: 0x00024A28
	// (set) Token: 0x060008B4 RID: 2228 RVA: 0x00026830 File Offset: 0x00024A30
	public int PlayCharaId
	{
		get
		{
			return this.m_PlayCharaId;
		}
		set
		{
			this.m_PlayCharaId = value;
		}
	}

	// Token: 0x17000101 RID: 257
	// (get) Token: 0x060008B5 RID: 2229 RVA: 0x0002683C File Offset: 0x00024A3C
	// (set) Token: 0x060008B6 RID: 2230 RVA: 0x00026844 File Offset: 0x00024A44
	public string SceneName
	{
		get
		{
			return this.m_SceneName;
		}
		set
		{
			this.m_SceneName = value;
		}
	}

	// Token: 0x060008B7 RID: 2231 RVA: 0x00026850 File Offset: 0x00024A50
	public bool CheckHash(string hashCode)
	{
		return this.HashCode.CompareTo(hashCode) == 0;
	}

	// Token: 0x04000775 RID: 1909
	private string m_HashCode;

	// Token: 0x04000776 RID: 1910
	private string m_TimeStamp;

	// Token: 0x04000777 RID: 1911
	private string m_LastMessage;

	// Token: 0x04000778 RID: 1912
	private int m_PlayCharaId;

	// Token: 0x04000779 RID: 1913
	private string m_SceneName;
}
