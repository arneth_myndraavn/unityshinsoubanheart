﻿using System;
using UnityEngine;

// Token: 0x02000192 RID: 402
public class IImageAnimation
{
	// Token: 0x06000B8A RID: 2954 RVA: 0x00031064 File Offset: 0x0002F264
	public virtual void Update()
	{
	}

	// Token: 0x06000B8B RID: 2955 RVA: 0x00031068 File Offset: 0x0002F268
	public virtual bool Active()
	{
		return true;
	}

	// Token: 0x06000B8C RID: 2956 RVA: 0x0003106C File Offset: 0x0002F26C
	public virtual void Restart()
	{
	}

	// Token: 0x06000B8D RID: 2957 RVA: 0x00031070 File Offset: 0x0002F270
	public virtual Vector3 CalcPosition(Vector3 position)
	{
		return position;
	}

	// Token: 0x06000B8E RID: 2958 RVA: 0x00031074 File Offset: 0x0002F274
	public virtual Vector3 CalcScale(Vector3 scale)
	{
		return scale;
	}

	// Token: 0x06000B8F RID: 2959 RVA: 0x00031078 File Offset: 0x0002F278
	public virtual Color CalcColor(Color color)
	{
		return color;
	}
}
