﻿using System;
using System.Collections;
using UnityEngine;

// Token: 0x0200013F RID: 319
public class EffectManager : MonoBehaviour
{
	// Token: 0x17000102 RID: 258
	// (get) Token: 0x060008CD RID: 2253 RVA: 0x00027010 File Offset: 0x00025210
	private static EffectManager Instance
	{
		get
		{
			if (EffectManager.m_instance == null)
			{
				GameObject gameObject = GameObject.Find("_EffectManager");
				if (gameObject == null)
				{
					gameObject = new GameObject("_EffectManager");
				}
				EffectManager.m_instance = gameObject.AddComponent<EffectManager>();
			}
			return EffectManager.m_instance;
		}
	}

	// Token: 0x17000103 RID: 259
	// (get) Token: 0x060008CE RID: 2254 RVA: 0x00027060 File Offset: 0x00025260
	// (set) Token: 0x060008CF RID: 2255 RVA: 0x00027068 File Offset: 0x00025268
	public int PassedTime
	{
		get
		{
			return this.m_passedTime;
		}
		set
		{
			this.m_passedTime = value;
		}
	}

	// Token: 0x17000104 RID: 260
	// (get) Token: 0x060008D0 RID: 2256 RVA: 0x00027074 File Offset: 0x00025274
	// (set) Token: 0x060008D1 RID: 2257 RVA: 0x0002707C File Offset: 0x0002527C
	public string EffectName
	{
		get
		{
			return this.m_effectName;
		}
		set
		{
			this.m_effectName = value;
		}
	}

	// Token: 0x17000105 RID: 261
	// (get) Token: 0x060008D2 RID: 2258 RVA: 0x00027088 File Offset: 0x00025288
	// (set) Token: 0x060008D3 RID: 2259 RVA: 0x00027090 File Offset: 0x00025290
	public int TransSec
	{
		get
		{
			return this.m_transSec;
		}
		set
		{
			this.m_transSec = value;
		}
	}

	// Token: 0x17000106 RID: 262
	// (get) Token: 0x060008D4 RID: 2260 RVA: 0x0002709C File Offset: 0x0002529C
	// (set) Token: 0x060008D5 RID: 2261 RVA: 0x000270A4 File Offset: 0x000252A4
	public Renderer Target
	{
		get
		{
			return this.m_target;
		}
		set
		{
			this.m_target = value;
		}
	}

	// Token: 0x17000107 RID: 263
	// (get) Token: 0x060008D6 RID: 2262 RVA: 0x000270B0 File Offset: 0x000252B0
	// (set) Token: 0x060008D7 RID: 2263 RVA: 0x000270B8 File Offset: 0x000252B8
	public Shader EffectShader
	{
		get
		{
			return this.m_effectShader;
		}
		set
		{
			this.m_effectShader = value;
		}
	}

	// Token: 0x17000108 RID: 264
	// (get) Token: 0x060008D8 RID: 2264 RVA: 0x000270C4 File Offset: 0x000252C4
	// (set) Token: 0x060008D9 RID: 2265 RVA: 0x000270CC File Offset: 0x000252CC
	public Texture SrcTexture
	{
		get
		{
			return this.m_srcTexture;
		}
		set
		{
			this.m_srcTexture = value;
		}
	}

	// Token: 0x17000109 RID: 265
	// (get) Token: 0x060008DA RID: 2266 RVA: 0x000270D8 File Offset: 0x000252D8
	// (set) Token: 0x060008DB RID: 2267 RVA: 0x000270E0 File Offset: 0x000252E0
	public Texture DstTexture
	{
		get
		{
			return this.m_dstTexture;
		}
		set
		{
			this.m_dstTexture = value;
		}
	}

	// Token: 0x1700010A RID: 266
	// (get) Token: 0x060008DC RID: 2268 RVA: 0x000270EC File Offset: 0x000252EC
	// (set) Token: 0x060008DD RID: 2269 RVA: 0x000270F4 File Offset: 0x000252F4
	public Texture2D PatternTexture
	{
		get
		{
			return this.m_patternTexture;
		}
		set
		{
			this.m_patternTexture = value;
		}
	}

	// Token: 0x1700010B RID: 267
	// (get) Token: 0x060008DE RID: 2270 RVA: 0x00027100 File Offset: 0x00025300
	// (set) Token: 0x060008DF RID: 2271 RVA: 0x00027108 File Offset: 0x00025308
	private Material NewMaterial
	{
		get
		{
			return this.m_newMaterial;
		}
		set
		{
			this.m_newMaterial = value;
		}
	}

	// Token: 0x1700010C RID: 268
	// (get) Token: 0x060008E0 RID: 2272 RVA: 0x00027114 File Offset: 0x00025314
	// (set) Token: 0x060008E1 RID: 2273 RVA: 0x0002711C File Offset: 0x0002531C
	private Material OldMaterial
	{
		get
		{
			return this.m_oldMaterial;
		}
		set
		{
			this.m_oldMaterial = value;
		}
	}

	// Token: 0x060008E2 RID: 2274 RVA: 0x00027128 File Offset: 0x00025328
	public static void Init(string effectName, int transSec, Renderer renderer, Texture src, Texture dst)
	{
		switch (effectName)
		{
		case "wave":
			EffectManager.Instance.EffectShader = (Resources.Load("Shader/Effect/Wave") as Shader);
			goto IL_E4;
		case "ripple":
			EffectManager.Instance.EffectShader = (Resources.Load("Shader/Effect/Ripple") as Shader);
			goto IL_E4;
		case "mosaic":
			EffectManager.Instance.EffectShader = (Resources.Load("Shader/Effect/Mosaic") as Shader);
			goto IL_E4;
		}
		EffectManager.Instance.EffectShader = (Resources.Load("Shader/Effect/Transition") as Shader);
		IL_E4:
		EffectManager.Instance.PassedTime = 0;
		EffectManager.Instance.EffectName = effectName;
		EffectManager.Instance.TransSec = transSec;
		EffectManager.Instance.Target = renderer;
		EffectManager.Instance.SrcTexture = src;
		EffectManager.Instance.DstTexture = dst;
		EffectManager.Instance.NewMaterial = new Material(EffectManager.Instance.EffectShader);
	}

	// Token: 0x060008E3 RID: 2275 RVA: 0x00027278 File Offset: 0x00025478
	public static IEnumerator StartEffect()
	{
		EffectManager.Instance.OldMaterial = EffectManager.Instance.Target.sharedMaterial;
		EffectManager.Instance.Target.material = new Material(EffectManager.Instance.EffectShader);
		EffectManager.Instance.Target.material.shader = EffectManager.Instance.EffectShader;
		EffectManager.Instance.Target.sharedMaterial = EffectManager.Instance.Target.material;
		EffectManager.Instance.Target.sharedMaterial.SetTexture("_tex0", EffectManager.Instance.SrcTexture);
		EffectManager.Instance.Target.sharedMaterial.SetTexture("_tex1", EffectManager.Instance.DstTexture);
		EffectManager.Instance.Target.sharedMaterial.SetFloat("_time", 0f);
		string effectName = EffectManager.Instance.EffectName;
		switch (effectName)
		{
		case "wave":
			EffectManager.Instance.Target.sharedMaterial.SetFloat("_power", 0.05f);
			EffectManager.Instance.Target.sharedMaterial.SetFloat("_count", 4f);
			goto IL_2FB;
		case "ripple":
			EffectManager.Instance.Target.sharedMaterial.SetFloat("_pow", 0.5f);
			EffectManager.Instance.Target.sharedMaterial.SetFloat("_w", 2f);
			EffectManager.Instance.Target.sharedMaterial.SetFloat("_h", 5f);
			EffectManager.Instance.Target.sharedMaterial.SetFloat("_speed", 1.2f);
			goto IL_2FB;
		case "mosaic":
			EffectManager.Instance.Target.sharedMaterial.SetFloat("_X", (float)(Screen.width * 2));
			EffectManager.Instance.Target.sharedMaterial.SetFloat("_Y", (float)(Screen.height * 2));
			EffectManager.Instance.Target.sharedMaterial.SetFloat("_MX", 32f);
			EffectManager.Instance.Target.sharedMaterial.SetFloat("_MY", 18f);
			goto IL_2FB;
		}
		yield return EffectManager.Instance.StartCoroutine(EffectManager.loadTexture());
		EffectManager.Instance.Target.sharedMaterial.SetTexture("_tex2", EffectManager.Instance.PatternTexture);
		IL_2FB:
		while (EffectManager.Instance.PassedTime < EffectManager.Instance.TransSec)
		{
			EffectManager.Instance.Target.sharedMaterial.SetFloat("_time", (float)EffectManager.Instance.PassedTime / (float)EffectManager.Instance.TransSec);
			EffectManager.Instance.PassedTime += (int)(Time.deltaTime * 1000f);
			yield return 0;
		}
		EffectManager.Instance.Target.material = EffectManager.Instance.OldMaterial;
		EffectManager.Instance.Target.sharedMaterial = EffectManager.Instance.OldMaterial;
		EffectManager.Instance.Target.sharedMaterial.mainTexture = EffectManager.Instance.DstTexture;
		if (EffectManager.Instance.PatternTexture != null)
		{
			UnityEngine.Object.Destroy(EffectManager.Instance.PatternTexture);
		}
		EffectManager.Instance.PatternTexture = null;
		yield break;
	}

	// Token: 0x060008E4 RID: 2276 RVA: 0x0002728C File Offset: 0x0002548C
	private static IEnumerator loadTexture()
	{
		string path = Application.streamingAssetsPath  + Pathing.ToPlatformAssetBundleName(EffectManager.Instance.EffectName);
		byte[] data = System.IO.File.ReadAllBytes(path + ".png");
		Texture2D tex = new Texture2D(0, 0, TextureFormat.RGBA32, false, true);
		tex.LoadImage(data);
		tex.wrapMode = TextureWrapMode.Clamp;
		EffectManager.Instance.PatternTexture = tex;
		yield break;
	}

	// Token: 0x060008E5 RID: 2277 RVA: 0x000272A0 File Offset: 0x000254A0
	public static void FadeBlackOut(float sec)
	{
		ScreenEffect.FadeExec = true;
		EffectManager.Instance.StartCoroutine(ScreenEffect.FadeBlackOut(sec));
	}

	// Token: 0x060008E6 RID: 2278 RVA: 0x000272BC File Offset: 0x000254BC
	public static void FadeBlackIn(float sec)
	{
		ScreenEffect.FadeExec = true;
		EffectManager.Instance.StartCoroutine(ScreenEffect.FadeBlackIn(sec));
	}

	// Token: 0x060008E7 RID: 2279 RVA: 0x000272D8 File Offset: 0x000254D8
	public static bool FadeIsEnd()
	{
		bool fadeExec = ScreenEffect.FadeExec;
		if (!fadeExec)
		{
			ScreenEffect.Term();
		}
		return fadeExec;
	}

	// Token: 0x04000781 RID: 1921
	private static EffectManager m_instance;

	// Token: 0x04000782 RID: 1922
	private int m_passedTime;

	// Token: 0x04000783 RID: 1923
	private string m_effectName;

	// Token: 0x04000784 RID: 1924
	private int m_transSec;

	// Token: 0x04000785 RID: 1925
	private Renderer m_target;

	// Token: 0x04000786 RID: 1926
	private Shader m_effectShader;

	// Token: 0x04000787 RID: 1927
	private Texture m_srcTexture;

	// Token: 0x04000788 RID: 1928
	private Texture m_dstTexture;

	// Token: 0x04000789 RID: 1929
	private Texture2D m_patternTexture;

	// Token: 0x0400078A RID: 1930
	private Material m_newMaterial;

	// Token: 0x0400078B RID: 1931
	private Material m_oldMaterial;
}
