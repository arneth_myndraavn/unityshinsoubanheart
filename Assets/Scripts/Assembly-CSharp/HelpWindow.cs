﻿using System;
using System.Collections;
using System.Collections.Generic;
using Qoo;
using UnityEngine;

// Token: 0x02000113 RID: 275
public class HelpWindow : BaseWindow
{
	private Dictionary<string,int> dict = null;

	// Token: 0x06000746 RID: 1862 RVA: 0x0001F45C File Offset: 0x0001D65C
	protected sealed override string GetBGMName()
	{
		return "lovecheck2_ali";
	}

	// Token: 0x06000747 RID: 1863 RVA: 0x0001F464 File Offset: 0x0001D664
	protected sealed override void OnAwake()
	{
		this.moveBackGround = new HelpWindow.MoveBackGround();
		this.pageManager = new HelpWindow.PageManager(5);
	}

	// Token: 0x06000748 RID: 1864 RVA: 0x0001F480 File Offset: 0x0001D680
	protected sealed override string[] newSceneTextureNameArray()
	{
		return new string[]
		{
			"screen/title/help_01",
			"screen/title/help_02",
			"screen/title/help_03",
			"screen/title/help_04",
			"screen/title/help_05",
			"screen/common/cancel",
			"screen/cgmemory/cgm_arwl",
			"screen/cgmemory/cgm_arwr"
		};
	}

	// Token: 0x06000749 RID: 1865 RVA: 0x0001F4D8 File Offset: 0x0001D6D8
	protected sealed override void BeforeInit()
	{
	}

	// Token: 0x0600074A RID: 1866 RVA: 0x0001F4DC File Offset: 0x0001D6DC
	protected sealed override void AfterInit()
	{
		this.moveBackGround.Init(base.GetGameObject("Before"), base.GetGameObject("After"), base.GetGameObject("BackGround"));
		float delay = 0f;
		int repeat = 0;
		float speed = 0.8f;
		ImageObject imageObject = base.GetImageObject("ArrowL");
		imageObject.AddImageAnimation(new ImageAnimationLocalPosition(repeat, speed, delay, MoveType.LoopSin000to180, new Vector3(0f, 0f, 0f), new Vector3(-25f, 0f, 0f)));
		ImageObject imageObject2 = base.GetImageObject("ArrowR");
		imageObject2.AddImageAnimation(new ImageAnimationLocalPosition(repeat, speed, delay, MoveType.LoopSin000to180, new Vector3(0f, 0f, 0f), new Vector3(25f, 0f, 0f)));
	}

	// Token: 0x0600074B RID: 1867 RVA: 0x0001F5AC File Offset: 0x0001D7AC
	protected sealed override BaseWindow.UIComponent[] newComponentArray()
	{
		string currentFPath = this.pageManager.GetCurrentFPath();
		return new BaseWindow.UIComponent[]
		{
			new BaseWindow.UIImage("Before", 0, 0, this.wndz - 1, currentFPath, false, false),
			new BaseWindow.UIImage("After", 0, 0, this.wndz - 1, currentFPath, false, false),
			new BaseWindow.UIImage("BackGround", 0, 0, this.wndz, currentFPath, true, true),
			new BaseWindow.UIButton("Cancel", 887, 6, this.wndz + 3, "screen/common/cancel", true, true, 1, 2, 0),
			new BaseWindow.UIImage("ArrowL", 33, 80, this.wndz + 3, "screen/cgmemory/cgm_arwl", false, true),
			new BaseWindow.UIImage("ArrowR", 848, 80, this.wndz + 3, "screen/cgmemory/cgm_arwr", false, true)
		};
	}

	// Token: 0x0600074C RID: 1868 RVA: 0x0001F684 File Offset: 0x0001D884
	protected sealed override void OnBaseWindowOnButton(string obj)
	{
		if (obj != null)
		{
			if (this.dict == null)
			{
				this.dict = new Dictionary<string, int>(1)
				{
					{
						"Cancel",
						0
					}
				};
			}
			int num;
			if (this.dict.TryGetValue(obj, out num))
			{
				if (num == 0)
				{
					base.PlaySE_Cancel();
					SceneManager.BackScene();
				}
			}
		}
	}

	// Token: 0x0600074D RID: 1869 RVA: 0x0001F6EC File Offset: 0x0001D8EC
	protected override void OnBaseWindowUpdate()
	{
		if (this.moveBackGround.Moving)
		{
			if (!this.moveBackGround.Update())
			{
				base.SetTexture("BackGround", "screen/title/" + this.pageManager.GetCurrentFPath());
			}
			return;
		}
		INPUT_STATUS status = UnityApp.Input.Status;
		if (status == INPUT_STATUS.DRAG || status == INPUT_STATUS.FLICK)
		{
			this.OnFlick(UnityApp.Input.SlideDragVector);
		}
	}

	// Token: 0x0600074E RID: 1870 RVA: 0x0001F770 File Offset: 0x0001D970
	private void OnFlick(SLIDE_VECTOR slide)
	{
		string currentFPath = this.pageManager.GetCurrentFPath();
		if (slide != SLIDE_VECTOR.LEFT)
		{
			if (slide == SLIDE_VECTOR.RIGHT)
			{
				base.PlaySE_Select();
				this.pageManager.Left();
			}
		}
		else
		{
			base.PlaySE_Select();
			this.pageManager.Right();
		}
		string currentFPath2 = this.pageManager.GetCurrentFPath();
		if (currentFPath2 != currentFPath)
		{
			Qoo.Debug.Print(currentFPath2);
			this.ChangeBG(currentFPath, currentFPath2, slide);
		}
	}

	// Token: 0x0600074F RID: 1871 RVA: 0x0001F7F4 File Offset: 0x0001D9F4
	private void ChangeBG(string beforePath, string afterPath, SLIDE_VECTOR slide)
	{
		this.moveBackGround.ChangeBG(beforePath, afterPath, slide);
		if (this.moveBackGround.Update())
		{
			base.StartCoroutine(this.TransitionCoroutine(this.moveBackGround.param));
		}
	}

	// Token: 0x06000750 RID: 1872 RVA: 0x0001F838 File Offset: 0x0001DA38
	private IEnumerator TransitionCoroutine(HelpWindow.TransitionParam param)
	{
		EffectManager.Init(param.effectName, 500, param.renderer, param.before, param.after);
		yield return base.StartCoroutine(EffectManager.StartEffect());
		param.action();
		yield break;
	}

	// Token: 0x040006A8 RID: 1704
	private readonly int wndz;

	// Token: 0x040006A9 RID: 1705
	private HelpWindow.MoveBackGround moveBackGround;

	// Token: 0x040006AA RID: 1706
	private HelpWindow.PageManager pageManager;

	// Token: 0x02000114 RID: 276
	private class PageManager
	{
		// Token: 0x06000751 RID: 1873 RVA: 0x0001F864 File Offset: 0x0001DA64
		public PageManager(int pageMax_)
		{
			this.page = 0;
			this.pageMax = pageMax_;
		}

		// Token: 0x06000752 RID: 1874 RVA: 0x0001F87C File Offset: 0x0001DA7C
		public string GetCurrentFPath()
		{
			return this.getName(this.page);
		}

		// Token: 0x06000753 RID: 1875 RVA: 0x0001F88C File Offset: 0x0001DA8C
		public string LeftFPath()
		{
			return this.getName(this.getLeft());
		}

		// Token: 0x06000754 RID: 1876 RVA: 0x0001F89C File Offset: 0x0001DA9C
		public string RightFPath()
		{
			return this.getName(this.getRight());
		}

		// Token: 0x06000755 RID: 1877 RVA: 0x0001F8AC File Offset: 0x0001DAAC
		public void Left()
		{
			this.page = this.getLeft();
		}

		// Token: 0x06000756 RID: 1878 RVA: 0x0001F8BC File Offset: 0x0001DABC
		public void Right()
		{
			this.page = this.getRight();
		}

		// Token: 0x06000757 RID: 1879 RVA: 0x0001F8CC File Offset: 0x0001DACC
		private string getName(int page)
		{
			return (page + 1).ToString("screen/title/help_00");
		}

		// Token: 0x06000758 RID: 1880 RVA: 0x0001F8EC File Offset: 0x0001DAEC
		private int getLeft()
		{
			return (this.page != 0) ? (this.page - 1) : (this.pageMax - 1);
		}

		// Token: 0x06000759 RID: 1881 RVA: 0x0001F91C File Offset: 0x0001DB1C
		private int getRight()
		{
			return (this.page != this.pageMax - 1) ? (this.page + 1) : 0;
		}

		// Token: 0x040006AC RID: 1708
		private int page;

		// Token: 0x040006AD RID: 1709
		private int pageMax;
	}

	// Token: 0x02000115 RID: 277
	private class MoveImage
	{
		// Token: 0x0600075A RID: 1882 RVA: 0x0001F940 File Offset: 0x0001DB40
		public MoveImage()
		{
			this.go = null;
		}

		// Token: 0x0600075B RID: 1883 RVA: 0x0001F950 File Offset: 0x0001DB50
		public void Init(GameObject go_)
		{
			this.go = go_;
		}

		// Token: 0x0600075C RID: 1884 RVA: 0x0001F95C File Offset: 0x0001DB5C
		public void SetTexture(string fpath)
		{
			ImageObject component = this.go.GetComponent<ImageObject>();
			if (component == null)
			{
				return;
			}
			component.SetTexture("screen/title/" + fpath);
		}

		// Token: 0x0600075D RID: 1885 RVA: 0x0001F994 File Offset: 0x0001DB94
		public Texture Texture()
		{
			ImageObject component = this.go.GetComponent<ImageObject>();
			if (component == null)
			{
				return null;
			}
			return component.Texture;
		}

		// Token: 0x040006AE RID: 1710
		private GameObject go;
	}

	// Token: 0x02000116 RID: 278
	private class TransitionParam
	{
		// Token: 0x0600075E RID: 1886 RVA: 0x0001F9C4 File Offset: 0x0001DBC4
		public TransitionParam(string effectName_ = null, Texture before_ = null, Texture after_ = null, Renderer renderer_ = null, Action action_ = null)
		{
			this.effectName = effectName_;
			this.before = before_;
			this.after = after_;
			this.renderer = renderer_;
			this.action = action_;
		}

		// Token: 0x040006AF RID: 1711
		public string effectName;

		// Token: 0x040006B0 RID: 1712
		public Texture before;

		// Token: 0x040006B1 RID: 1713
		public Texture after;

		// Token: 0x040006B2 RID: 1714
		public Renderer renderer;

		// Token: 0x040006B3 RID: 1715
		public Action action;
	}

	// Token: 0x02000117 RID: 279
	private class MoveBackGround
	{
		// Token: 0x0600075F RID: 1887 RVA: 0x0001F9F4 File Offset: 0x0001DBF4
		public MoveBackGround()
		{
			this.param = null;
			this.before = new HelpWindow.MoveImage();
			this.after = new HelpWindow.MoveImage();
			this.moving = false;
		}

		// Token: 0x170000DE RID: 222
		// (get) Token: 0x06000760 RID: 1888 RVA: 0x0001FA2C File Offset: 0x0001DC2C
		public bool Moving
		{
			get
			{
				return this.moving;
			}
		}

		// Token: 0x06000761 RID: 1889 RVA: 0x0001FA34 File Offset: 0x0001DC34
		public void Init(GameObject before_, GameObject after_, GameObject bg_)
		{
			this.before.Init(before_);
			this.after.Init(after_);
			ImageObject component = bg_.GetComponent<ImageObject>();
			if (component == null)
			{
				return;
			}
			this.renderer = component.GetComponent<Renderer>();
		}

		// Token: 0x06000762 RID: 1890 RVA: 0x0001FA7C File Offset: 0x0001DC7C
		public void ChangeBG(string beforePath, string afterPath, SLIDE_VECTOR slide)
		{
			string text = this.calcVector(slide);
			this.before.SetTexture(beforePath);
			this.after.SetTexture(afterPath);
			if (text != null)
			{
				this.moving = true;
				this.param = new HelpWindow.TransitionParam(text, this.before.Texture(), this.after.Texture(), this.renderer, new Action(this.SetTransitionFinish));
			}
		}

		// Token: 0x06000763 RID: 1891 RVA: 0x0001FAEC File Offset: 0x0001DCEC
		public bool Update()
		{
			return this.moving;
		}

		// Token: 0x06000764 RID: 1892 RVA: 0x0001FAF4 File Offset: 0x0001DCF4
		private string calcVector(SLIDE_VECTOR slide)
		{
			switch (slide)
			{
			case SLIDE_VECTOR.LEFT:
				return "/bytes/effect/grad_r";
			case SLIDE_VECTOR.RIGHT:
				return "/bytes/effect/grad_l";
			case SLIDE_VECTOR.UP:
				return "/bytes/effect/grad_d";
			case SLIDE_VECTOR.DOWN:
				return "/bytes/effect/grad_u";
			default:
				return null;
			}
		}

		// Token: 0x06000765 RID: 1893 RVA: 0x0001FB3C File Offset: 0x0001DD3C
		private void SetTransitionFinish()
		{
			this.moving = false;
		}

		// Token: 0x040006B4 RID: 1716
		public HelpWindow.TransitionParam param;

		// Token: 0x040006B5 RID: 1717
		private HelpWindow.MoveImage before;

		// Token: 0x040006B6 RID: 1718
		private HelpWindow.MoveImage after;

		// Token: 0x040006B7 RID: 1719
		private Renderer renderer;

		// Token: 0x040006B8 RID: 1720
		private bool moving;
	}
}
