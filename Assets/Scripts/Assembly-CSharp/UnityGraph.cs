﻿using System;
using System.Collections;
using Qoo;
using Qoo.Application;
using Qoo.File;
using UnityEngine;
using UnityEngine.Video;

// Token: 0x02000157 RID: 343
public class UnityGraph : Singleton<UnityGraph>
{
	// Token: 0x1700012C RID: 300
	// (get) Token: 0x060009AE RID: 2478 RVA: 0x0002AD04 File Offset: 0x00028F04
	public UIFont Font
	{
		get
		{
			return this.font;
		}
	}

	// Token: 0x1700012D RID: 301
	// (get) Token: 0x060009AF RID: 2479 RVA: 0x0002AD0C File Offset: 0x00028F0C
	// (set) Token: 0x060009B0 RID: 2480 RVA: 0x0002AD14 File Offset: 0x00028F14
	public bool RunEffect
	{
		get
		{
			return this.m_isRunEffect;
		}
		set
		{
			this.m_isRunEffect = value;
		}
	}

	// Token: 0x1700012E RID: 302
	// (get) Token: 0x060009B1 RID: 2481 RVA: 0x0002AD20 File Offset: 0x00028F20
	public bool IsFade
	{
		get
		{
			return this.FadeParam < 255;
		}
	}

	// Token: 0x060009B2 RID: 2482 RVA: 0x0002AD30 File Offset: 0x00028F30
	private void Awake()
	{

		this.sprite_obj = new GameObject("2D Sprite Manager");
		this.sprite_obj.transform.parent = base.gameObject.transform;
		this.m_ManSprite = this.sprite_obj.AddComponent<Man2D>();
		Camera camera = base.gameObject.GetComponent<Camera>();
		if (camera == null)
		{
			camera = base.gameObject.AddComponent<Camera>();
		}
		camera.enabled = false;
		this.m_FrameDraw = new UnitySprite(false, base.gameObject, false);
		this.m_FrameDraw.obj.name = "FrameBuffer Sprite";
		this.m_FrameDraw.z = 100;
		//this.m_FrameDraw.Brend = SPRITE_DRAW_MODE.MUL_NOTEXALPHA;
		this.m_FrameDraw.Update(this.m_width, this.m_height, 0f);
		this.FadeParam = 0;
		this.m_FrameBg = new UnitySprite(false, base.gameObject, false);
		this.m_FrameBg.obj.name = "FrameBufferBG";
		this.m_FrameBg.w = Screen.width;
		this.m_FrameBg.h = Screen.height;
		this.m_FrameBg.R = (this.m_FrameBg.G = (this.m_FrameBg.B = 0));
		this.m_FrameBg.A = byte.MaxValue;
		this.m_FrameBg.UW = Screen.width;
		this.m_FrameBg.VH = Screen.height;
		this.m_FrameBg.z = 0;
		this.m_FrameBg.Brend = SPRITE_DRAW_MODE.BACK;
		this.FadeParam = 0;
		this.Disable(false);
	}

	// Token: 0x060009B3 RID: 2483 RVA: 0x0002AECC File Offset: 0x000290CC
	public void Enable(bool IsBlack = false)
	{
		this.RunEffect = false;
		this.m_ManSprite.enabled = true;
		CreateSprite.CreateSpriteCamera(base.gameObject, Screen.height, false);
		base.gameObject.GetComponent<Camera>().aspect = (float)Screen.width / (float)Screen.height;
		//TODO fix camera issues, find out why this is needed
		//base.gameObject.camera.enabled = true;
		base.gameObject.GetComponent<Camera>().enabled = false;
		this.m_FrameBg.obj.SetActive(true);
		if (this.m_FrameBg.tex == null)
		{
			this.m_FrameBg.SetImage(this.m_ManSprite.LoadTexture("jimon.png"));
		}
		UnitySprite frameBg = this.m_FrameBg;
		int num = 0;
		this.m_FrameBg.y = num;
		frameBg.x = num;
		this.m_FrameBg.R = (this.m_FrameBg.G = (this.m_FrameBg.B = 0));
		this.m_FrameBg.A = byte.MaxValue;
		this.m_FrameDraw.obj.SetActive(true);
		UnitySprite frameDraw = this.m_FrameDraw;
		num = 0;
		this.m_FrameDraw.y = num;
		frameDraw.x = num;
		if (!IsBlack)
		{
			this.m_FrameDraw.SetRenderImage(this.m_ManSprite.GetFrameBuffer(), 960, 544);
		}
		this.m_FrameDraw.CalcRenderImageOffset(this.m_width, this.m_height);
		//this.m_FrameDraw.Brend = SPRITE_DRAW_MODE.MUL_NOTEXALPHA;
		this.m_FrameDraw.Update(this.m_width, this.m_height, 0f);
		UnityApp.Input.SetScreenRect(this.m_FrameDraw.x, this.m_FrameDraw.y, Screen.width, Screen.height, 960, 544);
		base.enabled = true;
	}

	// Token: 0x060009B4 RID: 2484 RVA: 0x0002B08C File Offset: 0x0002928C
	public void Disable(bool IsClean = false)
	{
		base.enabled = false;
		base.gameObject.GetComponent<Camera>().enabled = false;
		this.m_ManSprite.enabled = false;
		this.m_FrameDraw.obj.SetActive(false);
		this.m_FrameBg.obj.SetActive(false);
		if (IsClean)
		{
			this.m_FrameDraw.SetRenderImage(null, 960, 544);
		}
		UnityApp.Input.ResetScreenRect();
	}

	// Token: 0x060009B5 RID: 2485 RVA: 0x0002B108 File Offset: 0x00029308
	public void Restore()
	{
		if (base.enabled)
		{
			this.m_ManSprite.CreateFrameBuffer();
		}
	}

	// Token: 0x060009B6 RID: 2486 RVA: 0x0002B120 File Offset: 0x00029320
	public IEnumerator RestoreEnable(Texture2D tex)
	{
		this.m_ManSprite.CreateFrameBuffer();
		this.m_FrameDraw.SetRenderImage(tex, 960, 544);
		this.Enable(true);
		this.m_isRunEffect = true;
		yield return 0;
		this.m_ManSprite.UpdateFrameBuffer();
		yield return 0;
		this.m_ManSprite.UpdateFrameBuffer();
		this.m_isRunEffect = false;
		yield break;
	}

	// Token: 0x060009B7 RID: 2487 RVA: 0x0002B14C File Offset: 0x0002934C
	public Texture2D CopyFrameBuffer()
	{
		RenderTexture frameBuffer = Singleton<Man2D>.Instance.GetFrameBuffer();
		Texture2D texture2D = new Texture2D(frameBuffer.width, frameBuffer.height, TextureFormat.ARGB32, false, false);
		RenderTexture.active = frameBuffer;
		texture2D.ReadPixels(new Rect(0f, 0f, (float)frameBuffer.width, (float)frameBuffer.height), 0, 0);
		texture2D.Apply();
		RenderTexture.active = null;
		return texture2D;
	}

	// Token: 0x060009B8 RID: 2488 RVA: 0x0002B1B4 File Offset: 0x000293B4
	public bool CreateFont(string name_, byte[] data_, UnityTexture tex_)
	{
		BMFont bmfont = new BMFont();
		BMFontReader.Load(bmfont, name_, data_);
		this.font = base.gameObject.AddComponent<UIFont>();
		this.font.Font = bmfont;
		this.font.texture = tex_;
		Qoo.Debug.Print("CreateFont=" + name_);
		return true;
	}

	// Token: 0x060009B9 RID: 2489 RVA: 0x0002B20C File Offset: 0x0002940C
	private void Update()
	{
		if (!this.m_isRunEffect)
		{
			UnitySprite frameBg = this.m_FrameBg;
			int num = 0;
			this.m_FrameBg.y = num;
			frameBg.x = num;
			this.m_FrameBg.Update(Screen.width, Screen.height, 10f);
			this.m_ManSprite.UpdateFrameBuffer();
			RenderTexture frameBuffer = this.m_ManSprite.GetFrameBuffer();
			this.m_FrameDraw.SetRenderImage(frameBuffer, 960, 544);
			UnitySprite frameDraw = this.m_FrameDraw;
			num = 0;
			this.m_FrameDraw.y = num;
			frameDraw.x = num;
			if (frameBuffer != null)
			{
				this.m_FrameDraw.A = (byte)this.FadeParam;
			}
			else
			{
				this.m_FrameDraw.A = 0;
			}
			this.m_FrameDraw.CalcRenderImageOffset(this.m_width, this.m_height);
			this.m_FrameDraw.Update(this.m_width, this.m_height, 0f);
		}
	}

	// Token: 0x060009BA RID: 2490 RVA: 0x0002B300 File Offset: 0x00029500
	private IEnumerator Fade_In(int time_ = 1000)
	{
		if (this.FadeParam < 255)
		{
			return this.Fade(time_, 255, this.FadeParam);
		}
		return null;
	}

	// Token: 0x060009BB RID: 2491 RVA: 0x0002B334 File Offset: 0x00029534
	private IEnumerator Fade_Out(int time_ = 1000)
	{
		if (this.FadeParam > 0)
		{
			return this.Fade(time_, 0, this.FadeParam);
		}
		return null;
	}

	// Token: 0x060009BC RID: 2492 RVA: 0x0002B354 File Offset: 0x00029554
	public void ResetFade(int param = 255)
	{
		this.FadeParam = param;
	}

	// Token: 0x060009BD RID: 2493 RVA: 0x0002B360 File Offset: 0x00029560
	private IEnumerator Fade(int time, int dest, int src)
	{
		int start = Singleton<UnityApp>.Instance.GetTimeMilli();
		int now = Singleton<UnityApp>.Instance.GetTimeMilli();
		while (now < start + time)
		{
			now = Singleton<UnityApp>.Instance.GetTimeMilli();
			float param = (float)(now - start) / (float)time;
			if (param > 1f)
			{
				param = 1f;
			}
			if (param < 0f)
			{
				param = 0f;
			}
			param = (float)(dest - src) * param + (float)src;
			if (param > 255f)
			{
				param = 255f;
			}
			if (param < 0f)
			{
				param = 0f;
			}
			this.FadeParam = (int)((byte)param);
			yield return 0;
		}
		this.FadeParam = (int)((byte)dest);
		yield return 0;
		yield return 0;
		yield return 0;
		yield break;
	}

	// Token: 0x060009BE RID: 2494 RVA: 0x0002B3A8 File Offset: 0x000295A8
	public void SetEffect(string name, float time)
	{
		this.m_isRunEffect = true;
		UnityTask.Effect(name, time);
	}

	// Token: 0x060009BF RID: 2495 RVA: 0x0002B3BC File Offset: 0x000295BC
	public IEnumerator Effect(string name, float time = 1f)
	{
		//TODO effect movies crash game and standard effects need shaders. Fix and re-add them

		string text = name;
		switch (text) {
		case "FadeIn":
			this.m_isRunEffect = false;
			return this.Fade_In ((int)(time * 1000f));
		case "FadeOut":
			this.m_isRunEffect = false;
			return this.Fade_Out ((int)(time * 1000f));
		case "wave":
			return this.EffectMain (name, time);
		case "ripple":
			return this.EffectMain (name, time);
		case "mosaic":
			return this.EffectMain (name, time);
		}

		if (name == null || name.Length <= 0)
		{
			return this.CrossFade(time);
		}
		if (Singleton<UnityFileLoader>.Instance.GetPackNo(name) == 0)
		{
			string imageDir = Singleton<UnityFileLoader>.Instance.GetImageDir(name);
			name = imageDir + name;
			return this.EffectMain(name, time);
		}
		//return this.CrossFade(time);
		NMB_FILEINFO fileInfo = Nmb.GetFileInfo(name);
		if (fileInfo == null)
		{
			name = "ogv/effect/" + name + ".ogv";
		}
		else
		{
			name = fileInfo.DirName + fileInfo.FileName;
		}
		return this.EffectMovie(name);
	}

	// Token: 0x060009C0 RID: 2496 RVA: 0x0002B524 File Offset: 0x00029724
	protected IEnumerator EffectMain(string name, float time)
	{
		yield return 0;
		yield return 0;
		this.m_ManSprite.UpdateFrameBuffer();
		this.m_ManSprite.enabled = false;
		EffectManager.Init(name, (int)(time * 1000f), this.m_FrameDraw.obj.GetComponent<Renderer>(), this.m_ManSprite.GetFrameBufferOld(), this.m_ManSprite.GetFrameBuffer());
		yield return base.StartCoroutine(EffectManager.StartEffect());
		this.m_isRunEffect = false;
		this.m_ManSprite.enabled = true;
		yield return 0;
		yield break;
	}

	// Token: 0x060009C1 RID: 2497 RVA: 0x0002B55C File Offset: 0x0002975C
	private IEnumerator EffectMovie(string name)
	{
		yield return 0;
		yield return 0;
		this.m_ManSprite.UpdateFrameBuffer();
		this.m_ManSprite.enabled = false;
		//MovieTex movie = base.gameObject.AddComponent<MovieTex>();
		this.m_FrameDraw.SetRenderImage(this.m_ManSprite.GetFrameBuffer(), 960, 544);
		UnitySprite movieFrame = new UnitySprite(false, base.gameObject, false);
		movieFrame.SetName("Movie Frame");
		this.m_FrameDraw.A = (movieFrame.R = (movieFrame.G = (movieFrame.B = (movieFrame.A = byte.MaxValue))));
		

		//NewCode
		string path = name;
		if (path.IndexOf('/') == 0)
		{
			path = path.Substring(1);
		}
		string pathExt = System.IO.Path.GetExtension(path);
		path = path.Substring(0, path.Length - pathExt.Length);

		VideoPlayer m_tex = movieFrame.obj.transform.gameObject.AddComponent<VideoPlayer>();
		m_tex.playOnAwake = false;
		Material newMat = new Material(Shader.Find("QO/Sprite Add") as Shader);
		m_tex.url = Application.streamingAssetsPath + "/" + path + ".webm";
		m_tex.isLooping = false;
		movieFrame.SetMaterial(newMat, 960, 544);
		m_tex.renderMode = UnityEngine.Video.VideoRenderMode.MaterialOverride;


		//OLD Code
		//movieFrame.SetMaterial(movie.Play(name, MOVIE_TEX_TYPE.ADD, false), 960, 544);
		//yield return 0;
		//while (movie.IsPlay)
		m_tex.Play();
		yield return 0;
		yield return 0;
		yield return 0;
		while (m_tex.isPlaying || !m_tex.isPrepared)
		{
			if (Input.GetMouseButtonDown(0))
			{
				break;
			}
			UnityEngine.Debug.LogWarning(m_tex.isPlaying + ", " + !m_tex.isPrepared);
			this.m_FrameDraw.CalcRenderImageOffset(this.m_width, this.m_height);
			movieFrame.CalcRenderImageOffset(this.m_width, this.m_height);
			this.m_FrameDraw.Update(this.m_width, this.m_height, 10f);
			movieFrame.Update(this.m_width, this.m_height, 0f);
			yield return 0;
			yield return 0;
			yield return 0;
		}
		//UnityEngine.Object.Destroy(movie);
		movieFrame.obj.GetComponent<Renderer>().material = null;
		movieFrame.tex = null;
		this.m_ManSprite.RemoveSprite(movieFrame);
		movieFrame = null;
		this.m_isRunEffect = false;
		this.m_ManSprite.enabled = true;
		yield return 0;
		yield break;
	}

	// Token: 0x060009C2 RID: 2498 RVA: 0x0002B588 File Offset: 0x00029788
	private IEnumerator CrossFade(float time)
	{
		int ms = (int)(time * 1000f);
		this.m_isRunEffect = true;
		yield return 0;
		yield return 0;
		this.m_ManSprite.UpdateFrameBuffer();
		this.m_ManSprite.enabled = false;
		UnitySprite BaseFrame = new UnitySprite(false, base.gameObject, false);
		BaseFrame.SetName("CrossFade Frame");
		BaseFrame.Brend = SPRITE_DRAW_MODE.MUL;
		BaseFrame.Update(Screen.width, Screen.height, 0f);
		//this.m_FrameDraw.Brend = SPRITE_DRAW_MODE.MUL_NOTEXALPHA;
		this.m_FrameDraw.Update(this.m_width, this.m_height, 10f);
		BaseFrame.SetRenderImage(this.m_ManSprite.GetFrameBufferOld(), 960, 544);
		this.m_FrameDraw.SetRenderImage(this.m_ManSprite.GetFrameBuffer(), 960, 544);
		BaseFrame.R = (this.m_FrameBg.G = (this.m_FrameBg.B = byte.MaxValue));
		this.m_FrameBg.A = 0;
		this.m_FrameDraw.A = byte.MaxValue;
		int start = Singleton<UnityApp>.Instance.GetTimeMilli();
		int now = Singleton<UnityApp>.Instance.GetTimeMilli();
		while (now < start + ms)
		{
			now = Singleton<UnityApp>.Instance.GetTimeMilli();
			float param = (float)(now - start) / (float)ms;
			if (param > 1f)
			{
				param = 1f;
			}
			if (param < 0f)
			{
				param = 0f;
			}
			BaseFrame.A = (byte)(255f * (1f - param));
			this.m_FrameDraw.CalcRenderImageOffset(this.m_width, this.m_height);
			BaseFrame.CalcRenderImageOffset(this.m_width, this.m_height);
			this.m_FrameDraw.Update(this.m_width, this.m_height, 10f);
			BaseFrame.Update(this.m_width, this.m_height, 0f);
			yield return 0;
		}
		BaseFrame.tex = null;
		this.m_ManSprite.RemoveSprite(BaseFrame);
		BaseFrame = null;
		this.m_FrameDraw.A = byte.MaxValue;
		this.m_ManSprite.enabled = true;
		this.m_isRunEffect = false;
		yield return 0;
		yield break;
	}

	// Token: 0x060009C3 RID: 2499 RVA: 0x0002B5B4 File Offset: 0x000297B4
	public IEnumerator Flash(float time, Color32 rgb)
	{
		this.m_isRunEffect = false;
		foreach (UnitySprite item in base.QooSprite.SpriteManager.SpriteList)
		{
			if ((item.Effect & 2) != 0)
			{
				item.Brend = SPRITE_DRAW_MODE.TEX_ALPHA;
			}
		}
		int length = (int)(time * 1000f);
		int start = Singleton<UnityApp>.Instance.GetTimeMilli();
		int now = Singleton<UnityApp>.Instance.GetTimeMilli();
		while (now < start + length)
		{
			now = Singleton<UnityApp>.Instance.GetTimeMilli();
			float param = (float)(now - start) / (float)length;
			if (param > 1f)
			{
				param = 1f;
			}
			rgb.a = (byte)(255f * param);
			foreach (UnitySprite item2 in base.QooSprite.SpriteManager.SpriteList)
			{
				if ((item2.Effect & 2) != 0)
				{
					item2.FxColor = rgb;
				}
			}
			yield return 0;
		}
		base.QooSprite.SpriteManager.ResetFx(2);
		yield return 0;
		yield break;
	}

	// Token: 0x060009C4 RID: 2500 RVA: 0x0002B5EC File Offset: 0x000297EC
	public IEnumerator Shake(float time, int w, int h)
	{
		this.m_isRunEffect = false;
		int length = (int)(time * 1000f);
		int start = Singleton<UnityApp>.Instance.GetTimeMilli();
		int now = Singleton<UnityApp>.Instance.GetTimeMilli();
		while (now < start + length)
		{
			now = Singleton<UnityApp>.Instance.GetTimeMilli();
			int iAddX = App.GetRandom(w);
			int iAddY = App.GetRandom(h);
			foreach (UnitySprite item in base.QooSprite.SpriteManager.SpriteList)
			{
				if ((item.Effect & 1) != 0)
				{
					item.SetFxPos(iAddX, iAddY);
				}
			}
			yield return 0;
		}
		base.QooSprite.SpriteManager.ResetFx(1);
		yield return 0;
		yield break;
	}

	// Token: 0x040007E3 RID: 2019
	public Man2D m_ManSprite;

	// Token: 0x040007E4 RID: 2020
	private GameObject sprite_obj;

	// Token: 0x040007E5 RID: 2021
	private UnitySprite m_FrameBg;

	// Token: 0x040007E6 RID: 2022
	private UnitySprite m_FrameDraw;

	// Token: 0x040007E7 RID: 2023
	private UIFont font;

	// Token: 0x040007E8 RID: 2024
	private int FadeParam;

	// Token: 0x040007E9 RID: 2025
	private bool m_isRunEffect;

	private int m_width = 960;
	private int m_height = 544;
}
