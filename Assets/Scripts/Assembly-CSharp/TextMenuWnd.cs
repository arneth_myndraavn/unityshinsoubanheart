﻿using System;
using System.Collections.Generic;
using Qoo;
using UnityEngine;

// Token: 0x0200000E RID: 14
public class TextMenuWnd : UserBaseClass
{
	// Token: 0x1700000F RID: 15
	// (get) Token: 0x06000042 RID: 66 RVA: 0x00002848 File Offset: 0x00000A48
	// (set) Token: 0x06000043 RID: 67 RVA: 0x00002850 File Offset: 0x00000A50
	public bool IsSelect { get; private set; }

	// Token: 0x17000010 RID: 16
	// (get) Token: 0x06000044 RID: 68 RVA: 0x0000285C File Offset: 0x00000A5C
	// (set) Token: 0x06000045 RID: 69 RVA: 0x00002864 File Offset: 0x00000A64
	public int SelectID { get; private set; }

	// Token: 0x06000046 RID: 70 RVA: 0x00002870 File Offset: 0x00000A70
	private void Awake()
	{
		base.enabled = false;
	}

	// Token: 0x06000047 RID: 71 RVA: 0x0000287C File Offset: 0x00000A7C
	public void Release()
	{
		Qoo.Debug.Print("TextMenuWnd:Destroy:" + Singleton<Man2D>.IsReady);
		if (Singleton<Man2D>.IsReady)
		{
			foreach (UnityTextSprite sp in this.m_TxtList)
			{
				Singleton<Man2D>.Instance.RemoveSprite(sp);
			}
			this.m_TxtList.Clear();
			foreach (UnitySprite sp2 in this.m_BaseList)
			{
				Singleton<Man2D>.Instance.RemoveSprite(sp2);
			}
			this.m_BaseList.Clear();
		}
	}

	// Token: 0x06000048 RID: 72 RVA: 0x0000297C File Offset: 0x00000B7C
	private void OnDestroy()
	{
		this.Release();
	}

	// Token: 0x06000049 RID: 73 RVA: 0x00002984 File Offset: 0x00000B84
	public bool Init(Color32 ColTxt, Color32 ColBack, int height, int line_num)
	{
		this.m_nScrollPos = 0;
		this.m_RootItem.Create(-1, null, string.Empty);
		this.m_CurrentItem = null;
		this.m_colTxt = ColTxt;
		this.m_colBase = ColBack;
		this.m_nItemHeight = height;
		this.m_nLineNum = line_num;
		int num = 544 / line_num;
		for (int num2 = 0; num2 != this.m_nLineNum; num2++)
		{
			this.m_TxtList.Add(Man2D.TextSprite(null));
			this.m_BaseList.Add(Man2D.Sprite(string.Empty));
			this.m_BaseList[num2].SetPos(16, 0 + num * num2, 8000);
			this.m_BaseList[num2].SetSize(928, this.m_nItemHeight - 1);
			this.m_BaseList[num2].SetColor(this.m_colBase);
			this.m_BaseList[num2].Show = false;
			this.m_BaseList[num2].SetName("Debug:Base:" + num2);
			this.m_TxtList[num2].SetPos(16, 0 + num * num2, 8001);
			this.m_TxtList[num2].SetTextColor(this.m_colTxt);
			this.m_TxtList[num2].Show = false;
			this.m_TxtList[num2].SetName("Debug:Txt:" + num2);
		}
		return true;
	}

	// Token: 0x0600004A RID: 74 RVA: 0x00002B04 File Offset: 0x00000D04
	public bool AddMenu(int id_parent, int id, string name)
	{
		TextItem textItem = new TextItem();
		TextItem parent = this.GetParent(id_parent);
		if (parent != null)
		{
			textItem.Create(id, parent, name);
			return true;
		}
		return false;
	}

	// Token: 0x0600004B RID: 75 RVA: 0x00002B34 File Offset: 0x00000D34
	public bool Begin()
	{
		if (this.m_CurrentItem == null)
		{
			this.SetCurrent(this.m_RootItem);
		}
		this.IsSelect = false;
		this.SelectID = -1;
		this.m_nOldDelta = 0;
		base.enabled = true;
		return true;
	}

	// Token: 0x0600004C RID: 76 RVA: 0x00002B78 File Offset: 0x00000D78
	public bool End()
	{
		base.enabled = false;
		return true;
	}

	// Token: 0x0600004D RID: 77 RVA: 0x00002B84 File Offset: 0x00000D84
	private void Update()
	{
		INPUT_STATUS status = UnityApp.Input.Status;
		if (status != INPUT_STATUS.CLICK)
		{
			if (status == INPUT_STATUS.DRAG_ON)
			{
				if (this.m_nOldDelta != UnityApp.Input.Delta.y)
				{
					int num = -(this.m_nOldDelta - UnityApp.Input.Delta.y);
					num /= this.m_nItemHeight;
					if (num != 0)
					{
						this.Scroll(this.m_nScrollPos + num);
					}
					this.m_nOldDelta = UnityApp.Input.Delta.y;
				}
			}
		}
		else
		{
			Point2 end = UnityApp.Input.End;
			bool flag = false;
			for (int num2 = 0; num2 != this.m_nLineNum; num2++)
			{
				if (this.m_BaseList[num2].Show && this.m_BaseList[num2].IsHit(end))
				{
					this.SetSelect(this.m_nScrollPos + num2);
					flag = true;
				}
			}
			if (!flag)
			{
				this.Back();
			}
		}
	}

	// Token: 0x0600004E RID: 78 RVA: 0x00002C90 File Offset: 0x00000E90
	private void SetSelect(int nItemPos)
	{
		if (nItemPos < 0 || this.m_CurrentItem.Child.Count <= nItemPos)
		{
			return;
		}
		if (this.m_CurrentItem.Child[nItemPos].Child.Count > 0)
		{
			this.SetCurrent(this.m_CurrentItem.Child[nItemPos]);
		}
		else
		{
			this.IsSelect = true;
			this.SelectID = this.m_CurrentItem.Child[nItemPos].ID;
			this.End();
		}
	}

	// Token: 0x0600004F RID: 79 RVA: 0x00002D24 File Offset: 0x00000F24
	private void SetCurrent(TextItem curItem)
	{
		this.Clear();
		this.m_CurrentItem = curItem;
		this.Scroll(0);
	}

	// Token: 0x06000050 RID: 80 RVA: 0x00002D3C File Offset: 0x00000F3C
	private void Back()
	{
		if (this.m_CurrentItem.Parent != null)
		{
			this.SetCurrent(this.m_CurrentItem.Parent);
		}
	}

	// Token: 0x06000051 RID: 81 RVA: 0x00002D60 File Offset: 0x00000F60
	private void Scroll(int iPos)
	{
		if (iPos < 0)
		{
			iPos = 0;
		}
		else if (this.m_CurrentItem.Child.Count < this.m_nLineNum)
		{
			iPos = 0;
		}
		else if (this.m_CurrentItem.Child.Count - this.m_nLineNum < iPos)
		{
			iPos = this.m_CurrentItem.Child.Count - this.m_nLineNum;
		}
		this.m_nScrollPos = iPos;
		int num = this.m_nScrollPos;
		while (num < this.m_CurrentItem.Child.Count && num < this.m_nScrollPos + this.m_nLineNum)
		{
			int index = num - this.m_nScrollPos;
			this.m_TxtList[index].ClearText();
			this.m_TxtList[index].AddText(this.m_CurrentItem.Child[num].Name, this.m_nItemHeight - 2);
			this.m_TxtList[index].Show = true;
			this.m_BaseList[index].Show = true;
			num++;
		}
	}

	// Token: 0x06000052 RID: 82 RVA: 0x00002E84 File Offset: 0x00001084
	private void Clear()
	{
		foreach (UnityTextSprite unityTextSprite in this.m_TxtList)
		{
			unityTextSprite.Show = false;
		}
		foreach (UnitySprite unitySprite in this.m_BaseList)
		{
			unitySprite.Show = false;
		}
		this.m_nScrollPos = 0;
		this.m_CurrentItem = null;
	}

	// Token: 0x06000053 RID: 83 RVA: 0x00002F50 File Offset: 0x00001150
	private TextItem GetParent(int id)
	{
		if (id == -1)
		{
			return this.m_RootItem;
		}
		TextItem textItem = this.m_RootItem.SearchItem(id);
		if (textItem == null)
		{
			Qoo.Debug.Print("指定親IDが見つかりませんでした。ParentID:" + id);
			return null;
		}
		return textItem;
	}

	// Token: 0x04000022 RID: 34
	public const int ROOT_ID = -1;

	// Token: 0x04000023 RID: 35
	public const int DEF_ITEM_HEIGHT = 16;

	// Token: 0x04000024 RID: 36
	public const int ITEM_X = 16;

	// Token: 0x04000025 RID: 37
	public const int ITEM_Y = 0;

	// Token: 0x04000026 RID: 38
	private TextItem m_RootItem = new TextItem();

	// Token: 0x04000027 RID: 39
	private List<UnityTextSprite> m_TxtList = new List<UnityTextSprite>();

	// Token: 0x04000028 RID: 40
	private List<UnitySprite> m_BaseList = new List<UnitySprite>();

	// Token: 0x04000029 RID: 41
	private TextItem m_CurrentItem;

	// Token: 0x0400002A RID: 42
	private int m_nScrollPos;

	// Token: 0x0400002B RID: 43
	private Color32 m_colTxt = new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue);

	// Token: 0x0400002C RID: 44
	private Color32 m_colBase = new Color32(0, 0, 32, byte.MaxValue);

	// Token: 0x0400002D RID: 45
	private int m_nOldDelta;

	// Token: 0x0400002E RID: 46
	private int m_nItemHeight = 16;

	// Token: 0x0400002F RID: 47
	private int m_nLineNum = 34;
}
