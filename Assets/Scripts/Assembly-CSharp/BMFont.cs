﻿using System;
using System.Collections.Generic;
using Qoo;
using UnityEngine;

// Token: 0x0200014B RID: 331
[Serializable]
public class BMFont
{
	// Token: 0x17000115 RID: 277
	// (get) Token: 0x0600092B RID: 2347 RVA: 0x00027CB0 File Offset: 0x00025EB0
	public bool isValid
	{
		get
		{
			return this.mSaved.Count > 0 || this.LegacyCheck();
		}
	}

	// Token: 0x17000116 RID: 278
	// (get) Token: 0x0600092C RID: 2348 RVA: 0x00027CCC File Offset: 0x00025ECC
	// (set) Token: 0x0600092D RID: 2349 RVA: 0x00027CD4 File Offset: 0x00025ED4
	public int charSize
	{
		get
		{
			return this.mSize;
		}
		set
		{
			this.mSize = value;
		}
	}

	// Token: 0x17000117 RID: 279
	// (get) Token: 0x0600092E RID: 2350 RVA: 0x00027CE0 File Offset: 0x00025EE0
	// (set) Token: 0x0600092F RID: 2351 RVA: 0x00027CE8 File Offset: 0x00025EE8
	public int baseOffset
	{
		get
		{
			return this.mBase;
		}
		set
		{
			this.mBase = value;
		}
	}

	// Token: 0x17000118 RID: 280
	// (get) Token: 0x06000930 RID: 2352 RVA: 0x00027CF4 File Offset: 0x00025EF4
	// (set) Token: 0x06000931 RID: 2353 RVA: 0x00027CFC File Offset: 0x00025EFC
	public int texWidth
	{
		get
		{
			return this.mWidth;
		}
		set
		{
			this.mWidth = value;
		}
	}

	// Token: 0x17000119 RID: 281
	// (get) Token: 0x06000932 RID: 2354 RVA: 0x00027D08 File Offset: 0x00025F08
	// (set) Token: 0x06000933 RID: 2355 RVA: 0x00027D10 File Offset: 0x00025F10
	public int texHeight
	{
		get
		{
			return this.mHeight;
		}
		set
		{
			this.mHeight = value;
		}
	}

	// Token: 0x1700011A RID: 282
	// (get) Token: 0x06000934 RID: 2356 RVA: 0x00027D1C File Offset: 0x00025F1C
	public int glyphCount
	{
		get
		{
			return (!this.isValid) ? 0 : this.mSaved.Count;
		}
	}

	// Token: 0x1700011B RID: 283
	// (get) Token: 0x06000935 RID: 2357 RVA: 0x00027D3C File Offset: 0x00025F3C
	// (set) Token: 0x06000936 RID: 2358 RVA: 0x00027D44 File Offset: 0x00025F44
	public string spriteName
	{
		get
		{
			return this.mSpriteName;
		}
		set
		{
			this.mSpriteName = value;
		}
	}

	// Token: 0x1700011C RID: 284
	// (get) Token: 0x06000937 RID: 2359 RVA: 0x00027D50 File Offset: 0x00025F50
	public List<BMSymbol> symbols
	{
		get
		{
			return this.mSymbols;
		}
	}

	// Token: 0x06000938 RID: 2360 RVA: 0x00027D58 File Offset: 0x00025F58
	public bool LegacyCheck()
	{
		if (this.mGlyphs != null && this.mGlyphs.Length > 0)
		{
			int i = 0;
			int num = this.mGlyphs.Length;
			while (i < num)
			{
				BMGlyph bmglyph = this.mGlyphs[i];
				if (bmglyph != null)
				{
					bmglyph.index = i;
					this.mSaved.Add(bmglyph);
					this.mDict.Add(i, bmglyph);
				}
				i++;
			}
			this.mGlyphs = null;
			return true;
		}
		return false;
	}

	// Token: 0x06000939 RID: 2361 RVA: 0x00027DD4 File Offset: 0x00025FD4
	private int GetArraySize(int index)
	{
		if (index < 256)
		{
			return 256;
		}
		if (index < 65536)
		{
			return 65536;
		}
		if (index < 262144)
		{
			return 262144;
		}
		return 0;
	}

	// Token: 0x0600093A RID: 2362 RVA: 0x00027E18 File Offset: 0x00026018
	public BMGlyph GetGlyph(int index, bool createIfMissing)
	{
		BMGlyph bmglyph = null;
		if (this.mDict.Count == 0)
		{
			if (this.mSaved.Count == 0)
			{
				this.LegacyCheck();
			}
			else
			{
				int i = 0;
				int count = this.mSaved.Count;
				while (i < count)
				{
					BMGlyph bmglyph2 = this.mSaved[i];
					this.mDict.Add(bmglyph2.index, bmglyph2);
					i++;
				}
			}
		}
		if (!this.mDict.TryGetValue(index, out bmglyph) && createIfMissing)
		{
			bmglyph = new BMGlyph();
			bmglyph.index = index;
			this.mSaved.Add(bmglyph);
			this.mDict.Add(index, bmglyph);
		}
		return bmglyph;
	}

	// Token: 0x0600093B RID: 2363 RVA: 0x00027ED0 File Offset: 0x000260D0
	public BMGlyph GetGlyph(int index)
	{
		return this.GetGlyph(index, false);
	}

	// Token: 0x0600093C RID: 2364 RVA: 0x00027EDC File Offset: 0x000260DC
	public BMSymbol GetSymbol(string sequence, bool createIfMissing)
	{
		int i = 0;
		int count = this.mSymbols.Count;
		while (i < count)
		{
			BMSymbol bmsymbol = this.mSymbols[i];
			if (bmsymbol.sequence == sequence)
			{
				return bmsymbol;
			}
			i++;
		}
		if (createIfMissing)
		{
			BMSymbol bmsymbol2 = new BMSymbol();
			bmsymbol2.sequence = sequence;
			this.mSymbols.Add(bmsymbol2);
			return bmsymbol2;
		}
		return null;
	}

	// Token: 0x0600093D RID: 2365 RVA: 0x00027F4C File Offset: 0x0002614C
	public BMSymbol MatchSymbol(string text, int offset, int textLength)
	{
		int count = this.mSymbols.Count;
		if (count == 0)
		{
			return null;
		}
		textLength -= offset;
		for (int i = 0; i < count; i++)
		{
			BMSymbol bmsymbol = this.mSymbols[i];
			int length = bmsymbol.length;
			if (length != 0 && textLength >= length)
			{
				bool flag = true;
				for (int j = 0; j < length; j++)
				{
					if (text[offset + j] != bmsymbol.sequence[j])
					{
						flag = false;
						break;
					}
				}
				if (flag)
				{
					return bmsymbol;
				}
			}
		}
		return null;
	}

	// Token: 0x0600093E RID: 2366 RVA: 0x00027FF4 File Offset: 0x000261F4
	public void Clear()
	{
		this.mGlyphs = null;
		this.mDict.Clear();
		this.mSaved.Clear();
	}

	// Token: 0x0600093F RID: 2367 RVA: 0x00028014 File Offset: 0x00026214
	public void Trim(int xMin, int yMin, int xMax, int yMax)
	{
		if (this.isValid)
		{
			int i = 0;
			int count = this.mSaved.Count;
			while (i < count)
			{
				BMGlyph bmglyph = this.mSaved[i];
				if (bmglyph != null)
				{
					bmglyph.Trim(xMin, yMin, xMax, yMax);
				}
				i++;
			}
		}
	}

	// Token: 0x06000940 RID: 2368 RVA: 0x00028068 File Offset: 0x00026268
	public void Build()
	{
		BMGlyph glyph = this.GetGlyph(9633);
		if (glyph != null)
		{
			int advance = glyph.advance;
			int baseOffset = this.baseOffset;
			foreach (KeyValuePair<int, BMGlyph> keyValuePair in this.mDict)
			{
				keyValuePair.Value.offsetY -= baseOffset - advance;
			}
			this.baseOffset = advance;
			this.charSize = advance;
		}
		foreach (BMGlyph bmglyph in this.mDict.Values)
		{
			if (bmglyph.kerning != null && bmglyph.kerning.Count > 0)
			{
				string text = string.Empty;
				for (int num = 0; num != bmglyph.kerning.Count; num += 2)
				{
					text += (char)bmglyph.kerning[num];
					text += ' ';
				}
				char c = (char)bmglyph.index;
				Qoo.Debug.Print(string.Format("Kerning:BASE={0}:PAIR:{1}", c, text));
			}
		}
	}

	// Token: 0x06000941 RID: 2369 RVA: 0x000281F8 File Offset: 0x000263F8
	public bool CheckGlyph(string str)
	{
		foreach (char index in str)
		{
			if (this.GetGlyph((int)index) == null)
			{
				return false;
			}
		}
		return true;
	}

	// Token: 0x040007A7 RID: 1959
	[HideInInspector]
	[SerializeField]
	private BMGlyph[] mGlyphs;

	// Token: 0x040007A8 RID: 1960
	[HideInInspector]
	[SerializeField]
	private int mSize;

	// Token: 0x040007A9 RID: 1961
	[SerializeField]
	[HideInInspector]
	private int mBase;

	// Token: 0x040007AA RID: 1962
	[HideInInspector]
	[SerializeField]
	private int mWidth;

	// Token: 0x040007AB RID: 1963
	[SerializeField]
	[HideInInspector]
	private int mHeight;

	// Token: 0x040007AC RID: 1964
	[HideInInspector]
	[SerializeField]
	private string mSpriteName;

	// Token: 0x040007AD RID: 1965
	[SerializeField]
	[HideInInspector]
	private List<BMGlyph> mSaved = new List<BMGlyph>();

	// Token: 0x040007AE RID: 1966
	[SerializeField]
	[HideInInspector]
	private List<BMSymbol> mSymbols = new List<BMSymbol>();

	// Token: 0x040007AF RID: 1967
	private Dictionary<int, BMGlyph> mDict = new Dictionary<int, BMGlyph>();
}
