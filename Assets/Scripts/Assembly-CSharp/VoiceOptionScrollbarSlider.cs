﻿using System;
using UnityEngine;

// Token: 0x020000F7 RID: 247
public class VoiceOptionScrollbarSlider : MonoBehaviour
{
	// Token: 0x0600069E RID: 1694 RVA: 0x0001B650 File Offset: 0x00019850
	public void SetSlideObject(GameObject go)
	{
		this.slideObject = go;
	}

	// Token: 0x0600069F RID: 1695 RVA: 0x0001B65C File Offset: 0x0001985C
	public void SetHeight(int height)
	{
		this.m_Height = height;
	}

	// Token: 0x060006A0 RID: 1696 RVA: 0x0001B668 File Offset: 0x00019868
	public void SetListHeight(int listHeight)
	{
		this.m_ListHeight = listHeight;
	}

	// Token: 0x060006A1 RID: 1697 RVA: 0x0001B674 File Offset: 0x00019874
	private void OnMouseDown()
	{
		this.imageObject = base.gameObject.GetComponent<ImageObject>();
		this.InputStartObjectPosition = this.imageObject.OnViewPosition;
		this.InputStartMousePosition = Input.mousePosition;
	}

	// Token: 0x060006A2 RID: 1698 RVA: 0x0001B6A4 File Offset: 0x000198A4
	private void OnMouseDrag()
	{
		Camera camera = SubPartCamera.GetCamera();
		Vector3 vector = Input.mousePosition - this.InputStartMousePosition;
		Vector3 vector2 = camera.WorldToScreenPoint(this.InputStartObjectPosition);
		vector2.y += vector.y;
		vector2 = camera.ScreenToWorldPoint(vector2);
		if (vector2.y > this.imageObject.OriginalPosition.y)
		{
			vector2.y = this.imageObject.OriginalPosition.y;
		}
		if (vector2.y < this.imageObject.OriginalPosition.y - (float)this.m_Height)
		{
			vector2.y = this.imageObject.OriginalPosition.y - (float)this.m_Height;
		}
		this.imageObject.OnViewPosition = vector2;
		float num = (this.imageObject.OriginalPosition.y - vector2.y) / (float)this.m_Height;
		float y = num * (float)this.m_ListHeight;
		this.slideObject.transform.localPosition = new Vector3(this.slideObject.transform.localPosition.x, y, this.slideObject.transform.localPosition.z);
	}

	// Token: 0x060006A3 RID: 1699 RVA: 0x0001B7EC File Offset: 0x000199EC
	private void OnMouseUp()
	{
	}

	// Token: 0x04000605 RID: 1541
	private GameObject slideObject;

	// Token: 0x04000606 RID: 1542
	private ImageObject imageObject;

	// Token: 0x04000607 RID: 1543
	private Vector3 InputStartObjectPosition;

	// Token: 0x04000608 RID: 1544
	private Vector3 InputStartMousePosition;

	// Token: 0x04000609 RID: 1545
	private int m_Height;

	// Token: 0x0400060A RID: 1546
	private int m_ListHeight;
}
