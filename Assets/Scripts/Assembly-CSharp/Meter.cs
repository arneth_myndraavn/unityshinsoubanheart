﻿using System;
using System.Collections;
using UnityEngine;

// Token: 0x020000E9 RID: 233
public class Meter : MonoBehaviour
{
	// Token: 0x0600064E RID: 1614 RVA: 0x0001A430 File Offset: 0x00018630
	public void Init()
	{
		GameObject gameObject = GameObject.Find(base.name + "_collider");
		if (gameObject == null)
		{
			gameObject = new GameObject(base.name + "_collider");
		}
		MeterCollider meterCollider = gameObject.GetComponent<MeterCollider>();
		if (meterCollider == null)
		{
			meterCollider = gameObject.AddComponent<MeterCollider>();
		}
		gameObject.transform.parent = base.transform.parent;
		meterCollider.Init(base.gameObject);
		this.m_ImageObject = base.gameObject.GetComponent<ImageObject>();
		base.StartCoroutine(this.WaitLoadTextureCoroutine());
	}

	// Token: 0x0600064F RID: 1615 RVA: 0x0001A4D0 File Offset: 0x000186D0
	protected virtual IEnumerator WaitLoadTextureCoroutine()
	{
		while (base.gameObject.GetComponent<Renderer>().material.mainTexture == null)
		{
			yield return 0;
		}
		yield break;
	}

	// Token: 0x06000650 RID: 1616 RVA: 0x0001A4EC File Offset: 0x000186EC
	public virtual void OnEdit(float value)
	{
	}

	// Token: 0x06000651 RID: 1617 RVA: 0x0001A4F0 File Offset: 0x000186F0
	public virtual void UpdateMeter(float rate)
	{
		Vector3 originalScale = this.m_ImageObject.OriginalScale;
		Vector3 originalPosition = this.m_ImageObject.OriginalPosition;
		if (rate < 0f)
		{
			rate = 0f;
		}
		if (rate > 1f)
		{
			rate = 1f;
		}
		originalScale.x = this.m_ImageObject.OriginalScale.x * rate;
		originalPosition.x = this.m_ImageObject.OriginalPosition.x - (this.m_ImageObject.OriginalScale.x - originalScale.x) / 2f;
		this.m_ImageObject.OnViewScale = originalScale;
		this.m_ImageObject.OnViewPosition = originalPosition;
		this.OnEdit(rate);
	}

	// Token: 0x06000652 RID: 1618 RVA: 0x0001A5A8 File Offset: 0x000187A8
	public virtual float ValueStep(float value)
	{
		return value;
	}

	// Token: 0x06000653 RID: 1619 RVA: 0x0001A5AC File Offset: 0x000187AC
	public virtual void OnEditEnd()
	{
	}

	// Token: 0x040005C6 RID: 1478
	private ImageObject m_ImageObject;

	// Token: 0x040005C7 RID: 1479
	protected int m_Value;
}
