﻿using System;
using System.Collections;
using Qoo;
using UnityEngine;

// Token: 0x02000154 RID: 340
public class Man2D : Singleton<Man2D>
{
	// Token: 0x17000129 RID: 297
	// (get) Token: 0x06000974 RID: 2420 RVA: 0x00029DFC File Offset: 0x00027FFC
	public ManSprite SpriteManager
	{
		get
		{
			return this.m_Sprite;
		}
	}

	// Token: 0x1700012A RID: 298
	// (get) Token: 0x06000975 RID: 2421 RVA: 0x00029E04 File Offset: 0x00028004
	public ManTexture TexManager
	{
		get
		{
			return this.m_Texture;
		}
	}

	// Token: 0x06000976 RID: 2422 RVA: 0x00029E0C File Offset: 0x0002800C
	private void Awake()
	{
		this.m_nScreenW = 960;
		this.m_nScreenH = 544;
		this.CreateCamera();
		this.CreateSpriteManager();
		this.m_Texture = base.gameObject.AddComponent<ManTexture>();
		this.CreateFrameBuffer();
		/*this.m_BackSprite = this.AddSprite(string.Empty);
		this.m_BackSprite.z = 0;
		this.m_BackSprite.w = this.m_nScreenW;
		this.m_BackSprite.h = this.m_nScreenH;
		this.m_BackSprite.R = 0;
		this.m_BackSprite.G = 0;
		this.m_BackSprite.B = 0;
		this.m_BackSprite.A = byte.MaxValue;
		this.m_BackSprite.Brend = SPRITE_DRAW_MODE.BACK;*/
		base.enabled = false;
	}

	// Token: 0x06000977 RID: 2423 RVA: 0x00029ED8 File Offset: 0x000280D8
	private void OnEnable()
	{
		this.m_Sprite.Enable();
		base.GetComponent<Camera>().enabled = true;
	}

	// Token: 0x06000978 RID: 2424 RVA: 0x00029EF4 File Offset: 0x000280F4
	private void OnDisable()
	{
		this.m_Sprite.Disable();
		base.GetComponent<Camera>().enabled = false;
	}

	// Token: 0x06000979 RID: 2425 RVA: 0x00029F10 File Offset: 0x00028110
	public IEnumerator EraseFrameBuffer()
	{
		bool old = Singleton<Man2D>.Instance.enabled;
		Singleton<Man2D>.Instance.enabled = true;
		yield return 0;
		Singleton<Man2D>.Instance.UpdateFrameBuffer();
		yield return 0;
		Singleton<Man2D>.Instance.UpdateFrameBuffer();
		yield return 0;
		Singleton<Man2D>.Instance.enabled = old;
		yield break;
	}

	// Token: 0x0600097A RID: 2426 RVA: 0x00029F24 File Offset: 0x00028124
	public static UnitySprite Sprite(string name = "")
	{
		return Singleton<Man2D>.Instance.AddSprite(name);
	}

	// Token: 0x0600097B RID: 2427 RVA: 0x00029F34 File Offset: 0x00028134
	public static UnityTextSprite TextSprite(UIFont font = null)
	{
		UnityTextSprite unityTextSprite = Singleton<Man2D>.Instance.AddTextSprite();
		if (font == null)
		{
			unityTextSprite.Font = Singleton<UnityGraph>.Instance.Font;
		}
		else
		{
			unityTextSprite.Font = font;
		}
		return unityTextSprite;
	}

	// Token: 0x0600097C RID: 2428 RVA: 0x00029F78 File Offset: 0x00028178
	public static UnityTexture Texture(string name)
	{
		return Singleton<Man2D>.Instance.LoadTexture(name);
	}

	// Token: 0x0600097D RID: 2429 RVA: 0x00029F88 File Offset: 0x00028188
	internal static void ReadyTexture(string name, bool IsDec = true)
	{
		if (!Singleton<Man2D>.Instance.m_Texture.IsFind(name))
		{
			Singleton<Man2D>.Instance.m_Texture.Ready(name, IsDec);
		}
	}

	// Token: 0x0600097E RID: 2430 RVA: 0x00029FBC File Offset: 0x000281BC
	public static bool IsUseTexture(string name)
	{
		return Singleton<Man2D>.Instance.m_Texture.IsFind(name) && Singleton<Man2D>.Instance.m_Texture.IsUse(name);
	}

	// Token: 0x0600097F RID: 2431 RVA: 0x00029FE8 File Offset: 0x000281E8
	public UnitySprite AddSprite(string grp = "")
	{
		if (grp != null && grp.Length > 0)
		{
			UnitySprite unitySprite = this.m_Sprite.Add(new UnitySprite(true, base.gameObject, false));
			unitySprite.SetImage(this.LoadTexture(grp));
			return unitySprite;
		}
		return this.m_Sprite.Add(new UnitySprite(true, base.gameObject, false));
	}

	// Token: 0x06000980 RID: 2432 RVA: 0x0002A048 File Offset: 0x00028248
	public UnityTextSprite AddTextSprite()
	{
		UnityTextSprite unityTextSprite = new UnityTextSprite(true, base.gameObject);
		this.m_Sprite.Add(unityTextSprite);
		return unityTextSprite;
	}

	// Token: 0x06000981 RID: 2433 RVA: 0x0002A070 File Offset: 0x00028270
	public bool RemoveSprite(UnitySprite sp)
	{
		if (sp != null)
		{
			UnityEngine.Object.Destroy(sp.obj);
			if (sp.tex != null && sp.IsTexRelease)
			{
				this.ReleaseTexture(sp.tex, false);
			}
			sp.Reset();
			return this.m_Sprite.Remove(sp);
		}
		return true;
	}

	// Token: 0x06000982 RID: 2434 RVA: 0x0002A0C8 File Offset: 0x000282C8
	public UnityTexture LoadTexture(string name)
	{
		return this.m_Texture.Add(name);
	}

	// Token: 0x06000983 RID: 2435 RVA: 0x0002A0D8 File Offset: 0x000282D8
	public bool IsUseTextue(string name)
	{
		return this.m_Texture.IsUse(name);
	}

	// Token: 0x06000984 RID: 2436 RVA: 0x0002A0E8 File Offset: 0x000282E8
	public UnityTexture LoadTexture(string name, byte[] data_)
	{
		return this.m_Texture.Add(name, data_);
	}

	// Token: 0x06000985 RID: 2437 RVA: 0x0002A0F8 File Offset: 0x000282F8
	public UnityTexture SetTexture(string name, Texture2D tex)
	{
		return this.m_Texture.Add(name, tex);
	}

	// Token: 0x06000986 RID: 2438 RVA: 0x0002A108 File Offset: 0x00028308
	public void ReleaseTexture(UnityTexture tex, bool IsForce = false)
	{
		if (tex != null)
		{
			this.m_Texture.Remove(tex, IsForce);
		}
	}

	// Token: 0x06000987 RID: 2439 RVA: 0x0002A120 File Offset: 0x00028320
	public bool ReleaseTexture(string name, bool IsForce = false)
	{
		return name != null && this.m_Texture.Remove(name, IsForce);
	}

	// Token: 0x06000988 RID: 2440 RVA: 0x0002A138 File Offset: 0x00028338
	public RenderTexture GetFrameBuffer()
	{
		if (!this.IsReadyFrameBuffer())
		{
			return null;
		}
		if (this.m_FrameBufferAr[this.GetFrameNoNew()] != null && this.m_FrameBufferAr[this.GetFrameNoNew()].IsReady)
		{
			return this.m_FrameBufferAr[this.GetFrameNoNew()].Tex;
		}
		return null;
	}

	// Token: 0x06000989 RID: 2441 RVA: 0x0002A190 File Offset: 0x00028390
	public RenderTexture GetFrameBufferOld()
	{
		if (!this.IsReadyFrameBuffer())
		{
			return null;
		}
		if (this.m_FrameBufferAr[this.GetFrameNoOld()] != null && this.m_FrameBufferAr[this.GetFrameNoOld()].IsReady)
		{
			return this.m_FrameBufferAr[this.GetFrameNoOld()].Tex;
		}
		return null;
	}

	// Token: 0x0600098A RID: 2442 RVA: 0x0002A1E8 File Offset: 0x000283E8
	private int GetFrameNoNew()
	{
		int num = this.m_nFrameNo - 1;
		if (num < 0)
		{
			num += this.m_FrameBufferAr.Length;
		}
		return num;
	}

	// Token: 0x0600098B RID: 2443 RVA: 0x0002A214 File Offset: 0x00028414
	private int GetFrameNoOld()
	{
		int num = this.m_nFrameNo - 2;
		if (num < 0)
		{
			num += this.m_FrameBufferAr.Length;
		}
		return num;
	}

	// Token: 0x0600098C RID: 2444 RVA: 0x0002A240 File Offset: 0x00028440
	private bool IsReadyFrameBuffer()
	{
		for (int i = 0; i < this.m_FrameBufferAr.Length; i++)
		{
			if (this.m_FrameBufferAr[this.m_nFrameNo] == null || !this.m_FrameBufferAr[this.m_nFrameNo].IsReady)
			{
				return false;
			}
		}
		return true;
	}

	// Token: 0x0600098D RID: 2445 RVA: 0x0002A294 File Offset: 0x00028494
	private void Update()
	{
		this.m_Sprite.Update();
	}

	// Token: 0x0600098E RID: 2446 RVA: 0x0002A2A4 File Offset: 0x000284A4
	public void UpdateFrameBuffer()
	{
		if (base.GetComponent<Camera>().targetTexture != null && this.m_FrameBufferAr[this.m_nFrameNo] != null)
		{
			this.m_FrameBufferAr[this.m_nFrameNo].IsReady = true;
		}
		this.m_nFrameNo++;
		if (this.m_nFrameNo < 0 || this.m_nFrameNo >= this.m_FrameBufferAr.Length)
		{
			this.m_nFrameNo = 0;
		}
		if (this.m_FrameBufferAr[this.m_nFrameNo] != null)
		{
			base.GetComponent<Camera>().targetTexture = this.m_FrameBufferAr[this.m_nFrameNo].Tex;
		}
	}

	// Token: 0x0600098F RID: 2447 RVA: 0x0002A350 File Offset: 0x00028550
	private void CreateCamera()
	{
		base.gameObject.AddComponent<Camera>();
		CreateSprite.CreateSpriteCamera(base.gameObject, this.m_nScreenH, true);
		base.gameObject.GetComponent<Camera>().aspect = 1.7647059f;
	}

	// Token: 0x06000990 RID: 2448 RVA: 0x0002A390 File Offset: 0x00028590
	private void CreateSpriteManager()
	{
		this.m_Sprite = new ManSprite();
		float z = base.GetComponent<Camera>().transform.localPosition.z;
		this.m_Sprite.SetZ(base.GetComponent<Camera>().nearClipPlane + z, base.GetComponent<Camera>().farClipPlane + z);
		this.m_Sprite.SetScreenSize(this.m_nScreenW, this.m_nScreenH);
	}

	// Token: 0x06000991 RID: 2449 RVA: 0x0002A400 File Offset: 0x00028600
	public void CreateFrameBuffer()
	{
		this.ReleaseFrameBuffer();
		Qoo.Debug.Print("Create Frame Buffer");
		int w = 960;
		int h = 544;
		/*if (iPhone.generation == iPhoneGeneration.iPhone4)
		{
			w = 512;
			h = 512;
		}*/
		//Qoo.Debug.Print("Info:iPhone.generation:" + iPhone.generation);
		for (int i = 0; i < this.m_FrameBufferAr.Length; i++)
		{
			this.m_FrameBufferAr[i] = new FrameBuffer(w, h, "2D Sprite FrameBuffer" + i);
		}
		this.m_nFrameNo = 0;
		base.GetComponent<Camera>().targetTexture = this.m_FrameBufferAr[this.m_nFrameNo].Tex;
	}

	// Token: 0x06000992 RID: 2450 RVA: 0x0002A4B8 File Offset: 0x000286B8
	public void ReleaseFrameBuffer()
	{
		Qoo.Debug.Print("Release Frame Buffer");
		base.GetComponent<Camera>().targetTexture = null;
		for (int i = 0; i < this.m_FrameBufferAr.Length; i++)
		{
			if (this.m_FrameBufferAr[i] != null)
			{
				UnityEngine.Object.Destroy(this.m_FrameBufferAr[i].Tex);
				this.m_FrameBufferAr[i].Tex = null;
			}
			this.m_FrameBufferAr[i] = null;
		}
	}

	// Token: 0x040007D3 RID: 2003
	private const int FrameBufferNum = 2;

	// Token: 0x040007D4 RID: 2004
	private ManSprite m_Sprite;

	// Token: 0x040007D5 RID: 2005
	private ManTexture m_Texture;

	// Token: 0x040007D6 RID: 2006
	private int m_nScreenH;

	// Token: 0x040007D7 RID: 2007
	private int m_nScreenW;

	// Token: 0x040007D8 RID: 2008
	private UnitySprite m_BackSprite;

	// Token: 0x040007D9 RID: 2009
	private FrameBuffer[] m_FrameBufferAr = new FrameBuffer[2];

	// Token: 0x040007DA RID: 2010
	private int m_nFrameNo;
}
