﻿using System;
using System.Collections.Generic;
using Qoo.Application;
using Qoo.Game;
using Qoo.Ks;

namespace Qoo.Select
{
	// Token: 0x020000BB RID: 187
	public class SceneSelect
	{
		// Token: 0x170000CC RID: 204
		// (get) Token: 0x0600058C RID: 1420 RVA: 0x00016300 File Offset: 0x00014500
		public List<SelectItem> SelectAr
		{
			get
			{
				return this.m_SelectAr;
			}
		}

		// Token: 0x170000CD RID: 205
		// (get) Token: 0x0600058D RID: 1421 RVA: 0x00016308 File Offset: 0x00014508
		public int Count
		{
			get
			{
				return this.m_SelectAr.Count;
			}
		}

		// Token: 0x170000CE RID: 206
		// (get) Token: 0x0600058E RID: 1422 RVA: 0x00016318 File Offset: 0x00014518
		// (set) Token: 0x0600058F RID: 1423 RVA: 0x00016320 File Offset: 0x00014520
		public string Name { get; set; }

		// Token: 0x06000590 RID: 1424 RVA: 0x0001632C File Offset: 0x0001452C
		public bool AddSelect(EVENTSCENE_SELCET[] select)
		{
			this.Clear();
			foreach (EVENTSCENE_SELCET eventscene_SELCET in select)
			{
				if (eventscene_SELCET.Name.Length > 0)
				{
					this.m_SelectAr.Add(new SelectItem(eventscene_SELCET.Name, eventscene_SELCET.nParam == 1, eventscene_SELCET.KsName, eventscene_SELCET.LabelName, eventscene_SELCET.Effect));
				}
			}
			return true;
		}

		// Token: 0x06000591 RID: 1425 RVA: 0x0001639C File Offset: 0x0001459C
		public void Clear()
		{
			App.QooSelect.SelectAr.Clear();
		}

		// Token: 0x06000592 RID: 1426 RVA: 0x000163B0 File Offset: 0x000145B0
		public bool IsEntry(string szSelect, string szKs, string szLabel)
		{
			foreach (SelectItem selectItem in App.QooSelect.SelectAr)
			{
				if (selectItem.Ks == szKs && selectItem.Label == szLabel && selectItem.Select == szSelect)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000593 RID: 1427 RVA: 0x00016450 File Offset: 0x00014650
		public bool IsGrp()
		{
			bool flag = false;
			bool flag2 = false;
			foreach (SelectItem selectItem in App.QooSelect.SelectAr)
			{
				if (!flag2)
				{
					flag = selectItem.IsGrpFile;
					flag2 = true;
				}
				else if (flag != selectItem.IsGrpFile)
				{
					Debug.Assert(flag != selectItem.IsGrpFile, "Error:グラフィック選択肢とテキスト選択肢が混じっています");
				}
			}
			return flag;
		}

		// Token: 0x06000594 RID: 1428 RVA: 0x000164F0 File Offset: 0x000146F0
		internal bool IsAnim(int i)
		{
			return SysData.IsEnableLoveAnim() && i >= 0 && i < this.m_SelectAr.Count && this.m_SelectAr[i].Effect.Length > 0;
		}

		// Token: 0x0400041D RID: 1053
		private List<SelectItem> m_SelectAr = new List<SelectItem>();
	}
}
