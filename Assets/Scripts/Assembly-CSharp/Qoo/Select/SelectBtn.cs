﻿using System;
using Qoo.Def;

namespace Qoo.Select
{
	// Token: 0x020000BC RID: 188
	public class SelectBtn
	{
		// Token: 0x06000596 RID: 1430 RVA: 0x00016544 File Offset: 0x00014744
		public bool CreateGrpBtn(string grpFile, bool IsMark = false, int iAddZ = 0)
		{
			this.m_Btn = UnityApp.Graph.m_ManSprite.AddSprite(grpFile);
			this.m_Btn.SetPtnNum(1, 2);
			this.m_Btn.SetPtnPos(0, 0);
			this.m_Btn.z = 781 + iAddZ;
			this.m_Btn.Show = false;
			this.m_Btn.SetName("GRP BTN");
			if (IsMark)
			{
				this.m_Mark = UnityApp.Graph.m_ManSprite.AddSprite("sel_kidoku.png");
				this.m_Mark.z = 782 + iAddZ;
				this.m_Mark.Show = false;
				this.m_Btn.SetName("GRP BTN MARK");
			}
			return true;
		}

		// Token: 0x06000597 RID: 1431 RVA: 0x00016600 File Offset: 0x00014800
		public bool CreateTxtBtn(string message, bool IsMark = false)
		{
			this.m_Btn = UnityApp.Graph.m_ManSprite.AddSprite("select_bar.png");
			this.m_Btn.SetPtnNum(1, 2);
			this.m_Btn.SetPtnPos(0, 0);
			this.m_Btn.SetName("TXTBTN_BASE");
			this.m_Btn.z = 781;
			this.m_Text = Man2D.TextSprite(null);
			if (IsMark)
			{
				this.m_Text.SetTextColor(SelectDef.SELECT_COLOR);
			}
			else
			{
				this.m_Text.SetTextColor(SelectDef.TEXT_COLOR);
			}
			this.m_Text.AddText(message, 28);
			this.m_Text.z = 782;
			this.m_Text.SetName("TXTBTN_TEXT");
			return true;
		}

		// Token: 0x06000598 RID: 1432 RVA: 0x000166C8 File Offset: 0x000148C8
		public bool Restore()
		{
			if (this.m_Btn.w == 0 && this.m_Btn.h == 0)
			{
				this.m_Btn.SetImage(this.m_Btn.tex);
				this.m_Btn.SetPtnNum(1, 2);
				this.m_Btn.SetPtnPos(0, 0);
			}
			return true;
		}

		// Token: 0x06000599 RID: 1433 RVA: 0x00016728 File Offset: 0x00014928
		public bool Release()
		{
			if (Singleton<Man2D>.IsReady)
			{
				if (this.m_Btn != null)
				{
					Singleton<Man2D>.Instance.RemoveSprite(this.m_Btn);
				}
				if (this.m_Mark != null)
				{
					Singleton<Man2D>.Instance.RemoveSprite(this.m_Mark);
				}
				if (this.m_Text != null)
				{
					Singleton<Man2D>.Instance.RemoveSprite(this.m_Text);
				}
			}
			this.m_Btn = null;
			this.m_Mark = null;
			this.m_Text = null;
			return true;
		}

		// Token: 0x0600059A RID: 1434 RVA: 0x000167AC File Offset: 0x000149AC
		public void SetPos(Point2 pos)
		{
			this.m_Btn.x = pos.x;
			this.m_Btn.y = pos.y;
			if (this.m_Mark != null)
			{
				this.m_Mark.x = 16 + pos.x;
				this.m_Mark.y = 12 + pos.y;
			}
			if (this.m_Text != null)
			{
				this.m_Text.CalcSize();
				this.m_Text.x = 480 - this.m_Text.w / 2;
				this.m_Text.y = 32 + pos.y;
			}
		}

		// Token: 0x0600059B RID: 1435 RVA: 0x00016858 File Offset: 0x00014A58
		public void Show(bool isShow)
		{
			this.m_Btn.Show = isShow;
			if (this.m_Mark != null)
			{
				this.m_Mark.Show = isShow;
			}
			if (this.m_Text != null)
			{
				this.m_Text.Show = isShow;
			}
		}

		// Token: 0x0600059C RID: 1436 RVA: 0x000168A0 File Offset: 0x00014AA0
		public void Select(bool isSelect)
		{
			if (!isSelect)
			{
				this.m_Btn.SetPtnPos(0, 0);
			}
			else if (this.m_Text != null)
			{
				this.m_Btn.SetPtnPos(0, 1);
			}
			else
			{
				this.m_Btn.SetPtnPos(0, 1);
			}
		}

		// Token: 0x0600059D RID: 1437 RVA: 0x000168F0 File Offset: 0x00014AF0
		public Point2 GetPos()
		{
			return new Point2(this.m_Btn.x, this.m_Btn.y);
		}

		// Token: 0x0600059E RID: 1438 RVA: 0x00016910 File Offset: 0x00014B10
		public Size GetSize()
		{
			return new Size(this.m_Btn.w, this.m_Btn.h);
		}

		// Token: 0x0600059F RID: 1439 RVA: 0x00016930 File Offset: 0x00014B30
		public bool IsHit(Point2 pos)
		{
			return this.m_Btn.x <= pos.x && pos.x < this.m_Btn.x + this.m_Btn.w && this.m_Btn.y <= pos.y && pos.y < this.m_Btn.y + this.m_Btn.h;
		}

		// Token: 0x0400041F RID: 1055
		private UnitySprite m_Btn;

		// Token: 0x04000420 RID: 1056
		private UnitySprite m_Mark;

		// Token: 0x04000421 RID: 1057
		private UnityTextSprite m_Text;
	}
}
