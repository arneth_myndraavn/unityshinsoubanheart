﻿using System;

namespace Qoo.Select
{
	// Token: 0x020000BA RID: 186
	public class SelectItem
	{
		// Token: 0x06000580 RID: 1408 RVA: 0x00016250 File Offset: 0x00014450
		public SelectItem(string select_, bool isgrpfile_, string ks_, string label_, string effect_)
		{
			this.Select = select_;
			this.IsGrpFile = isgrpfile_;
			this.Ks = ks_;
			this.Label = label_;
			this.Effect = effect_;
		}

		// Token: 0x170000C7 RID: 199
		// (get) Token: 0x06000581 RID: 1409 RVA: 0x00016288 File Offset: 0x00014488
		// (set) Token: 0x06000582 RID: 1410 RVA: 0x00016290 File Offset: 0x00014490
		public string Select { get; set; }

		// Token: 0x170000C8 RID: 200
		// (get) Token: 0x06000583 RID: 1411 RVA: 0x0001629C File Offset: 0x0001449C
		// (set) Token: 0x06000584 RID: 1412 RVA: 0x000162A4 File Offset: 0x000144A4
		public bool IsGrpFile { get; set; }

		// Token: 0x170000C9 RID: 201
		// (get) Token: 0x06000585 RID: 1413 RVA: 0x000162B0 File Offset: 0x000144B0
		// (set) Token: 0x06000586 RID: 1414 RVA: 0x000162B8 File Offset: 0x000144B8
		public string Ks { get; private set; }

		// Token: 0x170000CA RID: 202
		// (get) Token: 0x06000587 RID: 1415 RVA: 0x000162C4 File Offset: 0x000144C4
		// (set) Token: 0x06000588 RID: 1416 RVA: 0x000162CC File Offset: 0x000144CC
		public string Label { get; private set; }

		// Token: 0x170000CB RID: 203
		// (get) Token: 0x06000589 RID: 1417 RVA: 0x000162D8 File Offset: 0x000144D8
		// (set) Token: 0x0600058A RID: 1418 RVA: 0x000162E0 File Offset: 0x000144E0
		public string Effect { get; set; }

		// Token: 0x04000417 RID: 1047
		private string m_Select;
	}
}
