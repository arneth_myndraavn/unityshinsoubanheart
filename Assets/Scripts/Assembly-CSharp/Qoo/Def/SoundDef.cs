﻿using System;

namespace Qoo.Def
{
	// Token: 0x020000DB RID: 219
	internal static class SoundDef
	{
		// Token: 0x0400059F RID: 1439
		public const string DIRNAME_SOUND = "mp3";

		// Token: 0x040005A0 RID: 1440
		public const string DIRNAME_VOICE = "voice";

		// Token: 0x040005A1 RID: 1441
		public const string DIRNAME_SE = "se";

		// Token: 0x040005A2 RID: 1442
		public const string DIRNAME_BGM = "bgm";

		// Token: 0x040005A3 RID: 1443
		public const string SE_SELECT = "sysse_hover";

		// Token: 0x040005A4 RID: 1444
		public const string SE_OK = "sysse_decide";

		// Token: 0x040005A5 RID: 1445
		public const string SE_BACK = "sysse_back";

		// Token: 0x040005A6 RID: 1446
		public static string[] SE_LOAD_DATA = new string[]
		{
			"sysse_hover",
			"sysse_decide",
			"sysse_back"
		};
	}
}
