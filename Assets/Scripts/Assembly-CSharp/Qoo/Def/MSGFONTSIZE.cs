﻿using System;

namespace Qoo.Def
{
	// Token: 0x020000CB RID: 203
	public enum MSGFONTSIZE
	{
		// Token: 0x040004D3 RID: 1235
		SMALL,
		// Token: 0x040004D4 RID: 1236
		NORMAL,
		// Token: 0x040004D5 RID: 1237
		LARGE,
		// Token: 0x040004D6 RID: 1238
		DOUBLE
	}
}
