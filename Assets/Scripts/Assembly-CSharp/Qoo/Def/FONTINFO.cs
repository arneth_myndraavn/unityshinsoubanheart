﻿using System;

namespace Qoo.Def
{
	// Token: 0x020000CD RID: 205
	public struct FONTINFO
	{
		// Token: 0x060005EF RID: 1519 RVA: 0x00018104 File Offset: 0x00016304
		public FONTINFO(int font_, int size_, float stretch_, int offset_)
		{
			this.nFont = font_;
			this.nSize = size_;
			this.fStretch = stretch_;
			this.nOffsetY = offset_;
		}

		// Token: 0x040004E5 RID: 1253
		public int nFont;

		// Token: 0x040004E6 RID: 1254
		public int nSize;

		// Token: 0x040004E7 RID: 1255
		public float fStretch;

		// Token: 0x040004E8 RID: 1256
		public int nOffsetY;
	}
}
