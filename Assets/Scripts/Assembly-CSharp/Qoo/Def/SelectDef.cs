﻿using System;
using System.Collections.Generic;
using Qoo.Table;
using UnityEngine;

namespace Qoo.Def
{
	// Token: 0x020000D9 RID: 217
	internal static class SelectDef
	{
		// Token: 0x06000600 RID: 1536 RVA: 0x00018860 File Offset: 0x00016A60
		static SelectDef()
		{
			foreach (SelectDef.BTN_TABLE btn_TABLE in SelectDef.g_SelectBtnTable)
			{
				SelectTable selectTable = new SelectTable();
				selectTable.Create(btn_TABLE.name);
				for (int j = 0; j < btn_TABLE.table.GetLength(0); j++)
				{
					selectTable.Add(btn_TABLE.table[j].Length, btn_TABLE.table[j]);
				}
				SelectDef.m_PosTable.Add(btn_TABLE.name, selectTable);
				if (btn_TABLE.isScale)
				{
					SelectDef.m_PosTable[btn_TABLE.name].Scale(2f, 2f);
				}
			}
		}

		// Token: 0x06000601 RID: 1537 RVA: 0x00018EC0 File Offset: 0x000170C0
		public static Point2[] GetBtnPos(string name, bool IsGrp, int num)
		{
			name += ((!IsGrp) ? "_text" : "_grp");
			Debug.Assert(SelectDef.m_PosTable[name] != null, string.Format("Error:指定名称のテーブルはありません name={0}", name));
			Debug.Assert(SelectDef.m_PosTable[name].GetPoint(num) != null, string.Format("Error:指定個数のテーブルはありません name={0} num={1}", name, num));
			return SelectDef.m_PosTable[name].GetPoint(num);
		}

		// Token: 0x0400056D RID: 1389
		public const int MAX = 10;

		// Token: 0x0400056E RID: 1390
		public const string GRP_MARK_FILE = "sel_kidoku.png";

		// Token: 0x0400056F RID: 1391
		public const int GRP_W_NUM = 1;

		// Token: 0x04000570 RID: 1392
		public const int GRP_H_NUM = 2;

		// Token: 0x04000571 RID: 1393
		public const int GRP_KIDOKUOFF_X = 16;

		// Token: 0x04000572 RID: 1394
		public const int GRP_KIDOKUOFF_Y = 12;

		// Token: 0x04000573 RID: 1395
		public const int GRP_MARK_POS_Z = 782;

		// Token: 0x04000574 RID: 1396
		public const int GRP_BTN_POS_Z = 781;

		// Token: 0x04000575 RID: 1397
		public const string BAR_FILE = "select_bar.png";

		// Token: 0x04000576 RID: 1398
		public const int BAR_W_NUM = 1;

		// Token: 0x04000577 RID: 1399
		public const int BAR_H_NUM = 2;

		// Token: 0x04000578 RID: 1400
		public const int BAR_POS_X = 27;

		// Token: 0x04000579 RID: 1401
		public const int BAR_POS_Z = 781;

		// Token: 0x0400057A RID: 1402
		public const int TXT_OFF_Y = 32;

		// Token: 0x0400057B RID: 1403
		public const int TXT_POS_Z = 782;

		// Token: 0x0400057C RID: 1404
		public const int TXT_FONT_SIZE = 28;

		// Token: 0x0400057D RID: 1405
		public const string LOVE_ANIM_FILE = "sys_love.png";

		// Token: 0x0400057E RID: 1406
		public const int LOVE_ANIM_W_NUM = 7;

		// Token: 0x0400057F RID: 1407
		public const int LOVE_ANIM_H_NUM = 2;

		// Token: 0x04000580 RID: 1408
		public const int LOVE_ANIM_TIME = 4;

		// Token: 0x04000581 RID: 1409
		public const int LOVE_ANIM_TXT_X = 364;

		// Token: 0x04000582 RID: 1410
		public const int LOVE_ANIM_TXT_Y = -18;

		// Token: 0x04000583 RID: 1411
		public const int LOVE_ANIM_GRP_X = 50;

		// Token: 0x04000584 RID: 1412
		public const int LOVE_ANIM_GRP_Y = 186;

		// Token: 0x04000585 RID: 1413
		public const int LOVE_ANIM_Z = 783;

		// Token: 0x04000586 RID: 1414
		private const string TEXT_EXT_NAME = "_text";

		// Token: 0x04000587 RID: 1415
		private const string GRP_EXT_NAME = "_grp";

		// Token: 0x04000588 RID: 1416
		private const string NORMAL_NAME = "normal";

		// Token: 0x04000589 RID: 1417
		private const string MAP_NAME = "map";

		// Token: 0x0400058A RID: 1418
		public const string TXTBTN_NAME = "normal_text";

		// Token: 0x0400058B RID: 1419
		public const string GRPBTN_NAME = "normal_grp";

		// Token: 0x0400058C RID: 1420
		public const string MAPBTN_NAME = "map_grp";

		// Token: 0x0400058D RID: 1421
		public const string MAPBTN_NO_CASTLE_NAME = "map_no_castle_grp";

		// Token: 0x0400058E RID: 1422
		public const string MAPBTN_NO_HATTER_NAME = "map_no_hatter_grp";

		// Token: 0x0400058F RID: 1423
		public const string MAPBTN_NO_AMUSE_NAME = "map_no_amuse_grp";

		// Token: 0x04000590 RID: 1424
		public const string MAPBTN_NO_TOWER_NAME = "map_no_tower_grp";

		// Token: 0x04000591 RID: 1425
		public static Color32 TEXT_COLOR = new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue);

		// Token: 0x04000592 RID: 1426
		public static Color32 SELECT_COLOR = new Color32(29, 32, 136, byte.MaxValue);

		// Token: 0x04000593 RID: 1427
		private static Point2[][] TxtBtnPosAr = new Point2[][]
		{
			new Point2[]
			{
				new Point2(27, 73),
				new Point2(27, 153)
			},
			new Point2[]
			{
				new Point2(27, 33),
				new Point2(27, 113),
				new Point2(27, 193)
			},
			new Point2[]
			{
				new Point2(27, 25),
				new Point2(27, 85),
				new Point2(27, 145),
				new Point2(27, 205)
			}
		};

		// Token: 0x04000594 RID: 1428
		private static Point2[][] GrpBtnPosAr = new Point2[][]
		{
			new Point2[]
			{
				new Point2(114, 56),
				new Point2(254, 56)
			},
			new Point2[]
			{
				new Point2(44, 56),
				new Point2(184, 56),
				new Point2(324, 56)
			},
			new Point2[]
			{
				new Point2(15, 61),
				new Point2(81, 206),
				new Point2(318, 20),
				new Point2(170, 126)
			},
			new Point2[]
			{
				new Point2(121, 45),
				new Point2(201, 45),
				new Point2(280, 45),
				new Point2(121, 148),
				new Point2(201, 148),
				new Point2(280, 148)
			},
			new Point2[]
			{
				new Point2(82, 45),
				new Point2(161, 45),
				new Point2(240, 45),
				new Point2(319, 45),
				new Point2(121, 148),
				new Point2(201, 148),
				new Point2(280, 148)
			},
			new Point2[]
			{
				new Point2(82, 45),
				new Point2(161, 45),
				new Point2(240, 45),
				new Point2(319, 45),
				new Point2(82, 148),
				new Point2(161, 148),
				new Point2(240, 148),
				new Point2(319, 148)
			},
			new Point2[]
			{
				new Point2(42, 45),
				new Point2(121, 45),
				new Point2(201, 45),
				new Point2(280, 45),
				new Point2(359, 45),
				new Point2(82, 148),
				new Point2(161, 148),
				new Point2(240, 148),
				new Point2(319, 148)
			}
		};

		// Token: 0x04000595 RID: 1429
		private static Point2[][] MapBtnPosAr = new Point2[][]
		{
			new Point2[]
			{
				new Point2(30, 122),
				new Point2(162, 412),
				new Point2(636, 40),
				new Point2(340, 252)
			}
		};

		// Token: 0x04000596 RID: 1430
		private static Point2[][] MapBtnPosAr_NoCASTLE = new Point2[][]
		{
			new Point2[]
			{
				SelectDef.MapBtnPosAr[0][1],
				SelectDef.MapBtnPosAr[0][2],
				SelectDef.MapBtnPosAr[0][3]
			}
		};

		// Token: 0x04000597 RID: 1431
		private static Point2[][] MapBtnPosAr_NoHATTER = new Point2[][]
		{
			new Point2[]
			{
				SelectDef.MapBtnPosAr[0][0],
				SelectDef.MapBtnPosAr[0][2],
				SelectDef.MapBtnPosAr[0][3]
			}
		};

		// Token: 0x04000598 RID: 1432
		private static Point2[][] MapBtnPosAr_NoAMUSE = new Point2[][]
		{
			new Point2[]
			{
				SelectDef.MapBtnPosAr[0][0],
				SelectDef.MapBtnPosAr[0][1],
				SelectDef.MapBtnPosAr[0][3]
			}
		};

		// Token: 0x04000599 RID: 1433
		private static Point2[][] MapBtnPosAr_NoTOWER = new Point2[][]
		{
			new Point2[]
			{
				SelectDef.MapBtnPosAr[0][1],
				SelectDef.MapBtnPosAr[0][2]
			},
			new Point2[]
			{
				SelectDef.MapBtnPosAr[0][0],
				SelectDef.MapBtnPosAr[0][1],
				SelectDef.MapBtnPosAr[0][2]
			}
		};

		// Token: 0x0400059A RID: 1434
		private static readonly SelectDef.BTN_TABLE[] g_SelectBtnTable = new SelectDef.BTN_TABLE[]
		{
			new SelectDef.BTN_TABLE("normal_text", SelectDef.TxtBtnPosAr, true),
			new SelectDef.BTN_TABLE("normal_grp", SelectDef.GrpBtnPosAr, true),
			new SelectDef.BTN_TABLE("map_grp", SelectDef.MapBtnPosAr, false),
			new SelectDef.BTN_TABLE("map_no_castle_grp", SelectDef.MapBtnPosAr_NoCASTLE, false),
			new SelectDef.BTN_TABLE("map_no_hatter_grp", SelectDef.MapBtnPosAr_NoHATTER, false),
			new SelectDef.BTN_TABLE("map_no_amuse_grp", SelectDef.MapBtnPosAr_NoAMUSE, false),
			new SelectDef.BTN_TABLE("map_no_tower_grp", SelectDef.MapBtnPosAr_NoTOWER, false)
		};

		// Token: 0x0400059B RID: 1435
		private static Dictionary<string, SelectTable> m_PosTable = new Dictionary<string, SelectTable>();

		// Token: 0x020000DA RID: 218
		public struct BTN_TABLE
		{
			// Token: 0x06000602 RID: 1538 RVA: 0x00018F4C File Offset: 0x0001714C
			public BTN_TABLE(string name_, Point2[][] table_, bool isScalePsp = true)
			{
				this.name = name_;
				this.table = table_;
				this.isScale = isScalePsp;
			}

			// Token: 0x0400059C RID: 1436
			public string name;

			// Token: 0x0400059D RID: 1437
			public Point2[][] table;

			// Token: 0x0400059E RID: 1438
			public bool isScale;
		}
	}
}
