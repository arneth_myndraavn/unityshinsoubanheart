﻿using System;
using Qoo.Game;

namespace Qoo.Def
{
	// Token: 0x020000CE RID: 206
	public class GameDef
	{
		// Token: 0x060005F2 RID: 1522 RVA: 0x00018310 File Offset: 0x00016510
		public static bool InitGameDataTbl()
		{
			GameData.Clear();
			foreach (GameDef.GAME_PARAM_TABLE game_PARAM_TABLE in GameDef.g_GameParamTable)
			{
				if (game_PARAM_TABLE.m_isStringParam)
				{
					GameData.AttachParam(game_PARAM_TABLE.m_Name, game_PARAM_TABLE.m_strInit, (!game_PARAM_TABLE.m_isCharArray) ? null : Chara.GetCharNameArray());
				}
				else
				{
					GameData.AttachParam(game_PARAM_TABLE.m_Name, game_PARAM_TABLE.m_nInit, (!game_PARAM_TABLE.m_isCharArray) ? null : Chara.GetCharNameArray());
				}
			}
			return true;
		}

		// Token: 0x040004E9 RID: 1257
		public const string INIT_FIRST_NAME = "アリス";

		// Token: 0x040004EA RID: 1258
		public const string INIT_FAMILY_NAME = "リデル";

		// Token: 0x040004EB RID: 1259
		public const int INIT_FIRST_MAX = 7;

		// Token: 0x040004EC RID: 1260
		public const int DEF_FONTSIZE = 28;

		// Token: 0x040004ED RID: 1261
		public const string PARAM_NAME_FIRSTNAME = "firstname";

		// Token: 0x040004EE RID: 1262
		public const string PARAM_NAME_FAMILYNAME = "familyname";

		// Token: 0x040004EF RID: 1263
		public const string PARAM_NAME_SCENE = "scene";

		// Token: 0x040004F0 RID: 1264
		public const string PARAM_NAME_MEMORY = "memory";

		// Token: 0x040004F1 RID: 1265
		public const string PARAM_NAME_MAP_BG = "map_bg";

		// Token: 0x040004F2 RID: 1266
		public const string PARAM_NAME_KARMA = "karma";

		// Token: 0x040004F3 RID: 1267
		public const string PARAM_NAME_ROUTE = "route";

		// Token: 0x040004F4 RID: 1268
		public const string PARAM_NAME_SCENARIO_CHAR = "scenario_char";

		// Token: 0x040004F5 RID: 1269
		public const string PARAM_NAME_SCENARIO_NO = "scenario_no";

		// Token: 0x040004F6 RID: 1270
		public const string PARAM_NAME_RAND = "rand";

		// Token: 0x040004F7 RID: 1271
		public const string PARAM_NAME_RESULT = "result";

		// Token: 0x040004F8 RID: 1272
		public const string PARAM_NAME_MAP = "map";

		// Token: 0x040004F9 RID: 1273
		public const string PARAM_NAME_STAY = "stay";

		// Token: 0x040004FA RID: 1274
		public const string PARAM_NAME_LOVE = "love";

		// Token: 0x040004FB RID: 1275
		public const string PARAM_NAME_MONEY = "money";

		// Token: 0x040004FC RID: 1276
		public const string PARAM_NAME_MONEY_FULL = "moneyfull";

		// Token: 0x040004FD RID: 1277
		public const string PARAM_NAME_VOICE_CHECK = "voicecheck";

		// Token: 0x040004FE RID: 1278
		private static readonly GameDef.GAME_PARAM_TABLE[] g_GameParamTable = new GameDef.GAME_PARAM_TABLE[]
		{
			UnityApp.Instance.isJapanese? new GameDef.GAME_PARAM_TABLE("firstname", "アリス", false) : new GameDef.GAME_PARAM_TABLE("firstname", "Alice", false),
			UnityApp.Instance.isJapanese? new GameDef.GAME_PARAM_TABLE("familyname", "リデル", false) : new GameDef.GAME_PARAM_TABLE("familyname", "Liddell", false),
			new GameDef.GAME_PARAM_TABLE("scene", string.Empty, false),
			new GameDef.GAME_PARAM_TABLE("memory", string.Empty, false),
			new GameDef.GAME_PARAM_TABLE("map_bg", string.Empty, false),
			new GameDef.GAME_PARAM_TABLE("karma", 0, false),
			new GameDef.GAME_PARAM_TABLE("route", string.Empty, false),
			new GameDef.GAME_PARAM_TABLE("scenario_char", string.Empty, false),
			new GameDef.GAME_PARAM_TABLE("scenario_no", string.Empty, false),
			new GameDef.GAME_PARAM_TABLE("rand", 0, false),
			new GameDef.GAME_PARAM_TABLE("result", 0, false),
			new GameDef.GAME_PARAM_TABLE("map", string.Empty, false),
			new GameDef.GAME_PARAM_TABLE("stay", string.Empty, false),
			new GameDef.GAME_PARAM_TABLE("love", 0, true),
			new GameDef.GAME_PARAM_TABLE("money", 0, true),
			new GameDef.GAME_PARAM_TABLE("moneyfull", 0, false),
			new GameDef.GAME_PARAM_TABLE("voicecheck", 0, false)
		};

		// Token: 0x020000CF RID: 207
		public struct GAME_PARAM_TABLE
		{
			// Token: 0x060005F3 RID: 1523 RVA: 0x000183B4 File Offset: 0x000165B4
			public GAME_PARAM_TABLE(string strName, string strInit, bool IsCharArray = false)
			{
				this.m_Name = strName;
				this.m_isStringParam = true;
				this.m_nInit = 0;
				this.m_strInit = strInit;
				this.m_isCharArray = IsCharArray;
			}

			// Token: 0x060005F4 RID: 1524 RVA: 0x000183DC File Offset: 0x000165DC
			public GAME_PARAM_TABLE(string strName, int nInit, bool IsCharArray = false)
			{
				this.m_Name = strName;
				this.m_isStringParam = false;
				this.m_nInit = nInit;
				this.m_strInit = string.Empty;
				this.m_isCharArray = IsCharArray;
			}

			// Token: 0x040004FF RID: 1279
			public string m_Name;

			// Token: 0x04000500 RID: 1280
			public bool m_isStringParam;

			// Token: 0x04000501 RID: 1281
			public int m_nInit;

			// Token: 0x04000502 RID: 1282
			public string m_strInit;

			// Token: 0x04000503 RID: 1283
			public bool m_isCharArray;
		}
	}
}
