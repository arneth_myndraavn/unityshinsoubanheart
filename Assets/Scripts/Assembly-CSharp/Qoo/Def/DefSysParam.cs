﻿using System;
using System.Collections.Generic;

namespace Qoo.Def
{
	// Token: 0x02000011 RID: 17
	public static class DefSysParam
	{
		// Token: 0x06000054 RID: 84 RVA: 0x00002F98 File Offset: 0x00001198
		static DefSysParam()
		{
			DefSysParam.Default.Add(SYSTEM_IDX.CLEAR_NUM, 0);
			DefSysParam.Default.Add(SYSTEM_IDX.READ_BGM, 0);
			DefSysParam.Default.Add(SYSTEM_IDX.VOL_SYS, 3);
			DefSysParam.Default.Add(SYSTEM_IDX.VOL_BGM, 3);
			DefSysParam.Default.Add(SYSTEM_IDX.VOL_SE, 3);
			DefSysParam.Default.Add(SYSTEM_IDX.ENABLE_VOICE, 1);
			DefSysParam.Default.Add(SYSTEM_IDX.VOL_CHR00, 5);
			DefSysParam.Default.Add(SYSTEM_IDX.VOL_CHR01, 5);
			DefSysParam.Default.Add(SYSTEM_IDX.VOL_CHR02, 5);
			DefSysParam.Default.Add(SYSTEM_IDX.VOL_CHR03, 5);
			DefSysParam.Default.Add(SYSTEM_IDX.VOL_CHR04, 5);
			DefSysParam.Default.Add(SYSTEM_IDX.VOL_CHR05, 5);
			DefSysParam.Default.Add(SYSTEM_IDX.VOL_CHR06, 5);
			DefSysParam.Default.Add(SYSTEM_IDX.VOL_CHR07, 5);
			DefSysParam.Default.Add(SYSTEM_IDX.VOL_CHR08, 5);
			DefSysParam.Default.Add(SYSTEM_IDX.VOL_CHR09, 5);
			DefSysParam.Default.Add(SYSTEM_IDX.VOL_CHR10, 5);
			DefSysParam.Default.Add(SYSTEM_IDX.VOL_CHR11, 5);
			DefSysParam.Default.Add(SYSTEM_IDX.VOL_CHR12, 5);
			DefSysParam.Default.Add(SYSTEM_IDX.VOL_CHR13, 5);
			DefSysParam.Default.Add(SYSTEM_IDX.VOL_CHR14, 5);
			DefSysParam.Default.Add(SYSTEM_IDX.VOL_CHR15, 5);
			DefSysParam.Default.Add(SYSTEM_IDX.SKIP, 1);
			DefSysParam.Default.Add(SYSTEM_IDX.AUTO_PAGE_TIME, 1);
			DefSysParam.Default.Add(SYSTEM_IDX.TEXT_SPPED, 1);
			DefSysParam.Default.Add(SYSTEM_IDX.FONT_TYPE, 0);
			DefSysParam.Default.Add(SYSTEM_IDX.READ_TEXT_COLOR, 1);
			DefSysParam.Default.Add(SYSTEM_IDX.PASSAGE_ANIM, 1);
			DefSysParam.Default.Add(SYSTEM_IDX.LOVE_ANIM, 0);
			DefSysParam.Default.Add(SYSTEM_IDX.SKIP_COTINUE, 1);
			DefSysParam.Default.Add(SYSTEM_IDX.ENABLE_JUMP, 0);
			DefSysParam.Default.Add(SYSTEM_IDX.ENABLE_FACE, 1);
			DefSysParam.Default.Add(SYSTEM_IDX.TIME, 0);
			DefSysParam.Default.Add(SYSTEM_IDX.PAY_ROUTE_CHR00, 0);
			DefSysParam.Default.Add(SYSTEM_IDX.PAY_ROUTE_CHR01, 0);
			DefSysParam.Default.Add(SYSTEM_IDX.PAY_ROUTE_CHR02, 0);
			DefSysParam.Default.Add(SYSTEM_IDX.PAY_ROUTE_CHR03, 0);
			DefSysParam.Default.Add(SYSTEM_IDX.PAY_ROUTE_CHR04, 0);
			DefSysParam.Default.Add(SYSTEM_IDX.PAY_ROUTE_CHR05, 0);
			DefSysParam.Default.Add(SYSTEM_IDX.PAY_ROUTE_CHR06, 0);
			DefSysParam.Default.Add(SYSTEM_IDX.PAY_ROUTE_CHR07, 0);
			DefSysParam.Default.Add(SYSTEM_IDX.PAY_ROUTE_CHR08, 0);
			DefSysParam.Default.Add(SYSTEM_IDX.PAY_ROUTE_CHR09, 0);
			DefSysParam.Default.Add(SYSTEM_IDX.PAY_ROUTE_CHR10, 0);
			DefSysParam.Default.Add(SYSTEM_IDX.PAY_ROUTE_CHR11, 0);
			DefSysParam.Default.Add(SYSTEM_IDX.PAY_ROUTE_CHR12, 0);
			DefSysParam.Default.Add(SYSTEM_IDX.PAY_ROUTE_CHR13, 0);
			DefSysParam.Default.Add(SYSTEM_IDX.PAY_ROUTE_CHR14, 0);
			DefSysParam.Default.Add(SYSTEM_IDX.PAY_ROUTE_CHR15, 0);
			DefSysParam.Default.Add(SYSTEM_IDX.PAY_VOICE_CHR00, 0);
			DefSysParam.Default.Add(SYSTEM_IDX.PAY_VOICE_CHR01, 0);
			DefSysParam.Default.Add(SYSTEM_IDX.PAY_VOICE_CHR02, 0);
			DefSysParam.Default.Add(SYSTEM_IDX.PAY_VOICE_CHR03, 0);
			DefSysParam.Default.Add(SYSTEM_IDX.PAY_VOICE_CHR04, 0);
			DefSysParam.Default.Add(SYSTEM_IDX.PAY_VOICE_CHR05, 0);
			DefSysParam.Default.Add(SYSTEM_IDX.PAY_VOICE_CHR06, 0);
			DefSysParam.Default.Add(SYSTEM_IDX.PAY_VOICE_CHR07, 0);
			DefSysParam.Default.Add(SYSTEM_IDX.PAY_VOICE_CHR08, 0);
			DefSysParam.Default.Add(SYSTEM_IDX.PAY_VOICE_CHR09, 0);
			DefSysParam.Default.Add(SYSTEM_IDX.PAY_VOICE_CHR10, 0);
			DefSysParam.Default.Add(SYSTEM_IDX.PAY_VOICE_CHR11, 0);
			DefSysParam.Default.Add(SYSTEM_IDX.PAY_VOICE_CHR12, 0);
			DefSysParam.Default.Add(SYSTEM_IDX.PAY_VOICE_CHR13, 0);
			DefSysParam.Default.Add(SYSTEM_IDX.PAY_VOICE_CHR14, 0);
			DefSysParam.Default.Add(SYSTEM_IDX.PAY_VOICE_CHR15, 0);
			DefSysParam.Default.Add(SYSTEM_IDX.PAY_ROUTE_FULL, 0);
			DefSysParam.Default.Add(SYSTEM_IDX.PAY_VOICE_FULL, 0);
		}

		// Token: 0x17000011 RID: 17
		// (get) Token: 0x06000055 RID: 85 RVA: 0x00003310 File Offset: 0x00001510
		public static Dictionary<SYSTEM_IDX, int> Default
		{
			get
			{
				return DefSysParam.m_Default;
			}
		}

		// Token: 0x0400007C RID: 124
		public const int DEF_SYSTEM_DATA_CLEAR_NUM = 0;

		// Token: 0x0400007D RID: 125
		public const int DEF_SYSTEM_DATA_READ_BGM = 0;

		// Token: 0x0400007E RID: 126
		public const int DEF_SYSTEM_DATA_VOL_SYS = 3;

		// Token: 0x0400007F RID: 127
		public const int DEF_SYSTEM_DATA_VOL_BGM = 3;

		// Token: 0x04000080 RID: 128
		public const int DEF_SYSTEM_DATA_VOL_SE = 3;

		// Token: 0x04000081 RID: 129
		public const int DEF_SYSTEM_DATA_ENABLE_VOICE = 1;

		// Token: 0x04000082 RID: 130
		public const int DEF_SYSTEM_IDX_VOL = 5;

		// Token: 0x04000083 RID: 131
		public const int DEF_SYSTEM_DATA_SKIP = 1;

		// Token: 0x04000084 RID: 132
		public const int DEF_SYSTEM_DATA_AUTO_PAGE_TIME = 1;

		// Token: 0x04000085 RID: 133
		public const int DEF_SYSTEM_DATA_TEXT_SPPED = 1;

		// Token: 0x04000086 RID: 134
		public const int DEF_SYSTEM_DATA_FONT_TYPE = 0;

		// Token: 0x04000087 RID: 135
		public const int DEF_SYSTEM_DATA_READ_TEXT_COLOR = 1;

		// Token: 0x04000088 RID: 136
		public const int DEF_SYSTEM_DATA_PASSAGE_ANIM = 1;

		// Token: 0x04000089 RID: 137
		public const int DEF_SYSTEM_DATA_LOVE_ANIM = 0;

		// Token: 0x0400008A RID: 138
		public const int DEF_SYSTEM_DATA_SKIP_COTINUE = 1;

		// Token: 0x0400008B RID: 139
		public const int DEF_SYSTEM_DATA_ENABLE_JUMP = 0;

		// Token: 0x0400008C RID: 140
		public const int DEF_SYSTEM_DATA_ENABLE_FACE = 1;

		// Token: 0x0400008D RID: 141
		public const int DEF_SYSTEM_DATA_PAY = 0;

		// Token: 0x0400008E RID: 142
		public static Dictionary<SYSTEM_IDX, int> m_Default = new Dictionary<SYSTEM_IDX, int>();
	}
}
