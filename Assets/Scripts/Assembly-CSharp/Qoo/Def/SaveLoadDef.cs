﻿using System;

namespace Qoo.Def
{
	// Token: 0x020000D8 RID: 216
	public class SaveLoadDef
	{
        void Awake()
        {
			if (!UnityApp.Instance.isJapanese)
			{
				MSG_SAVE_CONFIRM = "Do you want to save?";
				MSG_OVER_CONFIRM = "Do you want to overwrite this save？";
				MSG_LOAD_CONFIRM = "Do you want to load this save？";
			}
		}

		// Token: 0x04000565 RID: 1381
		public const string DLG_CG = "screen/common/sys_dialog";

		// Token: 0x04000566 RID: 1382
		public const string DLG_Y_CG = "screen/common/dlog_btn_y";

		// Token: 0x04000567 RID: 1383
		public const string DLG_N_CG = "screen/common/dlog_btn_n";

		// Token: 0x04000568 RID: 1384
		public const int DLG_MSG_X = 480;

		// Token: 0x04000569 RID: 1385
		public const int DLG_MSG_Y = 230;

		// Token: 0x0400056A RID: 1386
		public string MSG_SAVE_CONFIRM = "セーブしますか？";

		// Token: 0x0400056B RID: 1387
		public string MSG_OVER_CONFIRM = "上書きしますか？";

		// Token: 0x0400056C RID: 1388
		public string MSG_LOAD_CONFIRM = "ロードしますか？";
	}
}
