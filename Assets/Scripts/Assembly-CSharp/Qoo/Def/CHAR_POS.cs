﻿using System;

namespace Qoo.Def
{
	// Token: 0x020000D1 RID: 209
	public enum CHAR_POS
	{
		// Token: 0x0400050B RID: 1291
		CENTER,
		// Token: 0x0400050C RID: 1292
		LEFT,
		// Token: 0x0400050D RID: 1293
		RIGHT,
		// Token: 0x0400050E RID: 1294
		LEFT_CENTER,
		// Token: 0x0400050F RID: 1295
		RIGHT_CENTER,
		// Token: 0x04000510 RID: 1296
		NUM
	}
}
