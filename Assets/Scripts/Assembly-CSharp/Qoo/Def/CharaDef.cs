﻿using System;
using Game;
using Qoo.Game;

namespace Qoo.Def
{
	// Token: 0x020000C6 RID: 198
	public class CharaDef
	{
		// Token: 0x060005E7 RID: 1511 RVA: 0x00017F38 File Offset: 0x00016138
		public static void InitCharIdTable()
		{
			Chara.Clear();
			Chara.Add(CHAR_ID.BLOOD, CharaDef.CHARA_TABLE[0]);
			Chara.Add(CHAR_ID.ELLIOT, CharaDef.CHARA_TABLE[1]);
			Chara.Add(CHAR_ID.DEEDUM, CharaDef.CHARA_TABLE[2]);
			Chara.Add(CHAR_ID.VIVALDI, CharaDef.CHARA_TABLE[3]);
			Chara.Add(CHAR_ID.PETER, CharaDef.CHARA_TABLE[4]);
			Chara.Add(CHAR_ID.ACE, CharaDef.CHARA_TABLE[5]);
			Chara.Add(CHAR_ID.GOWLAND, CharaDef.CHARA_TABLE[6]);
			Chara.Add(CHAR_ID.BORIS, CharaDef.CHARA_TABLE[7]);
			Chara.Add(CHAR_ID.JULIUS, CharaDef.CHARA_TABLE[8]);
			Chara.Add(CHAR_ID.NIGHTMARE, CharaDef.CHARA_TABLE[9]);
			Chara.Add(CHAR_ID.MOB, CharaDef.CHARA_TABLE[10]);
		}

		// Token: 0x0400049D RID: 1181
		private static GAMECHAR_TABLE[] CHARA_TABLE = new GAMECHAR_TABLE[]
		{
			new GAMECHAR_TABLE("blood", "ブラッド＝デュプレ", "ブラッド", "blo", "blo"),
			new GAMECHAR_TABLE("elliot", "エリオット＝マーチ", "エリオット", "ell", "ell"),
			new GAMECHAR_TABLE("deedum", "ディー＆ダム", "ィー＆ダム", "dad", "dad"),
			new GAMECHAR_TABLE("vivaldi", "ビバルディ", "ビバルディ", "viv", "viv"),
			new GAMECHAR_TABLE("peter", "ペーター＝ホワイト", "ペーター", "pet", "pet"),
			new GAMECHAR_TABLE("ace", "エース", "エース", "ace", "ace"),
			new GAMECHAR_TABLE("gowland", "メリー＝ゴーランド", "ゴーランド", "gow", "gow"),
			new GAMECHAR_TABLE("boris", "ボリス＝エレイ", "ボリス", "bor", "bor"),
			new GAMECHAR_TABLE("julius", "ユリウス＝モンレー", "ユリウス", "jul", "jul"),
			new GAMECHAR_TABLE("nightmare", "ナイトメア", "ナイトメア", "nig", "nig"),
			new GAMECHAR_TABLE("other", "モブ", "モブ", null, null)
		};
	}
}
