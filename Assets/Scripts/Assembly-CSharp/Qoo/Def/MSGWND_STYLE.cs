﻿using System;

namespace Qoo.Def
{
	// Token: 0x020000D3 RID: 211
	public enum MSGWND_STYLE
	{
		// Token: 0x04000533 RID: 1331
		NULL = -1,
		// Token: 0x04000534 RID: 1332
		NORMAL,
		// Token: 0x04000535 RID: 1333
		KYARA,
		// Token: 0x04000536 RID: 1334
		TOGAKI,
		// Token: 0x04000537 RID: 1335
		HEROINE,
		// Token: 0x04000538 RID: 1336
		MONOLOGUE,
		// Token: 0x04000539 RID: 1337
		TEST,
		// Token: 0x0400053A RID: 1338
		MAX
	}
}
