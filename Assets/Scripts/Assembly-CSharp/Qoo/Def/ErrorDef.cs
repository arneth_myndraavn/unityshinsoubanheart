﻿using System;

namespace Qoo.Def
{
	// Token: 0x020000C8 RID: 200
	public class ErrorDef
	{
        void Awake()
        {
			ERROR_MSG_NO_GLYPH = "Contains characters that cannot be displayed";
			ERROR_MSG_MAX_OVER = "You can enter up to 7 characters for the name.";
			ERROR_MSG_NO_NAME = "No name has been entered.";
			ERROR_MSG_FAIL_CONNECT = "Connection failed. Try again later";
			ERROR_MSG_ASSERT_FORMAT = "We apologize for the inconvenience.\nA critical error has occurred.\n\nError Code ={0}\n{1}\n\nPlease send the above information to\n<ArnethMyndraavn@gmail.com>\n\nThank you";
			ERROR_MSG_UNKOWN = "Unexpected error";
			ERROR_MSG_FAIL_SAVE = "Failed to save game";
			ERROR_MSG_FAIL_LOAD = "Failed to load save file";
			CONFIRM_MSG_0 = "Is {0} {1} Correct?";
			CONFIRM_MSG_1 = "";

		}

		// Token: 0x040004A1 RID: 1185
		public const string DLG_CG = "sys_dialog";

		// Token: 0x040004A2 RID: 1186
		public const string DLG_Y_CG = "dlog_btn_y";

		// Token: 0x040004A3 RID: 1187
		public const string DLG_N_CG = "dlog_btn_n";

		// Token: 0x040004A4 RID: 1188
		public const int DLG_BTN_W = 1;

		// Token: 0x040004A5 RID: 1189
		public const int DLG_BTN_H = 2;

		// Token: 0x040004A6 RID: 1190
		public const int DLG_X = 480;

		// Token: 0x040004A7 RID: 1191
		public const int DLG_Y = 240;

		// Token: 0x040004A8 RID: 1192
		public const int DLG_Z = 1500;

		// Token: 0x040004A9 RID: 1193
		public const int DLG_BTN_Y_X = 176;

		// Token: 0x040004AA RID: 1194
		public const int DLG_BTN_Y_Y = 95;

		// Token: 0x040004AB RID: 1195
		public const int DLG_BTN_N_X = 377;

		// Token: 0x040004AC RID: 1196
		public const int DLG_BTN_N_Y = 95;

		// Token: 0x040004AD RID: 1197
		public const int DLG_BTN_Z = 1501;

		// Token: 0x040004AE RID: 1198
		public const int DLG_MSG_X = 140;

		// Token: 0x040004AF RID: 1199
		public const int DLG_MSG_Y = 50;

		// Token: 0x040004B0 RID: 1200
		public const int DLG_MSG_Z = 1501;

		// Token: 0x040004B1 RID: 1201
		public const int DLG_MSG_OFS_Y = 40;

		// Token: 0x040004B2 RID: 1202
		public string ERROR_MSG_NO_GLYPH = "表示できない文字が含まれています。";

		// Token: 0x040004B3 RID: 1203
		public string ERROR_MSG_MAX_OVER = "入力できる名前は７文字までです。";

		// Token: 0x040004B4 RID: 1204
		public string ERROR_MSG_NO_NAME = "名前が入力されていません。";

		// Token: 0x040004B5 RID: 1205
		public string ERROR_MSG_FAIL_CONNECT = "通信に失敗しました。通信状態の良いところで再度接続を試みてください。";

		// Token: 0x040004B6 RID: 1206
		public string ERROR_MSG_ASSERT_FORMAT = "ご迷惑をお掛けし、大変申し訳ございません。\n継続不能なエラーが発生しました。\n\nエラーコード={0}\n{1}\n\n上記のエラーコードを記載し、\n＜info@quinrose.com＞までご連絡ください。\n\nご協力のほどよろしくお願い申し上げます。";

		// Token: 0x040004B7 RID: 1207
		public string ERROR_MSG_UNKOWN = "予期せぬエラー";

		// Token: 0x040004B8 RID: 1208
		public string ERROR_MSG_FAIL_SAVE = "ゲームデータの保存を正常に終了できませんでした。";

		// Token: 0x040004B9 RID: 1209
		public string ERROR_MSG_FAIL_LOAD = "ゲームデータの読込を正常に終了できませんでした。";

		// Token: 0x040004BA RID: 1210
		public string CONFIRM_MSG_0 = "「{0}＝{1}」です。";

		// Token: 0x040004BB RID: 1211
		public string CONFIRM_MSG_1 = "よろしいですか？";

		// Token: 0x040004BC RID: 1212
		public const float WAIT_TIME = 1f;

		// Token: 0x020000C9 RID: 201
		public enum ASSERT_ERROR_CODE
		{
			// Token: 0x040004BE RID: 1214
			UNKNOWN,
			// Token: 0x040004BF RID: 1215
			FAIL_SAVE_CATCH_EXCEPTION,
			// Token: 0x040004C0 RID: 1216
			FAIL_SAVE_DATA_CREATE,
			// Token: 0x040004C1 RID: 1217
			FAIL_SAVE_HASHCODE_CREATE,
			// Token: 0x040004C2 RID: 1218
			FAIL_SAVE_HEADER_CREATE,
			// Token: 0x040004C3 RID: 1219
			FAIL_SAVE_RENAME,
			// Token: 0x040004C4 RID: 1220
			FAIL_LOAD,
			// Token: 0x040004C5 RID: 1221
			FAIL_LOAD_CATCH_EXCEPTION,
			// Token: 0x040004C6 RID: 1222
			UNMATCH_SAVE_HASH
		}
	}
}
