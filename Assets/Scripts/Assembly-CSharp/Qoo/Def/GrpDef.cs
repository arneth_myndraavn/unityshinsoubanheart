﻿using System;

namespace Qoo.Def
{
	// Token: 0x020000D0 RID: 208
	public class GrpDef
	{
		// Token: 0x04000504 RID: 1284
		public const int PspScreenW = 480;

		// Token: 0x04000505 RID: 1285
		public const int PspScreenH = 272;

		// Token: 0x04000506 RID: 1286
		public const float ScaleW = 2f;

		// Token: 0x04000507 RID: 1287
		public const float ScaleH = 2f;

		// Token: 0x04000508 RID: 1288
		public const int ScreenW = 960;

		// Token: 0x04000509 RID: 1289
		public const int ScreenH = 544;
	}
}
