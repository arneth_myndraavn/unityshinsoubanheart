﻿using System;

namespace Qoo.Def
{
	// Token: 0x0200000F RID: 15
	public enum SKIP_MODE
	{
		// Token: 0x04000033 RID: 51
		DISABLE,
		// Token: 0x04000034 RID: 52
		READ,
		// Token: 0x04000035 RID: 53
		ALL
	}
}
