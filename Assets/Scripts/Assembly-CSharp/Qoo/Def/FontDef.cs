﻿using System;

namespace Qoo.Def
{
	// Token: 0x020000CC RID: 204
	public class FontDef
	{
		// Token: 0x060005EE RID: 1518 RVA: 0x000180F0 File Offset: 0x000162F0
		public static FONTINFO GetFontInfo(MSGFONTSIZE eSize)
		{
			return FontDef.FontInfo[(int)eSize];
		}

        public static void SetFontInfo(FONTINFO[] newFontInfo)
        {
            FontDef.FontInfo = newFontInfo;
        }

        // Token: 0x040004D7 RID: 1239
        public const bool KERNING_MODE = true;

		// Token: 0x040004D8 RID: 1240
		public const int FONT_SMALL_FONT = 22;

		// Token: 0x040004D9 RID: 1241
		public const int FONT_SMALL_SIZE = 22;

		// Token: 0x040004DA RID: 1242
		public const float FONT_SMALL_STRETCH = 1f;

		// Token: 0x040004DB RID: 1243
		public const int FONT_NORMAL_FONT = 28;

		// Token: 0x040004DC RID: 1244
		public const int FONT_NORMAL_SIZE = 28;

		// Token: 0x040004DD RID: 1245
		public const float FONT_NORMAL_STRETCH = 1f;

		// Token: 0x040004DE RID: 1246
		public const int FONT_MIDDLE_FONT = 42;

		// Token: 0x040004DF RID: 1247
		public const int FONT_MIDDLE_SIZE = 42;

		// Token: 0x040004E0 RID: 1248
		public const float FONT_MIDDLE_STRETCH = 1f;

		// Token: 0x040004E1 RID: 1249
		public const int FONT_LARGE_FONT = 62;

		// Token: 0x040004E2 RID: 1250
		public const int FONT_LARGE_SIZE = 62;

		// Token: 0x040004E3 RID: 1251
		public const float FONT_LARGE_STRETCH = 1f;

		// Token: 0x040004E4 RID: 1252
		private static FONTINFO[] FontInfo = new FONTINFO[]
		{
			new FONTINFO(22, 22, 1f, 0),
			new FONTINFO(28, 28, 1f, 0),
			new FONTINFO(42, 42, 1f, -1),
			new FONTINFO(62, 62, 1f, -2)
		};
	}
}
