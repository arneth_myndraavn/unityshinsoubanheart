﻿using System;

namespace Qoo.Def
{
	// Token: 0x020000CA RID: 202
	public class FileDef
	{
		// Token: 0x040004C7 RID: 1223
		public const string NMB0_FILE_NAME = "data0.nmb";

		// Token: 0x040004C8 RID: 1224
		public const string NMB1_FILE_NAME = "data1.nmb";

		// Token: 0x040004C9 RID: 1225
		public const string NMB_FILE_PATH = "/nmb/";

		// Token: 0x040004CA RID: 1226
		public const string AKB_FILE_NAME = "kslist.akb";

		// Token: 0x040004CB RID: 1227
		public const string AKB_FILE_PATH = "/ks/";

		// Token: 0x040004CC RID: 1228
		public const string FONT_FILE_NAME = "h2m_font.fnt";

		// Token: 0x040004CD RID: 1229
		public const string FONT_FILE_PATH = "/font/";

		// Token: 0x040004CE RID: 1230
		public const string FONT_NAME = "FOT-ニューロダン Pro B";

		// Token: 0x040004CF RID: 1231
		public const string FONT_TEX_FILE_NAME = "h2m_font_0";

		// Token: 0x040004D0 RID: 1232
		public const string FONT_TEX_FILE_PATH = "font/";

		// Token: 0x040004D1 RID: 1233
		public static bool KS_LOAD_DIR_MODE = true;
	}
}
