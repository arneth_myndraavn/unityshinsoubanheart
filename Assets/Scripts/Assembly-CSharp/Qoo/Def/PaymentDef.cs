﻿using System;

namespace Qoo.Def
{
	// Token: 0x020000D7 RID: 215
	public class PaymentDef
	{

		// Token: 0x04000554 RID: 1364
		public const string APLICATION_ID = "";

		// Token: 0x04000555 RID: 1365
		public const string DLG_CG = "screen/common/sys_dialog";

		// Token: 0x04000556 RID: 1366
		public const int DLG_WND_W = 685;

		// Token: 0x04000557 RID: 1367
		public const int DLG_WND_H = 180;

		// Token: 0x04000558 RID: 1368
		public const int DLG_WND_X = 137;

		// Token: 0x04000559 RID: 1369
		public const int DLG_WND_Y = 182;

		// Token: 0x0400055A RID: 1370
		public const int DLG_MSG_X = 480;

		// Token: 0x0400055B RID: 1371
		public const int DLG_MSG_Y = 272;

		// Token: 0x0400055C RID: 1372
		public const float DLG_ENDTIME = 1f;

		//Should no longer be needed
		public string SCCESS_MSG_PURCHASE = "アドオンの購入が完了しました。";
		public string PROCESS_MSG_PURCHASE = "購入処理中です。";
		public string SCCESS_MSG_RENEWAL_LIST = "アドオンの購入履歴を更新しました。";
		public string PROCESS_MSG_RENEWAL_LIST = "アドオン購入履歴を更新中です。";
		public string SCCESS_MSG_RESTORE = "リストア（アドオン情報の復元）が完了しました。";
		public string PROCESS_MSG_RESTORE = "リストア処理中です。";
		public string ERROR_MSG_FAIL_CONNECT = "通信に失敗しました。\n通信状態の良いところで再度接続を試みてください。";
		public string ERROR_MSG_NOUSE_APP = "App内での購入が許可されていません。\n「機能制限」の設定画面から\n「App内での購入」をオンにしてください。";
	}
}
