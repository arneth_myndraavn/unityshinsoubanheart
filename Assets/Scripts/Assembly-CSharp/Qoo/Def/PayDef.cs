﻿using System;
using System.Collections.Generic;
using Game;
using Qoo.Game;

namespace Qoo.Def
{
	// Token: 0x020000D6 RID: 214
	public class PayDef
	{
		// Token: 0x060005FB RID: 1531 RVA: 0x0001857C File Offset: 0x0001677C
		static PayDef()
		{
			PayDef.m_Route.Clear();
			CHAR_ID[] value = new CHAR_ID[]
			{
				CHAR_ID.ACE
			};
			PayDef.m_Route.Add(CHAR_ID.ACE, value);
			CHAR_ID[] value2 = new CHAR_ID[1];
			PayDef.m_Route.Add(CHAR_ID.BLOOD, value2);
			CHAR_ID[] value3 = new CHAR_ID[]
			{
				CHAR_ID.BORIS
			};
			PayDef.m_Route.Add(CHAR_ID.BORIS, value3);
			CHAR_ID[] value4 = new CHAR_ID[]
			{
				CHAR_ID.DEEDUM
			};
			PayDef.m_Route.Add(CHAR_ID.DEEDUM, value4);
			CHAR_ID[] value5 = new CHAR_ID[]
			{
				CHAR_ID.ELLIOT
			};
			PayDef.m_Route.Add(CHAR_ID.ELLIOT, value5);
			CHAR_ID[] value6 = new CHAR_ID[]
			{
				CHAR_ID.GOWLAND
			};
			PayDef.m_Route.Add(CHAR_ID.GOWLAND, value6);
			CHAR_ID[] value7 = new CHAR_ID[]
			{
				CHAR_ID.JULIUS
			};
			PayDef.m_Route.Add(CHAR_ID.JULIUS, value7);
			CHAR_ID[] value8 = new CHAR_ID[]
			{
				CHAR_ID.PETER
			};
			PayDef.m_Route.Add(CHAR_ID.PETER, value8);
			CHAR_ID[] value9 = new CHAR_ID[]
			{
				CHAR_ID.VIVALDI
			};
			PayDef.m_Route.Add(CHAR_ID.VIVALDI, value9);
			CHAR_ID[] value10 = new CHAR_ID[0];
			PayDef.m_Route.Add(CHAR_ID.NIGHTMARE, value10);
			CHAR_ID[] value11 = new CHAR_ID[]
			{
				CHAR_ID.ACE
			};
			PayDef.m_Voice.Add(CHAR_ID.ACE, value11);
			CHAR_ID[] value12 = new CHAR_ID[1];
			PayDef.m_Voice.Add(CHAR_ID.BLOOD, value12);
			CHAR_ID[] value13 = new CHAR_ID[]
			{
				CHAR_ID.BORIS
			};
			PayDef.m_Voice.Add(CHAR_ID.BORIS, value13);
			CHAR_ID[] value14 = new CHAR_ID[]
			{
				CHAR_ID.DEEDUM
			};
			PayDef.m_Voice.Add(CHAR_ID.DEEDUM, value14);
			CHAR_ID[] value15 = new CHAR_ID[]
			{
				CHAR_ID.ELLIOT
			};
			PayDef.m_Voice.Add(CHAR_ID.ELLIOT, value15);
			CHAR_ID[] value16 = new CHAR_ID[]
			{
				CHAR_ID.GOWLAND
			};
			PayDef.m_Voice.Add(CHAR_ID.GOWLAND, value16);
			CHAR_ID[] value17 = new CHAR_ID[]
			{
				CHAR_ID.JULIUS
			};
			PayDef.m_Voice.Add(CHAR_ID.JULIUS, value17);
			CHAR_ID[] value18 = new CHAR_ID[]
			{
				CHAR_ID.PETER
			};
			PayDef.m_Voice.Add(CHAR_ID.PETER, value18);
			CHAR_ID[] value19 = new CHAR_ID[]
			{
				CHAR_ID.VIVALDI
			};
			PayDef.m_Voice.Add(CHAR_ID.VIVALDI, value19);
			CHAR_ID[] value20 = new CHAR_ID[]
			{
				CHAR_ID.ACE,
				CHAR_ID.BLOOD,
				CHAR_ID.BORIS,
				CHAR_ID.DEEDUM,
				CHAR_ID.ELLIOT,
				CHAR_ID.GOWLAND,
				CHAR_ID.JULIUS,
				CHAR_ID.PETER,
				CHAR_ID.VIVALDI
			};
			PayDef.m_Voice.Add(CHAR_ID.NIGHTMARE, value20);
			PayDef.m_Voice.Clear();
		}

		// Token: 0x060005FC RID: 1532 RVA: 0x000187B0 File Offset: 0x000169B0
		public static bool IsRoutePay(CHAR_ID id)
		{
			if (PayDef.m_Route.ContainsKey(id))
			{
				foreach (CHAR_ID idChar in PayDef.m_Route[id])
				{
					if (!SysData.IsPay(idChar))
					{
						return false;
					}
				}
			}
			return true;
		}

		// Token: 0x060005FD RID: 1533 RVA: 0x00018800 File Offset: 0x00016A00
		public static bool IsVoicePay(CHAR_ID id)
		{
			if (PayDef.m_Route.ContainsKey(id))
			{
				foreach (CHAR_ID idChar in PayDef.m_Route[id])
				{
					if (!SysData.IsPayVoice(idChar))
					{
						return false;
					}
				}
			}
			return true;
		}

		// Token: 0x04000552 RID: 1362
		private static Dictionary<CHAR_ID, CHAR_ID[]> m_Route = new Dictionary<CHAR_ID, CHAR_ID[]>();

		// Token: 0x04000553 RID: 1363
		private static Dictionary<CHAR_ID, CHAR_ID[]> m_Voice = new Dictionary<CHAR_ID, CHAR_ID[]>();
	}
}
