﻿using System;

namespace Qoo.Def
{
	// Token: 0x020000C7 RID: 199
	public class EffectDef
	{
		// Token: 0x0400049E RID: 1182
		public const string EFFECT_NAME_WAVE = "wave";

		// Token: 0x0400049F RID: 1183
		public const string EFFECT_NAME_RIPPLE = "ripple";

		// Token: 0x040004A0 RID: 1184
		public const string EFFECT_NAME_MOSAIC = "mosaic";
	}
}
