﻿using System;

namespace Qoo.Def
{
	// Token: 0x02000012 RID: 18
	public static class WndPri
	{
		// Token: 0x0400008F RID: 143
		public const int WND_Z_SCREEN_MAIN = 200;

		// Token: 0x04000090 RID: 144
		public const int WND_Z_SCREEN_SUB1 = 300;

		// Token: 0x04000091 RID: 145
		public const int WND_Z_SCREEN_SUB2 = 400;

		// Token: 0x04000092 RID: 146
		public const int WND_Z_MINI_MAIN = 500;

		// Token: 0x04000093 RID: 147
		public const int WND_Z_MINI_SUB = 600;

		// Token: 0x04000094 RID: 148
		public const int WND_Z_EVENT1 = 700;

		// Token: 0x04000095 RID: 149
		public const int WND_Z_MAP = 700;

		// Token: 0x04000096 RID: 150
		public const int WND_Z_EVENT2 = 800;

		// Token: 0x04000097 RID: 151
		public const int WND_Z_EVENTANIM = 850;

		// Token: 0x04000098 RID: 152
		public const int WND_Z_SYSMENU_1 = 1000;

		// Token: 0x04000099 RID: 153
		public const int WND_Z_SYSMENU_2 = 1100;

		// Token: 0x0400009A RID: 154
		public const int WND_Z_SYSMENU_3 = 1200;

		// Token: 0x0400009B RID: 155
		public const int WND_Z_DIALOG1 = 1300;

		// Token: 0x0400009C RID: 156
		public const int WND_Z_DIALOG2 = 1400;

		// Token: 0x0400009D RID: 157
		public const int WND_Z_DIALOG3 = 1500;

		// Token: 0x0400009E RID: 158
		public const int WND_Z_SCREENSHOT = 7000;

		// Token: 0x0400009F RID: 159
		public const int WND_Z_DEBUG1 = 8000;

		// Token: 0x040000A0 RID: 160
		public const int WND_Z_DEBUG2 = 8100;

		// Token: 0x040000A1 RID: 161
		public const int WND_Z_TITLELOGO = 200;

		// Token: 0x040000A2 RID: 162
		public const int WND_Z_MAINMENU = 200;

		// Token: 0x040000A3 RID: 163
		public const int WND_Z_DEBUGTITLE = 200;

		// Token: 0x040000A4 RID: 164
		public const int WND_Z_MEM_TITLE = 200;

		// Token: 0x040000A5 RID: 165
		public const int WND_Z_MEM_CHRSEL = 200;

		// Token: 0x040000A6 RID: 166
		public const int WND_Z_MEM_THUMSEL = 200;

		// Token: 0x040000A7 RID: 167
		public const int WND_Z_BGMPLAYER = 200;

		// Token: 0x040000A8 RID: 168
		public const int WND_Z_OMAKE = 200;

		// Token: 0x040000A9 RID: 169
		public const int WND_Z_BACKLOG = 1300;

		// Token: 0x040000AA RID: 170
		public const int WND_Z_REQMENU = 1400;

		// Token: 0x040000AB RID: 171
		public const int WND_Z_LOVESTATUS = 1200;

		// Token: 0x040000AC RID: 172
		public const int WND_Z_MAPSELECT = 700;

		// Token: 0x040000AD RID: 173
		public const int WND_Z_SAVRLOAD = 1500;

		// Token: 0x040000AE RID: 174
		public const int WND_Z_NAMESETTING = 1300;

		// Token: 0x040000AF RID: 175
		public const int WND_Z_DBG_POPUP = 8000;

		// Token: 0x040000B0 RID: 176
		public const int WND_Z_DBG_LOG = 8000;

		// Token: 0x040000B1 RID: 177
		public const int WND_Z_DBG_KSSELECT = 8000;

		// Token: 0x040000B2 RID: 178
		public const int SPR_Z_LAYER_12 = 905;

		// Token: 0x040000B3 RID: 179
		public const int SPR_Z_LAYER_MESSAGE = 800;

		// Token: 0x040000B4 RID: 180
		public const int SPR_Z_LAYER_11 = 795;

		// Token: 0x040000B5 RID: 181
		public const int SPR_Z_LAYER_10 = 790;

		// Token: 0x040000B6 RID: 182
		public const int SPR_Z_LAYER_09 = 785;

		// Token: 0x040000B7 RID: 183
		public const int SPR_Z_LAYER_CG = 780;

		// Token: 0x040000B8 RID: 184
		public const int SPR_Z_LAYER_08 = 775;

		// Token: 0x040000B9 RID: 185
		public const int SPR_Z_LAYER_CHARA_C = 770;

		// Token: 0x040000BA RID: 186
		public const int SPR_Z_LAYER_07 = 765;

		// Token: 0x040000BB RID: 187
		public const int SPR_Z_LAYER_CHARA_LC = 760;

		// Token: 0x040000BC RID: 188
		public const int SPR_Z_LAYER_06 = 755;

		// Token: 0x040000BD RID: 189
		public const int SPR_Z_LAYER_CHARA_RC = 750;

		// Token: 0x040000BE RID: 190
		public const int SPR_Z_LAYER_05 = 745;

		// Token: 0x040000BF RID: 191
		public const int SPR_Z_LAYER_CHARA_L = 740;

		// Token: 0x040000C0 RID: 192
		public const int SPR_Z_LAYER_04 = 735;

		// Token: 0x040000C1 RID: 193
		public const int SPR_Z_LAYER_CHARA_R = 730;

		// Token: 0x040000C2 RID: 194
		public const int SPR_Z_LAYER_03 = 725;

		// Token: 0x040000C3 RID: 195
		public const int SPR_Z_LAYER_02 = 720;

		// Token: 0x040000C4 RID: 196
		public const int SPR_Z_LAYER_01 = 715;

		// Token: 0x040000C5 RID: 197
		public const int SPR_Z_LAYER_BG = 710;

		// Token: 0x040000C6 RID: 198
		public const int SPR_Z_EVENT_BG = 710;

		// Token: 0x040000C7 RID: 199
		public const int SPR_Z_EVENT_CHARA = 730;

		// Token: 0x040000C8 RID: 200
		public const int SPR_Z_EVENT_CG = 780;

		// Token: 0x040000C9 RID: 201
		public const int SPR_Z_MESSAGE = 800;

		// Token: 0x040000CA RID: 202
		public const int SPR_Z_MESSAGE_TEXT = 801;

		// Token: 0x040000CB RID: 203
		public const int SPR_Z_MESSAGE_NAME = 802;

		// Token: 0x040000CC RID: 204
		public const int SPR_Z_MESSAGE_FACE = 803;

		// Token: 0x040000CD RID: 205
		public const int SPR_Z_SCREENSHOT = 7000;
	}
}
