﻿using System;

namespace Qoo.Def
{
	// Token: 0x020000D4 RID: 212
	public static class MsgDef
	{
		// Token: 0x060005F8 RID: 1528 RVA: 0x00018524 File Offset: 0x00016724
		static MsgDef()
		{
			CMessageText.IsHyphenation = false;
			CMessageText.EnabelLINETOP_PROC = true;
		}

		// Token: 0x0400053B RID: 1339
		public const int KEYCUR_OFF_X = 0;

		// Token: 0x0400053C RID: 1340
		public const int KEYCUR_OFF_Y = 0;

		// Token: 0x0400053D RID: 1341
		public const int KEYCUR_OFF_W = 0;

		// Token: 0x0400053E RID: 1342
		public const int KEYCUR_OFF_H = 2;

		// Token: 0x0400053F RID: 1343
		public const bool DEF_MSG_ANIM_POS_OUTER = true;

		// Token: 0x04000540 RID: 1344
		public const int WND_MSG_CHRNUM_X = 100;

		// Token: 0x04000541 RID: 1345
		public const int WND_MSG_CHRNUM_Y = 4;

		// Token: 0x04000542 RID: 1346
		public const int DEF_MSG_LINE_MARGIN = 4;

		// Token: 0x04000543 RID: 1347
		public const string DEF_MSG_NAME_PLATE_CG = "nam_common";

		// Token: 0x04000544 RID: 1348
		public const int DEF_MSG_NAME_STRING_AX = 52;

		// Token: 0x04000545 RID: 1349
		public const int DEF_MSG_NAME_STRING_AY = 2;

		// Token: 0x04000546 RID: 1350
		public const int DEF_MSG_NAME_STRING_AZ = 1;

		// Token: 0x04000547 RID: 1351
		public const int HIDE_BTN_X = 899;

		// Token: 0x04000548 RID: 1352
		public const int HIDE_BTN_Y = 318;

		// Token: 0x04000549 RID: 1353
		public const int HIDE_BTN_Z = 800;

		// Token: 0x0400054A RID: 1354
		public const string HIDE_BTN_CG = "cancel";

		// Token: 0x0400054B RID: 1355
		public const int DEF_MSG_NAME_MAX = 20;

		// Token: 0x0400054C RID: 1356
		public const bool DEF_IS_HYPHENATION = false;

		// Token: 0x0400054D RID: 1357
		public const bool DEF_IS_LINEPROC = true;

		// Token: 0x0400054E RID: 1358
		public static char[] SP_CODES = new char[]
		{
			'\u3000',
			'、',
			'。',
			'，',
			'．',
			'・',
			'？',
			'！',
			'゛',
			'゜',
			'ヽ',
			'ヾ',
			'ゝ',
			'ゞ',
			'々',
			'ー',
			'～',
			'）',
			'］',
			'｝',
			'」',
			'』',
			'〕',
			'〉',
			'》',
			'】',
			'〟',
			'’',
			'”',
			'ぁ',
			'ぃ',
			'ぅ',
			'ぇ',
			'ぉ',
			'っ',
			'ゃ',
			'ゅ',
			'ょ',
			'ゎ',
			'ァ',
			'ィ',
			'ゥ',
			'ェ',
			'ォ',
			'ヵ',
			'ヶ',
			'ッ',
			'ャ',
			'ュ',
			'ョ',
			'ヮ',
			',',
			'.',
			'-',
			')',
			'"',
			'\'',
			'`',
			' '
		};

		// Token: 0x0400054F RID: 1359
		public static char[] LINETOP_CODES = new char[]
		{
			'「',
			'『',
			'《',
			'【',
			'（',
			'［',
			'(',
			'[',
			'{'
		};
	}
}
