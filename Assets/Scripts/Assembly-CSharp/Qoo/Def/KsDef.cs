﻿using System;

namespace Qoo.Def
{
	// Token: 0x020000D2 RID: 210
	public class KsDef
	{
		// Token: 0x04000511 RID: 1297
		public const string INIT_KS_NAME = "prologue.ks";

		// Token: 0x04000512 RID: 1298
		public const string MAIN_KS_NAME = "honpen.ks";

		// Token: 0x04000513 RID: 1299
		public const string TRIAL_KS_NAME = "op00_trial.ks";

		// Token: 0x04000514 RID: 1300
		public const int CGMAX = 4;

		// Token: 0x04000515 RID: 1301
		public const int CHRMAX = 5;

		// Token: 0x04000516 RID: 1302
		public const int SEMAX = 2;

		// Token: 0x04000517 RID: 1303
		public const int MSG_NAME_MAX = 2;

		// Token: 0x04000518 RID: 1304
		public const int WORKMAX = 2;

		// Token: 0x04000519 RID: 1305
		public const int KSNUM = 1000;

		// Token: 0x0400051A RID: 1306
		public const int TAGMAX = 1536;

		// Token: 0x0400051B RID: 1307
		public const int MSGTAGMAX = 1024;

		// Token: 0x0400051C RID: 1308
		public const int PARAMMAX = 3096;

		// Token: 0x0400051D RID: 1309
		public const int MSGMAX = 2048;

		// Token: 0x0400051E RID: 1310
		public const int TEXTSIZE = 131072;

		// Token: 0x0400051F RID: 1311
		public const int LABELMAX = 200;

		// Token: 0x04000520 RID: 1312
		public const int REWINDMAX = 4096;

		// Token: 0x04000521 RID: 1313
		public const int CALLLOGMAX = 512;

		// Token: 0x04000522 RID: 1314
		public const int KSMAX = 20;

		// Token: 0x04000523 RID: 1315
		public const int SAVEOLDREWINDMAX = 1024;

		// Token: 0x04000524 RID: 1316
		public const int LOGMAX = 64;

		// Token: 0x04000525 RID: 1317
		public const string MARK_FRONT_CG = "sys_skip_ff";

		// Token: 0x04000526 RID: 1318
		public const string MARK_BACK_CG = "sys_skip_rew";

		// Token: 0x04000527 RID: 1319
		public const int MARK_FRONT_X = 880;

		// Token: 0x04000528 RID: 1320
		public const int MARK_FRONT_Y = 0;

		// Token: 0x04000529 RID: 1321
		public const int MARK_BACK_X = 880;

		// Token: 0x0400052A RID: 1322
		public const int MARK_BACK_Y = 0;

		// Token: 0x0400052B RID: 1323
		public const int MARK_PAT_W_NUM = 4;

		// Token: 0x0400052C RID: 1324
		public const int MARK_PAT_H_NUM = 2;

		// Token: 0x0400052D RID: 1325
		public const int MARK_Z = 910;

		// Token: 0x0400052E RID: 1326
		public const int MARK_PAT_TIME = 8;

		// Token: 0x0400052F RID: 1327
		public const int SKIPMARK_WAIT = 5;

		// Token: 0x04000530 RID: 1328
		public static Point3[] CharPos = new Point3[]
		{
			new Point3(0, 0, 770),
			new Point3(-106, 0, 740),
			new Point3(494, 0, 730),
			new Point3(-150, 0, 760),
			new Point3(278, 0, 750)
		};

		// Token: 0x04000531 RID: 1329
		public static string[] INIT_TEX_NAME_LIST = new string[]
		{
			"pagebreak_a",
			"pagebreak_b",
			"frame_togaki",
			"frame_kyara",
			"frame_heroine",
			"frame_monologue",
			"nam_common",
			"sel_kidoku.png",
			"select_bar.png",
			"sys_love.png",
			"jimon",
			"sys_skip_ff",
			"sys_skip_rew",
			"cancel",
			"sys_dialog",
			"dlog_btn_y",
			"dlog_btn_n"
		};
	}
}
