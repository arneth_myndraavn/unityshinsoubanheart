﻿using System;

namespace Qoo.Def
{
	// Token: 0x020000C4 RID: 196
	public class BackLogDef
	{
		// Token: 0x04000488 RID: 1160
		public const int FONT_SIZE = 28;

		// Token: 0x04000489 RID: 1161
		public const int POS_X_NAME = 128;

		// Token: 0x0400048A RID: 1162
		public const int POS_X_MES = 204;

		// Token: 0x0400048B RID: 1163
		public const int POS_Y_TEXT = 4;

		// Token: 0x0400048C RID: 1164
		public const int VOICEMARK_X = 64;

		// Token: 0x0400048D RID: 1165
		public const int DISPLINE_NUM = 16;

		// Token: 0x0400048E RID: 1166
		public const string HIDE_BTN_CG = "cancel";
	}
}
