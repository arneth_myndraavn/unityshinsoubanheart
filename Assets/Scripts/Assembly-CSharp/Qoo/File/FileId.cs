﻿using System;

namespace Qoo.File
{
	// Token: 0x02000013 RID: 19
	public class FileId
	{
		// Token: 0x06000056 RID: 86 RVA: 0x00003318 File Offset: 0x00001518
		public FileId(string name = "")
		{
			this.Set(name);
		}

		// Token: 0x17000012 RID: 18
		// (get) Token: 0x06000057 RID: 87 RVA: 0x00003328 File Offset: 0x00001528
		// (set) Token: 0x06000058 RID: 88 RVA: 0x00003330 File Offset: 0x00001530
		public string Name { get; private set; }

		// Token: 0x06000059 RID: 89 RVA: 0x0000333C File Offset: 0x0000153C
		public static string Normalize(string name)
		{
			int num = name.IndexOf('.');
			if (num >= 0)
			{
				name = name.Substring(0, num);
			}
			return name.ToLower();
		}

		// Token: 0x0600005A RID: 90 RVA: 0x0000336C File Offset: 0x0000156C
		public bool Set(string name)
		{
			this.Name = FileId.Normalize(name);
			return this.Name.Length > 0;
		}
	}
}
