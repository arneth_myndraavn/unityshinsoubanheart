﻿using System;

namespace Qoo.File
{
	// Token: 0x02000014 RID: 20
	public struct NMB_FILEHEADER
	{
		// Token: 0x040000CF RID: 207
		public uint FourCC;

		// Token: 0x040000D0 RID: 208
		public int nItemNum;

		// Token: 0x040000D1 RID: 209
		public int nSearchTop;

		// Token: 0x040000D2 RID: 210
		public int nStrSize;
	}
}
