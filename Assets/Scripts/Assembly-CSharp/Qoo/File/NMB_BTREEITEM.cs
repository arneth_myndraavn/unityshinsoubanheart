﻿using System;

namespace Qoo.File
{
	// Token: 0x02000016 RID: 22
	public struct NMB_BTREEITEM
	{
		// Token: 0x040000DB RID: 219
		public uint nKey;

		// Token: 0x040000DC RID: 220
		public int nIndex;

		// Token: 0x040000DD RID: 221
		public int nL;

		// Token: 0x040000DE RID: 222
		public int nR;

		// Token: 0x040000DF RID: 223
		public int nE;
	}
}
