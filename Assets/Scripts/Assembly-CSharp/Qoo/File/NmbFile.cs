﻿using System;
using System.Collections.Generic;
using Qoo.Memory;

namespace Qoo.File
{
	// Token: 0x02000017 RID: 23
	internal class NmbFile
	{
		// Token: 0x17000013 RID: 19
		// (get) Token: 0x0600005D RID: 93 RVA: 0x000033E0 File Offset: 0x000015E0
		public int No
		{
			get
			{
				return this.m_nNo;
			}
		}

		// Token: 0x0600005E RID: 94 RVA: 0x000033E8 File Offset: 0x000015E8
		public bool Create(byte[] data_, int no)
		{
			this.Release();
			this.m_nNo = no;
			MemFile memFile = new MemFile(data_);
			this.m_Header.FourCC = memFile.GetUInt32();
			this.m_Header.nItemNum = memFile.GetInt32();
			this.m_Header.nSearchTop = memFile.GetInt32();
			this.m_Header.nStrSize = memFile.GetInt32();
			for (int num = 0; num != this.m_Header.nItemNum; num++)
			{
				NMB_FILEINFO nmb_FILEINFO = new NMB_FILEINFO();
				nmb_FILEINFO.nStrName = memFile.GetInt32();
				nmb_FILEINFO.nStrDir = memFile.GetInt32();
				nmb_FILEINFO.nStrFile = memFile.GetInt32();
				nmb_FILEINFO.nCRC = (uint)memFile.GetUInt16();
				nmb_FILEINFO.nSUM = (uint)memFile.GetUInt16();
				this.m_FileInfo.Add(nmb_FILEINFO);
			}
			for (int num2 = 0; num2 != this.m_Header.nItemNum; num2++)
			{
				NMB_BTREEITEM item = default(NMB_BTREEITEM);
				item.nKey = memFile.GetUInt32();
				item.nIndex = (int)memFile.GetInt16();
				item.nL = (int)memFile.GetInt16();
				item.nR = (int)memFile.GetInt16();
				item.nE = (int)memFile.GetInt16();
				this.m_TreeItem.Add(item);
			}
			memFile.SetPos(memFile.Length() - this.m_Header.nStrSize);
			string @string = memFile.GetString(this.m_Header.nStrSize);
			Func<string, int, string> func = delegate(string str, int pos)
			{
				int num4 = str.IndexOf('\0', pos);
				return str.Substring(pos, num4 - pos);
			};
			for (int num3 = 0; num3 != this.m_Header.nItemNum; num3++)
			{
				NMB_FILEINFO nmb_FILEINFO2 = this.m_FileInfo[num3];
				nmb_FILEINFO2.StrName = func(@string, nmb_FILEINFO2.nStrName).ToLower();
				nmb_FILEINFO2.DirName = func(@string, nmb_FILEINFO2.nStrDir).ToLower();
				nmb_FILEINFO2.FileName = func(@string, nmb_FILEINFO2.nStrFile).ToLower();
				this.m_FileInfo[num3] = nmb_FILEINFO2;
				if (this.m_FileDictionary.ContainsKey(nmb_FILEINFO2.StrName))
				{
					Debug.Print(string.Format("ERROR:NMB ITEM :{0}/{1}", nmb_FILEINFO2.DirName, nmb_FILEINFO2.StrName));
				}
				else
				{
					this.m_FileDictionary.Add(nmb_FILEINFO2.StrName, num3);
				}
			}
			return true;
		}

		// Token: 0x0600005F RID: 95 RVA: 0x00003654 File Offset: 0x00001854
		public bool Release()
		{
			this.m_Header = default(NMB_FILEHEADER);
			this.m_FileDictionary.Clear();
			this.m_FileInfo.Clear();
			this.m_TreeItem.Clear();
			return true;
		}

		// Token: 0x06000060 RID: 96 RVA: 0x00003694 File Offset: 0x00001894
		public int GetFileNum()
		{
			return this.m_Header.nItemNum;
		}

		// Token: 0x06000061 RID: 97 RVA: 0x000036A4 File Offset: 0x000018A4
		public NMB_FILEINFO GetFileInfo(int nFileNo)
		{
			return this.m_FileInfo[nFileNo];
		}

		// Token: 0x06000062 RID: 98 RVA: 0x000036B4 File Offset: 0x000018B4
		public NMB_FILEINFO GetFileInfo(string szName)
		{
			szName = szName.ToLower();
			if (this.m_FileDictionary.ContainsKey(szName))
			{
				return this.m_FileInfo[this.m_FileDictionary[szName]];
			}
			return null;
		}

		// Token: 0x06000063 RID: 99 RVA: 0x000036F4 File Offset: 0x000018F4
		public int NameToNo(string szName)
		{
			szName = szName.ToLower();
			if (this.m_FileDictionary.ContainsKey(szName))
			{
				return this.m_FileDictionary[szName];
			}
			return -1;
		}

		// Token: 0x040000E0 RID: 224
		private NMB_FILEHEADER m_Header = default(NMB_FILEHEADER);

		// Token: 0x040000E1 RID: 225
		private List<NMB_FILEINFO> m_FileInfo = new List<NMB_FILEINFO>();

		// Token: 0x040000E2 RID: 226
		private List<NMB_BTREEITEM> m_TreeItem = new List<NMB_BTREEITEM>();

		// Token: 0x040000E3 RID: 227
		private Dictionary<string, int> m_FileDictionary = new Dictionary<string, int>();

		// Token: 0x040000E4 RID: 228
		private int m_nNo;
	}
}
