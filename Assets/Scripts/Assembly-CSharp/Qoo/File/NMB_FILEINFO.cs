﻿using System;

namespace Qoo.File
{
	// Token: 0x02000015 RID: 21
	public class NMB_FILEINFO
	{
		// Token: 0x040000D3 RID: 211
		public string StrName;

		// Token: 0x040000D4 RID: 212
		public string DirName;

		// Token: 0x040000D5 RID: 213
		public string FileName;

		// Token: 0x040000D6 RID: 214
		public int nStrName;

		// Token: 0x040000D7 RID: 215
		public int nStrDir;

		// Token: 0x040000D8 RID: 216
		public int nStrFile;

		// Token: 0x040000D9 RID: 217
		public uint nCRC;

		// Token: 0x040000DA RID: 218
		public uint nSUM;
	}
}
