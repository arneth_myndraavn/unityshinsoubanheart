﻿using System;
using System.Collections.Generic;

namespace Qoo.File
{
	// Token: 0x02000018 RID: 24
	public static class Nmb
	{
		// Token: 0x06000066 RID: 102 RVA: 0x0000374C File Offset: 0x0000194C
		public static bool Add(byte[] data_)
		{
			NmbFile nmbFile = new NmbFile();
			bool flag = nmbFile.Create(data_, Nmb.m_NmbFile.Count);
			if (flag)
			{
				Nmb.m_NmbFile.Add(nmbFile);
			}
			return flag;
		}

		// Token: 0x06000067 RID: 103 RVA: 0x00003784 File Offset: 0x00001984
		public static bool Clear()
		{
			foreach (NmbFile nmbFile in Nmb.m_NmbFile)
			{
				if (nmbFile != null)
				{
					nmbFile.Release();
				}
			}
			Nmb.m_NmbFile.Clear();
			return true;
		}

		// Token: 0x06000068 RID: 104 RVA: 0x000037FC File Offset: 0x000019FC
		public static int GetFileNum()
		{
			int num = 0;
			foreach (NmbFile nmbFile in Nmb.m_NmbFile)
			{
				num += nmbFile.GetFileNum();
			}
			return num;
		}

		// Token: 0x06000069 RID: 105 RVA: 0x00003868 File Offset: 0x00001A68
		public static NMB_FILEINFO GetFileInfo(int nFileNo)
		{
			int num = nFileNo;
			foreach (NmbFile nmbFile in Nmb.m_NmbFile)
			{
				if (num < nmbFile.GetFileNum())
				{
					return nmbFile.GetFileInfo(num);
				}
				num -= nmbFile.GetFileNum();
			}
			Debug.Print(string.Format("指定ファイルが見つかりません ({0})", nFileNo));
			return null;
		}

		// Token: 0x0600006A RID: 106 RVA: 0x00003904 File Offset: 0x00001B04
		public static NMB_FILEINFO GetFileInfo(string szName)
		{
			NMB_FILEINFO nmb_FILEINFO = null;
			foreach (NmbFile nmbFile in Nmb.m_NmbFile)
			{
				nmb_FILEINFO = nmbFile.GetFileInfo(szName);
				if (nmb_FILEINFO != null)
				{
					return nmb_FILEINFO;
				}
			}
			Debug.Print(string.Format("指定ファイルが見つかりません ({0})", szName));
			return nmb_FILEINFO;
		}

		// Token: 0x0600006B RID: 107 RVA: 0x0000398C File Offset: 0x00001B8C
		public static int GetPackNo(string name)
		{
			foreach (NmbFile nmbFile in Nmb.m_NmbFile)
			{
				NMB_FILEINFO fileInfo = nmbFile.GetFileInfo(name);
				if (fileInfo != null)
				{
					return nmbFile.No;
				}
			}
			return -1;
		}

		// Token: 0x0600006C RID: 108 RVA: 0x00003A08 File Offset: 0x00001C08
		public static int NameToNo(string szName)
		{
			int num = 0;
			foreach (NmbFile nmbFile in Nmb.m_NmbFile)
			{
				int num2 = nmbFile.NameToNo(szName);
				if (num2 >= 0)
				{
					return num + num2;
				}
				num += nmbFile.GetFileNum();
			}
			Debug.Assert(false, string.Format("指定ファイルが見つかりません ({0})", szName));
			return -1;
		}

		// Token: 0x040000E6 RID: 230
		private static List<NmbFile> m_NmbFile = new List<NmbFile>();
	}
}
