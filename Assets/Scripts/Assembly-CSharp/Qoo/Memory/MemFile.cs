﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Qoo.Memory
{
	// Token: 0x020000BE RID: 190
	public class MemFile
	{
		// Token: 0x060005B6 RID: 1462 RVA: 0x00016FF0 File Offset: 0x000151F0
		public MemFile(byte[] data_ = null)
		{
			this.Data = new List<byte>();
			this.Pos = 0;
			if (data_ != null)
			{
				this.Data.AddRange(data_);
			}
		}

		// Token: 0x170000D1 RID: 209
		// (get) Token: 0x060005B7 RID: 1463 RVA: 0x00017028 File Offset: 0x00015228
		// (set) Token: 0x060005B8 RID: 1464 RVA: 0x00017030 File Offset: 0x00015230
		public List<byte> Data { get; private set; }

		// Token: 0x170000D2 RID: 210
		// (get) Token: 0x060005B9 RID: 1465 RVA: 0x0001703C File Offset: 0x0001523C
		// (set) Token: 0x060005BA RID: 1466 RVA: 0x00017044 File Offset: 0x00015244
		public int Pos { get; private set; }

		// Token: 0x060005BB RID: 1467 RVA: 0x00017050 File Offset: 0x00015250
		public int GetInt32()
		{
			int num = (int)this.Data[this.Pos];
			num |= (int)this.Data[this.Pos + 1] << 8;
			num |= (int)this.Data[this.Pos + 2] << 16;
			num |= (int)((sbyte)this.Data[this.Pos + 3]) << 24;
			this.Pos += 4;
			return num;
		}

		// Token: 0x060005BC RID: 1468 RVA: 0x000170CC File Offset: 0x000152CC
		public void SetInt32(int data_)
		{
			this.SetUInt32((uint)data_);
		}

		// Token: 0x060005BD RID: 1469 RVA: 0x000170D8 File Offset: 0x000152D8
		public uint GetUInt32()
		{
			return (uint)this.GetInt32();
		}

		// Token: 0x060005BE RID: 1470 RVA: 0x000170E0 File Offset: 0x000152E0
		public void SetUInt32(uint data_)
		{
			byte[] collection = new byte[]
			{
				(byte)(data_ & 255U),
				(byte)((data_ & 65280U) >> 8),
				(byte)((data_ & 16711680U) >> 16),
				(byte)((data_ & 4278190080U) >> 24)
			};
			this.Data.AddRange(collection);
		}

		// Token: 0x060005BF RID: 1471 RVA: 0x00017134 File Offset: 0x00015334
		public short GetInt16()
		{
			short num = (short)this.Data[this.Pos];
			num |= (short)(this.Data[this.Pos + 1] << 8);
			this.Pos += 2;
			return num;
		}

		// Token: 0x060005C0 RID: 1472 RVA: 0x0001717C File Offset: 0x0001537C
		public void SetInt16(short data_)
		{
			this.SetUInt16((ushort)data_);
		}

		// Token: 0x060005C1 RID: 1473 RVA: 0x00017188 File Offset: 0x00015388
		public ushort GetUInt16()
		{
			return (ushort)this.GetInt16();
		}

		// Token: 0x060005C2 RID: 1474 RVA: 0x00017194 File Offset: 0x00015394
		public void SetUInt16(ushort data_)
		{
			byte[] collection = new byte[]
			{
				(byte)(data_ & 255),
				(byte)((data_ & 65280) >> 8)
			};
			this.Data.AddRange(collection);
		}

		// Token: 0x060005C3 RID: 1475 RVA: 0x000171CC File Offset: 0x000153CC
		public void SetInt16Array(short[] data_)
		{
			byte[] array = new byte[data_.Length * 2];
			Buffer.BlockCopy(data_, 0, array, 0, array.Length);
			this.Data.AddRange(array);
		}

		// Token: 0x060005C4 RID: 1476 RVA: 0x000171FC File Offset: 0x000153FC
		public sbyte GetInt8()
		{
			sbyte result = (sbyte)this.Data[this.Pos];
			this.Pos++;
			return result;
		}

		// Token: 0x060005C5 RID: 1477 RVA: 0x0001722C File Offset: 0x0001542C
		public byte GetUInt8()
		{
			return (byte)this.GetInt8();
		}

		// Token: 0x060005C6 RID: 1478 RVA: 0x00017238 File Offset: 0x00015438
		public void SetUInt8(byte data_)
		{
			this.Data.Add(data_);
		}

		// Token: 0x060005C7 RID: 1479 RVA: 0x00017248 File Offset: 0x00015448
		public void SetInt8(sbyte data_)
		{
			this.Data.Add((byte)data_);
		}

		// Token: 0x060005C8 RID: 1480 RVA: 0x00017258 File Offset: 0x00015458
		public string GetString(int size)
		{
			Encoding encoding = Encoding.GetEncoding("utf-8");
			Encoding unicode = Encoding.Unicode;
			byte[] bytes = Encoding.Convert(encoding, unicode, this.Data.ToArray(), this.Pos, size);
			string @string = unicode.GetString(bytes);
			this.Pos += size;
			return @string;
		}

		// Token: 0x060005C9 RID: 1481 RVA: 0x000172A8 File Offset: 0x000154A8
		public string GetStringUtf16()
		{
			string text = string.Empty;
			while (this.Pos < this.Length())
			{
				char @uint = (char)this.GetUInt16();
				if (@uint == '\0')
				{
					break;
				}
				text += @uint;
			}
			return text;
		}

		// Token: 0x060005CA RID: 1482 RVA: 0x000172F4 File Offset: 0x000154F4
		public void SetStringUtf16(string data_)
		{
			for (int num = 0; num != data_.Length; num++)
			{
				this.SetUInt16((ushort)data_[num]);
			}
			this.SetUInt16(0);
		}

		// Token: 0x060005CB RID: 1483 RVA: 0x0001732C File Offset: 0x0001552C
		public void Align(int iAlign)
		{
			this.Pos = (iAlign - this.Pos % iAlign) % iAlign + this.Pos;
		}

		// Token: 0x060005CC RID: 1484 RVA: 0x00017348 File Offset: 0x00015548
		public void SetPos(int pos_)
		{
			this.Pos = pos_;
		}

		// Token: 0x060005CD RID: 1485 RVA: 0x00017354 File Offset: 0x00015554
		public int Length()
		{
			return this.Data.Count;
		}

		// Token: 0x060005CE RID: 1486 RVA: 0x00017364 File Offset: 0x00015564
		internal bool GetBool()
		{
			return (int)this.GetInt8() != 0;
		}

		// Token: 0x060005CF RID: 1487 RVA: 0x0001737C File Offset: 0x0001557C
		internal void SetBool(bool IsString)
		{
			//Unity didn't like the ternary operator
			//this.SetInt8((!IsString) ? 0 : 1);
			if(!IsString){
				this.SetInt8(0);
			}
			else{
				this.SetInt8(1);
			}
		}

		// Token: 0x060005D0 RID: 1488 RVA: 0x00017394 File Offset: 0x00015594
		internal void Clear()
		{
			this.Data.Clear();
		}
	}
}
