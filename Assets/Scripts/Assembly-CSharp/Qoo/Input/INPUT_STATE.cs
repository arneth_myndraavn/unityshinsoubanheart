﻿using System;

namespace Qoo.Input
{
	// Token: 0x0200002D RID: 45
	public enum INPUT_STATE
	{
		// Token: 0x04000125 RID: 293
		WAIT,
		// Token: 0x04000126 RID: 294
		NEXT,
		// Token: 0x04000127 RID: 295
		BACK,
		// Token: 0x04000128 RID: 296
		AUTO,
		// Token: 0x04000129 RID: 297
		JUMP_NEXT,
		// Token: 0x0400012A RID: 298
		JUMP_BACK,
		// Token: 0x0400012B RID: 299
		HIDE
	}
}
