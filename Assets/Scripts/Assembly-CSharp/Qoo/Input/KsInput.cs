﻿using System;
using UnityEngine;

namespace Qoo.Input
{
	// Token: 0x0200002E RID: 46
	public static class KsInput
	{
		// Token: 0x17000029 RID: 41
		// (get) Token: 0x06000167 RID: 359 RVA: 0x00006724 File Offset: 0x00004924
		// (set) Token: 0x06000168 RID: 360 RVA: 0x0000672C File Offset: 0x0000492C
		public static INPUT_STATE State
		{
			get
			{
				return KsInput.m_eState;
			}
			set
			{
				if (KsInput.m_eState != value)
				{
				}
				KsInput.m_eState = value;
			}
		} 
		// TODO add default initialization later
		//= INPUT_STATE.WAIT;

		// Token: 0x1700002A RID: 42
		// (get) Token: 0x06000169 RID: 361 RVA: 0x00006740 File Offset: 0x00004940
		// (set) Token: 0x0600016A RID: 362 RVA: 0x00006748 File Offset: 0x00004948
		public static bool IsLock { get; private set; } 
		// TODO add default initialization later
		//= false;

		// Token: 0x1700002B RID: 43
		// (get) Token: 0x0600016B RID: 363 RVA: 0x00006750 File Offset: 0x00004950
		// (set) Token: 0x0600016C RID: 364 RVA: 0x00006758 File Offset: 0x00004958
		public static bool Pause { get; set; }

		// Token: 0x1700002C RID: 44
		// (get) Token: 0x0600016D RID: 365 RVA: 0x00006760 File Offset: 0x00004960
		public static bool IsJump
		{
			get
			{
				return KsInput.State == INPUT_STATE.JUMP_NEXT || KsInput.State == INPUT_STATE.JUMP_BACK;
			}
		}

		// Token: 0x1700002D RID: 45
		// (get) Token: 0x0600016E RID: 366 RVA: 0x00006778 File Offset: 0x00004978
		public static bool IsRewind
		{
			get
			{
				return KsInput.State == INPUT_STATE.BACK || KsInput.State == INPUT_STATE.JUMP_BACK;
			}
		}

		// Token: 0x1700002E RID: 46
		// (get) Token: 0x0600016F RID: 367 RVA: 0x00006790 File Offset: 0x00004990
		public static bool IsSkip
		{
			get
			{
				return KsInput.State == INPUT_STATE.NEXT;
			}
		}

		// Token: 0x1700002F RID: 47
		// (get) Token: 0x06000170 RID: 368 RVA: 0x0000679C File Offset: 0x0000499C
		public static bool IsAuto
		{
			get
			{
				return KsInput.State == INPUT_STATE.AUTO;
			}
		}

		// Token: 0x17000030 RID: 48
		// (get) Token: 0x06000171 RID: 369 RVA: 0x000067A8 File Offset: 0x000049A8
		public static bool IsHide
		{
			get
			{
				return KsInput.State == INPUT_STATE.HIDE;
			}
		}

		// Token: 0x17000031 RID: 49
		// (get) Token: 0x06000172 RID: 370 RVA: 0x000067B4 File Offset: 0x000049B4
		// (set) Token: 0x06000173 RID: 371 RVA: 0x000067BC File Offset: 0x000049BC
		public static bool IsMenu
		{
			get
			{
				return KsInput.m_isMenu;
			}
			private set
			{
				KsInput.m_isMenu = value;
			}
		}

		// Token: 0x17000032 RID: 50
		// (get) Token: 0x06000174 RID: 372 RVA: 0x000067C4 File Offset: 0x000049C4
		// (set) Token: 0x06000175 RID: 373 RVA: 0x000067CC File Offset: 0x000049CC
		public static bool IsBackLog
		{
			get
			{
				return KsInput.m_isBacklog;
			}
			private set
			{
				KsInput.m_isBacklog = value;
			}
		}

		// Token: 0x17000033 RID: 51
		// (get) Token: 0x06000176 RID: 374 RVA: 0x000067D4 File Offset: 0x000049D4
		// (set) Token: 0x06000177 RID: 375 RVA: 0x000067DC File Offset: 0x000049DC
		public static bool Enable
		{
			get
			{
				return KsInput.m_isEnable;
			}
			set
			{
				KsInput.m_isEnable = value;
			}
		}

		// Token: 0x17000034 RID: 52
		// (get) Token: 0x06000178 RID: 376 RVA: 0x000067E4 File Offset: 0x000049E4
		// (set) Token: 0x06000179 RID: 377 RVA: 0x000067EC File Offset: 0x000049EC
		public static bool IsTrig
		{
			get
			{
				return KsInput.m_isTrg;
			}
			set
			{
				KsInput.m_isTrg = value;
			}
		}

		// Token: 0x17000035 RID: 53
		// (get) Token: 0x0600017A RID: 378 RVA: 0x000067F4 File Offset: 0x000049F4
		// (set) Token: 0x0600017B RID: 379 RVA: 0x000067FC File Offset: 0x000049FC
		public static UnitySprite HideSprite { get; set; }

		// Token: 0x17000036 RID: 54
		// (get) Token: 0x0600017C RID: 380 RVA: 0x00006804 File Offset: 0x00004A04
		// (set) Token: 0x0600017D RID: 381 RVA: 0x0000680C File Offset: 0x00004A0C
		public static bool IsAutoLock { get; set; } 
		// TODO add default initialization later
		//= false;

		// Token: 0x0600017E RID: 382 RVA: 0x00006814 File Offset: 0x00004A14
		public static void SetAuto()
		{
			KsInput.State = INPUT_STATE.AUTO;
		}

		// Token: 0x0600017F RID: 383 RVA: 0x0000681C File Offset: 0x00004A1C
		public static void SetJump(bool IsNext)
		{
			if (IsNext)
			{
				KsInput.State = INPUT_STATE.JUMP_NEXT;
			}
			else
			{
				KsInput.State = INPUT_STATE.JUMP_BACK;
			}
			KsInput.IsLock = true;
		}

		// Token: 0x06000180 RID: 384 RVA: 0x0000683C File Offset: 0x00004A3C
		public static void SetSkip(bool IsNext)
		{
			KsInput.IsLock = true;
			if (IsNext)
			{
				KsInput.State = INPUT_STATE.NEXT;
			}
			else
			{
				KsInput.State = INPUT_STATE.BACK;
			}
		}

		// Token: 0x06000181 RID: 385 RVA: 0x0000685C File Offset: 0x00004A5C
		public static bool Update()
		{
			if (KsInput.IsMenu || KsInput.IsBackLog)
			{
				return true;
			}
			if (!KsInput.Enable)
			{
				return true;
			}
			if (KsInput.State == INPUT_STATE.JUMP_NEXT || KsInput.State == INPUT_STATE.JUMP_BACK)
			{
				return true;
			}
			INPUT_STATUS status = UnityApp.Input.Status;
			if (status == INPUT_STATUS.DRAG || status == INPUT_STATUS.FLICK)
			{
				if (KsInput.IsSlideUp())
				{
					KsInput.IsMenu = true;
				}
				else if (KsInput.IsSlideDown())
				{
					KsInput.IsBackLog = true;
				}
			}
			switch (KsInput.State)
			{
			case INPUT_STATE.WAIT:
				switch (UnityApp.Input.Status)
				{
				case INPUT_STATUS.NONE:
					KsInput.m_iPushTime = 0;
					break;
				case INPUT_STATUS.PUSH:
				{
					bool isDebugBuild = UnityEngine.Debug.isDebugBuild;
					if (isDebugBuild)
					{
						KsInput.m_iPushTime++;
						if (KsInput.m_iPushTime > 100)
						{
							if (Singleton<ParamWnd>.IsReady)
							{
								Singleton<ParamWnd>.Instance.enabled = !Singleton<ParamWnd>.Instance.enabled;
							}
							KsInput.m_iPushTime = 0;
						}
					}
					else
					{
						KsInput.m_iPushTime = 0;
					}
					break;
				}
				case INPUT_STATUS.CLICK:
					if (KsInput.HideSprite != null && KsInput.HideSprite.Show && KsInput.HideSprite.IsHit(UnityApp.Input.Start) && KsInput.HideSprite.IsHit(UnityApp.Input.End))
					{
						KsInput.m_isTrg = false;
						KsInput.State = INPUT_STATE.HIDE;
					}
					else
					{
						KsInput.m_isTrg = true;
					}
					break;
				case INPUT_STATUS.DRAG_ON:
					KsInput.SetSlide(true);
					break;
				case INPUT_STATUS.DRAG:
					KsInput.SetSlide(false);
					break;
				case INPUT_STATUS.FLICK:
				{
					INPUT_STATE state = KsInput.State;
					KsInput.SetSlide(true);
					KsInput.SetSlide(false);
					if (state != KsInput.State)
					{
						KsInput.IsLock = true;
					}
					break;
				}
				}
				break;
			case INPUT_STATE.NEXT:
				if (!KsInput.IsLock)
				{
					switch (UnityApp.Input.Status)
					{
					case INPUT_STATUS.DRAG_ON:
						KsInput.SetSlide(true);
						break;
					case INPUT_STATUS.DRAG:
						KsInput.SetSlide(false);
						break;
					case INPUT_STATUS.FLICK:
						KsInput.IsLock = true;
						KsInput.SetSlide(true);
						KsInput.SetSlide(false);
						break;
					default:
						KsInput.State = INPUT_STATE.WAIT;
						break;
					}
				}
				else if (KsInput.Pause)
				{
					KsInput.PushReset();
				}
				break;
			case INPUT_STATE.BACK:
				if (!KsInput.IsLock)
				{
					switch (UnityApp.Input.Status)
					{
					case INPUT_STATUS.DRAG_ON:
						KsInput.SetSlide(true);
						break;
					case INPUT_STATUS.DRAG:
						KsInput.SetSlide(false);
						break;
					case INPUT_STATUS.FLICK:
						KsInput.IsLock = true;
						KsInput.SetSlide(true);
						KsInput.SetSlide(false);
						break;
					default:
						KsInput.State = INPUT_STATE.WAIT;
						break;
					}
				}
				else
				{
					KsInput.PushReset();
				}
				break;
			case INPUT_STATE.AUTO:
				if (!KsInput.IsAutoLock && KsInput.Pause)
				{
					KsInput.PushReset();
				}
				break;
			case INPUT_STATE.HIDE:
				KsInput.PushReset();
				break;
			}
			return true;
		}

		// Token: 0x06000182 RID: 386 RVA: 0x00006B78 File Offset: 0x00004D78
		public static void Unlock()
		{
			KsInput.Clear();
		}

		// Token: 0x06000183 RID: 387 RVA: 0x00006B80 File Offset: 0x00004D80
		internal static void SetMask(INPUT_STATE iNPUT_STATE)
		{
		}

		// Token: 0x06000184 RID: 388 RVA: 0x00006B84 File Offset: 0x00004D84
		internal static void ResetMask(INPUT_STATE iNPUT_STATE)
		{
		}

		// Token: 0x06000185 RID: 389 RVA: 0x00006B88 File Offset: 0x00004D88
		public static void Clear()
		{
			KsInput.IsLock = false;
			KsInput.IsAutoLock = false;
			KsInput.State = INPUT_STATE.WAIT;
			UnityApp.Input.IsOnePushDelete = true;
		}

		// Token: 0x06000186 RID: 390 RVA: 0x00006BA8 File Offset: 0x00004DA8
		public static void ClearMenu()
		{
			KsInput.IsMenu = false;
		}

		// Token: 0x06000187 RID: 391 RVA: 0x00006BB0 File Offset: 0x00004DB0
		public static void ClearBacklog()
		{
			KsInput.IsBackLog = false;
		}

		// Token: 0x06000188 RID: 392 RVA: 0x00006BB8 File Offset: 0x00004DB8
		private static void PushReset()
		{
			if (UnityApp.Input.Status != INPUT_STATUS.NONE)
			{
				KsInput.Clear();
			}
		}

		// Token: 0x06000189 RID: 393 RVA: 0x00006BD0 File Offset: 0x00004DD0
		private static bool IsSlideDown()
		{
			return KsInput.Slide() == SLIDE_VECTOR.DOWN;
		}

		// Token: 0x0600018A RID: 394 RVA: 0x00006BDC File Offset: 0x00004DDC
		private static bool IsSlideUp()
		{
			return KsInput.Slide() == SLIDE_VECTOR.UP;
		}

		// Token: 0x0600018B RID: 395 RVA: 0x00006BE8 File Offset: 0x00004DE8
		private static void SetSlide(bool IsOn)
		{
			SLIDE_VECTOR slide_VECTOR = KsInput.Slide();
			if (slide_VECTOR != SLIDE_VECTOR.LEFT)
			{
				if (slide_VECTOR == SLIDE_VECTOR.RIGHT)
				{
					if (IsOn && KsInput.Pause)
					{
						KsInput.State = INPUT_STATE.NEXT;
					}
				}
			}
			else if (IsOn)
			{
				KsInput.State = INPUT_STATE.BACK;
			}
		}

		// Token: 0x0600018C RID: 396 RVA: 0x00006C3C File Offset: 0x00004E3C
		private static SLIDE_VECTOR Slide()
		{
			if ((UnityApp.Input.Status == INPUT_STATUS.DRAG || UnityApp.Input.Status == INPUT_STATUS.DRAG_ON) && UnityApp.Input.StartDeltaMagnitude < 32f)
			{
				return SLIDE_VECTOR.NONE;
			}
			return UnityApp.Input.Slide;
		}

		// Token: 0x0400012C RID: 300
		private const float VEC_THRESHOLD = 0.6f;

		// Token: 0x0400012D RID: 301
		private const float DEF_CHECK_SLIDE_DELTA = 32f;

		// Token: 0x0400012E RID: 302
		private static INPUT_STATE m_eState;

		// Token: 0x0400012F RID: 303
		private static int m_iPushTime;

		// Token: 0x04000130 RID: 304
		private static bool m_isMenu;

		// Token: 0x04000131 RID: 305
		private static bool m_isBacklog;

		// Token: 0x04000132 RID: 306
		private static bool m_isEnable = true;

		// Token: 0x04000133 RID: 307
		public static bool m_isTrg;
	}
}
