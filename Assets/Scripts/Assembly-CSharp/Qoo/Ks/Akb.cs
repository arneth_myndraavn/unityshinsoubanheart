﻿using System;
using Qoo.File;
using Qoo.Memory;

namespace Qoo.Ks
{
	// Token: 0x02000033 RID: 51
	public static class Akb
	{
		// Token: 0x0600018F RID: 399 RVA: 0x00006CEC File Offset: 0x00004EEC
		public static void Create(byte[] data)
		{
			Akb.Release();
			MemFile memFile = new MemFile(data);
			Akb.m_Fh.FourCC = memFile.GetUInt32();
			Akb.m_Fh.nNumOfDir = memFile.GetUInt32();
			Akb.m_Fh.nNumOfFile = memFile.GetUInt32();
			Akb.m_Fh.nNumOfLabel = memFile.GetUInt32();
			Akb.m_Fh.nStringOffset = memFile.GetUInt32();
			Akb.m_Fh.nMaxTagNum = memFile.GetUInt32();
			Akb.m_Fh.nMaxMsgTagNum = memFile.GetUInt32();
			Akb.m_Fh.nMaxParamNum = memFile.GetUInt32();
			Akb.m_Fh.nMaxMsgNum = memFile.GetUInt32();
			Akb.m_Fh.nMaxTextSize = memFile.GetUInt32();
			Akb.m_Fh.nMaxLabelNum = memFile.GetUInt32();
			int pos = memFile.Pos;
			memFile.SetPos((int)Akb.m_Fh.nStringOffset);
			int size = memFile.Length() - memFile.Pos;
			string @string = memFile.GetString(size);
			memFile.SetPos(pos);
			Akb.m_Dirs = new AKB_DIRINFO[Akb.m_Fh.nNumOfDir];
			int num = 0;
			while ((long)num != (long)((ulong)Akb.m_Fh.nNumOfDir))
			{
				Akb.m_Dirs[num].Name = Akb.GetString(@string, (int)memFile.GetUInt32());
				Akb.m_Dirs[num].nNameCRC = memFile.GetUInt32();
				num++;
			}
			Akb.m_Files = new AKB_FILEINFO[Akb.m_Fh.nNumOfFile];
			int num2 = 0;
			while ((long)num2 != (long)((ulong)Akb.m_Fh.nNumOfFile))
			{
				Akb.m_Files[num2].nDirNo = memFile.GetUInt32();
				Akb.m_Files[num2].Name = Akb.GetString(@string, (int)memFile.GetUInt32());
				Akb.m_Files[num2].nNameCRC = memFile.GetUInt32();
				Akb.m_Files[num2].nLabelPos = memFile.GetUInt32();
				Akb.m_Files[num2].nLabelNum = memFile.GetInt32();
				Akb.m_Files[num2].nTagNum = memFile.GetInt32();
				Akb.m_Files[num2].nMsgTagNum = memFile.GetInt32();
				Akb.m_Files[num2].nParamNum = memFile.GetInt32();
				Akb.m_Files[num2].nMsgNum = memFile.GetInt32();
				Akb.m_Files[num2].nTextSize = memFile.GetInt32();
				num2++;
			}
			Akb.m_Labels = new AKB_LABELINFO[Akb.m_Fh.nNumOfLabel];
			int num3 = 0;
			while ((long)num3 != (long)((ulong)Akb.m_Fh.nNumOfLabel))
			{
				Akb.m_Labels[num3].nFileNo = memFile.GetUInt32();
				Akb.m_Labels[num3].Name = Akb.GetString(@string, (int)memFile.GetUInt32());
				Akb.m_Labels[num3].nNameCRC = memFile.GetUInt32();
				Akb.m_Labels[num3].nTagNum = memFile.GetInt32();
				num3++;
			}
		}

		// Token: 0x06000190 RID: 400 RVA: 0x00007010 File Offset: 0x00005210
		public static bool Release()
		{
			Akb.m_Fh.Init();
			Akb.m_Dirs = null;
			Akb.m_Files = null;
			Akb.m_Labels = null;
			return true;
		}

		// Token: 0x06000191 RID: 401 RVA: 0x00007030 File Offset: 0x00005230
		public static int GetDirNum()
		{
			return Akb.m_Dirs.Length;
		}

		// Token: 0x06000192 RID: 402 RVA: 0x0000703C File Offset: 0x0000523C
		public static int GetFileNum()
		{
			return Akb.m_Files.Length;
		}

		// Token: 0x06000193 RID: 403 RVA: 0x00007048 File Offset: 0x00005248
		public static int GetLabelNum()
		{
			return Akb.m_Labels.Length;
		}

		// Token: 0x06000194 RID: 404 RVA: 0x00007054 File Offset: 0x00005254
		public static AKB_FILEHEADER GetHeader()
		{
			return Akb.m_Fh;
		}

		// Token: 0x06000195 RID: 405 RVA: 0x0000705C File Offset: 0x0000525C
		public static AKB_DIRINFO GetDirInfo(int nDirNo)
		{
			return Akb.m_Dirs[nDirNo];
		}

		// Token: 0x06000196 RID: 406 RVA: 0x00007070 File Offset: 0x00005270
		public static AKB_FILEINFO GetFileInfo(int nFileNo)
		{
			return Akb.m_Files[nFileNo];
		}

		// Token: 0x06000197 RID: 407 RVA: 0x00007084 File Offset: 0x00005284
		public static AKB_LABELINFO GetLabelInfo(int nLabelNo)
		{
			return Akb.m_Labels[nLabelNo];
		}

		// Token: 0x06000198 RID: 408 RVA: 0x00007098 File Offset: 0x00005298
		public static bool CheckFile(string name)
		{
			FileId fileId = new FileId(name);
			return Akb.SearchFile(fileId.Name) != -1;
		}

		// Token: 0x06000199 RID: 409 RVA: 0x000070C0 File Offset: 0x000052C0
		public static int SearchFile(string name)
		{
			for (int i = 0; i < Akb.m_Files.Length; i++)
			{
				if (Akb.m_Files[i].Name.CompareTo(name) == 0)
				{
					return i;
				}
			}
			return -1;
		}

		// Token: 0x0600019A RID: 410 RVA: 0x00007104 File Offset: 0x00005304
		public static int SearchLabel(int nFileNo, string labelName)
		{
			if (labelName.Length > 0 && labelName[0] == '*')
			{
				labelName = labelName.Substring(1);
			}
			AKB_FILEINFO fileInfo = Akb.GetFileInfo(nFileNo);
			for (int i = 0; i < fileInfo.nLabelNum; i++)
			{
				if (Akb.m_Labels[(int)(checked((IntPtr)(unchecked((long)i + (long)((ulong)fileInfo.nLabelPos)))))].Name.CompareTo(labelName) == 0)
				{
					return i;
				}
			}
			return -1;
		}

		// Token: 0x0600019B RID: 411 RVA: 0x0000717C File Offset: 0x0000537C
		public static bool GetFileLabel(ref int KsNo, ref int LabelNo, string FileName, string LabelName)
		{
			FileId fileId = new FileId(FileName);
			KsNo = Akb.SearchFile(fileId.Name);
			if (KsNo < 0)
			{
				Debug.Assert(KsNo != 0, string.Format("AKB内に指定KSファイルが見つかりません({0})\n", FileName));
				return false;
			}
			LabelNo = 0;
			if (LabelName != null && LabelName.Length > 0)
			{
				if (LabelName.IndexOf('*') == 0)
				{
					LabelName = LabelName.Substring(1);
				}
				LabelNo = Akb.SearchLabel(KsNo, LabelName);
				Debug.Assert(LabelNo >= 0, string.Format("AKB内に指定ラベルが見つかりません({0}:{1})\n", FileName, LabelName));
			}
			return LabelNo >= 0;
		}

		// Token: 0x0600019C RID: 412 RVA: 0x00007218 File Offset: 0x00005418
		private static string GetString(string str, int pos)
		{
			int num = str.IndexOf('\0', pos);
			return str.Substring(pos, num - pos);
		}

		// Token: 0x04000153 RID: 339
		private static AKB_FILEHEADER m_Fh;

		// Token: 0x04000154 RID: 340
		private static AKB_DIRINFO[] m_Dirs;

		// Token: 0x04000155 RID: 341
		private static AKB_FILEINFO[] m_Files;

		// Token: 0x04000156 RID: 342
		private static AKB_LABELINFO[] m_Labels;
	}
}
