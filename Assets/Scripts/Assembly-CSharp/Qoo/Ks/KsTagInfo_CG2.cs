﻿using System;

namespace Qoo.Ks
{
	// Token: 0x02000054 RID: 84
	internal class KsTagInfo_CG2 : KsTagInfo_CG
	{
		// Token: 0x060002C7 RID: 711 RVA: 0x00009C98 File Offset: 0x00007E98
		public KsTagInfo_CG2()
		{
			base.CgNo = 1;
		}

		// Token: 0x060002C8 RID: 712 RVA: 0x00009CA8 File Offset: 0x00007EA8
		public override KSID GetId()
		{
			return KSID.CG2;
		}
	}
}
