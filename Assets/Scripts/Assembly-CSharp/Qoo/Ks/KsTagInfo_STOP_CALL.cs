﻿using System;

namespace Qoo.Ks
{
	// Token: 0x02000061 RID: 97
	internal class KsTagInfo_STOP_CALL : KsTagInfo_STOP
	{
		// Token: 0x060002F6 RID: 758 RVA: 0x0000A514 File Offset: 0x00008714
		public override KSID GetId()
		{
			return KSID.STOP_CALL;
		}

		// Token: 0x060002F7 RID: 759 RVA: 0x0000A518 File Offset: 0x00008718
		public override bool IsCallRet()
		{
			return true;
		}
	}
}
