﻿using System;
using Qoo.Game;
using Qoo.Graphics;
using Qoo.Input;

namespace Qoo.Ks
{
	// Token: 0x02000062 RID: 98
	internal class KsTagInfo_WAIT : KsTagInfo
	{
		// Token: 0x060002F9 RID: 761 RVA: 0x0000A524 File Offset: 0x00008724
		public override KSID GetId()
		{
			return KSID.WAIT;
		}

		// Token: 0x060002FA RID: 762 RVA: 0x0000A528 File Offset: 0x00008728
		public override TAG_RESULT Run(EventPlayer player, TagData tag)
		{
			if (player.RestoreOn)
			{
				return TAG_RESULT.NEXT_EXIT;
			}
			if (GameData.CheckReadedSkip(player.CurKsNo, player.CurLabelNo, player.CurTagNo))
			{
				return TAG_RESULT.NEXT_EXIT;
			}
			bool paramBool = tag.GetParamBool(KSID.KEY, false);
			bool paramBool2 = tag.GetParamBool(KSID.CURSOR, true);
			int num = 1000;
			ParamData param;
			if ((param = tag.GetParam(KSID.TIME)) != null)
			{
				num = param.ToInt();
			}
			else if (paramBool)
			{
				if (KsInput.IsAuto)
				{
					num = SysData.GetAutoPage() * 1000;
					if (num == 0)
					{
						num = 1;
					}
				}
				else
				{
					num = -1;
				}
			}
			if (player.TagCallCnt == 0)
			{
				this.m_iStart = Singleton<UnityApp>.Instance.GetTimeMilli();
			}
			KsInput.Pause = false;
			if (paramBool)
			{
				this.m_IsSilentBackup = Singleton<MsgWnd>.Instance.IsSilentMode;
				Singleton<MsgWnd>.Instance.IsSilentMode = true;
				bool flag = num >= 0 && this.m_iStart + num < Singleton<UnityApp>.Instance.GetTimeMilli();
				bool isTrig = KsInput.IsTrig;
				KsInput.IsTrig = false;
				if (!flag && !isTrig)
				{
					return TAG_RESULT.CONTINUE_EXIT;
				}
			}
			else if (num < 0 || this.m_iStart + num >= Singleton<UnityApp>.Instance.GetTimeMilli())
			{
				return TAG_RESULT.CONTINUE_EXIT;
			}
			if (paramBool)
			{
				Singleton<MsgWnd>.Instance.IsSilentMode = this.m_IsSilentBackup;
			}
			if (paramBool2)
			{
				Graph.Fade_Cross(100);
			}
			return TAG_RESULT.NEXT_EXIT;
		}

		// Token: 0x04000257 RID: 599
		public bool m_IsSilentBackup;

		// Token: 0x04000258 RID: 600
		private int m_iStart;
	}
}
