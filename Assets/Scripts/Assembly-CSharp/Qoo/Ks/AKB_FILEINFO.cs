﻿using System;

namespace Qoo.Ks
{
	// Token: 0x02000031 RID: 49
	public struct AKB_FILEINFO
	{
		// Token: 0x04000145 RID: 325
		public uint nDirNo;

		// Token: 0x04000146 RID: 326
		public string Name;

		// Token: 0x04000147 RID: 327
		public uint nNameCRC;

		// Token: 0x04000148 RID: 328
		public uint nLabelPos;

		// Token: 0x04000149 RID: 329
		public int nLabelNum;

		// Token: 0x0400014A RID: 330
		public int nTagNum;

		// Token: 0x0400014B RID: 331
		public int nMsgTagNum;

		// Token: 0x0400014C RID: 332
		public int nParamNum;

		// Token: 0x0400014D RID: 333
		public int nMsgNum;

		// Token: 0x0400014E RID: 334
		public int nTextSize;
	}
}
