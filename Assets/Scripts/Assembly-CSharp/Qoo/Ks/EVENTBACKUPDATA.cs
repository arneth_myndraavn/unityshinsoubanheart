﻿using System;
using Qoo.Memory;

namespace Qoo.Ks
{
	// Token: 0x0200008A RID: 138
	public class EVENTBACKUPDATA
	{
		// Token: 0x060003E6 RID: 998 RVA: 0x0000E218 File Offset: 0x0000C418
		public void Load(MemFile mem)
		{
			this.Scene.Load(mem);
		}

		// Token: 0x060003E7 RID: 999 RVA: 0x0000E228 File Offset: 0x0000C428
		public void Save(MemFile mem)
		{
			this.Scene.Save(mem);
		}

		// Token: 0x040002B3 RID: 691
		public EVENTSCENE Scene = new EVENTSCENE();
	}
}
