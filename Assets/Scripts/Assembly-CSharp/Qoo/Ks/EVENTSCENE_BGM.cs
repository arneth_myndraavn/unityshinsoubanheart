﻿using System;
using Qoo.Memory;

namespace Qoo.Ks
{
	// Token: 0x02000084 RID: 132
	public class EVENTSCENE_BGM
	{
		// Token: 0x060003B8 RID: 952 RVA: 0x0000CFF8 File Offset: 0x0000B1F8
		public EVENTSCENE_BGM()
		{
			this.Init();
		}

		// Token: 0x060003B9 RID: 953 RVA: 0x0000D008 File Offset: 0x0000B208
		public void Reset()
		{
			this.Sw = SWITCH.NULL;
		}

		// Token: 0x060003BA RID: 954 RVA: 0x0000D014 File Offset: 0x0000B214
		public void Init()
		{
			this.Name = string.Empty;
			this.Sw = SWITCH.NULL;
			this.nVolume = 0;
			this.nTime = 0;
		}

		// Token: 0x060003BB RID: 955 RVA: 0x0000D044 File Offset: 0x0000B244
		public void Copy(EVENTSCENE_BGM other)
		{
			this.Name = other.Name;
			this.Sw = other.Sw;
			this.nVolume = other.nVolume;
			this.nTime = other.nTime;
		}

		// Token: 0x060003BC RID: 956 RVA: 0x0000D084 File Offset: 0x0000B284
		public void Save(MemFile conv)
		{
			conv.SetStringUtf16(this.Name);
			conv.SetInt32(this.nVolume);
		}

		// Token: 0x060003BD RID: 957 RVA: 0x0000D0A0 File Offset: 0x0000B2A0
		public void Load(MemFile conv)
		{
			this.Name = conv.GetStringUtf16();
			this.nVolume = conv.GetInt32();
		}

		// Token: 0x060003BE RID: 958 RVA: 0x0000D0BC File Offset: 0x0000B2BC
		public void SetLog(SWITCH sw_, string name_, int nVolume_)
		{
			this.Sw = sw_;
			this.Name = name_;
			this.nVolume = nVolume_;
		}

		// Token: 0x060003BF RID: 959 RVA: 0x0000D0D4 File Offset: 0x0000B2D4
		public void Set(bool bOn, string Name_ = "", int nVol_ = 100, int nTime_ = 3000)
		{
			this.Sw = ((!bOn) ? SWITCH.OFF : SWITCH.ON);
			this.Name = Name_;
			this.nVolume = nVol_;
			this.nTime = nTime_;
		}

		// Token: 0x04000295 RID: 661
		public string Name;

		// Token: 0x04000296 RID: 662
		public SWITCH Sw;

		// Token: 0x04000297 RID: 663
		public int nVolume;

		// Token: 0x04000298 RID: 664
		public int nTime;
	}
}
