﻿using System;

namespace Qoo.Ks
{
	// Token: 0x02000030 RID: 48
	public struct AKB_DIRINFO
	{
		// Token: 0x04000143 RID: 323
		public string Name;

		// Token: 0x04000144 RID: 324
		public uint nNameCRC;
	}
}
