﻿using System;

namespace Qoo.Ks
{
	// Token: 0x02000071 RID: 113
	internal class KsTagInfo_EXIT : KsTagInfo
	{
		// Token: 0x06000333 RID: 819 RVA: 0x0000AB4C File Offset: 0x00008D4C
		public override KSID GetId()
		{
			return KSID.EXIT;
		}

		// Token: 0x06000334 RID: 820 RVA: 0x0000AB50 File Offset: 0x00008D50
		public override bool IsRevContinue()
		{
			return false;
		}

		// Token: 0x06000335 RID: 821 RVA: 0x0000AB54 File Offset: 0x00008D54
		public override bool IsJumpTag()
		{
			return true;
		}

		// Token: 0x06000336 RID: 822 RVA: 0x0000AB58 File Offset: 0x00008D58
		public override bool IsJumpBreak(bool bFront, bool bMemoryMode)
		{
			return true;
		}

		// Token: 0x06000337 RID: 823 RVA: 0x0000AB5C File Offset: 0x00008D5C
		public override TAG_RESULT Run(EventPlayer player, TagData tag)
		{
			return TAG_RESULT.END;
		}
	}
}
