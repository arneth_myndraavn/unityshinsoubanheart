﻿using System;

namespace Qoo.Ks
{
	// Token: 0x02000042 RID: 66
	public struct SCENE_RESTOREDATA
	{
		// Token: 0x17000079 RID: 121
		// (get) Token: 0x0600025C RID: 604 RVA: 0x00008E78 File Offset: 0x00007078
		// (set) Token: 0x0600025D RID: 605 RVA: 0x00008E80 File Offset: 0x00007080
		public byte[] BackupData { get; set; }

		// Token: 0x1700007A RID: 122
		// (get) Token: 0x0600025E RID: 606 RVA: 0x00008E8C File Offset: 0x0000708C
		// (set) Token: 0x0600025F RID: 607 RVA: 0x00008E94 File Offset: 0x00007094
		public SCENEPOSDATA[] PosData { get; set; }

		// Token: 0x1700007B RID: 123
		// (get) Token: 0x06000260 RID: 608 RVA: 0x00008EA0 File Offset: 0x000070A0
		// (set) Token: 0x06000261 RID: 609 RVA: 0x00008EA8 File Offset: 0x000070A8
		public SCENECALLDATA[] CallData { get; set; }
	}
}
