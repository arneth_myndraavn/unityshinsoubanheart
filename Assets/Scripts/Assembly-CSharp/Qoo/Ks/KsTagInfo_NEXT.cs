﻿using System;

namespace Qoo.Ks
{
	// Token: 0x02000064 RID: 100
	internal class KsTagInfo_NEXT : KsTagInfo
	{
		// Token: 0x06000301 RID: 769 RVA: 0x0000A6F0 File Offset: 0x000088F0
		public override KSID GetId()
		{
			return KSID.NEXT;
		}

		// Token: 0x06000302 RID: 770 RVA: 0x0000A6F4 File Offset: 0x000088F4
		public override bool IsJumpTag()
		{
			return true;
		}

		// Token: 0x06000303 RID: 771 RVA: 0x0000A6F8 File Offset: 0x000088F8
		public override TAG_RESULT Run(EventPlayer player, TagData tag)
		{
			string paramString = tag.GetParamString(KSID.FILE, string.Empty);
			string paramString2 = tag.GetParamString(KSID.TARGET, string.Empty);
			return (!player.JumpKs(paramString, paramString2, this.IsCallRet())) ? TAG_RESULT.END : TAG_RESULT.CONTINUE;
		}
	}
}
