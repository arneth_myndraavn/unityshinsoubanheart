﻿using System;

namespace Qoo.Ks
{
	// Token: 0x02000053 RID: 83
	internal class KsTagInfo_CG1 : KsTagInfo_CG
	{
		// Token: 0x060002C5 RID: 709 RVA: 0x00009C84 File Offset: 0x00007E84
		public KsTagInfo_CG1()
		{
			base.CgNo = 0;
		}

		// Token: 0x060002C6 RID: 710 RVA: 0x00009C94 File Offset: 0x00007E94
		public override KSID GetId()
		{
			return KSID.CG1;
		}
	}
}
