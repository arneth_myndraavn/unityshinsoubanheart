﻿using System;

namespace Qoo.Ks
{
	// Token: 0x0200006B RID: 107
	internal class KsTagInfo_IF_READED_NEXT : KsTagInfo_IF_SYS_READED_NEXT
	{
		// Token: 0x0600031C RID: 796 RVA: 0x0000A9C8 File Offset: 0x00008BC8
		public KsTagInfo_IF_READED_NEXT()
		{
			base.IsSys = false;
		}

		// Token: 0x0600031D RID: 797 RVA: 0x0000A9D8 File Offset: 0x00008BD8
		public override KSID GetId()
		{
			return KSID.IF_READED_NEXT;
		}
	}
}
