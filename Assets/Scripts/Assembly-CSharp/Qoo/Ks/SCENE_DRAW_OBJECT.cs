﻿using System;

namespace Qoo.Ks
{
	// Token: 0x0200007F RID: 127
	public struct SCENE_DRAW_OBJECT
	{
		// Token: 0x04000279 RID: 633
		public DRAW_COMMAND com;

		// Token: 0x0400027A RID: 634
		public bool isCenter;

		// Token: 0x0400027B RID: 635
		public UnitySprite sp;

		// Token: 0x0400027C RID: 636
		public int x;

		// Token: 0x0400027D RID: 637
		public int y;

		// Token: 0x0400027E RID: 638
		public int z;

		// Token: 0x0400027F RID: 639
		public byte a;

		// Token: 0x04000280 RID: 640
		public string name;

		// Token: 0x04000281 RID: 641
		public UnityTexture tex;
	}
}
