﻿using System;

namespace Qoo.Ks
{
	// Token: 0x0200004F RID: 79
	internal class KsTagInfo_CHARA3 : KsTagInfo_CHARA
	{
		// Token: 0x060002B9 RID: 697 RVA: 0x00009B1C File Offset: 0x00007D1C
		public KsTagInfo_CHARA3()
		{
			base.CharNo = 2;
		}

		// Token: 0x060002BA RID: 698 RVA: 0x00009B2C File Offset: 0x00007D2C
		public override KSID GetId()
		{
			return KSID.CHARA3;
		}
	}
}
