﻿using System;
using Qoo.SoundSystem;

namespace Qoo.Ks
{
	// Token: 0x02000057 RID: 87
	internal class KsTagInfo_BGM : KsTagInfo
	{
		// Token: 0x060002CE RID: 718 RVA: 0x00009CDC File Offset: 0x00007EDC
		public override KSID GetId()
		{
			return KSID.BGM;
		}

		// Token: 0x060002CF RID: 719 RVA: 0x00009CE0 File Offset: 0x00007EE0
		public override bool IsRevSceneApply()
		{
			return true;
		}

		// Token: 0x060002D0 RID: 720 RVA: 0x00009CE4 File Offset: 0x00007EE4
		public override TAG_RESULT Run(EventPlayer player, TagData tag)
		{
			int num = (!Sound.BgmIsPlay()) ? 0 : 3000;
			num = tag.GetParamInt(KSID.TIME, num);
			int paramInt = tag.GetParamInt(KSID.VOLUME, 100);
			ParamData param;
			if ((param = tag.GetParam(KSID.FILE)) != null)
			{
				player.Scene.EntryBgm(true, param.Param, paramInt, num);
			}
			else
			{
				player.Scene.EntryBgm(false, string.Empty, paramInt, num);
			}
			return TAG_RESULT.NEXT_EXIT;
		}
	}
}
