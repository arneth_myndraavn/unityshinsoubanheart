﻿using System;

namespace Qoo.Ks
{
	// Token: 0x0200006F RID: 111
	internal class KsTagInfo_ENDMEMORY : KsTagInfo
	{
		// Token: 0x06000329 RID: 809 RVA: 0x0000AAD8 File Offset: 0x00008CD8
		public override KSID GetId()
		{
			return KSID.ENDMEMORY;
		}

		// Token: 0x0600032A RID: 810 RVA: 0x0000AADC File Offset: 0x00008CDC
		public override bool IsRevContinue()
		{
			return false;
		}

		// Token: 0x0600032B RID: 811 RVA: 0x0000AAE0 File Offset: 0x00008CE0
		public override bool IsJumpBreak(bool bFront, bool bMemoryMode)
		{
			return bMemoryMode;
		}

		// Token: 0x0600032C RID: 812 RVA: 0x0000AAE8 File Offset: 0x00008CE8
		public override TAG_RESULT Run(EventPlayer player, TagData tag)
		{
			if (player.Mode == PLAY_MODE.MEMORY)
			{
				return TAG_RESULT.END;
			}
			UnityApp.AutoSave();
			return TAG_RESULT.NEXT;
		}
	}
}
