﻿using System;

namespace Qoo.Ks
{
	// Token: 0x0200007C RID: 124
	public enum EVSP
	{
		// Token: 0x04000265 RID: 613
		BG,
		// Token: 0x04000266 RID: 614
		CHAR,
		// Token: 0x04000267 RID: 615
		FACE = 6,
		// Token: 0x04000268 RID: 616
		FLASH,
		// Token: 0x04000269 RID: 617
		NUM
	}
}
