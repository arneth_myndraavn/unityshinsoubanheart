﻿using System;

namespace Qoo.Ks
{
	// Token: 0x02000043 RID: 67
	public enum TAG_RESULT
	{
		// Token: 0x040001DD RID: 477
		NULL,
		// Token: 0x040001DE RID: 478
		CONTINUE,
		// Token: 0x040001DF RID: 479
		CONTINUE_EXIT,
		// Token: 0x040001E0 RID: 480
		NEXT,
		// Token: 0x040001E1 RID: 481
		NEXT_EXIT,
		// Token: 0x040001E2 RID: 482
		END
	}
}
