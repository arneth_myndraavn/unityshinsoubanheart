﻿using System;

namespace Qoo.Ks
{
	// Token: 0x02000065 RID: 101
	internal class KsTagInfo_CALL : KsTagInfo_NEXT
	{
		// Token: 0x06000305 RID: 773 RVA: 0x0000A744 File Offset: 0x00008944
		public override KSID GetId()
		{
			return KSID.CALL;
		}

		// Token: 0x06000306 RID: 774 RVA: 0x0000A748 File Offset: 0x00008948
		public override bool IsCallRet()
		{
			return true;
		}
	}
}
