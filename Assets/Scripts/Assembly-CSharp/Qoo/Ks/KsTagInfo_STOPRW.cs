﻿using System;
using Qoo.Application;
using Qoo.Game;

namespace Qoo.Ks
{
	// Token: 0x02000063 RID: 99
	internal class KsTagInfo_STOPRW : KsTagInfo
	{
		// Token: 0x060002FC RID: 764 RVA: 0x0000A69C File Offset: 0x0000889C
		public override KSID GetId()
		{
			return KSID.STOPRW;
		}

		// Token: 0x060002FD RID: 765 RVA: 0x0000A6A0 File Offset: 0x000088A0
		public override bool IsRevContinue()
		{
			return false;
		}

		// Token: 0x060002FE RID: 766 RVA: 0x0000A6A4 File Offset: 0x000088A4
		public override bool IsJumpBreak(bool bFront, bool bMemoryMode)
		{
			return !bFront;
		}

		// Token: 0x060002FF RID: 767 RVA: 0x0000A6AC File Offset: 0x000088AC
		public override TAG_RESULT Run(EventPlayer player, TagData tag)
		{
			GameData.UnlockGameData();
			player.Scene.Backup();
			App.QooKsLog.Reset();
			PaymentParam.Conv();
			GameData.LockGameData();
			player.SnapShot(true);
			return TAG_RESULT.NEXT_EXIT;
		}
	}
}
