﻿using System;

namespace Qoo.Ks
{
	// Token: 0x0200003E RID: 62
	public struct KsWorkLog
	{
		// Token: 0x17000054 RID: 84
		// (get) Token: 0x0600020A RID: 522 RVA: 0x000088EC File Offset: 0x00006AEC
		// (set) Token: 0x0600020B RID: 523 RVA: 0x000088F4 File Offset: 0x00006AF4
		public string Value { get; set; }

		// Token: 0x17000055 RID: 85
		// (get) Token: 0x0600020C RID: 524 RVA: 0x00008900 File Offset: 0x00006B00
		// (set) Token: 0x0600020D RID: 525 RVA: 0x00008908 File Offset: 0x00006B08
		public string Index { get; set; }

		// Token: 0x17000056 RID: 86
		// (get) Token: 0x0600020E RID: 526 RVA: 0x00008914 File Offset: 0x00006B14
		// (set) Token: 0x0600020F RID: 527 RVA: 0x0000891C File Offset: 0x00006B1C
		public string Old { get; set; }

		// Token: 0x17000057 RID: 87
		// (get) Token: 0x06000210 RID: 528 RVA: 0x00008928 File Offset: 0x00006B28
		// (set) Token: 0x06000211 RID: 529 RVA: 0x00008930 File Offset: 0x00006B30
		public string New { get; set; }

		// Token: 0x06000212 RID: 530 RVA: 0x0000893C File Offset: 0x00006B3C
		public void Reset()
		{
			this.Value = string.Empty;
			this.Index = string.Empty;
			this.Old = string.Empty;
			this.New = string.Empty;
		}

		// Token: 0x06000213 RID: 531 RVA: 0x00008978 File Offset: 0x00006B78
		public bool IsEmpty()
		{
			return this.Value == null || this.Value.Length == 0;
		}
	}
}
