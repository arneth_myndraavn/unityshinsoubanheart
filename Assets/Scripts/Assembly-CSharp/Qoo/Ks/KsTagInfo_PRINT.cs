﻿using System;
using Qoo.Game;

namespace Qoo.Ks
{
	// Token: 0x02000078 RID: 120
	internal class KsTagInfo_PRINT : KsTagInfo
	{
		// Token: 0x06000350 RID: 848 RVA: 0x0000AD9C File Offset: 0x00008F9C
		public override KSID GetId()
		{
			return KSID.PRINT;
		}

		// Token: 0x06000351 RID: 849 RVA: 0x0000ADA0 File Offset: 0x00008FA0
		public override bool RunMessage(MsgTagData tag)
		{
			string name = string.Empty;
			ParamData param;
			if ((param = tag.GetParam(KSID.VALUE)) != null)
			{
				name = param.Param;
			}
			string index = string.Empty;
			if ((param = tag.GetParam(KSID.INDEX)) != null)
			{
				index = param.Param;
			}
			string paramString = GameData.GetParamString(name, index);
			Singleton<MsgWnd>.Instance.AddMessage(paramString);
			return true;
		}
	}
}
