﻿using System;
using Qoo.Game;

namespace Qoo.Ks
{
	// Token: 0x02000073 RID: 115
	internal class KsTagInfo_SET : KsTagInfo
	{
		// Token: 0x06000340 RID: 832 RVA: 0x0000AC00 File Offset: 0x00008E00
		public override KSID GetId()
		{
			return KSID.SET;
		}

		// Token: 0x06000341 RID: 833 RVA: 0x0000AC04 File Offset: 0x00008E04
		public override TAG_RESULT Run(EventPlayer player, TagData tag)
		{
			if (player.IsParamLock)
			{
				return TAG_RESULT.NEXT;
			}
			string paramString = tag.GetParamString(KSID.VALUE, string.Empty);
			string paramString2 = tag.GetParamString(KSID.INDEX, string.Empty);
			string paramString3 = tag.GetParamString(KSID.PARAM, string.Empty);
			GameData.SetParamString(paramString, paramString2, paramString3);
			return TAG_RESULT.NEXT;
		}
	}
}
