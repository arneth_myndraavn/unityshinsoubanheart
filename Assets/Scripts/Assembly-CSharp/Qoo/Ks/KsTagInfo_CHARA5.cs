﻿using System;

namespace Qoo.Ks
{
	// Token: 0x02000051 RID: 81
	internal class KsTagInfo_CHARA5 : KsTagInfo_CHARA
	{
		// Token: 0x060002BD RID: 701 RVA: 0x00009B44 File Offset: 0x00007D44
		public KsTagInfo_CHARA5()
		{
			base.CharNo = 4;
		}

		// Token: 0x060002BE RID: 702 RVA: 0x00009B54 File Offset: 0x00007D54
		public override KSID GetId()
		{
			return KSID.CHARA5;
		}
	}
}
