﻿using System;
using Qoo.Script;

namespace Qoo.Ks
{
	// Token: 0x02000075 RID: 117
	internal class KsTagInfo_CMD : KsTagInfo
	{
		// Token: 0x06000346 RID: 838 RVA: 0x0000ACB8 File Offset: 0x00008EB8
		public override KSID GetId()
		{
			return KSID.CMD;
		}

		// Token: 0x06000347 RID: 839 RVA: 0x0000ACBC File Offset: 0x00008EBC
		public override TAG_RESULT Run(EventPlayer player, TagData tag)
		{
			string paramString = tag.GetParamString(KSID.EXP, null);
			if (paramString == null)
			{
				return TAG_RESULT.NEXT;
			}
			int num = 0;
			Calc.ExecCmdStr(paramString, ref num);
			return TAG_RESULT.NEXT;
		}
	}
}
