﻿using System;

namespace Qoo.Ks
{
	// Token: 0x02000058 RID: 88
	internal class KsTagInfo_SE : KsTagInfo
	{
		// Token: 0x060002D1 RID: 721 RVA: 0x00009D5C File Offset: 0x00007F5C
		public KsTagInfo_SE()
		{
			this.SeNo = -1;
		}

		// Token: 0x17000088 RID: 136
		// (get) Token: 0x060002D2 RID: 722 RVA: 0x00009D6C File Offset: 0x00007F6C
		// (set) Token: 0x060002D3 RID: 723 RVA: 0x00009D74 File Offset: 0x00007F74
		protected int SeNo { get; set; }

		// Token: 0x060002D4 RID: 724 RVA: 0x00009D80 File Offset: 0x00007F80
		public override KSID GetId()
		{
			return KSID.SE;
		}

		// Token: 0x060002D5 RID: 725 RVA: 0x00009D84 File Offset: 0x00007F84
		public override bool IsRevSceneApply()
		{
			return true;
		}

		// Token: 0x060002D6 RID: 726 RVA: 0x00009D88 File Offset: 0x00007F88
		public override TAG_RESULT Run(EventPlayer player, TagData tag)
		{
			bool paramBool = tag.GetParamBool(KSID.LOOP, false);
			int paramInt = tag.GetParamInt(KSID.VOLUME, 100);
			ParamData param;
			if ((param = tag.GetParam(KSID.FILE)) != null)
			{
				if (!player.RestoreOn || paramBool)
				{
					player.Scene.EntrySe(true, (this.SeNo >= 0) ? this.SeNo : 0, param.Param, paramInt, paramBool);
				}
				else if (player.RestoreOn && !paramBool)
				{
					player.Scene.EntrySe(false, (this.SeNo >= 0) ? this.SeNo : 0, string.Empty, 100, false);
				}
			}
			else if (this.SeNo < 0)
			{
				for (int num = 0; num != 2; num++)
				{
					player.Scene.EntrySe(false, num, string.Empty, 100, false);
				}
			}
			else
			{
				player.Scene.EntrySe(false, this.SeNo, string.Empty, 100, false);
			}
			return TAG_RESULT.NEXT_EXIT;
		}
	}
}
