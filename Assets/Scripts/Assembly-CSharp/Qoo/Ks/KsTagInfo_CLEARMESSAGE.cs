﻿using System;

namespace Qoo.Ks
{
	// Token: 0x0200004A RID: 74
	internal class KsTagInfo_CLEARMESSAGE : KsTagInfo
	{
		// Token: 0x060002A7 RID: 679 RVA: 0x00009880 File Offset: 0x00007A80
		public override KSID GetId()
		{
			return KSID.CLEARMESSAGE;
		}

		// Token: 0x060002A8 RID: 680 RVA: 0x00009884 File Offset: 0x00007A84
		public override bool IsRevSceneApply()
		{
			return true;
		}

		// Token: 0x060002A9 RID: 681 RVA: 0x00009888 File Offset: 0x00007A88
		public override bool IsRevContinue()
		{
			return false;
		}

		// Token: 0x060002AA RID: 682 RVA: 0x0000988C File Offset: 0x00007A8C
		public override TAG_RESULT Run(EventPlayer player, TagData tag)
		{
			player.Scene.EntryMessage(false, null, null, 0, false);
			player.Scene.EntryFace(false, string.Empty);
			base.CheckEffectParam(player, tag);
			return TAG_RESULT.NEXT_EXIT;
		}
	}
}
