﻿using System;
using Qoo.Memory;

namespace Qoo.Ks
{
	// Token: 0x02000088 RID: 136
	public class EVENTSCENE_MSG
	{
		// Token: 0x060003D4 RID: 980 RVA: 0x0000D4D4 File Offset: 0x0000B6D4
		public EVENTSCENE_MSG()
		{
			this.Name = new EVENTECENR_NAME();
			this.Init();
		}

		// Token: 0x1700008F RID: 143
		// (get) Token: 0x060003D5 RID: 981 RVA: 0x0000D4F0 File Offset: 0x0000B6F0
		// (set) Token: 0x060003D6 RID: 982 RVA: 0x0000D4F8 File Offset: 0x0000B6F8
		public EVENTECENR_NAME Name { get; set; }

		// Token: 0x060003D7 RID: 983 RVA: 0x0000D504 File Offset: 0x0000B704
		public void Init()
		{
			this.Sw = SWITCH.NULL;
			this.Name.Init();
			this.nFrameType = 0;
			this.Tag = null;
			this.bAddLog = false;
		}

		// Token: 0x060003D8 RID: 984 RVA: 0x0000D530 File Offset: 0x0000B730
		public void Reset()
		{
			this.Sw = SWITCH.NULL;
		}

		// Token: 0x060003D9 RID: 985 RVA: 0x0000D53C File Offset: 0x0000B73C
		public void Copy(EVENTSCENE_MSG other)
		{
			this.Sw = other.Sw;
			this.Name.Copy(other.Name);
			this.nFrameType = other.nFrameType;
			this.Tag = other.Tag;
			this.bAddLog = other.bAddLog;
		}

		// Token: 0x060003DA RID: 986 RVA: 0x0000D58C File Offset: 0x0000B78C
		public void Save(MemFile conv)
		{
			this.Name.Save(conv);
		}

		// Token: 0x060003DB RID: 987 RVA: 0x0000D59C File Offset: 0x0000B79C
		public void Load(MemFile conv)
		{
			this.Name.Load(conv);
		}

		// Token: 0x060003DC RID: 988 RVA: 0x0000D5AC File Offset: 0x0000B7AC
		public void SetLog(SCENELOGDATA log, bool bAddLog_)
		{
			if (log.Msg != null)
			{
				this.Sw = SWITCH.ON;
				this.Tag = log.Msg;
				for (int num = 0; num != this.Name.Name.Length; num++)
				{
					this.Name.Name[num] = log.aszMsgName[num];
				}
				this.nFrameType = (int)log.nMsgFrameType;
				this.bAddLog = bAddLog_;
			}
			else
			{
				this.Sw = SWITCH.OFF;
				this.Tag = null;
				this.Name.Init();
				this.nFrameType = 0;
			}
		}

		// Token: 0x040002A4 RID: 676
		public SWITCH Sw;

		// Token: 0x040002A5 RID: 677
		public int nFrameType;

		// Token: 0x040002A6 RID: 678
		public TagData Tag;

		// Token: 0x040002A7 RID: 679
		public bool bAddLog;
	}
}
