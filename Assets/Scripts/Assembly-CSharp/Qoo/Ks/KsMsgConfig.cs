﻿using System;
using UnityEngine;

namespace Qoo.Ks
{
	// Token: 0x02000039 RID: 57
	public class KsMsgConfig
	{
		// Token: 0x17000042 RID: 66
		// (get) Token: 0x060001C9 RID: 457 RVA: 0x00008220 File Offset: 0x00006420
		// (set) Token: 0x060001CA RID: 458 RVA: 0x00008228 File Offset: 0x00006428
		public string StyleName { get; set; }

		// Token: 0x17000043 RID: 67
		// (get) Token: 0x060001CB RID: 459 RVA: 0x00008234 File Offset: 0x00006434
		// (set) Token: 0x060001CC RID: 460 RVA: 0x0000823C File Offset: 0x0000643C
		public Vector3 Frame { get; set; }

		// Token: 0x17000044 RID: 68
		// (get) Token: 0x060001CD RID: 461 RVA: 0x00008248 File Offset: 0x00006448
		// (set) Token: 0x060001CE RID: 462 RVA: 0x00008250 File Offset: 0x00006450
		public Vector3 Name { get; set; }

		// Token: 0x17000045 RID: 69
		// (get) Token: 0x060001CF RID: 463 RVA: 0x0000825C File Offset: 0x0000645C
		// (set) Token: 0x060001D0 RID: 464 RVA: 0x00008264 File Offset: 0x00006464
		public Vector3 Name2 { get; set; }

		// Token: 0x17000046 RID: 70
		// (get) Token: 0x060001D1 RID: 465 RVA: 0x00008270 File Offset: 0x00006470
		// (set) Token: 0x060001D2 RID: 466 RVA: 0x00008278 File Offset: 0x00006478
		public Vector3 NameText { get; set; }

		// Token: 0x17000047 RID: 71
		// (get) Token: 0x060001D3 RID: 467 RVA: 0x00008284 File Offset: 0x00006484
		// (set) Token: 0x060001D4 RID: 468 RVA: 0x0000828C File Offset: 0x0000648C
		public Vector3 NameText2 { get; set; }

		// Token: 0x17000048 RID: 72
		// (get) Token: 0x060001D5 RID: 469 RVA: 0x00008298 File Offset: 0x00006498
		// (set) Token: 0x060001D6 RID: 470 RVA: 0x000082A0 File Offset: 0x000064A0
		public Vector3 Mark { get; set; }

		// Token: 0x17000049 RID: 73
		// (get) Token: 0x060001D7 RID: 471 RVA: 0x000082AC File Offset: 0x000064AC
		// (set) Token: 0x060001D8 RID: 472 RVA: 0x000082B4 File Offset: 0x000064B4
		public Vector3 Text { get; set; }

		// Token: 0x1700004A RID: 74
		// (get) Token: 0x060001D9 RID: 473 RVA: 0x000082C0 File Offset: 0x000064C0
		// (set) Token: 0x060001DA RID: 474 RVA: 0x000082C8 File Offset: 0x000064C8
		public Vector2 TextSize { get; set; }

		// Token: 0x1700004B RID: 75
		// (get) Token: 0x060001DB RID: 475 RVA: 0x000082D4 File Offset: 0x000064D4
		// (set) Token: 0x060001DC RID: 476 RVA: 0x000082DC File Offset: 0x000064DC
		public float TextEx { get; set; }

		// Token: 0x1700004C RID: 76
		// (get) Token: 0x060001DD RID: 477 RVA: 0x000082E8 File Offset: 0x000064E8
		// (set) Token: 0x060001DE RID: 478 RVA: 0x000082F0 File Offset: 0x000064F0
		public Vector3 Face { get; set; }
	}
}
