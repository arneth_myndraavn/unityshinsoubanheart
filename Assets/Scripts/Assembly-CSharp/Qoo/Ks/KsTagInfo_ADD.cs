﻿using System;
using Qoo.Game;

namespace Qoo.Ks
{
	// Token: 0x02000074 RID: 116
	internal class KsTagInfo_ADD : KsTagInfo
	{
		// Token: 0x06000343 RID: 835 RVA: 0x0000AC5C File Offset: 0x00008E5C
		public override KSID GetId()
		{
			return KSID.ADD;
		}

		// Token: 0x06000344 RID: 836 RVA: 0x0000AC60 File Offset: 0x00008E60
		public override TAG_RESULT Run(EventPlayer player, TagData tag)
		{
			if (player.IsParamLock)
			{
				return TAG_RESULT.NEXT;
			}
			string paramString = tag.GetParamString(KSID.VALUE, string.Empty);
			string paramString2 = tag.GetParamString(KSID.INDEX, string.Empty);
			string paramString3 = tag.GetParamString(KSID.PARAM, string.Empty);
			GameData.AddParam(paramString, paramString2, paramString3);
			return TAG_RESULT.NEXT;
		}
	}
}
