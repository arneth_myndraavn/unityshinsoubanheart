﻿using System;
using Qoo.Game;

namespace Qoo.Ks
{
	// Token: 0x02000052 RID: 82
	internal class KsTagInfo_CG : KsTagInfo
	{
		// Token: 0x060002BF RID: 703 RVA: 0x00009B58 File Offset: 0x00007D58
		public KsTagInfo_CG()
		{
			this.CgNo = -1;
		}

		// Token: 0x17000087 RID: 135
		// (get) Token: 0x060002C0 RID: 704 RVA: 0x00009B68 File Offset: 0x00007D68
		// (set) Token: 0x060002C1 RID: 705 RVA: 0x00009B70 File Offset: 0x00007D70
		protected int CgNo { get; set; }

		// Token: 0x060002C2 RID: 706 RVA: 0x00009B7C File Offset: 0x00007D7C
		public override KSID GetId()
		{
			return KSID.CG;
		}

		// Token: 0x060002C3 RID: 707 RVA: 0x00009B80 File Offset: 0x00007D80
		public override bool IsRevSceneApply()
		{
			return true;
		}

		// Token: 0x060002C4 RID: 708 RVA: 0x00009B84 File Offset: 0x00007D84
		public override TAG_RESULT Run(EventPlayer player, TagData tag)
		{
			int paramInt = tag.GetParamInt(KSID.LAYER, -1);
			int num = (int)((float)tag.GetParamInt(KSID.POSX, 0));
			int num2 = (int)((float)tag.GetParamInt(KSID.POSY, 0));
			int num3 = (int)((float)tag.GetParamInt(KSID.SX, num));
			int num4 = (int)((float)tag.GetParamInt(KSID.SY, num2));
			num = (int)((float)num * 2f);
			num2 = (int)((float)num2 * 2f);
			num3 = (int)((float)num3 * 2f);
			num4 = (int)((float)num4 * 2f);
			int paramInt2 = tag.GetParamInt(KSID.TIME, 500);
			ParamData param;
			if ((param = tag.GetParam(KSID.FILE)) != null)
			{
				player.Scene.EntryCg(true, (this.CgNo == -1) ? 0 : this.CgNo, param.Param, num, num2, num3, num4, paramInt2, paramInt);
				SysData.SetReadCG(param.Param);
			}
			else
			{
				player.Scene.EntryCg(false, this.CgNo, string.Empty, 0, 0, 0, 0, 0, -1);
			}
			base.CheckEffectParam(player, tag);
			return TAG_RESULT.NEXT_EXIT;
		}
	}
}
