﻿using System;

namespace Qoo.Ks
{
	// Token: 0x02000049 RID: 73
	internal class KsTagInfo_BACKLOG : KsTagInfo_MESSAGE
	{
		// Token: 0x060002A4 RID: 676 RVA: 0x00009864 File Offset: 0x00007A64
		public KsTagInfo_BACKLOG()
		{
			base.IsSilent = true;
		}

		// Token: 0x060002A5 RID: 677 RVA: 0x00009874 File Offset: 0x00007A74
		public override KSID GetId()
		{
			return KSID.BACKLOG;
		}
	}
}
