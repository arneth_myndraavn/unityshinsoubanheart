﻿using System;
using Qoo.Memory;

namespace Qoo.Ks
{
	// Token: 0x02000085 RID: 133
	public class EVENTSCENE_SE
	{
		// Token: 0x060003C0 RID: 960 RVA: 0x0000D100 File Offset: 0x0000B300
		public EVENTSCENE_SE()
		{
			this.Init();
		}

		// Token: 0x060003C1 RID: 961 RVA: 0x0000D110 File Offset: 0x0000B310
		public void Reset()
		{
			this.Sw = SWITCH.NULL;
		}

		// Token: 0x060003C2 RID: 962 RVA: 0x0000D11C File Offset: 0x0000B31C
		public void Init()
		{
			this.Name = string.Empty;
			this.Sw = SWITCH.NULL;
			this.nVolume = 0;
			this.nLoop = 0;
		}

		// Token: 0x060003C3 RID: 963 RVA: 0x0000D14C File Offset: 0x0000B34C
		public void Copy(EVENTSCENE_SE other)
		{
			this.Name = other.Name;
			this.Sw = other.Sw;
			this.nVolume = other.nVolume;
			this.nLoop = other.nLoop;
		}

		// Token: 0x060003C4 RID: 964 RVA: 0x0000D18C File Offset: 0x0000B38C
		public void SetLog(SWITCH sw_, string name_ = "", int nLoop_ = 0, int nVolume_ = 100)
		{
			this.Sw = sw_;
			this.Name = name_;
			this.nLoop = nLoop_;
			this.nVolume = nVolume_;
		}

		// Token: 0x060003C5 RID: 965 RVA: 0x0000D1AC File Offset: 0x0000B3AC
		public void Save(MemFile conv)
		{
			conv.SetStringUtf16(this.Name);
		}

		// Token: 0x060003C6 RID: 966 RVA: 0x0000D1BC File Offset: 0x0000B3BC
		public void Load(MemFile conv)
		{
			this.Name = conv.GetStringUtf16();
		}

		// Token: 0x04000299 RID: 665
		public string Name;

		// Token: 0x0400029A RID: 666
		public SWITCH Sw;

		// Token: 0x0400029B RID: 667
		public int nVolume;

		// Token: 0x0400029C RID: 668
		public int nLoop;
	}
}
