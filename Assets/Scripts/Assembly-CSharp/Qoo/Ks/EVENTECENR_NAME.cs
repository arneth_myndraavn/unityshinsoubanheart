﻿using System;
using Qoo.Memory;

namespace Qoo.Ks
{
	// Token: 0x02000087 RID: 135
	public class EVENTECENR_NAME
	{
		// Token: 0x060003CF RID: 975 RVA: 0x0000D3E4 File Offset: 0x0000B5E4
		public EVENTECENR_NAME()
		{
			this.Name = new string[2];
			this.Init();
		}

		// Token: 0x060003D0 RID: 976 RVA: 0x0000D400 File Offset: 0x0000B600
		public void Init()
		{
			for (int i = 0; i < this.Name.Length; i++)
			{
				this.Name[i] = string.Empty;
			}
		}

		// Token: 0x060003D1 RID: 977 RVA: 0x0000D434 File Offset: 0x0000B634
		public void Copy(EVENTECENR_NAME other)
		{
			for (int num = 0; num != this.Name.Length; num++)
			{
				this.Name[num] = other.Name[num];
			}
		}

		// Token: 0x060003D2 RID: 978 RVA: 0x0000D46C File Offset: 0x0000B66C
		public void Save(MemFile conv)
		{
			for (int i = 0; i < this.Name.Length; i++)
			{
				conv.SetStringUtf16(this.Name[i]);
			}
		}

		// Token: 0x060003D3 RID: 979 RVA: 0x0000D4A0 File Offset: 0x0000B6A0
		public void Load(MemFile conv)
		{
			for (int i = 0; i < this.Name.Length; i++)
			{
				this.Name[i] = conv.GetStringUtf16();
			}
		}

		// Token: 0x040002A3 RID: 675
		public string[] Name;
	}
}
