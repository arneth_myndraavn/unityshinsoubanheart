﻿using System;
using Qoo.Game;

namespace Qoo.Ks
{
	// Token: 0x0200005C RID: 92
	internal class KsTagInfo_SHAKE : KsTagInfo
	{
		// Token: 0x060002E1 RID: 737 RVA: 0x00009F58 File Offset: 0x00008158
		public override KSID GetId()
		{
			return KSID.SHAKE;
		}

		// Token: 0x060002E2 RID: 738 RVA: 0x00009F5C File Offset: 0x0000815C
		public override TAG_RESULT Run(EventPlayer player, TagData tag)
		{
			if (player.RestoreOn)
			{
				return TAG_RESULT.NEXT_EXIT;
			}
			if (GameData.CheckReadedSkip(player.CurKsNo, player.CurLabelNo, player.CurTagNo))
			{
				return TAG_RESULT.NEXT_EXIT;
			}
			int paramInt = tag.GetParamInt(KSID.TIME, 1000);
			for (int i = 0; i < 8; i++)
			{
				if (i != 6)
				{
					player.Scene.GrpList[i].Effect |= 1;
				}
			}
			UnityTask.SetSubTask(Singleton<UnityGraph>.Instance.Shake((float)paramInt / 1000f, 10, 10));
			return TAG_RESULT.NEXT_EXIT;
		}
	}
}
