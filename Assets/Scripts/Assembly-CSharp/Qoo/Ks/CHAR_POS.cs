﻿using System;

namespace Qoo.Ks
{
	// Token: 0x02000036 RID: 54
	internal enum CHAR_POS
	{
		// Token: 0x04000167 RID: 359
		LEFT,
		// Token: 0x04000168 RID: 360
		CENTER_LEFT,
		// Token: 0x04000169 RID: 361
		CENTER,
		// Token: 0x0400016A RID: 362
		CENTER_RIGHT,
		// Token: 0x0400016B RID: 363
		RIGHT,
		// Token: 0x0400016C RID: 364
		NUM
	}
}
