﻿using System;

namespace Qoo.Ks
{
	// Token: 0x0200005F RID: 95
	internal class KsTagInfo_SELECT : KsTagInfo
	{
		// Token: 0x060002EC RID: 748 RVA: 0x0000A2BC File Offset: 0x000084BC
		public override KSID GetId()
		{
			return KSID.SELECT;
		}

		// Token: 0x060002ED RID: 749 RVA: 0x0000A2C0 File Offset: 0x000084C0
		public override TAG_RESULT Run(EventPlayer player, TagData tag)
		{
			string paramString = tag.GetParamString(KSID.NAME, string.Empty);
			string paramString2 = tag.GetParamString(KSID.WORD, string.Empty);
			string text = (paramString.Length <= 0) ? paramString2 : paramString;
			string paramString3 = tag.GetParamString(KSID.FILE, string.Empty);
			string paramString4 = tag.GetParamString(KSID.TARGET, string.Empty);
			string paramString5 = tag.GetParamString(KSID.EFFECT, string.Empty);
			int nType = (paramString.Length <= 0) ? 0 : 1;
			Debug.Print(string.Format("Select:{0}", text));
			if (player.Scene.GetSelectNum() == 0)
			{
				player.SnapShot(true);
			}
			if (!player.Scene.IsEntrySelect(text, paramString3, paramString4))
			{
				player.Scene.EntrySelect(player.Scene.GetSelectNum(), text, paramString3, paramString4, nType, paramString5);
			}
			return TAG_RESULT.NEXT_EXIT;
		}
	}
}
