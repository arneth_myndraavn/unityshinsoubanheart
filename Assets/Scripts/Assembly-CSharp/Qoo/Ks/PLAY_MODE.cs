﻿using System;

namespace Qoo.Ks
{
	// Token: 0x02000034 RID: 52
	public enum PLAY_MODE
	{
		// Token: 0x04000158 RID: 344
		NORMAL,
		// Token: 0x04000159 RID: 345
		MEMORY,
		// Token: 0x0400015A RID: 346
		MINIGAME
	}
}
