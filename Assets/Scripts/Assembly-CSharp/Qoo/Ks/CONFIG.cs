﻿using System;

namespace Qoo.Ks
{
	// Token: 0x0200003B RID: 59
	public static class CONFIG
	{
		// Token: 0x060001EA RID: 490 RVA: 0x00008380 File Offset: 0x00006580
		static CONFIG()
		{
			CONFIG.LABEL_MAX = 200;
			CONFIG.KS_MAX = 200;
		}

		// Token: 0x17000052 RID: 82
		// (get) Token: 0x060001EB RID: 491 RVA: 0x00008398 File Offset: 0x00006598
		// (set) Token: 0x060001EC RID: 492 RVA: 0x000083A0 File Offset: 0x000065A0
		public static int LABEL_MAX { get; private set; }

		// Token: 0x17000053 RID: 83
		// (get) Token: 0x060001ED RID: 493 RVA: 0x000083A8 File Offset: 0x000065A8
		// (set) Token: 0x060001EE RID: 494 RVA: 0x000083B0 File Offset: 0x000065B0
		public static int KS_MAX { get; private set; }
	}
}
