﻿using System;
using System.Collections.Generic;

namespace Qoo.Ks
{
	// Token: 0x02000079 RID: 121
	public class KsTagTable
	{
		// Token: 0x06000354 RID: 852 RVA: 0x0000B094 File Offset: 0x00009294
		public static KSID GetID(string v)
		{
			KSID result;
			try
			{
				result = (KSID)((int)Enum.Parse(typeof(KSID), v, true));
			}
			catch (ArgumentException)
			{
				result = KSID.UNKNOWN;
			}
			return result;
		}

		// Token: 0x06000355 RID: 853 RVA: 0x0000B0EC File Offset: 0x000092EC
		public static KsTagInfo GetInfo(KSID id)
		{
			return KsTagTable._tagDir[id];
		}

		// Token: 0x06000356 RID: 854 RVA: 0x0000B0FC File Offset: 0x000092FC
		public static bool IsTag(KSID id)
		{
			return KsTagTable.GetInfo(id) != null;
		}

		// Token: 0x06000357 RID: 855 RVA: 0x0000B10C File Offset: 0x0000930C
		public static bool IsParam(KSID id)
		{
			return KsTagTable.GetInfo(id) == null;
		}

		// Token: 0x0400025A RID: 602
		private static Dictionary<KSID, KsTagInfo> _tagDir = new Dictionary<KSID, KsTagInfo>
		{
			{
				KSID.MESSAGE,
				new KsTagInfo_MESSAGE()
			},
			{
				KSID.BACKLOG,
				new KsTagInfo_BACKLOG()
			},
			{
				KSID.CLEARMESSAGE,
				new KsTagInfo_CLEARMESSAGE()
			},
			{
				KSID.BGIMAGE,
				new KsTagInfo_BGIMAGE()
			},
			{
				KSID.CHARA,
				new KsTagInfo_CHARA()
			},
			{
				KSID.CHARA1,
				new KsTagInfo_CHARA1()
			},
			{
				KSID.CHARA2,
				new KsTagInfo_CHARA2()
			},
			{
				KSID.CHARA3,
				new KsTagInfo_CHARA3()
			},
			{
				KSID.CHARA4,
				new KsTagInfo_CHARA4()
			},
			{
				KSID.CHARA5,
				new KsTagInfo_CHARA5()
			},
			{
				KSID.CG,
				new KsTagInfo_CG()
			},
			{
				KSID.CG1,
				new KsTagInfo_CG1()
			},
			{
				KSID.CG2,
				new KsTagInfo_CG2()
			},
			{
				KSID.CG3,
				new KsTagInfo_CG3()
			},
			{
				KSID.CG4,
				new KsTagInfo_CG4()
			},
			{
				KSID.BGM,
				new KsTagInfo_BGM()
			},
			{
				KSID.SE,
				new KsTagInfo_SE()
			},
			{
				KSID.SE1,
				new KsTagInfo_SE1()
			},
			{
				KSID.SE2,
				new KsTagInfo_SE2()
			},
			{
				KSID.MOVIE,
				new KsTagInfo_MOVIE()
			},
			{
				KSID.SHAKE,
				new KsTagInfo_SHAKE()
			},
			{
				KSID.FLASH,
				new KsTagInfo_FLASH()
			},
			{
				KSID.ANIMATION,
				new KsTagInfo_ANIMATION()
			},
			{
				KSID.SELECT,
				new KsTagInfo_SELECT()
			},
			{
				KSID.STOP,
				new KsTagInfo_STOP()
			},
			{
				KSID.STOP_CALL,
				new KsTagInfo_STOP_CALL()
			},
			{
				KSID.WAIT,
				new KsTagInfo_WAIT()
			},
			{
				KSID.STOPRW,
				new KsTagInfo_STOPRW()
			},
			{
				KSID.NEXT,
				new KsTagInfo_NEXT()
			},
			{
				KSID.CALL,
				new KsTagInfo_CALL()
			},
			{
				KSID.RETURN,
				new KsTagInfo_RETURN()
			},
			{
				KSID.IF_PARAM_NEXT,
				new KsTagInfo_IF_PARAM_NEXT()
			},
			{
				KSID.IF_PARAM_CALL,
				new KsTagInfo_IF_PARAM_CALL()
			},
			{
				KSID.IF_SYS_READED_NEXT,
				new KsTagInfo_IF_SYS_READED_NEXT()
			},
			{
				KSID.IF_SYS_READED_CALL,
				new KsTagInfo_IF_SYS_READED_CALL()
			},
			{
				KSID.IF_READED_NEXT,
				new KsTagInfo_IF_READED_NEXT()
			},
			{
				KSID.IF_READED_CALL,
				new KsTagInfo_IF_READED_CALL()
			},
			{
				KSID.IF_COMPARE_NEXT,
				new KsTagInfo_IF_COMPARE_NEXT()
			},
			{
				KSID.IF_COMPARE_CALL,
				new KsTagInfo_IF_COMPARE_CALL()
			},
			{
				KSID.ENDMEMORY,
				new KsTagInfo_ENDMEMORY()
			},
			{
				KSID.END,
				new KsTagInfo_END()
			},
			{
				KSID.EXIT,
				new KsTagInfo_EXIT()
			},
			{
				KSID.EXEC,
				new KsTagInfo_EXEC()
			},
			{
				KSID.SET,
				new KsTagInfo_SET()
			},
			{
				KSID.ADD,
				new KsTagInfo_ADD()
			},
			{
				KSID.CMD,
				new KsTagInfo_CMD()
			},
			{
				KSID.BR,
				new KsTagInfo_BR()
			},
			{
				KSID.F,
				new KsTagInfo_F()
			},
			{
				KSID.PRINT,
				new KsTagInfo_PRINT()
			}
		};
	}
}
