﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Qoo.File;

namespace Qoo.Ks
{
	// Token: 0x0200003C RID: 60
	public class KsData
	{
		// Token: 0x060001F0 RID: 496 RVA: 0x000083D8 File Offset: 0x000065D8
		public IEnumerator LoadKsAll()
		{
			for (int i = 0; i != Akb.GetFileNum(); i++)
			{
				AKB_FILEINFO info = Akb.GetFileInfo(i);
				string name = info.Name;
				string path = Akb.GetDirInfo((int)info.nDirNo).Name;
				UnityFile file = Singleton<UnityFileLoader>.Instance.LoadKsFile(path + "/" + name + ".ks");
				while (!file.IsReadEnd)
				{
					yield return 0;
				}
				string script = Encoding.Unicode.GetString(file.Data);
				this.m_DataList.Add(name, script);
			}
			yield break;
		}

		// Token: 0x060001F1 RID: 497 RVA: 0x000083F4 File Offset: 0x000065F4
		public IEnumerator LoadKs(string ksname)
		{
			Debug.Print("BeforeLoad:KS:" + ksname);
			FileId id = new FileId(ksname);
			int no = Akb.SearchFile(id.Name);
			AKB_FILEINFO info = Akb.GetFileInfo(no);
			string name = info.Name;
			if (!this.m_DataList.ContainsKey(name))
			{
				string path = Akb.GetDirInfo((int)info.nDirNo).Name;
				UnityFile file = Singleton<UnityFileLoader>.Instance.LoadKsFile(path + "/" + name + ".ks");
				while (!file.IsReadEnd)
				{
					yield return 0;
				}
				string script = Encoding.Unicode.GetString(file.Data);
				this.m_DataList.Add(name, script);
			}
			yield break;
		}

		// Token: 0x060001F2 RID: 498 RVA: 0x00008420 File Offset: 0x00006620
		public bool Add(string name)
		{
			if (!this.m_DataList.ContainsKey(name))
			{
				Debug.Print(string.Format("ERROR:KSファイルがロードされていません:{0}", name));
				return false;
			}
			string str = this.m_DataList[name];
			if (this.IsItem(name))
			{
				return true;
			}
			Debug.Print(string.Format("INFO:Ksファイルの解析:{0}", name));
			TagReader tagReader = new TagReader();
			tagReader.Create(name, str);
			this.m_List.Add(name, tagReader);
			return true;
		}

		// Token: 0x060001F3 RID: 499 RVA: 0x00008498 File Offset: 0x00006698
		public bool Clear()
		{
			this.m_List.Clear();
			return true;
		}

		// Token: 0x060001F4 RID: 500 RVA: 0x000084A8 File Offset: 0x000066A8
		public bool Remove(string name)
		{
			if (!this.IsItem(name))
			{
				return false;
			}
			this.m_List.Remove(name);
			return true;
		}

		// Token: 0x060001F5 RID: 501 RVA: 0x000084C8 File Offset: 0x000066C8
		public bool IsItem(string name)
		{
			return this.m_List.ContainsKey(name);
		}

		// Token: 0x060001F6 RID: 502 RVA: 0x000084D8 File Offset: 0x000066D8
		public TagReader Get(string name)
		{
			if (!this.IsItem(name))
			{
				Debug.Assert(false, string.Format("Error:KsData:NotFound:{0}\n", name));
				return null;
			}
			return this.m_List[name];
		}

		// Token: 0x060001F7 RID: 503 RVA: 0x00008508 File Offset: 0x00006708
		public void CloseNoCurrent(string name)
		{
			TagReader tagReader = this.Get(name);
			if (tagReader != null)
			{
				List<string> list = new List<string>(tagReader.NextKsList);
				for (int num = 0; num != list.Count; num++)
				{
					FileId fileId = new FileId(list[num]);
					int nFileNo = Akb.SearchFile(fileId.Name);
					AKB_FILEINFO fileInfo = Akb.GetFileInfo(nFileNo);
					list[num] = fileInfo.Name;
				}
				list.Add(name);
				List<string> list3 = new List<string>(this.m_List.Keys);
				List<string> list2 = new List<string>(this.m_DataList.Keys);
				list3.RemoveAll((string s) => list.Contains(s));
				list2.RemoveAll((string s) => list.Contains(s));
				foreach (string key in list3)
				{
					this.m_List.Remove(key);
				}
				foreach (string key2 in list2)
				{
					this.m_DataList.Remove(key2);
				}
			}
		}

		// Token: 0x040001AF RID: 431
		private Dictionary<string, TagReader> m_List = new Dictionary<string, TagReader>();

		// Token: 0x040001B0 RID: 432
		private Dictionary<string, string> m_DataList = new Dictionary<string, string>();
	}
}
