﻿using System;
using Qoo.Game;

namespace Qoo.Ks
{
	// Token: 0x0200004B RID: 75
	internal class KsTagInfo_BGIMAGE : KsTagInfo
	{
		// Token: 0x060002AC RID: 684 RVA: 0x000098CC File Offset: 0x00007ACC
		public override KSID GetId()
		{
			return KSID.BGIMAGE;
		}

		// Token: 0x060002AD RID: 685 RVA: 0x000098D0 File Offset: 0x00007AD0
		public override bool IsRevSceneApply()
		{
			return true;
		}

		// Token: 0x060002AE RID: 686 RVA: 0x000098D4 File Offset: 0x00007AD4
		public override TAG_RESULT Run(EventPlayer player, TagData tag)
		{
			ParamData param;
			if ((param = tag.GetParam(KSID.FILE)) != null)
			{
				player.Scene.EntryBg(true, param.Param);
				SysData.SetReadCG(param.Param);
			}
			else
			{
				player.Scene.EntryBg(false, string.Empty);
			}
			base.CheckEffectParam(player, tag);
			return TAG_RESULT.NEXT_EXIT;
		}
	}
}
