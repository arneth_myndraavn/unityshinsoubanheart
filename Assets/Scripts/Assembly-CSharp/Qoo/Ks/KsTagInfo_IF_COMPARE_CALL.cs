﻿using System;

namespace Qoo.Ks
{
	// Token: 0x0200006E RID: 110
	internal class KsTagInfo_IF_COMPARE_CALL : KsTagInfo_IF_COMPARE_NEXT
	{
		// Token: 0x06000326 RID: 806 RVA: 0x0000AAC8 File Offset: 0x00008CC8
		public override KSID GetId()
		{
			return KSID.IF_COMPARE_CALL;
		}

		// Token: 0x06000327 RID: 807 RVA: 0x0000AACC File Offset: 0x00008CCC
		public override bool IsCallRet()
		{
			return true;
		}
	}
}
