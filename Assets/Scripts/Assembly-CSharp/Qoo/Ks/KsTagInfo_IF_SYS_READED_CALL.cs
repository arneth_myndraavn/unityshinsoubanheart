﻿using System;

namespace Qoo.Ks
{
	// Token: 0x0200006A RID: 106
	internal class KsTagInfo_IF_SYS_READED_CALL : KsTagInfo_IF_SYS_READED_NEXT
	{
		// Token: 0x06000319 RID: 793 RVA: 0x0000A9B0 File Offset: 0x00008BB0
		public KsTagInfo_IF_SYS_READED_CALL()
		{
			base.IsSys = true;
		}

		// Token: 0x0600031A RID: 794 RVA: 0x0000A9C0 File Offset: 0x00008BC0
		public override KSID GetId()
		{
			return KSID.IF_SYS_READED_CALL;
		}

		// Token: 0x0600031B RID: 795 RVA: 0x0000A9C4 File Offset: 0x00008BC4
		public override bool IsCallRet()
		{
			return true;
		}
	}
}
