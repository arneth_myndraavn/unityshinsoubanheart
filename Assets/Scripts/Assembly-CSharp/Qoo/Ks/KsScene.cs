﻿using System;
using System.Collections;
using Qoo.Application;
using Qoo.Def;
using Qoo.Game;
using Qoo.Graphics;
using Qoo.Input;
using Qoo.Message;
using Qoo.SoundSystem;

namespace Qoo.Ks
{
	// Token: 0x0200007D RID: 125
	public class KsScene
	{
		// Token: 0x1700008A RID: 138
		// (get) Token: 0x06000359 RID: 857 RVA: 0x0000B144 File Offset: 0x00009344
		// (set) Token: 0x0600035A RID: 858 RVA: 0x0000B14C File Offset: 0x0000934C
		public UnitySprite KeyWait { get; set; }

		// Token: 0x1700008B RID: 139
		// (get) Token: 0x0600035B RID: 859 RVA: 0x0000B158 File Offset: 0x00009358
		// (set) Token: 0x0600035C RID: 860 RVA: 0x0000B160 File Offset: 0x00009360
		public UnitySprite SkipMark { get; set; }

		// Token: 0x1700008C RID: 140
		// (get) Token: 0x0600035D RID: 861 RVA: 0x0000B16C File Offset: 0x0000936C
		// (set) Token: 0x0600035E RID: 862 RVA: 0x0000B174 File Offset: 0x00009374
		public UnitySprite[] GrpList { get; private set; }

		// Token: 0x1700008D RID: 141
		// (get) Token: 0x0600035F RID: 863 RVA: 0x0000B180 File Offset: 0x00009380
		// (set) Token: 0x06000360 RID: 864 RVA: 0x0000B188 File Offset: 0x00009388
		public UnitySprite[] CgList { get; private set; }

		// Token: 0x1700008E RID: 142
		// (get) Token: 0x06000361 RID: 865 RVA: 0x0000B194 File Offset: 0x00009394
		// (set) Token: 0x06000362 RID: 866 RVA: 0x0000B19C File Offset: 0x0000939C
		private int m_nSkipFlag { get; set; }

		// Token: 0x06000363 RID: 867 RVA: 0x0000B1A8 File Offset: 0x000093A8
		public bool Init()
		{
			this.Release();
			this.KeyWait = Man2D.Sprite(string.Empty);
			this.KeyWait.SetName("KeyWait");
			this.SkipMark = Man2D.Sprite(string.Empty);
			this.SkipMark.SetName("SkipMark");
			this.GrpList = new UnitySprite[8];
			for (int num = 0; num != this.GrpList.Length; num++)
			{
				this.GrpList[num] = Man2D.Sprite(string.Empty);
				if (num >= 1 && num < 6)
				{
					this.GrpList[num].SetName(string.Format("CHAR{0}", num - 1));
				}
				else
				{
					this.GrpList[num].SetName(((EVSP)num).ToString());
				}
			}
			Singleton<MsgWnd>.Instance.FaceSprite = this.GrpList[6];
			this.CgList = new UnitySprite[4];
			for (int num2 = 0; num2 != this.CgList.Length; num2++)
			{
				this.CgList[num2] = Man2D.Sprite(string.Empty);
				this.CgList[num2].SetName(string.Format("CG{0}", num2));
			}
			this.m_Scene.Init();
			App.QooSelect.Clear();
			return true;
		}

		// Token: 0x06000364 RID: 868 RVA: 0x0000B2F8 File Offset: 0x000094F8
		public void Release()
		{
			this.m_Draw.Clear();
			if (Singleton<Man2D>.IsReady)
			{
				Singleton<Man2D>.Instance.RemoveSprite(this.KeyWait);
				this.KeyWait = null;
				Singleton<Man2D>.Instance.RemoveSprite(this.SkipMark);
				this.SkipMark = null;
				if (this.GrpList != null)
				{
					for (int num = 0; num != this.GrpList.Length; num++)
					{
						Singleton<Man2D>.Instance.RemoveSprite(this.GrpList[num]);
						this.GrpList[num] = null;
					}
				}
				if (this.CgList != null)
				{
					for (int num2 = 0; num2 != this.CgList.Length; num2++)
					{
						Singleton<Man2D>.Instance.RemoveSprite(this.CgList[num2]);
						this.CgList[num2] = null;
					}
				}
			}
		}

		// Token: 0x06000365 RID: 869 RVA: 0x0000B3CC File Offset: 0x000095CC
		private void InitTex()
		{
		}

		// Token: 0x06000366 RID: 870 RVA: 0x0000B3D0 File Offset: 0x000095D0
		public void SceneInit()
		{
			this.m_Scene.Init();
			this.SceneReset();
		}

		// Token: 0x06000367 RID: 871 RVA: 0x0000B3E4 File Offset: 0x000095E4
		public void SceneResetEffect()
		{
			this.m_Scene.Effect.Sw = SWITCH.NULL;
		}

		// Token: 0x06000368 RID: 872 RVA: 0x0000B3F8 File Offset: 0x000095F8
		public void SceneApplyEffect()
		{
			if (this.m_Scene.Effect.Sw != SWITCH.ON)
			{
				return;
			}
			if ((this.m_nSkipFlag & 3) != 0)
			{
				App.FrameUpdate(true);
				return;
			}
			Effect.ApplyKsEffect(this.m_Scene.Effect.Name, this.m_Scene.Effect.nParam);
			KsInput.Pause = false;
		}

		// Token: 0x06000369 RID: 873 RVA: 0x0000B45C File Offset: 0x0000965C
		public void SceneApply(bool IsMsg, bool IsVoice = true)
		{
			this.ReadyGrpFile();
			this.m_Scene = this.SceneApplySub(this.m_Scene, IsMsg, IsVoice);
		}

		// Token: 0x0600036A RID: 874 RVA: 0x0000B478 File Offset: 0x00009678
		private void ReadyGrpFile()
		{
			if (this.m_Scene.Bg.Sw == SWITCH.ON && this.m_Scene.Bg.Name.Length > 0)
			{
				Man2D.ReadyTexture(this.m_Scene.Bg.Name, true);
			}
			foreach (EVENTSCENE_CG eventscene_CG in this.m_Scene.Cg)
			{
				if (eventscene_CG.Sw == SWITCH.ON && eventscene_CG.Name.Length > 0)
				{
					Man2D.ReadyTexture(eventscene_CG.Name, true);
				}
			}
			foreach (EVENTSCENE_OBJ eventscene_OBJ in this.m_Scene.Chr)
			{
				if (eventscene_OBJ.Sw == SWITCH.ON && eventscene_OBJ.Name.Length > 0)
				{
					Man2D.ReadyTexture(eventscene_OBJ.Name, true);
				}
			}
			if (this.m_Scene.Face.Sw == SWITCH.ON && this.m_Scene.Face.Name.Length > 0)
			{
				Man2D.ReadyTexture(this.m_Scene.Face.Name, true);
			}
		}

		// Token: 0x0600036B RID: 875 RVA: 0x0000B5B8 File Offset: 0x000097B8
		public void SceneReset()
		{
			this.m_Scene.Reset();
			this.SetSkipFlag(0);
		}

		// Token: 0x0600036C RID: 876 RVA: 0x0000B5CC File Offset: 0x000097CC
		public IEnumerator Move()
		{
			int start = App.GetTimeMilli();
			EVENTSCENE scene = new EVENTSCENE();
			scene.Copy(this.m_Scene);
			for (int i = 0; i != 4; i++)
			{
				this.m_Scene.Cg[i].SwMove = SWITCH.NULL;
			}
			for (;;)
			{
				bool bCoutinue = false;
				if (KsInput.IsTrig)
				{
					break;
				}
				float now = (float)(App.GetTimeMilli() - start);
				for (int j = 0; j != 4; j++)
				{
					if (scene.Cg[j].SwMove == SWITCH.ON && scene.Cg[j].Name.Length > 0 && this.MoveCg(ref this.CgList[j], ref scene.Cg[j], now))
					{
						bCoutinue = true;
					}
				}
				if (!bCoutinue)
				{
					break;
				}
				yield return UnityTask.FrameUpdate();
			}
			for (int k = 0; k != 4; k++)
			{
				this.CgList[k].x = (int)scene.Cg[k].nX;
				this.CgList[k].y = (int)scene.Cg[k].nY;
			}
			yield break;
		}

		// Token: 0x0600036D RID: 877 RVA: 0x0000B5E8 File Offset: 0x000097E8
		public void SceneApplyCg(bool IsMove)
		{
			if (IsMove)
			{
				if ((this.m_nSkipFlag & 2) == 0 && this.IsMoveCg())
				{
					UnityTask.SetSubTask(this.Move());
				}
			}
			else
			{
				for (int num = 0; num != 4; num++)
				{
					this.ResetPosCg(num);
				}
			}
		}

		// Token: 0x0600036E RID: 878 RVA: 0x0000B640 File Offset: 0x00009840
		private bool IsEffect()
		{
			return this.m_Scene.Effect.Sw == SWITCH.ON && (this.m_nSkipFlag & 3) == 0;
		}

		// Token: 0x0600036F RID: 879 RVA: 0x0000B678 File Offset: 0x00009878
		private bool IsMoveCg()
		{
			for (int num = 0; num != 4; num++)
			{
				if (this.m_Scene.Cg[num].SwMove == SWITCH.ON)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000370 RID: 880 RVA: 0x0000B6B4 File Offset: 0x000098B4
		public void Backup()
		{
			this.BackupData.Scene.Copy(this.m_Scene);
		}

		// Token: 0x06000371 RID: 881 RVA: 0x0000B6CC File Offset: 0x000098CC
		public void ApplyBackup(EVENTBACKUPDATA data)
		{
			this.BackupData = data;
			this.BackupData.Scene.Face.Sw = ((this.BackupData.Scene.Face.Name.Length <= 0) ? SWITCH.OFF : SWITCH.ON);
			this.BackupData.Scene.Bg.Sw = ((this.BackupData.Scene.Bg.Name.Length <= 0) ? SWITCH.OFF : SWITCH.ON);
			for (int num = 0; num != this.BackupData.Scene.Cg.Length; num++)
			{
				this.BackupData.Scene.Cg[num].Sw = ((this.BackupData.Scene.Cg[num].Name.Length <= 0) ? SWITCH.OFF : SWITCH.ON);
			}
			for (int num2 = 0; num2 != this.BackupData.Scene.Chr.Length; num2++)
			{
				this.BackupData.Scene.Chr[num2].Sw = ((this.BackupData.Scene.Chr[num2].Name.Length <= 0) ? SWITCH.OFF : SWITCH.ON);
			}
			this.BackupData.Scene.Bgm.Sw = ((this.BackupData.Scene.Bgm.Name.Length <= 0) ? SWITCH.OFF : SWITCH.ON);
			for (int num3 = 0; num3 != this.BackupData.Scene.Se.Length; num3++)
			{
				this.BackupData.Scene.Se[num3].Sw = ((this.BackupData.Scene.Se[num3].nLoop <= 0 || this.BackupData.Scene.Se[num3].Name.Length <= 0) ? SWITCH.OFF : SWITCH.ON);
			}
			this.m_Scene.Copy(this.BackupData.Scene);
			this.EntryMessage(false, null, null, 0, false);
		}

		// Token: 0x06000372 RID: 882 RVA: 0x0000B8FC File Offset: 0x00009AFC
		public bool SaveSceneLog(ref SCENELOGDATA log)
		{
			return this.m_Scene.SaveLog(ref log);
		}

		// Token: 0x06000373 RID: 883 RVA: 0x0000B90C File Offset: 0x00009B0C
		public void SceneApplyLog(SCENELOGDATA pLog, bool bMsg, bool bAddLog = false)
		{
			if (!pLog.nExist)
			{
				return;
			}
			this.SceneSetLog(pLog, bAddLog);
			this.SceneApply(bMsg, false);
			if (bMsg)
			{
				Singleton<MsgWnd>.Instance.Full();
			}
		}

		// Token: 0x06000374 RID: 884 RVA: 0x0000B948 File Offset: 0x00009B48
		public void SceneSetLog(SCENELOGDATA pLog, bool bAddlog)
		{
			this.m_Scene.Init();
			this.m_Scene.SetLog(pLog, bAddlog);
		}

		// Token: 0x06000375 RID: 885 RVA: 0x0000B964 File Offset: 0x00009B64
		public bool UpdateSkipMark()
		{
			if (KsInput.IsLock && !KsInput.IsJump)
			{
				if (KsInput.IsSkip)
				{
					if ("sys_skip_ff".Length > 0)
					{
						this.UpdateAnimSkipMark("sys_skip_ff", 880, 0);
						return true;
					}
				}
				else if (KsInput.IsRewind && "sys_skip_rew".Length > 0)
				{
					this.UpdateAnimSkipMark("sys_skip_rew", 880, 0);
					return true;
				}
			}
			this.ResetSkipMark();
			return true;
		}

		// Token: 0x06000376 RID: 886 RVA: 0x0000B9EC File Offset: 0x00009BEC
		private void UpdateAnimSkipMark(string idCG, int iX, int iY)
		{
			if (this.m_nSkipWaitCount < 5)
			{
				this.m_nSkipWaitCount++;
				return;
			}
			if (!this.SkipMark.IsEqualCg(idCG) || !this.SkipMark.IsEnableUVAnim())
			{
				this.SkipMark.SetImage(Singleton<Man2D>.Instance.LoadTexture(idCG));
				this.SkipMark.A = byte.MaxValue;
				this.SkipMark.SetPtnNum(4, 2);
				this.SkipMark.SetPtnPos(0, 0);
				this.SkipMark.SetPos(iX, iY, 910);
				this.SkipMark.EnableUVAnim(true);
				this.SkipMark.RepeatUVAnim(true);
				this.SkipMark.SetUVAnimWait(8);
				this.SkipMark.ResetUVAnim();
			}
		}

		// Token: 0x06000377 RID: 887 RVA: 0x0000BAB8 File Offset: 0x00009CB8
		public void ResetSkipMark()
		{
			this.SkipMark.A = 0;
			this.m_nSkipWaitCount = 0;
			this.SkipMark.EnableUVAnim(false);
			this.SkipMark.ResetUVAnim();
		}

		// Token: 0x06000378 RID: 888 RVA: 0x0000BAF0 File Offset: 0x00009CF0
		public int GetSkipFlag()
		{
			return this.m_nSkipFlag;
		}

		// Token: 0x06000379 RID: 889 RVA: 0x0000BAF8 File Offset: 0x00009CF8
		public void SetSkipFlag(int nFlag)
		{
			this.m_nSkipFlag = nFlag;
		}

		// Token: 0x0600037A RID: 890 RVA: 0x0000BB04 File Offset: 0x00009D04
		public void EntryFace(bool bOn, string name = "")
		{
			this.m_Scene.Face.Sw = ((!bOn) ? SWITCH.OFF : SWITCH.ON);
			this.m_Scene.Face.Name = name;
		}

		// Token: 0x0600037B RID: 891 RVA: 0x0000BB40 File Offset: 0x00009D40
		public void EntryBg(bool bOn, string name = "")
		{
			this.m_Scene.Bg.Sw = ((!bOn) ? SWITCH.OFF : SWITCH.ON);
			this.m_Scene.Bg.Name = name;
		}

		// Token: 0x0600037C RID: 892 RVA: 0x0000BB7C File Offset: 0x00009D7C
		public void EntryCg(bool bOn, int nNo, string name = "", int nX = 0, int nY = 0, int nFromX = 0, int nFromY = 0, int nTime = 0, int nLayer = -1)
		{
			if (nNo >= 4)
			{
				Debug.Assert(false, string.Format("指定CG番号が最大数を超えています。({0}>={1})", nNo, 4));
			}
			if (nNo < 0 && !bOn)
			{
				for (int num = 0; num != this.m_Scene.Cg.Length; num++)
				{
					this.EntryCg(bOn, num, name, nX, nY, nFromX, nFromY, nTime, nLayer);
				}
			}
			else
			{
				this.m_Scene.Cg[nNo].Set(bOn, name, nX, nY, nFromX, nFromY, nTime, nLayer);
			}
		}

		// Token: 0x0600037D RID: 893 RVA: 0x0000BC14 File Offset: 0x00009E14
		public void EntryChar(bool bOn, int nNo, string name = "", int nPos = 0)
		{
			this.m_Scene.Chr[nNo].Sw = ((!bOn) ? SWITCH.OFF : SWITCH.ON);
			this.m_Scene.Chr[nNo].Name = name;
			this.m_Scene.Chr[nNo].nParam = nPos;
		}

		// Token: 0x0600037E RID: 894 RVA: 0x0000BC68 File Offset: 0x00009E68
		public void EntrySelect(int nNo, string name = "", string ksName = "", string labelName = "", int nType = 0, string effectName = "")
		{
			if (nNo < 0)
			{
				for (int num = 0; num != this.m_Scene.Select.Length; num++)
				{
					this.m_Scene.Select[num].Init();
				}
				App.QooSelect.Clear();
			}
			else
			{
				this.m_Scene.Select[nNo].Set(name, ksName, labelName, effectName, nType);
				for (int i = nNo + 1; i < this.m_Scene.Select.Length; i++)
				{
					this.m_Scene.Select[i].Init();
				}
				App.QooSelect.AddSelect(this.m_Scene.Select);
			}
		}

		// Token: 0x0600037F RID: 895 RVA: 0x0000BD20 File Offset: 0x00009F20
		public void EntryBgm(bool bOn, string Name = "", int nVol = 100, int nTime = 3000)
		{
			this.m_Scene.Bgm.Set(bOn, Name, nVol, nTime);
		}

		// Token: 0x06000380 RID: 896 RVA: 0x0000BD38 File Offset: 0x00009F38
		public void EntryVoice(bool bOn, string Name = "")
		{
			this.m_Scene.Voice.Sw = ((!bOn) ? SWITCH.OFF : SWITCH.ON);
			this.m_Scene.Voice.Name = Name;
		}

		// Token: 0x06000381 RID: 897 RVA: 0x0000BD74 File Offset: 0x00009F74
		public void EntrySe(bool bOn, int nNo, string Name = "", int nVol = 100, bool bLoop = false)
		{
			this.m_Scene.Se[nNo].Sw = ((!bOn) ? SWITCH.OFF : SWITCH.ON);
			this.m_Scene.Se[nNo].nLoop = ((!bLoop) ? 0 : 1);
			this.m_Scene.Se[nNo].nVolume = nVol;
			if (bOn)
			{
				this.m_Scene.Se[nNo].Name = Name;
			}
		}

		// Token: 0x06000382 RID: 898 RVA: 0x0000BDF0 File Offset: 0x00009FF0
		public void EntryEffect(bool bOn, string Name = "", int nTime = 0)
		{
			this.m_Scene.Effect.Sw = ((!bOn) ? SWITCH.OFF : SWITCH.ON);
			this.m_Scene.Effect.Name = Name;
			this.m_Scene.Effect.nParam = nTime;
		}

		// Token: 0x06000383 RID: 899 RVA: 0x0000BE3C File Offset: 0x0000A03C
		public void EntryMessage(bool bOn, TagData Tag = null, EVENTECENR_NAME Name = null, int nFrameType = 0, bool bAddLog = false)
		{
			this.m_Scene.Message.Sw = ((!bOn) ? SWITCH.OFF : SWITCH.ON);
			this.m_Scene.Message.Tag = Tag;
			if (Name == null)
			{
				this.m_Scene.Message.Name.Init();
			}
			else
			{
				this.m_Scene.Message.Name = Name;
			}
			this.m_Scene.Message.nFrameType = nFrameType;
			this.m_Scene.Message.bAddLog = bAddLog;
		}

		// Token: 0x06000384 RID: 900 RVA: 0x0000BECC File Offset: 0x0000A0CC
		private EVENTSCENE SceneApplySub(EVENTSCENE Scene, bool bMsg, bool IsVoice = true)
		{
			if (Scene.Message.Sw == SWITCH.ON && Scene.Message.Tag != null)
			{
				MsgWnd instance = Singleton<MsgWnd>.Instance;
				instance.ResetMessage();
				instance.SetStyle((MSGWND_STYLE)Scene.Message.nFrameType);
				if (bMsg)
				{
					this.ProcMessageData(Scene.Message.Tag);
				}
				instance.SetName(Scene.Message.Name.Name);
				if (Scene.Face.Name.Length > 0)
				{
					this.SetDrawFace(Scene.Face.Name);
				}
				else
				{
					this.SetDrawFace(null);
				}
				this.m_Draw.ShowMessage();
				this.m_Draw.DeleteClearMessage();
			}
			else if (Scene.Message.Sw == SWITCH.OFF)
			{
				this.m_Draw.ClearMessage();
				this.SetDrawFace(null);
			}
			if (Scene.Face.Sw == SWITCH.OFF)
			{
				this.SetDrawFace(null);
			}
			if (Scene.Bg.Sw == SWITCH.ON && Scene.Bg.Name.Length > 0)
			{
				this.SetBg(Scene.Bg.Name);
			}
			else if (Scene.Bg.Sw == SWITCH.OFF)
			{
				this.SetBg(null);
			}
			for (int i = 0; i < Scene.Cg.Length; i++)
			{
				if (Scene.Cg[i].Sw == SWITCH.ON && Scene.Cg[i].Name.Length > 0)
				{
					this.SetCg(i, Scene.Cg[i].Name, (int)Scene.Cg[i].nFromX, (int)Scene.Cg[i].nFromY, this.GetCgLayerZ(Scene.Cg[i].nLayer));
				}
				else if (Scene.Cg[i].Sw == SWITCH.OFF)
				{
					this.SetCg(i, null, 0, 0, -1);
				}
			}
			for (int j = 0; j < Scene.Chr.Length; j++)
			{
				if (Scene.Chr[j].Sw == SWITCH.ON && Scene.Chr[j].Name.Length > 0)
				{
					this.SetChar(j, Scene.Chr[j].Name, Scene.Chr[j].nParam);
				}
				else if (Scene.Chr[j].Sw == SWITCH.OFF)
				{
					this.SetChar(j, null, -1);
				}
			}
			App.QooSelect.AddSelect(Scene.Select);
			if (Scene.Bgm.Sw == SWITCH.ON)
			{
				float fVol = (float)Scene.Bgm.nVolume / 100f;
				if (Scene.Bgm.Name.Length > 0)
				{
					Sound.BgmPlay(Scene.Bgm.Name, fVol, Scene.Bgm.nTime, Scene.Bgm.nTime);
				}
			}
			else if (Scene.Bgm.Sw == SWITCH.OFF)
			{
				Sound.BgmStop(Scene.Bgm.nTime);
			}
			bool flag = (this.m_nSkipFlag & 2) != 0;
			bool flag2 = (this.m_nSkipFlag & 1) != 0;
			for (int k = 0; k < Scene.Se.Length; k++)
			{
				bool flag3 = Scene.Se[k].nLoop == 1;
				if (Scene.Se[k].Sw == SWITCH.ON && Scene.Se[k].Name.Length > 0 && (!flag || flag3))
				{
					float vol = (float)Scene.Se[k].nVolume / 100f;
					Sound.SeSlotPlay(k, Scene.Se[k].Name, flag3, vol);
				}
				else if (Scene.Se[k].Sw == SWITCH.OFF)
				{
					Sound.SeSlotStop(k);
					Scene.Se[k].Name = string.Empty;
				}
			}
			if (Scene.Voice.Sw == SWITCH.ON && bMsg)
			{
				bool flag4 = flag || flag2;
				if (Scene.Voice.Name.Length > 0 && !flag4 && IsVoice)
				{
					Sound.VoicePlay(Scene.Voice.Name, 1f);
				}
			}
			else if (Scene.Voice.Sw == SWITCH.OFF)
			{
				Sound.VoiceStop();
			}
			if (Scene.Message.Sw == SWITCH.ON && Scene.Message.bAddLog)
			{
				this.ApplyBacklog(Scene);
			}
			this.m_Draw.Run();
			return Scene;
		}

		// Token: 0x06000385 RID: 901 RVA: 0x0000C37C File Offset: 0x0000A57C
		public void SceneApply_Message(bool IsMesWndClear = true)
		{
			if (this.m_Scene.Message.Sw == SWITCH.ON && this.m_Scene.Message.bAddLog)
			{
				Singleton<MsgWnd>.Instance.SetStyle((MSGWND_STYLE)this.m_Scene.Message.nFrameType);
				this.SetMessage(null);
				this.ApplyBacklog(this.m_Scene);
			}
			Singleton<MsgWnd>.Instance.ResetMessage();
			if (IsMesWndClear && Singleton<MsgWnd>.Instance.IsVisible)
			{
				this.m_Draw.ClearMessage();
				this.SetDrawFace(null);
			}
		}

		// Token: 0x06000386 RID: 902 RVA: 0x0000C414 File Offset: 0x0000A614
		public void SetMessage(TagData tag = null)
		{
			Singleton<MsgWnd>.Instance.ResetMessage();
			this.ProcMessageData((tag == null) ? this.m_Scene.Message.Tag : tag);
		}

		// Token: 0x06000387 RID: 903 RVA: 0x0000C450 File Offset: 0x0000A650
		private void ApplyBacklog(EVENTSCENE Scene_)
		{
			string text = string.Empty;
			for (int num = 0; num != Scene_.Message.Name.Name.Length; num++)
			{
				if (text.Length > 0 && Scene_.Message.Name.Name[num].Length > 0)
				{
					text += "＆";
				}
				if (Scene_.Message.Name.Name[num].Length > 0)
				{
					text += Scene_.Message.Name.Name[num];
				}
			}
			string voice = string.Empty;
			if (Scene_.Voice.Sw == SWITCH.ON)
			{
				voice = Scene_.Voice.Name;
			}
			string message = Singleton<MsgWnd>.Instance.Message;
			App.QooBackLog.AddMessage(message, text, voice);
			GameData.LastMessage = message;
		}

		// Token: 0x06000388 RID: 904 RVA: 0x0000C534 File Offset: 0x0000A734
		private void SetBg(string name)
		{
			UnitySprite sp = this.GrpList[0];
			if (name == null || name.Length == 0)
			{
				this.m_Draw.AddEraseObject(sp);
			}
			else
			{
				this.m_Draw.AddDrawObject(sp, 0, 0, 710, name, byte.MaxValue);
			}
		}

		// Token: 0x06000389 RID: 905 RVA: 0x0000C588 File Offset: 0x0000A788
		private void SetCg(int slot, string name, int x = 0, int y = 0, int z = -1)
		{
			UnitySprite sp = this.CgList[slot];
			if (name == null || name.Length == 0)
			{
				this.m_Draw.AddEraseObject(sp);
			}
			else
			{
				this.m_Draw.AddDrawObject(sp, x, y, z, name, byte.MaxValue);
			}
		}

		// Token: 0x0600038A RID: 906 RVA: 0x0000C5D8 File Offset: 0x0000A7D8
		private void SetChar(int no, string name, int pos)
		{
			UnitySprite sp = this.GrpList[1 + no];
			if (name == null || name.Length == 0)
			{
				this.m_Draw.AddEraseObject(sp);
			}
			else if (pos > 0)
			{
				this.m_Draw.AddDrawObject(sp, KsDef.CharPos[pos].x, KsDef.CharPos[pos].y, this.ChrPosIndexToZ(pos), name, byte.MaxValue);
			}
			else
			{
				this.m_Draw.AddDrawObjectC(sp, this.ChrPosIndexToZ(pos), name, byte.MaxValue);
			}
		}

		// Token: 0x0600038B RID: 907 RVA: 0x0000C668 File Offset: 0x0000A868
		public void SetDrawFace(string name)
		{
			UnitySprite sp = this.GrpList[6];
			if (name == null || name.Length == 0)
			{
				this.m_Draw.AddEraseObject(sp);
				this.m_bDrawFace = false;
			}
			else
			{
				this.m_bDrawFace = true;
				byte a = byte.MaxValue;
				if (!SysData.IsDrawFace())
				{
					a = 0;
				}
				MSGWND_STYLE_DATA data = MessageStyle.GetData(Singleton<MsgWnd>.Instance.Style);
				this.m_Draw.AddDrawObject(sp, data.posFace.x, data.posFace.y, data.posFace.z, name, a);
			}
		}

		// Token: 0x0600038C RID: 908 RVA: 0x0000C700 File Offset: 0x0000A900
		public void RedrawFace(bool bDraw = true)
		{
			UnitySprite unitySprite = this.GrpList[6];
			if (bDraw && SysData.IsDrawFace())
			{
				if (this.m_bDrawFace)
				{
					unitySprite.A = byte.MaxValue;
				}
				else
				{
					unitySprite.A = 0;
				}
			}
			else
			{
				unitySprite.A = 0;
			}
		}

		// Token: 0x0600038D RID: 909 RVA: 0x0000C754 File Offset: 0x0000A954
		public int ChrPosIndexToZ(int nPos)
		{
			switch (nPos)
			{
			case 1:
				return 740;
			case 2:
				return 730;
			case 3:
				return 760;
			case 4:
				return 750;
			default:
				return 770;
			}
		}

		// Token: 0x0600038E RID: 910 RVA: 0x0000C7A0 File Offset: 0x0000A9A0
		public int GetCgLayerZ(int no)
		{
			int result = 780;
			switch (no)
			{
			case 1:
				result = 715;
				break;
			case 2:
				result = 720;
				break;
			case 3:
				result = 725;
				break;
			case 4:
				result = 735;
				break;
			case 5:
				result = 745;
				break;
			case 6:
				result = 755;
				break;
			case 7:
				result = 765;
				break;
			case 8:
				result = 775;
				break;
			case 9:
				result = 785;
				break;
			case 10:
				result = 790;
				break;
			case 11:
				result = 795;
				break;
			case 12:
				result = 905;
				break;
			}
			return result;
		}

		// Token: 0x0600038F RID: 911 RVA: 0x0000C878 File Offset: 0x0000AA78
		private static float InterScalar(float fV0, float fV1, float fParam)
		{
			return (fV1 - fV0) * fParam + fV0;
		}

		// Token: 0x06000390 RID: 912 RVA: 0x0000C884 File Offset: 0x0000AA84
		private void ResetPosCg(int i)
		{
			this.m_Draw.AddDrawObject(this.CgList[i], (int)this.m_Scene.Cg[i].nX, (int)this.m_Scene.Cg[i].nY);
			this.m_Scene.Cg[i].SwMove = SWITCH.NULL;
		}

		// Token: 0x06000391 RID: 913 RVA: 0x0000C8DC File Offset: 0x0000AADC
		private bool MoveCg(ref UnitySprite sp, ref EVENTSCENE_CG cg, float now)
		{
			float num = (float)cg.nTime;
			if (num != 0f && now < num)
			{
				float fParam = now / num;
				sp.x = (int)KsScene.InterScalar((float)cg.nFromX, (float)cg.nX, fParam);
				sp.y = (int)KsScene.InterScalar((float)cg.nFromY, (float)cg.nY, fParam);
				return true;
			}
			sp.x = (int)cg.nX;
			sp.y = (int)cg.nY;
			cg.SwMove = SWITCH.NULL;
			return false;
		}

		// Token: 0x06000392 RID: 914 RVA: 0x0000C96C File Offset: 0x0000AB6C
		private void ProcMessageData(TagData Tag)
		{
			Singleton<MsgWnd>.Instance.SetSize(MSGFONTSIZE.NORMAL);
			for (int i = 0; i < Tag.Message.Count; i++)
			{
				MsgTagData msgTagData = Tag.Message[i];
				if (msgTagData.IsMessage())
				{
					Singleton<MsgWnd>.Instance.AddMessage(msgTagData.Message);
				}
				else
				{
					KsTagTable.GetInfo(msgTagData.ID).RunMessage(msgTagData);
				}
			}
		}

		// Token: 0x06000393 RID: 915 RVA: 0x0000C9E0 File Offset: 0x0000ABE0
		internal int GetSelectNum()
		{
			return App.QooSelect.SelectAr.Count;
		}

		// Token: 0x06000394 RID: 916 RVA: 0x0000C9F4 File Offset: 0x0000ABF4
		internal bool IsEntrySelect(string szSelect, string szKs, string szLabel)
		{
			return App.QooSelect.IsEntry(szSelect, szKs, szLabel);
		}

		// Token: 0x0400026A RID: 618
		public EVENTSCENE m_Scene = new EVENTSCENE();

		// Token: 0x0400026B RID: 619
		private KsSceneDrawObject m_Draw = new KsSceneDrawObject();

		// Token: 0x0400026C RID: 620
		public EVENTBACKUPDATA BackupData = new EVENTBACKUPDATA();

		// Token: 0x0400026D RID: 621
		private bool m_bDrawFace;

		// Token: 0x0400026E RID: 622
		private int m_nSkipWaitCount;
	}
}
