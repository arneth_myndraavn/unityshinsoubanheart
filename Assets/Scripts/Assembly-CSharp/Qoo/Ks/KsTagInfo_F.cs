﻿using System;
using Qoo.Def;

namespace Qoo.Ks
{
	// Token: 0x02000077 RID: 119
	internal class KsTagInfo_F : KsTagInfo
	{
		// Token: 0x0600034C RID: 844 RVA: 0x0000AD00 File Offset: 0x00008F00
		public override KSID GetId()
		{
			return KSID.F;
		}

		// Token: 0x0600034D RID: 845 RVA: 0x0000AD04 File Offset: 0x00008F04
		public override bool RunMessage(MsgTagData tag)
		{
			ParamData param;
			if (tag.IsClose)
			{
				Singleton<MsgWnd>.Instance.SetSize(MSGFONTSIZE.NORMAL);
			}
			else if ((param = tag.GetParam(KSID.SIZE)) != null)
			{
				Singleton<MsgWnd>.Instance.SetSize(this.ParamToSize(param.ToInt()));
			}
			return true;
		}

		// Token: 0x0600034E RID: 846 RVA: 0x0000AD54 File Offset: 0x00008F54
		public MSGFONTSIZE ParamToSize(int param)
		{
			MSGFONTSIZE result = MSGFONTSIZE.NORMAL;
			if (param >= 200)
			{
				result = MSGFONTSIZE.DOUBLE;
			}
			else if (param >= 150)
			{
				result = MSGFONTSIZE.LARGE;
			}
			else if (param < 100)
			{
				result = MSGFONTSIZE.SMALL;
			}
			return result;
		}
	}
}
