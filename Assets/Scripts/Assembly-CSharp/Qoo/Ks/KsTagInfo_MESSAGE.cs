﻿using System;
using Qoo.Application;
using Qoo.Def;
using Qoo.Input;
using Qoo.SoundSystem;

namespace Qoo.Ks
{
	// Token: 0x02000048 RID: 72
	internal class KsTagInfo_MESSAGE : KsTagInfo
	{
		// Token: 0x0600029B RID: 667 RVA: 0x00009608 File Offset: 0x00007808
		public KsTagInfo_MESSAGE()
		{
			this.IsSilent = false;
		}

		// Token: 0x17000085 RID: 133
		// (get) Token: 0x0600029C RID: 668 RVA: 0x00009618 File Offset: 0x00007818
		// (set) Token: 0x0600029D RID: 669 RVA: 0x00009620 File Offset: 0x00007820
		protected bool IsSilent { get; set; }

		// Token: 0x0600029E RID: 670 RVA: 0x0000962C File Offset: 0x0000782C
		public override KSID GetId()
		{
			return KSID.MESSAGE;
		}

		// Token: 0x0600029F RID: 671 RVA: 0x00009630 File Offset: 0x00007830
		public override bool IsMessage()
		{
			return true;
		}

		// Token: 0x060002A0 RID: 672 RVA: 0x00009634 File Offset: 0x00007834
		public override bool IsSceneApply()
		{
			return true;
		}

		// Token: 0x060002A1 RID: 673 RVA: 0x00009638 File Offset: 0x00007838
		public override bool IsRevSceneApply()
		{
			return true;
		}

		// Token: 0x060002A2 RID: 674 RVA: 0x0000963C File Offset: 0x0000783C
		public override bool IsRevContinue()
		{
			return false;
		}

		// Token: 0x060002A3 RID: 675 RVA: 0x00009640 File Offset: 0x00007840
		public override TAG_RESULT Run(EventPlayer player, TagData tag)
		{
			MSGWND_STYLE nFrameType = MSGWND_STYLE.KYARA;
			EVENTECENR_NAME eventecenr_NAME = new EVENTECENR_NAME();
			if (player.TagCallCnt == 0)
			{
				ParamData param;
				if ((param = tag.GetParam(KSID.WINDOW)) != null)
				{
					for (MSGWND_STYLE msgwnd_STYLE = MSGWND_STYLE.NORMAL; msgwnd_STYLE != MSGWND_STYLE.MAX; msgwnd_STYLE++)
					{
						if (string.Equals(msgwnd_STYLE.ToString(), param.Param, StringComparison.OrdinalIgnoreCase))
						{
							nFrameType = msgwnd_STYLE;
							break;
						}
					}
				}
				eventecenr_NAME.Name[0] = tag.GetParamString(KSID.NAME, string.Empty);
				if (eventecenr_NAME.Name[0].Length > 20)
				{
					eventecenr_NAME.Name[0] = eventecenr_NAME.Name[0].Substring(0, 20);
				}
				eventecenr_NAME.Name[1] = tag.GetParamString(KSID.NAME2, string.Empty);
				if (eventecenr_NAME.Name[1].Length > 20)
				{
					eventecenr_NAME.Name[1] = eventecenr_NAME.Name[1].Substring(0, 20);
				}
				bool bAddLog = true;
				player.Scene.EntryMessage(true, tag, eventecenr_NAME, (int)nFrameType, bAddLog);
				string paramString = tag.GetParamString(KSID.VOICE, null);
				if (paramString != null)
				{
					player.Scene.EntryVoice(true, paramString);
				}
				else
				{
					player.Scene.EntryVoice(false, string.Empty);
				}
				if ((param = tag.GetParam(KSID.FACE)) != null)
				{
					player.Scene.EntryFace(true, param.Param);
				}
				Singleton<MsgWnd>.Instance.SetMesNo(player.CurKsNo, player.CurLabelNo, player.CurTagNo);
				App.QooBackLog.SetMesNo(player.CurKsNo, player.CurLabelNo, tag.MesNo);
				Singleton<MsgWnd>.Instance.IsSilentMode = this.IsSilent;
				KsInput.SetMask(INPUT_STATE.HIDE);
				KsInput.SetMask(INPUT_STATE.AUTO);
			}
			else
			{
				if (player.RestoreOn)
				{
					KsInput.ResetMask(INPUT_STATE.HIDE);
					KsInput.ResetMask(INPUT_STATE.AUTO);
					if (!this.IsSilent)
					{
						Singleton<MsgWnd>.Instance.IsSilentMode = false;
					}
					return TAG_RESULT.NEXT_EXIT;
				}
				if (Singleton<MsgWnd>.Instance.IsSkip())
				{
					KsInput.ResetMask(INPUT_STATE.HIDE);
					KsInput.ResetMask(INPUT_STATE.AUTO);
					Sound.VoiceStop();
					if (!this.IsSilent)
					{
						Singleton<MsgWnd>.Instance.IsSilentMode = false;
					}
					return TAG_RESULT.NEXT_EXIT;
				}
			}
			return TAG_RESULT.CONTINUE_EXIT;
		}
	}
}
