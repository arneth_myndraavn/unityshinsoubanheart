﻿using System;
using System.Collections.Generic;

namespace Qoo.Ks
{
	// Token: 0x0200003D RID: 61
	public class KsLog
	{
		// Token: 0x060001F9 RID: 505 RVA: 0x000086D8 File Offset: 0x000068D8
		public void Reset()
		{
			this.m_SceneLog.Clear();
			this.m_PosLog.Clear();
			this.m_CallLog.Clear();
		}

		// Token: 0x060001FA RID: 506 RVA: 0x000086FC File Offset: 0x000068FC
		public SCENELOGDATA GetScene(int index)
		{
			if (index >= 0 && index < this.GetLogNum())
			{
				return this.m_SceneLog[index];
			}
			return null;
		}

		// Token: 0x060001FB RID: 507 RVA: 0x00008720 File Offset: 0x00006920
		public SCENELOGDATA GetSceneCurrent()
		{
			return this.GetScene(this.m_SceneLog.Count - 1);
		}

		// Token: 0x060001FC RID: 508 RVA: 0x00008738 File Offset: 0x00006938
		private bool SetScene(int index, SCENELOGDATA data)
		{
			if (index < 0 || index >= this.GetLogNum())
			{
				return false;
			}
			this.m_SceneLog[index] = data;
			return true;
		}

		// Token: 0x060001FD RID: 509 RVA: 0x00008760 File Offset: 0x00006960
		public bool SetSceneCurrrent(SCENELOGDATA data)
		{
			return this.SetScene(this.GetLogNum() - 1, data);
		}

		// Token: 0x060001FE RID: 510 RVA: 0x00008774 File Offset: 0x00006974
		public int GetLogNum()
		{
			return this.m_SceneLog.Count;
		}

		// Token: 0x060001FF RID: 511 RVA: 0x00008784 File Offset: 0x00006984
		public SCENEPOSDATA GetPos(int index)
		{
			if (index >= 0 && index < this.GetLogNum())
			{
				return this.m_PosLog[index];
			}
			return null;
		}

		// Token: 0x06000200 RID: 512 RVA: 0x000087A8 File Offset: 0x000069A8
		public SCENEPOSDATA GetPosCurrent()
		{
			return this.GetPos(this.m_PosLog.Count - 1);
		}

		// Token: 0x06000201 RID: 513 RVA: 0x000087C0 File Offset: 0x000069C0
		private bool SetPos(int index, SCENEPOSDATA data)
		{
			if (index < 0 || index >= this.GetLogNum())
			{
				return false;
			}
			this.m_PosLog[index] = data;
			return true;
		}

		// Token: 0x06000202 RID: 514 RVA: 0x000087E8 File Offset: 0x000069E8
		public bool SetPosCurrent(SCENEPOSDATA data)
		{
			return this.SetPos(this.GetLogNum() - 1, data);
		}

		// Token: 0x06000203 RID: 515 RVA: 0x000087FC File Offset: 0x000069FC
		public int Push()
		{
			this.m_SceneLog.Add(new SCENELOGDATA());
			this.m_PosLog.Add(new SCENEPOSDATA());
			return this.GetLogNum() - 1;
		}

		// Token: 0x06000204 RID: 516 RVA: 0x00008834 File Offset: 0x00006A34
		public int Pop()
		{
			int index = this.GetLogNum() - 1;
			this.m_SceneLog.RemoveAt(index);
			this.m_PosLog.RemoveAt(index);
			return this.GetLogNum() - 1;
		}

		// Token: 0x06000205 RID: 517 RVA: 0x0000886C File Offset: 0x00006A6C
		public bool IsRewind()
		{
			return this.GetLogNum() > 1;
		}

		// Token: 0x06000206 RID: 518 RVA: 0x00008878 File Offset: 0x00006A78
		public int PushCall()
		{
			this.m_CallLog.Add(default(SCENECALLDATA));
			return this.GetCallNum() - 1;
		}

		// Token: 0x06000207 RID: 519 RVA: 0x000088A4 File Offset: 0x00006AA4
		public int PopCall()
		{
			this.m_CallLog.RemoveAt(this.GetCallNum() - 1);
			return this.GetCallNum() - 1;
		}

		// Token: 0x06000208 RID: 520 RVA: 0x000088CC File Offset: 0x00006ACC
		public int GetCallNum()
		{
			return this.m_CallLog.Count;
		}

		// Token: 0x06000209 RID: 521 RVA: 0x000088DC File Offset: 0x00006ADC
		public SCENECALLDATA GetCallData(int index)
		{
			return this.m_CallLog[index];
		}

		// Token: 0x040001B1 RID: 433
		private List<SCENELOGDATA> m_SceneLog = new List<SCENELOGDATA>();

		// Token: 0x040001B2 RID: 434
		private List<SCENEPOSDATA> m_PosLog = new List<SCENEPOSDATA>();

		// Token: 0x040001B3 RID: 435
		private List<SCENECALLDATA> m_CallLog = new List<SCENECALLDATA>();
	}
}
