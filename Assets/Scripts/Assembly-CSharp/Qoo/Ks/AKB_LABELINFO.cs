﻿using System;

namespace Qoo.Ks
{
	// Token: 0x02000032 RID: 50
	public struct AKB_LABELINFO
	{
		// Token: 0x0400014F RID: 335
		public uint nFileNo;

		// Token: 0x04000150 RID: 336
		public string Name;

		// Token: 0x04000151 RID: 337
		public uint nNameCRC;

		// Token: 0x04000152 RID: 338
		public int nTagNum;
	}
}
