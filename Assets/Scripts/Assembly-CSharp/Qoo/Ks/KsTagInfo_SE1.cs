﻿using System;

namespace Qoo.Ks
{
	// Token: 0x02000059 RID: 89
	internal class KsTagInfo_SE1 : KsTagInfo_SE
	{
		// Token: 0x060002D7 RID: 727 RVA: 0x00009E94 File Offset: 0x00008094
		public KsTagInfo_SE1()
		{
			base.SeNo = 0;
		}

		// Token: 0x060002D8 RID: 728 RVA: 0x00009EA4 File Offset: 0x000080A4
		public override KSID GetId()
		{
			return KSID.SE1;
		}
	}
}
