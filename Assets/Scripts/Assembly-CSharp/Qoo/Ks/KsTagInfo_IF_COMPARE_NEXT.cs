﻿using System;
using Qoo.Game;

namespace Qoo.Ks
{
	// Token: 0x0200006D RID: 109
	internal class KsTagInfo_IF_COMPARE_NEXT : KsTagInfo
	{
		// Token: 0x06000322 RID: 802 RVA: 0x0000A9FC File Offset: 0x00008BFC
		public override KSID GetId()
		{
			return KSID.IF_COMPARE_NEXT;
		}

		// Token: 0x06000323 RID: 803 RVA: 0x0000AA00 File Offset: 0x00008C00
		public override bool IsJumpTag()
		{
			return true;
		}

		// Token: 0x06000324 RID: 804 RVA: 0x0000AA04 File Offset: 0x00008C04
		public override TAG_RESULT Run(EventPlayer player, TagData tag)
		{
			string paramString = tag.GetParamString(KSID.VALUE, string.Empty);
			string paramString2 = tag.GetParamString(KSID.INDEX, string.Empty);
			string paramString3 = tag.GetParamString(KSID.VALUE1, string.Empty);
			string paramString4 = tag.GetParamString(KSID.INDEX1, string.Empty);
			KSID id = KSID.COMP;
			KSID id2 = KSID.COMPTARGET;
			if (!GameData.CmpOtherParam(paramString, paramString2, paramString3, paramString4))
			{
				id = KSID.INCOMP;
				id2 = KSID.INCOMPTARGET;
			}
			string paramString5 = tag.GetParamString(id, string.Empty);
			string paramString6 = tag.GetParamString(id2, string.Empty);
			if (paramString5.Length > 0 && paramString6.Length > 0)
			{
				return (!player.JumpKs(paramString5, paramString6, this.IsCallRet())) ? TAG_RESULT.NEXT : TAG_RESULT.CONTINUE;
			}
			return TAG_RESULT.NEXT;
		}
	}
}
