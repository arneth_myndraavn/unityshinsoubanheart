﻿using System;

namespace Qoo.Ks
{
	// Token: 0x02000090 RID: 144
	public class LabelData
	{
		// Token: 0x06000419 RID: 1049 RVA: 0x0000EA00 File Offset: 0x0000CC00
		public LabelData(string name, int no, int start)
		{
			this.Name = name;
			this.No = no;
			this.TagStart = start;
		}

		// Token: 0x1700009B RID: 155
		// (get) Token: 0x0600041A RID: 1050 RVA: 0x0000EA28 File Offset: 0x0000CC28
		// (set) Token: 0x0600041B RID: 1051 RVA: 0x0000EA30 File Offset: 0x0000CC30
		public string Name { get; private set; }

		// Token: 0x1700009C RID: 156
		// (get) Token: 0x0600041C RID: 1052 RVA: 0x0000EA3C File Offset: 0x0000CC3C
		// (set) Token: 0x0600041D RID: 1053 RVA: 0x0000EA44 File Offset: 0x0000CC44
		public int No { get; private set; }

		// Token: 0x1700009D RID: 157
		// (get) Token: 0x0600041E RID: 1054 RVA: 0x0000EA50 File Offset: 0x0000CC50
		// (set) Token: 0x0600041F RID: 1055 RVA: 0x0000EA58 File Offset: 0x0000CC58
		public int TagStart { get; private set; }

		// Token: 0x1700009E RID: 158
		// (get) Token: 0x06000420 RID: 1056 RVA: 0x0000EA64 File Offset: 0x0000CC64
		// (set) Token: 0x06000421 RID: 1057 RVA: 0x0000EA6C File Offset: 0x0000CC6C
		public int TagNum { get; private set; }

		// Token: 0x06000422 RID: 1058 RVA: 0x0000EA78 File Offset: 0x0000CC78
		public void SetNowTag(int no)
		{
			this.TagNum = no - this.TagStart;
		}
	}
}
