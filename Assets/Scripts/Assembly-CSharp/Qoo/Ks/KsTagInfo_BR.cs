﻿using System;

namespace Qoo.Ks
{
	// Token: 0x02000076 RID: 118
	internal class KsTagInfo_BR : KsTagInfo
	{
		// Token: 0x06000349 RID: 841 RVA: 0x0000ACF0 File Offset: 0x00008EF0
		public override KSID GetId()
		{
			return KSID.BR;
		}

		// Token: 0x0600034A RID: 842 RVA: 0x0000ACF4 File Offset: 0x00008EF4
		public override bool RunMessage(MsgTagData tag)
		{
			return true;
		}
	}
}
