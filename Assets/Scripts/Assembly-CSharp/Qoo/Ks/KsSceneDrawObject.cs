﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Qoo.Ks
{
	// Token: 0x02000080 RID: 128
	public class KsSceneDrawObject
	{
		// Token: 0x06000396 RID: 918 RVA: 0x0000CA18 File Offset: 0x0000AC18
		public bool Clear()
		{
			this.m_DrawList.Clear();
			this.m_isClearMessage = false;
			this.m_isShowMessage = false;
			return true;
		}

		// Token: 0x06000397 RID: 919 RVA: 0x0000CA34 File Offset: 0x0000AC34
		public void AddDrawObject(UnitySprite sp, int x, int y, int z, string name, byte a = 255)
		{
			this.Add(new SCENE_DRAW_OBJECT
			{
				com = DRAW_COMMAND.DRAW,
				sp = sp,
				isCenter = false,
				x = x,
				y = y,
				z = z,
				a = a,
				name = name,
				tex = Man2D.Texture(name)
			});
		}

		// Token: 0x06000398 RID: 920 RVA: 0x0000CAA4 File Offset: 0x0000ACA4
		public void AddDrawObjectC(UnitySprite sp, int z, string name, byte a = 255)
		{
			this.Add(new SCENE_DRAW_OBJECT
			{
				com = DRAW_COMMAND.DRAW,
				sp = sp,
				isCenter = true,
				z = z,
				a = a,
				name = name,
				tex = Man2D.Texture(name)
			});
		}

		// Token: 0x06000399 RID: 921 RVA: 0x0000CB00 File Offset: 0x0000AD00
		public void AddDrawObject(UnitySprite sp, int x, int y)
		{
			this.Add(new SCENE_DRAW_OBJECT
			{
				com = DRAW_COMMAND.DRAW_NOTEX,
				sp = sp,
				isCenter = false,
				x = x,
				y = y,
				tex = null
			});
		}

		// Token: 0x0600039A RID: 922 RVA: 0x0000CB4C File Offset: 0x0000AD4C
		public void AddDrawObject_TexOnly(UnitySprite sp, string name)
		{
			this.Add(new SCENE_DRAW_OBJECT
			{
				com = DRAW_COMMAND.SETTEX,
				sp = sp,
				isCenter = false,
				name = name,
				tex = Man2D.Texture(name)
			});
		}

		// Token: 0x0600039B RID: 923 RVA: 0x0000CB98 File Offset: 0x0000AD98
		public void AddEraseObject(UnitySprite sp)
		{
			this.Add(new SCENE_DRAW_OBJECT
			{
				com = DRAW_COMMAND.ERASE,
				sp = sp
			});
		}

		// Token: 0x0600039C RID: 924 RVA: 0x0000CBC4 File Offset: 0x0000ADC4
		public void ClearMessage()
		{
			this.m_isClearMessage = true;
		}

		// Token: 0x0600039D RID: 925 RVA: 0x0000CBD0 File Offset: 0x0000ADD0
		public void DeleteClearMessage()
		{
			this.m_isClearMessage = false;
		}

		// Token: 0x0600039E RID: 926 RVA: 0x0000CBDC File Offset: 0x0000ADDC
		public void ShowMessage()
		{
			this.m_isShowMessage = true;
		}

		// Token: 0x0600039F RID: 927 RVA: 0x0000CBE8 File Offset: 0x0000ADE8
		public void DeleteShowMessage()
		{
			this.m_isShowMessage = false;
		}

		// Token: 0x060003A0 RID: 928 RVA: 0x0000CBF4 File Offset: 0x0000ADF4
		public void Run()
		{
			if (this.m_DrawList.Count > 0 || this.m_isClearMessage || this.m_isShowMessage)
			{
				UnityTask.SetSubTask(this.OnTask());
			}
		}

		// Token: 0x060003A1 RID: 929 RVA: 0x0000CC2C File Offset: 0x0000AE2C
		private void Add(SCENE_DRAW_OBJECT obj)
		{
			this.m_DrawList.Add(obj);
		}

		// Token: 0x060003A2 RID: 930 RVA: 0x0000CC3C File Offset: 0x0000AE3C
		private IEnumerator OnTask()
		{
			foreach (SCENE_DRAW_OBJECT item in this.m_DrawList)
			{
				bool IsTexWait = false;
				switch (item.com)
				{
				case DRAW_COMMAND.DRAW:
					IsTexWait = true;
					break;
				case DRAW_COMMAND.SETTEX:
					IsTexWait = true;
					break;
				}
				if (IsTexWait && item.name != null && item.name.Length > 0)
				{
					while (!Singleton<Man2D>.Instance.IsUseTextue(item.name))
					{
						yield return 0;
					}
				}
			}
			foreach (SCENE_DRAW_OBJECT item2 in this.m_DrawList)
			{
				bool IsTex = false;
				bool IsAlpha = false;
				bool IsSetPos = false;
				switch (item2.com)
				{
				case DRAW_COMMAND.DRAW:
					IsTex = true;
					IsSetPos = true;
					IsAlpha = true;
					break;
				case DRAW_COMMAND.DRAW_NOTEX:
					this.SetPosXY(item2);
					continue;
				case DRAW_COMMAND.ERASE:
					this.Clear(item2);
					continue;
				case DRAW_COMMAND.SETTEX:
					IsTex = true;
					break;
				}
				if (IsTex)
				{
					this.SetImage(item2);
				}
				if (IsAlpha)
				{
					this.SetAlpha(item2);
				}
				if (IsSetPos)
				{
					if (item2.isCenter)
					{
						this.SetPosCenter(item2);
					}
					else
					{
						this.SetPos(item2);
					}
				}
			}
			this.m_DrawList.Clear();
			if (this.m_isShowMessage)
			{
				Singleton<MsgWnd>.Instance.Show(true);
			}
			else if (this.m_isClearMessage)
			{
				Singleton<MsgWnd>.Instance.ResetMessage();
				Singleton<MsgWnd>.Instance.Show(false);
			}
			this.m_isClearMessage = false;
			this.m_isShowMessage = false;
			yield return 0;
			yield break;
		}

		// Token: 0x060003A3 RID: 931 RVA: 0x0000CC58 File Offset: 0x0000AE58
		private void Clear(SCENE_DRAW_OBJECT item)
		{
			item.sp.SetImage(null);
			item.sp.A = 0;
		}

		// Token: 0x060003A4 RID: 932 RVA: 0x0000CC74 File Offset: 0x0000AE74
		private void SetImage(SCENE_DRAW_OBJECT item)
		{
			if (item.tex == item.sp.tex)
			{
				item.tex.DecCount();
			}
			item.sp.SetImage(item.tex);
		}

		// Token: 0x060003A5 RID: 933 RVA: 0x0000CCB0 File Offset: 0x0000AEB0
		private void SetAlpha(SCENE_DRAW_OBJECT item)
		{
			item.sp.A = item.a;
		}

		// Token: 0x060003A6 RID: 934 RVA: 0x0000CCC8 File Offset: 0x0000AEC8
		private void SetPos(SCENE_DRAW_OBJECT item)
		{
			item.sp.x = item.x;
			item.sp.y = item.y;
			item.sp.z = item.z;
		}

		// Token: 0x060003A7 RID: 935 RVA: 0x0000CD04 File Offset: 0x0000AF04
		private void SetPosCenter(SCENE_DRAW_OBJECT item)
		{
			item.sp.SetCenterPosH();
			item.sp.z = item.z;
		}

		// Token: 0x060003A8 RID: 936 RVA: 0x0000CD28 File Offset: 0x0000AF28
		private void SetPosXY(SCENE_DRAW_OBJECT item)
		{
			item.sp.x = item.x;
			item.sp.y = item.y;
		}

		// Token: 0x04000282 RID: 642
		private List<SCENE_DRAW_OBJECT> m_DrawList = new List<SCENE_DRAW_OBJECT>();

		// Token: 0x04000283 RID: 643
		private bool m_isClearMessage;

		// Token: 0x04000284 RID: 644
		private bool m_isShowMessage;
	}
}
