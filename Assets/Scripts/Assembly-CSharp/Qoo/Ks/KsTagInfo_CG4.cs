﻿using System;

namespace Qoo.Ks
{
	// Token: 0x02000056 RID: 86
	internal class KsTagInfo_CG4 : KsTagInfo_CG
	{
		// Token: 0x060002CB RID: 715 RVA: 0x00009CC0 File Offset: 0x00007EC0
		public KsTagInfo_CG4()
		{
			base.CgNo = 3;
		}

		// Token: 0x060002CC RID: 716 RVA: 0x00009CD0 File Offset: 0x00007ED0
		public override KSID GetId()
		{
			return KSID.CG4;
		}
	}
}
