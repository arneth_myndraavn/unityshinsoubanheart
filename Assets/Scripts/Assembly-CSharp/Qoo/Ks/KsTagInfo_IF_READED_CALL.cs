﻿using System;

namespace Qoo.Ks
{
	// Token: 0x0200006C RID: 108
	internal class KsTagInfo_IF_READED_CALL : KsTagInfo_IF_SYS_READED_NEXT
	{
		// Token: 0x0600031E RID: 798 RVA: 0x0000A9DC File Offset: 0x00008BDC
		public KsTagInfo_IF_READED_CALL()
		{
			base.IsSys = true;
		}

		// Token: 0x0600031F RID: 799 RVA: 0x0000A9EC File Offset: 0x00008BEC
		public override KSID GetId()
		{
			return KSID.IF_READED_CALL;
		}

		// Token: 0x06000320 RID: 800 RVA: 0x0000A9F0 File Offset: 0x00008BF0
		public override bool IsCallRet()
		{
			return true;
		}
	}
}
