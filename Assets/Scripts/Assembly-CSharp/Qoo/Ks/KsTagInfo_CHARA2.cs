﻿using System;

namespace Qoo.Ks
{
	// Token: 0x0200004E RID: 78
	internal class KsTagInfo_CHARA2 : KsTagInfo_CHARA
	{
		// Token: 0x060002B7 RID: 695 RVA: 0x00009B08 File Offset: 0x00007D08
		public KsTagInfo_CHARA2()
		{
			base.CharNo = 1;
		}

		// Token: 0x060002B8 RID: 696 RVA: 0x00009B18 File Offset: 0x00007D18
		public override KSID GetId()
		{
			return KSID.CHARA2;
		}
	}
}
