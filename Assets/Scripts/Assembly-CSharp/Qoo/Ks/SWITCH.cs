﻿using System;

namespace Qoo.Ks
{
	// Token: 0x02000081 RID: 129
	public enum SWITCH
	{
		// Token: 0x04000286 RID: 646
		NULL,
		// Token: 0x04000287 RID: 647
		ON,
		// Token: 0x04000288 RID: 648
		OFF
	}
}
