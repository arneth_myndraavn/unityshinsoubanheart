﻿using System;
using System.Text;

namespace Qoo.Ks
{
	// Token: 0x0200008F RID: 143
	public class MsgTagData : TagBase
	{
		// Token: 0x06000413 RID: 1043 RVA: 0x0000E950 File Offset: 0x0000CB50
		public MsgTagData() : base(KSID.MESSAGE, false)
		{
		}

		// Token: 0x1700009A RID: 154
		// (get) Token: 0x06000414 RID: 1044 RVA: 0x0000E95C File Offset: 0x0000CB5C
		// (set) Token: 0x06000415 RID: 1045 RVA: 0x0000E964 File Offset: 0x0000CB64
		public string Message { get; private set; }

		// Token: 0x06000416 RID: 1046 RVA: 0x0000E970 File Offset: 0x0000CB70
		public bool IsMessage()
		{
			return this.Message.Length > 0;
		}

		// Token: 0x06000417 RID: 1047 RVA: 0x0000E980 File Offset: 0x0000CB80
		public void AddMessage(string msg)
		{
			StringBuilder stringBuilder = new StringBuilder(this.Message);
			stringBuilder.Append(msg);
			this.Message = stringBuilder.ToString();
			base.IsClose = false;
		}

		// Token: 0x06000418 RID: 1048 RVA: 0x0000E9B4 File Offset: 0x0000CBB4
		public void SetTagData(TagData tag)
		{
			base.ID = tag.ID;
			this.Message = string.Empty;
			base.IsClose = tag.IsClose;
			base.Param.Clear();
			base.Param.AddRange(tag.Param);
		}
	}
}
