﻿using System;
using Qoo.Game;
using Qoo.Script;

namespace Qoo.Ks
{
	// Token: 0x02000067 RID: 103
	internal class KsTagInfo_IF_PARAM_NEXT : KsTagInfo
	{
		// Token: 0x0600030D RID: 781 RVA: 0x0000A77C File Offset: 0x0000897C
		public override KSID GetId()
		{
			return KSID.IF_PARAM_NEXT;
		}

		// Token: 0x0600030E RID: 782 RVA: 0x0000A780 File Offset: 0x00008980
		public override bool IsJumpTag()
		{
			return true;
		}

		// Token: 0x0600030F RID: 783 RVA: 0x0000A784 File Offset: 0x00008984
		public override TAG_RESULT Run(EventPlayer player, TagData tag)
		{
			string text = string.Empty;
			string text2 = string.Empty;
			ParamData param;
			bool flag;
			if ((param = tag.GetParam(KSID.EXP)) != null)
			{
				int num = 0;
				if (!Calc.ExecCmdStr(param.Param, ref num))
				{
					Debug.Print(string.Format("●判定式エラー : {0}\n", param.Param));
				}
				flag = (num != 0);
			}
			else
			{
				string paramString = tag.GetParamString(KSID.VALUE, string.Empty);
				string paramString2 = tag.GetParamString(KSID.INDEX, string.Empty);
				string paramString3 = tag.GetParamString(KSID.PARAM, string.Empty);
				flag = GameData.CmpParam(paramString, paramString2, paramString3);
			}
			if (flag)
			{
				text = tag.GetParamString(KSID.COMP, string.Empty);
				text2 = tag.GetParamString(KSID.COMPTARGET, string.Empty);
			}
			else
			{
				text = tag.GetParamString(KSID.INCOMP, string.Empty);
				text2 = tag.GetParamString(KSID.INCOMPTARGET, string.Empty);
			}
			if (text.Length > 0 || text2.Length > 0)
			{
				return (!player.JumpKs(text, text2, this.IsCallRet())) ? TAG_RESULT.NEXT : TAG_RESULT.CONTINUE;
			}
			return TAG_RESULT.NEXT;
		}
	}
}
