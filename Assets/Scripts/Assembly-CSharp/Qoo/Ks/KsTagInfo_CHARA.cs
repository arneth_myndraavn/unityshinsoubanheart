﻿using System;

namespace Qoo.Ks
{
	// Token: 0x0200004C RID: 76
	internal class KsTagInfo_CHARA : KsTagInfo
	{
		// Token: 0x060002AF RID: 687 RVA: 0x00009930 File Offset: 0x00007B30
		public KsTagInfo_CHARA()
		{
			this.CharNo = -1;
		}

		// Token: 0x17000086 RID: 134
		// (get) Token: 0x060002B0 RID: 688 RVA: 0x00009940 File Offset: 0x00007B40
		// (set) Token: 0x060002B1 RID: 689 RVA: 0x00009948 File Offset: 0x00007B48
		protected int CharNo { get; set; }

		// Token: 0x060002B2 RID: 690 RVA: 0x00009954 File Offset: 0x00007B54
		public override KSID GetId()
		{
			return KSID.CHARA;
		}

		// Token: 0x060002B3 RID: 691 RVA: 0x00009958 File Offset: 0x00007B58
		public override bool IsRevSceneApply()
		{
			return true;
		}

		// Token: 0x060002B4 RID: 692 RVA: 0x0000995C File Offset: 0x00007B5C
		public override TAG_RESULT Run(EventPlayer player, TagData tag)
		{
			int nPos = 0;
			ParamData param;
			if ((param = tag.GetParam(KSID.POS)) != null)
			{
				string text = param.Param.ToLower();
				switch (text)
				{
				case "center":
					nPos = 0;
					goto IL_F3;
				case "left":
					nPos = 1;
					goto IL_F3;
				case "right":
					nPos = 2;
					goto IL_F3;
				case "left_center":
					nPos = 3;
					goto IL_F3;
				case "right_center":
					nPos = 4;
					goto IL_F3;
				}
				Debug.Print(string.Format("CHARA TAG PARAM ERROR:POS={0}\n", param.Param.ToLower()));
			}
			IL_F3:
			if ((param = tag.GetParam(KSID.FILE)) != null)
			{
				player.Scene.EntryChar(true, (this.CharNo >= 0) ? this.CharNo : 0, param.Param, nPos);
			}
			else if (this.CharNo < 0)
			{
				for (int i = 0; i < 5; i++)
				{
					player.Scene.EntryChar(false, i, string.Empty, 0);
				}
			}
			else
			{
				player.Scene.EntryChar(false, this.CharNo, string.Empty, 0);
			}
			base.CheckEffectParam(player, tag);
			return TAG_RESULT.NEXT_EXIT;
		}
	}
}
