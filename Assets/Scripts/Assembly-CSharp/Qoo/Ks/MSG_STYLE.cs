﻿using System;

namespace Qoo.Ks
{
	// Token: 0x02000037 RID: 55
	internal enum MSG_STYLE
	{
		// Token: 0x0400016E RID: 366
		NORMAL,
		// Token: 0x0400016F RID: 367
		KYARA,
		// Token: 0x04000170 RID: 368
		TOGAKI,
		// Token: 0x04000171 RID: 369
		HEROINE,
		// Token: 0x04000172 RID: 370
		MONOLOGUE,
		// Token: 0x04000173 RID: 371
		NUM
	}
}
