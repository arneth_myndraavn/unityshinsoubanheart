﻿using System;

namespace Qoo.Ks
{
	// Token: 0x0200005E RID: 94
	internal class KsTagInfo_ANIMATION : KsTagInfo
	{
		// Token: 0x060002E8 RID: 744 RVA: 0x0000A244 File Offset: 0x00008444
		public override KSID GetId()
		{
			return KSID.ANIMATION;
		}

		// Token: 0x060002E9 RID: 745 RVA: 0x0000A248 File Offset: 0x00008448
		public override bool IsRevSceneApply()
		{
			return true;
		}

		// Token: 0x060002EA RID: 746 RVA: 0x0000A24C File Offset: 0x0000844C
		public override TAG_RESULT Run(EventPlayer player, TagData tag)
		{
			if (player.RestoreOn)
			{
				return TAG_RESULT.NEXT;
			}
			int paramInt = tag.GetParamInt(KSID.POSX, 0);
			int paramInt2 = tag.GetParamInt(KSID.POSY, 0);
			ParamData param;
			if ((param = tag.GetParam(KSID.NAME)) != null)
			{
				int num = paramInt2 << 16 & 65535;
				num |= (paramInt & -65536);
				player.Scene.EntryEffect(true, param.Param, num);
			}
			return TAG_RESULT.NEXT_EXIT;
		}
	}
}
