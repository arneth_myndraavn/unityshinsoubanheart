﻿using System;
using System.Collections.Generic;

namespace Qoo.Ks
{
	// Token: 0x0200008E RID: 142
	public class TagData : TagBase
	{
		// Token: 0x06000404 RID: 1028 RVA: 0x0000E83C File Offset: 0x0000CA3C
		public TagData(KSID id, int label, bool IsClose_ = false) : base(id, IsClose_)
		{
			this.LabelNo = label;
			this.Connect = false;
			if (this.IsMessage())
			{
				this.Message = new List<MsgTagData>();
			}
		}

		// Token: 0x17000096 RID: 150
		// (get) Token: 0x06000405 RID: 1029 RVA: 0x0000E878 File Offset: 0x0000CA78
		// (set) Token: 0x06000406 RID: 1030 RVA: 0x0000E880 File Offset: 0x0000CA80
		public int LabelNo { get; private set; }

		// Token: 0x17000097 RID: 151
		// (get) Token: 0x06000407 RID: 1031 RVA: 0x0000E88C File Offset: 0x0000CA8C
		// (set) Token: 0x06000408 RID: 1032 RVA: 0x0000E894 File Offset: 0x0000CA94
		public bool Connect { get; private set; }

		// Token: 0x17000098 RID: 152
		// (get) Token: 0x06000409 RID: 1033 RVA: 0x0000E8A0 File Offset: 0x0000CAA0
		// (set) Token: 0x0600040A RID: 1034 RVA: 0x0000E8A8 File Offset: 0x0000CAA8
		public List<MsgTagData> Message { get; private set; }

		// Token: 0x17000099 RID: 153
		// (get) Token: 0x0600040B RID: 1035 RVA: 0x0000E8B4 File Offset: 0x0000CAB4
		// (set) Token: 0x0600040C RID: 1036 RVA: 0x0000E8BC File Offset: 0x0000CABC
		public int MesNo { get; private set; }

		// Token: 0x0600040D RID: 1037 RVA: 0x0000E8C8 File Offset: 0x0000CAC8
		public bool Add(ParamData param)
		{
			base.Param.Add(param);
			return true;
		}

		// Token: 0x0600040E RID: 1038 RVA: 0x0000E8D8 File Offset: 0x0000CAD8
		public bool AddMessage(MsgTagData tag)
		{
			this.Message.Add(tag);
			return true;
		}

		// Token: 0x0600040F RID: 1039 RVA: 0x0000E8E8 File Offset: 0x0000CAE8
		public bool AddLastMessage(string str)
		{
			if (this.IsMessage())
			{
				this.Message[this.Message.Count - 1].AddMessage(str);
				return true;
			}
			return false;
		}

		// Token: 0x06000410 RID: 1040 RVA: 0x0000E924 File Offset: 0x0000CB24
		public bool IsMessage()
		{
			return KsTagTable.GetInfo(base.ID).IsMessage();
		}

		// Token: 0x06000411 RID: 1041 RVA: 0x0000E938 File Offset: 0x0000CB38
		public void SetMesNo(int no)
		{
			this.MesNo = no;
		}

		// Token: 0x06000412 RID: 1042 RVA: 0x0000E944 File Offset: 0x0000CB44
		public void SetConnect()
		{
			this.Connect = true;
		}
	}
}
