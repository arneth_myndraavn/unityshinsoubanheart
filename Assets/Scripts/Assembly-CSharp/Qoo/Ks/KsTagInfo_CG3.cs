﻿using System;

namespace Qoo.Ks
{
	// Token: 0x02000055 RID: 85
	internal class KsTagInfo_CG3 : KsTagInfo_CG
	{
		// Token: 0x060002C9 RID: 713 RVA: 0x00009CAC File Offset: 0x00007EAC
		public KsTagInfo_CG3()
		{
			base.CgNo = 2;
		}

		// Token: 0x060002CA RID: 714 RVA: 0x00009CBC File Offset: 0x00007EBC
		public override KSID GetId()
		{
			return KSID.CG3;
		}
	}
}
