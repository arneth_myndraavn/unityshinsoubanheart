﻿using System;
using Qoo.Memory;

namespace Qoo.Ks
{
	// Token: 0x02000040 RID: 64
	public class SCENEPOSDATA
	{
		// Token: 0x1700006E RID: 110
		// (get) Token: 0x06000242 RID: 578 RVA: 0x00008C54 File Offset: 0x00006E54
		// (set) Token: 0x06000243 RID: 579 RVA: 0x00008C5C File Offset: 0x00006E5C
		public short sFileNo { get; set; }

		// Token: 0x1700006F RID: 111
		// (get) Token: 0x06000244 RID: 580 RVA: 0x00008C68 File Offset: 0x00006E68
		// (set) Token: 0x06000245 RID: 581 RVA: 0x00008C70 File Offset: 0x00006E70
		public short sLabelNo { get; set; }

		// Token: 0x17000070 RID: 112
		// (get) Token: 0x06000246 RID: 582 RVA: 0x00008C7C File Offset: 0x00006E7C
		// (set) Token: 0x06000247 RID: 583 RVA: 0x00008C84 File Offset: 0x00006E84
		public short sTagNo { get; set; }

		// Token: 0x17000071 RID: 113
		// (get) Token: 0x06000248 RID: 584 RVA: 0x00008C90 File Offset: 0x00006E90
		// (set) Token: 0x06000249 RID: 585 RVA: 0x00008C98 File Offset: 0x00006E98
		public bool bRun { get; set; }

		// Token: 0x17000072 RID: 114
		// (get) Token: 0x0600024A RID: 586 RVA: 0x00008CA4 File Offset: 0x00006EA4
		// (set) Token: 0x0600024B RID: 587 RVA: 0x00008CAC File Offset: 0x00006EAC
		public bool bLock { get; set; }

		// Token: 0x0600024C RID: 588 RVA: 0x00008CB8 File Offset: 0x00006EB8
		public bool Save(MemFile mem)
		{
			mem.SetInt16(this.sFileNo);
			mem.SetInt16(this.sLabelNo);
			mem.SetInt16(this.sTagNo);
			mem.SetBool(this.bRun);
			mem.SetBool(this.bLock);
			return true;
		}

		// Token: 0x0600024D RID: 589 RVA: 0x00008D04 File Offset: 0x00006F04
		public bool Load(MemFile mem)
		{
			this.sFileNo = mem.GetInt16();
			this.sLabelNo = mem.GetInt16();
			this.sTagNo = mem.GetInt16();
			this.bRun = mem.GetBool();
			this.bLock = mem.GetBool();
			return true;
		}
	}
}
