﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Qoo.Ks
{
	// Token: 0x02000091 RID: 145
	public class TagReader
	{
		// Token: 0x06000423 RID: 1059 RVA: 0x0000EA88 File Offset: 0x0000CC88
		public TagReader()
		{
			this.Label = new List<LabelData>();
			this.Tag = new List<TagData>();
			this.NextKsList = new List<string>();
			this.IsOldTag = false;
		}

		// Token: 0x1700009F RID: 159
		// (get) Token: 0x06000424 RID: 1060 RVA: 0x0000EAC4 File Offset: 0x0000CCC4
		// (set) Token: 0x06000425 RID: 1061 RVA: 0x0000EACC File Offset: 0x0000CCCC
		public List<LabelData> Label { get; private set; }

		// Token: 0x170000A0 RID: 160
		// (get) Token: 0x06000426 RID: 1062 RVA: 0x0000EAD8 File Offset: 0x0000CCD8
		// (set) Token: 0x06000427 RID: 1063 RVA: 0x0000EAE0 File Offset: 0x0000CCE0
		public List<TagData> Tag { get; private set; }

		// Token: 0x170000A1 RID: 161
		// (get) Token: 0x06000428 RID: 1064 RVA: 0x0000EAEC File Offset: 0x0000CCEC
		// (set) Token: 0x06000429 RID: 1065 RVA: 0x0000EAF4 File Offset: 0x0000CCF4
		public List<string> NextKsList { get; set; }

		// Token: 0x170000A2 RID: 162
		// (get) Token: 0x0600042A RID: 1066 RVA: 0x0000EB00 File Offset: 0x0000CD00
		// (set) Token: 0x0600042B RID: 1067 RVA: 0x0000EB08 File Offset: 0x0000CD08
		public int Line { get; private set; }

		// Token: 0x170000A3 RID: 163
		// (get) Token: 0x0600042C RID: 1068 RVA: 0x0000EB14 File Offset: 0x0000CD14
		// (set) Token: 0x0600042D RID: 1069 RVA: 0x0000EB1C File Offset: 0x0000CD1C
		public string Name { get; private set; }

		// Token: 0x170000A4 RID: 164
		// (get) Token: 0x0600042E RID: 1070 RVA: 0x0000EB28 File Offset: 0x0000CD28
		// (set) Token: 0x0600042F RID: 1071 RVA: 0x0000EB30 File Offset: 0x0000CD30
		private bool IsOldTag { get; set; }

		// Token: 0x170000A5 RID: 165
		// (get) Token: 0x06000430 RID: 1072 RVA: 0x0000EB3C File Offset: 0x0000CD3C
		// (set) Token: 0x06000431 RID: 1073 RVA: 0x0000EB44 File Offset: 0x0000CD44
		public int MesNum { get; private set; }

		// Token: 0x06000432 RID: 1074 RVA: 0x0000EB50 File Offset: 0x0000CD50
		public bool Create(string ksName, string str)
		{
			this.Label.Clear();
			this.Tag.Clear();
			this.Line = 0;
			this.Name = ksName;
			if (!this.Analysis(str))
			{
				return false;
			}
			this.AddMessageNo();
			this.CreateNextKsList();
			return true;
		}

		// Token: 0x06000433 RID: 1075 RVA: 0x0000EB9C File Offset: 0x0000CD9C
		private bool Analysis(string str)
		{
			if (str[0] == '"')
			{
				str = str.Substring(1);
			}
			int num = (from c in str.ToList<char>()
			where c.Equals('\n')
			select c).Count<char>();
			int num2 = 0;
			for (int num3 = 0; num3 != num; num3++)
			{
				int num4 = str.IndexOf('\n', num2);
				string text;
				if (num4 == -1)
				{
					text = str.Substring(num2);
				}
				else
				{
					text = str.Substring(num2, num4 - num2);
					num2 += text.Length + 1;
				}
				if (!this.Analysis_local(text, num3))
				{
					return false;
				}
			}
			if (num2 < str.Length)
			{
				string line = str.Substring(num2);
				if (!this.Analysis_local(line, num))
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x06000434 RID: 1076 RVA: 0x0000EC70 File Offset: 0x0000CE70
		private bool Analysis_local(string line, int line_no)
		{
			KsTextSeparator ksTextSeparator = new KsTextSeparator();
			if (!ksTextSeparator.Analysis(line))
			{
				Debug.Print(string.Concat(new object[]
				{
					"Error:",
					this.Name,
					"(",
					line_no + 1,
					"):解析中にエラーが発生しました"
				}));
				return false;
			}
			string[] dest = ksTextSeparator.Dest;
			if (this.m_isMessage)
			{
				if (!this.AnalysisMessage(dest))
				{
					this.m_isMessage = false;
				}
			}
			else if (dest.Length == 0)
			{
				this.m_isOldTag = false;
			}
			else
			{
				this.AnalysisLine(dest);
			}
			return true;
		}

		// Token: 0x06000435 RID: 1077 RVA: 0x0000ED18 File Offset: 0x0000CF18
		private bool AnalysisLine(string[] data)
		{
			for (int i = 0; i < data.Length; i++)
			{
				string text = data[i];
				char c = text[0];
				switch (c)
				{
				case '\n':
					if (i == 0)
					{
						this.m_isOldTag = false;
					}
					break;
				default:
					if (c != '*')
					{
						if (c != ';')
						{
							if (c != '[')
							{
								if (i == 0)
								{
									this.m_isOldTag = false;
								}
							}
							else
							{
								i++;
								if (this.m_isOldTag && this.Tag.Count > 0)
								{
									this.Tag[this.Tag.Count - 1].SetConnect();
								}
								TagData tagData = this.AddTag(data[i]);
								if (tagData != null)
								{
									i++;
									while (data[i][0] != ']')
									{
										i = this.AddTagParam(ref tagData, data, i);
									}
									this.Tag.Add(tagData);
									if (tagData.IsMessage())
									{
										this.m_isMessage = true;
									}
									this.SetLabelTagNum();
									this.m_isOldTag = true;
								}
							}
						}
						else if (i == 0)
						{
							this.m_isOldTag = false;
						}
					}
					else
					{
						this.AddLabel(text.Substring(1));
						this.m_isOldTag = false;
					}
					break;
				case '\r':
					if (i == 0)
					{
						this.m_isOldTag = false;
					}
					break;
				}
			}
			return true;
		}

		// Token: 0x06000436 RID: 1078 RVA: 0x0000EE88 File Offset: 0x0000D088
		private bool AddLabel(string name)
		{
			this.SetLabelTagNum();
			this.Label.Add(new LabelData(name, this.Label.Count, this.Tag.Count));
			return true;
		}

		// Token: 0x06000437 RID: 1079 RVA: 0x0000EEC4 File Offset: 0x0000D0C4
		private bool SetLabelTagNum()
		{
			if (this.Label.Count != 0)
			{
				this.Label[this.Label.Count - 1].SetNowTag(this.Tag.Count);
			}
			return true;
		}

		// Token: 0x06000438 RID: 1080 RVA: 0x0000EF0C File Offset: 0x0000D10C
		private TagData AddTag(string name)
		{
			bool flag = name[0] == '/';
			if (flag)
			{
				name = name.Substring(1);
			}
			KSID id = KsTagTable.GetID(name);
			if (id == KSID.UNKNOWN)
			{
				this.Error(string.Format("Error:タグ名が不正={0}", name));
				return null;
			}
			return new TagData(id, this.Label.Count, flag);
		}

		// Token: 0x06000439 RID: 1081 RVA: 0x0000EF68 File Offset: 0x0000D168
		private int AddTagParam(ref TagData tag, string[] data, int tokunNo)
		{
			while (tokunNo < data.Length)
			{
				if (data[tokunNo][0] == ']')
				{
					return tokunNo;
				}
				KSID id = KsTagTable.GetID(data[tokunNo]);
				if (id == KSID.UNKNOWN)
				{
					this.Error(string.Format("Error:パラメータ名が不正={0}", data[tokunNo]));
					return -1;
				}
				tokunNo++;
				if (tokunNo >= data.Length || data[tokunNo][0] != '=')
				{
					this.Error("Error:パラメータ区切り文字がない。");
					return -1;
				}
				tokunNo++;
				string param = data[tokunNo];
				tokunNo++;
				tag.Add(new ParamData(id, param));
			}
			return tokunNo;
		}

		// Token: 0x0600043A RID: 1082 RVA: 0x0000F004 File Offset: 0x0000D204
		private bool AnalysisMessage(string[] data)
		{
			for (int i = 0; i < data.Length; i++)
			{
				MsgTagData msgTagData = new MsgTagData();
				char c = data[i][0];
				if (c != '\r')
				{
					if (c != '[')
					{
						msgTagData.AddMessage(data[i]);
					}
					else
					{
						i++;
						TagData tagData = this.AddTag(data[i]);
						if (tagData != null)
						{
							i++;
							while (i < data.Length && data[i][0] != ']')
							{
								i = this.AddTagParam(ref tagData, data, i);
							}
							if (tagData.ID == KSID.MESSAGE && tagData.IsClose)
							{
								return false;
							}
							if (tagData.ID == KSID.BACKLOG && tagData.IsClose)
							{
								return false;
							}
							msgTagData.SetTagData(tagData);
						}
					}
				}
				this.Tag[this.Tag.Count - 1].AddMessage(msgTagData);
			}
			this.Tag[this.Tag.Count - 1].AddLastMessage("\n");
			return true;
		}

		// Token: 0x0600043B RID: 1083 RVA: 0x0000F124 File Offset: 0x0000D324
		private void AddMessageNo()
		{
			this.MesNum = 0;
			this.Tag.ForEach(delegate(TagData tag)
			{
				if (tag.IsMessage())
				{
					tag.SetMesNo(this.MesNum++);
				}
			});
		}

		// Token: 0x0600043C RID: 1084 RVA: 0x0000F144 File Offset: 0x0000D344
		private void CreateNextKsList()
		{
			List<string> tbl = new List<string>();
			this.Tag.ForEach(delegate(TagData tag)
			{
				string text = string.Empty;
				if (tag.ID == KSID.NEXT)
				{
					text = tag.GetParamString(KSID.FILE, string.Empty);
					if (text != string.Empty)
					{
						tbl.Add(text);
					}
				}
				else if (tag.ID == KSID.SELECT)
				{
					text = tag.GetParamString(KSID.FILE, string.Empty);
					if (text != string.Empty)
					{
						tbl.Add(text);
					}
				}
				else
				{
					text = tag.GetParamString(KSID.COMP, string.Empty);
					if (text != string.Empty)
					{
						tbl.Add(text);
					}
					text = tag.GetParamString(KSID.INCOMP, string.Empty);
					if (text != string.Empty)
					{
						tbl.Add(text);
					}
				}
			});
			this.NextKsList.Clear();
			this.NextKsList.AddRange(tbl.Distinct<string>());
		}

		// Token: 0x0600043D RID: 1085 RVA: 0x0000F19C File Offset: 0x0000D39C
		private void Error(string msg)
		{
			Debug.Print(string.Format("{0}({1}):{2}\n", this.Name, this.Line, msg));
		}

		// Token: 0x040002C9 RID: 713
		private bool m_isMessage;

		// Token: 0x040002CA RID: 714
		private bool m_isOldTag;
	}
}
