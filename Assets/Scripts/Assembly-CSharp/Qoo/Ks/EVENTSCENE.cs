﻿using System;
using Qoo.Memory;

namespace Qoo.Ks
{
	// Token: 0x02000089 RID: 137
	public class EVENTSCENE
	{
		// Token: 0x060003DD RID: 989 RVA: 0x0000D644 File Offset: 0x0000B844
		public EVENTSCENE()
		{
			this.Face = new EVENTSCENE_OBJ();
			this.Bg = new EVENTSCENE_OBJ();
			this.Cg = new EVENTSCENE_CG[4];
			for (int i = 0; i < this.Cg.Length; i++)
			{
				this.Cg[i] = new EVENTSCENE_CG();
			}
			this.Bgm = new EVENTSCENE_BGM();
			this.Voice = new EVENTSCENE_OBJ();
			this.Effect = new EVENTSCENE_OBJ();
			this.Chr = new EVENTSCENE_OBJ[5];
			for (int j = 0; j < this.Chr.Length; j++)
			{
				this.Chr[j] = new EVENTSCENE_OBJ();
			}
			this.Se = new EVENTSCENE_SE[2];
			for (int k = 0; k < this.Se.Length; k++)
			{
				this.Se[k] = new EVENTSCENE_SE();
			}
			this.Select = new EVENTSCENE_SELCET[10];
			for (int l = 0; l < this.Select.Length; l++)
			{
				this.Select[l] = new EVENTSCENE_SELCET();
			}
			this.Message = new EVENTSCENE_MSG();
		}

		// Token: 0x060003DE RID: 990 RVA: 0x0000D764 File Offset: 0x0000B964
		public void Save(MemFile conv)
		{
			this.Face.Save(conv);
			this.Bg.Save(conv);
			foreach (EVENTSCENE_CG eventscene_CG in this.Cg)
			{
				eventscene_CG.Save(conv);
			}
			this.Bgm.Save(conv);
			this.Voice.Save(conv);
			this.Effect.Save(conv);
			foreach (EVENTSCENE_OBJ eventscene_OBJ in this.Chr)
			{
				eventscene_OBJ.Save(conv);
			}
			foreach (EVENTSCENE_SE eventscene_SE in this.Se)
			{
				eventscene_SE.Save(conv);
			}
			foreach (EVENTSCENE_SELCET eventscene_SELCET in this.Select)
			{
				eventscene_SELCET.Save(conv);
			}
			this.Message.Save(conv);
		}

		// Token: 0x060003DF RID: 991 RVA: 0x0000D870 File Offset: 0x0000BA70
		public void Load(MemFile conv)
		{
			this.Face.Load(conv);
			this.Bg.Load(conv);
			foreach (EVENTSCENE_CG eventscene_CG in this.Cg)
			{
				eventscene_CG.Load(conv);
			}
			this.Bgm.Load(conv);
			this.Voice.Load(conv);
			this.Effect.Load(conv);
			foreach (EVENTSCENE_OBJ eventscene_OBJ in this.Chr)
			{
				eventscene_OBJ.Load(conv);
			}
			foreach (EVENTSCENE_SE eventscene_SE in this.Se)
			{
				eventscene_SE.Load(conv);
			}
			foreach (EVENTSCENE_SELCET eventscene_SELCET in this.Select)
			{
				eventscene_SELCET.Load(conv);
			}
			this.Message.Load(conv);
		}

		// Token: 0x060003E0 RID: 992 RVA: 0x0000D97C File Offset: 0x0000BB7C
		public void Init()
		{
			this.Face.Init();
			this.Bg.Init();
			foreach (EVENTSCENE_CG eventscene_CG in this.Cg)
			{
				eventscene_CG.Init();
			}
			this.Bgm.Init();
			this.Voice.Init();
			this.Effect.Init();
			foreach (EVENTSCENE_OBJ eventscene_OBJ in this.Chr)
			{
				eventscene_OBJ.Init();
			}
			foreach (EVENTSCENE_SE eventscene_SE in this.Se)
			{
				eventscene_SE.Init();
			}
			foreach (EVENTSCENE_SELCET eventscene_SELCET in this.Select)
			{
				eventscene_SELCET.Init();
			}
			this.Message.Init();
		}

		// Token: 0x060003E1 RID: 993 RVA: 0x0000DA7C File Offset: 0x0000BC7C
		public void Reset()
		{
			this.Face.Reset();
			this.Bg.Reset();
			foreach (EVENTSCENE_CG eventscene_CG in this.Cg)
			{
				eventscene_CG.Reset();
			}
			this.Bgm.Reset();
			this.Voice.Reset();
			this.Effect.Reset();
			foreach (EVENTSCENE_OBJ eventscene_OBJ in this.Chr)
			{
				eventscene_OBJ.Reset();
			}
			foreach (EVENTSCENE_SE eventscene_SE in this.Se)
			{
				eventscene_SE.Reset();
			}
			foreach (EVENTSCENE_SELCET eventscene_SELCET in this.Select)
			{
				eventscene_SELCET.Reset();
			}
			this.Message.Reset();
		}

		// Token: 0x060003E2 RID: 994 RVA: 0x0000DB7C File Offset: 0x0000BD7C
		public void Copy(EVENTSCENE other)
		{
			this.Face.Copy(other.Face);
			this.Bg.Copy(other.Bg);
			for (int num = 0; num != this.Cg.Length; num++)
			{
				this.Cg[num].Copy(other.Cg[num]);
			}
			this.Bgm.Copy(other.Bgm);
			this.Voice.Copy(other.Voice);
			this.Effect.Copy(other.Effect);
			for (int num2 = 0; num2 != this.Chr.Length; num2++)
			{
				this.Chr[num2].Copy(other.Chr[num2]);
			}
			for (int num3 = 0; num3 != this.Se.Length; num3++)
			{
				this.Se[num3].Copy(other.Se[num3]);
			}
			for (int num4 = 0; num4 != this.Select.Length; num4++)
			{
				this.Select[num4].Copy(other.Select[num4]);
			}
			this.Message.Copy(other.Message);
		}

		// Token: 0x060003E3 RID: 995 RVA: 0x0000DCA8 File Offset: 0x0000BEA8
		public void SetLog(SCENELOGDATA log, bool bAddLog)
		{
			this.Message.SetLog(log, bAddLog);
			if (log.szFace.Length > 0)
			{
				this.Face.SetLog((this.Message.Sw != SWITCH.OFF) ? SWITCH.ON : SWITCH.OFF, log.szFace, 0);
			}
			else
			{
				this.Face.SetLog(SWITCH.OFF, string.Empty, 0);
			}
			if (log.szBg.Length > 0)
			{
				this.Bg.SetLog(SWITCH.ON, log.szBg, 0);
			}
			else
			{
				this.Bg.SetLog(SWITCH.OFF, string.Empty, 0);
			}
			for (int i = 0; i < log.szCg.Length; i++)
			{
				if (log.szCg[i].Length > 0)
				{
					this.Cg[i].SetLog(SWITCH.ON, log.szCg[i], (int)log.nCgLayer[i], (int)log.nCgX[i], (int)log.nCgY[i]);
				}
				else
				{
					this.Cg[i].Init();
					this.Cg[i].Sw = SWITCH.OFF;
				}
			}
			if (log.szBgm.Length > 0)
			{
				this.Bgm.SetLog(SWITCH.ON, log.szBgm, (int)log.nBgmVol);
			}
			else
			{
				this.Bgm.SetLog(SWITCH.OFF, string.Empty, 0);
			}
			if (log.szVoice.Length > 0)
			{
				this.Voice.SetLog(SWITCH.ON, log.szVoice, 0);
			}
			else
			{
				this.Voice.SetLog(SWITCH.OFF, string.Empty, 0);
			}
			for (int j = 0; j < log.szSe.Length; j++)
			{
				if (log.szSe[j] != null && log.szSe[j].Length > 0)
				{
					this.Se[j].SetLog(SWITCH.ON, log.szSe[j], 1, 100);
				}
				else
				{
					this.Se[j].SetLog(SWITCH.OFF, string.Empty, 0, 100);
				}
			}
			for (int k = 0; k < log.aszChr.Length; k++)
			{
				if (log.aszChr[k].Length > 0)
				{
					this.Chr[k].SetLog(SWITCH.ON, log.aszChr[k], (int)log.anChrPos[k]);
				}
				else
				{
					this.Chr[k].SetLog(SWITCH.OFF, string.Empty, 0);
				}
			}
			for (int l = 0; l < log.aszSelect.Length; l++)
			{
				if (log.aszSelect[l].Length > 0)
				{
					this.Select[l].SetLog(SWITCH.NULL, log.aszSelect[l], log.aszSelectKs[l], log.aszSelectLabel[l], log.aszSelectEffect[l], log.anSelectParam[l]);
				}
				else
				{
					this.Select[l].Init();
				}
			}
		}

		// Token: 0x060003E4 RID: 996 RVA: 0x0000DF8C File Offset: 0x0000C18C
		public bool SaveLog(ref SCENELOGDATA log)
		{
			log.Msg = this.Message.Tag;
			for (int num = 0; num != this.Message.Name.Name.Length; num++)
			{
				log.aszMsgName[num] = this.Message.Name.Name[num];
			}
			log.nMsgFrameType = (char)this.Message.nFrameType;
			log.szFace = this.Face.Name;
			log.szBg = this.Bg.Name;
			for (int i = 0; i < this.Cg.Length; i++)
			{
				log.szCg[i] = this.Cg[i].Name;
				log.nCgLayer[i] = (sbyte)this.Cg[i].nLayer;
				log.nCgX[i] = this.Cg[i].nX;
				log.nCgY[i] = this.Cg[i].nY;
			}
			for (int j = 0; j < this.Chr.Length; j++)
			{
				log.aszChr[j] = this.Chr[j].Name;
				log.anChrPos[j] = (byte)this.Chr[j].nParam;
			}
			for (int k = 0; k < this.Select.Length; k++)
			{
				log.aszSelect[k] = this.Select[k].Name;
				log.aszSelectKs[k] = this.Select[k].KsName;
				log.aszSelectLabel[k] = this.Select[k].LabelName;
				log.aszSelectEffect[k] = this.Select[k].Effect;
				log.anSelectParam[k] = this.Select[k].nParam;
			}
			log.szBgm = this.Bgm.Name;
			log.nBgmVol = (byte)this.Bgm.nVolume;
			log.szVoice = this.Voice.Name;
			for (int l = 0; l < this.Se.Length; l++)
			{
				if (this.Se[l].Name.Length > 0 && this.Se[l].nLoop == 1)
				{
					log.szSe[l] = this.Se[l].Name;
				}
			}
			log.nExist = true;
			return true;
		}

		// Token: 0x040002A9 RID: 681
		public EVENTSCENE_OBJ Face;

		// Token: 0x040002AA RID: 682
		public EVENTSCENE_OBJ Bg;

		// Token: 0x040002AB RID: 683
		public EVENTSCENE_CG[] Cg;

		// Token: 0x040002AC RID: 684
		public EVENTSCENE_BGM Bgm;

		// Token: 0x040002AD RID: 685
		public EVENTSCENE_OBJ Voice;

		// Token: 0x040002AE RID: 686
		public EVENTSCENE_OBJ Effect;

		// Token: 0x040002AF RID: 687
		public EVENTSCENE_OBJ[] Chr;

		// Token: 0x040002B0 RID: 688
		public EVENTSCENE_SE[] Se;

		// Token: 0x040002B1 RID: 689
		public EVENTSCENE_SELCET[] Select;

		// Token: 0x040002B2 RID: 690
		public EVENTSCENE_MSG Message;
	}
}
