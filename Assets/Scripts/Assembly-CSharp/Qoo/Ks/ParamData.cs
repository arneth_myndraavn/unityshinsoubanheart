﻿using System;

namespace Qoo.Ks
{
	// Token: 0x0200008C RID: 140
	public class ParamData
	{
		// Token: 0x060003F2 RID: 1010 RVA: 0x0000E6BC File Offset: 0x0000C8BC
		public ParamData(KSID id, string param)
		{
			this.ID = id;
			this.Param = param;
		}

		// Token: 0x17000091 RID: 145
		// (get) Token: 0x060003F3 RID: 1011 RVA: 0x0000E6D4 File Offset: 0x0000C8D4
		// (set) Token: 0x060003F4 RID: 1012 RVA: 0x0000E6DC File Offset: 0x0000C8DC
		public KSID ID { get; private set; }

		// Token: 0x17000092 RID: 146
		// (get) Token: 0x060003F5 RID: 1013 RVA: 0x0000E6E8 File Offset: 0x0000C8E8
		// (set) Token: 0x060003F6 RID: 1014 RVA: 0x0000E6F0 File Offset: 0x0000C8F0
		public string Param { get; private set; }

		// Token: 0x060003F7 RID: 1015 RVA: 0x0000E6FC File Offset: 0x0000C8FC
		public int ToInt()
		{
			return int.Parse(this.Param);
		}

		// Token: 0x060003F8 RID: 1016 RVA: 0x0000E70C File Offset: 0x0000C90C
		public float ToFloat()
		{
			return float.Parse(this.Param);
		}
	}
}
