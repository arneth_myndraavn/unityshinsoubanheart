﻿using System;

namespace Qoo.Ks
{
	// Token: 0x02000044 RID: 68
	public enum PLAYSTAT
	{
		// Token: 0x040001E4 RID: 484
		NULL,
		// Token: 0x040001E5 RID: 485
		PLAY,
		// Token: 0x040001E6 RID: 486
		STOP,
		// Token: 0x040001E7 RID: 487
		END
	}
}
