﻿using System;

namespace Qoo.Ks
{
	// Token: 0x0200003F RID: 63
	public class SCENELOGDATA
	{
		// Token: 0x06000214 RID: 532 RVA: 0x000089A4 File Offset: 0x00006BA4
		public SCENELOGDATA()
		{
			this.nCgX = new short[4];
			this.nCgY = new short[4];
			this.nCgLayer = new sbyte[4];
			this.aszMsgName = new string[2];
			this.szCg = new string[4];
			this.szSe = new string[2];
			this.aszSelect = new string[10];
			this.aszSelectKs = new string[10];
			this.aszSelectLabel = new string[10];
			this.aszSelectEffect = new string[10];
			this.anSelectParam = new int[10];
			this.aszChr = new string[5];
			this.anChrPos = new byte[5];
			this.WorkArray = new KsWorkLog[2];
			foreach (KsWorkLog ksWorkLog in this.WorkArray)
			{
				ksWorkLog.Reset();
			}
		}

		// Token: 0x17000058 RID: 88
		// (get) Token: 0x06000215 RID: 533 RVA: 0x00008A94 File Offset: 0x00006C94
		// (set) Token: 0x06000216 RID: 534 RVA: 0x00008A9C File Offset: 0x00006C9C
		public bool nExist { get; set; }

		// Token: 0x17000059 RID: 89
		// (get) Token: 0x06000217 RID: 535 RVA: 0x00008AA8 File Offset: 0x00006CA8
		// (set) Token: 0x06000218 RID: 536 RVA: 0x00008AB0 File Offset: 0x00006CB0
		public byte nBgmVol { get; set; }

		// Token: 0x1700005A RID: 90
		// (get) Token: 0x06000219 RID: 537 RVA: 0x00008ABC File Offset: 0x00006CBC
		// (set) Token: 0x0600021A RID: 538 RVA: 0x00008AC4 File Offset: 0x00006CC4
		public short[] nCgX { get; set; }

		// Token: 0x1700005B RID: 91
		// (get) Token: 0x0600021B RID: 539 RVA: 0x00008AD0 File Offset: 0x00006CD0
		// (set) Token: 0x0600021C RID: 540 RVA: 0x00008AD8 File Offset: 0x00006CD8
		public short[] nCgY { get; set; }

		// Token: 0x1700005C RID: 92
		// (get) Token: 0x0600021D RID: 541 RVA: 0x00008AE4 File Offset: 0x00006CE4
		// (set) Token: 0x0600021E RID: 542 RVA: 0x00008AEC File Offset: 0x00006CEC
		public sbyte[] nCgLayer { get; set; }

		// Token: 0x1700005D RID: 93
		// (get) Token: 0x0600021F RID: 543 RVA: 0x00008AF8 File Offset: 0x00006CF8
		// (set) Token: 0x06000220 RID: 544 RVA: 0x00008B00 File Offset: 0x00006D00
		public string[] aszMsgName { get; set; }

		// Token: 0x1700005E RID: 94
		// (get) Token: 0x06000221 RID: 545 RVA: 0x00008B0C File Offset: 0x00006D0C
		// (set) Token: 0x06000222 RID: 546 RVA: 0x00008B14 File Offset: 0x00006D14
		public char nMsgFrameType { get; set; }

		// Token: 0x1700005F RID: 95
		// (get) Token: 0x06000223 RID: 547 RVA: 0x00008B20 File Offset: 0x00006D20
		// (set) Token: 0x06000224 RID: 548 RVA: 0x00008B28 File Offset: 0x00006D28
		public TagData Msg { get; set; }

		// Token: 0x17000060 RID: 96
		// (get) Token: 0x06000225 RID: 549 RVA: 0x00008B34 File Offset: 0x00006D34
		// (set) Token: 0x06000226 RID: 550 RVA: 0x00008B3C File Offset: 0x00006D3C
		public string szFace { get; set; }

		// Token: 0x17000061 RID: 97
		// (get) Token: 0x06000227 RID: 551 RVA: 0x00008B48 File Offset: 0x00006D48
		// (set) Token: 0x06000228 RID: 552 RVA: 0x00008B50 File Offset: 0x00006D50
		public string szBg { get; set; }

		// Token: 0x17000062 RID: 98
		// (get) Token: 0x06000229 RID: 553 RVA: 0x00008B5C File Offset: 0x00006D5C
		// (set) Token: 0x0600022A RID: 554 RVA: 0x00008B64 File Offset: 0x00006D64
		public string[] szCg { get; set; }

		// Token: 0x17000063 RID: 99
		// (get) Token: 0x0600022B RID: 555 RVA: 0x00008B70 File Offset: 0x00006D70
		// (set) Token: 0x0600022C RID: 556 RVA: 0x00008B78 File Offset: 0x00006D78
		public string szBgm { get; set; }

		// Token: 0x17000064 RID: 100
		// (get) Token: 0x0600022D RID: 557 RVA: 0x00008B84 File Offset: 0x00006D84
		// (set) Token: 0x0600022E RID: 558 RVA: 0x00008B8C File Offset: 0x00006D8C
		public string szVoice { get; set; }

		// Token: 0x17000065 RID: 101
		// (get) Token: 0x0600022F RID: 559 RVA: 0x00008B98 File Offset: 0x00006D98
		// (set) Token: 0x06000230 RID: 560 RVA: 0x00008BA0 File Offset: 0x00006DA0
		public string[] szSe { get; set; }

		// Token: 0x17000066 RID: 102
		// (get) Token: 0x06000231 RID: 561 RVA: 0x00008BAC File Offset: 0x00006DAC
		// (set) Token: 0x06000232 RID: 562 RVA: 0x00008BB4 File Offset: 0x00006DB4
		public string[] aszSelect { get; set; }

		// Token: 0x17000067 RID: 103
		// (get) Token: 0x06000233 RID: 563 RVA: 0x00008BC0 File Offset: 0x00006DC0
		// (set) Token: 0x06000234 RID: 564 RVA: 0x00008BC8 File Offset: 0x00006DC8
		public string[] aszSelectKs { get; set; }

		// Token: 0x17000068 RID: 104
		// (get) Token: 0x06000235 RID: 565 RVA: 0x00008BD4 File Offset: 0x00006DD4
		// (set) Token: 0x06000236 RID: 566 RVA: 0x00008BDC File Offset: 0x00006DDC
		public string[] aszSelectLabel { get; set; }

		// Token: 0x17000069 RID: 105
		// (get) Token: 0x06000237 RID: 567 RVA: 0x00008BE8 File Offset: 0x00006DE8
		// (set) Token: 0x06000238 RID: 568 RVA: 0x00008BF0 File Offset: 0x00006DF0
		public string[] aszSelectEffect { get; set; }

		// Token: 0x1700006A RID: 106
		// (get) Token: 0x06000239 RID: 569 RVA: 0x00008BFC File Offset: 0x00006DFC
		// (set) Token: 0x0600023A RID: 570 RVA: 0x00008C04 File Offset: 0x00006E04
		public int[] anSelectParam { get; set; }

		// Token: 0x1700006B RID: 107
		// (get) Token: 0x0600023B RID: 571 RVA: 0x00008C10 File Offset: 0x00006E10
		// (set) Token: 0x0600023C RID: 572 RVA: 0x00008C18 File Offset: 0x00006E18
		public string[] aszChr { get; set; }

		// Token: 0x1700006C RID: 108
		// (get) Token: 0x0600023D RID: 573 RVA: 0x00008C24 File Offset: 0x00006E24
		// (set) Token: 0x0600023E RID: 574 RVA: 0x00008C2C File Offset: 0x00006E2C
		public byte[] anChrPos { get; set; }

		// Token: 0x1700006D RID: 109
		// (get) Token: 0x0600023F RID: 575 RVA: 0x00008C38 File Offset: 0x00006E38
		// (set) Token: 0x06000240 RID: 576 RVA: 0x00008C40 File Offset: 0x00006E40
		public KsWorkLog[] WorkArray { get; set; }
	}
}
