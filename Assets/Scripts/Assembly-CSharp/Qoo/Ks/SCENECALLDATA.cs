﻿using System;
using Qoo.Memory;

namespace Qoo.Ks
{
	// Token: 0x02000041 RID: 65
	public struct SCENECALLDATA
	{
		// Token: 0x17000073 RID: 115
		// (get) Token: 0x0600024E RID: 590 RVA: 0x00008D50 File Offset: 0x00006F50
		// (set) Token: 0x0600024F RID: 591 RVA: 0x00008D58 File Offset: 0x00006F58
		public short sFileNo { get; set; }

		// Token: 0x17000074 RID: 116
		// (get) Token: 0x06000250 RID: 592 RVA: 0x00008D64 File Offset: 0x00006F64
		// (set) Token: 0x06000251 RID: 593 RVA: 0x00008D6C File Offset: 0x00006F6C
		public short sLabelNo { get; set; }

		// Token: 0x17000075 RID: 117
		// (get) Token: 0x06000252 RID: 594 RVA: 0x00008D78 File Offset: 0x00006F78
		// (set) Token: 0x06000253 RID: 595 RVA: 0x00008D80 File Offset: 0x00006F80
		public short sTagNo { get; set; }

		// Token: 0x17000076 RID: 118
		// (get) Token: 0x06000254 RID: 596 RVA: 0x00008D8C File Offset: 0x00006F8C
		// (set) Token: 0x06000255 RID: 597 RVA: 0x00008D94 File Offset: 0x00006F94
		public bool bCall { get; set; }

		// Token: 0x17000077 RID: 119
		// (get) Token: 0x06000256 RID: 598 RVA: 0x00008DA0 File Offset: 0x00006FA0
		// (set) Token: 0x06000257 RID: 599 RVA: 0x00008DA8 File Offset: 0x00006FA8
		public bool bRet { get; set; }

		// Token: 0x17000078 RID: 120
		// (get) Token: 0x06000258 RID: 600 RVA: 0x00008DB4 File Offset: 0x00006FB4
		// (set) Token: 0x06000259 RID: 601 RVA: 0x00008DBC File Offset: 0x00006FBC
		public short sRetNo { get; set; }

		// Token: 0x0600025A RID: 602 RVA: 0x00008DC8 File Offset: 0x00006FC8
		public bool Save(MemFile mem)
		{
			mem.SetInt16(this.sFileNo);
			mem.SetInt16(this.sLabelNo);
			mem.SetInt16(this.sTagNo);
			mem.SetBool(this.bCall);
			mem.SetBool(this.bRet);
			mem.SetInt16(this.sRetNo);
			return true;
		}

		// Token: 0x0600025B RID: 603 RVA: 0x00008E20 File Offset: 0x00007020
		public bool Load(MemFile mem)
		{
			this.sFileNo = mem.GetInt16();
			this.sLabelNo = mem.GetInt16();
			this.sTagNo = mem.GetInt16();
			this.bCall = mem.GetBool();
			this.bRet = mem.GetBool();
			this.sRetNo = mem.GetInt16();
			return true;
		}
	}
}
