﻿using System;

namespace Qoo.Ks
{
	// Token: 0x0200005A RID: 90
	internal class KsTagInfo_SE2 : KsTagInfo_SE
	{
		// Token: 0x060002D9 RID: 729 RVA: 0x00009EA8 File Offset: 0x000080A8
		public KsTagInfo_SE2()
		{
			base.SeNo = 1;
		}

		// Token: 0x060002DA RID: 730 RVA: 0x00009EB8 File Offset: 0x000080B8
		public override KSID GetId()
		{
			return KSID.SE2;
		}
	}
}
