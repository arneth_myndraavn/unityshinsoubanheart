﻿using System;

namespace Qoo.Ks
{
	// Token: 0x02000050 RID: 80
	internal class KsTagInfo_CHARA4 : KsTagInfo_CHARA
	{
		// Token: 0x060002BB RID: 699 RVA: 0x00009B30 File Offset: 0x00007D30
		public KsTagInfo_CHARA4()
		{
			base.CharNo = 3;
		}

		// Token: 0x060002BC RID: 700 RVA: 0x00009B40 File Offset: 0x00007D40
		public override KSID GetId()
		{
			return KSID.CHARA4;
		}
	}
}
