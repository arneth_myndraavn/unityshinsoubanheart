﻿using System;
using Qoo.File;
using Qoo.Game;

namespace Qoo.Ks
{
	// Token: 0x02000069 RID: 105
	internal class KsTagInfo_IF_SYS_READED_NEXT : KsTagInfo
	{
		// Token: 0x06000313 RID: 787 RVA: 0x0000A8B0 File Offset: 0x00008AB0
		public KsTagInfo_IF_SYS_READED_NEXT()
		{
			this.IsSys = true;
		}

		// Token: 0x17000089 RID: 137
		// (get) Token: 0x06000314 RID: 788 RVA: 0x0000A8C0 File Offset: 0x00008AC0
		// (set) Token: 0x06000315 RID: 789 RVA: 0x0000A8C8 File Offset: 0x00008AC8
		protected bool IsSys { get; set; }

		// Token: 0x06000316 RID: 790 RVA: 0x0000A8D4 File Offset: 0x00008AD4
		public override KSID GetId()
		{
			return KSID.IF_SYS_READED_NEXT;
		}

		// Token: 0x06000317 RID: 791 RVA: 0x0000A8D8 File Offset: 0x00008AD8
		public override bool IsJumpTag()
		{
			return true;
		}

		// Token: 0x06000318 RID: 792 RVA: 0x0000A8DC File Offset: 0x00008ADC
		public override TAG_RESULT Run(EventPlayer player, TagData tag)
		{
			string paramString = tag.GetParamString(KSID.CHECK, string.Empty);
			string paramString2 = tag.GetParamString(KSID.LABEL, string.Empty);
			int num = Akb.SearchFile(FileId.Normalize(paramString));
			int num2 = Akb.SearchLabel(num, paramString2);
			KSID id;
			KSID id2;
			if ((!this.IsSys) ? GameData.IsRead(num, num2, 0) : SysData.IsRead(num, num2, 0))
			{
				id = KSID.COMP;
				id2 = KSID.COMPTARGET;
			}
			else
			{
				id = KSID.INCOMP;
				id2 = KSID.INCOMPTARGET;
			}
			string paramString3 = tag.GetParamString(id, string.Empty);
			string paramString4 = tag.GetParamString(id2, string.Empty);
			if (paramString3.Length > 0 || paramString4.Length > 0)
			{
				return (!player.JumpKs(paramString3, paramString4, this.IsCallRet())) ? TAG_RESULT.NEXT : TAG_RESULT.CONTINUE;
			}
			return TAG_RESULT.NEXT;
		}
	}
}
