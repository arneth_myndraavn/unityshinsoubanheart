﻿using System;
using UnityEngine;

namespace Qoo.Ks
{
	// Token: 0x0200003A RID: 58
	public class KsPlayerConfig
	{
		// Token: 0x060001DF RID: 479 RVA: 0x000082FC File Offset: 0x000064FC
		public KsPlayerConfig()
		{
			this.m_Char = new Vector3[5];
			this.m_Msg = new KsMsgConfig[5];
		}

		// Token: 0x1700004D RID: 77
		// (get) Token: 0x060001E0 RID: 480 RVA: 0x0000831C File Offset: 0x0000651C
		// (set) Token: 0x060001E1 RID: 481 RVA: 0x00008324 File Offset: 0x00006524
		public Vector3[] Char
		{
			get
			{
				return this.m_Char;
			}
			set
			{
				this.m_Char = value;
			}
		}

		// Token: 0x1700004E RID: 78
		// (get) Token: 0x060001E2 RID: 482 RVA: 0x00008330 File Offset: 0x00006530
		// (set) Token: 0x060001E3 RID: 483 RVA: 0x00008338 File Offset: 0x00006538
		public Vector3 MarkFront { get; set; }

		// Token: 0x1700004F RID: 79
		// (get) Token: 0x060001E4 RID: 484 RVA: 0x00008344 File Offset: 0x00006544
		// (set) Token: 0x060001E5 RID: 485 RVA: 0x0000834C File Offset: 0x0000654C
		public Vector3 MarkBack { get; set; }

		// Token: 0x17000050 RID: 80
		// (get) Token: 0x060001E6 RID: 486 RVA: 0x00008358 File Offset: 0x00006558
		// (set) Token: 0x060001E7 RID: 487 RVA: 0x00008360 File Offset: 0x00006560
		public float[] FontSize { get; set; }

		// Token: 0x17000051 RID: 81
		// (get) Token: 0x060001E8 RID: 488 RVA: 0x0000836C File Offset: 0x0000656C
		// (set) Token: 0x060001E9 RID: 489 RVA: 0x00008374 File Offset: 0x00006574
		public KsMsgConfig[] Msg
		{
			get
			{
				return this.m_Msg;
			}
			set
			{
				this.m_Msg = value;
			}
		}

		// Token: 0x040001A5 RID: 421
		private Vector3[] m_Char;

		// Token: 0x040001A6 RID: 422
		private Vector3 m_MarkFront;

		// Token: 0x040001A7 RID: 423
		private Vector3 m_MarkBack;

		// Token: 0x040001A8 RID: 424
		private float[] m_fFontSize;

		// Token: 0x040001A9 RID: 425
		private KsMsgConfig[] m_Msg;
	}
}
