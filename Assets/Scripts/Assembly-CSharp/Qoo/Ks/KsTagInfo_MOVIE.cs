﻿using System;
using Qoo.Game;
using Qoo.Graphics;
using Qoo.Input;

namespace Qoo.Ks
{
	// Token: 0x0200005B RID: 91
	internal class KsTagInfo_MOVIE : KsTagInfo
	{
		// Token: 0x060002DC RID: 732 RVA: 0x00009EC4 File Offset: 0x000080C4
		public override KSID GetId()
		{
			return KSID.MOVIE;
		}

		// Token: 0x060002DD RID: 733 RVA: 0x00009EC8 File Offset: 0x000080C8
		public override bool IsJumpEffectResetTag()
		{
			return true;
		}

		// Token: 0x060002DE RID: 734 RVA: 0x00009ECC File Offset: 0x000080CC
		public override bool IsJumpBreak(bool bFront, bool bMemoryMode)
		{
			return true;
		}

		// Token: 0x060002DF RID: 735 RVA: 0x00009ED0 File Offset: 0x000080D0
		public override TAG_RESULT Run(EventPlayer player, TagData tag)
		{
			UnityEngine.Debug.LogWarning("RUN");
			Singleton<MsgWnd>.Instance.ResetMessage();
			Singleton<MsgWnd>.Instance.Show(false);
			if (!player.RestoreOn)
			{
				if (!SysData.IsSkipContinue())
				{
					KsInput.Unlock();
				}
				if (player.JumpOn)
				{
					Graph.EnableFade(false);
					player.SetJump(false);
				}
				ParamData param;
				if ((param = tag.GetParam(KSID.FILE)) != null)
				{
					Movie.Play(param.Param, true);
					SysData.SetLookMovie(param.Param);
				}
			}
			return TAG_RESULT.NEXT_EXIT;
		}
	}
}
