﻿using System;
using Qoo.Application;
using Qoo.Def;
using Qoo.File;
using Qoo.Game;
using Qoo.Graphics;
using Qoo.Input;
using Qoo.SoundSystem;

namespace Qoo.Ks
{
	// Token: 0x02000035 RID: 53
	public class EventPlayer : KsPlayer
	{
		// Token: 0x0600019D RID: 413 RVA: 0x00007238 File Offset: 0x00005438
		public EventPlayer()
		{
			this.Scene = new KsScene();
		}

		// Token: 0x17000037 RID: 55
		// (get) Token: 0x0600019E RID: 414 RVA: 0x0000724C File Offset: 0x0000544C
		// (set) Token: 0x0600019F RID: 415 RVA: 0x00007254 File Offset: 0x00005454
		public PLAY_MODE Mode { get; private set; }

		// Token: 0x17000038 RID: 56
		// (get) Token: 0x060001A0 RID: 416 RVA: 0x00007260 File Offset: 0x00005460
		// (set) Token: 0x060001A1 RID: 417 RVA: 0x00007268 File Offset: 0x00005468
		public KsScene Scene { get; private set; }

		// Token: 0x17000039 RID: 57
		// (get) Token: 0x060001A2 RID: 418 RVA: 0x00007274 File Offset: 0x00005474
		// (set) Token: 0x060001A3 RID: 419 RVA: 0x0000727C File Offset: 0x0000547C
		private bool RewOn { get; set; }

		// Token: 0x1700003A RID: 58
		// (get) Token: 0x060001A4 RID: 420 RVA: 0x00007288 File Offset: 0x00005488
		// (set) Token: 0x060001A5 RID: 421 RVA: 0x00007290 File Offset: 0x00005490
		private int nRawWaitCnt { get; set; }

		// Token: 0x1700003B RID: 59
		// (get) Token: 0x060001A6 RID: 422 RVA: 0x0000729C File Offset: 0x0000549C
		// (set) Token: 0x060001A7 RID: 423 RVA: 0x000072A4 File Offset: 0x000054A4
		public bool JumpOn { get; set; }

		// Token: 0x1700003C RID: 60
		// (get) Token: 0x060001A8 RID: 424 RVA: 0x000072B0 File Offset: 0x000054B0
		// (set) Token: 0x060001A9 RID: 425 RVA: 0x000072B8 File Offset: 0x000054B8
		public bool RestoreOn { get; private set; }

		// Token: 0x1700003D RID: 61
		// (get) Token: 0x060001AA RID: 426 RVA: 0x000072C4 File Offset: 0x000054C4
		// (set) Token: 0x060001AB RID: 427 RVA: 0x000072CC File Offset: 0x000054CC
		private bool MesFull { get; set; }

		// Token: 0x1700003E RID: 62
		// (get) Token: 0x060001AC RID: 428 RVA: 0x000072D8 File Offset: 0x000054D8
		// (set) Token: 0x060001AD RID: 429 RVA: 0x000072E0 File Offset: 0x000054E0
		public string Name { get; set; }

		// Token: 0x1700003F RID: 63
		// (get) Token: 0x060001AE RID: 430 RVA: 0x000072EC File Offset: 0x000054EC
		// (set) Token: 0x060001AF RID: 431 RVA: 0x000072F4 File Offset: 0x000054F4
		public int CurKsNo { get; private set; }

		// Token: 0x17000040 RID: 64
		// (get) Token: 0x060001B0 RID: 432 RVA: 0x00007300 File Offset: 0x00005500
		// (set) Token: 0x060001B1 RID: 433 RVA: 0x00007308 File Offset: 0x00005508
		public bool IsParamLock { get; private set; }

		// Token: 0x17000041 RID: 65
		// (get) Token: 0x060001B2 RID: 434 RVA: 0x00007314 File Offset: 0x00005514
		// (set) Token: 0x060001B3 RID: 435 RVA: 0x00007330 File Offset: 0x00005530
		public bool Face
		{
			get
			{
				return this.Scene.GrpList[6].A != 0;
			}
			set
			{
				this.Scene.RedrawFace(value);
			}
		}

		// Token: 0x060001B4 RID: 436 RVA: 0x00007340 File Offset: 0x00005540
		public bool Init(PLAY_MODE mode)
		{
			base.Init();
			this.Mode = mode;
			this.Scene.Init();
			App.QooKsData.Clear();
			this.RewOn = false;
			this.nRawWaitCnt = 0;
			this.MesFull = false;
			this.Name = string.Empty;
			this.CurKsNo = -1;
			GameData.EnableChangeParam = true;
			GameData.OldWork.Clear();
			this.m_isNextKsRead = false;
			return true;
		}

		// Token: 0x060001B5 RID: 437 RVA: 0x000073B4 File Offset: 0x000055B4
		public bool Release()
		{
			App.QooKsData.Clear();
			GameData.EnableChangeParam = false;
			GameData.OldWork.Clear();
			this.Scene.Release();
			KsInput.Clear();
			return true;
		}

		// Token: 0x060001B6 RID: 438 RVA: 0x000073F0 File Offset: 0x000055F0
		internal void Restore(GAME_SAVE_DATA data)
		{
			this.RestoreOn = true;
			KSID ksid = KSID.UNKNOWN;
			this.Scene.SceneInit();
			this.Scene.ApplyBackup(data.m_Scene);
			int num = data.m_Log.m_LogData.Length;
			SCENEPOSDATA[] logData = data.m_Log.m_LogData;
			for (int num2 = 0; num2 != num; num2++)
			{
				if (ksid == KSID.STOP)
				{
					this.Scene.EntrySelect(-1, string.Empty, string.Empty, string.Empty, 0, string.Empty);
				}
				this.LoadNext((int)logData[num2].sFileNo, 0, 0);
				this.SetNext((int)logData[num2].sLabelNo, (int)logData[num2].sTagNo);
				base.PeekTag();
				ksid = base.CurTag.ID;
				if (num2 == num - 1 && ksid == KSID.EXEC)
				{
					this.RestoreOn = false;
				}
				this.IsParamLock = logData[num2].bLock;
				this.ProcTag_Restore(base.CurTag, KsTagTable.GetInfo(base.CurTag.ID));
				if (num2 == num - 1 && ksid == KSID.EXEC)
				{
					Singleton<UnityGraph>.Instance.Effect("FadeOut", 0f);
				}
			}
			this.RestoreOn = false;
			this.JumpOn = true;
			this.IsParamLock = false;
			this.Scene.SceneApply(true, true);
			this.Scene.SceneApplyCg(false);
			if (!this.IsExecutedTag())
			{
				this.SnapShot(true);
			}
			this.Scene.SceneResetEffect();
			this.Scene.SceneReset();
			Singleton<MsgWnd>.Instance.ResetMessage();
			base.ResetCallCount(0);
		}

		// Token: 0x060001B7 RID: 439 RVA: 0x00007584 File Offset: 0x00005784
		public bool Load(string filename, string labelname = "")
		{
			FileId fileId = new FileId(string.Empty);
			fileId.Set(filename);
			int num = Akb.SearchFile(fileId.Name);
			int num2 = (labelname.Length <= 0) ? 0 : Akb.SearchLabel(num, labelname);
			if (num2 < 0)
			{
				num2 = 0;
			}
			bool flag = this.LoadNext(num, num2, 0);
			if (flag)
			{
				App.QooKsLog.Reset();
				this.Scene.Backup();
				this.SnapShot(true);
			}
			return flag;
		}

		// Token: 0x060001B8 RID: 440 RVA: 0x00007604 File Offset: 0x00005804
		public bool LoadNext(int nKsNo, int nLabelNo = 0, int nTagNo = 0)
		{
			if (nKsNo != this.CurKsNo)
			{
				string name = Akb.GetFileInfo(nKsNo).Name;
				if (!App.QooKsData.IsItem(name))
				{
					App.QooKsData.Add(name);
				}
				this.Name = name;
				this.CurKsNo = nKsNo;
				this.SetKsData(App.QooKsData.Get(name));
				this.m_isNextKsRead = true;
			}
			this.SetNext(nLabelNo, nTagNo);
			return true;
		}

		// Token: 0x060001B9 RID: 441 RVA: 0x00007678 File Offset: 0x00005878
		protected override TAG_RESULT ProcTag(TagData tag)
		{
			this.Scene.KeyWait.Show = true;
			KsInput.Pause = true;
			this.m_isNextKsRead = false;
			KsTagInfo info = KsTagTable.GetInfo(tag.ID);
			if (KsInput.IsJump)
			{
				if (!KsInput.IsRewind)
				{
					return this.ProcTag_JumpFront(tag, info);
				}
				return this.ProcTag_JumpBack(tag, info);
			}
			else
			{
				this.Scene.UpdateSkipMark();
				if (this.RewOn)
				{
					return this.ProcTag_Back(tag, info);
				}
				if (KsInput.IsRewind && App.QooKsLog.IsRewind())
				{
					this.SetRewind(true);
					return TAG_RESULT.CONTINUE;
				}
				if (KsInput.IsRewind)
				{
					KsInput.Clear();
				}
				return this.ProcTag_Next(tag, info);
			}
		}

		// Token: 0x060001BA RID: 442 RVA: 0x00007730 File Offset: 0x00005930
		private TAG_RESULT ProcTag_Next(TagData tag, KsTagInfo info)
		{
			bool flag = !info.IsJumpTag();
			if (flag && GameData.CheckReadedSkip(this.CurKsNo, base.CurLabelNo, base.CurTagNo))
			{
				Sound.VoiceStop();
			}
			if (base.TagCallCnt == 0)
			{
				KsInput.IsTrig = false;
			}
			TAG_RESULT tag_RESULT = info.Run(this, tag);
			int num = 0;
			if (KsInput.IsSkip)
			{
				num |= 1;
			}
			if (flag)
			{
				if (GameData.CheckReadedSkip(this.CurKsNo, base.CurLabelNo, base.CurTagNo))
				{
					num |= 2;
				}
				else
				{
					this.Scene.ResetSkipMark();
				}
			}
			this.Scene.SetSkipFlag(num);
			this.Scene.SceneApply(info.IsMessage(), true);
			if (this.MesFull && info.IsMessage())
			{
				Singleton<MsgWnd>.Instance.Full();
			}
			this.MesFull = false;
			if (!this.IsExecutedTag() && !info.IsUserSnapShot())
			{
				this.SnapShot(true);
			}
			if (tag.Connect && info.IsEnabelConnect())
			{
				if (tag_RESULT == TAG_RESULT.NEXT_EXIT)
				{
					tag_RESULT = TAG_RESULT.NEXT;
				}
			}
			else
			{
				if (!this.JumpOn)
				{
					this.Scene.SceneApplyEffect();
				}
				this.Scene.SceneApplyCg(!this.JumpOn);
				this.Scene.SceneResetEffect();
			}
			this.Scene.SceneReset();
			if (this.JumpOn)
			{
				this.JumpOn = false;
				App.FrameUpdate(true);
				Graph.Fade_In();
			}
			if (this.m_isNextKsRead)
			{
				base.ResetCallCount(base.TagCallCnt - 1);
				return TAG_RESULT.CONTINUE_EXIT;
			}
			return tag_RESULT;
		}

		// Token: 0x060001BB RID: 443 RVA: 0x000078D0 File Offset: 0x00005AD0
		private TAG_RESULT ProcTag_Back(TagData tag, KsTagInfo info)
		{
			if (!KsInput.IsRewind)
			{
				this.SetRewind(false);
				return TAG_RESULT.CONTINUE_EXIT;
			}
			if (Singleton<SelectWnd>.Instance.IsRun)
			{
				Singleton<SelectWnd>.Instance.End();
			}
			if (!this.JumpOn && this.nRawWaitCnt > 0)
			{
				this.nRawWaitCnt--;
				return TAG_RESULT.CONTINUE_EXIT;
			}
			this.nRawWaitCnt = 2;
			GameData.SetRead(this.CurKsNo, base.CurLabelNo, base.CurTagNo);
			while (App.QooKsLog.IsRewind())
			{
				if (tag.IsMessage())
				{
					App.QooBackLog.RemoveMessage(this.CurKsNo, base.CurLabelNo, tag.MesNo);
				}
				Sound.VoiceStop();
				int nextLog = EventPlayer.Pop();
				tag = this.SetNextLog(nextLog);
				info = KsTagTable.GetInfo(tag.ID);
				bool flag = info.IsRevSceneApply();
				bool flag2 = info.IsJumpBreak(false, this.Mode == PLAY_MODE.MEMORY);
				bool flag3 = info.IsRevContinue();
				if (flag)
				{
					SCENELOGDATA sceneCurrent = App.QooKsLog.GetSceneCurrent();
					bool flag4 = info.IsMessage();
					if (sceneCurrent.Msg != null && sceneCurrent.Msg.ID == KSID.BACKLOG)
					{
						Singleton<MsgWnd>.Instance.IsSilentMode = true;
					}
					else if (sceneCurrent.Msg != null)
					{
						Singleton<MsgWnd>.Instance.IsSilentMode = false;
					}
					if (flag4)
					{
						App.QooBackLog.SetMesNo(this.CurKsNo, base.CurLabelNo, tag.MesNo);
					}
					if (this.JumpOn)
					{
						this.Scene.SceneSetLog(sceneCurrent, flag4 && App.QooBackLog.GetNum() == 0);
					}
					else
					{
						this.Scene.SceneApplyLog(sceneCurrent, flag4, flag4 && App.QooBackLog.GetNum() == 0);
						this.Scene.SceneApplyCg(false);
					}
					Sound.VoiceStop();
				}
				if (info.IsMessage())
				{
					this.Scene.SetMessage(null);
					this.MesFull = true;
					Singleton<MsgWnd>.Instance.Full();
					string message = Singleton<MsgWnd>.Instance.Message;
					GameData.LastMessage = message;
				}
				else
				{
					this.RecallSaveMessage();
				}
				if (!this.JumpOn || !flag2)
				{
					if (tag.Connect)
					{
						continue;
					}
					if (!this.JumpOn)
					{
					}
					if (this.JumpOn || flag3)
					{
						continue;
					}
				}
				base.ResetCallCount(-1);
				return TAG_RESULT.CONTINUE_EXIT;
			}
			this.SetRewind(false);
			return TAG_RESULT.CONTINUE_EXIT;
		}

		// Token: 0x060001BC RID: 444 RVA: 0x00007B5C File Offset: 0x00005D5C
		private static int Pop()
		{
			int num = App.QooKsLog.Pop();
			Debug.Assert(num != -1, "error:LogData nothing!!");
			GameData.OldWork.Clear();
			SCENELOGDATA sceneCurrent = App.QooKsLog.GetSceneCurrent();
			if (sceneCurrent.WorkArray != null && sceneCurrent.WorkArray.Length > 0)
			{
				foreach (KsWorkLog ksWorkLog in sceneCurrent.WorkArray)
				{
					if (!ksWorkLog.IsEmpty())
					{
						GameData.SetParamString(ksWorkLog.Value, ksWorkLog.Index, ksWorkLog.Old);
					}
				}
				sceneCurrent.WorkArray = null;
			}
			return num;
		}

		// Token: 0x060001BD RID: 445 RVA: 0x00007C10 File Offset: 0x00005E10
		private TAG_RESULT ProcTag_Restore(TagData tag, KsTagInfo info)
		{
			TAG_RESULT tag_RESULT = info.Run(this, tag);
			if (!this.IsExecutedTag() && info.IsMessage())
			{
				this.Scene.SceneApply_Message(true);
			}
			if (!this.IsExecutedTag())
			{
				this.SnapShot(true);
			}
			this.Scene.EntryVoice(false, string.Empty);
			if (tag.Connect && tag_RESULT == TAG_RESULT.NEXT_EXIT)
			{
				tag_RESULT = TAG_RESULT.NEXT;
			}
			base.RunResult(tag_RESULT);
			return tag_RESULT;
		}

		// Token: 0x060001BE RID: 446 RVA: 0x00007C8C File Offset: 0x00005E8C
		private TAG_RESULT ProcTag_JumpFront(TagData tag, KsTagInfo info)
		{
			if (this.JumpOn || this.RestoreOn)
			{
				TagData tagData = tag;
				KsTagInfo ksTagInfo = info;
				while (!ksTagInfo.IsJumpBreak(true, this.Mode == PLAY_MODE.MEMORY))
				{
					if (ksTagInfo.IsJumpTag() || SysData.GetSkip() != SKIP_MODE.READ || SysData.IsRead(this.CurKsNo, base.CurLabelNo, base.CurTagNo + 1))
					{
						if (this.ProcTag_Restore(tagData, ksTagInfo) != TAG_RESULT.END)
						{
							if (base.NextTagNo >= 0)
							{
								base.PeekTag();
								tagData = base.CurTag;
								ksTagInfo = KsTagTable.GetInfo(tagData.ID);
							}
							else
							{
								base.ResetCallCount(base.TagCallCnt + 1);
							}
							if (this.m_isNextKsRead)
							{
								base.ResetCallCount(base.TagCallCnt - 1);
								return TAG_RESULT.CONTINUE_EXIT;
							}
							continue;
						}
					}
					//IL_165:
					if (ksTagInfo.GetId() == KSID.MOVIE)
					{
						this.Scene.SceneApply(false, true);
					}
					this.RestoreOn = false;
					KsInput.Unlock();
					base.ResetCallCount(-1);
					return TAG_RESULT.CONTINUE_EXIT;
				}
				if (ksTagInfo.IsJumpEffectResetTag())
				{
					this.Scene.SceneResetEffect();
				}
				//goto IL_165;
				if (ksTagInfo.GetId() == KSID.MOVIE)
				{
					this.Scene.SceneApply(false, true);
				}
				this.RestoreOn = false;
				KsInput.Unlock();
				base.ResetCallCount(-1);
				return TAG_RESULT.CONTINUE_EXIT;
				if (ksTagInfo.IsJumpEffectResetTag())
				{
					this.Scene.SceneResetEffect();
				}
			}
			SKIP_MODE skip = SysData.GetSkip();
			if (skip != SKIP_MODE.DISABLE)
			{
				if (skip == SKIP_MODE.READ)
				{
					if (!SysData.IsRead(this.CurKsNo, base.CurLabelNo, base.CurTagNo + 1))
					{
						base.ResetCallCount(-1);
						KsInput.Unlock();
						return TAG_RESULT.CONTINUE_EXIT;
					}
				}
				Sound.VoiceStop();
				Sound.SeSlotStopAll();
				Graph.Fade_Out();
				this.JumpOn = true;
				this.RestoreOn = true;
				return TAG_RESULT.CONTINUE_EXIT;
			}
			base.ResetCallCount(-1);
			KsInput.Unlock();
			return TAG_RESULT.CONTINUE_EXIT;
		}

		// Token: 0x060001BF RID: 447 RVA: 0x00007E2C File Offset: 0x0000602C
		private TAG_RESULT ProcTag_JumpBack(TagData tag, KsTagInfo info)
		{
			if (App.QooKsLog.IsRewind())
			{
				this.JumpOn = true;
				Sound.SeSlotStopAll();
				Graph.Fade_Out();
				if (Singleton<SelectWnd>.Instance.IsRun)
				{
					Singleton<SelectWnd>.Instance.End();
				}
				this.ProcTag_Back(tag, info);
				KsInput.Unlock();
			}
			this.SetRewind(false);
			this.Scene.SceneApply(false, true);
			return TAG_RESULT.CONTINUE_EXIT;
		}

		// Token: 0x060001C0 RID: 448 RVA: 0x00007E98 File Offset: 0x00006098
		private bool SetRewind(bool bEnable)
		{
			this.RewOn = bEnable;
			if (!bEnable)
			{
				base.ResetCallCount(-1);
				KsInput.Unlock();
			}
			return true;
		}

		// Token: 0x060001C1 RID: 449 RVA: 0x00007EB4 File Offset: 0x000060B4
		public int SnapShot(bool bRun = true)
		{
			if (base.CurLabelNo == -1 && base.CurTagNo == -1)
			{
				return -1;
			}
			int result = App.QooKsLog.Push();
			SCENELOGDATA sceneCurrent = App.QooKsLog.GetSceneCurrent();
			this.Scene.SaveSceneLog(ref sceneCurrent);
			if (GameData.OldWork.Count > 0)
			{
				sceneCurrent.WorkArray = GameData.OldWork.ToArray();
			}
			GameData.OldWork.Clear();
			App.QooKsLog.SetSceneCurrrent(sceneCurrent);
			SCENEPOSDATA posCurrent = App.QooKsLog.GetPosCurrent();
			posCurrent.sFileNo = (short)this.CurKsNo;
			posCurrent.sLabelNo = (short)base.CurLabelNo;
			posCurrent.sTagNo = (short)base.CurTagNo;
			posCurrent.bRun = bRun;
			App.QooKsLog.SetPosCurrent(posCurrent);
			GameData.SetRead((int)posCurrent.sFileNo, (int)posCurrent.sLabelNo, (int)(posCurrent.sTagNo + 1));
			return result;
		}

		// Token: 0x060001C2 RID: 450 RVA: 0x00007F98 File Offset: 0x00006198
		private TagData SetNextLog(int nPos)
		{
			SCENEPOSDATA pos = App.QooKsLog.GetPos(nPos);
			Debug.Print(string.Format("Back:KS:{0}:{1}:{2}(LogNo={3})\n", new object[]
			{
				pos.sFileNo,
				pos.sLabelNo,
				pos.sTagNo,
				nPos
			}));
			this.LoadNext((int)pos.sFileNo, (int)pos.sLabelNo, (int)pos.sTagNo);
			base.PeekTag();
			GameData.SetRead(this.CurKsNo, base.CurLabelNo, base.CurTagNo);
			return base.CurTag;
		}

		// Token: 0x060001C3 RID: 451 RVA: 0x00008038 File Offset: 0x00006238
		public bool IsExecutedTag()
		{
			SCENEPOSDATA posCurrent = App.QooKsLog.GetPosCurrent();
			if (posCurrent != null && (int)posCurrent.sFileNo == this.CurKsNo && (int)posCurrent.sLabelNo == base.CurLabelNo && (int)posCurrent.sTagNo == base.CurTagNo)
			{
				SCENELOGDATA sceneCurrent = App.QooKsLog.GetSceneCurrent();
				return sceneCurrent.nExist;
			}
			return false;
		}

		// Token: 0x060001C4 RID: 452 RVA: 0x0000809C File Offset: 0x0000629C
		internal void SetJump(bool isJump)
		{
			this.JumpOn = isJump;
		}

		// Token: 0x060001C5 RID: 453 RVA: 0x000080A8 File Offset: 0x000062A8
		internal bool JumpKs(string ks, string label, bool IsCall)
		{
			Debug.Assert(ks != null && label != null);
			bool flag = ks.Length > 0;
			bool flag2 = label.Length > 0;
			if (!flag && !flag2)
			{
				return false;
			}
			if (!this.IsExecutedTag())
			{
				this.SnapShot(true);
			}
			if (flag)
			{
				FileId fileId = new FileId(string.Empty);
				fileId.Set(ks);
				this.LoadNext(Akb.SearchFile(fileId.Name), 0, 0);
			}
			if (label.IndexOf('*') >= 0)
			{
				label = label.Substring(label.IndexOf('*') + 1);
			}
			int num = (!flag2) ? 0 : base.GetLabelNo(label);
			Debug.Assert(num >= 0, string.Format("====ERROR!!!!====:KS：{0} のラベル名:'{1}'は見つかりません\n", ks, label));
			this.SetNext(num, 0);
			return true;
		}

		// Token: 0x060001C6 RID: 454 RVA: 0x00008184 File Offset: 0x00006384
		internal bool ReturnKs()
		{
			throw new NotImplementedException();
		}

		// Token: 0x060001C7 RID: 455 RVA: 0x0000818C File Offset: 0x0000638C
		private bool RecallSaveMessage()
		{
			for (int i = App.QooKsLog.GetLogNum() - 1; i >= 0; i--)
			{
				SCENELOGDATA scene = App.QooKsLog.GetScene(i);
				if (scene != null && scene.Msg != null)
				{
					bool isVisible = Singleton<MsgWnd>.Instance.IsVisible;
					this.Scene.SetMessage(scene.Msg);
					GameData.LastMessage = Singleton<MsgWnd>.Instance.Message;
					Singleton<MsgWnd>.Instance.ResetMessage();
					Singleton<MsgWnd>.Instance.Show(isVisible);
					return true;
				}
			}
			return false;
		}

		// Token: 0x0400015B RID: 347
		private bool m_isNextKsRead;
	}
}
