﻿using System;
using Qoo.Memory;

namespace Qoo.Ks
{
	// Token: 0x02000083 RID: 131
	public class EVENTSCENE_CG
	{
		// Token: 0x060003B0 RID: 944 RVA: 0x0000CE0C File Offset: 0x0000B00C
		public EVENTSCENE_CG()
		{
			this.Init();
		}

		// Token: 0x060003B1 RID: 945 RVA: 0x0000CE1C File Offset: 0x0000B01C
		public void Reset()
		{
			this.Sw = SWITCH.NULL;
		}

		// Token: 0x060003B2 RID: 946 RVA: 0x0000CE28 File Offset: 0x0000B028
		public void Init()
		{
			this.Name = string.Empty;
			this.Sw = SWITCH.NULL;
			this.SwMove = SWITCH.NULL;
			this.nX = (this.nY = (this.nFromX = (this.nFromY = (this.nTime = 0))));
			this.nLayer = 0;
		}

		// Token: 0x060003B3 RID: 947 RVA: 0x0000CE80 File Offset: 0x0000B080
		public void Copy(EVENTSCENE_CG other)
		{
			this.Name = other.Name;
			this.Sw = other.Sw;
			this.SwMove = other.SwMove;
			this.nX = other.nX;
			this.nY = other.nY;
			this.nFromX = other.nFromX;
			this.nFromY = other.nFromY;
			this.nTime = other.nTime;
			this.nLayer = other.nLayer;
		}

		// Token: 0x060003B4 RID: 948 RVA: 0x0000CEFC File Offset: 0x0000B0FC
		public void Save(MemFile conv)
		{
			conv.SetStringUtf16(this.Name);
		}

		// Token: 0x060003B5 RID: 949 RVA: 0x0000CF0C File Offset: 0x0000B10C
		public void Load(MemFile conv)
		{
			this.Name = conv.GetStringUtf16();
		}

		// Token: 0x060003B6 RID: 950 RVA: 0x0000CF1C File Offset: 0x0000B11C
		public void SetLog(SWITCH sw_, string name_, int nLayer_, int x, int y)
		{
			this.Sw = sw_;
			this.Name = name_;
			this.SwMove = SWITCH.OFF;
			this.nLayer = nLayer_;
			this.nX = (this.nFromX = (short)x);
			this.nY = (this.nFromY = (short)y);
			this.nTime = 0;
		}

		// Token: 0x060003B7 RID: 951 RVA: 0x0000CF70 File Offset: 0x0000B170
		public void Set(bool bOn, string name = "", int nX_ = 0, int nY_ = 0, int nFromX_ = 0, int nFromY_ = 0, int nTime_ = 0, int nLayer_ = -1)
		{
			this.Sw = ((!bOn) ? SWITCH.OFF : SWITCH.ON);
			this.SwMove = (((!bOn || nX_ == nFromX_) && (!bOn || nY_ == nFromY_)) ? SWITCH.NULL : SWITCH.ON);
			this.Name = name;
			this.nX = (short)nX_;
			this.nY = (short)nY_;
			this.nFromX = (short)nFromX_;
			this.nFromY = (short)nFromY_;
			this.nTime = (short)nTime_;
			this.nLayer = nLayer_;
		}

		// Token: 0x0400028C RID: 652
		public string Name;

		// Token: 0x0400028D RID: 653
		public SWITCH Sw;

		// Token: 0x0400028E RID: 654
		public SWITCH SwMove;

		// Token: 0x0400028F RID: 655
		public short nX;

		// Token: 0x04000290 RID: 656
		public short nY;

		// Token: 0x04000291 RID: 657
		public short nFromX;

		// Token: 0x04000292 RID: 658
		public short nFromY;

		// Token: 0x04000293 RID: 659
		public short nTime;

		// Token: 0x04000294 RID: 660
		public int nLayer;
	}
}
