﻿using System;

namespace Qoo.Ks
{
	// Token: 0x0200004D RID: 77
	internal class KsTagInfo_CHARA1 : KsTagInfo_CHARA
	{
		// Token: 0x060002B5 RID: 693 RVA: 0x00009AF4 File Offset: 0x00007CF4
		public KsTagInfo_CHARA1()
		{
			base.CharNo = 0;
		}

		// Token: 0x060002B6 RID: 694 RVA: 0x00009B04 File Offset: 0x00007D04
		public override KSID GetId()
		{
			return KSID.CHARA1;
		}
	}
}
