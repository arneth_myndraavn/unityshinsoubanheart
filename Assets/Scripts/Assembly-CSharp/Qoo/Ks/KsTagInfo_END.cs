﻿using System;
using Qoo.Game;
using Qoo.Input;

namespace Qoo.Ks
{
	// Token: 0x02000070 RID: 112
	internal class KsTagInfo_END : KsTagInfo
	{
		// Token: 0x0600032E RID: 814 RVA: 0x0000AB08 File Offset: 0x00008D08
		public override KSID GetId()
		{
			return KSID.END;
		}

		// Token: 0x0600032F RID: 815 RVA: 0x0000AB0C File Offset: 0x00008D0C
		public override bool IsRevContinue()
		{
			return false;
		}

		// Token: 0x06000330 RID: 816 RVA: 0x0000AB10 File Offset: 0x00008D10
		public override bool IsJumpTag()
		{
			return true;
		}

		// Token: 0x06000331 RID: 817 RVA: 0x0000AB14 File Offset: 0x00008D14
		public override TAG_RESULT Run(EventPlayer player, TagData tag)
		{
			KsInput.Unlock();
			player.Scene.ResetSkipMark();
			GameData.SetGameEnd();
			SaveLoadManager.SaveSystem();
			GameData.IsMoveTitle = true;
			return TAG_RESULT.END;
		}
	}
}
