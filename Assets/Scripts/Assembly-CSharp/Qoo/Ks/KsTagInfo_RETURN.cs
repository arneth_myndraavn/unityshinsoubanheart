﻿using System;

namespace Qoo.Ks
{
	// Token: 0x02000066 RID: 102
	internal class KsTagInfo_RETURN : KsTagInfo
	{
		// Token: 0x06000308 RID: 776 RVA: 0x0000A754 File Offset: 0x00008954
		public override KSID GetId()
		{
			return KSID.RETURN;
		}

		// Token: 0x06000309 RID: 777 RVA: 0x0000A758 File Offset: 0x00008958
		public override bool IsCallRet()
		{
			return true;
		}

		// Token: 0x0600030A RID: 778 RVA: 0x0000A75C File Offset: 0x0000895C
		public override bool IsJumpTag()
		{
			return true;
		}

		// Token: 0x0600030B RID: 779 RVA: 0x0000A760 File Offset: 0x00008960
		public override TAG_RESULT Run(EventPlayer player, TagData tag)
		{
			return (!player.ReturnKs()) ? TAG_RESULT.NEXT : TAG_RESULT.CONTINUE;
		}
	}
}
