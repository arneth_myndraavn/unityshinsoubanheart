﻿using System;

namespace Qoo.Ks
{
	// Token: 0x0200002F RID: 47
	public struct AKB_FILEHEADER
	{
		// Token: 0x0600018D RID: 397 RVA: 0x00006C8C File Offset: 0x00004E8C
		public void Init()
		{
			this.FourCC = 0U;
			this.nNumOfDir = 0U;
			this.nNumOfFile = 0U;
			this.nNumOfLabel = 0U;
			this.nStringOffset = 0U;
			this.nMaxTagNum = 0U;
			this.nMaxMsgTagNum = 0U;
			this.nMaxParamNum = 0U;
			this.nMaxMsgNum = 0U;
			this.nMaxTextSize = 0U;
			this.nMaxLabelNum = 0U;
		}

		// Token: 0x04000138 RID: 312
		public uint FourCC;

		// Token: 0x04000139 RID: 313
		public uint nNumOfDir;

		// Token: 0x0400013A RID: 314
		public uint nNumOfFile;

		// Token: 0x0400013B RID: 315
		public uint nNumOfLabel;

		// Token: 0x0400013C RID: 316
		public uint nStringOffset;

		// Token: 0x0400013D RID: 317
		public uint nMaxTagNum;

		// Token: 0x0400013E RID: 318
		public uint nMaxMsgTagNum;

		// Token: 0x0400013F RID: 319
		public uint nMaxParamNum;

		// Token: 0x04000140 RID: 320
		public uint nMaxMsgNum;

		// Token: 0x04000141 RID: 321
		public uint nMaxTextSize;

		// Token: 0x04000142 RID: 322
		public uint nMaxLabelNum;
	}
}
