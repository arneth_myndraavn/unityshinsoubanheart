﻿using System;
using Qoo.Memory;

namespace Qoo.Ks
{
	// Token: 0x02000082 RID: 130
	public class EVENTSCENE_OBJ
	{
		// Token: 0x060003AA RID: 938 RVA: 0x0000CD78 File Offset: 0x0000AF78
		public void Reset()
		{
			this.Sw = SWITCH.NULL;
		}

		// Token: 0x060003AB RID: 939 RVA: 0x0000CD84 File Offset: 0x0000AF84
		public void Init()
		{
			this.Name = string.Empty;
			this.Sw = SWITCH.NULL;
			this.nParam = 0;
		}

		// Token: 0x060003AC RID: 940 RVA: 0x0000CDA0 File Offset: 0x0000AFA0
		public void Copy(EVENTSCENE_OBJ other)
		{
			this.Name = other.Name;
			this.Sw = other.Sw;
			this.nParam = other.nParam;
		}

		// Token: 0x060003AD RID: 941 RVA: 0x0000CDD4 File Offset: 0x0000AFD4
		public void Save(MemFile conv)
		{
			conv.SetStringUtf16(this.Name);
		}

		// Token: 0x060003AE RID: 942 RVA: 0x0000CDE4 File Offset: 0x0000AFE4
		public void Load(MemFile conv)
		{
			this.Name = conv.GetStringUtf16();
		}

		// Token: 0x060003AF RID: 943 RVA: 0x0000CDF4 File Offset: 0x0000AFF4
		public void SetLog(SWITCH sw_, string name_, int nParam_ = 0)
		{
			this.Sw = sw_;
			this.Name = name_;
			this.nParam = nParam_;
		}

		// Token: 0x04000289 RID: 649
		public string Name = string.Empty;

		// Token: 0x0400028A RID: 650
		public SWITCH Sw = SWITCH.ON;

		// Token: 0x0400028B RID: 651
		public int nParam;
	}
}
