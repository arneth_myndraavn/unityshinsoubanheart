﻿using System;
using System.Collections.Generic;

namespace Qoo.Ks
{
	// Token: 0x0200008D RID: 141
	public class TagBase
	{
		// Token: 0x060003F9 RID: 1017 RVA: 0x0000E71C File Offset: 0x0000C91C
		public TagBase(KSID id, bool bClose = false)
		{
			this.ID = id;
			this.IsClose = bClose;
			this.Param = new List<ParamData>();
		}

		// Token: 0x17000093 RID: 147
		// (get) Token: 0x060003FA RID: 1018 RVA: 0x0000E748 File Offset: 0x0000C948
		// (set) Token: 0x060003FB RID: 1019 RVA: 0x0000E750 File Offset: 0x0000C950
		public KSID ID { get; protected set; }

		// Token: 0x17000094 RID: 148
		// (get) Token: 0x060003FC RID: 1020 RVA: 0x0000E75C File Offset: 0x0000C95C
		// (set) Token: 0x060003FD RID: 1021 RVA: 0x0000E764 File Offset: 0x0000C964
		public List<ParamData> Param { get; protected set; }

		// Token: 0x17000095 RID: 149
		// (get) Token: 0x060003FE RID: 1022 RVA: 0x0000E770 File Offset: 0x0000C970
		// (set) Token: 0x060003FF RID: 1023 RVA: 0x0000E778 File Offset: 0x0000C978
		public bool IsClose { get; protected set; }

		// Token: 0x06000400 RID: 1024 RVA: 0x0000E784 File Offset: 0x0000C984
		public ParamData GetParam(KSID id)
		{
			return this.Param.Find((ParamData item) => item.ID == id);
		}

		// Token: 0x06000401 RID: 1025 RVA: 0x0000E7B8 File Offset: 0x0000C9B8
		public int GetParamInt(KSID id, int default_value)
		{
			ParamData param = this.GetParam(id);
			return (param != null) ? param.ToInt() : default_value;
		}

		// Token: 0x06000402 RID: 1026 RVA: 0x0000E7E0 File Offset: 0x0000C9E0
		public string GetParamString(KSID id, string default_value)
		{
			ParamData param = this.GetParam(id);
			return (param != null) ? param.Param : default_value;
		}

		// Token: 0x06000403 RID: 1027 RVA: 0x0000E808 File Offset: 0x0000CA08
		public bool GetParamBool(KSID id, bool default_value)
		{
			ParamData param = this.GetParam(id);
			return (param != null) ? string.Equals(param.Param, "true", StringComparison.OrdinalIgnoreCase) : default_value;
		}
	}
}
