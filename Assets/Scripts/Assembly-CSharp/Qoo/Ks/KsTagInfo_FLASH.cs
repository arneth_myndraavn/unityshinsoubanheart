﻿using System;
using Qoo.Game;
using UnityEngine;

namespace Qoo.Ks
{
	// Token: 0x0200005D RID: 93
	internal class KsTagInfo_FLASH : KsTagInfo
	{
		// Token: 0x060002E4 RID: 740 RVA: 0x0000A000 File Offset: 0x00008200
		public override KSID GetId()
		{
			return KSID.FLASH;
		}

		// Token: 0x060002E5 RID: 741 RVA: 0x0000A004 File Offset: 0x00008204
		public override bool IsEnabelConnect()
		{
			return false;
		}

		// Token: 0x060002E6 RID: 742 RVA: 0x0000A008 File Offset: 0x00008208
		public override TAG_RESULT Run(EventPlayer player, TagData tag)
		{
			if (player.RestoreOn)
			{
				return TAG_RESULT.NEXT_EXIT;
			}
			if (GameData.CheckReadedSkip(player.CurKsNo, player.CurLabelNo, player.CurTagNo))
			{
				return TAG_RESULT.NEXT_EXIT;
			}
			int paramInt = tag.GetParamInt(KSID.TIME, 100);
			byte r = (byte)tag.GetParamInt(KSID.R, 255);
			byte g = (byte)tag.GetParamInt(KSID.G, 255);
			byte b = (byte)tag.GetParamInt(KSID.B, 255);
			int paramInt2 = tag.GetParamInt(KSID.BEGINLAYER, 1);
			int num = tag.GetParamInt(KSID.ENDLAYER, 11);
			if (num > 12)
			{
				num = 12;
			}
			bool paramBool = tag.GetParamBool(KSID.BASE, true);
			int num2 = player.Scene.GetCgLayerZ(paramInt2) - 1;
			int num3;
			if (num >= 12)
			{
				num3 = player.Scene.GetCgLayerZ(12) + 1;
			}
			else if (num >= 11)
			{
				num3 = 799;
			}
			else
			{
				num3 = player.Scene.GetCgLayerZ(num + 1) - 1;
			}
			for (int i = 0; i < player.Scene.m_Scene.Cg.Length; i++)
			{
				int cgLayerZ = player.Scene.GetCgLayerZ(player.Scene.m_Scene.Cg[i].nLayer);
				if (cgLayerZ >= num2 && cgLayerZ <= num3)
				{
					player.Scene.CgList[i].Effect |= 2;
				}
			}
			for (int j = 0; j < player.Scene.m_Scene.Chr.Length; j++)
			{
				int num4 = player.Scene.ChrPosIndexToZ(player.Scene.m_Scene.Chr[j].nParam);
				if (num4 >= num2 && num4 <= num3)
				{
					player.Scene.GrpList[1 + j].Effect |= 2;
				}
			}
			if (paramBool)
			{
				player.Scene.GrpList[0].Effect |= 2;
			}
			UnityTask.SetSubTask(Singleton<UnityGraph>.Instance.Flash((float)paramInt / 1000f, new Color32(r, g, b, 0)));
			return TAG_RESULT.NEXT_EXIT;
		}
	}
}
