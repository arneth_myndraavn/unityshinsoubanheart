﻿using System;
using Qoo.Game;
using Qoo.Input;

namespace Qoo.Ks
{
	// Token: 0x02000072 RID: 114
	internal class KsTagInfo_EXEC : KsTagInfo
	{
		// Token: 0x06000339 RID: 825 RVA: 0x0000AB68 File Offset: 0x00008D68
		public override KSID GetId()
		{
			return KSID.EXEC;
		}

		// Token: 0x0600033A RID: 826 RVA: 0x0000AB6C File Offset: 0x00008D6C
		public override bool IsApplyLog()
		{
			return false;
		}

		// Token: 0x0600033B RID: 827 RVA: 0x0000AB70 File Offset: 0x00008D70
		public override bool IsRevContinue()
		{
			return false;
		}

		// Token: 0x0600033C RID: 828 RVA: 0x0000AB74 File Offset: 0x00008D74
		public override bool IsUserSnapShot()
		{
			return true;
		}

		// Token: 0x0600033D RID: 829 RVA: 0x0000AB78 File Offset: 0x00008D78
		public override bool IsJumpBreak(bool bFront, bool bMemoryMode)
		{
			return true;
		}

		// Token: 0x0600033E RID: 830 RVA: 0x0000AB7C File Offset: 0x00008D7C
		public override TAG_RESULT Run(EventPlayer player, TagData tag)
		{
			player.SnapShot(false);
			if (player.RestoreOn)
			{
				return TAG_RESULT.NEXT_EXIT;
			}
			if (!SysData.IsSkipContinue())
			{
				KsInput.Unlock();
			}
			player.Scene.ResetSkipMark();
			string paramString = tag.GetParamString(KSID.NAME, string.Empty);
			string paramString2 = tag.GetParamString(KSID.PARAM, string.Empty);
			string paramString3 = tag.GetParamString(KSID.VALUE, string.Empty);
			if (!KsExec.KsExecProc(paramString, paramString2, paramString3, player.TagCallCnt))
			{
				return TAG_RESULT.CONTINUE_EXIT;
			}
			return TAG_RESULT.NEXT_EXIT;
		}
	}
}
