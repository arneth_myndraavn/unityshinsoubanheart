﻿using System;

namespace Qoo.Ks
{
	// Token: 0x0200007E RID: 126
	public enum DRAW_COMMAND
	{
		// Token: 0x04000275 RID: 629
		DRAW,
		// Token: 0x04000276 RID: 630
		DRAW_NOTEX,
		// Token: 0x04000277 RID: 631
		ERASE,
		// Token: 0x04000278 RID: 632
		SETTEX
	}
}
