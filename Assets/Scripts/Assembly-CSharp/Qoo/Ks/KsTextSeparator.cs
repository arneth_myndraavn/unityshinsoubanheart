﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Qoo.Ks
{
	// Token: 0x0200008B RID: 139
	public class KsTextSeparator
	{
		// Token: 0x17000090 RID: 144
		// (get) Token: 0x060003E9 RID: 1001 RVA: 0x0000E258 File Offset: 0x0000C458
		public string[] Dest
		{
			get
			{
				return this.m_dest.ToArray();
			}
		}

		// Token: 0x060003EA RID: 1002 RVA: 0x0000E268 File Offset: 0x0000C468
		public bool Analysis(string src)
		{
			this.m_src = src;
			this.m_isDQMode = false;
			this.m_isComMode = false;
			this.m_isEscMode = false;
			this.m_isEnd = false;
			for (int num = 0; num != src.Length; num++)
			{
				if (!this.AddChar(src[num]))
				{
					Debug.Print("Error:文字列解析を終了します");
					return false;
				}
				if (this.m_isEnd)
				{
					this.New();
					this.m_src = string.Empty;
					return true;
				}
			}
			this.New();
			this.m_src = string.Empty;
			return true;
		}

		// Token: 0x060003EB RID: 1003 RVA: 0x0000E300 File Offset: 0x0000C500
		private bool AddChar(char ch)
		{
			if (this.m_isEscMode)
			{
				this.Append(ch);
				this.m_isEscMode = false;
				return true;
			}
			if (this.m_isDQMode)
			{
				return this.AddCharDQ(ch);
			}
			if (this.m_isComMode)
			{
				return this.AddCharCom(ch);
			}
			return this.AddCharOther(ch);
		}

		// Token: 0x060003EC RID: 1004 RVA: 0x0000E358 File Offset: 0x0000C558
		private bool AddCharDQ(char ch)
		{
			if (ch != '\\')
			{
				if (ch == ']')
				{
					Debug.Print("Error:ダブルクォーテーションの終わりが来るまでにコマンドが、']'で閉じられました:" + this.m_src);
					return false;
				}
				if (ch == '\r')
				{
					Debug.Print("Error:ダブルクォーテーションの終わりが来るまでに改行がきました:" + this.m_src);
					return false;
				}
				if (ch != '"')
				{
					if (ch == ';')
					{
						Debug.Print("Error:ダブルクォーテーションの終わりが来るまでにコマンドが、;でコメントが始まりました:" + this.m_src);
						return false;
					}
					this.Append(ch);
				}
				else
				{
					this.m_isDQMode = false;
					this.New();
				}
			}
			else
			{
				this.m_isEscMode = true;
			}
			return true;
		}

		// Token: 0x060003ED RID: 1005 RVA: 0x0000E408 File Offset: 0x0000C608
		private bool AddCharCom(char ch)
		{
			switch (ch)
			{
			case ' ':
				this.New();
				break;
			default:
				switch (ch)
				{
				case ';':
					Debug.Print("Error:コマンドの終わりが来るまでにコマンドが、;でコメントが始まりました:" + this.m_src);
					return false;
				default:
					switch (ch)
					{
					case '[':
						Debug.Print("Error:コマンドの終わりが来るまでに'['でコマンドが始まろうとしています:" + this.m_src);
						this.m_isComMode = true;
						break;
					case '\\':
						this.m_isEscMode = true;
						break;
					case ']':
						this.m_isComMode = false;
						this.New();
						this.Append(ch);
						this.New();
						break;
					default:
						if (ch != '\t')
						{
							if (ch == '\r')
							{
								Debug.Print("Error:コマンドの終わりが来るまでに改行がきました:" + this.m_src);
								return false;
							}
							if (ch != '\u3000')
							{
								this.Append(ch);
							}
							else
							{
								this.New();
							}
						}
						else
						{
							this.New();
						}
						break;
					}
					break;
				case '=':
					this.New();
					this.Append(ch);
					this.New();
					break;
				}
				break;
			case '"':
				this.m_isDQMode = true;
				this.New();
				break;
			}
			return true;
		}

		// Token: 0x060003EE RID: 1006 RVA: 0x0000E554 File Offset: 0x0000C754
		private bool AddCharOther(char ch)
		{
			if (ch != '[')
			{
				if (ch != '\\')
				{
					if (ch != '\t')
					{
						if (ch != '\r')
						{
							if (ch != ' ')
							{
								if (ch != ';')
								{
									if (ch != '\u3000')
									{
										this.Append(ch);
									}
									else if (this.NowLength() > 0)
									{
										this.Append(ch);
									}
								}
								else
								{
									this.m_isEnd = true;
									this.New();
									this.Append(ch);
								}
							}
							else if (this.NowLength() > 0)
							{
								this.Append(ch);
							}
						}
						else
						{
							this.m_isEnd = true;
							this.New();
							this.Append(ch);
						}
					}
				}
				else
				{
					this.m_isEscMode = true;
				}
			}
			else
			{
				this.m_isComMode = true;
				this.New();
				this.Append(ch);
				this.New();
			}
			return true;
		}

		// Token: 0x060003EF RID: 1007 RVA: 0x0000E64C File Offset: 0x0000C84C
		private bool Append(char ch)
		{
			this.m_current.Append(ch);
			return true;
		}

		// Token: 0x060003F0 RID: 1008 RVA: 0x0000E65C File Offset: 0x0000C85C
		private bool New()
		{
			if (this.m_current.Length > 0)
			{
				this.m_dest.Add(this.m_current.ToString());
			}
			this.m_current.Remove(0, this.m_current.Length);
			return true;
		}

		// Token: 0x060003F1 RID: 1009 RVA: 0x0000E6AC File Offset: 0x0000C8AC
		private int NowLength()
		{
			return this.m_current.Length;
		}

		// Token: 0x040002B4 RID: 692
		private string m_src;

		// Token: 0x040002B5 RID: 693
		private StringBuilder m_current = new StringBuilder();

		// Token: 0x040002B6 RID: 694
		private List<string> m_dest = new List<string>();

		// Token: 0x040002B7 RID: 695
		private bool m_isDQMode;

		// Token: 0x040002B8 RID: 696
		private bool m_isComMode;

		// Token: 0x040002B9 RID: 697
		private bool m_isEscMode;

		// Token: 0x040002BA RID: 698
		private bool m_isEnd;
	}
}
