﻿using System;

namespace Qoo.Ks
{
	// Token: 0x02000047 RID: 71
	public abstract class KsTagInfo
	{
		// Token: 0x0600028B RID: 651 RVA: 0x00009550 File Offset: 0x00007750
		public KsTagInfo()
		{
		}

		// Token: 0x0600028C RID: 652 RVA: 0x00009558 File Offset: 0x00007758
		public virtual KSID GetId()
		{
			return KSID.UNKNOWN;
		}

		// Token: 0x0600028D RID: 653 RVA: 0x0000955C File Offset: 0x0000775C
		public virtual bool IsMessage()
		{
			return false;
		}

		// Token: 0x0600028E RID: 654 RVA: 0x00009560 File Offset: 0x00007760
		public virtual bool IsJumpBreak(bool bFront = true, bool bMemoryMode = false)
		{
			return false;
		}

		// Token: 0x0600028F RID: 655 RVA: 0x00009564 File Offset: 0x00007764
		public virtual bool IsJumpTag()
		{
			return false;
		}

		// Token: 0x06000290 RID: 656 RVA: 0x00009568 File Offset: 0x00007768
		public virtual bool IsCallRet()
		{
			return false;
		}

		// Token: 0x06000291 RID: 657 RVA: 0x0000956C File Offset: 0x0000776C
		public virtual bool IsRevContinue()
		{
			return true;
		}

		// Token: 0x06000292 RID: 658 RVA: 0x00009570 File Offset: 0x00007770
		public virtual bool IsSceneApply()
		{
			return false;
		}

		// Token: 0x06000293 RID: 659 RVA: 0x00009574 File Offset: 0x00007774
		public virtual bool IsRevSceneApply()
		{
			return false;
		}

		// Token: 0x06000294 RID: 660 RVA: 0x00009578 File Offset: 0x00007778
		public virtual bool IsApplyLog()
		{
			return true;
		}

		// Token: 0x06000295 RID: 661 RVA: 0x0000957C File Offset: 0x0000777C
		public virtual bool IsJumpEffectResetTag()
		{
			return false;
		}

		// Token: 0x06000296 RID: 662 RVA: 0x00009580 File Offset: 0x00007780
		public virtual bool IsUserSnapShot()
		{
			return false;
		}

		// Token: 0x06000297 RID: 663 RVA: 0x00009584 File Offset: 0x00007784
		public virtual bool IsEnabelConnect()
		{
			return true;
		}

		// Token: 0x06000298 RID: 664 RVA: 0x00009588 File Offset: 0x00007788
		protected void CheckEffectParam(EventPlayer player, TagData tag)
		{
			int paramInt = tag.GetParamInt(KSID.TIME, 500);
			string paramString = tag.GetParamString(KSID.EFFECT, string.Empty);
			player.Scene.EntryEffect(true, paramString, paramInt);
		}

		// Token: 0x06000299 RID: 665 RVA: 0x000095C0 File Offset: 0x000077C0
		public virtual TAG_RESULT Run(EventPlayer player, TagData tag)
		{
			Debug.Assert(false, string.Format("タグの実装がありません:class={0}\n", this.GetId().ToString()));
			return TAG_RESULT.CONTINUE_EXIT;
		}

		// Token: 0x0600029A RID: 666 RVA: 0x000095E4 File Offset: 0x000077E4
		public virtual bool RunMessage(MsgTagData tag)
		{
			Debug.Assert(false, string.Format("メッセージ内タグの実装がされていません。:class={0}\n", this.GetId().ToString()));
			return false;
		}
	}
}
