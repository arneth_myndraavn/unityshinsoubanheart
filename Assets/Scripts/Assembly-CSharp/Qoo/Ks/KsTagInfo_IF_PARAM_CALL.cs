﻿using System;

namespace Qoo.Ks
{
	// Token: 0x02000068 RID: 104
	internal class KsTagInfo_IF_PARAM_CALL : KsTagInfo_IF_PARAM_NEXT
	{
		// Token: 0x06000311 RID: 785 RVA: 0x0000A8A8 File Offset: 0x00008AA8
		public override KSID GetId()
		{
			return KSID.IF_PARAM_CALL;
		}

		// Token: 0x06000312 RID: 786 RVA: 0x0000A8AC File Offset: 0x00008AAC
		public override bool IsCallRet()
		{
			return true;
		}
	}
}
