﻿using System;
using Qoo.Memory;

namespace Qoo.Ks
{
	// Token: 0x02000086 RID: 134
	public class EVENTSCENE_SELCET
	{
		// Token: 0x060003C7 RID: 967 RVA: 0x0000D1CC File Offset: 0x0000B3CC
		public EVENTSCENE_SELCET()
		{
			this.Init();
		}

		// Token: 0x060003C8 RID: 968 RVA: 0x0000D214 File Offset: 0x0000B414
		public void Reset()
		{
			this.Sw = SWITCH.NULL;
		}

		// Token: 0x060003C9 RID: 969 RVA: 0x0000D220 File Offset: 0x0000B420
		public void Init()
		{
			this.Name = (this.KsName = (this.LabelName = (this.Effect = string.Empty)));
			this.Sw = SWITCH.NULL;
			this.nParam = 0;
		}

		// Token: 0x060003CA RID: 970 RVA: 0x0000D264 File Offset: 0x0000B464
		public void Copy(EVENTSCENE_SELCET other)
		{
			this.Name = other.Name;
			this.KsName = other.KsName;
			this.LabelName = other.LabelName;
			this.Effect = other.Effect;
			this.Sw = other.Sw;
			this.nParam = other.nParam;
		}

		// Token: 0x060003CB RID: 971 RVA: 0x0000D2BC File Offset: 0x0000B4BC
		public void Save(MemFile conv)
		{
			conv.SetStringUtf16(this.Name);
			conv.SetStringUtf16(this.KsName);
			conv.SetStringUtf16(this.LabelName);
			conv.SetInt32(this.nParam);
		}

		// Token: 0x060003CC RID: 972 RVA: 0x0000D2FC File Offset: 0x0000B4FC
		public void Load(MemFile conv)
		{
			this.Name = conv.GetStringUtf16();
			this.KsName = conv.GetStringUtf16();
			this.LabelName = conv.GetStringUtf16();
			this.nParam = conv.GetInt32();
		}

		// Token: 0x060003CD RID: 973 RVA: 0x0000D33C File Offset: 0x0000B53C
		public void SetLog(SWITCH sw_, string strName, string strKs, string strLabel, string strEffect, int nParam_)
		{
			this.Sw = sw_;
			this.Name = strName;
			this.KsName = strKs;
			this.LabelName = strLabel;
			this.Effect = strEffect;
			this.nParam = nParam_;
			Debug.Print(string.Concat(new string[]
			{
				"SELECT:",
				this.Name,
				" KsName:",
				this.KsName,
				" LabelName:",
				this.LabelName
			}));
		}

		// Token: 0x060003CE RID: 974 RVA: 0x0000D3BC File Offset: 0x0000B5BC
		public void Set(string strName, string strKs, string strLabel, string strEffect, int nType)
		{
			this.Name = strName;
			this.KsName = strKs;
			this.LabelName = strLabel;
			this.Effect = strEffect;
			this.nParam = nType;
		}

		// Token: 0x0400029D RID: 669
		public string Name = string.Empty;

		// Token: 0x0400029E RID: 670
		public string KsName = string.Empty;

		// Token: 0x0400029F RID: 671
		public string LabelName = string.Empty;

		// Token: 0x040002A0 RID: 672
		public string Effect = string.Empty;

		// Token: 0x040002A1 RID: 673
		public SWITCH Sw;

		// Token: 0x040002A2 RID: 674
		public int nParam;
	}
}
