﻿using System;
using System.Collections.Generic;

namespace Qoo.Ks
{
	// Token: 0x02000045 RID: 69
	public class KsPlayer
	{
		// Token: 0x1700007C RID: 124
		// (get) Token: 0x06000263 RID: 611 RVA: 0x00008EC8 File Offset: 0x000070C8
		// (set) Token: 0x06000264 RID: 612 RVA: 0x00008ED0 File Offset: 0x000070D0
		public PLAYSTAT Status { get; private set; }

		// Token: 0x1700007D RID: 125
		// (get) Token: 0x06000265 RID: 613 RVA: 0x00008EDC File Offset: 0x000070DC
		// (set) Token: 0x06000266 RID: 614 RVA: 0x00008EE4 File Offset: 0x000070E4
		public TagData CurTag { get; private set; }

		// Token: 0x1700007E RID: 126
		// (get) Token: 0x06000267 RID: 615 RVA: 0x00008EF0 File Offset: 0x000070F0
		// (set) Token: 0x06000268 RID: 616 RVA: 0x00008EF8 File Offset: 0x000070F8
		public int CurTagNo { get; private set; }

		// Token: 0x1700007F RID: 127
		// (get) Token: 0x06000269 RID: 617 RVA: 0x00008F04 File Offset: 0x00007104
		// (set) Token: 0x0600026A RID: 618 RVA: 0x00008F0C File Offset: 0x0000710C
		public int NextTagNo { get; private set; }

		// Token: 0x17000080 RID: 128
		// (get) Token: 0x0600026B RID: 619 RVA: 0x00008F18 File Offset: 0x00007118
		// (set) Token: 0x0600026C RID: 620 RVA: 0x00008F20 File Offset: 0x00007120
		public int LastTagNo { get; private set; }

		// Token: 0x17000081 RID: 129
		// (get) Token: 0x0600026D RID: 621 RVA: 0x00008F2C File Offset: 0x0000712C
		// (set) Token: 0x0600026E RID: 622 RVA: 0x00008F34 File Offset: 0x00007134
		public int MaxTagNo { get; private set; }

		// Token: 0x17000082 RID: 130
		// (get) Token: 0x0600026F RID: 623 RVA: 0x00008F40 File Offset: 0x00007140
		// (set) Token: 0x06000270 RID: 624 RVA: 0x00008F48 File Offset: 0x00007148
		public int CurLabelNo { get; private set; }

		// Token: 0x17000083 RID: 131
		// (get) Token: 0x06000271 RID: 625 RVA: 0x00008F54 File Offset: 0x00007154
		// (set) Token: 0x06000272 RID: 626 RVA: 0x00008F5C File Offset: 0x0000715C
		public int NextLabelNo { get; private set; }

		// Token: 0x17000084 RID: 132
		// (get) Token: 0x06000273 RID: 627 RVA: 0x00008F68 File Offset: 0x00007168
		// (set) Token: 0x06000274 RID: 628 RVA: 0x00008F70 File Offset: 0x00007170
		public int TagCallCnt { get; private set; }

		// Token: 0x06000275 RID: 629 RVA: 0x00008F7C File Offset: 0x0000717C
		protected virtual TAG_RESULT ProcTag(TagData tag)
		{
			return TAG_RESULT.CONTINUE_EXIT;
		}

		// Token: 0x06000276 RID: 630 RVA: 0x00008F80 File Offset: 0x00007180
		public bool Init()
		{
			this.m_Reader = null;
			this.Status = PLAYSTAT.NULL;
			this.CurTag = null;
			this.CurTagNo = -1;
			this.CurLabelNo = -1;
			this.MaxTagNo = -1;
			this.LastTagNo = -1;
			this.NextTagNo = 0;
			this.NextLabelNo = 0;
			this.TagCallCnt = 0;
			this.m_nExecWait = 0;
			return true;
		}

		// Token: 0x06000277 RID: 631 RVA: 0x00008FDC File Offset: 0x000071DC
		public virtual bool SetKsData(TagReader reader_)
		{
			this.m_Reader = reader_;
			foreach (string item in this.m_Reader.NextKsList)
			{
				this.m_NextKsList.Add(item);
			}
			this.CurTag = null;
			this.CurTagNo = -1;
			this.CurLabelNo = -1;
			this.MaxTagNo = -1;
			this.LastTagNo = -1;
			this.NextTagNo = 0;
			this.NextLabelNo = 0;
			return true;
		}

		// Token: 0x06000278 RID: 632 RVA: 0x00009088 File Offset: 0x00007288
		public virtual bool Exec()
		{
			if (this.m_nExecWait > 0)
			{
				this.m_nExecWait--;
				return true;
			}
			while (this.Status == PLAYSTAT.PLAY)
			{
				if (DispAssert.IsAssert())
				{
					this.PlayStop();
				}
				TAG_RESULT nResult = TAG_RESULT.CONTINUE_EXIT;
				if (this.NextTagNo >= 0)
				{
					this.PeekTag();
				}
				if (this.CurTag != null)
				{
					nResult = this.ProcTag(this.CurTag);
					this.TagCallCnt++;
				}
				if (this.RunResult(nResult))
				{
					break;
				}
			}
			return true;
		}

		// Token: 0x06000279 RID: 633 RVA: 0x00009124 File Offset: 0x00007324
		protected bool RunResult(TAG_RESULT nResult)
		{
			switch (nResult)
			{
			case TAG_RESULT.CONTINUE:
				return false;
			case TAG_RESULT.NEXT:
				this.NextForward();
				return false;
			case TAG_RESULT.NEXT_EXIT:
				this.NextForward();
				break;
			case TAG_RESULT.END:
				this.Status = PLAYSTAT.END;
				break;
			}
			return true;
		}

		// Token: 0x0600027A RID: 634 RVA: 0x0000917C File Offset: 0x0000737C
		public virtual void PlayStart()
		{
			this.Status = PLAYSTAT.PLAY;
		}

		// Token: 0x0600027B RID: 635 RVA: 0x00009188 File Offset: 0x00007388
		public virtual void PlayStop()
		{
			this.Status = PLAYSTAT.STOP;
		}

		// Token: 0x0600027C RID: 636 RVA: 0x00009194 File Offset: 0x00007394
		public virtual bool IsPlayEnd()
		{
			return this.Status == PLAYSTAT.END;
		}

		// Token: 0x0600027D RID: 637 RVA: 0x000091A0 File Offset: 0x000073A0
		public virtual void NextForward()
		{
			this.SetNext(this.CurLabelNo, this.CurTagNo + 1);
		}

		// Token: 0x0600027E RID: 638 RVA: 0x000091C4 File Offset: 0x000073C4
		public virtual void NextBack()
		{
			this.SetNext(this.CurLabelNo, this.CurTagNo - 1);
		}

		// Token: 0x0600027F RID: 639 RVA: 0x000091E8 File Offset: 0x000073E8
		public virtual void SetNext(int nLabel, int nNo)
		{
			if (this.m_Reader.Label.Count <= nLabel || nLabel < 0)
			{
				Debug.Assert(false, string.Format("ERROR:OVER LABELNO KS={0}, Label={1} LabelNum={2}", this.m_Reader.Name, nLabel, this.m_Reader.Label.Count));
			}
			if (nNo >= this.m_Reader.Label[nLabel].TagNum)
			{
				if (nLabel + 1 < this.m_Reader.Label.Count)
				{
					for (;;)
					{
						nLabel++;
						if (this.m_Reader.Label[nLabel].TagNum > 0)
						{
							break;
						}
						if (nLabel + 1 >= this.m_Reader.Label.Count)
						{
							goto IL_C8;
						}
					}
					nNo = 0;
					IL_C8:;
				}
				else
				{
					nNo = this.m_Reader.Label[nLabel].TagNum - 1;
				}
			}
			if (nNo < 0 && nLabel - 1 > 0)
			{
				do
				{
					nLabel--;
					nNo = this.m_Reader.Label[nLabel].TagNum - 1;
				}
				while (nLabel - 1 >= 0 && nNo < 0);
				if (nNo < 0)
				{
					nLabel = 0;
					nNo = 0;
				}
			}
			this.NextTagNo = nNo;
			this.NextLabelNo = nLabel;
		}

		// Token: 0x06000280 RID: 640 RVA: 0x00009338 File Offset: 0x00007538
		public void ResetCallCount(int nCnt = 0)
		{
			this.TagCallCnt = nCnt;
		}

		// Token: 0x06000281 RID: 641 RVA: 0x00009344 File Offset: 0x00007544
		public int GetTagNum()
		{
			return this.m_Reader.Tag.Count;
		}

		// Token: 0x06000282 RID: 642 RVA: 0x00009358 File Offset: 0x00007558
		public TagData GetTagData(int nIndex, int nLabelIndex)
		{
			return this.m_Reader.Tag[nIndex + this.m_Reader.Label[nLabelIndex].TagStart];
		}

		// Token: 0x06000283 RID: 643 RVA: 0x00009390 File Offset: 0x00007590
		public string GetCurLabel()
		{
			return this.m_Reader.Label[this.CurLabelNo].Name;
		}

		// Token: 0x06000284 RID: 644 RVA: 0x000093B8 File Offset: 0x000075B8
		public int GetLabelNo(string szLabel)
		{
			for (int i = 0; i < this.m_Reader.Label.Count; i++)
			{
				if (szLabel == this.m_Reader.Label[i].Name)
				{
					return i;
				}
			}
			return 0;
		}

		// Token: 0x06000285 RID: 645 RVA: 0x0000940C File Offset: 0x0000760C
		public string GetLabelName(int iLabelNo)
		{
			return this.m_Reader.Label[iLabelNo].Name;
		}

		// Token: 0x06000286 RID: 646 RVA: 0x00009424 File Offset: 0x00007624
		public void SetExecWait(int nFrameTime)
		{
			this.m_nExecWait = nFrameTime;
		}

		// Token: 0x06000287 RID: 647 RVA: 0x00009430 File Offset: 0x00007630
		public void PeekTag()
		{
			this.LastTagNo = this.CurTagNo;
			this.CurTagNo = this.NextTagNo;
			this.CurLabelNo = this.NextLabelNo;
			this.NextTagNo = -1;
			this.NextLabelNo = -1;
			if (this.CurTagNo > this.MaxTagNo)
			{
				this.MaxTagNo = this.CurTagNo;
			}
			if (this.CurLabelNo < 0)
			{
				this.CurLabelNo = 0;
			}
			if (this.CurTagNo >= this.m_Reader.Label[this.CurLabelNo].TagNum)
			{
				this.Status = PLAYSTAT.END;
				this.CurTag = null;
				return;
			}
			if (this.CurTagNo < 0)
			{
				this.CurTagNo = 0;
			}
			this.CurTag = this.m_Reader.Tag[this.m_Reader.Label[this.CurLabelNo].TagStart + this.CurTagNo];
			this.TagCallCnt = 0;
		}

		// Token: 0x06000288 RID: 648 RVA: 0x00009528 File Offset: 0x00007728
		public List<string> GetNextKsArray()
		{
			return this.m_NextKsList;
		}

		// Token: 0x06000289 RID: 649 RVA: 0x00009530 File Offset: 0x00007730
		internal void ResetNextKs()
		{
			this.m_NextKsList.Clear();
		}

		// Token: 0x0600028A RID: 650 RVA: 0x00009540 File Offset: 0x00007740
		internal bool IsNextKs()
		{
			return this.m_NextKsList.Count > 0;
		}

		// Token: 0x040001E8 RID: 488
		private TagReader m_Reader;

		// Token: 0x040001E9 RID: 489
		private List<string> m_NextKsList = new List<string>();

		// Token: 0x040001EA RID: 490
		private int m_nExecWait;
	}
}
