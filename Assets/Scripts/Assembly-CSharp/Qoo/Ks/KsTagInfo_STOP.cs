﻿using System;
using Qoo.Application;
using Qoo.Game;
using Qoo.Input;

namespace Qoo.Ks
{
	// Token: 0x02000060 RID: 96
	internal class KsTagInfo_STOP : KsTagInfo
	{
		// Token: 0x060002EF RID: 751 RVA: 0x0000A3A4 File Offset: 0x000085A4
		public override KSID GetId()
		{
			return KSID.STOP;
		}

		// Token: 0x060002F0 RID: 752 RVA: 0x0000A3A8 File Offset: 0x000085A8
		public override bool IsRevSceneApply()
		{
			return true;
		}

		// Token: 0x060002F1 RID: 753 RVA: 0x0000A3AC File Offset: 0x000085AC
		public override bool IsRevContinue()
		{
			return false;
		}

		// Token: 0x060002F2 RID: 754 RVA: 0x0000A3B0 File Offset: 0x000085B0
		public override bool IsJumpTag()
		{
			return true;
		}

		// Token: 0x060002F3 RID: 755 RVA: 0x0000A3B4 File Offset: 0x000085B4
		public override bool IsJumpBreak(bool bFront, bool bMemoryMode)
		{
			return true;
		}

		// Token: 0x060002F4 RID: 756 RVA: 0x0000A3B8 File Offset: 0x000085B8
		public override TAG_RESULT Run(EventPlayer player, TagData tag)
		{
			if (player.RestoreOn)
			{
				player.Scene.EntryMessage(false, null, null, 0, false);
				player.Scene.EntryFace(false, string.Empty);
				return TAG_RESULT.CONTINUE;
			}
			KsInput.Pause = false;
			if (player.TagCallCnt == 0)
			{
				if (!SysData.IsSkipContinue())
				{
					KsInput.Unlock();
				}
				KsInput.IsTrig = false;
				UnityApp.AutoSave();
				App.QooSelect.AddSelect(player.Scene.m_Scene.Select);
				bool paramBool = tag.GetParamBool(KSID.READED, true);
				string text = tag.GetParamString(KSID.NAME, "normal");
				text = text.ToLower();
				player.Scene.EntryMessage(false, null, null, 0, false);
				player.Scene.EntryFace(false, string.Empty);
				player.Scene.SceneApply(false, true);
				Singleton<SelectWnd>.Instance.Begin(true, paramBool, text);
			}
			else if (Singleton<SelectWnd>.Instance.IsSelectEnd())
			{
				player.JumpKs(Singleton<SelectWnd>.Instance.GetSelectKs(), Singleton<SelectWnd>.Instance.GetSelectLabel(), this.IsCallRet());
				Singleton<SelectWnd>.Instance.End();
				player.Scene.EntrySelect(-1, string.Empty, string.Empty, string.Empty, 0, string.Empty);
				player.Scene.EntryEffect(true, string.Empty, 200);
				return TAG_RESULT.CONTINUE;
			}
			return TAG_RESULT.CONTINUE_EXIT;
		}
	}
}
