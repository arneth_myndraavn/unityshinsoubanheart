﻿using System;
using Qoo.Application;
using Qoo.Memory;

namespace Qoo.Game
{
	// Token: 0x02000023 RID: 35
	internal class SaveData
	{
		// Token: 0x1700001F RID: 31
		// (get) Token: 0x060000F6 RID: 246 RVA: 0x0000582C File Offset: 0x00003A2C
		public static bool IsSave
		{
			get
			{
				return App.QooKsLog.GetLogNum() > 0;
			}
		}

		// Token: 0x17000020 RID: 32
		// (get) Token: 0x060000F7 RID: 247 RVA: 0x0000583C File Offset: 0x00003A3C
		public byte[] Data
		{
			get
			{
				return this.m_Data.Data.ToArray();
			}
		}

		// Token: 0x17000021 RID: 33
		// (get) Token: 0x060000F8 RID: 248 RVA: 0x00005850 File Offset: 0x00003A50
		public GAME_SAVE_DATA LoadData
		{
			get
			{
				return this.m_GameData;
			}
		}

		// Token: 0x060000F9 RID: 249 RVA: 0x00005858 File Offset: 0x00003A58
		public byte[] Save()
		{
			if (!SaveData.IsSave)
			{
				return null;
			}
			this.m_Data.Clear();
			this.m_GameData = new GAME_SAVE_DATA();
			this.m_GameData.Ready();
			if (this.m_GameData.Save(this.m_Data))
			{
				return this.m_Data.Data.ToArray();
			}
			return null;
		}

		// Token: 0x060000FA RID: 250 RVA: 0x000058BC File Offset: 0x00003ABC
		public bool Load(byte[] data_)
		{
			this.m_Data = new MemFile(data_);
			this.m_GameData = new GAME_SAVE_DATA();
			return this.m_GameData.Load(this.m_Data);
		}

		// Token: 0x0400010E RID: 270
		private MemFile m_Data = new MemFile(null);

		// Token: 0x0400010F RID: 271
		private GAME_SAVE_DATA m_GameData;
	}
}
