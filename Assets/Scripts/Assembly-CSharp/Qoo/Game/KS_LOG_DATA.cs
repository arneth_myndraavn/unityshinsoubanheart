﻿using System;
using System.Collections.Generic;
using System.Linq;
using Qoo.Application;
using Qoo.Ks;
using Qoo.Memory;

namespace Qoo.Game
{
	// Token: 0x02000022 RID: 34
	public class KS_LOG_DATA
	{
		// Token: 0x060000F2 RID: 242 RVA: 0x00005534 File Offset: 0x00003734
		public bool Ready()
		{
			int logNum = App.QooKsLog.GetLogNum();
			this.m_LogData = new SCENEPOSDATA[logNum];
			int callNum = App.QooKsLog.GetCallNum();
			this.m_CallData = new SCENECALLDATA[callNum];
			for (int num = 0; num != this.m_LogData.Length; num++)
			{
				this.m_LogData[num] = App.QooKsLog.GetPos(num);
			}
			for (int num2 = 0; num2 != this.m_CallData.Length; num2++)
			{
				this.m_CallData[num2] = App.QooKsLog.GetCallData(num2);
			}
			List<int> list = new List<int>();
			foreach (SCENEPOSDATA sceneposdata in this.m_LogData)
			{
				list.Add((int)sceneposdata.sFileNo);
			}
			int[] array = list.Distinct<int>().ToArray<int>();
			this.m_KsNameArray = new string[array.Length];
			for (int num3 = 0; num3 != array.Length; num3++)
			{
				this.m_KsNameArray[num3] = Akb.GetFileInfo(array[num3]).Name;
			}
			return true;
		}

		// Token: 0x060000F3 RID: 243 RVA: 0x00005660 File Offset: 0x00003860
		public bool Save(MemFile mem)
		{
			mem.SetInt32(this.m_LogData.Length);
			mem.SetInt32(this.m_CallData.Length);
			mem.SetInt32(this.m_KsNameArray.Length);
			foreach (SCENEPOSDATA sceneposdata in this.m_LogData)
			{
				sceneposdata.Save(mem);
			}
			foreach (SCENECALLDATA scenecalldata in this.m_CallData)
			{
				scenecalldata.Save(mem);
			}
			foreach (string stringUtf in this.m_KsNameArray)
			{
				mem.SetStringUtf16(stringUtf);
			}
			return true;
		}

		// Token: 0x060000F4 RID: 244 RVA: 0x00005728 File Offset: 0x00003928
		public bool Load(MemFile mem)
		{
			int @int = mem.GetInt32();
			int int2 = mem.GetInt32();
			int int3 = mem.GetInt32();
			this.m_LogData = new SCENEPOSDATA[@int];
			this.m_CallData = new SCENECALLDATA[int2];
			this.m_KsNameArray = new string[int3];
			for (int num = 0; num != this.m_LogData.Length; num++)
			{
				this.m_LogData[num] = new SCENEPOSDATA();
				this.m_LogData[num].Load(mem);
			}
			for (int num2 = 0; num2 != this.m_CallData.Length; num2++)
			{
				this.m_CallData[num2] = default(SCENECALLDATA);
				this.m_CallData[num2].Load(mem);
			}
			for (int num3 = 0; num3 != int3; num3++)
			{
				this.m_KsNameArray[num3] = mem.GetStringUtf16();
			}
			return true;
		}

		// Token: 0x0400010B RID: 267
		public SCENEPOSDATA[] m_LogData;

		// Token: 0x0400010C RID: 268
		public SCENECALLDATA[] m_CallData;

		// Token: 0x0400010D RID: 269
		public string[] m_KsNameArray;
	}
}
