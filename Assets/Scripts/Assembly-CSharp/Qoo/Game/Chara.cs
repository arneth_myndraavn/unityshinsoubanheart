﻿using System;
using System.Collections.Generic;
using Game;

namespace Qoo.Game
{
	// Token: 0x0200001A RID: 26
	public static class Chara
	{
		// Token: 0x0600006F RID: 111 RVA: 0x00003AD4 File Offset: 0x00001CD4
		public static void Clear()
		{
			Chara.m_CharTbl.Clear();
		}

		// Token: 0x06000070 RID: 112 RVA: 0x00003AE0 File Offset: 0x00001CE0
		public static void Add(CHAR_ID id, GAMECHAR_TABLE tbl)
		{
			Chara.m_CharTbl.Add(id, tbl);
			Debug.Print(string.Format("INFO:キャラ番号を追加しました ID={0} Name={1}", id.ToString(), tbl.szNameJFull));
		}

		// Token: 0x06000071 RID: 113 RVA: 0x00003B10 File Offset: 0x00001D10
		public static string[] GetCharNameArray()
		{
			List<string> list = new List<string>();
			foreach (KeyValuePair<CHAR_ID, GAMECHAR_TABLE> keyValuePair in Chara.m_CharTbl)
			{
				list.Add(keyValuePair.Value.szName);
			}
			return list.ToArray();
		}

		// Token: 0x06000072 RID: 114 RVA: 0x00003B90 File Offset: 0x00001D90
		public static string GetFirstName()
		{
			return "アリス";
		}

		// Token: 0x06000073 RID: 115 RVA: 0x00003B98 File Offset: 0x00001D98
		public static string GetFamilyName()
		{
			return "リデル";
		}

		// Token: 0x06000074 RID: 116 RVA: 0x00003BA0 File Offset: 0x00001DA0
		public static string GetNameFromId(CHAR_ID nCharId)
		{
			Debug.Assert(nCharId < CHAR_ID.NUM, string.Format("範囲外のキャラIDです{0}\n", nCharId));
			return Chara.m_CharTbl[nCharId].szName;
		}

		// Token: 0x06000075 RID: 117 RVA: 0x00003BDC File Offset: 0x00001DDC
		public static CHAR_ID GetIdFromName(string szName)
		{
			string a = szName.ToLower();
			foreach (KeyValuePair<CHAR_ID, GAMECHAR_TABLE> keyValuePair in Chara.m_CharTbl)
			{
				if (string.Equals(a, keyValuePair.Value.szName))
				{
					return keyValuePair.Key;
				}
			}
			return CHAR_ID.NOTHING;
		}

		// Token: 0x06000076 RID: 118 RVA: 0x00003C70 File Offset: 0x00001E70
		public static CHAR_ID GetIdFromVoice(string szVoiceName)
		{
			string text = szVoiceName.ToLower();
			foreach (KeyValuePair<CHAR_ID, GAMECHAR_TABLE> keyValuePair in Chara.m_CharTbl)
			{
				if (keyValuePair.Value.szVoice != null && keyValuePair.Value.szVoice.Length > 0 && text.IndexOf(keyValuePair.Value.szVoice) == 0)
				{
					return keyValuePair.Key;
				}
			}
			return CHAR_ID.MOB;
		}

		// Token: 0x06000077 RID: 119 RVA: 0x00003D34 File Offset: 0x00001F34
		public static string GetVoiceFromId(CHAR_ID nCharId)
		{
			Debug.Assert(nCharId < CHAR_ID.NUM, string.Format("範囲外のキャラIDです{0}\n", nCharId));
			return Chara.m_CharTbl[nCharId].szVoice;
		}

		// Token: 0x06000078 RID: 120 RVA: 0x00003D70 File Offset: 0x00001F70
		public static string GetKsName(CHAR_ID eChar, int iNo)
		{
			Debug.Assert(Chara.m_CharTbl[eChar].szKsName != null, "指定キャラにはKS名がセットされていません\n");
			return string.Format("{0}{1:00}.ks", Chara.m_CharTbl[eChar].szKsName, iNo);
		}

		// Token: 0x040000EC RID: 236
		private static Dictionary<CHAR_ID, GAMECHAR_TABLE> m_CharTbl = new Dictionary<CHAR_ID, GAMECHAR_TABLE>();
	}
}
