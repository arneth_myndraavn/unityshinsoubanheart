﻿using System;
using Game;
using Qoo.Def;

namespace Qoo.Game
{
	// Token: 0x020000E5 RID: 229
	public class PaymentParam
	{
		// Token: 0x06000634 RID: 1588 RVA: 0x00019BD0 File Offset: 0x00017DD0
		public static void Conv()
		{
			for (int num = 0; num != 10; num++)
			{
				if (PayDef.IsRoutePay((CHAR_ID)num))
				{
					GameData.SetParamInt("money", Chara.GetNameFromId((CHAR_ID)num), 1);
				}
				else
				{
					GameData.SetParamInt("money", Chara.GetNameFromId((CHAR_ID)num), 0);
				}
			}
			if (SysData.IsPayFull())
			{
				GameData.SetParamInt("moneyfull", string.Empty, 1);
			}
			else
			{
				GameData.SetParamInt("moneyfull", string.Empty, 0);
			}
		}
	}
}
