﻿using System;
using Game;
using Qoo.Def;
using Qoo.File;
using Qoo.Ks;
using Qoo.Param;
using Qoo.SoundSystem;

namespace Qoo.Game
{
	// Token: 0x02000029 RID: 41
	public static class SysData
	{
		// Token: 0x17000024 RID: 36
		// (get) Token: 0x06000113 RID: 275 RVA: 0x00005E7C File Offset: 0x0000407C
		public static Read ReadData
		{
			get
			{
				return SysData.m_SysRead;
			}
		}

		// Token: 0x17000025 RID: 37
		// (get) Token: 0x06000114 RID: 276 RVA: 0x00005E84 File Offset: 0x00004084
		public static Look LookCg
		{
			get
			{
				return SysData.m_LookCg;
			}
		}

		// Token: 0x17000026 RID: 38
		// (get) Token: 0x06000115 RID: 277 RVA: 0x00005E8C File Offset: 0x0000408C
		public static Look LookMovie
		{
			get
			{
				return SysData.m_LookMovie;
			}
		}

		// Token: 0x17000027 RID: 39
		// (get) Token: 0x06000116 RID: 278 RVA: 0x00005E94 File Offset: 0x00004094
		public static Look LookBgm
		{
			get
			{
				return SysData.m_LookBgm;
			}
		}

		// Token: 0x17000028 RID: 40
		// (get) Token: 0x06000117 RID: 279 RVA: 0x00005E9C File Offset: 0x0000409C
		public static SystemParam Param
		{
			get
			{
				return SysData.m_Param;
			}
		}

		// Token: 0x06000118 RID: 280 RVA: 0x00005EA4 File Offset: 0x000040A4
		public static bool Init()
		{
			SysData.InitCg();
			SysData.InitBgm();
            return SysData.m_Param.Init();
		}

		// Token: 0x06000119 RID: 281 RVA: 0x00005EBC File Offset: 0x000040BC
		public static void SetDefaultSetting()
		{
			SysData.m_Param.Defualt();
		}

		// Token: 0x0600011A RID: 282 RVA: 0x00005ECC File Offset: 0x000040CC
		public static int GetSystemFlag(SYSTEM_IDX eIdx)
		{
			return SysData.m_Param.Get((int)eIdx);
		}

		// Token: 0x0600011B RID: 283 RVA: 0x00005EDC File Offset: 0x000040DC
		public static void SetSystemFlag(SYSTEM_IDX eIdx, int Value)
		{
			SysData.m_Param.Set((int)eIdx, Value);
		}

		// Token: 0x0600011C RID: 284 RVA: 0x00005EEC File Offset: 0x000040EC
		public static int GetVolumeBgm()
		{
			return SysData.m_Param.Get(3);
		}

		// Token: 0x0600011D RID: 285 RVA: 0x00005EFC File Offset: 0x000040FC
		public static int GetVolumeSe()
		{
			return SysData.m_Param.Get(4);
		}

		// Token: 0x0600011E RID: 286 RVA: 0x00005F0C File Offset: 0x0000410C
		public static int GetVolumeSys()
		{
			return SysData.m_Param.Get(2);
		}

		// Token: 0x0600011F RID: 287 RVA: 0x00005F1C File Offset: 0x0000411C
		public static void SetVolumeBgm(int iVol)
		{
			SysData.m_Param.Set(3, iVol);
		}

		// Token: 0x06000120 RID: 288 RVA: 0x00005F2C File Offset: 0x0000412C
		public static void SetVolumeSe(int iVol)
		{
			SysData.m_Param.Set(4, iVol);
		}

		// Token: 0x06000121 RID: 289 RVA: 0x00005F3C File Offset: 0x0000413C
		public static void SetVolumeSys(int iVol)
		{
			SysData.m_Param.Set(2, iVol);
		}

		// Token: 0x06000122 RID: 290 RVA: 0x00005F4C File Offset: 0x0000414C
		public static bool IsVoiceEnable()
		{
			return SysData.m_Param.Get(5) == 1;
		}

		// Token: 0x06000123 RID: 291 RVA: 0x00005F5C File Offset: 0x0000415C
		public static void SetVoiceEnable(bool IsVoice)
		{
			SysData.m_Param.Set(5, (!IsVoice) ? 0 : 1);
		}

		// Token: 0x06000124 RID: 292 RVA: 0x00005F78 File Offset: 0x00004178
		public static int GetVoiceVolume(CHAR_ID eChar)
		{
			return SysData.m_Param.Get((int)(6 + eChar));
		}

		// Token: 0x06000125 RID: 293 RVA: 0x00005F88 File Offset: 0x00004188
		public static void SetVoiceVolume(CHAR_ID eChar, int iVol)
		{
			SysData.m_Param.Set((int)(6 + eChar), iVol);
		}

		// Token: 0x06000126 RID: 294 RVA: 0x00005F9C File Offset: 0x0000419C
		public static int GetTextSpeed()
		{
			return SysData.m_Param.Get(24);
		}

		// Token: 0x06000127 RID: 295 RVA: 0x00005FAC File Offset: 0x000041AC
		public static void SetTextSpeed(int iSpeed)
		{
			SysData.m_Param.Set(24, iSpeed);
		}

		// Token: 0x06000128 RID: 296 RVA: 0x00005FBC File Offset: 0x000041BC
		public static int GetFontType()
		{
			return SysData.m_Param.Get(25);
		}

		// Token: 0x06000129 RID: 297 RVA: 0x00005FCC File Offset: 0x000041CC
		public static void SetFontType(int iType)
		{
			SysData.m_Param.Set(25, iType);
		}

		// Token: 0x0600012A RID: 298 RVA: 0x00005FDC File Offset: 0x000041DC
		public static bool IsChangeReadTextColor()
		{
			return SysData.m_Param.Get(26) == 1;
		}

		// Token: 0x0600012B RID: 299 RVA: 0x00005FF0 File Offset: 0x000041F0
		public static void SetChangeReadTextColor(bool IsChange)
		{
			SysData.m_Param.Set(26, (!IsChange) ? 0 : 1);
		}

		// Token: 0x0600012C RID: 300 RVA: 0x0000600C File Offset: 0x0000420C
		public static bool IsEnablePassageAnim()
		{
			return SysData.m_Param.Get(27) == 1;
		}

		// Token: 0x0600012D RID: 301 RVA: 0x00006020 File Offset: 0x00004220
		public static void SetEnablePassageAnim(bool IsEnable)
		{
			SysData.m_Param.Set(27, (!IsEnable) ? 0 : 1);
		}

		// Token: 0x0600012E RID: 302 RVA: 0x0000603C File Offset: 0x0000423C
		public static bool IsEnableLoveAnim()
		{
			return SysData.m_Param.Get(28) == 1;
		}

		// Token: 0x0600012F RID: 303 RVA: 0x00006050 File Offset: 0x00004250
		public static void SetEnableLoveAnim(bool IsEnable)
		{
			SysData.m_Param.Set(28, (!IsEnable) ? 0 : 1);
		}

		// Token: 0x06000130 RID: 304 RVA: 0x0000606C File Offset: 0x0000426C
		public static SKIP_MODE GetSkip()
		{
			return (SKIP_MODE)SysData.m_Param.Get(22);
		}

		// Token: 0x06000131 RID: 305 RVA: 0x0000607C File Offset: 0x0000427C
		public static void SetSkip(int iType)
		{
			SysData.m_Param.Set(22, iType);
		}

		// Token: 0x06000132 RID: 306 RVA: 0x0000608C File Offset: 0x0000428C
		public static bool IsSkipContinue()
		{
			return SysData.m_Param.Get(29) == 1;
		}

		// Token: 0x06000133 RID: 307 RVA: 0x000060A0 File Offset: 0x000042A0
		public static void SetSkipContinue(bool bEnable)
		{
			SysData.m_Param.Set(29, (!bEnable) ? 0 : 1);
		}

		// Token: 0x06000134 RID: 308 RVA: 0x000060BC File Offset: 0x000042BC
		public static int GetAutoPage()
		{
			return SysData.m_Param.Get(23);
		}

		// Token: 0x06000135 RID: 309 RVA: 0x000060CC File Offset: 0x000042CC
		public static void SetAutoPage(int iType)
		{
			SysData.m_Param.Set(23, iType);
		}

		// Token: 0x06000136 RID: 310 RVA: 0x000060DC File Offset: 0x000042DC
		public static bool IsScreenJump()
		{
			return SysData.m_Param.Get(30) == 1;
		}

		// Token: 0x06000137 RID: 311 RVA: 0x000060F0 File Offset: 0x000042F0
		public static void SetScreenJump(bool bEnable)
		{
			SysData.m_Param.Set(30, (!bEnable) ? 0 : 1);
		}

		// Token: 0x06000138 RID: 312 RVA: 0x0000610C File Offset: 0x0000430C
		public static bool IsDrawFace()
		{
			return SysData.m_Param.Get(31) == 1;
		}

		// Token: 0x06000139 RID: 313 RVA: 0x00006120 File Offset: 0x00004320
		public static void SetDrawFace(bool bEnable)
		{
			SysData.m_Param.Set(31, (!bEnable) ? 0 : 1);
		}

		// Token: 0x0600013A RID: 314 RVA: 0x0000613C File Offset: 0x0000433C
		public static bool InitCg()
		{
			SysData.m_LookCg.Clear();
			if (!SysData.InitLook(ref SysData.m_LookCg, "CG_START", "CG_END"))
			{
				Debug.Assert(false, "NMBファイル内にCG領域がありません");
			}
			return true;
		}

		// Token: 0x0600013B RID: 315 RVA: 0x00006170 File Offset: 0x00004370
		public static bool IsReadCG(string name)
		{
            return SysData.m_LookCg.IsLook(FileId.Normalize(name));
		}

		// Token: 0x0600013C RID: 316 RVA: 0x00006184 File Offset: 0x00004384
		public static bool SetReadCG(string name)
		{
			return SysData.m_LookCg.Set(FileId.Normalize(name), true);
		}

		// Token: 0x0600013D RID: 317 RVA: 0x00006198 File Offset: 0x00004398
		public static void ResetReadCGAll()
		{
			SysData.m_LookCg.Init();
		}

		// Token: 0x0600013E RID: 318 RVA: 0x000061A4 File Offset: 0x000043A4
		//Will 100% all CGs if called
		public static void SetReadCGAll()
		{
			SysData.m_LookCg.SetAll(true);
		}

		// Token: 0x0600013F RID: 319 RVA: 0x000061B4 File Offset: 0x000043B4
		private static bool InitMovie()
		{
			SysData.m_LookMovie.Clear();
			if (!SysData.InitLook(ref SysData.m_LookMovie, "MOV_START", "MOV_END"))
			{
				Debug.Assert(false, "NMBファイル内にムービーリスト領域がありません");
			}
			return true;
		}

		// Token: 0x06000140 RID: 320 RVA: 0x000061E8 File Offset: 0x000043E8
		public static bool IsLookMovie(string strMovie)
		{
			return SysData.m_LookMovie.IsLook(FileId.Normalize(strMovie));
		}

		// Token: 0x06000141 RID: 321 RVA: 0x000061FC File Offset: 0x000043FC
		public static bool SetLookMovie(string strMovie)
		{
			return SysData.m_LookMovie.Set(FileId.Normalize(strMovie), true);
		}

		// Token: 0x06000142 RID: 322 RVA: 0x00006210 File Offset: 0x00004410
		public static void ResetLookMovieAll()
		{
			SysData.m_LookMovie.Init();
		}

		// Token: 0x06000143 RID: 323 RVA: 0x0000621C File Offset: 0x0000441C
		public static void SetReadMovieAll()
		{
			SysData.m_LookMovie.SetAll(true);
		}

		// Token: 0x06000144 RID: 324 RVA: 0x0000622C File Offset: 0x0000442C
		public static void SetRead(int ks, int label = -1, int pos = 0)
		{
			SysData.m_SysRead.Add(ks, label, pos);
		}

		// Token: 0x06000145 RID: 325 RVA: 0x0000623C File Offset: 0x0000443C
		public static bool IsRead(int iKsNo, int iLabelNo = 0, int iPos = 0)
		{
			int num = (int)(Akb.GetFileInfo(iKsNo).nLabelPos + (uint)iLabelNo);
			if (num < Akb.GetLabelNum())
			{
				int nTagNum = Akb.GetLabelInfo(num).nTagNum;
				if (nTagNum <= iPos)
				{
					iLabelNo++;
					iPos = 0;
				}
			}
			return SysData.m_SysRead.IsRead(iKsNo, iLabelNo, iPos);
		}

		// Token: 0x06000146 RID: 326 RVA: 0x00006290 File Offset: 0x00004490
		public static void SetRead(string ks, string label = "", int iPos = 0)
		{
			int ks2 = 0;
			int label2 = 0;
			if (Akb.GetFileLabel(ref ks2, ref label2, ks, label))
			{
				SysData.SetRead(ks2, label2, iPos);
			}
		}

		// Token: 0x06000147 RID: 327 RVA: 0x000062BC File Offset: 0x000044BC
		public static bool IsRead(string ks, string label = "", int iPos = 0)
		{
			int iKsNo = 0;
			int iLabelNo = 0;
			return Akb.GetFileLabel(ref iKsNo, ref iLabelNo, ks, label) && SysData.IsRead(iKsNo, iLabelNo, iPos);
		}

		// Token: 0x06000148 RID: 328 RVA: 0x000062E8 File Offset: 0x000044E8
		public static void ClearRead()
		{
			SysData.m_SysRead.Init();
		}

		// Token: 0x06000149 RID: 329 RVA: 0x000062F4 File Offset: 0x000044F4
		public static void SetReadAll()
		{
			SysData.m_SysRead.SetAll();
		}

		// Token: 0x0600014A RID: 330 RVA: 0x00006300 File Offset: 0x00004500
		private static bool InitBgm()
		{
			SysData.m_LookBgm.Clear();
			if (!SysData.InitLook(ref SysData.m_LookBgm, "DIR_BEGIN_BGM", "DIR_END_BGM"))
			{
				Debug.Assert(false, "NMBファイル内にBGMリスト領域がありません");
			}
			return true;
		}

		// Token: 0x0600014B RID: 331 RVA: 0x00006334 File Offset: 0x00004534
		public static bool IsPlayBgmFlag(string name)
		{
			return SysData.m_LookBgm.IsLook(FileId.Normalize(name));
		}

		// Token: 0x0600014C RID: 332 RVA: 0x00006348 File Offset: 0x00004548
		public static void SetPlayBgmFlag(string name)
		{
			SysData.m_LookBgm.Set(FileId.Normalize(name), true);
		}

		// Token: 0x0600014D RID: 333 RVA: 0x0000635C File Offset: 0x0000455C
		public static void ResetPlayBgmFlagAll()
		{
			SysData.m_LookBgm.Init();
		}

		// Token: 0x0600014E RID: 334 RVA: 0x00006368 File Offset: 0x00004568
		public static void SetPlayBgmFlagAll()
		{
			SysData.m_LookBgm.SetAll(true);
		}

		// Token: 0x0600014F RID: 335 RVA: 0x00006378 File Offset: 0x00004578
		private static bool InitLook(ref Look look, string start, string end)
		{
			int num = Nmb.NameToNo(start);
			int num2 = Nmb.NameToNo(end);
			if (num < 0 || num2 < 0)
			{
				return false;
			}
			for (int i = num + 1; i < num2; i++)
			{
				look.Add(Nmb.GetFileInfo(i).StrName);
			}
			return true;
		}

		// Token: 0x06000150 RID: 336 RVA: 0x000063CC File Offset: 0x000045CC
		internal static void SetReadRef(string p)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000151 RID: 337 RVA: 0x000063D4 File Offset: 0x000045D4
		public static bool IsReadedMark(string ks, string label)
		{
			bool result = false;
			if (SysData.IsChangeReadTextColor())
			{
				result = SysData.IsRead(ks, label, 0);
			}
			return result;
		}

		// Token: 0x06000152 RID: 338 RVA: 0x000063F8 File Offset: 0x000045F8
		internal static void SetPay(CHAR_ID idChar, bool isPay, bool isVoice)
		{
			SysData.m_Param.Set((int)(33 + idChar), (!isPay) ? 0 : 1);
			SysData.m_Param.Set((int)(49 + idChar), (!isVoice) ? 0 : 1);
		}

		// Token: 0x06000153 RID: 339 RVA: 0x00006440 File Offset: 0x00004640
		internal static void SetPayFull(bool isPay, bool isVoice)
		{
			SysData.m_Param.Set(65, (!isPay) ? 0 : 1);
			SysData.m_Param.Set(66, (!isVoice) ? 0 : 1);
		}

		// Token: 0x06000154 RID: 340 RVA: 0x00006484 File Offset: 0x00004684
		internal static void SetPayRoute(CHAR_ID idChar, bool isPay)
		{
			SysData.m_Param.Set((int)(33 + idChar), (!isPay) ? 0 : 1);
		}

		// Token: 0x06000155 RID: 341 RVA: 0x000064A4 File Offset: 0x000046A4
		internal static void SetPayVoice(CHAR_ID idChar, bool isVoice)
		{
			SysData.m_Param.Set((int)(49 + idChar), (!isVoice) ? 0 : 1);
		}

		// Token: 0x06000156 RID: 342 RVA: 0x000064C4 File Offset: 0x000046C4
		internal static void SetPayFullRoute(bool isPay)
		{
			SysData.m_Param.Set(65, (!isPay) ? 0 : 1);
		}

		// Token: 0x06000157 RID: 343 RVA: 0x000064E0 File Offset: 0x000046E0
		internal static void SetPayFullVoice(bool isVoice)
		{
			SysData.m_Param.Set(66, (!isVoice) ? 0 : 1);
		}

		// Token: 0x06000158 RID: 344 RVA: 0x000064FC File Offset: 0x000046FC
		internal static bool IsPay(CHAR_ID idChar)
		{
			return SysData.IsPayFull() || SysData.m_Param.Get((int)(33 + idChar)) > 0;
		}

		// Token: 0x06000159 RID: 345 RVA: 0x00006528 File Offset: 0x00004728
		internal static bool IsPayFull()
		{
			return SysData.m_Param.Get(65) > 0;
		}

		// Token: 0x0600015A RID: 346 RVA: 0x00006544 File Offset: 0x00004744
		internal static bool IsPayVoice(CHAR_ID idChar)
		{
			if (SysData.IsPayVoiceFull())
			{
				return true;
			}
			if (idChar == CHAR_ID.NOTHING)
			{
				for (CHAR_ID char_ID = CHAR_ID.BLOOD; char_ID < CHAR_ID.NIGHTMARE; char_ID++)
				{
					if (SysData.m_Param.Get((int)(49 + char_ID)) > 0)
					{
						return true;
					}
				}
				return false;
			}
			if (idChar == CHAR_ID.NIGHTMARE)
			{
				bool result = true;
				for (CHAR_ID char_ID2 = CHAR_ID.BLOOD; char_ID2 < CHAR_ID.NIGHTMARE; char_ID2++)
				{
					if (SysData.m_Param.Get((int)(49 + char_ID2)) <= 0)
					{
						result = false;
						break;
					}
				}
				return result;
			}
			return SysData.m_Param.Get((int)(49 + idChar)) > 0;
		}

		// Token: 0x0600015B RID: 347 RVA: 0x000065EC File Offset: 0x000047EC
		internal static bool IsPayVoiceFull()
		{
			return SysData.m_Param.Get(66) > 0;
		}

		// Token: 0x0600015C RID: 348 RVA: 0x00006608 File Offset: 0x00004808
		public static void Apply()
		{
			Sound.BgmApplyVolume();
			Sound.SeSlotApplyVolume();
		}

		// Token: 0x0600015D RID: 349 RVA: 0x00006614 File Offset: 0x00004814
		public static bool IsTrial()
		{
			return Akb.CheckFile("op00_trial.ks");
		}

		// Token: 0x0400011F RID: 287
		private static Read m_SysRead = new Read();

		// Token: 0x04000120 RID: 288
		private static Look m_LookCg = new Look();

		// Token: 0x04000121 RID: 289
		private static Look m_LookMovie = new Look();

		// Token: 0x04000122 RID: 290
		private static Look m_LookBgm = new Look();

		// Token: 0x04000123 RID: 291
		private static SystemParam m_Param = new SystemParam();
	}
}
