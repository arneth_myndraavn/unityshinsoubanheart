﻿using System;
using System.Collections;
using System.Collections.Generic;
using Game;
using Qoo.Input;
using Qoo.Wnd;
using UnityEngine;

namespace Qoo.Game
{
	// Token: 0x020000DE RID: 222
	public static class KsExec
	{
		// Token: 0x06000611 RID: 1553 RVA: 0x00019374 File Offset: 0x00017574
		internal static bool KsExecProc(string Func, string Param, string Value, int count)
		{
			Dictionary<string,int> dict = null;
			if (Func != null)
			{
				if (dict == null)
				{
					dict = new Dictionary<string, int>(2)
					{
						{
							"inputname",
							0
						},
						{
							"kakin",
							1
						}
					};
				}
				int num;
				if (dict.TryGetValue(Func, out num))
				{
					if (num != 0)
					{
						if (num == 1)
						{
							if (Debug.IsAutoKsDebug)
							{
								return true;
							}
							KsInput.Enable = false;
							KsInput.Clear();
							/*bool flag = KsExec.ExecPay(Param, Value, count);
							if (flag)
							{
								KsInput.Enable = true;
							}
							return flag;*/
							//should disable all payment checks
							return true;
						}
					}
					else
					{
						if (Debug.IsAutoKsDebug)
						{
							return true;
						}
						KsInput.Enable = false;
						bool flag2 = KsExec.InputName(count);
						if (flag2)
						{
							KsInput.Enable = true;
						}
						return flag2;
					}
				}
			}
			return false;
		}

		// Token: 0x06000612 RID: 1554 RVA: 0x0001943C File Offset: 0x0001763C
		public static bool InputName(int count)
		{
			//UnityEngine.Debug.LogWarning(count);
			if (count == 0)
			{
				UnityEngine.Debug.LogWarning("first");
				KsExec.confirm = false;
				KsExec.confirmResult = false;
				if (UnityApp.Instance.isJapanese)
				{
					KsExec.editName = "アリス";
                    KsExec.editLastName = "リデル";
                }
                else
                {
					KsExec.editName = "Alice";
                    KsExec.editLastName = "Liddell";
                }
				NameInputKeyboard.DebugInputText = KsExec.editName;
                NameInputKeyboard.DebugInputTextLastName = KsExec.editLastName;
                KsExec.KeyboardMain();
			}
			else if (KsExec.confirm)
			{
				if (KsExec.confirmResult)
				{
					GameData.SetParamString("firstname", string.Empty, KsExec.editName);
                    GameData.SetParamString("familyname", string.Empty, KsExec.editLastName);
                    NameInputKeyboard.Close();
					return true;
				}
				KsExec.KeyboardMain();
				KsExec.confirm = false;
				KsExec.confirmResult = false;
			}
			else
			{
				KsExec.editName = NameInputKeyboard.InputText;
                KsExec.editLastName = NameInputKeyboard.InputTextLastName;
                if (KsExec.editName.Length == 0 || KsExec.checkSpaceOnly(KsExec.editName))
				{
						UnityTask.SetSubNoUnityTask(MessageDlg.ExecDlg("No name entered", null));
					KsExec.KeyboardMain();
				}
				else if (KsExec.editName.Length > 7)
				{
					UnityTask.SetSubNoUnityTask(MessageDlg.ExecDlg("You can enter up to 7 characters for the name.", null));
					KsExec.KeyboardMain();
				}
				else if (!Singleton<UnityGraph>.Instance.Font.Font.CheckGlyph(KsExec.editName))
				{
					UnityTask.SetSubNoUnityTask(MessageDlg.ExecDlg("Contains characters that cannot be displayed.", null));
					KsExec.KeyboardMain();
				}
				else
				{
					string arg = KsExec.editName;
                    string arg2 = KsExec.editLastName;
                    string paramString = GameData.GetParamString("familyname", string.Empty);
					string[] msgs;
                    /*if (UnityApp.Instance.isJapanese)
                    {
						msgs = new string[]
						{
							string.Format("「{0}＝{1}」です。", arg, paramString),
							"よろしいですか？"
						};
					}
                    else
                    {
						msgs = new string[]
						{
							string.Format("Is {0} {1} correct?", arg, paramString)
						};

					}*/

                    if (UnityApp.Instance.isJapanese)
                    {
                        msgs = new string[]
                        {
                            string.Format("「{0}＝{1}」です。", arg, arg2),
                            "よろしいですか？"
                        };
                    }
                    else
                    {
                        msgs = new string[]
                        {
                            string.Format("Is {0} {1} correct?", arg, arg2)
                        };

                    }
                    UnityTask.SetSubNoUnityTask(MessageDlg.ExecDlg(msgs, new Action<bool>(KsExec.SetResult)));
					KsExec.confirm = true;
					KsExec.confirmResult = false;
				}
			}
			return false;
		}

		// Token: 0x06000613 RID: 1555 RVA: 0x000195C0 File Offset: 0x000177C0
		private static bool checkSpaceOnly(string firstname)
		{
			foreach (char c in firstname)
			{
				if (c != ' ' && c != '\u3000')
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x06000614 RID: 1556 RVA: 0x00019604 File Offset: 0x00017804
		private static void KeyboardMain()
		{
			UnityTask.SetSubTask(NameInputKeyboard.Open(KsExec.editName, KsExec.editLastName, true));
			/*switch (UnityEngine.Application.platform)
			{
			case RuntimePlatform.IPhonePlayer:
			case RuntimePlatform.Android:
				UnityTask.SetSubTask(NameInputKeyboard.Open(KsExec.editName, true));
				break;
			}*/
		}

		// Token: 0x06000615 RID: 1557 RVA: 0x0001964C File Offset: 0x0001784C
		private static void SetResult(bool result)
		{
			KsExec.confirmResult = result;
		}

		// Token: 0x06000616 RID: 1558 RVA: 0x00019654 File Offset: 0x00017854
		private static bool ExecPay(string Param, string Value, int count)
		{
			if (count == 0)
			{
				PaymentParam.Conv();
				GameData.StartVoiceCheck();
				CHAR_ID charaId = CHAR_ID.NOTHING;
				Debug.Print("Param = " + Param + ": Value = " + Value);
				if (Param == "param" && Value == "full")
				{
					Debug.Print("初回処理");
					if (GameData.IsPayRouteFull())
					{
						return true;
					}
				}
				else
				{
					Debug.Print("初回処理以外");
					if (GameData.IsPayRouteFull())
					{
						return true;
					}
					if (GameData.IsPayRoute(GameData.GetRoute()))
					{
						return true;
					}
					charaId = GameData.GetRoute();
				}
				Debug.Print("課金処理起動");
				Singleton<UnityGraph>.Instance.SetEffect("FadeOut", 1f);
				UIValue.Payment_Call = PaymentCallType.ADV;
				UnityTask.SetSubNoUnityTask(new PaymentTask().Open(charaId));
				return false;
			}
			PaymentParam.Conv();
			if (Param == "param" && Value == "full")
			{
				if (!GameData.IsPayRouteFull())
				{
					GameData.IsMoveTitle = true;
				}
			}
			else if (!GameData.IsPayRoute(GameData.GetRoute()))
			{
				GameData.IsMoveTitle = true;
			}
			if (!GameData.IsMoveTitle)
			{
				Singleton<UnityGraph>.Instance.Enable(false);
				Singleton<UnityGraph>.Instance.SetEffect("FadeIn", 1f);
			}
			KsInput.ClearMenu();
			KsInput.ClearBacklog();
			return true;
		}

		// Token: 0x06000617 RID: 1559 RVA: 0x000197AC File Offset: 0x000179AC
		private static IEnumerator Sample_ExecPay(CHAR_ID idChar)
		{
			SysData.SetPay(idChar, true, true);
			SysData.SetPayFull(true, true);
			yield return 0;
			yield break;
		}

		// Token: 0x040005A9 RID: 1449
		private static bool confirm;

		// Token: 0x040005AA RID: 1450
		private static bool confirmResult;

		// Token: 0x040005AB RID: 1451
		private static string editName = string.Empty;

        private static string editLastName = string.Empty;
    }
}
