﻿using System;
using System.Collections.Generic;
using Game;
using Qoo.Application;
using Qoo.Def;
using Qoo.Input;
using Qoo.Ks;
using Qoo.Param;

namespace Qoo.Game
{
	// Token: 0x0200001B RID: 27
	public static class GameData
	{
		// Token: 0x17000014 RID: 20
		// (get) Token: 0x0600007A RID: 122 RVA: 0x00003E20 File Offset: 0x00002020
		public static Read ReadData
		{
			get
			{
				return GameData.m_GameRead;
			}
		}

		// Token: 0x17000015 RID: 21
		// (get) Token: 0x0600007B RID: 123 RVA: 0x00003E28 File Offset: 0x00002028
		// (set) Token: 0x0600007C RID: 124 RVA: 0x00003E30 File Offset: 0x00002030
		public static GameParam Param
		{
			get
			{
				return GameData.m_Param;
			}
			set
			{
				GameData.m_Param = value;
			}
		}

		// Token: 0x17000016 RID: 22
		// (get) Token: 0x0600007D RID: 125 RVA: 0x00003E38 File Offset: 0x00002038
		// (set) Token: 0x0600007E RID: 126 RVA: 0x00003E54 File Offset: 0x00002054
		public static GameParam LockParam
		{
			get
			{
				return (!GameData.m_isLockGameData) ? GameData.Param : GameData.m_LockParam;
			}
			set
			{
				GameData.m_LockParam = value;
			}
		}

		// Token: 0x17000017 RID: 23
		// (get) Token: 0x0600007F RID: 127 RVA: 0x00003E5C File Offset: 0x0000205C
		// (set) Token: 0x06000080 RID: 128 RVA: 0x00003E64 File Offset: 0x00002064
		public static List<KsWorkLog> OldWork
		{
			get
			{
				return GameData.m_OldWorkList;
			}
			set
			{
				GameData.m_OldWorkList = value;
			}
		}

		// Token: 0x17000018 RID: 24
		// (get) Token: 0x06000081 RID: 129 RVA: 0x00003E6C File Offset: 0x0000206C
		// (set) Token: 0x06000082 RID: 130 RVA: 0x00003E74 File Offset: 0x00002074
		public static bool EnableChangeParam
		{
			get
			{
				return GameData.m_isChangeParam;
			}
			set
			{
				GameData.m_isChangeParam = value;
			}
		}

		// Token: 0x17000019 RID: 25
		// (get) Token: 0x06000083 RID: 131 RVA: 0x00003E7C File Offset: 0x0000207C
		// (set) Token: 0x06000084 RID: 132 RVA: 0x00003E84 File Offset: 0x00002084
		public static bool IsMoveTitle
		{
			get
			{
				return GameData.m_isMoveTitle;
			}
			set
			{
				GameData.m_isMoveTitle = value;
			}
		}

		// Token: 0x06000085 RID: 133 RVA: 0x00003E8C File Offset: 0x0000208C
		public static void Clear()
		{
			GameData.Param.Clear();
			GameData.LockParam.Clear();
			GameData.m_GameRead.Init();
			GameData.OldWork.Clear();
			GameData.EnableChangeParam = false;
			GameData.m_isLoadData = false;
			GameData.m_strLastMessage = string.Empty;
			GameData.m_memory = false;
			GameData.m_memoryKs = string.Empty;
			GameData.m_memoryLabel = string.Empty;
		}

		// Token: 0x06000086 RID: 134 RVA: 0x00003EF4 File Offset: 0x000020F4
		public static void Init()
		{
			GameData.Param.Init();
			GameData.LockParam.Init();
			GameData.m_GameRead.Init();
			GameData.OldWork.Clear();
			GameData.EnableChangeParam = false;
			GameData.m_isLockGameData = false;
			GameData.m_isLoadData = false;
			GameData.m_strLastMessage = string.Empty;
			GameData.m_memory = false;
			GameData.m_memoryKs = string.Empty;
			GameData.m_memoryLabel = string.Empty;
		}

		// Token: 0x06000087 RID: 135 RVA: 0x00003F60 File Offset: 0x00002160
		public static bool SetRead(int nKsNo, int nLabelNo, int nTagNo)
		{
			GameData.m_GameRead.Set(nKsNo, nLabelNo, nTagNo);
			SysData.SetRead(nKsNo, nLabelNo, nTagNo);
			return true;
		}

		// Token: 0x06000088 RID: 136 RVA: 0x00003F7C File Offset: 0x0000217C
		internal static bool IsRead(int KsNo, int LabelNo, int TagNo)
		{
			return GameData.m_GameRead.IsRead(KsNo, LabelNo, TagNo);
		}

		// Token: 0x06000089 RID: 137 RVA: 0x00003F8C File Offset: 0x0000218C
		internal static bool IsRead(string KsName, string LabelName)
		{
			int nKs = 0;
			int nLabel = 0;
			return Akb.GetFileLabel(ref nKs, ref nLabel, KsName, LabelName) && GameData.m_GameRead.IsRead(nKs, nLabel, 0);
		}

		// Token: 0x0600008A RID: 138 RVA: 0x00003FBC File Offset: 0x000021BC
		public static bool CheckReadedSkip(int nKsNo, int nLabelNo, int nTagNo)
		{
			if (!KsInput.IsSkip)
			{
				return false;
			}
			if (SysData.GetSkip() == SKIP_MODE.ALL)
			{
				return true;
			}
			if (SysData.GetSkip() == SKIP_MODE.DISABLE || !SysData.IsRead(nKsNo, nLabelNo, nTagNo + 1))
			{
				KsInput.Unlock();
				return false;
			}
			return true;
		}

		// Token: 0x0600008B RID: 139 RVA: 0x00004004 File Offset: 0x00002204
		public static bool AttachParam(string name, int init, string[] indexArray = null)
		{
			if (indexArray != null)
			{
				for (int num = 0; num != indexArray.Length; num++)
				{
					GameData.Param.Attach(name + indexArray[num], init.ToString(), num, false);
					Debug.Print(string.Format("INFO:ゲームパラメータ {0}[{1}]={2} type=int を追加しました", name, indexArray[num], init));
				}
			}
			else
			{
				GameData.Param.Attach(name, init.ToString(), 0, false);
				Debug.Print(string.Format("INFO:ゲームパラメータ {0}={1} type=int を追加しました", name, init));
			}
			return true;
		}

		// Token: 0x0600008C RID: 140 RVA: 0x00004094 File Offset: 0x00002294
		public static bool AttachParam(string name, string init, string[] indexArray = null)
		{
			if (indexArray != null)
			{
				for (int num = 0; num != indexArray.Length; num++)
				{
					GameData.Param.Attach(name + indexArray[num], init, num, true);
					Debug.Print(string.Format("INFO:ゲームパラメータ {0}[{1}]={2} type=string を追加しました", name, indexArray[num], init));
				}
			}
			else
			{
				GameData.Param.Attach(name, init, 0, true);
				Debug.Print(string.Format("INFO:ゲームパラメータ {0}={1} type=string を追加しました", name, init));
			}
			return true;
		}

		// Token: 0x0600008D RID: 141 RVA: 0x0000410C File Offset: 0x0000230C
		public static bool SetParamString(string name, string index, string param)
		{
			string name2 = name + index;
			string old_param = GameData.Param.Get(name2);
			if (name == "rand")
			{
				param = App.GetRandom(int.Parse(param)).ToString();
			}
			GameData.ChangeParam(name, index, old_param, param);
			return GameData.Param.Set(name2, param);
		}

		// Token: 0x0600008E RID: 142 RVA: 0x00004168 File Offset: 0x00002368
		public static string GetParamString(string name, string index = "")
		{
			return GameData.Param.Get(name + index);
		}

		// Token: 0x0600008F RID: 143 RVA: 0x0000417C File Offset: 0x0000237C
		public static int GetParamInt(string name, string index = "")
		{
			return GameData.Param.GetInt(name + index);
		}

		// Token: 0x06000090 RID: 144 RVA: 0x00004190 File Offset: 0x00002390
		public static bool SetParamInt(string name_, string index_, int param_)
		{
			string name = name_ + index_;
			string old_param = GameData.Param.Get(name);
			if (name_ == "rand")
			{
				int random = App.GetRandom(param_);
				Debug.Print(string.Format("INFO:パラメータ:{0}に{1}を元に{2}を設定しました", name_, param_, random));
				param_ = random;
			}
			GameData.ChangeParam(name_, index_, old_param, param_.ToString());
			return GameData.Param.SetInt(name_ + index_, param_);
		}

		// Token: 0x06000091 RID: 145 RVA: 0x00004208 File Offset: 0x00002408
		public static bool CmpParam(string valueName, string indexName, string paramName)
		{
			string name = valueName + indexName;
			if (GameData.Param.IsString(name))
			{
				return GameData.CmpParamString(valueName, indexName, paramName);
			}
			return GameData.CmpParamInt(valueName, indexName, int.Parse(paramName));
		}

		// Token: 0x06000092 RID: 146 RVA: 0x00004244 File Offset: 0x00002444
		public static bool CmpParamString(string ValueName, string IndexName, string Param_)
		{
			return GameData.GetParamString(ValueName, IndexName) == Param_;
		}

		// Token: 0x06000093 RID: 147 RVA: 0x00004254 File Offset: 0x00002454
		public static bool CmpParamInt(string ValueName, string IndexName, int Param_)
		{
			int paramInt = GameData.GetParamInt(ValueName, IndexName);
			return Param_ <= paramInt;
		}

		// Token: 0x06000094 RID: 148 RVA: 0x00004270 File Offset: 0x00002470
		public static bool AddParam(string value_, string index_, string param_)
		{
			int paramInt = GameData.GetParamInt(value_, index_);
			return GameData.SetParamInt(value_, index_, int.Parse(param_) + paramInt);
		}

		// Token: 0x06000095 RID: 149 RVA: 0x00004294 File Offset: 0x00002494
		public static bool CmpOtherParam(string ValueName, string IndexName, string Value1Name, string Index1Name)
		{
			return GameData.GetParamString(ValueName, IndexName) == GameData.GetParamString(Value1Name, Index1Name);
		}

		// Token: 0x06000096 RID: 150 RVA: 0x000042AC File Offset: 0x000024AC
		public static int SearchParam(string p, ref int nArraySize)
		{
			nArraySize = 0;
			int num = GameData.Param.SearchZeroIndex(p);
			if (num < 0)
			{
				return -1;
			}
			nArraySize = GameData.Param.GetIndexNum(num);
			return num;
		}

		// Token: 0x06000097 RID: 151 RVA: 0x000042E0 File Offset: 0x000024E0
		internal static string GetParamString(int index)
		{
			return GameData.Param.GetParamString(index);
		}

		// Token: 0x06000098 RID: 152 RVA: 0x000042F0 File Offset: 0x000024F0
		internal static void SetParam(int index, int value)
		{
			GameData.Param.SetParam(index, value);
		}

		// Token: 0x06000099 RID: 153 RVA: 0x00004300 File Offset: 0x00002500
		public static bool IsPayRoute(CHAR_ID id)
		{
			return GameData.GetParamInt("money", Chara.GetNameFromId(id)) > 0;
		}

		// Token: 0x0600009A RID: 154 RVA: 0x00004320 File Offset: 0x00002520
		public static bool IsPayRouteFull()
		{
			return GameData.GetParamInt("moneyfull", string.Empty) > 0;
		}

		// Token: 0x0600009B RID: 155 RVA: 0x00004340 File Offset: 0x00002540
		public static void StartVoiceCheck()
		{
			GameData.SetParamInt("voicecheck", string.Empty, 0);
		}

		// Token: 0x0600009C RID: 156 RVA: 0x00004354 File Offset: 0x00002554
		public static void ResetVoiceCheck()
		{
			GameData.SetParamInt("voicecheck", string.Empty, 0);
		}

		// Token: 0x0600009D RID: 157 RVA: 0x00004368 File Offset: 0x00002568
		public static bool IsVoiceCheck()
		{
			return GameData.GetParamInt("voicecheck", string.Empty) > 0;
		}

		// Token: 0x0600009E RID: 158 RVA: 0x00004388 File Offset: 0x00002588
		public static bool IsFullVoiceCheck()
		{
			return GameData.GetParamInt("voicecheck", string.Empty) > 0;
		}

		// Token: 0x0600009F RID: 159 RVA: 0x000043A8 File Offset: 0x000025A8
		private static void ChangeParam(string name, string index, string old_param, string new_param)
		{
			if (GameData.EnableChangeParam && new_param != old_param)
			{
				Debug.Print(string.Format("INFO:ChangeParam:Name={0},Index={1},New={2},Old={3}", new object[]
				{
					name,
					index,
					new_param,
					old_param
				}));
				KsWorkLog item = default(KsWorkLog);
				item.Value = name;
				item.Index = index;
				item.Old = old_param;
				item.New = new_param;
				GameData.OldWork.Add(item);
			}
		}

		// Token: 0x060000A0 RID: 160 RVA: 0x00004424 File Offset: 0x00002624
		internal static void SetGameEnd()
		{
		}

		// Token: 0x060000A1 RID: 161 RVA: 0x00004428 File Offset: 0x00002628
		public static bool LockGameData()
		{
			bool isLockGameData = GameData.m_isLockGameData;
			GameData.m_isLockGameData = true;
			GameData.LockParam.Copy(GameData.Param);
			return isLockGameData;
		}

		// Token: 0x060000A2 RID: 162 RVA: 0x00004454 File Offset: 0x00002654
		public static bool UnlockGameData()
		{
			bool isLockGameData = GameData.m_isLockGameData;
			GameData.m_isLockGameData = false;
			return isLockGameData;
		}

		// Token: 0x1700001A RID: 26
		// (get) Token: 0x060000A3 RID: 163 RVA: 0x00004470 File Offset: 0x00002670
		// (set) Token: 0x060000A4 RID: 164 RVA: 0x00004478 File Offset: 0x00002678
		public static string LastMessage
		{
			get
			{
				return GameData.m_strLastMessage;
			}
			set
			{
				GameData.m_strLastMessage = value;
			}
		}

		// Token: 0x060000A5 RID: 165 RVA: 0x00004480 File Offset: 0x00002680
		public static string GetSceneName()
		{
			return GameData.GetParamString("scene", string.Empty);
		}

		// Token: 0x060000A6 RID: 166 RVA: 0x00004494 File Offset: 0x00002694
		public static CHAR_ID GetRoute()
		{
			return Chara.GetIdFromName(GameData.GetParamString("route", string.Empty));
		}

		// Token: 0x060000A7 RID: 167 RVA: 0x000044AC File Offset: 0x000026AC
		public static void SetRoute(CHAR_ID id)
		{
			GameData.SetParamString("route", string.Empty, Chara.GetNameFromId(id));
		}

		// Token: 0x060000A8 RID: 168 RVA: 0x000044C4 File Offset: 0x000026C4
		public static string GetFirstName()
		{
			return GameData.GetParamString("firstname", string.Empty);
		}

		// Token: 0x060000A9 RID: 169 RVA: 0x000044D8 File Offset: 0x000026D8
		public static void SetLastName(string name)
		{
			GameData.SetParamString("familyname", string.Empty, name);
		}

        public static string GetLastName()
        {
            return GameData.GetParamString("familyname", string.Empty);
        }

        // Token: 0x060000A9 RID: 169 RVA: 0x000044D8 File Offset: 0x000026D8
        public static void SetFirstName(string name)
        {
            GameData.SetParamString("firstname", string.Empty, name);
        }

        // Token: 0x060000AA RID: 170 RVA: 0x000044EC File Offset: 0x000026EC
        public static byte[] Save()
		{
			if (!GameData.m_isLoadData)
			{
				return GameData.m_Data.Save();
			}
			return null;
		}

		// Token: 0x060000AB RID: 171 RVA: 0x00004504 File Offset: 0x00002704
		public static bool Load(byte[] data_)
		{
			GameData.m_isLoadData = true;
			return GameData.m_Data.Load(data_);
		}

		// Token: 0x060000AC RID: 172 RVA: 0x00004518 File Offset: 0x00002718
		internal static bool IsLoadData()
		{
			return GameData.m_isLoadData;
		}

		// Token: 0x060000AD RID: 173 RVA: 0x00004520 File Offset: 0x00002720
		internal static GAME_SAVE_DATA GetLoadData()
		{
			GameData.m_isLoadData = false;
			GameData.m_isLockGameData = true;
			GameData.m_Param.Copy(GameData.m_Data.LoadData.m_Param);
			GameData.m_GameRead.Copy(GameData.m_Data.LoadData.m_Read);
			GameData.LockGameData();
			return GameData.m_Data.LoadData;
		}

		// Token: 0x060000AE RID: 174 RVA: 0x0000457C File Offset: 0x0000277C
		internal static GameParam GetSaveParam()
		{
			GameParam gameParam = new GameParam();
			gameParam.Copy(GameData.LockParam);
			gameParam.Set("firstname", GameData.GetFirstName());
            gameParam.Set("familyname", GameData.GetLastName());
            return gameParam;
		}

		// Token: 0x060000AF RID: 175 RVA: 0x000045AC File Offset: 0x000027AC
		public static void MemoryMode_Create(string route, string stay, string ks, string label, string nameEvent)
		{
			string firstName = GameData.GetFirstName();
			GameData.Init();
			GameData.SetFirstName(firstName);
			GameData.StartVoiceCheck();
			GameData.SetParamString("route", string.Empty, route);
			GameData.SetParamString("stay", string.Empty, stay);
			GameData.SetParamString("memory", string.Empty, nameEvent);
			GameData.m_memory = true;
			GameData.m_memoryKs = ks;
			GameData.m_memoryLabel = ((label.Length != 0) ? label.Substring(1) : string.Empty);
		}

		// Token: 0x060000B0 RID: 176 RVA: 0x00004630 File Offset: 0x00002830
		public static void MemoryMode_Destroy()
		{
			GameData.m_memory = false;
			GameData.m_memoryKs = string.Empty;
			GameData.m_memoryLabel = string.Empty;
		}

		// Token: 0x060000B1 RID: 177 RVA: 0x0000464C File Offset: 0x0000284C
		public static bool MemoryMode_IsActive()
		{
			return GameData.m_memory;
		}

		// Token: 0x1700001B RID: 27
		// (get) Token: 0x060000B2 RID: 178 RVA: 0x00004654 File Offset: 0x00002854
		public static string MemoryKs
		{
			get
			{
				return GameData.m_memoryKs;
			}
		}

		// Token: 0x1700001C RID: 28
		// (get) Token: 0x060000B3 RID: 179 RVA: 0x0000465C File Offset: 0x0000285C
		public static string MemoryLabel
		{
			get
			{
				return GameData.m_memoryLabel;
			}
		}

		// Token: 0x040000ED RID: 237
		private static Read m_GameRead = new Read();

		// Token: 0x040000EE RID: 238
		private static bool m_isLockGameData = false;

		// Token: 0x040000EF RID: 239
		private static SaveData m_Data = new SaveData();

		// Token: 0x040000F0 RID: 240
		private static GameParam m_Param = new GameParam();

		// Token: 0x040000F1 RID: 241
		private static GameParam m_LockParam = new GameParam();

		// Token: 0x040000F2 RID: 242
		private static List<KsWorkLog> m_OldWorkList = new List<KsWorkLog>();

		// Token: 0x040000F3 RID: 243
		private static bool m_isChangeParam = false;

		// Token: 0x040000F4 RID: 244
		private static bool m_isMoveTitle = false;

		// Token: 0x040000F5 RID: 245
		private static string m_strLastMessage = string.Empty;

		// Token: 0x040000F6 RID: 246
		private static bool m_isLoadData;

		// Token: 0x040000F7 RID: 247
		private static bool m_memory;

		// Token: 0x040000F8 RID: 248
		private static string m_memoryKs;

		// Token: 0x040000F9 RID: 249
		private static string m_memoryLabel;
	}
}
