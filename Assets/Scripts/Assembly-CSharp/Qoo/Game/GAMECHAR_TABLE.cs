﻿using System;

namespace Qoo.Game
{
	// Token: 0x02000019 RID: 25
	public struct GAMECHAR_TABLE
	{
		// Token: 0x0600006D RID: 109 RVA: 0x00003AA0 File Offset: 0x00001CA0
		public GAMECHAR_TABLE(string a, string b, string c, string d, string e)
		{
			this.szName = a;
			this.szNameJFull = b;
			this.szNameJ = c;
			this.szKsName = d;
			this.szVoice = e;
		}

		// Token: 0x040000E7 RID: 231
		public string szName;

		// Token: 0x040000E8 RID: 232
		public string szNameJFull;

		// Token: 0x040000E9 RID: 233
		public string szNameJ;

		// Token: 0x040000EA RID: 234
		public string szKsName;

		// Token: 0x040000EB RID: 235
		public string szVoice;
	}
}
