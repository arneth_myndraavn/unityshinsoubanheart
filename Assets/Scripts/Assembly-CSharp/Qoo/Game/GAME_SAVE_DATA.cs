﻿using System;
using System.Collections.Generic;
using Qoo.Ks;
using Qoo.Memory;
using Qoo.Param;

namespace Qoo.Game
{
	// Token: 0x02000021 RID: 33
	public class GAME_SAVE_DATA
	{
		// Token: 0x060000ED RID: 237 RVA: 0x00005370 File Offset: 0x00003570
		public bool Ready()
		{
			this.m_Read = new Read();
			this.m_Read.Copy(GameData.ReadData);
			this.m_Param = GameData.GetSaveParam();
			this.m_Log = new KS_LOG_DATA();
			this.m_Log.Ready();
			this.m_Scene = Singleton<EventWnd>.Instance.SceneBackup;
			return true;
		}

		// Token: 0x060000EE RID: 238 RVA: 0x000053CC File Offset: 0x000035CC
		public bool Save(MemFile mem)
		{
			this.m_Header.Save(mem);
			this.m_Read.Save(mem);
			this.m_Param.Save(mem);
			this.m_Log.Save(mem);
			this.m_Scene.Save(mem);
			return true;
		}

		// Token: 0x060000EF RID: 239 RVA: 0x0000541C File Offset: 0x0000361C
		public bool Load(MemFile mem)
		{
			this.m_Header.Load(mem);
			this.m_Read = new Read();
			this.m_Read.Load(mem);
			this.m_Param = new GameParam();
			this.m_Param.Load(mem);
			this.m_Log = new KS_LOG_DATA();
			this.m_Log.Load(mem);
			this.m_Scene = new EVENTBACKUPDATA();
			this.m_Scene.Load(mem);
			return true;
		}

		// Token: 0x060000F0 RID: 240 RVA: 0x00005498 File Offset: 0x00003698
		public void Apply()
		{
			List<string> list = new List<string>();
			list.AddRange(this.m_Log.m_KsNameArray);
			foreach (SCENEPOSDATA sceneposdata in this.m_Log.m_LogData)
			{
				if (!list.Contains(Akb.GetFileInfo((int)sceneposdata.sFileNo).Name))
				{
					list.Add(Akb.GetFileInfo((int)sceneposdata.sFileNo).Name);
				}
			}
			this.m_Log.m_KsNameArray = list.ToArray();
		}

		// Token: 0x04000106 RID: 262
		public GAME_SAVE_DATA_HEADER m_Header = new GAME_SAVE_DATA_HEADER();

		// Token: 0x04000107 RID: 263
		public Read m_Read;

		// Token: 0x04000108 RID: 264
		public GameParam m_Param;

		// Token: 0x04000109 RID: 265
		public KS_LOG_DATA m_Log;

		// Token: 0x0400010A RID: 266
		public EVENTBACKUPDATA m_Scene;
	}
}
