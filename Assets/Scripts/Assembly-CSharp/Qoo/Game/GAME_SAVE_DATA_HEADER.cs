﻿using System;
using Qoo.Memory;

namespace Qoo.Game
{
	// Token: 0x02000020 RID: 32
	public class GAME_SAVE_DATA_HEADER
	{
		// Token: 0x060000EA RID: 234 RVA: 0x00005300 File Offset: 0x00003500
		public bool Save(MemFile mem)
		{
			mem.SetStringUtf16(this.m_ID);
			mem.SetInt32(this.m_iVersion);
			return true;
		}

		// Token: 0x060000EB RID: 235 RVA: 0x0000531C File Offset: 0x0000351C
		public bool Load(MemFile mem)
		{
			string stringUtf = mem.GetStringUtf16();
			if (stringUtf != this.m_ID)
			{
				return false;
			}
			int @int = mem.GetInt32();
			return @int == this.m_iVersion;
		}

		// Token: 0x04000103 RID: 259
		public const int GAME_SAVE_DATA_VERSION = 2;

		// Token: 0x04000104 RID: 260
		public string m_ID = "QOO.SAVEDATA.GAME";

		// Token: 0x04000105 RID: 261
		public int m_iVersion = 2;
	}
}
