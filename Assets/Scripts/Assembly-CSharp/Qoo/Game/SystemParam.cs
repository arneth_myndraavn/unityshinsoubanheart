﻿using System;
using System.Collections.Generic;
using Qoo.Def;
using Qoo.Memory;

namespace Qoo.Game
{
	// Token: 0x02000024 RID: 36
	public class SystemParam
	{
		// Token: 0x060000FC RID: 252 RVA: 0x00005908 File Offset: 0x00003B08
		public bool Init()
		{
			this.m_Param.Clear();
			for (int num = 0; num != 67; num++)
			{
				this.Add(num, DefSysParam.Default[(SYSTEM_IDX)num]);
			}
			this.Defualt();
			return true;
		}

		// Token: 0x060000FD RID: 253 RVA: 0x00005950 File Offset: 0x00003B50
		public bool Defualt()
		{
			foreach (KeyValuePair<SYSTEM_IDX, SystemParam.PARAM_DATA> keyValuePair in this.m_Param)
			{
				keyValuePair.Value.SetDefault();
			}
			return true;
		}

		// Token: 0x060000FE RID: 254 RVA: 0x000059C0 File Offset: 0x00003BC0
		public bool Add(int index_, int default_)
		{
			Debug.Assert(!this.m_Param.ContainsKey((SYSTEM_IDX)index_), "ERROR:SystemParam:Add:すでにそのインデックスは登録済みです");
			this.m_Param.Add((SYSTEM_IDX)index_, new SystemParam.PARAM_DATA(default_, default_, true));
			return true;
		}

		// Token: 0x060000FF RID: 255 RVA: 0x000059FC File Offset: 0x00003BFC
		public bool Add(int index_)
		{
			Debug.Assert(!this.m_Param.ContainsKey((SYSTEM_IDX)index_), "ERROR:SystemParam:Add:すでにそのインデックスは登録済みです");
			this.m_Param.Add((SYSTEM_IDX)index_, new SystemParam.PARAM_DATA(0, 0, false));
			return true;
		}

		// Token: 0x06000100 RID: 256 RVA: 0x00005A38 File Offset: 0x00003C38
		public int Get(int index_)
		{
			Debug.Assert(this.m_Param.ContainsKey((SYSTEM_IDX)index_), "ERROR:SystemParam:Get:指定インデックスは、存在しません");
			return this.m_Param[(SYSTEM_IDX)index_].Param;
		}

		// Token: 0x06000101 RID: 257 RVA: 0x00005A70 File Offset: 0x00003C70
		public bool Set(int index_, int param_)
		{
			Debug.Assert(this.m_Param.ContainsKey((SYSTEM_IDX)index_), "ERROR:SystemParam:Set:指定インデックスは、存在しません");
			SystemParam.PARAM_DATA value = this.m_Param[(SYSTEM_IDX)index_];
			value.Param = param_;
			this.m_Param[(SYSTEM_IDX)index_] = value;
			return true;
		}

		// Token: 0x06000102 RID: 258 RVA: 0x00005AB8 File Offset: 0x00003CB8
		public bool Save(MemFile mem)
		{
			mem.SetInt32(this.m_Param.Count);
			foreach (KeyValuePair<SYSTEM_IDX, SystemParam.PARAM_DATA> keyValuePair in this.m_Param)
			{
				mem.SetInt32(keyValuePair.Value.Param);
			}
			return true;
		}

		// Token: 0x06000103 RID: 259 RVA: 0x00005B40 File Offset: 0x00003D40
		public bool Load(MemFile mem)
		{
			int @int = mem.GetInt32();
			if (@int == this.m_Param.Count)
			{
				for (int num = 0; num != this.m_Param.Count; num++)
				{
					this.Set(num, mem.GetInt32());
				}
			}
			return false;
		}

		// Token: 0x04000110 RID: 272
		private Dictionary<SYSTEM_IDX, SystemParam.PARAM_DATA> m_Param = new Dictionary<SYSTEM_IDX, SystemParam.PARAM_DATA>();

		// Token: 0x02000025 RID: 37
		private struct PARAM_DATA
		{
			// Token: 0x06000104 RID: 260 RVA: 0x00005B90 File Offset: 0x00003D90
			public PARAM_DATA(int param_, int default_, bool IsDefault_ = true)
			{
				this.Param = param_;
				this.Default = default_;
				this.IsDefault = IsDefault_;
			}

			// Token: 0x06000105 RID: 261 RVA: 0x00005BA8 File Offset: 0x00003DA8
			public void SetDefault()
			{
				if (this.IsDefault)
				{
					this.Param = this.Default;
				}
			}

			// Token: 0x04000111 RID: 273
			public int Param;

			// Token: 0x04000112 RID: 274
			public int Default;

			// Token: 0x04000113 RID: 275
			public bool IsDefault;
		}
	}
}
