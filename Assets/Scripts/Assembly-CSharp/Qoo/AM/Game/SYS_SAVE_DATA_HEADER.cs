﻿using System;
using Qoo.Memory;

namespace Qoo.AM.Game
{
	// Token: 0x02000026 RID: 38
	public class SYS_SAVE_DATA_HEADER
	{
		// Token: 0x06000107 RID: 263 RVA: 0x00005BE0 File Offset: 0x00003DE0
		public bool Save(MemFile mem)
		{
			mem.SetStringUtf16(this.m_ID);
			mem.SetInt32(this.m_iVersion);
			return true;
		}

		// Token: 0x06000108 RID: 264 RVA: 0x00005BFC File Offset: 0x00003DFC
		public bool Load(MemFile mem)
		{
			string stringUtf = mem.GetStringUtf16();
			if (stringUtf != this.m_ID)
			{
				return false;
			}
			int @int = mem.GetInt32();
			return @int == this.m_iVersion;
		}

		// Token: 0x04000114 RID: 276
		public const int SYS_SAVE_DATA_VERSION = 1;

		// Token: 0x04000115 RID: 277
		public string m_ID = "QOO.SAVEDATA.SYSTEM";

		// Token: 0x04000116 RID: 278
		public int m_iVersion = 1;
	}
}
