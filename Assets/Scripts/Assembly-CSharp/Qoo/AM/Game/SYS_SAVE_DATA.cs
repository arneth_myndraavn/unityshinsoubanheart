﻿using System;
using Qoo.Game;
using Qoo.Memory;
using Qoo.Param;

namespace Qoo.AM.Game
{
	// Token: 0x02000027 RID: 39
	public class SYS_SAVE_DATA
	{
		// Token: 0x0600010A RID: 266 RVA: 0x00005C50 File Offset: 0x00003E50
		public bool Ready()
		{
			this.m_Read = SysData.ReadData;
			this.m_LookCg = SysData.LookCg;
			this.m_LookMovie = SysData.LookMovie;
			this.m_LookBgm = SysData.LookBgm;
			this.m_Param = SysData.Param;
			return true;
		}

		// Token: 0x0600010B RID: 267 RVA: 0x00005C98 File Offset: 0x00003E98
		public bool Save(MemFile mem)
		{
			this.m_Header.Save(mem);
			this.m_Read.Save(mem);
			this.m_LookCg.Save(mem);
			this.m_LookMovie.Save(mem);
			this.m_LookBgm.Save(mem);
			this.m_Param.Save(mem);
			return true;
		}

		// Token: 0x0600010C RID: 268 RVA: 0x00005CF4 File Offset: 0x00003EF4
		public bool Load(MemFile mem)
		{
			this.Ready();
			if (this.m_Header.Load(mem))
			{
				this.m_Read.Load(mem);
				this.m_LookCg.Load(mem);
				this.m_LookMovie.Load(mem);
				this.m_LookBgm.Load(mem);
				this.m_Param.Load(mem);
				return true;
			}
			return false;
		}

		// Token: 0x04000117 RID: 279
		public SYS_SAVE_DATA_HEADER m_Header = new SYS_SAVE_DATA_HEADER();

		// Token: 0x04000118 RID: 280
		public Read m_Read;

		// Token: 0x04000119 RID: 281
		public Look m_LookCg;

		// Token: 0x0400011A RID: 282
		public Look m_LookMovie;

		// Token: 0x0400011B RID: 283
		public Look m_LookBgm;

		// Token: 0x0400011C RID: 284
		public SystemParam m_Param;
	}
}
