﻿using System;
using Qoo.Memory;

namespace Qoo.AM.Game
{
	// Token: 0x02000028 RID: 40
	public class SysSaveData
	{
		// Token: 0x17000022 RID: 34
		// (get) Token: 0x0600010E RID: 270 RVA: 0x00005D74 File Offset: 0x00003F74
		public byte[] Data
		{
			get
			{
				return this.m_Data.Data.ToArray();
			}
		}

		// Token: 0x17000023 RID: 35
		// (get) Token: 0x0600010F RID: 271 RVA: 0x00005D88 File Offset: 0x00003F88
		public SYS_SAVE_DATA LoadData
		{
			get
			{
				return this.m_SysData;
			}
		}

		// Token: 0x06000110 RID: 272 RVA: 0x00005D90 File Offset: 0x00003F90
		public byte[] Save()
		{
			this.m_Data.Clear();
			this.m_SysData = new SYS_SAVE_DATA();
			this.m_SysData.Ready();
			if (this.m_SysData.Save(this.m_Data))
			{
				return this.m_Data.Data.ToArray();
			}
			return null;
		}

		// Token: 0x06000111 RID: 273 RVA: 0x00005DE8 File Offset: 0x00003FE8
		public bool Load(byte[] data_)
		{
			if (data_ != null && data_.Length > 0)
			{
				this.m_Data = new MemFile(data_);
				this.m_SysData = new SYS_SAVE_DATA();
				this.m_SysData.Ready();
				return this.m_SysData.Load(this.m_Data);
			}
			return false;
		}

		// Token: 0x0400011D RID: 285
		private MemFile m_Data = new MemFile(null);

		// Token: 0x0400011E RID: 286
		private SYS_SAVE_DATA m_SysData;
	}
}
