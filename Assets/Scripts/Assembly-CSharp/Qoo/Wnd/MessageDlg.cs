﻿using System;
using System.Collections;
using Qoo.Input;
using Qoo.SoundSystem;
using UnityEngine;

namespace Qoo.Wnd
{
	// Token: 0x020000A4 RID: 164
	public class MessageDlg
	{
		// Token: 0x170000AD RID: 173
		// (get) Token: 0x060004BF RID: 1215 RVA: 0x000119A8 File Offset: 0x0000FBA8
		// (set) Token: 0x060004C0 RID: 1216 RVA: 0x000119B0 File Offset: 0x0000FBB0
		public int ReturnCode { get; private set; }

		// Token: 0x060004C1 RID: 1217 RVA: 0x000119BC File Offset: 0x0000FBBC
		public static IEnumerator ExecDlg(string msg, Action<bool> action = null)
		{
			string[] msgs = new string[]
			{
				msg
			};
			MessageDlg dlg = new MessageDlg();
			yield return dlg.Exec(msgs, action);
			yield return 0;
			dlg = null;
			yield break;
		}

		// Token: 0x060004C2 RID: 1218 RVA: 0x000119EC File Offset: 0x0000FBEC
		public static IEnumerator ExecDlg(string[] msgs, Action<bool> action = null)
		{
			MessageDlg dlg = new MessageDlg();
			yield return dlg.Exec(msgs, action);
			yield return 0;
			dlg = null;
			yield break;
		}

		// Token: 0x060004C3 RID: 1219 RVA: 0x00011A1C File Offset: 0x0000FC1C
		private IEnumerator Exec(string[] msgs, Action<bool> action = null)
		{
			KsInput.Pause = false;
			this.Start(msgs, action != null);
			UnityApp.Input.Clear();
			UnityApp.Input.IsOnePushDelete = true;
			while (this.Update())
			{
				yield return 0;
			}
			if (this.m_Btn != null)
			{
				Sound.SE_Ok();
				for (int i = 0; i != this.m_Btn.Length; i++)
				{
					if (this.ReturnCode == i)
					{
						this.m_Btn[i].SetPtnPos(0, 1);
					}
					else
					{
						this.m_Btn[i].SetPtnPos(0, 0);
					}
				}
				int start = Singleton<UnityApp>.Instance.GetTimeMilli();
				for (int now = Singleton<UnityApp>.Instance.GetTimeMilli(); now < start + 1000; now = Singleton<UnityApp>.Instance.GetTimeMilli())
				{
					yield return 0;
				}
			}
			yield return 0;
			this.End();
			yield return 0;
			if (action != null)
			{
				action(this.ReturnCode == 0);
			}
			KsInput.Pause = true;
			yield break;
		}

		// Token: 0x060004C4 RID: 1220 RVA: 0x00011A54 File Offset: 0x0000FC54
		private bool Start(string[] msgs, bool isBtn)
		{
			int num = 480;
			int num2 = 240;
			this.m_Base = Man2D.Sprite("sys_dialog");
			this.m_Base.SetPos(num, num2, 1500);
			this.m_Base.SetCenterPosH();
			this.m_Base.SetCenterPosV();
			num = this.m_Base.x;
			num2 = this.m_Base.y;
			int w = this.m_Base.w;
			this.m_Msg = new UnityTextSprite[msgs.Length];
			int num3 = 0;
			foreach (string str_ in msgs)
			{
				UnityTextSprite unityTextSprite = Man2D.TextSprite(null);
				unityTextSprite.AddText(str_, 22);
				unityTextSprite.SetPosition(num + w / 2, num2 + 40 + 22 * num3, UnityTextSprite.PositionType.Center, UnityTextSprite.PositionType.TopLeft);
				unityTextSprite.z = 1501;
				this.m_Msg[num3++] = unityTextSprite;
			}
			if (isBtn)
			{
				this.m_Btn = new UnitySprite[2];
				this.m_Btn[0] = Man2D.Sprite("dlog_btn_y");
				this.m_Btn[0].SetPos(num + 176, num2 + 95, 1501);
				this.m_Btn[0].SetPtnNum(1, 2);
				this.m_Btn[0].SetPtnPos(0, 0);
				this.m_Btn[1] = Man2D.Sprite("dlog_btn_n");
				this.m_Btn[1].SetPos(num + 377, num2 + 95, 1501);
				this.m_Btn[1].SetPtnNum(1, 2);
				this.m_Btn[1].SetPtnPos(0, 0);
			}
			else
			{
				this.m_Btn = null;
			}
			Singleton<UnityGraph>.Instance.Effect(string.Empty, 1f);
			this.elapsed = 0f;
			return true;
		}

		// Token: 0x060004C5 RID: 1221 RVA: 0x00011C1C File Offset: 0x0000FE1C
		private bool Update()
		{
			if (this.m_Btn != null)
			{
				switch (UnityApp.Input.Status)
				{
				case INPUT_STATUS.PUSH:
					foreach (UnitySprite unitySprite in this.m_Btn)
					{
						if (unitySprite.IsHit(UnityApp.Input.Start))
						{
							unitySprite.SetPtnPos(0, 1);
						}
					}
					break;
				case INPUT_STATUS.CLICK:
					foreach (UnitySprite unitySprite2 in this.m_Btn)
					{
						unitySprite2.SetPtnPos(0, 0);
					}
					if (this.m_Btn[0].IsHit(UnityApp.Input.End))
					{
						this.ReturnCode = 0;
						this.m_Btn[0].SetPtnPos(0, 1);
						return false;
					}
					if (this.m_Btn[1].IsHit(UnityApp.Input.End))
					{
						this.ReturnCode = 1;
						this.m_Btn[0].SetPtnPos(0, 1);
						return false;
					}
					if (this.m_Base.IsHit(UnityApp.Input.End))
					{
					}
					break;
				case INPUT_STATUS.DRAG_ON:
					break;
				default:
					foreach (UnitySprite unitySprite3 in this.m_Btn)
					{
						unitySprite3.SetPtnPos(0, 0);
					}
					break;
				}
			}
			else
			{
				this.elapsed += Time.deltaTime;
				if (this.elapsed > 1f)
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x060004C6 RID: 1222 RVA: 0x00011DBC File Offset: 0x0000FFBC
		private bool End()
		{
			if (Singleton<Man2D>.IsReady)
			{
				Singleton<Man2D>.Instance.RemoveSprite(this.m_Base);
				foreach (UnityTextSprite sp in this.m_Msg)
				{
					Singleton<Man2D>.Instance.RemoveSprite(sp);
				}
				if (this.m_Btn != null)
				{
					if (this.m_Btn[0] != null)
					{
						Singleton<Man2D>.Instance.RemoveSprite(this.m_Btn[0]);
					}
					if (this.m_Btn[1] != null)
					{
						Singleton<Man2D>.Instance.RemoveSprite(this.m_Btn[1]);
					}
				}
				this.m_Base = null;
				this.m_Msg = null;
				this.m_Btn = null;
				Singleton<UnityGraph>.Instance.Effect(string.Empty, 1f);
			}
			return true;
		}

		// Token: 0x04000377 RID: 887
		private UnitySprite m_Base;

		// Token: 0x04000378 RID: 888
		private UnityTextSprite[] m_Msg;

		// Token: 0x04000379 RID: 889
		private UnitySprite[] m_Btn;

		// Token: 0x0400037A RID: 890
		private float elapsed;
	}
}
