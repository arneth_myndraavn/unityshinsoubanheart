﻿using System;
using Game;
using Qoo.Game;

namespace Qoo.SoundSystem
{
	// Token: 0x020000A1 RID: 161
	public static class Sound
	{
		// Token: 0x0600049B RID: 1179 RVA: 0x00011590 File Offset: 0x0000F790
		public static void BgmPlay(string name, float fVol = 1f, int nInTime = 1000, int nOutTime = 1000)
		{
			UnityApp.Sound.BgmPlay(name, fVol * Sound.GetVolumeBgm(), nInTime, nOutTime);
		}

		// Token: 0x0600049C RID: 1180 RVA: 0x000115A8 File Offset: 0x0000F7A8
		public static void BgmStop(int time = 1000)
		{
			UnityApp.Sound.BgmStop(time);
		}

		// Token: 0x0600049D RID: 1181 RVA: 0x000115B8 File Offset: 0x0000F7B8
		internal static bool BgmIsPlay()
		{
			return UnityApp.Sound.BgmIsPlay();
		}

		// Token: 0x0600049E RID: 1182 RVA: 0x000115C4 File Offset: 0x0000F7C4
		public static void BgmApplyVolume()
		{
			UnityApp.Sound.BgmSetVolume(Sound.GetVolumeBgm());
		}

		// Token: 0x0600049F RID: 1183 RVA: 0x000115D8 File Offset: 0x0000F7D8
		public static void SeSlotPlay(int slot, string name, bool bLoop, float vol)
		{
			UnityApp.Sound.SlotSePlay(slot, name, vol * Sound.GetVolumeSe(), bLoop);
		}

		// Token: 0x060004A0 RID: 1184 RVA: 0x000115F0 File Offset: 0x0000F7F0
		public static void SeSlotStop(int slot)
		{
			UnityApp.Sound.SlotSeStop(slot, 0);
		}

		// Token: 0x060004A1 RID: 1185 RVA: 0x00011600 File Offset: 0x0000F800
		public static void SeSlotStopAll()
		{
			UnityApp.Sound.SlotSeStopAll(1000);
		}

		// Token: 0x060004A2 RID: 1186 RVA: 0x00011614 File Offset: 0x0000F814
		public static void SeSlotApplyVolume()
		{
			UnityApp.Sound.SlotSeSetVolume(0, Sound.GetVolumeSe());
			UnityApp.Sound.SlotSeSetVolume(1, Sound.GetVolumeSe());
		}

		// Token: 0x060004A3 RID: 1187 RVA: 0x00011644 File Offset: 0x0000F844
		public static void VoicePlay(string name, float fVol = 1f)
		{
			Debug.Print("VOICE:" + name);
			UnityApp.Sound.VoicePlay(name, fVol * Sound.GetVolumeVoice(name));
		}

		// Token: 0x060004A4 RID: 1188 RVA: 0x00011674 File Offset: 0x0000F874
		public static void VoiceStop()
		{
			UnityApp.Sound.VoiceStop();
		}

		// Token: 0x060004A5 RID: 1189 RVA: 0x00011680 File Offset: 0x0000F880
		internal static bool VoiceIsPlay()
		{
			return UnityApp.Sound.VoiceIsPlay();
		}

		// Token: 0x060004A6 RID: 1190 RVA: 0x0001168C File Offset: 0x0000F88C
		public static void SysSePlay(string name, float fVol = 1f)
		{
			UnityApp.Sound.SysSePlay(name, fVol * Sound.GetSeVolumeSys());
		}

		// Token: 0x060004A7 RID: 1191 RVA: 0x000116A0 File Offset: 0x0000F8A0
		public static void SysPlayerSePlay(string name, float fVol = 1f)
		{
			UnityApp.Sound.SysPlayerSePlay(name, fVol * Sound.GetSeVolumeSys());
		}

		// Token: 0x060004A8 RID: 1192 RVA: 0x000116B4 File Offset: 0x0000F8B4
		internal static bool SysPlayerSeIsPlay()
		{
			return UnityApp.Sound.SysPlayerSeIsPlay();
		}

		// Token: 0x060004A9 RID: 1193 RVA: 0x000116C0 File Offset: 0x0000F8C0
		internal static void SE_Ok()
		{
			Sound.SysSePlay("sysse_decide", 1f);
		}

		// Token: 0x060004AA RID: 1194 RVA: 0x000116D4 File Offset: 0x0000F8D4
		internal static void SE_Select()
		{
			Sound.SysSePlay("sysse_hover", 1f);
		}

		// Token: 0x060004AB RID: 1195 RVA: 0x000116E8 File Offset: 0x0000F8E8
		internal static void SE_Cancel()
		{
			Sound.SysSePlay("sysse_back", 1f);
		}

		// Token: 0x060004AC RID: 1196 RVA: 0x000116FC File Offset: 0x0000F8FC
		public static float GetVolumeBgm()
		{
			return (float)SysData.GetVolumeBgm() * 0.2f;
		}

		// Token: 0x060004AD RID: 1197 RVA: 0x0001170C File Offset: 0x0000F90C
		private static float GetVolumeSe()
		{
			return (float)SysData.GetVolumeSe() * 0.2f;
		}

		// Token: 0x060004AE RID: 1198 RVA: 0x0001171C File Offset: 0x0000F91C
		private static float GetSeVolumeSys()
		{
			return (float)SysData.GetVolumeSys() * 0.2f;
		}

		// Token: 0x060004AF RID: 1199 RVA: 0x0001172C File Offset: 0x0000F92C
		private static float GetVolumeVoice(string nameVoice)
		{
			if (!SysData.IsVoiceEnable())
			{
				return 0f;
			}
			if (GameData.IsFullVoiceCheck())
			{
				if (!SysData.IsPayVoice(CHAR_ID.NIGHTMARE))
				{
					return 0f;
				}
			}
			else if (GameData.IsVoiceCheck() && !SysData.IsPayVoice(GameData.GetRoute()))
			{
				return 0f;
			}
			CHAR_ID idFromVoice = Chara.GetIdFromVoice(nameVoice);
			return (float)SysData.GetVoiceVolume(idFromVoice) * 0.2f;
		}
	}
}
