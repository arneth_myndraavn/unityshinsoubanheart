﻿using System;
using System.IO;
using Qoo.Memory;
using UnityEngine;

namespace Qoo
{
	// Token: 0x0200000A RID: 10
	public static class Debug
	{
		// Token: 0x06000021 RID: 33 RVA: 0x0000231C File Offset: 0x0000051C
		static Debug()
		{
			if (UnityEngine.Debug.isDebugBuild)
			{
				Debug.m_IsDebug = true;
			}
		}

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x06000022 RID: 34 RVA: 0x00002330 File Offset: 0x00000530
		public static bool IsDebug
		{
			get
			{
				return Debug.m_IsDebug;
			}
		}

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x06000023 RID: 35 RVA: 0x00002338 File Offset: 0x00000538
		// (set) Token: 0x06000024 RID: 36 RVA: 0x00002340 File Offset: 0x00000540
		public static bool IsAutoKsDebug
		{
			get
			{
				return Debug.m_isAutoKsDebug;
			}
			set
			{
				if (Debug.IsDebug)
				{
					Debug.m_isAutoKsDebug = value;
				}
			}
		}

		// Token: 0x06000025 RID: 37 RVA: 0x00002354 File Offset: 0x00000554
		public static void Print(string message)
		{
			if (Debug.IsDebug)
			{
				UnityEngine.Debug.Log(message);
			}
		}

		// Token: 0x06000026 RID: 38 RVA: 0x00002368 File Offset: 0x00000568
		public static void Assert(bool assert)
		{
			if (Debug.IsDebug && !assert)
			{
				Debug.Print("ASSERT!!");
				UnityEngine.Debug.DebugBreak();
			}
		}

		// Token: 0x06000027 RID: 39 RVA: 0x0000238C File Offset: 0x0000058C
		public static void Assert(bool assert, string message)
		{
			if (Debug.IsDebug && !assert)
			{
				Debug.Print(message);
				UnityEngine.Debug.DebugBreak();
			}
		}

		// Token: 0x06000028 RID: 40 RVA: 0x000023AC File Offset: 0x000005AC
		public static void PrintLog(string path, string message)
		{
			MemFile memFile = new MemFile(null);
			memFile.SetStringUtf16(message);
			FileStream fileStream = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite);
			fileStream.Seek(0L, SeekOrigin.End);
			fileStream.Write(memFile.Data.ToArray(), 0, memFile.Data.Count);
			fileStream.Flush();
			fileStream.Close();
		}

		// Token: 0x04000017 RID: 23
		private static bool m_IsDebug;

		// Token: 0x04000018 RID: 24
		private static bool m_isAutoKsDebug;
	}
}
