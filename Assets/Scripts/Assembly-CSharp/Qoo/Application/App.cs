﻿using System;
using Game;
using Qoo.Ks;
using Qoo.Message;
using Qoo.Select;

namespace Qoo.Application
{
	// Token: 0x02000009 RID: 9
	public static class App
	{
		// Token: 0x06000013 RID: 19 RVA: 0x00002258 File Offset: 0x00000458
		static App()
		{
			App.rnd = new Random();
		}

		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000014 RID: 20 RVA: 0x000022A0 File Offset: 0x000004A0
		// (set) Token: 0x06000015 RID: 21 RVA: 0x000022A8 File Offset: 0x000004A8
		public static int AdvMode { get; set; }

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x06000016 RID: 22 RVA: 0x000022B0 File Offset: 0x000004B0
		// (set) Token: 0x06000017 RID: 23 RVA: 0x000022B8 File Offset: 0x000004B8
		public static bool IsInit
		{
			get
			{
				return App.m_bInit;
			}
			private set
			{
				App.m_bInit = value;
			}
		}

		// Token: 0x06000018 RID: 24 RVA: 0x000022C0 File Offset: 0x000004C0
		public static bool Init()
		{
			GameInitilize.Init();
			App.IsInit = true;
			return true;
		}

		// Token: 0x06000019 RID: 25 RVA: 0x000022D0 File Offset: 0x000004D0
		public static void FrameUpdate(bool bFrame)
		{
			UnityTask.FrameUpdate();
		}

		// Token: 0x0600001A RID: 26 RVA: 0x000022D8 File Offset: 0x000004D8
		public static int GetTimeMilli()
		{
			return Singleton<UnityApp>.Instance.GetTimeMilli();
		}

		// Token: 0x0600001B RID: 27 RVA: 0x000022E4 File Offset: 0x000004E4
		public static int GetRandom(int max)
		{
			return App.rnd.Next(max);
		}

		// Token: 0x0600001C RID: 28 RVA: 0x000022F4 File Offset: 0x000004F4
		public static void SetAdvMode(int iMode)
		{
			App.AdvMode = iMode;
		}

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x0600001D RID: 29 RVA: 0x000022FC File Offset: 0x000004FC
		public static KsData QooKsData
		{
			get
			{
				return App.m_KsData;
			}
		}

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x0600001E RID: 30 RVA: 0x00002304 File Offset: 0x00000504
		public static KsLog QooKsLog
		{
			get
			{
				return App.m_KsLog;
			}
		}

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x0600001F RID: 31 RVA: 0x0000230C File Offset: 0x0000050C
		public static BackLog QooBackLog
		{
			get
			{
				return App.m_BackLog;
			}
		}

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x06000020 RID: 32 RVA: 0x00002314 File Offset: 0x00000514
		public static SceneSelect QooSelect
		{
			get
			{
				return App.m_Select;
			}
		}

		// Token: 0x04000010 RID: 16
		private static bool m_bInit = false;

		// Token: 0x04000011 RID: 17
		private static Random rnd;

		// Token: 0x04000012 RID: 18
		private static KsData m_KsData = new KsData();

		// Token: 0x04000013 RID: 19
		private static KsLog m_KsLog = new KsLog();

		// Token: 0x04000014 RID: 20
		private static BackLog m_BackLog = new BackLog();

		// Token: 0x04000015 RID: 21
		private static SceneSelect m_Select = new SceneSelect();
	}
}
