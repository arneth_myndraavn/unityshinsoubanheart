﻿using System;
using System.Collections.Generic;

namespace Qoo.Message
{
	// Token: 0x020000A7 RID: 167
	public class BackLog
	{
		// Token: 0x170000B2 RID: 178
		// (get) Token: 0x060004E6 RID: 1254 RVA: 0x0001239C File Offset: 0x0001059C
		public bool Empty
		{
			get
			{
				return this.GetNum() == 0;
			}
		}

		// Token: 0x060004E7 RID: 1255 RVA: 0x000123A8 File Offset: 0x000105A8
		public void Init()
		{
			this.Clear();
			this.m_isEnable = true;
		}

		// Token: 0x060004E8 RID: 1256 RVA: 0x000123B8 File Offset: 0x000105B8
		public void Clear()
		{
			this.LogList.Clear();
		}

		// Token: 0x060004E9 RID: 1257 RVA: 0x000123C8 File Offset: 0x000105C8
		public int GetNum()
		{
			return this.LogList.Count;
		}

		// Token: 0x060004EA RID: 1258 RVA: 0x000123D8 File Offset: 0x000105D8
		public void SetMesNo(int nKs, int nLabel, int nMes)
		{
			this.m_nKsNo = nKs;
			this.m_nLabelNo = nLabel;
			this.m_nMesNo = nMes;
		}

		// Token: 0x060004EB RID: 1259 RVA: 0x000123F0 File Offset: 0x000105F0
		public bool AddMessage(string text, string name, string voice)
		{
			if (!this.m_isEnable)
			{
				return false;
			}
			if (this.LogList.Count > 0 && this.LogList[this.LogList.Count - 1].IsEqual(this.m_nKsNo, this.m_nLabelNo, this.m_nMesNo))
			{
				this.LogList[this.LogList.Count - 1] = new BACKLOG_ITEM(this.m_nKsNo, this.m_nLabelNo, this.m_nMesNo, voice, name, text);
				return true;
			}
			this.LogList.Add(new BACKLOG_ITEM(this.m_nKsNo, this.m_nLabelNo, this.m_nMesNo, voice, name, text));
			if (this.LogList.Count - 64 > 0)
			{
				this.LogList.RemoveRange(0, this.LogList.Count - 64);
			}
			return true;
		}

		// Token: 0x060004EC RID: 1260 RVA: 0x000124D8 File Offset: 0x000106D8
		public void RemoveMessage(int nKsNo, int nLabelNo, int nMesNo)
		{
			while (this.m_isEnable)
			{
				if (this.LogList.Count == 0)
				{
					break;
				}
				if (!this.LogList[this.LogList.Count - 1].IsEqual(nKsNo, nLabelNo, nMesNo))
				{
					break;
				}
				this.LogList.RemoveAt(this.LogList.Count - 1);
			}
		}

		// Token: 0x060004ED RID: 1261 RVA: 0x0001254C File Offset: 0x0001074C
		public BACKLOG_ITEM[] GetRenge(int max)
		{
			int count = (this.LogList.Count <= max) ? this.LogList.Count : max;
			int index = (this.LogList.Count <= max) ? 0 : (this.LogList.Count - max);
			return this.LogList.GetRange(index, count).ToArray();
		}

		// Token: 0x04000381 RID: 897
		private List<BACKLOG_ITEM> LogList = new List<BACKLOG_ITEM>();

		// Token: 0x04000382 RID: 898
		private int m_nKsNo;

		// Token: 0x04000383 RID: 899
		private int m_nLabelNo;

		// Token: 0x04000384 RID: 900
		private int m_nMesNo;

		// Token: 0x04000385 RID: 901
		private bool m_isEnable;
	}
}
