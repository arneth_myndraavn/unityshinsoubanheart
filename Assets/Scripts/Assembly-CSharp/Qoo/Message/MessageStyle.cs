﻿using System;
using System.Collections.Generic;
using Qoo.Def;

namespace Qoo.Message
{
	// Token: 0x020000B1 RID: 177
	public static class MessageStyle
	{
		// Token: 0x06000525 RID: 1317 RVA: 0x00013B10 File Offset: 0x00011D10
		public static bool AddStyle(MSGWND_STYLE style, string nameStyle, string nameFrameCg, MSGWND_STYLE_DATA data)
		{
			MessageStyle.DicStyleData.Add(style, data);
			MessageStyle.DicFrameCg.Add(style, nameFrameCg);
			MessageStyle.DicNameTable.Add(nameStyle, style);
			return true;
		}

		// Token: 0x06000526 RID: 1318 RVA: 0x00013B38 File Offset: 0x00011D38
		public static bool Clear()
		{
			MessageStyle.DicStyleData.Clear();
			MessageStyle.DicFrameCg.Clear();
			MessageStyle.DicNameTable.Clear();
			return true;
		}

		// Token: 0x06000527 RID: 1319 RVA: 0x00013B5C File Offset: 0x00011D5C
		public static MSGWND_STYLE_DATA GetData(MSGWND_STYLE style)
		{
			return MessageStyle.DicStyleData[style];
		}

		// Token: 0x06000528 RID: 1320 RVA: 0x00013B6C File Offset: 0x00011D6C
		public static MSGWND_STYLE GetStyle(string style_name)
		{
			return MessageStyle.DicNameTable[style_name];
		}

		// Token: 0x06000529 RID: 1321 RVA: 0x00013B7C File Offset: 0x00011D7C
		public static string GetFrameCgName(MSGWND_STYLE style)
		{
			return MessageStyle.DicFrameCg[style];
		}

		// Token: 0x040003D1 RID: 977
		private static Dictionary<MSGWND_STYLE, MSGWND_STYLE_DATA> DicStyleData = new Dictionary<MSGWND_STYLE, MSGWND_STYLE_DATA>();

		// Token: 0x040003D2 RID: 978
		private static Dictionary<MSGWND_STYLE, string> DicFrameCg = new Dictionary<MSGWND_STYLE, string>();

		// Token: 0x040003D3 RID: 979
		private static Dictionary<string, MSGWND_STYLE> DicNameTable = new Dictionary<string, MSGWND_STYLE>();
	}
}
