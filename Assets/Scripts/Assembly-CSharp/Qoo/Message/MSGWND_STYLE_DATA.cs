﻿using System;
using UnityEngine;

namespace Qoo.Message
{
	// Token: 0x020000B0 RID: 176
	public class MSGWND_STYLE_DATA
	{
		// Token: 0x06000521 RID: 1313 RVA: 0x0001398C File Offset: 0x00011B8C
		public MSGWND_STYLE_DATA()
		{
			this.posName = new Point3[2];
		}

		// Token: 0x06000522 RID: 1314 RVA: 0x000139A0 File Offset: 0x00011BA0
		public MSGWND_STYLE_DATA(Point3 posFrm_, Point3[] posName_, Point3 posFace_, Point3 posBrk_, Size sizeBrkSplit_, int nBrkWait_, Point3 posTxt_, Size sizeTxt_, int nTxtWEx_, bool bTxtAutoRet_, Color32 colTxt_, Color32 colRefWord_, Color32 colRefBg_, Color32 colRefCur_, string BrkCg_, string BrkAutoCg_)
		{
			this.posName = new Point3[2];
			Debug.Assert(this.posName.Length == posName_.Length);
			this.posFrm = posFrm_;
			for (int num = 0; num != this.posName.Length; num++)
			{
				this.posName[num] = posName_[num];
			}
			this.posFace = posFace_;
			this.posBrk = posBrk_;
			this.sizeBrkSplit = sizeBrkSplit_;
			this.nBrkWait = nBrkWait_;
			this.posTxt = posTxt_;
			this.sizeTxt = sizeTxt_;
			this.nTxtWEx = nTxtWEx_;
			this.bTxtAutoRet = bTxtAutoRet_;
			this.colTxt = colTxt_;
			this.colRefWord = colRefWord_;
			this.colRefBg = colRefBg_;
			this.colRefCur = colRefCur_;
			this.BrkCg = BrkCg_;
			this.BrkAutoCg = BrkAutoCg_;
		}

		// Token: 0x06000523 RID: 1315 RVA: 0x00013A6C File Offset: 0x00011C6C
		internal void SetPosScale(float fX, float fY)
		{
			this.posFrm.Scale(fX, fY, 1f);
			foreach (Point3 point in this.posName)
			{
				point.Scale(fX, fY, 1f);
			}
			this.posFace.Scale(fX, fY, 1f);
			this.posBrk.Scale(fX, fY, 1f);
			this.posTxt.Scale(fX, fY, 1f);
		}

		// Token: 0x040003C1 RID: 961
		public Point3 posFrm;

		// Token: 0x040003C2 RID: 962
		public Point3[] posName;

		// Token: 0x040003C3 RID: 963
		public Point3 posFace;

		// Token: 0x040003C4 RID: 964
		public Point3 posBrk;

		// Token: 0x040003C5 RID: 965
		public Size sizeBrkSplit;

		// Token: 0x040003C6 RID: 966
		public int nBrkWait;

		// Token: 0x040003C7 RID: 967
		public Point3 posTxt;

		// Token: 0x040003C8 RID: 968
		public Size sizeTxt;

		// Token: 0x040003C9 RID: 969
		public int nTxtWEx;

		// Token: 0x040003CA RID: 970
		public bool bTxtAutoRet;

		// Token: 0x040003CB RID: 971
		public Color32 colTxt;

		// Token: 0x040003CC RID: 972
		public Color32 colRefWord;

		// Token: 0x040003CD RID: 973
		public Color32 colRefBg;

		// Token: 0x040003CE RID: 974
		public Color32 colRefCur;

		// Token: 0x040003CF RID: 975
		public string BrkCg;

		// Token: 0x040003D0 RID: 976
		public string BrkAutoCg;
	}
}
