﻿using System;

namespace Qoo.Message
{
	// Token: 0x020000A8 RID: 168
	public class BACKLOG_ITEM
	{
		// Token: 0x060004EE RID: 1262 RVA: 0x000125B4 File Offset: 0x000107B4
		public BACKLOG_ITEM(int nKs, int nLabel, int nPos, string voice_, string name_, string txt_)
		{
			this.KsNo = nKs;
			this.LabelNo = nLabel;
			this.Pos = nPos;
			this.Voice = voice_;
			this.Name = name_;
			this.Txt = txt_;
		}

		// Token: 0x170000B3 RID: 179
		// (get) Token: 0x060004EF RID: 1263 RVA: 0x000125F4 File Offset: 0x000107F4
		// (set) Token: 0x060004F0 RID: 1264 RVA: 0x000125FC File Offset: 0x000107FC
		public int KsNo { get; set; }

		// Token: 0x170000B4 RID: 180
		// (get) Token: 0x060004F1 RID: 1265 RVA: 0x00012608 File Offset: 0x00010808
		// (set) Token: 0x060004F2 RID: 1266 RVA: 0x00012610 File Offset: 0x00010810
		public int LabelNo { get; set; }

		// Token: 0x170000B5 RID: 181
		// (get) Token: 0x060004F3 RID: 1267 RVA: 0x0001261C File Offset: 0x0001081C
		// (set) Token: 0x060004F4 RID: 1268 RVA: 0x00012624 File Offset: 0x00010824
		public int Pos { get; set; }

		// Token: 0x170000B6 RID: 182
		// (get) Token: 0x060004F5 RID: 1269 RVA: 0x00012630 File Offset: 0x00010830
		// (set) Token: 0x060004F6 RID: 1270 RVA: 0x00012638 File Offset: 0x00010838
		public string Voice { get; set; }

		// Token: 0x170000B7 RID: 183
		// (get) Token: 0x060004F7 RID: 1271 RVA: 0x00012644 File Offset: 0x00010844
		// (set) Token: 0x060004F8 RID: 1272 RVA: 0x0001264C File Offset: 0x0001084C
		public string Name { get; set; }

		// Token: 0x170000B8 RID: 184
		// (get) Token: 0x060004F9 RID: 1273 RVA: 0x00012658 File Offset: 0x00010858
		// (set) Token: 0x060004FA RID: 1274 RVA: 0x00012660 File Offset: 0x00010860
		public string Txt { get; set; }

		// Token: 0x060004FB RID: 1275 RVA: 0x0001266C File Offset: 0x0001086C
		public bool IsEqual(int nKs, int nLabel, int nPos)
		{
			return this.KsNo == nKs && this.LabelNo == nLabel && this.Pos >= nPos;
		}
	}
}
