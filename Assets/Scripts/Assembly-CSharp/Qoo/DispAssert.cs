﻿using System;
using Qoo.Def;
using UnityEngine;

namespace Qoo
{
	// Token: 0x0200000C RID: 12
	public class DispAssert : MonoBehaviour
	{
		// Token: 0x17000009 RID: 9
		// (get) Token: 0x0600002D RID: 45 RVA: 0x0000248C File Offset: 0x0000068C
		// (set) Token: 0x0600002E RID: 46 RVA: 0x00002494 File Offset: 0x00000694
		public ErrorDef.ASSERT_ERROR_CODE ErrorCode
		{
			get
			{
				return this.errorCode;
			}
			set
			{
				this.errorCode = value;
			}
		}

		// Token: 0x1700000A RID: 10
		// (get) Token: 0x0600002F RID: 47 RVA: 0x000024A0 File Offset: 0x000006A0
		// (set) Token: 0x06000030 RID: 48 RVA: 0x000024A8 File Offset: 0x000006A8
		public string ErrorMessage
		{
			get
			{
				return this.errorMessage;
			}
			set
			{
				this.errorMessage = value;
			}
		}

		// Token: 0x06000031 RID: 49 RVA: 0x000024B4 File Offset: 0x000006B4
		public static void Show(ErrorDef.ASSERT_ERROR_CODE errorCode, string errorMessage)
		{
			if (DispAssert.instance != null)
			{
				return;
			}
			GameObject gameObject = new GameObject();
			DispAssert.instance = gameObject.AddComponent<DispAssert>();
			DispAssert.instance.init(errorCode, errorMessage);
		}

		// Token: 0x06000032 RID: 50 RVA: 0x000024F0 File Offset: 0x000006F0
		public static bool IsAssert()
		{
			return DispAssert.instance != null;
		}

		// Token: 0x06000033 RID: 51 RVA: 0x00002500 File Offset: 0x00000700
		private void Start()
		{
			this.guiStyle = new GUIStyle();
		}

		// Token: 0x06000034 RID: 52 RVA: 0x00002510 File Offset: 0x00000710
		private void Update()
		{
		}

		// Token: 0x06000035 RID: 53 RVA: 0x00002514 File Offset: 0x00000714
		private void OnGUI()
		{
			this.guiStyle = GUI.skin.textArea;
			this.guiStyle.fontSize = 20;
			string format = "ご迷惑をお掛けし、大変申し訳ございません。\n継続不能なエラーが発生しました。\n\nエラーコード={0}\n{1}\n\n上記のエラーコードを記載し、\n＜ArnethMyndraavn@gmail.com＞までご連絡ください。\n\nご協力のほどよろしくお願い申し上げます。";
			string str = "E";
			int num = (int)this.errorCode;
			string text = string.Format(format, str + num.ToString("0000"), this.errorMessage);
			GUI.Box(new Rect(0f, 0f, (float)Screen.width, (float)Screen.height), string.Empty);
			GUI.TextArea(new Rect(0f, (float)Screen.height / 6f, (float)Screen.width, (float)Screen.height / 6f * 4f), text, this.guiStyle);
		}

		// Token: 0x06000036 RID: 54 RVA: 0x000025CC File Offset: 0x000007CC
		private void init(ErrorDef.ASSERT_ERROR_CODE errorCode, string errorMessage)
		{
			this.ErrorCode = errorCode;
			this.ErrorMessage = errorMessage;
		}

		// Token: 0x0400001A RID: 26
		private static DispAssert instance;

		// Token: 0x0400001B RID: 27
		private ErrorDef.ASSERT_ERROR_CODE errorCode;

		// Token: 0x0400001C RID: 28
		private string errorMessage = "予期せぬエラー";

		// Token: 0x0400001D RID: 29
		private GUIStyle guiStyle;
	}
}
