﻿using System;
using System.Collections.Generic;

namespace Qoo.Table
{
	// Token: 0x020000A2 RID: 162
	internal static class DirScaleTable
	{
		// Token: 0x170000AA RID: 170
		// (get) Token: 0x060004B1 RID: 1201 RVA: 0x000117E0 File Offset: 0x0000F9E0
		// (set) Token: 0x060004B2 RID: 1202 RVA: 0x000117E8 File Offset: 0x0000F9E8
		public static SizeF Base
		{
			get
			{
				return DirScaleTable.m_Base;
			}
			set
			{
				DirScaleTable.m_Base = value;
			}
		}

		// Token: 0x170000AB RID: 171
		// (get) Token: 0x060004B3 RID: 1203 RVA: 0x000117F0 File Offset: 0x0000F9F0
		// (set) Token: 0x060004B4 RID: 1204 RVA: 0x000117F8 File Offset: 0x0000F9F8
		public static SizeF Default
		{
			get
			{
				return DirScaleTable.m_Default;
			}
			set
			{
				DirScaleTable.m_Default = value;
			}
		}

		// Token: 0x060004B5 RID: 1205 RVA: 0x00011800 File Offset: 0x0000FA00
		public static bool Add(string name, SizeF size)
		{
			if (DirScaleTable.m_dicDirTable.ContainsKey(name))
			{
				Debug.Print(string.Format("Error:{0}は、二重登録です", name));
				return false;
			}
			Debug.Print(string.Format("DirTable:Add:Name={0} w={1} h={2}", name, size.w, size.h));
			DirScaleTable.m_dicDirTable.Add(name, size);
			return true;
		}

		// Token: 0x060004B6 RID: 1206 RVA: 0x00011864 File Offset: 0x0000FA64
		public static void Clear()
		{
			DirScaleTable.m_dicDirTable.Clear();
		}

		// Token: 0x060004B7 RID: 1207 RVA: 0x00011870 File Offset: 0x0000FA70
		public static SizeF GetSize(string nameDir)
		{
			if (DirScaleTable.m_dicDirTable.ContainsKey(nameDir))
			{
				return DirScaleTable.m_dicDirTable[nameDir];
			}
			return DirScaleTable.Default;
		}

		// Token: 0x04000372 RID: 882
		private static Dictionary<string, SizeF> m_dicDirTable = new Dictionary<string, SizeF>();

		// Token: 0x04000373 RID: 883
		private static SizeF m_Default = new SizeF(1f, 1f);

		// Token: 0x04000374 RID: 884
		private static SizeF m_Base = new SizeF(480f, 272f);
	}
}
