﻿using System;
using System.Collections.Generic;

namespace Qoo.Table
{
	// Token: 0x020000A3 RID: 163
	public class SelectTable
	{
		// Token: 0x170000AC RID: 172
		// (get) Token: 0x060004B9 RID: 1209 RVA: 0x000118C0 File Offset: 0x0000FAC0
		public string Name
		{
			get
			{
				return this.m_Name;
			}
		}

		// Token: 0x060004BA RID: 1210 RVA: 0x000118C8 File Offset: 0x0000FAC8
		public bool Create(string table_name)
		{
			this.m_Name = table_name;
			this.m_dicPoint.Clear();
			return true;
		}

		// Token: 0x060004BB RID: 1211 RVA: 0x000118E0 File Offset: 0x0000FAE0
		public void Add(int iSelectNum, Point2[] pos_table)
		{
			this.m_dicPoint.Add(iSelectNum, pos_table);
		}

		// Token: 0x060004BC RID: 1212 RVA: 0x000118F0 File Offset: 0x0000FAF0
		public Point2[] GetPoint(int iSelectNum)
		{
			if (this.m_dicPoint.ContainsKey(iSelectNum))
			{
				return this.m_dicPoint[iSelectNum];
			}
			return null;
		}

		// Token: 0x060004BD RID: 1213 RVA: 0x00011914 File Offset: 0x0000FB14
		public void Scale(float fW, float fH)
		{
			foreach (KeyValuePair<int, Point2[]> keyValuePair in this.m_dicPoint)
			{
				for (int num = 0; num != keyValuePair.Value.Length; num++)
				{
					keyValuePair.Value[num].Scale(fW, fH);
				}
			}
		}

		// Token: 0x04000375 RID: 885
		private string m_Name = string.Empty;

		// Token: 0x04000376 RID: 886
		private Dictionary<int, Point2[]> m_dicPoint = new Dictionary<int, Point2[]>();
	}
}
