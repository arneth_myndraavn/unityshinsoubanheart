﻿using System;

namespace Qoo.Script
{
	// Token: 0x02000097 RID: 151
	public static class Calc
	{
		// Token: 0x0600044C RID: 1100 RVA: 0x0000F6C4 File Offset: 0x0000D8C4
		public static bool ExecCmdStr(string szCmdExp, ref int pnResult)
		{
			return Calc.m_Calc.ExecCmdStr(szCmdExp, ref pnResult);
		}

		// Token: 0x040002E5 RID: 741
		private static AmCalc m_Calc = new AmCalc();
	}
}
