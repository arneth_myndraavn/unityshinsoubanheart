﻿using System;

namespace Qoo.Script
{
	// Token: 0x02000092 RID: 146
	public enum QC_VARTYPE
	{
		// Token: 0x040002D4 RID: 724
		CONST,
		// Token: 0x040002D5 RID: 725
		GAMEPARAM,
		// Token: 0x040002D6 RID: 726
		USER
	}
}
