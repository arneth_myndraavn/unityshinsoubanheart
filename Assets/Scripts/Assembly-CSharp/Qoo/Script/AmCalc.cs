﻿using System;
using Game;
using NsQT;
using Qoo.Application;
using Qoo.Game;

namespace Qoo.Script
{
	// Token: 0x02000095 RID: 149
	internal class AmCalc : CQPocketK3
	{
		// Token: 0x06000442 RID: 1090 RVA: 0x0000F29C File Offset: 0x0000D49C
		public override void Init()
		{
			base.Init();
		}

		// Token: 0x06000443 RID: 1091 RVA: 0x0000F2A4 File Offset: 0x0000D4A4
		public virtual bool ExecCmdStr(string szCmdExp, ref int pnResult)
		{
			CQK3Node cqk3Node = new CQK3Node();
			bool flag = base.ExecCalc(szCmdExp, ref cqk3Node);
			if (flag && cqk3Node.GetNodeType() == QK3_NODETYPE.INT)
			{
				pnResult = cqk3Node.GetInt();
			}
			else
			{
				pnResult = -1;
			}
			return flag;
		}

		// Token: 0x06000444 RID: 1092 RVA: 0x0000F2E4 File Offset: 0x0000D4E4
		protected override bool Error(QK3_ERROR nErrNo)
		{
			return base.Error(nErrNo);
		}

		// Token: 0x06000445 RID: 1093 RVA: 0x0000F2F0 File Offset: 0x0000D4F0
		protected override void SetupOperator()
		{
			base.SetupOperator();
			this.SetOp(QK3_OP.QK3_OP_STR_BEGIN, Q_TOKEN.QTK_S_QUOT);
			this.SetOp(QK3_OP.QK3_OP_STR_END, Q_TOKEN.QTK_S_QUOT);
		}

		// Token: 0x06000446 RID: 1094 RVA: 0x0000F30C File Offset: 0x0000D50C
		protected override QK3_IDTYPE GetIdType(string szName, ref QK3_ITEMINFO ItemInfo)
		{
			if (string.Equals(szName, "null"))
			{
				ItemInfo.nType = 0;
				ItemInfo.nId = 0;
				return QK3_IDTYPE.QK3_IDTYPE_VAR;
			}
			CHAR_ID idFromName = Chara.GetIdFromName(szName);
			if (idFromName != CHAR_ID.NOTHING)
			{
				ItemInfo.nType = 0;
				ItemInfo.nId = (int)idFromName;
				return QK3_IDTYPE.QK3_IDTYPE_VAR;
			}
			for (int i = 0; i < AmCalc.g_aQcFuncList.Length; i++)
			{
				if (string.Equals(AmCalc.g_aQcFuncList[i].szName, szName))
				{
					ItemInfo.nType = 0;
					ItemInfo.nId = i;
					ItemInfo.nIndex = AmCalc.g_aQcFuncList[i].nReserved;
					return QK3_IDTYPE.QK3_IDTYPE_FUNC;
				}
			}
			int nIndex = 0;
			int num = GameData.SearchParam(szName.ToUpper(), ref nIndex);
			if (num > 0)
			{
				ItemInfo.nType = 1;
				ItemInfo.nId = num;
				ItemInfo.nIndex = nIndex;
				return QK3_IDTYPE.QK3_IDTYPE_VAR;
			}
			return QK3_IDTYPE.QK3_IDTYPE_NULL;
		}

		// Token: 0x06000447 RID: 1095 RVA: 0x0000F3E0 File Offset: 0x0000D5E0
		protected override bool VarSetValue(QK3_ITEMINFO ItemInfo, CQK3Node NodeIndex, CQK3Node NodeValue)
		{
			QC_VARTYPE nType = (QC_VARTYPE)ItemInfo.nType;
			if (nType == QC_VARTYPE.CONST)
			{
				Debug.Print("ERR: 定数には代入できません。");
				return false;
			}
			if (nType != QC_VARTYPE.GAMEPARAM)
			{
				return false;
			}
			if (NodeIndex.GetInt() < 0 || NodeIndex.GetInt() >= ItemInfo.nIndex)
			{
				Debug.Print(string.Format("ERR: 配列インデックスが範囲外です -> {0}", NodeIndex.GetInt()));
				return false;
			}
			GameData.SetParam(ItemInfo.nId + NodeIndex.GetInt(), NodeValue.GetInt());
			return true;
		}

		// Token: 0x06000448 RID: 1096 RVA: 0x0000F46C File Offset: 0x0000D66C
		protected override bool VarGetValue(QK3_ITEMINFO ItemInfo, CQK3Node NodeIndex, ref CQK3Node NodeRet)
		{
			QC_VARTYPE nType = (QC_VARTYPE)ItemInfo.nType;
			if (nType == QC_VARTYPE.CONST)
			{
				NodeRet.SetInt(ItemInfo.nId);
				return true;
			}
			if (nType != QC_VARTYPE.GAMEPARAM)
			{
				return false;
			}
			if (NodeIndex.GetInt() < 0 || NodeIndex.GetInt() >= ItemInfo.nIndex)
			{
				Debug.Print(string.Format("ERR: 配列インデックスが範囲外です -> {0}", NodeIndex.GetInt()));
				return false;
			}
			NodeRet.SetInt(GameData.GetParamString(ItemInfo.nId + NodeIndex.GetInt()));
			return true;
		}

		// Token: 0x06000449 RID: 1097 RVA: 0x0000F4FC File Offset: 0x0000D6FC
		protected override bool FuncCall(QK3_ITEMINFO ItemInfo, CQK3Node[] pParams, int nParamNum, ref CQK3Node NodeRet)
		{
			if (ItemInfo.nType != 0)
			{
				return false;
			}
			switch (ItemInfo.nId)
			{
			case 0:
				if (nParamNum < 1)
				{
					Debug.Print("rand関数の引数がありません。");
					return false;
				}
				NodeRet.SetInt(App.GetRandom(pParams[0].GetInt()));
				return true;
			case 1:
				if (nParamNum < 2)
				{
					Debug.Print("max関数の引数が足りません。");
					return false;
				}
				NodeRet.SetInt((pParams[0].GetInt() >= pParams[1].GetInt()) ? pParams[0].GetInt() : pParams[1].GetInt());
				return true;
			case 2:
				if (nParamNum < 2)
				{
					Debug.Print("min関数の引数が足りません。");
					return false;
				}
				NodeRet.SetInt((pParams[0].GetInt() <= pParams[1].GetInt()) ? pParams[0].GetInt() : pParams[1].GetInt());
				return true;
			case 3:
				if (nParamNum < 2)
				{
					Debug.Print("skidoku関数の引数が足りません。");
					return false;
				}
				if (pParams[0].GetNodeType() != QK3_NODETYPE.STR)
				{
					Debug.Print("skidoku関数の引数タイプが違います。");
					return false;
				}
				NodeRet.SetInt((!SysData.IsRead(pParams[0].GetString(), pParams[1].GetString(), 0)) ? 0 : 1);
				return true;
			case 4:
				if (nParamNum < 2)
				{
					Debug.Print("gkidoku関数の引数が足りません。");
					return false;
				}
				if (pParams[0].GetNodeType() != QK3_NODETYPE.STR)
				{
					Debug.Print("gkidoku関数の引数タイプが違います。");
					return false;
				}
				NodeRet.SetInt((!GameData.IsRead(pParams[0].GetString(), pParams[1].GetString())) ? 0 : 1);
				return true;
			default:
				return false;
			}
		}

		// Token: 0x040002E1 RID: 737
		private const int STR_MAX = 8;

		// Token: 0x040002E2 RID: 738
		private static AmCalc.QC_FUNCINFO[] g_aQcFuncList = new AmCalc.QC_FUNCINFO[]
		{
			new AmCalc.QC_FUNCINFO("rand", 0),
			new AmCalc.QC_FUNCINFO("max", 0),
			new AmCalc.QC_FUNCINFO("min", 0),
			new AmCalc.QC_FUNCINFO("skidoku", 0),
			new AmCalc.QC_FUNCINFO("gkidoku", 0)
		};

		// Token: 0x02000096 RID: 150
		private struct QC_FUNCINFO
		{
			// Token: 0x0600044A RID: 1098 RVA: 0x0000F6A8 File Offset: 0x0000D8A8
			public QC_FUNCINFO(string s, int n)
			{
				this.szName = s;
				this.nReserved = n;
			}

			// Token: 0x040002E3 RID: 739
			public string szName;

			// Token: 0x040002E4 RID: 740
			public int nReserved;
		}
	}
}
