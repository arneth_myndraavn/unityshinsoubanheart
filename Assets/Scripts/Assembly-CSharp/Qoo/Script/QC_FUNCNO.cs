﻿using System;

namespace Qoo.Script
{
	// Token: 0x02000094 RID: 148
	public enum QC_FUNCNO
	{
		// Token: 0x040002DB RID: 731
		RAND,
		// Token: 0x040002DC RID: 732
		NMAX,
		// Token: 0x040002DD RID: 733
		NMIN,
		// Token: 0x040002DE RID: 734
		SKIDOKU,
		// Token: 0x040002DF RID: 735
		GKIDOKU,
		// Token: 0x040002E0 RID: 736
		USER
	}
}
