﻿using System;
using Qoo.Memory;

namespace Qoo.Param
{
	// Token: 0x0200001C RID: 28
	public class PARAM_DATA
	{
		// Token: 0x060000B4 RID: 180 RVA: 0x00004664 File Offset: 0x00002864
		public PARAM_DATA(PARAM_DATA other)
		{
			this.param = other.param;
			this.init_param = other.init_param;
			this.IsString = other.IsString;
			this.index = other.index;
		}

		// Token: 0x060000B5 RID: 181 RVA: 0x000046C0 File Offset: 0x000028C0
		public PARAM_DATA(string s = "", bool b = false, int index_ = 0)
		{
			this.param = s;
			this.init_param = s;
			this.IsString = b;
			this.index = index_;
		}

		// Token: 0x060000B6 RID: 182 RVA: 0x00004708 File Offset: 0x00002908
		public void Init()
		{
			this.param = this.init_param;
		}

		// Token: 0x060000B7 RID: 183 RVA: 0x00004718 File Offset: 0x00002918
		public void Set(string param_)
		{
			this.param = param_;
		}

		// Token: 0x060000B8 RID: 184 RVA: 0x00004724 File Offset: 0x00002924
		public void Save(MemFile mem)
		{
			mem.SetStringUtf16(this.param);
			mem.SetStringUtf16(this.init_param);
			mem.SetBool(this.IsString);
			mem.SetInt32(this.index);
		}

		// Token: 0x060000B9 RID: 185 RVA: 0x00004764 File Offset: 0x00002964
		public void Load(MemFile mem)
		{
			this.param = mem.GetStringUtf16();
			this.init_param = mem.GetStringUtf16();
			this.IsString = mem.GetBool();
			this.index = mem.GetInt32();
		}

		// Token: 0x040000FA RID: 250
		public string param = string.Empty;

		// Token: 0x040000FB RID: 251
		public string init_param = string.Empty;

		// Token: 0x040000FC RID: 252
		public bool IsString;

		// Token: 0x040000FD RID: 253
		public int index;
	}
}
