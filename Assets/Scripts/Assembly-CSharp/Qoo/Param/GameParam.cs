﻿using System;
using System.Collections.Generic;
using Qoo.Memory;

namespace Qoo.Param
{
	// Token: 0x0200001D RID: 29
	public class GameParam
	{
		// Token: 0x1700001D RID: 29
		// (get) Token: 0x060000BB RID: 187 RVA: 0x000047B8 File Offset: 0x000029B8
		public int Num
		{
			get
			{
				return this.m_Param.Count;
			}
		}

		// Token: 0x060000BC RID: 188 RVA: 0x000047C8 File Offset: 0x000029C8
		public void Clear()
		{
			this.m_Param.Clear();
		}

		// Token: 0x060000BD RID: 189 RVA: 0x000047D8 File Offset: 0x000029D8
		public bool Attach(string name, string init, int index = 0, bool IsString = true)
		{
			if (this.m_Param.ContainsKey(name))
			{
				return false;
			}
			this.m_Param.Add(name, new PARAM_DATA(init, IsString, index));
			return true;
		}

		// Token: 0x060000BE RID: 190 RVA: 0x00004810 File Offset: 0x00002A10
		public void Init()
		{
			foreach (KeyValuePair<string, PARAM_DATA> keyValuePair in this.m_Param)
			{
				keyValuePair.Value.Init();
			}
		}

		// Token: 0x060000BF RID: 191 RVA: 0x0000487C File Offset: 0x00002A7C
		public void Copy(GameParam param)
		{
			this.Clear();
			foreach (KeyValuePair<string, PARAM_DATA> keyValuePair in param.m_Param)
			{
				this.m_Param.Add(keyValuePair.Key, new PARAM_DATA(keyValuePair.Value));
			}
		}

		// Token: 0x060000C0 RID: 192 RVA: 0x00004900 File Offset: 0x00002B00
		public string Get(string name)
		{
			if (!this.m_Param.ContainsKey(name))
			{
				return string.Empty;
			}
			return this.m_Param[name].param;
		}

		// Token: 0x060000C1 RID: 193 RVA: 0x00004938 File Offset: 0x00002B38
		public int GetIndex(string name)
		{
			if (!this.m_Param.ContainsKey(name))
			{
				return 0;
			}
			return this.m_Param[name].index;
		}

		// Token: 0x060000C2 RID: 194 RVA: 0x0000496C File Offset: 0x00002B6C
		public int GetInt(string name)
		{
			if (!this.m_Param.ContainsKey(name))
			{
				Debug.Print("Get Param Int :" + name + " is none");
				return 0;
			}
			int num = int.Parse(this.Get(name));
			Debug.Print(string.Concat(new object[]
			{
				"Get Param Int :",
				name,
				" is ",
				num
			}));
			return num;
		}

		// Token: 0x060000C3 RID: 195 RVA: 0x000049DC File Offset: 0x00002BDC
		public bool Set(string name, string param_)
		{
			if (!this.m_Param.ContainsKey(name))
			{
				return false;
			}
			this.m_Param[name].Set(param_);
			return true;
		}

		// Token: 0x060000C4 RID: 196 RVA: 0x00004A10 File Offset: 0x00002C10
		public bool SetInt(string name, int param_)
		{
			if (!this.m_Param.ContainsKey(name))
			{
				return false;
			}
			this.m_Param[name].Set(param_.ToString());
			return true;
		}

		// Token: 0x060000C5 RID: 197 RVA: 0x00004A4C File Offset: 0x00002C4C
		public bool AddInt(string name, int add_param)
		{
			return this.m_Param.ContainsKey(name) && this.SetInt(name, this.GetInt(name) + add_param);
		}

		// Token: 0x060000C6 RID: 198 RVA: 0x00004A7C File Offset: 0x00002C7C
		public bool IsString(string name)
		{
			return this.m_Param.ContainsKey(name) && this.m_Param[name].IsString;
		}

		// Token: 0x060000C7 RID: 199 RVA: 0x00004AB0 File Offset: 0x00002CB0
		public int GetIndexNum(int iNo)
		{
			Debug.Assert(iNo >= 0 && iNo < this.m_Param.Count, "パラメータの範囲外です。\n");
			Dictionary<string, PARAM_DATA>.Enumerator enumerator = this.m_Param.GetEnumerator();
			for (int i = 0; i <= iNo; i++)
			{
				enumerator.MoveNext();
			}
			int num = 1;
			while (enumerator.MoveNext())
			{
				KeyValuePair<string, PARAM_DATA> keyValuePair = enumerator.Current;
				if (keyValuePair.Value.index == 0)
				{
					break;
				}
				num++;
			}
			return num;
		}

		// Token: 0x060000C8 RID: 200 RVA: 0x00004B3C File Offset: 0x00002D3C
		public int SearchZeroIndex(string name)
		{
			Dictionary<string, PARAM_DATA>.Enumerator enumerator = this.m_Param.GetEnumerator();
			if (this.m_Param.Count > 0)
			{
				int num = 0;
				while (enumerator.MoveNext())
				{
					KeyValuePair<string, PARAM_DATA> keyValuePair = enumerator.Current;
					if (keyValuePair.Key.IndexOf(name) == 0)
					{
						KeyValuePair<string, PARAM_DATA> keyValuePair2 = enumerator.Current;
						if (keyValuePair2.Value.index == 0)
						{
							return num;
						}
					}
					num++;
				}
			}
			return -1;
		}

		// Token: 0x060000C9 RID: 201 RVA: 0x00004BB4 File Offset: 0x00002DB4
		public string GetParamString(int index)
		{
			Dictionary<string, PARAM_DATA>.Enumerator enumerator = this.m_Param.GetEnumerator();
			if (this.m_Param.Count > 0)
			{
				for (int num = 0; num != index; num++)
				{
					enumerator.MoveNext();
				}
				KeyValuePair<string, PARAM_DATA> keyValuePair = enumerator.Current;
				if (keyValuePair.Value != null)
				{
					KeyValuePair<string, PARAM_DATA> keyValuePair2 = enumerator.Current;
					return keyValuePair2.Value.param;
				}
			}
			return string.Empty;
		}

		// Token: 0x060000CA RID: 202 RVA: 0x00004C28 File Offset: 0x00002E28
		internal void SetParam(int index, int value)
		{
			Dictionary<string, PARAM_DATA>.Enumerator enumerator = this.m_Param.GetEnumerator();
			if (this.m_Param.Count > 0)
			{
				for (int num = 0; num != index; num++)
				{
					enumerator.MoveNext();
				}
				KeyValuePair<string, PARAM_DATA> keyValuePair = enumerator.Current;
				keyValuePair.Value.Set(value.ToString());
			}
		}

		// Token: 0x060000CB RID: 203 RVA: 0x00004C88 File Offset: 0x00002E88
		public void Save(MemFile mem)
		{
			mem.SetInt32(this.Num);
			for (int num = 0; num != this.Num; num++)
			{
				string name = this.GetName(num);
				mem.SetStringUtf16(name);
				this.m_Param[name].Save(mem);
			}
		}

		// Token: 0x060000CC RID: 204 RVA: 0x00004CDC File Offset: 0x00002EDC
		public void Load(MemFile mem)
		{
			this.Clear();
			int @int = mem.GetInt32();
			for (int num = 0; num != @int; num++)
			{
				string stringUtf = mem.GetStringUtf16();
				PARAM_DATA param_DATA = new PARAM_DATA(string.Empty, false, 0);
				param_DATA.Load(mem);
				this.m_Param.Add(stringUtf, param_DATA);
			}
		}

		// Token: 0x060000CD RID: 205 RVA: 0x00004D30 File Offset: 0x00002F30
		public string GetName(int index)
		{
			Dictionary<string, PARAM_DATA>.Enumerator enumerator = this.m_Param.GetEnumerator();
			enumerator.MoveNext();
			for (int num = 0; num != index; num++)
			{
				enumerator.MoveNext();
			}
			KeyValuePair<string, PARAM_DATA> keyValuePair = enumerator.Current;
			return keyValuePair.Key;
		}

		// Token: 0x040000FE RID: 254
		private Dictionary<string, PARAM_DATA> m_Param = new Dictionary<string, PARAM_DATA>();
	}
}
