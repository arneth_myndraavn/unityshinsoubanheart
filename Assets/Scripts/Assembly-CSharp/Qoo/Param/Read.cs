﻿using System;
using Qoo.Memory;

namespace Qoo.Param
{
	// Token: 0x0200001F RID: 31
	public class Read
	{
		// Token: 0x1700001E RID: 30
		// (get) Token: 0x060000DC RID: 220 RVA: 0x0000506C File Offset: 0x0000326C
		public short[] Data
		{
			get
			{
				return this.m_Pos;
			}
		}

		// Token: 0x060000DD RID: 221 RVA: 0x00005074 File Offset: 0x00003274
		public void Init()
		{
			for (int i = 0; i < this.m_Pos.GetLength(0); i++)
			{
				this.m_Pos[i] = 0;
			}
		}

		// Token: 0x060000DE RID: 222 RVA: 0x000050A8 File Offset: 0x000032A8
		public void Copy(Read other)
		{
			for (int i = 0; i < this.m_Pos.GetLength(0); i++)
			{
				this.m_Pos[i] = other.m_Pos[i];
			}
		}

		// Token: 0x060000DF RID: 223 RVA: 0x000050E4 File Offset: 0x000032E4
		public bool IsRead(int nKs, int nLabel, int nTag = 0)
		{
			if (!this.IsRengeKs(nKs))
			{
				return false;
			}
			if (this.IsRengeLabel(nLabel))
			{
				return (int)this.m_Pos[nKs * 200 + nLabel] > nTag;
			}
			if (nLabel < 0)
			{
				int num = 0;
				for (int num2 = 0; num2 != 200; num2++)
				{
					num += (int)this.m_Pos[nKs * 200 + num2];
				}
				return num > nTag;
			}
			return false;
		}

		// Token: 0x060000E0 RID: 224 RVA: 0x00005158 File Offset: 0x00003358
		public bool Set(int nKs, int nLabel, int nNo)
		{
			if (!this.IsRenge(nKs, nLabel))
			{
				return false;
			}
			this.m_Pos[nKs * 200 + nLabel] = (short)nNo;
			return true;
		}

		// Token: 0x060000E1 RID: 225 RVA: 0x00005188 File Offset: 0x00003388
		public bool Add(int nKs, int nLabel, int nNo)
		{
			if (!this.IsRenge(nKs, nLabel))
			{
				return false;
			}
			if ((int)this.m_Pos[nKs * 200 + nLabel] <= nNo)
			{
				this.m_Pos[nKs * 200 + nLabel] = (short)nNo;
			}
			return true;
		}

		// Token: 0x060000E2 RID: 226 RVA: 0x000051D0 File Offset: 0x000033D0
		public int Get(int nKs, int nLabel)
		{
			if (!this.IsRenge(nKs, nLabel))
			{
				return 0;
			}
			return (int)this.m_Pos[nKs * 200 + nLabel];
		}

		// Token: 0x060000E3 RID: 227 RVA: 0x000051F4 File Offset: 0x000033F4
		private bool IsRenge(int nKs, int nLabel)
		{
			return this.IsRengeKs(nKs) && this.IsRengeLabel(nLabel);
		}

		// Token: 0x060000E4 RID: 228 RVA: 0x0000520C File Offset: 0x0000340C
		private bool IsRengeKs(int nKs)
		{
			return nKs >= 0 && nKs < 1000;
		}

		// Token: 0x060000E5 RID: 229 RVA: 0x00005220 File Offset: 0x00003420
		private bool IsRengeLabel(int nLabel)
		{
			return nLabel >= 0 && nLabel < 200;
		}

		// Token: 0x060000E6 RID: 230 RVA: 0x00005234 File Offset: 0x00003434
		public void SetAll()
		{
			for (int i = 0; i < this.m_Pos.GetLength(0); i++)
			{
				this.m_Pos[i] = 16383;
			}
		}

		// Token: 0x060000E7 RID: 231 RVA: 0x0000526C File Offset: 0x0000346C
		public bool Save(MemFile mem)
		{
			mem.SetInt32(this.m_Pos.GetLength(0));
			mem.SetInt16Array(this.m_Pos);
			return true;
		}

		// Token: 0x060000E8 RID: 232 RVA: 0x00005290 File Offset: 0x00003490
		public bool Load(MemFile mem)
		{
			int @int = mem.GetInt32();
			if (this.m_Pos.GetLength(0) == @int)
			{
				for (int num = 0; num != this.m_Pos.GetLength(0); num++)
				{
					this.m_Pos[num] = mem.GetInt16();
				}
				return true;
			}
			return false;
		}

		// Token: 0x04000102 RID: 258
		private short[] m_Pos = new short[200000];
	}
}
