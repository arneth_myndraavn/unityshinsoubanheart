﻿using System;
using System.Collections.Generic;
using Qoo.Memory;

namespace Qoo.Param
{
	// Token: 0x0200001E RID: 30
	public class Look
	{
		// Token: 0x060000CF RID: 207 RVA: 0x00004DA8 File Offset: 0x00002FA8
		public bool Add(string name)
		{
			if (!this.m_FileTbl.ContainsKey(name))
			{
				int count = this.m_FileTbl.Count;
				this.m_FileTbl.Add(name, count);
				this.m_PosTbl.Add(count, name);
				this.m_LookTbl.Add(false);
			}
			return true;
		}

		// Token: 0x060000D0 RID: 208 RVA: 0x00004DFC File Offset: 0x00002FFC
		public void Clear()
		{
			this.m_FileTbl.Clear();
			this.m_PosTbl.Clear();
			this.m_LookTbl.Clear();
		}

		// Token: 0x060000D1 RID: 209 RVA: 0x00004E20 File Offset: 0x00003020
		public void Init()
		{
			this.SetAll(false);
		}

		// Token: 0x060000D2 RID: 210 RVA: 0x00004E2C File Offset: 0x0000302C
		public void SetAll(bool Is = true)
		{
			for (int num = 0; num != this.m_LookTbl.Count; num++)
			{
				this.m_LookTbl[num] = Is;
			}
		}

		// Token: 0x060000D3 RID: 211 RVA: 0x00004E64 File Offset: 0x00003064
		public int Get(string name)
		{
			Debug.Assert(this.m_FileTbl.ContainsKey(name), string.Format("リストに登録されていない名前です{0}", name));
			if (!this.m_FileTbl.ContainsKey(name))
			{
				return -1;
			}
			return this.m_FileTbl[name];
		}

		// Token: 0x060000D4 RID: 212 RVA: 0x00004EAC File Offset: 0x000030AC
		public string Get(int pos)
		{
			Debug.Assert(this.m_PosTbl.ContainsKey(pos), string.Format("リストに登録されていないインデックスです{0}", pos));
			return this.m_PosTbl[pos];
		}

		// Token: 0x060000D5 RID: 213 RVA: 0x00004EDC File Offset: 0x000030DC
		public bool Set(string name, bool IsLook = true)
		{
			if (this.m_FileTbl.ContainsKey(name))
			{
				this.m_LookTbl[this.Get(name)] = IsLook;
				return IsLook;
			}
			return false;
		}

		// Token: 0x060000D6 RID: 214 RVA: 0x00004F14 File Offset: 0x00003114
		public bool Set(int pos, bool IsLook = true)
		{
			if (this.m_PosTbl.ContainsKey(pos))
			{
				this.m_LookTbl[pos] = IsLook;
				return IsLook;
			}
			return false;
		}

		// Token: 0x060000D7 RID: 215 RVA: 0x00004F44 File Offset: 0x00003144
		public bool IsLook(string name)
		{
			int num = this.Get(name);
			return num != -1 && this.m_LookTbl[this.Get(name)];
		}

		// Token: 0x060000D8 RID: 216 RVA: 0x00004F74 File Offset: 0x00003174
		public bool IsLook(int pos)
		{
			return this.m_LookTbl[pos];
		}

		// Token: 0x060000D9 RID: 217 RVA: 0x00004F84 File Offset: 0x00003184
		public bool Save(MemFile mem)
		{
			mem.SetInt32(this.m_LookTbl.Count);
			foreach (bool @bool in this.m_LookTbl)
			{
				mem.SetBool(@bool);
			}
			return true;
		}

		// Token: 0x060000DA RID: 218 RVA: 0x00004FFC File Offset: 0x000031FC
		public bool Load(MemFile mem)
		{
			int @int = mem.GetInt32();
			if (@int == this.m_LookTbl.Count)
			{
				for (int num = 0; num != this.m_LookTbl.Count; num++)
				{
					this.m_LookTbl[num] = mem.GetBool();
				}
				return true;
			}
			return false;
		}

		// Token: 0x040000FF RID: 255
		private Dictionary<string, int> m_FileTbl = new Dictionary<string, int>();

		// Token: 0x04000100 RID: 256
		private Dictionary<int, string> m_PosTbl = new Dictionary<int, string>();

		// Token: 0x04000101 RID: 257
		private List<bool> m_LookTbl = new List<bool>();
	}
}
