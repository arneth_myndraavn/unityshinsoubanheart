﻿using System;

namespace Qoo
{
	// Token: 0x0200000B RID: 11
	public class CheckString
	{
		// Token: 0x06000029 RID: 41 RVA: 0x00002408 File Offset: 0x00000608
		public CheckString(string str)
		{
			this.debug_string = string.Empty;
			Debug.Print(string.Format("CheckString() {0}", str));
		}

		// Token: 0x0600002A RID: 42 RVA: 0x0000242C File Offset: 0x0000062C
		public void end()
		{
			if (this.debug_string.Length != 0)
			{
				Debug.Print(this.debug_string);
			}
		}

		// Token: 0x0600002B RID: 43 RVA: 0x0000244C File Offset: 0x0000064C
		public void check(string a, string b)
		{
			if (a != b)
			{
				this.debug_string += string.Format("ちがうよ(a,b)=({0},{1})\n", a, b);
			}
		}

		// Token: 0x04000019 RID: 25
		private string debug_string;
	}
}
