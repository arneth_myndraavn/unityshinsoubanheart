﻿using System;

namespace Qoo.Graphics
{
	// Token: 0x0200002B RID: 43
	public static class Graph
	{
		// Token: 0x06000161 RID: 353 RVA: 0x00006640 File Offset: 0x00004840
		public static bool Fade_Out()
		{
			Singleton<UnityGraph>.Instance.SetEffect("FadeOut", 1f);
			return true;
		}

		// Token: 0x06000162 RID: 354 RVA: 0x00006658 File Offset: 0x00004858
		public static bool Fade_In()
		{
			Singleton<UnityGraph>.Instance.SetEffect("FadeIn", 1f);
			return true;
		}

		// Token: 0x06000163 RID: 355 RVA: 0x00006670 File Offset: 0x00004870
		internal static void EnableFade(bool IsEnable)
		{
			if (!IsEnable)
			{
				Singleton<UnityGraph>.Instance.ResetFade(255);
			}
		}

		// Token: 0x06000164 RID: 356 RVA: 0x00006688 File Offset: 0x00004888
		internal static void Fade_Cross(int time_ms)
		{
			Singleton<UnityGraph>.Instance.SetEffect(string.Empty, (float)time_ms / 1000f);
		}
	}
}
