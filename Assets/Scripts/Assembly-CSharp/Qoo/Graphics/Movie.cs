﻿using System;
using System.Collections;
using Qoo.File;
using UnityEngine;

namespace Qoo.Graphics
{
	// Token: 0x0200002C RID: 44
	public static class Movie
	{
		// Token: 0x06000165 RID: 357 RVA: 0x000066A4 File Offset: 0x000048A4
		internal static IEnumerator Play(string name, bool IsSkip)
		{
			FileId fileId = new FileId(string.Empty);
			fileId.Set(name);
			NMB_FILEINFO fileInfo = Nmb.GetFileInfo(fileId.Name);
			string text = fileInfo.DirName + fileInfo.FileName;
			Debug.Print(string.Format("INFO:Start Movie({0} Skip={1})", text, IsSkip.ToString()));
			//TODO fix movie playing. either make platform specific versions OR make platform agnostic
			//UnityTask.SetSubTask(MovieManager.PlayMovie(text, FullScreenMovieControlMode.CancelOnInput));
			yield return MovieManager.PlayMovie(text, new GameObject());
		}
	}
}
