﻿using System;

namespace Qoo.Graphics
{
	// Token: 0x0200002A RID: 42
	public static class Effect
	{
		// Token: 0x0600015F RID: 351 RVA: 0x00006624 File Offset: 0x00004824
		public static bool ApplyKsEffect(string strName, int iParam)
		{
			Singleton<UnityGraph>.Instance.SetEffect(strName, (float)iParam / 1000f);
			return true;
		}
	}
}
