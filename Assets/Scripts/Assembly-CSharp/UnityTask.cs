﻿using System;
using System.Collections;

// Token: 0x0200018E RID: 398
public class UnityTask
{
	// Token: 0x06000B62 RID: 2914 RVA: 0x00030C08 File Offset: 0x0002EE08
	public UnityTask(IEnumerator task_)
	{
		this.task = task_;
		this.type = TASK_TYPE.COROUTINE;
		this.IsTaskEnd = false;
	}

	// Token: 0x06000B63 RID: 2915 RVA: 0x00030C28 File Offset: 0x0002EE28
	public UnityTask(IEnumerator task_, bool isNoUnityTask)
	{
		this.task = task_;
		if (isNoUnityTask)
		{
			this.type = TASK_TYPE.COROUTINE_NOUNITY;
		}
		else
		{
			this.type = TASK_TYPE.COROUTINE;
		}
		this.IsTaskEnd = false;
	}

	// Token: 0x06000B64 RID: 2916 RVA: 0x00030C58 File Offset: 0x0002EE58
	private UnityTask(TASK_TYPE type_)
	{
		this.task = null;
		this.type = type_;
		this.IsTaskEnd = false;
	}

	// Token: 0x06000B65 RID: 2917 RVA: 0x00030C78 File Offset: 0x0002EE78
	private UnityTask(float waitTime)
	{
		this.type = TASK_TYPE.TIME_WAIT;
		this.param_float = waitTime;
		this.IsTaskEnd = false;
	}

	// Token: 0x06000B66 RID: 2918 RVA: 0x00030C98 File Offset: 0x0002EE98
	private UnityTask(string name_, float time_)
	{
		this.type = TASK_TYPE.EFFECT;
		this.param_string = name_;
		this.param_float = time_;
	}

	// Token: 0x17000185 RID: 389
	// (get) Token: 0x06000B67 RID: 2919 RVA: 0x00030CB8 File Offset: 0x0002EEB8
	// (set) Token: 0x06000B68 RID: 2920 RVA: 0x00030CC0 File Offset: 0x0002EEC0
	public bool IsTaskEnd { get; set; }

	// Token: 0x06000B69 RID: 2921 RVA: 0x00030CCC File Offset: 0x0002EECC
	public static int FrameUpdate()
	{
		Singleton<TaskManager>.Instance.AddSubTask(new UnityTask(TASK_TYPE.NEXT_FRAME));
		return 0;
	}

	// Token: 0x06000B6A RID: 2922 RVA: 0x00030CE0 File Offset: 0x0002EEE0
	public static int Effect(string name, float time)
	{
		Singleton<TaskManager>.Instance.AddSubTask(new UnityTask(name, time));
		return 0;
	}

	// Token: 0x06000B6B RID: 2923 RVA: 0x00030CF8 File Offset: 0x0002EEF8
	public static int Wait(float time)
	{
		Singleton<TaskManager>.Instance.AddSubTask(new UnityTask(time));
		return 0;
	}

	// Token: 0x06000B6C RID: 2924 RVA: 0x00030D0C File Offset: 0x0002EF0C
	public static int SetMainTask(IEnumerator task_)
	{
		Singleton<TaskManager>.Instance.AddMainTask(new UnityTask(task_));
		return 0;
	}

	// Token: 0x06000B6D RID: 2925 RVA: 0x00030D20 File Offset: 0x0002EF20
	public static int SetSubTask(IEnumerator task_)
	{
		Singleton<TaskManager>.Instance.AddSubTask(new UnityTask(task_));
		return 0;
	}

	// Token: 0x06000B6E RID: 2926 RVA: 0x00030D34 File Offset: 0x0002EF34
	public static int SetSubNoUnityTask(IEnumerator task_)
	{
		Singleton<TaskManager>.Instance.AddSubTask(new UnityTask(task_, true));
		return 0;
	}

	// Token: 0x06000B6F RID: 2927 RVA: 0x00030D4C File Offset: 0x0002EF4C
	public static void SetReadTask(IEnumerator task_)
	{
		Singleton<TaskManager>.Instance.AddReadTask(new UnityTask(task_));
	}

	// Token: 0x06000B70 RID: 2928 RVA: 0x00030D60 File Offset: 0x0002EF60
	public bool MoveNext()
	{
		if (this.m_Child != null)
		{
			if (this.m_Child.MoveNext())
			{
				return true;
			}
			this.m_Child = null;
		}
		if (this.task.MoveNext())
		{
			if (this.task.Current is IEnumerator)
			{
				this.m_Child = new UnityTask(this.task.Current as IEnumerator);
			}
			return true;
		}
		return false;
	}

	// Token: 0x040008FD RID: 2301
	private UnityTask m_Child;

	// Token: 0x040008FE RID: 2302
	public IEnumerator task;

	// Token: 0x040008FF RID: 2303
	public TASK_TYPE type;

	// Token: 0x04000900 RID: 2304
	public float param_float;

	// Token: 0x04000901 RID: 2305
	public string param_string;
}
