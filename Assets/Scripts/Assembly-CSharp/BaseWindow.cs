﻿using System;
using System.Collections;
using System.Collections.Generic;
using Qoo;
using Qoo.Game;
using Qoo.SoundSystem;
using UnityEngine;

// Token: 0x020001A1 RID: 417
public abstract class BaseWindow : MonoBehaviourWrap
{
	// Token: 0x06000C0D RID: 3085 RVA: 0x00032380 File Offset: 0x00030580
	protected virtual void OnAwake()
	{
	}

	// Token: 0x06000C0E RID: 3086 RVA: 0x00032384 File Offset: 0x00030584
	protected virtual string[] newSceneTextureNameArray()
	{
		return new string[0];
	}

	// Token: 0x06000C0F RID: 3087 RVA: 0x0003238C File Offset: 0x0003058C
	protected virtual void BeforeInit()
	{
	}

	// Token: 0x06000C10 RID: 3088 RVA: 0x00032390 File Offset: 0x00030590
	protected virtual BaseWindow.UIComponent[] newComponentArray()
	{
		return new BaseWindow.UIComponent[0];
	}

	// Token: 0x06000C11 RID: 3089 RVA: 0x00032398 File Offset: 0x00030598
	protected virtual void AfterInit()
	{
	}

	// Token: 0x06000C12 RID: 3090 RVA: 0x0003239C File Offset: 0x0003059C
	protected virtual void OnBaseWindowUpdate()
	{
	}

	// Token: 0x06000C13 RID: 3091 RVA: 0x000323A0 File Offset: 0x000305A0
	protected virtual void OnBaseWindowOnButton(string obj)
	{
	}

	// Token: 0x06000C14 RID: 3092 RVA: 0x000323A4 File Offset: 0x000305A4
	protected virtual void OnBaseWindowDestroy()
	{
	}

	// Token: 0x06000C15 RID: 3093 RVA: 0x000323A8 File Offset: 0x000305A8
	protected virtual void OnGraphicLoadComplete()
	{
	}

	// Token: 0x06000C16 RID: 3094 RVA: 0x000323AC File Offset: 0x000305AC
	protected virtual string GetBGMName()
	{
		return null;
	}

	// Token: 0x06000C17 RID: 3095 RVA: 0x000323B0 File Offset: 0x000305B0
	public sealed override void Awake()
	{
		this.UIGameObjectsDic = new Dictionary<string, GameObject>();
		this.UITextSpriteDic = new Dictionary<string, UnityTextSprite>();
		this.imageObjectList = new List<ImageObject>();
		this.graphicHolder = null;
		this.alreadyCalledAddScene = false;
		this.bUpdate = false;
		this.OnAwake();
		this.graphicHolder = new GraphicManager.SceneGraphicHolder(this.newSceneTextureNameArray());
		string bgmname = this.GetBGMName();
		if (bgmname != null)
		{
			if (bgmname.Length != 0)
			{
				this.PlayBGM(bgmname);
			}
			else
			{
				this.StopBGM();
			}
		}
		this.StartTime();
	}

	// Token: 0x06000C18 RID: 3096 RVA: 0x0003243C File Offset: 0x0003063C
	public sealed override void Start()
	{
		//base.transform.parent.localPosition = new Vector3(2000f, 0f, 0f);
		//base.transform.gameObject.transform.position = new Vector3(2000f, 0f, 0f);
		base.transform.gameObject.transform.position = new Vector3(0f, 0f, 0f);
		this.BeforeInit();
		foreach (BaseWindow.UIComponent uicomponent in this.newComponentArray())
		{
			uicomponent.CreateComponent(this);
		}
		this.AfterInit();
	}

	// Token: 0x06000C19 RID: 3097 RVA: 0x000324A0 File Offset: 0x000306A0
	public sealed override void Update()
	{
		if (this.bUpdate)
		{
			this.OnBaseWindowUpdate();
		}
		else
		{
			this.bUpdate = this.graphicHolder.isDone();
			if (this.bUpdate)
			{
				this.OnGraphicLoadComplete();
			}
		}
		this.ProcTime();
	}

	// Token: 0x06000C1A RID: 3098 RVA: 0x000324EC File Offset: 0x000306EC
	public sealed override void OnDestroy()
	{
		this.OnBaseWindowDestroy();
		this.graphicHolder.Release();
		this.graphicHolder = null;
	}

	// Token: 0x06000C1B RID: 3099 RVA: 0x00032508 File Offset: 0x00030708
	public IEnumerator WaitLoadTexture()
	{
		yield return base.StartCoroutine(this.graphicHolder.WaitLoadTexture());
		foreach (ImageObject obj in this.imageObjectList)
		{
			obj.ApplyRendererEnableAfterLoad();
		}
		foreach (KeyValuePair<string, UnityTextSprite> obj2 in this.UITextSpriteDic)
		{
			UnityTextSprite p = obj2.Value;
			p.Show = true;
			p.Update(960, 544, p.obj.transform.localPosition.z);
		}
		yield break;
	}

	// Token: 0x06000C1C RID: 3100 RVA: 0x00032524 File Offset: 0x00030724
	public sealed override void OnButton(string obj)
	{
		if (!this.bUpdate)
		{
			return;
		}
		if (!this.alreadyCalledAddScene)
		{
			this.OnBaseWindowOnButton(obj);
		}
	}

	// Token: 0x06000C1D RID: 3101 RVA: 0x00032544 File Offset: 0x00030744
	protected void PlayBGM(string name)
	{
		Sound.BgmPlay(name, 1f, 1000, 1000);
	}

	// Token: 0x06000C1E RID: 3102 RVA: 0x0003255C File Offset: 0x0003075C
	protected void StopBGM()
	{
		Sound.BgmStop(1000);
	}

	// Token: 0x06000C1F RID: 3103 RVA: 0x00032568 File Offset: 0x00030768
	protected void PlaySE(string name)
	{
		Sound.SysSePlay(name, 1f);
	}

	// Token: 0x06000C20 RID: 3104 RVA: 0x00032578 File Offset: 0x00030778
	protected void PlayPlayerSE(string name)
	{
		Sound.SysPlayerSePlay(name, 1f);
		Qoo.Debug.Print(name);
	}

	// Token: 0x06000C21 RID: 3105 RVA: 0x0003258C File Offset: 0x0003078C
	protected bool IsPlayerSEPlaying()
	{
		return Sound.SysPlayerSeIsPlay();
	}

	// Token: 0x06000C22 RID: 3106 RVA: 0x00032594 File Offset: 0x00030794
	protected void PlayVoice(string name, bool isSystem = false)
	{
		if (isSystem)
		{
			Sound.VoicePlay(name, 1f);
		}
		else
		{
			Singleton<ManSound>.Instance.VoicePlay(name, 1f);
		}
	}

	// Token: 0x06000C23 RID: 3107 RVA: 0x000325C8 File Offset: 0x000307C8
	protected void StopVoice()
	{
		Sound.VoiceStop();
	}

	// Token: 0x06000C24 RID: 3108 RVA: 0x000325D0 File Offset: 0x000307D0
	protected void PlaySE_Ok()
	{
		Sound.SE_Ok();
	}

	// Token: 0x06000C25 RID: 3109 RVA: 0x000325D8 File Offset: 0x000307D8
	protected void PlaySE_Select()
	{
		Sound.SE_Select();
	}

	// Token: 0x06000C26 RID: 3110 RVA: 0x000325E0 File Offset: 0x000307E0
	protected void PlaySE_Cancel()
	{
		Sound.SE_Cancel();
	}

	// Token: 0x06000C27 RID: 3111 RVA: 0x000325E8 File Offset: 0x000307E8
	public GameObject MakeCollisionObject(string name, int x, int y, int z, int width, int height)
	{
		int num = -480;
		int num2 = 272;
		GameObject gameObject = new GameObject(name);
		gameObject.transform.parent = base.transform;
		gameObject.transform.localPosition = new Vector3((float)(num + width / 2 + x), (float)(num2 - height / 2 - y), (float)(-(float)z));
		gameObject.transform.localScale = new Vector3((float)width, (float)height, 1f);
		gameObject.AddComponent<BoxCollider>();
		this.UIGameObjectsDic.Add(gameObject.name, gameObject);
		return gameObject;
	}

	// Token: 0x06000C28 RID: 3112 RVA: 0x00032674 File Offset: 0x00030874
	public GameObject MakeImageObject(string name_, int x_, int y_, int z_, string fpath_, bool collisionEnable = false, bool RendererEnableAfterLoad = true, int xImgCnt_ = 1, int yImgCnt_ = 1, int defaultIndex_ = 0)
	{
		UIObjectInfo uiObject = new UIObjectInfo(name_, x_, y_, z_, fpath_, xImgCnt_, yImgCnt_, defaultIndex_);
		ImageObject imageObject = ImageObject.Create(uiObject, base.transform, collisionEnable);
		imageObject.RendererEnableAfterLoad = RendererEnableAfterLoad;
		GameObject gameObject = imageObject.gameObject;
		if (gameObject.GetComponent<Collider>() != null)
		{
			gameObject.GetComponent<Collider>().enabled = (collisionEnable && RendererEnableAfterLoad);
		}
		this.UIGameObjectsDic.Add(gameObject.name, gameObject);
		this.imageObjectList.Add(imageObject);
		return gameObject;
	}

	// Token: 0x06000C29 RID: 3113 RVA: 0x000326FC File Offset: 0x000308FC
	public void MakeUIAction(UIAction uiAction)
	{
		uiAction.Init();
	}

	// Token: 0x06000C2A RID: 3114 RVA: 0x00032704 File Offset: 0x00030904
	public void EntryTextObject(string name_, UnityTextSprite textSprite)
	{
		this.UITextSpriteDic.Add(name_, textSprite);
	}

	// Token: 0x06000C2B RID: 3115 RVA: 0x00032714 File Offset: 0x00030914
	protected UnityTextSprite GetTextSprite(string name)
	{
		return this.UITextSpriteDic[name];
	}

	// Token: 0x170001A8 RID: 424
	// (get) Token: 0x06000C2C RID: 3116 RVA: 0x00032724 File Offset: 0x00030924
	protected bool IsTrial
	{
		get
		{
			return SysData.IsTrial();
		}
	}

	// Token: 0x06000C2D RID: 3117 RVA: 0x0003272C File Offset: 0x0003092C
	protected void GameObjectShow(string name, bool show)
	{
		if (this.ExistsGameObject(name))
		{
			this.GameObjectShow(this.GetGameObject(name), show);
		}
	}

	// Token: 0x06000C2E RID: 3118 RVA: 0x00032748 File Offset: 0x00030948
	protected void GameObjectShow(GameObject go, bool show)
	{
		if (go == null)
		{
			return;
		}
		if (go.GetComponent<Renderer>() != null)
		{
			go.GetComponent<Renderer>().enabled = show;
		}
		if (go.GetComponent<Collider>() != null)
		{
			go.GetComponent<Collider>().enabled = show;
		}
	}

	// Token: 0x06000C2F RID: 3119 RVA: 0x0003279C File Offset: 0x0003099C
	protected void AddGameObject(string name, GameObject go)
	{
		this.UIGameObjectsDic.Add(name, go);
	}

	// Token: 0x06000C30 RID: 3120 RVA: 0x000327AC File Offset: 0x000309AC
	protected bool ExistsGameObject(string name)
	{
		return this.UIGameObjectsDic.ContainsKey(name);
	}

	// Token: 0x06000C31 RID: 3121 RVA: 0x000327BC File Offset: 0x000309BC
	protected GameObject GetGameObject(string name)
	{
		if (!this.ExistsGameObject(name))
		{
			return null;
		}
		return this.UIGameObjectsDic[name];
	}

	// Token: 0x06000C32 RID: 3122 RVA: 0x000327D8 File Offset: 0x000309D8
	protected ImageObject GetImageObject(string name)
	{
		GameObject gameObject = this.GetGameObject(name);
		if (gameObject == null)
		{
			return null;
		}
		return gameObject.GetComponent<ImageObject>();
	}

	// Token: 0x06000C33 RID: 3123 RVA: 0x00032804 File Offset: 0x00030A04
	protected UIActionToggle GetActionToggle(string name)
	{
		GameObject gameObject = this.GetGameObject(name);
		if (gameObject == null)
		{
			return null;
		}
		return gameObject.GetComponent<UIActionToggle>();
	}

	// Token: 0x06000C34 RID: 3124 RVA: 0x00032830 File Offset: 0x00030A30
	protected UIActionButton GetActionButton(string name)
	{
		GameObject gameObject = this.GetGameObject(name);
		if (gameObject == null)
		{
			return null;
		}
		return gameObject.GetComponent<UIActionButton>();
	}

	// Token: 0x06000C35 RID: 3125 RVA: 0x0003285C File Offset: 0x00030A5C
	protected void SetTexture(string name, string fpath)
	{
		ImageObject imageObject = this.GetImageObject(name);
		if (imageObject == null)
		{
			return;
		}

		imageObject.SetTexture(fpath);
	}

	// Token: 0x06000C36 RID: 3126 RVA: 0x00032888 File Offset: 0x00030A88
	protected bool LoadCompleted(string name)
	{
		ImageObject imageObject = this.GetImageObject(name);
		return imageObject == null || imageObject.LoadCompleted;
	}

	// Token: 0x06000C37 RID: 3127 RVA: 0x000328B4 File Offset: 0x00030AB4
	protected void RestartAnimation(string name)
	{
		ImageObject imageObject = this.GetImageObject(name);
		if (imageObject == null)
		{
			return;
		}
		imageObject.RestartAnimation();
	}

	// Token: 0x06000C38 RID: 3128 RVA: 0x000328DC File Offset: 0x00030ADC
	protected void SetParent(string child, string parent)
	{
		GameObject gameObject = this.GetGameObject(child);
		GameObject gameObject2 = this.GetGameObject(parent);
		gameObject.transform.parent = gameObject2.transform;
	}

	// Token: 0x06000C39 RID: 3129 RVA: 0x0003290C File Offset: 0x00030B0C
	protected void AddScene(string nextSceneName)
	{
		if (!this.alreadyCalledAddScene)
		{
			this.alreadyCalledAddScene = true;
			SceneManager.AddScene(nextSceneName);
		}
	}

	// Token: 0x06000C3A RID: 3130 RVA: 0x00032928 File Offset: 0x00030B28
	protected void DeleteLastAddScene()
	{
		SceneManager.DeleteLastAddScene();
	}

	// Token: 0x06000C3B RID: 3131 RVA: 0x00032930 File Offset: 0x00030B30
	protected void DeleteAllAddScene()
	{
		SceneManager.DeleteAllAddScene();
	}

	// Token: 0x06000C3C RID: 3132 RVA: 0x00032938 File Offset: 0x00030B38
	public void ClearCalledAddScene()
	{
		this.alreadyCalledAddScene = false;
	}

	// Token: 0x06000C3D RID: 3133 RVA: 0x00032944 File Offset: 0x00030B44
	public virtual void OnCurrentWindow()
	{
		this.ClearCalledAddScene();
	}

	// Token: 0x06000C3E RID: 3134 RVA: 0x0003294C File Offset: 0x00030B4C
	public virtual void OnPastWindow()
	{
		this.alreadyCalledAddScene = true;
	}

	// Token: 0x06000C3F RID: 3135 RVA: 0x00032958 File Offset: 0x00030B58
	public void StartTime()
	{
		this.m_fTime = 0f;
	}

	// Token: 0x06000C40 RID: 3136 RVA: 0x00032968 File Offset: 0x00030B68
	private void ProcTime()
	{
		this.m_fTime += Time.deltaTime;
	}

	// Token: 0x06000C41 RID: 3137 RVA: 0x0003297C File Offset: 0x00030B7C
	public bool CheckTime(float fTime)
	{
		return fTime < this.m_fTime;
	}

	// Token: 0x0400094C RID: 2380
	private Dictionary<string, GameObject> UIGameObjectsDic;

	// Token: 0x0400094D RID: 2381
	private Dictionary<string, UnityTextSprite> UITextSpriteDic;

	// Token: 0x0400094E RID: 2382
	private List<ImageObject> imageObjectList;

	// Token: 0x0400094F RID: 2383
	private GraphicManager.SceneGraphicHolder graphicHolder;

	// Token: 0x04000950 RID: 2384
	private bool alreadyCalledAddScene;

	// Token: 0x04000951 RID: 2385
	private bool bUpdate;

	// Token: 0x04000952 RID: 2386
	private float m_fTime;

	// Token: 0x020001A2 RID: 418
	protected abstract class UIComponent
	{
		// Token: 0x06000C43 RID: 3139
		public abstract void CreateComponent(BaseWindow window);
	}

	// Token: 0x020001A3 RID: 419
	protected abstract class UIBase : BaseWindow.UIComponent
	{
		// Token: 0x06000C44 RID: 3140 RVA: 0x00032998 File Offset: 0x00030B98
		protected UIBase(string name_, int x_, int y_, int z_, bool collisionEnable_)
		{
			this.name = name_;
			this.x = x_;
			this.y = y_;
			this.z = z_;
			this.collisionEnable = collisionEnable_;
		}

		// Token: 0x06000C45 RID: 3141 RVA: 0x000329C8 File Offset: 0x00030BC8
		protected void CreateHandler(BaseWindow window, GameObject go)
		{
			if (go != null && this.collisionEnable)
			{
				ScriptEventSelfHandler.Create(go, ScriptEventTriger.OnMouseDownUp, delegate(string obj)
				{
					window.OnButton(obj);
				}, this.name);
			}
		}

		// Token: 0x04000953 RID: 2387
		protected string name;

		// Token: 0x04000954 RID: 2388
		protected int x;

		// Token: 0x04000955 RID: 2389
		protected int y;

		// Token: 0x04000956 RID: 2390
		protected int z;

		// Token: 0x04000957 RID: 2391
		protected bool collisionEnable;
	}

	// Token: 0x020001A4 RID: 420
	protected class UICollision : BaseWindow.UIBase
	{
		// Token: 0x06000C46 RID: 3142 RVA: 0x00032A14 File Offset: 0x00030C14
		public UICollision(string name_, int x_, int y_, int z_, int w_, int h_) : base(name_, x_, y_, z_, true)
		{
			this.w = w_;
			this.h = h_;
		}

		// Token: 0x06000C47 RID: 3143 RVA: 0x00032A34 File Offset: 0x00030C34
		public override void CreateComponent(BaseWindow window)
		{
			GameObject go = window.MakeCollisionObject(this.name, this.x, this.y, this.z, this.w, this.h);
			base.CreateHandler(window, go);
		}

		// Token: 0x04000958 RID: 2392
		protected int w;

		// Token: 0x04000959 RID: 2393
		protected int h;
	}

	// Token: 0x020001A5 RID: 421
	protected class UIText : BaseWindow.UIBase
	{
		// Token: 0x06000C48 RID: 3144 RVA: 0x00032A74 File Offset: 0x00030C74
		public UIText(string name_, int x_, int y_, int z_, string text_, int size_, UnityTextSprite.PositionType px_ = UnityTextSprite.PositionType.TopLeft, UnityTextSprite.PositionType py_ = UnityTextSprite.PositionType.TopLeft, byte r_ = 255, byte g_ = 255, byte b_ = 255, byte a_ = 255) : base(name_, x_, y_, z_, false)
		{
			this.text = text_;
			this.size = size_;
			this.px = px_;
			this.py = py_;
			this.color = new Color32(r_, g_, b_, a_);
		}

		// Token: 0x06000C49 RID: 3145 RVA: 0x00032AC0 File Offset: 0x00030CC0
		public override void CreateComponent(BaseWindow window)
		{
			GameObject gameObject = window.gameObject;
			UnityTextSprite unityTextSprite = new UnityTextSprite(false, gameObject);
			unityTextSprite.obj.name = this.name;
			//Changed to true, revisit later
			unityTextSprite.Show = true;
			unityTextSprite.Font = Singleton<UnityGraph>.Instance.Font;
			unityTextSprite.ClearText();
			unityTextSprite.SetColor(this.color);
			unityTextSprite.AddText(this.text, this.size);
			unityTextSprite.SetPosition(this.x, this.y, this.px, this.py);
			unityTextSprite.Update(960, 544, (float)(-(float)this.z));
			window.EntryTextObject(this.name, unityTextSprite);
		}

		// Token: 0x0400095A RID: 2394
		private string text;

		// Token: 0x0400095B RID: 2395
		private int size;

		// Token: 0x0400095C RID: 2396
		private UnityTextSprite.PositionType px;

		// Token: 0x0400095D RID: 2397
		private UnityTextSprite.PositionType py;

		// Token: 0x0400095E RID: 2398
		private Color32 color;
	}

	// Token: 0x020001A6 RID: 422
	protected class UIImage : BaseWindow.UIBase
	{
		// Token: 0x06000C4A RID: 3146 RVA: 0x00032B6C File Offset: 0x00030D6C
		public UIImage(string name_, int x_, int y_, int z_, string fpath_, bool collisionEnable_ = false, bool RendererEnableAfterLoad_ = true) : base(name_, x_, y_, z_, collisionEnable_)
		{
			this.fpath = fpath_;
			this.RendererEnableAfterLoad = RendererEnableAfterLoad_;
		}

		// Token: 0x06000C4B RID: 3147 RVA: 0x00032B8C File Offset: 0x00030D8C
		public override void CreateComponent(BaseWindow window)
		{
			GameObject go = window.MakeImageObject(this.name, this.x, this.y, this.z, this.fpath, this.collisionEnable, this.RendererEnableAfterLoad, 1, 1, 0);
			base.CreateHandler(window, go);
		}

		// Token: 0x0400095F RID: 2399
		protected string fpath;

		// Token: 0x04000960 RID: 2400
		protected bool RendererEnableAfterLoad;
	}

	// Token: 0x020001A7 RID: 423
	protected abstract class UIImageComponent : BaseWindow.UIImage
	{
		// Token: 0x06000C4C RID: 3148 RVA: 0x00032BD8 File Offset: 0x00030DD8
		protected UIImageComponent(string name_, int x_, int y_, int z_, string fpath_, bool collisionEnable_ = false, bool RendererEnableAfterLoad_ = true, int xImgCnt_ = 1, int yImgCnt_ = 1, int defaultIndex_ = 0) : base(name_, x_, y_, z_, fpath_, collisionEnable_, RendererEnableAfterLoad_)
		{
			this.xImgCnt = xImgCnt_;
			this.yImgCnt = yImgCnt_;
			this.defaultIndex = defaultIndex_;
		}

		// Token: 0x06000C4D RID: 3149
		protected abstract UIAction newAction(GameObject go);

		// Token: 0x06000C4E RID: 3150 RVA: 0x00032C10 File Offset: 0x00030E10
		public override void CreateComponent(BaseWindow window)
		{
			GameObject go = window.MakeImageObject(this.name, this.x, this.y, this.z, this.fpath, this.collisionEnable, this.RendererEnableAfterLoad, this.xImgCnt, this.yImgCnt, this.defaultIndex);
			window.MakeUIAction(this.newAction(go));
			base.CreateHandler(window, go);
		}

		// Token: 0x04000961 RID: 2401
		private int xImgCnt;

		// Token: 0x04000962 RID: 2402
		private int yImgCnt;

		// Token: 0x04000963 RID: 2403
		private int defaultIndex;
	}

	// Token: 0x020001A8 RID: 424
	protected class UIButton : BaseWindow.UIImageComponent
	{
		// Token: 0x06000C4F RID: 3151 RVA: 0x00032C78 File Offset: 0x00030E78
		public UIButton(string name_, int x_, int y_, int z_, string fpath_, bool collisionEnable_ = false, bool RendererEnableAfterLoad_ = true, int xImgCnt_ = 1, int yImgCnt_ = 1, int defaultIndex_ = 0) : base(name_, x_, y_, z_, fpath_, collisionEnable_, RendererEnableAfterLoad_, xImgCnt_, yImgCnt_, defaultIndex_)
		{
		}

		// Token: 0x06000C50 RID: 3152 RVA: 0x00032C9C File Offset: 0x00030E9C
		protected override UIAction newAction(GameObject go)
		{
			return UIActionButton.Create(go, 0, 1);
		}
	}

	// Token: 0x020001A9 RID: 425
	protected class UIToggle : BaseWindow.UIImageComponent
	{
		// Token: 0x06000C51 RID: 3153 RVA: 0x00032CA8 File Offset: 0x00030EA8
		public UIToggle(string name_, int x_, int y_, int z_, string fpath_, bool collisionEnable_ = false, bool RendererEnableAfterLoad_ = true, int xImgCnt_ = 1, int yImgCnt_ = 1, int on = 0, int off = 1, bool autoSwitch = false) : base(name_, x_, y_, z_, fpath_, collisionEnable_, RendererEnableAfterLoad_, xImgCnt_, yImgCnt_, 0)
		{
			this.on = on;
			this.off = off;
			this.autoSwitch = autoSwitch;
		}

		// Token: 0x06000C52 RID: 3154 RVA: 0x00032CE4 File Offset: 0x00030EE4
		protected override UIAction newAction(GameObject go)
		{
			return UIActionToggle.Create(go, this.on, this.off, this.autoSwitch);
		}

		// Token: 0x04000964 RID: 2404
		private int on;

		// Token: 0x04000965 RID: 2405
		private int off;

		// Token: 0x04000966 RID: 2406
		private bool autoSwitch;
	}
}
