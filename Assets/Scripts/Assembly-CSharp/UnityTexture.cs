﻿using System;
using Qoo;
using UnityEngine;

// Token: 0x0200015E RID: 350
public class UnityTexture
{
	// Token: 0x06000A14 RID: 2580 RVA: 0x0002CDE4 File Offset: 0x0002AFE4
	public UnityTexture(string name_, Texture2D texture_, UnityFile file, SizeF scale = null)
	{
		this.m_Name = name_;
		this.m_Texture = texture_;
		this.m_Texture.name = name_;
		this.m_File = file;
		if (scale != null)
		{
			this.m_Scale = scale;
		}
		this.m_Count = 1;
		this.m_isInit = false;
	}

	// Token: 0x06000A15 RID: 2581 RVA: 0x0002CE4C File Offset: 0x0002B04C
	public UnityTexture(string name_, Texture2D texture_, byte[] data, SizeF scale = null)
	{
		this.m_Name = name_;
		this.m_Texture = texture_;
		this.m_Texture.name = name_;
		if (scale != null)
		{
			this.m_Scale = scale;
		}
		this.m_Count = 1;
		this.SetData(data);
	}

	// Token: 0x06000A16 RID: 2582 RVA: 0x0002CEAC File Offset: 0x0002B0AC
	public UnityTexture(string name_, Texture2D texture_, SizeF scale = null)
	{
		this.m_Name = name_;
		this.m_Texture = texture_;
		this.m_Texture.name = name_;
		if (scale != null)
		{
			this.m_Scale = scale;
		}
		this.m_Count = 1;
		this.m_isInit = true;
	}

	// Token: 0x17000142 RID: 322
	// (get) Token: 0x06000A17 RID: 2583 RVA: 0x0002CF0C File Offset: 0x0002B10C
	// (set) Token: 0x06000A18 RID: 2584 RVA: 0x0002CF14 File Offset: 0x0002B114
	public string m_Name { get; private set; }

	// Token: 0x17000143 RID: 323
	// (get) Token: 0x06000A19 RID: 2585 RVA: 0x0002CF20 File Offset: 0x0002B120
	// (set) Token: 0x06000A1A RID: 2586 RVA: 0x0002CF28 File Offset: 0x0002B128
	public Texture2D m_Texture { get; set; }

	// Token: 0x17000144 RID: 324
	// (get) Token: 0x06000A1B RID: 2587 RVA: 0x0002CF34 File Offset: 0x0002B134
	public bool IsInit
	{
		get
		{
			return this.m_isInit;
		}
	}

	// Token: 0x06000A1C RID: 2588 RVA: 0x0002CF3C File Offset: 0x0002B13C
	public void IncCount()
	{
		this.m_Count++;
	}

	// Token: 0x06000A1D RID: 2589 RVA: 0x0002CF4C File Offset: 0x0002B14C
	public void DecCount()
	{
		if (this.m_Count > 0)
		{
			this.m_Count--;
		}
	}

	// Token: 0x06000A1E RID: 2590 RVA: 0x0002CF68 File Offset: 0x0002B168
	public bool IsRelease()
	{
		return this.m_Count == 0;
	}

	// Token: 0x06000A1F RID: 2591 RVA: 0x0002CF74 File Offset: 0x0002B174
	public bool Update()
	{
		if (!this.m_isInit && this.m_File.IsReadEnd && this.SetData(this.m_File.Data))
		{
			this.m_File = null;
		}
		return true;
	}

	// Token: 0x06000A20 RID: 2592 RVA: 0x0002CFB0 File Offset: 0x0002B1B0
	private bool SetData(byte[] data)
	{
		if (data == null)
		{
			Qoo.Debug.Assert(false, string.Format("Error:Load:Not Found TextureFile = {0}", this.m_Name));
		}
		else
		{
			if (this.m_Texture.LoadImage(data))
			{
				this.m_isInit = true;
				return true;
			}
			Qoo.Debug.Assert(false, string.Format("Error:Load:Not Found TextureFile = {0}", this.m_Name));
		}
		return false;
	}

	// Token: 0x04000838 RID: 2104
	public SizeF m_Scale = new SizeF(1f, 1f);

	// Token: 0x04000839 RID: 2105
	private int m_Count;

	// Token: 0x0400083A RID: 2106
	private UnityFile m_File;

	// Token: 0x0400083B RID: 2107
	private bool m_isInit;
}
