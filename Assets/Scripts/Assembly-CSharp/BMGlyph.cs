﻿using System;
using System.Collections.Generic;

// Token: 0x0200014D RID: 333
[Serializable]
public class BMGlyph
{
	// Token: 0x06000946 RID: 2374 RVA: 0x000286DC File Offset: 0x000268DC
	public int GetKerning(int previousChar, bool IsKerning = true)
	{
		if (this.kerning != null && IsKerning)
		{
			int i = 0;
			int count = this.kerning.Count;
			while (i < count)
			{
				if (this.kerning[i] == previousChar)
				{
					return this.kerning[i + 1];
				}
				i += 2;
			}
		}
		return 0;
	}

	// Token: 0x06000947 RID: 2375 RVA: 0x0002873C File Offset: 0x0002693C
	public void SetKerning(int previousChar, int amount)
	{
		if (this.kerning == null)
		{
			this.kerning = new List<int>();
		}
		for (int i = 0; i < this.kerning.Count; i += 2)
		{
			if (this.kerning[i] == previousChar)
			{
				this.kerning[i + 1] = amount;
				return;
			}
		}
		this.kerning.Add(previousChar);
		this.kerning.Add(amount);
	}

	// Token: 0x06000948 RID: 2376 RVA: 0x000287B8 File Offset: 0x000269B8
	public void Trim(int xMin, int yMin, int xMax, int yMax)
	{
		int num = this.x + this.width;
		int num2 = this.y + this.height;
		if (this.x < xMin)
		{
			int num3 = xMin - this.x;
			this.x += num3;
			this.width -= num3;
			this.offsetX += num3;
		}
		if (this.y < yMin)
		{
			int num4 = yMin - this.y;
			this.y += num4;
			this.height -= num4;
			this.offsetY += num4;
		}
		if (num > xMax)
		{
			this.width -= num - xMax;
		}
		if (num2 > yMax)
		{
			this.height -= num2 - yMax;
		}
	}

	// Token: 0x040007B0 RID: 1968
	public int index;

	// Token: 0x040007B1 RID: 1969
	public int x;

	// Token: 0x040007B2 RID: 1970
	public int y;

	// Token: 0x040007B3 RID: 1971
	public int width;

	// Token: 0x040007B4 RID: 1972
	public int height;

	// Token: 0x040007B5 RID: 1973
	public int offsetX;

	// Token: 0x040007B6 RID: 1974
	public int offsetY;

	// Token: 0x040007B7 RID: 1975
	public int advance;

	// Token: 0x040007B8 RID: 1976
	public int channel;

	// Token: 0x040007B9 RID: 1977
	public List<int> kerning;
}
