﻿using System;
using Qoo.Game;

// Token: 0x02000169 RID: 361
public class CSVSceneMemory
{
	// Token: 0x06000A60 RID: 2656 RVA: 0x0002DF44 File Offset: 0x0002C144
	public CSVSceneMemory(string[] values)
	{
		this.charaIndex = -1;
		this.charaId = values[0];
		this.eventName = values[1];
		this.startKs = values[2].Replace(".ks", string.Empty);
		this.startLabel = values[3];
		this.triggerKs = values[4].Replace(".ks", string.Empty);
		this.triggerLabel = values[5];
		this.thumbnail = values[6].Replace(".tga", string.Empty);
		this.route = ((values.Length >= 8) ? values[7] : string.Empty);
		this.stay = ((values.Length >= 9) ? values[8] : string.Empty);
		this.collectCount = 0;
		this.charaIndex = CSVManager.calcCharaIndex(this.charaId);
	}

	// Token: 0x06000A61 RID: 2657 RVA: 0x0002E01C File Offset: 0x0002C21C
	public void Update()
	{
		//Set to True for 100% Scene Unlock
		bool flag = SysData.IsRead(this.triggerKs, (this.triggerLabel.Length != 0) ? this.triggerLabel.Substring(1) : string.Empty, 0);
        this.collectCount = ((!flag) ? 0 : 1);
	}

	// Token: 0x04000864 RID: 2148
	public int charaIndex;

	// Token: 0x04000865 RID: 2149
	public string charaId;

	// Token: 0x04000866 RID: 2150
	public string eventName;

	// Token: 0x04000867 RID: 2151
	public string startKs;

	// Token: 0x04000868 RID: 2152
	public string startLabel;

	// Token: 0x04000869 RID: 2153
	public string triggerKs;

	// Token: 0x0400086A RID: 2154
	public string triggerLabel;

	// Token: 0x0400086B RID: 2155
	public string thumbnail;

	// Token: 0x0400086C RID: 2156
	public string route;

	// Token: 0x0400086D RID: 2157
	public string stay;

	// Token: 0x0400086E RID: 2158
	public int collectCount;
}
