﻿using System;
using UnityEngine;

// Token: 0x02000142 RID: 322
public class ScriptEvent : MonoBehaviour
{
	// Token: 0x060008F1 RID: 2289 RVA: 0x00027524 File Offset: 0x00025724
	public virtual void Action()
	{
	}

	// Token: 0x060008F2 RID: 2290 RVA: 0x00027528 File Offset: 0x00025728
	private void OnMouseUpAsButton()
	{
		if (this.Triger == ScriptEventTriger.OnMouseDownUp)
		{
			this.Action();
		}
	}

	// Token: 0x04000792 RID: 1938
	public ScriptEventTriger Triger;
}
