﻿using System;
using UnityEngine;

// Token: 0x0200019C RID: 412
public class UIAction : MonoBehaviour
{
	// Token: 0x170001A5 RID: 421
	// (get) Token: 0x06000BF3 RID: 3059 RVA: 0x00032148 File Offset: 0x00030348
	// (set) Token: 0x06000BF4 RID: 3060 RVA: 0x00032150 File Offset: 0x00030350
	public bool ActionEnable
	{
		get
		{
			return this.actionEnable;
		}
		set
		{
			this.actionEnable = value;
		}
	}

	// Token: 0x06000BF5 RID: 3061 RVA: 0x0003215C File Offset: 0x0003035C
	public virtual void Init()
	{
		this.imageObject = base.gameObject.GetComponent<ImageObject>();
	}

	// Token: 0x0400093A RID: 2362
	public ImageObject imageObject;

	// Token: 0x0400093B RID: 2363
	private bool actionEnable = true;
}
