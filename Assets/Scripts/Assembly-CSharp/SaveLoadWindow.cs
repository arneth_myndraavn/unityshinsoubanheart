﻿using System;
using Qoo.Game;
using Qoo.SoundSystem;
using UnityEngine;

// Token: 0x02000130 RID: 304
public class SaveLoadWindow : BaseWindow
{
	// Token: 0x06000821 RID: 2081 RVA: 0x0002432C File Offset: 0x0002252C
	private bool CheckDataExist(int idx)
	{
		SaveFileInfo saveFileInfo = SaveLoadManager.LoadHeader(idx);
		return saveFileInfo != null;
	}

	// Token: 0x06000822 RID: 2082 RVA: 0x00024348 File Offset: 0x00022548
	private void OnDataSelect(int value)
	{
		this.m_Index = value;
		SaveFileInfo saveFileInfo = SaveLoadManager.LoadHeader(this.m_Index);
		this.viewSaveInfo(saveFileInfo);
		if (this.m_SaveOrLoad == SaveLoadType.SAVE)
		{
			if (value != 9)
			{
				if (saveFileInfo != null)
				{
					this.ConfirmShow(true, SaveLoadWindow.ConfrmType.Over);
				}
				else
				{
					this.ConfirmShow(true, SaveLoadWindow.ConfrmType.Save);
				}
			}
		}
		else if (saveFileInfo != null)
		{
			this.ConfirmShow(true, SaveLoadWindow.ConfrmType.Load);
		}
	}

	// Token: 0x06000823 RID: 2083 RVA: 0x000243B4 File Offset: 0x000225B4
	private void OnSaveLoad()
	{
		if (this.m_SaveOrLoad == SaveLoadType.SAVE)
		{
			this.OnSave();
		}
		else
		{
			this.OnLoad();
		}
	}

	// Token: 0x06000824 RID: 2084 RVA: 0x000243D4 File Offset: 0x000225D4
	private void OnSave()
	{
		byte[] saveData = this.GetSaveData();
		SaveFileInfo info = this.CreateSaveFileInfo();
		SaveLoadManager.Save(saveData, info, this.m_Index);
		SaveLoadManager.SaveSystem();
		this.viewSaveInfo(info);
		base.SetTexture(this.m_Index.ToString("SaveData0"), this.GetSaveDataIconName(this.m_Index));
	}

	// Token: 0x06000825 RID: 2085 RVA: 0x0002442C File Offset: 0x0002262C
	private void OnLoad()
	{
		byte[] data_ = SaveLoadManager.Load(this.m_Index);
		GameData.Load(data_);
		if (this.m_TitleOrAdvMode == SaveLoadCallType.ADVMENU)
		{
			base.DeleteAllAddScene();
		}
		else
		{
			SceneManager.ChangeScene("SceneAdvMode");
		}
		Sound.BgmStop(1000);
	}

	// Token: 0x06000826 RID: 2086 RVA: 0x00024478 File Offset: 0x00022678
	private void ConfirmShow(bool show = true, SaveLoadWindow.ConfrmType ct = SaveLoadWindow.ConfrmType.Save)
	{
		base.GameObjectShow("Confirm", show);
		base.GameObjectShow("Confirm_YES", show);
		base.GameObjectShow("Confirm_NO", show);
		base.GameObjectShow("CofirmCollision", show);
		if (show)
		{
			switch (ct)
			{
			case SaveLoadWindow.ConfrmType.Save:
				this.SetText("SaveConfirmMsg", (!show) ? string.Empty : UnityApp.Instance.isJapanese ? "セーブしますか？" : "Do you want to save?");
				break;
			case SaveLoadWindow.ConfrmType.Load:
				this.SetText("LoadConfirmMsg", (!show) ? string.Empty : UnityApp.Instance.isJapanese ? "ロードしますか？" : "Do you want to load this save?");
				break;
			case SaveLoadWindow.ConfrmType.Over:
				this.SetText("OverConfirmMsg", (!show) ? string.Empty : UnityApp.Instance.isJapanese ? "上書きしますか？" : "Do you want to overwrite this Save?");
				break;
			}
		}
		else
		{
			this.SetText("SaveConfirmMsg", string.Empty);
			this.SetText("LoadConfirmMsg", string.Empty);
			this.SetText("OverConfirmMsg", string.Empty);
		}
	}

	// Token: 0x06000827 RID: 2087 RVA: 0x00024578 File Offset: 0x00022778
	private byte[] GetSaveData()
	{
		return GameData.Save();
	}

	// Token: 0x06000828 RID: 2088 RVA: 0x00024580 File Offset: 0x00022780
	private SaveFileInfo CreateSaveFileInfo()
	{
		return new SaveFileInfo
		{
			LastMessage = GameData.LastMessage,
			PlayCharaId = (int)GameData.GetRoute(),
			SceneName = GameData.GetSceneName(),
			TimeStamp = DateTime.Now.ToString("yyyy／MM／dd  HH：mm")
		};
	}

	// Token: 0x06000829 RID: 2089 RVA: 0x000245D0 File Offset: 0x000227D0
	private void SetText(string name, string text)
	{
		UnityTextSprite textSprite = base.GetTextSprite(name);
		textSprite.ClearText();
		textSprite.AddText(text, 28);
		textSprite.Update(960, 544, -5f);
	}

	// Token: 0x0600082A RID: 2090 RVA: 0x0002460C File Offset: 0x0002280C
	private string getTimeStamp(SaveFileInfo info)
	{
		string result = string.Empty;
		if (info == null)
		{
			if (this.m_SaveOrLoad == SaveLoadType.SAVE)
			{
				result = "New Data";
			}
		}
		else
		{
			result = info.TimeStamp;
		}
		return result;
	}

	// Token: 0x0600082B RID: 2091 RVA: 0x00024644 File Offset: 0x00022844
	private string getSceneName(SaveFileInfo info)
	{
		if (info != null)
		{
			return info.SceneName;
		}
		return string.Empty;
	}

	// Token: 0x0600082C RID: 2092 RVA: 0x00024658 File Offset: 0x00022858
	private string getLastMessage1(SaveFileInfo info)
	{
		string result = string.Empty;
		if (info == null)
		{
			return string.Empty;
		}
		int num = info.LastMessage.IndexOf('\n');
		if (num < 0)
		{
			result = info.LastMessage;
		}
		else
		{
			result = info.LastMessage.Substring(0, num);
		}
		return result;
	}

	// Token: 0x0600082D RID: 2093 RVA: 0x000246A8 File Offset: 0x000228A8
	private string getLastMessage2(SaveFileInfo info)
	{
		string result = string.Empty;
		if (info == null)
		{
			return string.Empty;
		}
		int num = info.LastMessage.IndexOf('\n');
		if (num >= 0)
		{
			string text = info.LastMessage.Substring(num + 1);
			int num2 = text.IndexOf('\n');
			if (num2 < 0)
			{
				result = text;
			}
			else
			{
				result = text.Substring(0, num2);
			}
		}
		return result;
	}

	// Token: 0x0600082E RID: 2094 RVA: 0x0002470C File Offset: 0x0002290C
	private void viewSaveInfo(SaveFileInfo info)
	{
		this.SetText("TimeStamp", this.getTimeStamp(info));
		this.SetText("SceneName", this.getSceneName(info));
		this.SetText("LastMessage1", this.getLastMessage1(info));
		this.SetText("LastMessage2", this.getLastMessage2(info));
	}

	// Token: 0x0600082F RID: 2095 RVA: 0x00024764 File Offset: 0x00022964
	protected sealed override void OnAwake()
	{
		this.m_SaveOrLoad = UIValue.SaveLoadType;
		this.m_TitleOrAdvMode = UIValue.SaveLoadCallType;
	}

	// Token: 0x06000830 RID: 2096 RVA: 0x0002477C File Offset: 0x0002297C
	protected sealed override string[] newSceneTextureNameArray()
	{
		string str = (this.m_SaveOrLoad != SaveLoadType.SAVE) ? "load" : "save";
		return new string[]
		{
			"screen/sysmenu/sys_bnr_" + str,
			"screen/cgmemory2/cgm_bg_1",
			"screen/sysmenu/sys_pop",
			"screen/common/sys_dialog_save",
			"screen/common/dlog_btn_y",
			"screen/common/dlog_btn_n",
			"screen/common/cancel",
			"screen/saveload/icon_empty",
			"screen/saveload/icon_play_auto",
			"screen/saveload/icon_play_cha01",
			"screen/saveload/icon_play_cha02",
			"screen/saveload/icon_play_cha03",
			"screen/saveload/icon_play_cha04",
			"screen/saveload/icon_play_cha05",
			"screen/saveload/icon_play_cha06",
			"screen/saveload/icon_play_cha07",
			"screen/saveload/icon_play_cha08",
			"screen/saveload/icon_play_cha09",
			"screen/saveload/icon_play_cha10",
			"screen/saveload/icon_play_cha99"
		};
	}

	// Token: 0x06000831 RID: 2097 RVA: 0x00024860 File Offset: 0x00022A60
	protected sealed override void BeforeInit()
	{
		base.transform.localPosition = new Vector3(0f, 0f, -50f);
	}

	// Token: 0x06000832 RID: 2098 RVA: 0x00024884 File Offset: 0x00022A84
	protected sealed override void AfterInit()
	{
		this.ConfirmShow(false, SaveLoadWindow.ConfrmType.Save);
	}

	// Token: 0x06000833 RID: 2099 RVA: 0x00024890 File Offset: 0x00022A90
	private string GetSaveDataIconName(int i)
	{
		SaveFileInfo saveFileInfo = SaveLoadManager.LoadHeader(i);
		bool flag = saveFileInfo != null;
		string result = "icon_empty";
		if (flag)
		{
			if (i < 9)
			{
				result = ((saveFileInfo.PlayCharaId != -1) ? (saveFileInfo.PlayCharaId + 1).ToString("icon_play_cha00") : "icon_play_cha99");
			}
			else
			{
				result = "icon_play_auto";
			}
		}
		return result;
	}

	// Token: 0x06000834 RID: 2100 RVA: 0x000248F8 File Offset: 0x00022AF8
	private string[] GetSaveDataIcons()
	{
		string[] array = new string[10];
		for (int i = 0; i < 10; i++)
		{
			array[i] = this.GetSaveDataIconName(i);
		}
		return array;
	}

	// Token: 0x06000835 RID: 2101 RVA: 0x0002492C File Offset: 0x00022B2C
	protected sealed override BaseWindow.UIComponent[] newComponentArray()
	{
		string str = (this.m_SaveOrLoad != SaveLoadType.SAVE) ? "load" : "save";
		bool flag = this.m_SaveOrLoad == SaveLoadType.LOAD;
		string[] saveDataIcons = this.GetSaveDataIcons();
		int num = 188;
		int num2 = 131;
		return new BaseWindow.UIComponent[]
		{
			new BaseWindow.UIImage("BackGround", 0, 0, this.wndz, "screen/cgmemory2/cgm_bg_1", true, true),
			new BaseWindow.UIImage("Banner", 0, 0, this.wndz + 1, "screen/sysmenu/sys_bnr_" + str, true, true),
			new BaseWindow.UIImage("SysPop", 5, 373, this.wndz + 1, "screen/sysmenu/sys_pop", true, true),
			new BaseWindow.UIButton("Cancel", 887, 6, this.wndz + 2, "screen/common/cancel", true, true, 1, 2, 0),
			new BaseWindow.UIImage("SaveData0", 13 + 0 * num, 102 + 0 * num2, this.wndz + 2, "screen/saveload/" + saveDataIcons[0], true, true),
			new BaseWindow.UIImage("SaveData1", 13 + 1 * num, 102 + 0 * num2, this.wndz + 2, "screen/saveload/" + saveDataIcons[1], true, true),
			new BaseWindow.UIImage("SaveData2", 13 + 2 * num, 102 + 0 * num2, this.wndz + 2, "screen/saveload/" + saveDataIcons[2], true, true),
			new BaseWindow.UIImage("SaveData3", 13 + 3 * num, 102 + 0 * num2, this.wndz + 2, "screen/saveload/" + saveDataIcons[3], true, true),
			new BaseWindow.UIImage("SaveData4", 13 + 4 * num, 102 + 0 * num2, this.wndz + 2, "screen/saveload/" + saveDataIcons[4], true, true),
			new BaseWindow.UIImage("SaveData5", 13 + 0 * num, 102 + 1 * num2, this.wndz + 2, "screen/saveload/" + saveDataIcons[5], true, true),
			new BaseWindow.UIImage("SaveData6", 13 + 1 * num, 102 + 1 * num2, this.wndz + 2, "screen/saveload/" + saveDataIcons[6], true, true),
			new BaseWindow.UIImage("SaveData7", 13 + 2 * num, 102 + 1 * num2, this.wndz + 2, "screen/saveload/" + saveDataIcons[7], true, true),
			new BaseWindow.UIImage("SaveData8", 13 + 3 * num, 102 + 1 * num2, this.wndz + 2, "screen/saveload/" + saveDataIcons[8], true, true),
			new BaseWindow.UIImage("SaveData9", 13 + 4 * num, 102 + 1 * num2, this.wndz + 2, "screen/saveload/" + saveDataIcons[9], flag, flag),
			new BaseWindow.UIText("TimeStamp", 59, 386, this.wndz + 3, string.Empty, 28, UnityTextSprite.PositionType.TopLeft, UnityTextSprite.PositionType.TopLeft, byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue),
			new BaseWindow.UIText("SceneName", 59, 414, this.wndz + 3, string.Empty, 28, UnityTextSprite.PositionType.TopLeft, UnityTextSprite.PositionType.TopLeft, byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue),
			new BaseWindow.UIText("LastMessage1", 59, 456, this.wndz + 3, string.Empty, 28, UnityTextSprite.PositionType.TopLeft, UnityTextSprite.PositionType.TopLeft, byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue),
			new BaseWindow.UIText("LastMessage2", 59, 484, this.wndz + 3, string.Empty, 28, UnityTextSprite.PositionType.TopLeft, UnityTextSprite.PositionType.TopLeft, byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue),
			new BaseWindow.UICollision("CofirmCollision", 0, 0, this.wndz + 3, 960, 544),
			new BaseWindow.UIImage("Confirm", 136, 182, this.wndz + 3, "screen/common/sys_dialog", true, false),
			new BaseWindow.UIButton("Confirm_YES", 312, 277, this.wndz + 4, "screen/common/dlog_btn_y", true, false, 1, 2, 0),
			new BaseWindow.UIButton("Confirm_NO", 513, 277, this.wndz + 4, "screen/common/dlog_btn_n", true, false, 1, 2, 0),
			new BaseWindow.UIText("SaveConfirmMsg", 480, 230, this.wndz + 4, UnityApp.Instance.isJapanese ? "セーブしますか？" : "Do you want to save?", 28, UnityTextSprite.PositionType.Center, UnityTextSprite.PositionType.TopLeft, byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue),
			new BaseWindow.UIText("LoadConfirmMsg", 480, 230, this.wndz + 4, UnityApp.Instance.isJapanese ? "ロードしますか？" : "Do you want to load this save?", 28, UnityTextSprite.PositionType.Center, UnityTextSprite.PositionType.TopLeft, byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue),
			new BaseWindow.UIText("OverConfirmMsg", 480, 230, this.wndz + 4, UnityApp.Instance.isJapanese ? "上書きしますか？" : "Do you want to overwrite this Save?", 28, UnityTextSprite.PositionType.Center, UnityTextSprite.PositionType.TopLeft, byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue)
		};
	}

	// Token: 0x06000836 RID: 2102 RVA: 0x00024E34 File Offset: 0x00023034
	protected sealed override void OnBaseWindowOnButton(string obj)
	{
		switch (obj)
		{
		case "SaveData0":
		case "SaveData1":
		case "SaveData2":
		case "SaveData3":
		case "SaveData4":
		case "SaveData5":
		case "SaveData6":
		case "SaveData7":
		case "SaveData8":
		case "SaveData9":
			this.saveloadAction(obj);
			break;
		case "Confirm_YES":
			base.PlaySE_Ok();
			this.OnSaveLoad();
			this.ConfirmShow(false, SaveLoadWindow.ConfrmType.Save);
			break;
		case "Confirm_NO":
			base.PlaySE_Cancel();
			this.ConfirmShow(false, SaveLoadWindow.ConfrmType.Save);
			break;
		case "Cancel":
			base.PlaySE_Cancel();
			if (this.m_TitleOrAdvMode == SaveLoadCallType.ADVMENU)
			{
				base.DeleteLastAddScene();
			}
			else
			{
				SceneManager.BackScene();
			}
			break;
		}
	}

	// Token: 0x06000837 RID: 2103 RVA: 0x00024F8C File Offset: 0x0002318C
	private void saveloadAction(string obj)
	{
		int idx = int.Parse(obj.Replace("SaveData", string.Empty));
		if (this.m_SaveOrLoad == SaveLoadType.LOAD && !this.CheckDataExist(idx))
		{
			return;
		}
		base.PlaySE_Ok();
		this.OnDataSelect(int.Parse(obj.Replace("SaveData", string.Empty)));
	}

	// Token: 0x0400072B RID: 1835
	private SaveLoadType m_SaveOrLoad;

	// Token: 0x0400072C RID: 1836
	private SaveLoadCallType m_TitleOrAdvMode;

	// Token: 0x0400072D RID: 1837
	private int m_Index;

	// Token: 0x0400072E RID: 1838
	private readonly int wndz;

	// Token: 0x02000131 RID: 305
	private enum ConfrmType
	{
		// Token: 0x04000731 RID: 1841
		Save,
		// Token: 0x04000732 RID: 1842
		Load,
		// Token: 0x04000733 RID: 1843
		Over
	}
}
