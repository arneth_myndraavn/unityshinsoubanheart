﻿using System;
using Qoo.Input;
using UnityEngine;

// Token: 0x02000177 RID: 375
public class GameInput : UnityInput
{
	// Token: 0x1700015E RID: 350
	// (get) Token: 0x06000A9A RID: 2714 RVA: 0x0002EB94 File Offset: 0x0002CD94
	// (set) Token: 0x06000A9B RID: 2715 RVA: 0x0002EB9C File Offset: 0x0002CD9C
	public INPUT_STATUS Status
	{
		get
		{
			return this.m_eStatus;
		}
		private set
		{
			this.m_eStatus = value;
		}
	}

	// Token: 0x1700015F RID: 351
	// (get) Token: 0x06000A9C RID: 2716 RVA: 0x0002EBA8 File Offset: 0x0002CDA8
	// (set) Token: 0x06000A9D RID: 2717 RVA: 0x0002EBB0 File Offset: 0x0002CDB0
	public SLIDE_VECTOR Slide
	{
		get
		{
			return this.m_eSlide;
		}
		private set
		{
			this.m_eSlide = value;
		}
	}

	// Token: 0x17000160 RID: 352
	// (get) Token: 0x06000A9E RID: 2718 RVA: 0x0002EBBC File Offset: 0x0002CDBC
	// (set) Token: 0x06000A9F RID: 2719 RVA: 0x0002EBC4 File Offset: 0x0002CDC4
	public Point2 Start
	{
		get
		{
			return this.m_Start;
		}
		private set
		{
			this.m_Start = value;
		}
	}

	// Token: 0x17000161 RID: 353
	// (get) Token: 0x06000AA0 RID: 2720 RVA: 0x0002EBD0 File Offset: 0x0002CDD0
	// (set) Token: 0x06000AA1 RID: 2721 RVA: 0x0002EBD8 File Offset: 0x0002CDD8
	public Point2 End
	{
		get
		{
			return this.m_End;
		}
		private set
		{
			this.m_End = value;
		}
	}

	// Token: 0x17000162 RID: 354
	// (get) Token: 0x06000AA2 RID: 2722 RVA: 0x0002EBE4 File Offset: 0x0002CDE4
	// (set) Token: 0x06000AA3 RID: 2723 RVA: 0x0002EBEC File Offset: 0x0002CDEC
	public Point2 Delta
	{
		get
		{
			return this.m_Delta;
		}
		private set
		{
			this.m_Delta = value;
		}
	}

	// Token: 0x17000163 RID: 355
	// (get) Token: 0x06000AA4 RID: 2724 RVA: 0x0002EBF8 File Offset: 0x0002CDF8
	// (set) Token: 0x06000AA5 RID: 2725 RVA: 0x0002EC00 File Offset: 0x0002CE00
	public float Accel
	{
		get
		{
			return this.m_Accel;
		}
		private set
		{
			this.m_Accel = value;
		}
	}

	// Token: 0x17000164 RID: 356
	// (get) Token: 0x06000AA6 RID: 2726 RVA: 0x0002EC0C File Offset: 0x0002CE0C
	// (set) Token: 0x06000AA7 RID: 2727 RVA: 0x0002EC14 File Offset: 0x0002CE14
	public Vector2 Normal
	{
		get
		{
			return this.m_Normal;
		}
		private set
		{
			this.m_Normal = value;
		}
	}

	// Token: 0x17000165 RID: 357
	// (get) Token: 0x06000AA8 RID: 2728 RVA: 0x0002EC20 File Offset: 0x0002CE20
	public Vector2 DragVector
	{
		get
		{
			return new Vector2((float)(this.m_End.x - this.m_Start.x), (float)(this.m_End.y - this.m_Start.y));
		}
	}

	// Token: 0x17000166 RID: 358
	// (get) Token: 0x06000AA9 RID: 2729 RVA: 0x0002EC58 File Offset: 0x0002CE58
	public SLIDE_VECTOR SlideDragVector
	{
		get
		{
			Vector2 dragVector = this.DragVector;
			if (Mathf.Abs(dragVector.x) > Mathf.Abs(dragVector.y))
			{
				return (dragVector.x >= 0f) ? SLIDE_VECTOR.RIGHT : SLIDE_VECTOR.LEFT;
			}
			return (dragVector.y >= 0f) ? SLIDE_VECTOR.DOWN : SLIDE_VECTOR.UP;
		}
	}

	// Token: 0x17000167 RID: 359
	// (get) Token: 0x06000AAA RID: 2730 RVA: 0x0002ECBC File Offset: 0x0002CEBC
	public float StartDeltaMagnitude
	{
		get
		{
			return base.StartDeltaPosition.magnitude;
		}
	}

	// Token: 0x06000AAB RID: 2731 RVA: 0x0002ECD8 File Offset: 0x0002CED8
	private new void Awake()
	{
		base.Awake();
		this.ResetScreenRect();
	}

	// Token: 0x06000AAC RID: 2732 RVA: 0x0002ECE8 File Offset: 0x0002CEE8
	private new void Update()
	{
		base.Update();
		TOUCH_STATE state = base.State;
		Vector2 normalized = base.StartDeltaPosition.normalized;
		this.Slide = SLIDE_VECTOR.NONE;
		this.Normal = default(Vector2);
		this.Start = new Point2(0, 0);
		this.End = new Point2(0, 0);
		this.Delta = new Point2(0, 0);
		switch (state)
		{
		case TOUCH_STATE.NULL:
			this.Status = INPUT_STATUS.NONE;
			break;
		case TOUCH_STATE.PUSH:
			if (this.Status != INPUT_STATUS.DRAG_ON)
			{
				this.Status = INPUT_STATUS.PUSH;
			}
			this.Start = this.DPToSp(base.StartPosition);
			break;
		case TOUCH_STATE.CLICK:
			this.Status = INPUT_STATUS.CLICK;
			this.Start = this.DPToSp(base.StartPosition);
			this.End = this.DPToSp(base.EndPosition);
			this.Delta.x = this.Start.x - this.End.x;
			this.Delta.y = this.Start.y - this.End.y;
			break;
		case TOUCH_STATE.DRAG:
			this.Status = INPUT_STATUS.DRAG_ON;
			this.Normal = normalized;
			this.SetParam(true);
			break;
		case TOUCH_STATE.DRAG_END:
			this.Status = INPUT_STATUS.DRAG;
			this.SetParam(false);
			break;
		case TOUCH_STATE.FLICK:
			this.Status = INPUT_STATUS.FLICK;
			this.SetParam(false);
			break;
		}
		KsInput.Update();
	}

	// Token: 0x06000AAD RID: 2733 RVA: 0x0002EE68 File Offset: 0x0002D068
	private void SetParam(bool bDrag = false)
	{
		this.Normal = base.StartDeltaPosition.normalized;
		this.Start = this.DPToSp(base.StartPosition);
		if (bDrag)
		{
			this.End = this.DPToSp(base.PushPosition);
		}
		else
		{
			this.End = this.DPToSp(base.EndPosition);
		}
		this.Delta.x = this.Start.x - this.End.x;
		this.Delta.y = this.Start.y - this.End.y;
		if (this.Normal.x > 0.7f)
		{
			this.Slide = SLIDE_VECTOR.RIGHT;
		}
		else if (this.Normal.x < -0.7f)
		{
			this.Slide = SLIDE_VECTOR.LEFT;
		}
		else if (this.Normal.y > 0.7f)
		{
			this.Slide = SLIDE_VECTOR.DOWN;
		}
		else if (this.Normal.y < -0.7f)
		{
			this.Slide = SLIDE_VECTOR.UP;
		}
	}

	// Token: 0x06000AAE RID: 2734 RVA: 0x0002EF9C File Offset: 0x0002D19C
	private Point2 DPToSp(Vector2 posDp)
	{
		float num = (posDp.x - (float)this.m_Base.x) / (float)this.m_Size.w;
		float num2 = (posDp.y - (float)this.m_Base.y) / (float)this.m_Size.h;
		float num3 = (float)this.m_ScrSize.w;
		float num4 = (float)this.m_ScrSize.h;
		return new Point2((int)(num * num3), (int)(num2 * num4));
	}

	// Token: 0x06000AAF RID: 2735 RVA: 0x0002F014 File Offset: 0x0002D214
	public void ResetScreenRect()
	{
		this.SetScreenRect(0, 0, Screen.width, Screen.height, 960, 544);
	}

	// Token: 0x06000AB0 RID: 2736 RVA: 0x0002F040 File Offset: 0x0002D240
	public void SetScreenRect(int x, int y, int w, int h, int dw, int dh)
	{
		this.m_Base = new Point2(x, y);
		this.m_Size = new Size(w, h);
		this.m_ScrSize = new Size(dw, dh);
	}

	// Token: 0x04000895 RID: 2197
	private const float VEC_THRESHOLD = 0.7f;

	// Token: 0x04000896 RID: 2198
	private INPUT_STATUS m_eStatus;

	// Token: 0x04000897 RID: 2199
	private SLIDE_VECTOR m_eSlide;

	// Token: 0x04000898 RID: 2200
	private Point2 m_Start = new Point2(0, 0);

	// Token: 0x04000899 RID: 2201
	private Point2 m_End = new Point2(0, 0);

	// Token: 0x0400089A RID: 2202
	private Point2 m_Delta = new Point2(0, 0);

	// Token: 0x0400089B RID: 2203
	private float m_Accel;

	// Token: 0x0400089C RID: 2204
	private Vector2 m_Normal = default(Vector2);

	// Token: 0x0400089D RID: 2205
	private Point2 m_Base = new Point2(0, 0);

	// Token: 0x0400089E RID: 2206
	private Size m_Size = new Size(Screen.width, Screen.height);

	// Token: 0x0400089F RID: 2207
	private Size m_ScrSize = new Size(Screen.width, Screen.height);
}
