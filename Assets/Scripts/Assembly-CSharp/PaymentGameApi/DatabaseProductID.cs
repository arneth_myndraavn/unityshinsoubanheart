﻿using System;
using System.Collections.Generic;
using Game;
using Qoo.Game;

namespace PaymentGameApi
{
	// Token: 0x020000E0 RID: 224
	public class DatabaseProductID
	{
		// Token: 0x0600061E RID: 1566 RVA: 0x00019878 File Offset: 0x00017A78
		public DatabaseProductID()
		{
			this.dic = new Dictionary<string, DatabaseProductID.PairCharID_CheckType>();
			for (CHAR_ID char_ID = CHAR_ID.BLOOD; char_ID <= CHAR_ID.NIGHTMARE; char_ID++)
			{
				for (DatabaseProductID.CheckType checkType = DatabaseProductID.CheckType.ROUTE_VOICE; checkType <= DatabaseProductID.CheckType.VOICE; checkType++)
				{
					this.dic.Add(this.GetAddonName(char_ID, checkType), new DatabaseProductID.PairCharID_CheckType(char_ID, checkType));
				}
			}
		}

		// Token: 0x0600061F RID: 1567 RVA: 0x000198D8 File Offset: 0x00017AD8
		public string GetAddonName(CHAR_ID charId, DatabaseProductID.CheckType checkType)
		{
			return string.Concat(new string[]
			{
				this.GetAddonCompanyName(),
				this.GetAddonGameName(),
				this.GetAddonPrefixName(charId),
				this.GetAddonCharName(charId),
				this.GetAddonTypeName(checkType)
			});
		}

		// Token: 0x06000620 RID: 1568 RVA: 0x00019920 File Offset: 0x00017B20
		private string GetAddonCompanyName()
		{
			return "com.quinrose.";
		}

		// Token: 0x06000621 RID: 1569 RVA: 0x00019928 File Offset: 0x00017B28
		private string GetAddonGameName()
		{
			return "aliceheart.";
		}

		// Token: 0x06000622 RID: 1570 RVA: 0x00019930 File Offset: 0x00017B30
		private string GetAddonPrefixName(CHAR_ID charId)
		{
			switch (charId)
			{
			case CHAR_ID.BLOOD:
			case CHAR_ID.ELLIOT:
			case CHAR_ID.DEEDUM:
			case CHAR_ID.VIVALDI:
			case CHAR_ID.PETER:
			case CHAR_ID.ACE:
			case CHAR_ID.GOWLAND:
			case CHAR_ID.BORIS:
			case CHAR_ID.JULIUS:
				return "t1.";
			default:
				return "t0.";
			}
		}

		// Token: 0x06000623 RID: 1571 RVA: 0x0001997C File Offset: 0x00017B7C
		private string GetAddonCharName(CHAR_ID charId)
		{
			switch (charId)
			{
			case CHAR_ID.BLOOD:
			case CHAR_ID.ELLIOT:
			case CHAR_ID.DEEDUM:
			case CHAR_ID.VIVALDI:
			case CHAR_ID.PETER:
			case CHAR_ID.ACE:
			case CHAR_ID.GOWLAND:
			case CHAR_ID.BORIS:
			case CHAR_ID.JULIUS:
				return Chara.GetNameFromId(charId) + ".";
			default:
				return "full.";
			}
		}

		// Token: 0x06000624 RID: 1572 RVA: 0x000199D0 File Offset: 0x00017BD0
		private string GetAddonTypeName(DatabaseProductID.CheckType checkType)
		{
			switch (checkType)
			{
			case DatabaseProductID.CheckType.ROUTE_VOICE:
				return "routevoice";
			case DatabaseProductID.CheckType.ROUTE:
				return "route";
			case DatabaseProductID.CheckType.VOICE:
				return "voice";
			default:
				return string.Empty;
			}
		}

		// Token: 0x06000625 RID: 1573 RVA: 0x00019A10 File Offset: 0x00017C10
		public CHAR_ID GetCharaIdForProductId(string ProductId)
		{
			if (!this.dic.ContainsKey(ProductId))
			{
				return CHAR_ID.NOTHING;
			}
			return this.dic[ProductId].charId;
		}

		// Token: 0x06000626 RID: 1574 RVA: 0x00019A44 File Offset: 0x00017C44
		public DatabaseProductID.CheckType GetCheckTypeForProductId(string ProductId)
		{
			if (!this.dic.ContainsKey(ProductId))
			{
				return DatabaseProductID.CheckType.ROUTE_VOICE;
			}
			return this.dic[ProductId].checkType;
		}

		// Token: 0x06000627 RID: 1575 RVA: 0x00019A78 File Offset: 0x00017C78
		public string[] GetAllProductId()
		{
			List<string> list = new List<string>();
			for (CHAR_ID char_ID = CHAR_ID.BLOOD; char_ID <= CHAR_ID.NIGHTMARE; char_ID++)
			{
				list.Add(this.GetAddonName(char_ID, DatabaseProductID.CheckType.ROUTE_VOICE));
				list.Add(this.GetAddonName(char_ID, DatabaseProductID.CheckType.ROUTE));
				list.Add(this.GetAddonName(char_ID, DatabaseProductID.CheckType.VOICE));
			}
			return list.ToArray();
		}

		// Token: 0x040005AD RID: 1453
		private Dictionary<string, DatabaseProductID.PairCharID_CheckType> dic;

		// Token: 0x020000E1 RID: 225
		public enum CheckType
		{
			// Token: 0x040005AF RID: 1455
			ROUTE_VOICE,
			// Token: 0x040005B0 RID: 1456
			ROUTE,
			// Token: 0x040005B1 RID: 1457
			VOICE
		}

		// Token: 0x020000E2 RID: 226
		private struct PairCharID_CheckType
		{
			// Token: 0x06000628 RID: 1576 RVA: 0x00019AD0 File Offset: 0x00017CD0
			public PairCharID_CheckType(CHAR_ID charId_, DatabaseProductID.CheckType checkType_)
			{
				this.charId = charId_;
				this.checkType = checkType_;
			}

			// Token: 0x040005B2 RID: 1458
			public CHAR_ID charId;

			// Token: 0x040005B3 RID: 1459
			public DatabaseProductID.CheckType checkType;
		}
	}
}
