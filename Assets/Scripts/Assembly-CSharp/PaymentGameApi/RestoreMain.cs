﻿using System;
using Qoo;

namespace PaymentGameApi
{
	// Token: 0x020000E3 RID: 227
	public class RestoreMain
	{
		// Token: 0x06000629 RID: 1577 RVA: 0x00019AE0 File Offset: 0x00017CE0
		public RestoreMain()
		{
			this.m_eResut = AMAppStore.ResultStatus.RESULT_FAILD;
		}

		// Token: 0x0600062A RID: 1578 RVA: 0x00019AF0 File Offset: 0x00017CF0
		public void Init()
		{
			DatabaseProductID databaseProductID = new DatabaseProductID();
			AMAppStore.RequestRestore(databaseProductID.GetAllProductId());
		}

		// Token: 0x0600062B RID: 1579 RVA: 0x00019B10 File Offset: 0x00017D10
		public bool Exec()
		{
			return AMAppStore.ResultWaitRestore(ref this.m_eResut);
		}

		// Token: 0x0600062C RID: 1580 RVA: 0x00019B20 File Offset: 0x00017D20
		public AMAppStore.ResultStatus Result()
		{
			return this.m_eResut;
		}

		// Token: 0x0600062D RID: 1581 RVA: 0x00019B28 File Offset: 0x00017D28
		public string ResultMessage()
		{
			Debug.Print(AMAppStore.GetErrorRestore());
			return (this.m_eResut != AMAppStore.ResultStatus.RESULT_SUCCESS) ? "通信に失敗しました。\n通信状態の良いところで再度接続を試みてください。" : "リストア（アドオン情報の復元）が完了しました。";
		}

		// Token: 0x040005B4 RID: 1460
		private AMAppStore.ResultStatus m_eResut;
	}
}
