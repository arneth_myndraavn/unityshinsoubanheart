﻿using System;
using Qoo;

namespace PaymentGameApi
{
	// Token: 0x020000E4 RID: 228
	public class PaymentInit
	{
		// Token: 0x0600062E RID: 1582 RVA: 0x00019B5C File Offset: 0x00017D5C
		public PaymentInit()
		{
			this.m_eResut = AMAppStore.ResultStatus.RESULT_FAILD;
		}

		// Token: 0x0600062F RID: 1583 RVA: 0x00019B6C File Offset: 0x00017D6C
		public void Init()
		{
			AMAppStore.Init(string.Empty);
		}

		// Token: 0x06000630 RID: 1584 RVA: 0x00019B7C File Offset: 0x00017D7C
		public bool Exec()
		{
			return AMAppStore.ResultWaitInit(ref this.m_eResut);
		}

		// Token: 0x06000631 RID: 1585 RVA: 0x00019B8C File Offset: 0x00017D8C
		public AMAppStore.ResultStatus Result()
		{
			return this.m_eResut;
		}

		// Token: 0x06000632 RID: 1586 RVA: 0x00019B94 File Offset: 0x00017D94
		public string ResultMessage()
		{
			Debug.Print(AMAppStore.GetErrorInit());
			return (this.m_eResut != AMAppStore.ResultStatus.RESULT_SUCCESS) ? "通信に失敗しました。\n通信状態の良いところで再度接続を試みてください。" : " ";
		}

		// Token: 0x040005B5 RID: 1461
		private AMAppStore.ResultStatus m_eResut;
	}
}
