﻿using System;
using System.Collections;
using UnityEngine;

// Token: 0x020000F2 RID: 242
public class AdvMenuTask : SubPartTask
{
	// Token: 0x06000685 RID: 1669 RVA: 0x0001B03C File Offset: 0x0001923C
	public IEnumerator Open(Texture tex, bool isNormal)
	{
		AdventureMenuWindow.SetSubTaskParam(tex, isNormal);
		yield return base.Open(UIValue.SCENE_ADVMENU, false);
		yield break;
	}
}
