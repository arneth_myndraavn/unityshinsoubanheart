﻿using System;
using System.Collections;
using UnityEngine;

// Token: 0x02000140 RID: 320
public class ScreenEffect
{
	// Token: 0x1700010D RID: 269
	// (get) Token: 0x060008E9 RID: 2281 RVA: 0x00027300 File Offset: 0x00025500
	// (set) Token: 0x060008EA RID: 2282 RVA: 0x00027308 File Offset: 0x00025508
	public static bool FadeExec
	{
		get
		{
			return ScreenEffect.fadeExec;
		}
		set
		{
			ScreenEffect.fadeExec = value;
		}
	}

	// Token: 0x060008EB RID: 2283 RVA: 0x00027310 File Offset: 0x00025510
	public static IEnumerator FadeBlackOut(float sec)
	{
		return ScreenEffect.FadeMain(sec, new Color(0f, 0f, 0f, 0f), new Color(0f, 0f, 0f, 1f));
	}

	// Token: 0x060008EC RID: 2284 RVA: 0x00027358 File Offset: 0x00025558
	public static IEnumerator FadeBlackIn(float sec)
	{
		return ScreenEffect.FadeMain(sec, new Color(0f, 0f, 0f, 1f), new Color(0f, 0f, 0f, 0f));
	}

	// Token: 0x060008ED RID: 2285 RVA: 0x000273A0 File Offset: 0x000255A0
	private static IEnumerator FadeMain(float sec, Color src, Color dst)
	{
		AnimationRange_Color work = new AnimationRange_Color(1, sec, 0f, src, dst);
		while (work.Active)
		{
			if (ScreenEffect.goScreenEffect == null)
			{
				ScreenEffect.Init(99, Color.black, null);
			}
			ScreenEffect.goScreenEffect.GetComponent<Renderer>().material.SetColor("_Color", work.Offset);
			work.Update();
			yield return 0;
		}
		ScreenEffect.fadeExec = false;
		yield break;
	}

	// Token: 0x060008EE RID: 2286 RVA: 0x000273E0 File Offset: 0x000255E0
	public static void Init(int z, Color color, Transform parent = null)
	{
		ScreenEffect.goScreenEffect = GameObject.Find("__ScreenEffenct");
		if (ScreenEffect.goScreenEffect == null)
		{
			ScreenEffect.goScreenEffect = GameObject.CreatePrimitive(PrimitiveType.Quad);
			ScreenEffect.goScreenEffect.GetComponent<Renderer>().material = new Material(Resources.Load("Shader/Color/UnlitColor") as Shader);
			ScreenEffect.goScreenEffect.transform.localScale = new Vector3(960f, 544f, 1f);
			ScreenEffect.goScreenEffect.name = "__ScreenEffenct";
		}
		ScreenEffect.goScreenEffect.transform.parent = parent;
		if (parent != null)
		{
			ScreenEffect.goScreenEffect.transform.localPosition = new Vector3(0f, 0f, (float)(-(float)z));
		}
		else
		{
			//ScreenEffect.goScreenEffect.transform.localPosition = new Vector3(2000f, 0f, (float)(-(float)z));
			ScreenEffect.goScreenEffect.transform.localPosition = new Vector3(0f, 0f, (float)(-(float)z));
		}
		ScreenEffect.goScreenEffect.GetComponent<Renderer>().material.SetColor("_Color", color);
	}

	// Token: 0x060008EF RID: 2287 RVA: 0x000274EC File Offset: 0x000256EC
	public static void Term()
	{
		if (ScreenEffect.goScreenEffect != null)
		{
			UnityEngine.Object.Destroy(ScreenEffect.goScreenEffect);
			ScreenEffect.goScreenEffect = null;
		}
	}

	// Token: 0x0400078E RID: 1934
	private static GameObject goScreenEffect;

	// Token: 0x0400078F RID: 1935
	private static bool fadeExec;
}
