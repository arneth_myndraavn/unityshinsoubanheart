﻿using System;
using UnityEngine;

// Token: 0x02000194 RID: 404
public class AnimationRange
{
	// Token: 0x06000B90 RID: 2960 RVA: 0x0003107C File Offset: 0x0002F27C
	public AnimationRange(int repeat_, float speed_, float delay_, MoveType moveType_)
	{
		this.repeat = repeat_;
		this.speed = speed_;
		this.delay = delay_;
		this.moveType = moveType_;
		this.active = true;
		this.elapsedTime = 0f;
		this.rate = 0f;
	}

	// Token: 0x17000189 RID: 393
	// (get) Token: 0x06000B91 RID: 2961 RVA: 0x000310CC File Offset: 0x0002F2CC
	public bool Active
	{
		get
		{
			return this.active;
		}
	}

	// Token: 0x1700018A RID: 394
	// (get) Token: 0x06000B92 RID: 2962 RVA: 0x000310D4 File Offset: 0x0002F2D4
	public float Rate
	{
		get
		{
			return this.rate;
		}
	}

	// Token: 0x1700018B RID: 395
	// (get) Token: 0x06000B93 RID: 2963 RVA: 0x000310DC File Offset: 0x0002F2DC
	// (set) Token: 0x06000B94 RID: 2964 RVA: 0x000310E4 File Offset: 0x0002F2E4
	public float Speed
	{
		get
		{
			return this.speed;
		}
		set
		{
			this.speed = value;
		}
	}

	// Token: 0x06000B95 RID: 2965 RVA: 0x000310F0 File Offset: 0x0002F2F0
	public void Update()
	{
		if (!this.active)
		{
			return;
		}
		this.elapsedTime += Time.deltaTime;
		if (this.repeat != 0 && this.elapsedTime > this.delay + (float)this.repeat * this.speed)
		{
			this.active = false;
			this.rate = 1f;
		}
		else
		{
			float num = this.elapsedTime - this.delay;
			if (num < 0f)
			{
				num = 0f;
			}
			else
			{
				while (num > this.speed)
				{
					num -= this.speed;
				}
				if (this.repeat == 0)
				{
					this.elapsedTime = num + this.delay;
				}
			}
			this.rate = this.calcRate(num);
		}
	}

	// Token: 0x06000B96 RID: 2966 RVA: 0x000311C4 File Offset: 0x0002F3C4
	public void Restart()
	{
		this.active = true;
		this.elapsedTime = 0f;
		this.rate = 0f;
	}

	// Token: 0x06000B97 RID: 2967 RVA: 0x000311E4 File Offset: 0x0002F3E4
	private float calcRate(float nowTime)
	{
		switch (this.moveType)
		{
		case MoveType.OneshotLiner:
			return nowTime / this.speed;
		case MoveType.LoopSin000to180:
			return Mathf.Sin(3.1415927f * nowTime / this.speed);
		case MoveType.OneshotCos180to360:
			return Mathf.Cos((nowTime / this.speed + 1f) * 3.1415927f) / 2f + 0.5f;
		case MoveType.OneshotSin000to090:
			return Mathf.Sin(3.1415927f * nowTime / this.speed / 2f);
		default:
			return nowTime / this.speed;
		}
	}

	// Token: 0x04000912 RID: 2322
	private int repeat;

	// Token: 0x04000913 RID: 2323
	private float speed;

	// Token: 0x04000914 RID: 2324
	private float delay;

	// Token: 0x04000915 RID: 2325
	private MoveType moveType;

	// Token: 0x04000916 RID: 2326
	private bool active;

	// Token: 0x04000917 RID: 2327
	private float elapsedTime;

	// Token: 0x04000918 RID: 2328
	private float rate;
}
