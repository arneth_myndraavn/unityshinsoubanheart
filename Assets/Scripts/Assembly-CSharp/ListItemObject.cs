﻿using System;
using System.Collections;
using UnityEngine;

// Token: 0x020000E8 RID: 232
public class ListItemObject : MonoBehaviour
{
	// Token: 0x170000D8 RID: 216
	// (get) Token: 0x06000642 RID: 1602 RVA: 0x00019FF8 File Offset: 0x000181F8
	// (set) Token: 0x06000643 RID: 1603 RVA: 0x0001A000 File Offset: 0x00018200
	public bool IsAreaOver { get; set; }

	// Token: 0x06000644 RID: 1604 RVA: 0x0001A00C File Offset: 0x0001820C
	public static ListItemObject Create(ImageObject imageObject)
	{
		ListItemObject listItemObject = imageObject.gameObject.AddComponent<ListItemObject>();
		listItemObject.m_ImageObject = imageObject;
		listItemObject.StartCoroutine(listItemObject.WaitLoadTextureCoroutine());
		return listItemObject;
	}

	// Token: 0x06000645 RID: 1605 RVA: 0x0001A03C File Offset: 0x0001823C
	public void SetArea(float top, float bot)
	{
		int num = 272;
		this.Top = (float)num - top;
		this.Bot = (float)num - bot;
	}

	// Token: 0x06000646 RID: 1606 RVA: 0x0001A064 File Offset: 0x00018264
	private IEnumerator WaitLoadTextureCoroutine()
	{
		while (base.GetComponent<Renderer>().material.mainTexture == null)
		{
			yield return 0;
		}
		this.Init();
		yield break;
	}

	// Token: 0x06000647 RID: 1607 RVA: 0x0001A080 File Offset: 0x00018280
	private void Awake()
	{
	}

	// Token: 0x06000648 RID: 1608 RVA: 0x0001A084 File Offset: 0x00018284
	private void Update()
	{
		base.transform.localPosition = this.m_ImageObject.OriginalPosition;
		base.transform.localScale = this.m_ImageObject.OriginalScale;
		this.ListViewPosition = this.m_ImageObject.OnViewPosition;
		this.ListViewScale = this.m_ImageObject.OnViewScale;
		this.IsAreaOver = false;
		this.isBotOver = false;
		this.scaling();
		this.positioning();
		this.moveUV();
		base.transform.localPosition = this.ListViewPosition;
		base.transform.localScale = this.ListViewScale;
	}

	// Token: 0x06000649 RID: 1609 RVA: 0x0001A124 File Offset: 0x00018324
	public void Init()
	{
		Vector3 localScale = new Vector3(0f, 0f, 1f);
		this.m_ImageObject.OriginalScale = base.transform.localScale;
		this.m_ImageObject.OriginalPosition = base.transform.localPosition;
		this.m_ImageObject.OnViewPosition = this.m_ImageObject.OriginalPosition;
		this.m_ImageObject.OnViewScale = this.m_ImageObject.OriginalScale;
		if (base.GetComponent<Renderer>().material.mainTexture != null)
		{
			localScale.x = (float)base.GetComponent<Renderer>().material.mainTexture.width;
			localScale.y = (float)base.GetComponent<Renderer>().material.mainTexture.height;
			base.transform.localScale = localScale;
			this.scaling();
			this.positioning();
			this.moveUV();
		}
	}

	// Token: 0x0600064A RID: 1610 RVA: 0x0001A214 File Offset: 0x00018414
	private void scaling()
	{
		Vector3 position = base.transform.position;
		Vector3 listViewScale = this.ListViewScale;
		this.myTop = position.y + listViewScale.y / 2f;
		this.myBot = position.y - listViewScale.y / 2f;
		this.over = 0f;
		if (this.myTop > this.Top)
		{
			this.over = this.Top - this.myTop;
			this.IsAreaOver = true;
		}
		else if (this.myBot < this.Bot)
		{
			this.over = this.myBot - this.Bot;
			this.IsAreaOver = true;
			this.isBotOver = true;
		}
		listViewScale.y = this.m_ImageObject.OriginalScale.y + this.over;
		if (listViewScale.y > this.m_ImageObject.OriginalScale.y)
		{
			listViewScale.y = this.m_ImageObject.OriginalScale.y;
		}
		if (listViewScale.y < 0f)
		{
			listViewScale.y = 0f;
		}
		this.ListViewScale = listViewScale;
	}

	// Token: 0x0600064B RID: 1611 RVA: 0x0001A34C File Offset: 0x0001854C
	private void positioning()
	{
		float num = this.over / 2f;
		if (this.isBotOver)
		{
			num *= -1f;
		}
		this.ListViewPosition.y = this.m_ImageObject.OnViewPosition.y + num;
	}

	// Token: 0x0600064C RID: 1612 RVA: 0x0001A398 File Offset: 0x00018598
	private void moveUV()
	{
		float num = this.ListViewScale.y / this.m_ImageObject.OriginalScale.y;
		if (this.isBotOver)
		{
			base.GetComponent<Renderer>().material.SetVector("_UVWH", new Vector4(0f, 1f - num, 1f, num));
		}
		else
		{
			base.GetComponent<Renderer>().material.SetVector("_UVWH", new Vector4(0f, 0f, 1f, num));
		}
	}

	// Token: 0x040005BA RID: 1466
	private ImageObject m_ImageObject;

	// Token: 0x040005BB RID: 1467
	public float Top;

	// Token: 0x040005BC RID: 1468
	public float Bot;

	// Token: 0x040005BD RID: 1469
	public Vector3 ListViewPosition;

	// Token: 0x040005BE RID: 1470
	public Vector3 ListViewScale;

	// Token: 0x040005BF RID: 1471
	private Vector3 StartPosition;

	// Token: 0x040005C0 RID: 1472
	private Vector3 StartInputPosition;

	// Token: 0x040005C1 RID: 1473
	private float over;

	// Token: 0x040005C2 RID: 1474
	private float myTop;

	// Token: 0x040005C3 RID: 1475
	private float myBot;

	// Token: 0x040005C4 RID: 1476
	private bool isBotOver;
}
