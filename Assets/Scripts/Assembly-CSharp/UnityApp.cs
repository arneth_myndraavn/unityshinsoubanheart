﻿using System;
using System.Collections;
using Qoo;
using Qoo.Application;
using Qoo.File;
using Qoo.Ks;
using UnityEngine;

// Token: 0x0200013C RID: 316
public class UnityApp : Singleton<UnityApp>
{
	// Token: 0x170000F5 RID: 245
	// (get) Token: 0x06000894 RID: 2196 RVA: 0x000264A0 File Offset: 0x000246A0
	public bool IsInit
	{
		get
		{
			return this.m_isInit;
		}
	}

	// Token: 0x06000895 RID: 2197 RVA: 0x000264A8 File Offset: 0x000246A8
	private void Awake()
	{
		UnityEngine.Object.DontDestroyOnLoad(this);
		Application.targetFrameRate = 60;
		this.preferredFontSize = 28;
        Application.runInBackground = true;
        //Screen.SetResolution(1024, 576, false, 60);
        this.task = base.gameObject.AddComponent<TaskManager>();
		this.time = base.gameObject.AddComponent<UnityTimer>();
		this.file = base.gameObject.AddComponent<UnityFileLoader>();
		this.input = base.gameObject.AddComponent<GameInput>();
		this.graph = base.gameObject.AddComponent<UnityGraph>();
		this.sound = base.gameObject.AddComponent<ManSound>();
		UnityTask.SetMainTask(this.InitData());
		this.m_isInit = false;
	}

	// Token: 0x06000896 RID: 2198 RVA: 0x00026534 File Offset: 0x00024734
	private void Update()
	{
	}

	// Token: 0x06000897 RID: 2199 RVA: 0x00026538 File Offset: 0x00024738
	private void OnEnable()
	{
		//AMAppStore.RegistrationEvent();
	}

	// Token: 0x06000898 RID: 2200 RVA: 0x00026540 File Offset: 0x00024740
	private void OnDisable()
	{
		//AMAppStore.DeleteEvent();
	}

	// Token: 0x06000899 RID: 2201 RVA: 0x00026548 File Offset: 0x00024748
	private void OnApplicationFocus(bool isFocus)
	{
		if (isFocus)
		{
			Qoo.Debug.Print("APP Focus!!");
		}
		else
		{
			Qoo.Debug.Print("APP FocusLost!!");
		}
	}

	// Token: 0x0600089A RID: 2202 RVA: 0x0002656C File Offset: 0x0002476C
	private void OnApplicationPause(bool pauseStatus)
	{
		if (pauseStatus)
		{
			Qoo.Debug.Print("APP PAUSE!!");
			UnityApp.AutoSave();
		}
		else
		{
			Qoo.Debug.Print("APP Resume!!");
		}
	}

	// Token: 0x0600089B RID: 2203 RVA: 0x000265A0 File Offset: 0x000247A0
	private void OnApplicationQuit()
	{
		Qoo.Debug.Print("APP QUIT!!");
		UnityApp.AutoSave();
		//AMAppStore.End();
	}

	// Token: 0x0600089C RID: 2204 RVA: 0x000265B8 File Offset: 0x000247B8
	private IEnumerator InitData()
	{
		this.m_LoadQueue.LoadOther("data0.nmb", "/nmb/");
		this.m_LoadQueue.LoadOther("data1.nmb", "/nmb/");
		while (!this.m_LoadQueue.IsReadEndAll())
		{
			yield return 0;
		}
		this.CreateNmb();
		this.m_LoadQueue.LoadOther("kslist.akb", "/ks/");
		while (!this.m_LoadQueue.IsReadEndAll())
		{
			yield return 0;
		}
		this.CreateAkb();
		this.m_LoadQueue.LoadOther("h2m_font.fnt", "/font/");
		this.m_LoadQueue.LoadResource("h2m_font_0", "font/");
		while (!this.m_LoadQueue.IsReadEndAll())
		{
			yield return 0;
		}
		this.CreateFontFile();
		App.Init();
		this.m_isInit = true;
		CSVManager.init();
		Qoo.Debug.Print("---Init End---");
		if (UnityEngine.Debug.isDebugBuild)
		{
			SceneManager.ChangeScene(this.InitDebugScene);
		}
		else
		{
			SceneManager.ChangeScene(this.InitScene);
		}
		GameObject lcamera = GameObject.Find("LoadingCamera");
		if (lcamera != null)
		{
			UnityEngine.Object.Destroy(lcamera);
			lcamera = null;
		}
		GameObject lsprite = GameObject.Find("LoadingSprite");
		if (lsprite != null)
		{
			UnityEngine.Object.Destroy(lsprite);
			lsprite = null;
		}
		yield break;
	}

	// Token: 0x0600089D RID: 2205 RVA: 0x000265D4 File Offset: 0x000247D4
	public int GetTimeMilli()
	{
		return this.time.GetTimeMilli();
	}

	// Token: 0x0600089E RID: 2206 RVA: 0x000265E4 File Offset: 0x000247E4
	public int FrameUpdate()
	{
		return UnityTask.FrameUpdate();
	}

	// Token: 0x0600089F RID: 2207 RVA: 0x000265EC File Offset: 0x000247EC
	private void CreateAkb()
	{
		Akb.Create(this.m_LoadQueue.GetData("kslist.akb"));
		this.m_LoadQueue.Remove("kslist.akb");
	}

	// Token: 0x060008A0 RID: 2208 RVA: 0x00026620 File Offset: 0x00024820
	private void CreateNmb()
	{
		Nmb.Add(this.m_LoadQueue.GetData("data0.nmb"));
		Nmb.Add(this.m_LoadQueue.GetData("data1.nmb"));
		this.m_LoadQueue.Remove("data0.nmb");
		this.m_LoadQueue.Remove("data1.nmb");
	}

	// Token: 0x060008A1 RID: 2209 RVA: 0x0002667C File Offset: 0x0002487C
	private void CreateFontFile()
	{
		this.graph.CreateFont("FOT-ニューロダン Pro B", this.m_LoadQueue.GetData("h2m_font.fnt"), this.graph.m_ManSprite.SetTexture("h2m_font_0", this.m_LoadQueue.GetObject("h2m_font_0") as Texture2D));
		this.m_LoadQueue.Remove("h2m_font.fnt");
		this.m_LoadQueue.Remove("h2m_font_0");
	}

	// Token: 0x060008A2 RID: 2210 RVA: 0x000266F8 File Offset: 0x000248F8
	public static void AutoSave()
	{
		if (App.IsInit)
		{
			SaveLoadManager.SaveSystem();
			GameObject gameObject = GameObject.Find("SceneAdvMode");
			if (gameObject != null)
			{
				EventWnd component = gameObject.GetComponent<EventWnd>();
				if (component != null && component.Mode == PLAY_MODE.NORMAL && !component.Player.RestoreOn)
				{
					SaveLoadManager.SaveAuto();
					Qoo.Debug.Print("Auto Save");
				}
			}
		}
	}

	// Token: 0x060008A3 RID: 2211 RVA: 0x00026770 File Offset: 0x00024970
	public static int GetOSVersion()
	{
		return 0;
	}

	// Token: 0x060008A4 RID: 2212 RVA: 0x00026774 File Offset: 0x00024974
	public static void CleanMemory()
	{
		Resources.UnloadUnusedAssets();
		GC.Collect();
		GC.WaitForPendingFinalizers();
		GC.Collect();
	}

	// Token: 0x170000F6 RID: 246
	// (get) Token: 0x060008A5 RID: 2213 RVA: 0x0002678C File Offset: 0x0002498C
	public static UnityApp PlatformApp
	{
		get
		{
			return Singleton<UnityApp>.Instance;
		}
	}

	// Token: 0x170000F7 RID: 247
	// (get) Token: 0x060008A6 RID: 2214 RVA: 0x00026794 File Offset: 0x00024994
	public static ManSound Sound
	{
		get
		{
			return Singleton<UnityApp>.Instance.sound;
		}
	}

	// Token: 0x170000F8 RID: 248
	// (get) Token: 0x060008A7 RID: 2215 RVA: 0x000267A0 File Offset: 0x000249A0
	public static UnityGraph Graph
	{
		get
		{
			return Singleton<UnityApp>.Instance.graph;
		}
	}

	// Token: 0x170000F9 RID: 249
	// (get) Token: 0x060008A8 RID: 2216 RVA: 0x000267AC File Offset: 0x000249AC
	public static Man2D Sprite
	{
		get
		{
			return Singleton<UnityApp>.Instance.graph.m_ManSprite;
		}
	}

	// Token: 0x170000FA RID: 250
	// (get) Token: 0x060008A9 RID: 2217 RVA: 0x000267C0 File Offset: 0x000249C0
	public static TaskManager Task
	{
		get
		{
			return Singleton<UnityApp>.Instance.task;
		}
	}

	// Token: 0x170000FB RID: 251
	// (get) Token: 0x060008AA RID: 2218 RVA: 0x000267CC File Offset: 0x000249CC
	public static UnityFileLoader File
	{
		get
		{
			return Singleton<UnityApp>.Instance.file;
		}
	}

	// Token: 0x170000FC RID: 252
	// (get) Token: 0x060008AB RID: 2219 RVA: 0x000267D8 File Offset: 0x000249D8
	public static GameInput Input
	{
		get
		{
			return Singleton<UnityApp>.Instance.input;
		}
	}

	// Token: 0x0400076B RID: 1899
	private TaskManager task;

	// Token: 0x0400076C RID: 1900
	private UnityFileLoader file;

	// Token: 0x0400076D RID: 1901
	private UnityTimer time;

	// Token: 0x0400076E RID: 1902
	private UnityGraph graph;

	// Token: 0x0400076F RID: 1903
	private ManSound sound;

	// Token: 0x04000770 RID: 1904
	private GameInput input;

	// Token: 0x04000771 RID: 1905
	private UnityFileLoaderQueue m_LoadQueue = new UnityFileLoaderQueue();

	// Token: 0x04000772 RID: 1906
	public string InitScene;

	// Token: 0x04000773 RID: 1907
	public string InitDebugScene;

	// Token: 0x04000774 RID: 1908
	private bool m_isInit;

	public bool isJapanese;

    public int preferredFontSize = 28;
}
