﻿using System;

namespace NsQT
{
	// Token: 0x0200009E RID: 158
	public class CQK3Node
	{
		// Token: 0x06000465 RID: 1125 RVA: 0x000103EC File Offset: 0x0000E5EC
		public CQK3Node()
		{
			this.m_nType = QK3_NODETYPE.INT;
			this.Zero();
		}

		// Token: 0x06000466 RID: 1126 RVA: 0x00010404 File Offset: 0x0000E604
		public QK3_NODETYPE GetNodeType()
		{
			return this.m_nType;
		}

		// Token: 0x06000467 RID: 1127 RVA: 0x0001040C File Offset: 0x0000E60C
		public int GetInt()
		{
			return this.m_ValueInt;
		}

		// Token: 0x06000468 RID: 1128 RVA: 0x00010414 File Offset: 0x0000E614
		public string GetString()
		{
			if (this.m_nType == QK3_NODETYPE.STR)
			{
				return this.m_ValueStr;
			}
			if (this.m_nType == QK3_NODETYPE.INT)
			{
				return (this.m_ValueInt != 0) ? string.Empty : null;
			}
			return null;
		}

		// Token: 0x06000469 RID: 1129 RVA: 0x00010458 File Offset: 0x0000E658
		public void SetInt(string szNum)
		{
			this.m_nType = QK3_NODETYPE.INT;
			this.m_ValueInt = int.Parse(szNum);
			this.m_ValueStr = null;
		}

		// Token: 0x0600046A RID: 1130 RVA: 0x00010474 File Offset: 0x0000E674
		public void SetInt(int nNum)
		{
			this.m_nType = QK3_NODETYPE.INT;
			this.m_ValueInt = nNum;
			this.m_ValueStr = null;
		}

		// Token: 0x0600046B RID: 1131 RVA: 0x0001048C File Offset: 0x0000E68C
		public void SetString(string szStr)
		{
			this.m_nType = QK3_NODETYPE.STR;
			this.m_ValueInt = int.Parse(szStr);
			this.m_ValueStr = szStr;
		}

		// Token: 0x0600046C RID: 1132 RVA: 0x000104A8 File Offset: 0x0000E6A8
		public void Zero()
		{
			this.m_ValueInt = 0;
			this.m_ValueStr = null;
		}

		// Token: 0x0600046D RID: 1133 RVA: 0x000104B8 File Offset: 0x0000E6B8
		public bool Calc_ADD(CQK3Node NodeR)
		{
			this.m_ValueInt += NodeR.m_ValueInt;
			return true;
		}

		// Token: 0x0600046E RID: 1134 RVA: 0x000104D0 File Offset: 0x0000E6D0
		public bool Calc_PLUS(CQK3Node NodeR)
		{
			this.m_ValueInt = NodeR.m_ValueInt;
			return true;
		}

		// Token: 0x0600046F RID: 1135 RVA: 0x000104E0 File Offset: 0x0000E6E0
		public bool Calc_INC()
		{
			this.m_ValueInt++;
			return true;
		}

		// Token: 0x06000470 RID: 1136 RVA: 0x000104F4 File Offset: 0x0000E6F4
		public bool Calc_SUB(CQK3Node NodeR)
		{
			this.m_ValueInt -= NodeR.m_ValueInt;
			return true;
		}

		// Token: 0x06000471 RID: 1137 RVA: 0x0001050C File Offset: 0x0000E70C
		public bool Calc_MINUS(CQK3Node NodeR)
		{
			this.m_ValueInt = NodeR.m_ValueInt * -1;
			return true;
		}

		// Token: 0x06000472 RID: 1138 RVA: 0x00010520 File Offset: 0x0000E720
		public bool Calc_DEC()
		{
			this.m_ValueInt--;
			return true;
		}

		// Token: 0x06000473 RID: 1139 RVA: 0x00010534 File Offset: 0x0000E734
		public bool Calc_MUL(CQK3Node NodeR)
		{
			this.m_ValueInt *= NodeR.m_ValueInt;
			return true;
		}

		// Token: 0x06000474 RID: 1140 RVA: 0x0001054C File Offset: 0x0000E74C
		public bool Calc_DIV(CQK3Node NodeR)
		{
			this.m_ValueInt /= NodeR.m_ValueInt;
			return true;
		}

		// Token: 0x06000475 RID: 1141 RVA: 0x00010564 File Offset: 0x0000E764
		public bool Calc_MOD(CQK3Node NodeR)
		{
			this.m_ValueInt %= NodeR.m_ValueInt;
			return true;
		}

		// Token: 0x06000476 RID: 1142 RVA: 0x0001057C File Offset: 0x0000E77C
		public bool Calc_NOT(CQK3Node NodeR)
		{
			this.m_ValueInt = ((NodeR.m_ValueInt != 0) ? 0 : 1);
			return true;
		}

		// Token: 0x06000477 RID: 1143 RVA: 0x00010598 File Offset: 0x0000E798
		public bool Calc_CMP_E(CQK3Node NodeR)
		{
			if (this.m_nType == QK3_NODETYPE.STR && NodeR.m_nType == QK3_NODETYPE.STR)
			{
				this.m_ValueInt = ((!string.Equals(this.m_ValueStr, NodeR.m_ValueStr)) ? 0 : 1);
			}
			else
			{
				this.m_ValueInt = ((this.m_ValueInt != NodeR.m_ValueInt) ? 0 : 1);
			}
			return true;
		}

		// Token: 0x06000478 RID: 1144 RVA: 0x00010604 File Offset: 0x0000E804
		public bool Calc_CMP_NE(CQK3Node NodeR)
		{
			if (this.m_nType == QK3_NODETYPE.STR && NodeR.m_nType == QK3_NODETYPE.STR)
			{
				this.m_ValueInt = ((!string.Equals(this.m_ValueStr, NodeR.m_ValueStr)) ? 1 : 0);
			}
			else
			{
				this.m_ValueInt = ((this.m_ValueInt == NodeR.m_ValueInt) ? 0 : 1);
			}
			return true;
		}

		// Token: 0x06000479 RID: 1145 RVA: 0x00010670 File Offset: 0x0000E870
		public bool Calc_CMP_L(CQK3Node NodeR)
		{
			this.m_ValueInt = ((this.m_ValueInt >= NodeR.m_ValueInt) ? 0 : 1);
			return true;
		}

		// Token: 0x0600047A RID: 1146 RVA: 0x00010694 File Offset: 0x0000E894
		public bool Calc_CMP_G(CQK3Node NodeR)
		{
			this.m_ValueInt = ((this.m_ValueInt <= NodeR.m_ValueInt) ? 0 : 1);
			return true;
		}

		// Token: 0x0600047B RID: 1147 RVA: 0x000106B8 File Offset: 0x0000E8B8
		public bool Calc_CMP_LE(CQK3Node NodeR)
		{
			this.m_ValueInt = ((this.m_ValueInt > NodeR.m_ValueInt) ? 0 : 1);
			return true;
		}

		// Token: 0x0600047C RID: 1148 RVA: 0x000106DC File Offset: 0x0000E8DC
		public bool Calc_CMP_GE(CQK3Node NodeR)
		{
			this.m_ValueInt = ((this.m_ValueInt < NodeR.m_ValueInt) ? 0 : 1);
			return true;
		}

		// Token: 0x0600047D RID: 1149 RVA: 0x00010700 File Offset: 0x0000E900
		public bool Calc_LOP_AND(CQK3Node NodeR)
		{
			this.m_ValueInt = ((this.m_ValueInt == 0 || NodeR.m_ValueInt == 0) ? 0 : 1);
			return true;
		}

		// Token: 0x0600047E RID: 1150 RVA: 0x00010734 File Offset: 0x0000E934
		public bool Calc_LOP_OR(CQK3Node NodeR)
		{
			this.m_ValueInt = ((this.m_ValueInt == 0 && NodeR.m_ValueInt == 0) ? 0 : 1);
			return true;
		}

		// Token: 0x0400032A RID: 810
		protected QK3_NODETYPE m_nType;

		// Token: 0x0400032B RID: 811
		protected int m_ValueInt;

		// Token: 0x0400032C RID: 812
		protected string m_ValueStr;
	}
}
