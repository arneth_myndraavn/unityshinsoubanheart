﻿using System;
using Qoo;

namespace NsQT
{
	// Token: 0x0200009C RID: 156
	public class CQPocketK3
	{
		// Token: 0x0600044E RID: 1102 RVA: 0x0000F6EC File Offset: 0x0000D8EC
		public CQPocketK3()
		{
			this.m_aFuncParams = new CQK3Node[32];
			this.m_aOp = new Q_TOKEN[34];
		}

		// Token: 0x170000A6 RID: 166
		// (get) Token: 0x0600044F RID: 1103 RVA: 0x0000F71C File Offset: 0x0000D91C
		// (set) Token: 0x06000450 RID: 1104 RVA: 0x0000F724 File Offset: 0x0000D924
		protected CQTkReader m_Scan { get; set; }

		// Token: 0x06000451 RID: 1105 RVA: 0x0000F730 File Offset: 0x0000D930
		private Q_TOKEN Op(QK3_OP op)
		{
			return this.m_aOp[(int)op];
		}

		// Token: 0x06000452 RID: 1106 RVA: 0x0000F73C File Offset: 0x0000D93C
		protected virtual void SetupScanner(string szExp)
		{
			this.m_Scan.SetSource(szExp);
			this.m_Scan.Enable2byteSym(true);
			this.m_Scan.EnableUpperId(false);
			this.m_Tk = Q_TOKEN.QTK_ERROR;
		}

		// Token: 0x06000453 RID: 1107 RVA: 0x0000F774 File Offset: 0x0000D974
		protected virtual bool Analysis_L(ref CQK3Node Node)
		{
			CQK3Node nodeR = new CQK3Node();
			if (!this.Analysis_C(ref Node))
			{
				return false;
			}
			for (;;)
			{
				if (this.m_Tk == this.Op(QK3_OP.QK3_OP_AND))
				{
					this.m_Tk = this.m_Scan.Scan();
					if (!this.Analysis_C(ref nodeR))
					{
						break;
					}
					Node.Calc_LOP_AND(nodeR);
				}
				else
				{
					if (this.m_Tk != this.Op(QK3_OP.QK3_OP_OR))
					{
						return true;
					}
					this.m_Tk = this.m_Scan.Scan();
					if (!this.Analysis_C(ref nodeR))
					{
						return false;
					}
					Node.Calc_LOP_OR(nodeR);
				}
			}
			return false;
		}

		// Token: 0x06000454 RID: 1108 RVA: 0x0000F81C File Offset: 0x0000DA1C
		protected virtual bool Analysis_C(ref CQK3Node Node)
		{
			CQK3Node nodeR = new CQK3Node();
			if (!this.Analysis_E(ref Node))
			{
				return false;
			}
			for (;;)
			{
				if (this.m_Tk == this.Op(QK3_OP.QK3_OP_CMP_E))
				{
					this.m_Tk = this.m_Scan.Scan();
					if (!this.Analysis_E(ref nodeR))
					{
						break;
					}
					Node.Calc_CMP_E(nodeR);
				}
				else if (this.m_Tk == this.Op(QK3_OP.QK3_OP_CMP_NE))
				{
					this.m_Tk = this.m_Scan.Scan();
					if (!this.Analysis_E(ref nodeR))
					{
						return false;
					}
					Node.Calc_CMP_NE(nodeR);
				}
				else if (this.m_Tk == this.Op(QK3_OP.QK3_OP_CMP_L))
				{
					this.m_Tk = this.m_Scan.Scan();
					if (!this.Analysis_E(ref nodeR))
					{
						return false;
					}
					Node.Calc_CMP_L(nodeR);
				}
				else if (this.m_Tk == this.Op(QK3_OP.QK3_OP_CMP_G))
				{
					this.m_Tk = this.m_Scan.Scan();
					if (!this.Analysis_E(ref nodeR))
					{
						return false;
					}
					Node.Calc_CMP_G(nodeR);
				}
				else if (this.m_Tk == this.Op(QK3_OP.QK3_OP_CMP_LE))
				{
					this.m_Tk = this.m_Scan.Scan();
					if (!this.Analysis_E(ref nodeR))
					{
						return false;
					}
					Node.Calc_CMP_LE(nodeR);
				}
				else
				{
					if (this.m_Tk != this.Op(QK3_OP.QK3_OP_CMP_GE))
					{
						return true;
					}
					this.m_Tk = this.m_Scan.Scan();
					if (!this.Analysis_E(ref nodeR))
					{
						return false;
					}
					Node.Calc_CMP_GE(nodeR);
				}
			}
			return false;
		}

		// Token: 0x06000455 RID: 1109 RVA: 0x0000F9C4 File Offset: 0x0000DBC4
		protected virtual bool Analysis_E(ref CQK3Node Node)
		{
			CQK3Node nodeR = new CQK3Node();
			if (!this.Analysis_T(ref Node))
			{
				return false;
			}
			for (;;)
			{
				if (this.m_Tk == this.Op(QK3_OP.QK3_OP_ADD))
				{
					this.m_Tk = this.m_Scan.Scan();
					if (!this.Analysis_T(ref nodeR))
					{
						break;
					}
					Node.Calc_ADD(nodeR);
				}
				else
				{
					if (this.m_Tk != this.Op(QK3_OP.QK3_OP_SUB))
					{
						return true;
					}
					this.m_Tk = this.m_Scan.Scan();
					if (!this.Analysis_T(ref nodeR))
					{
						return false;
					}
					Node.Calc_SUB(nodeR);
				}
			}
			return false;
		}

		// Token: 0x06000456 RID: 1110 RVA: 0x0000FA70 File Offset: 0x0000DC70
		protected virtual bool Analysis_T(ref CQK3Node Node)
		{
			CQK3Node nodeR = new CQK3Node();
			if (!this.Analysis_F(ref Node))
			{
				return false;
			}
			for (;;)
			{
				if (this.m_Tk == this.Op(QK3_OP.QK3_OP_MUL))
				{
					this.m_Tk = this.m_Scan.Scan();
					if (!this.Analysis_F(ref nodeR))
					{
						break;
					}
					Node.Calc_MUL(nodeR);
				}
				else if (this.m_Tk == this.Op(QK3_OP.QK3_OP_DIV))
				{
					this.m_Tk = this.m_Scan.Scan();
					if (!this.Analysis_F(ref nodeR))
					{
						return false;
					}
					Node.Calc_DIV(nodeR);
				}
				else
				{
					if (this.m_Tk != this.Op(QK3_OP.QK3_OP_MOD))
					{
						return true;
					}
					this.m_Tk = this.m_Scan.Scan();
					if (!this.Analysis_F(ref nodeR))
					{
						return false;
					}
					Node.Calc_MOD(nodeR);
				}
			}
			return false;
		}

		// Token: 0x06000457 RID: 1111 RVA: 0x0000FB5C File Offset: 0x0000DD5C
		protected virtual bool Analysis_F(ref CQK3Node Node)
		{
			CQK3Node nodeR = new CQK3Node();
			if (this.m_Tk == Q_TOKEN.QTK_NUM)
			{
				Node.SetInt(this.m_szId);
				this.m_Tk = this.m_Scan.Scan();
				return true;
			}
			if (this.m_Tk == this.Op(QK3_OP.QK3_OP_BLOCK_L))
			{
				this.m_Tk = this.m_Scan.Scan();
				if (!this.Analysis_E(ref Node))
				{
					return false;
				}
				if (this.m_Tk != this.Op(QK3_OP.QK3_OP_BLOCK_R))
				{
					return this.Error(QK3_ERROR.QK3_ERROR_PARENTH_END);
				}
				this.m_Tk = this.m_Scan.Scan();
				return true;
			}
			else if (this.m_Tk == this.Op(QK3_OP.QK3_OP_MINUS))
			{
				this.m_Tk = this.m_Scan.Scan();
				if (!this.Analysis_F(ref nodeR))
				{
					return false;
				}
				Node.Calc_MINUS(nodeR);
				return true;
			}
			else if (this.m_Tk == this.Op(QK3_OP.QK3_OP_PLUS))
			{
				this.m_Tk = this.m_Scan.Scan();
				if (!this.Analysis_F(ref nodeR))
				{
					return false;
				}
				Node.Calc_PLUS(nodeR);
				return true;
			}
			else if (this.m_Tk == this.Op(QK3_OP.QK3_OP_NOT))
			{
				this.m_Tk = this.m_Scan.Scan();
				if (!this.Analysis_F(ref nodeR))
				{
					return false;
				}
				Node.Calc_NOT(nodeR);
				return true;
			}
			else if (this.m_Tk == this.Op(QK3_OP.QK3_OP_STR_BEGIN))
			{
				this.m_Tk = this.m_Scan.ScanString(this.Op(QK3_OP.QK3_OP_STR_END));
				if (this.m_Tk != Q_TOKEN.QTK_STR)
				{
					return this.Error(QK3_ERROR.QK3_ERROR_SCAN_STRING);
				}
				Node.SetString(this.m_szId);
				this.m_Tk = this.m_Scan.Scan();
				return true;
			}
			else
			{
				if (this.m_Tk != Q_TOKEN.QTK_ID)
				{
					return this.Error(QK3_ERROR.QK3_ERROR_TOKEN);
				}
				QK3_ITEMINFO qk3_ITEMINFO = default(QK3_ITEMINFO);
				qk3_ITEMINFO.Clear();
				QK3_IDTYPE idType = this.GetIdType(this.m_szId, ref qk3_ITEMINFO);
				if (idType == QK3_IDTYPE.QK3_IDTYPE_FUNC)
				{
					this.m_Tk = this.m_Scan.Scan();
					return this.Analysis_O(ref Node, ref qk3_ITEMINFO);
				}
				if (idType == QK3_IDTYPE.QK3_IDTYPE_VAR)
				{
					this.m_Tk = this.m_Scan.Scan();
					return this.Analysis_XV(ref Node, ref qk3_ITEMINFO);
				}
				return this.Error(QK3_ERROR.QK3_ERROR_ID);
			}
		}

		// Token: 0x06000458 RID: 1112 RVA: 0x0000FDA4 File Offset: 0x0000DFA4
		protected virtual bool Analysis_XV(ref CQK3Node Node, ref QK3_ITEMINFO ItemInfo)
		{
			CQK3Node cqk3Node = new CQK3Node();
			CQK3Node cqk3Node2 = new CQK3Node();
			if (this.m_Tk == this.Op(QK3_OP.QK3_OP_IDX_L))
			{
				this.m_Tk = this.m_Scan.Scan();
				if (!this.Analysis_E(ref cqk3Node2))
				{
					return false;
				}
				if (this.m_Tk != this.Op(QK3_OP.QK3_OP_IDX_R))
				{
					return this.Error(QK3_ERROR.QK3_ERROR_BRACKET_END);
				}
				if (cqk3Node2.GetNodeType() != QK3_NODETYPE.INT)
				{
					return this.Error(QK3_ERROR.QK3_ERROR_ARRAY_INDEX_INT);
				}
				this.m_Tk = this.m_Scan.Scan();
			}
			if (this.m_Tk == this.Op(QK3_OP.QK3_OP_SET))
			{
				this.m_Tk = this.m_Scan.Scan();
				if (!this.Analysis_E(ref cqk3Node))
				{
					return false;
				}
				if (!this.VarSetValue(ItemInfo, cqk3Node2, cqk3Node))
				{
					return false;
				}
				Node = cqk3Node;
				return true;
			}
			else
			{
				if (!this.VarGetValue(ItemInfo, cqk3Node2, ref Node))
				{
					return false;
				}
				if (this.m_Tk == this.Op(QK3_OP.QK3_OP_SET_INC))
				{
					Node.Calc_INC();
					this.m_Tk = this.m_Scan.Scan();
				}
				else if (this.m_Tk == this.Op(QK3_OP.QK3_OP_SET_DEC))
				{
					Node.Calc_DEC();
					this.m_Tk = this.m_Scan.Scan();
				}
				else if (this.m_Tk == this.Op(QK3_OP.QK3_OP_SET_ADD))
				{
					this.m_Tk = this.m_Scan.Scan();
					if (!this.Analysis_E(ref cqk3Node))
					{
						return false;
					}
					Node.Calc_ADD(cqk3Node);
				}
				else if (this.m_Tk == this.Op(QK3_OP.QK3_OP_SET_SUB))
				{
					this.m_Tk = this.m_Scan.Scan();
					if (!this.Analysis_E(ref cqk3Node))
					{
						return false;
					}
					Node.Calc_SUB(cqk3Node);
				}
				else if (this.m_Tk == this.Op(QK3_OP.QK3_OP_SET_MUL))
				{
					this.m_Tk = this.m_Scan.Scan();
					if (!this.Analysis_E(ref cqk3Node))
					{
						return false;
					}
					Node.Calc_MUL(cqk3Node);
				}
				else if (this.m_Tk == this.Op(QK3_OP.QK3_OP_SET_DIV))
				{
					this.m_Tk = this.m_Scan.Scan();
					if (!this.Analysis_E(ref cqk3Node))
					{
						return false;
					}
					Node.Calc_DIV(cqk3Node);
				}
				else
				{
					if (this.m_Tk != this.Op(QK3_OP.QK3_OP_SET_MOD))
					{
						return true;
					}
					this.m_Tk = this.m_Scan.Scan();
					if (!this.Analysis_E(ref cqk3Node))
					{
						return false;
					}
					Node.Calc_MOD(cqk3Node);
				}
				return this.VarSetValue(ItemInfo, cqk3Node2, Node);
			}
		}

		// Token: 0x06000459 RID: 1113 RVA: 0x00010054 File Offset: 0x0000E254
		protected virtual bool Analysis_O(ref CQK3Node Node, ref QK3_ITEMINFO ItemInfo)
		{
			if (this.m_Tk != this.Op(QK3_OP.QK3_OP_FUNC_L))
			{
				return this.Error(QK3_ERROR.QK3_ERROR_FUNC_BEGIN);
			}
			this.m_Tk = this.m_Scan.Scan();
			this.m_nFuncParamNum = 0;
			while (this.Analysis_E(ref this.m_aFuncParams[this.m_nFuncParamNum]))
			{
				this.m_nFuncParamNum++;
				if (this.m_Tk == this.Op(QK3_OP.QK3_OP_PARAMS))
				{
					if (this.m_nFuncParamNum >= 32)
					{
						return this.Error(QK3_ERROR.QK3_ERROR_FUNC_PARAM_OVER);
					}
					this.m_Tk = this.m_Scan.Scan();
				}
				else
				{
					if (this.m_Tk != this.Op(QK3_OP.QK3_OP_FUNC_R))
					{
						return this.Error(QK3_ERROR.QK3_ERROR_TOKEN);
					}
					if (!this.FuncCall(ItemInfo, this.m_aFuncParams, this.m_nFuncParamNum, ref Node))
					{
						return this.Error(QK3_ERROR.QK3_ERROR_FUNC_CALLFUNC);
					}
					this.m_Tk = this.m_Scan.Scan();
					return true;
				}
			}
			return false;
		}

		// Token: 0x0600045A RID: 1114 RVA: 0x0001015C File Offset: 0x0000E35C
		protected virtual bool Error(QK3_ERROR nErrNo)
		{
			this.m_nErr = nErrNo;
			Debug.Print(string.Format("QK3-Err: {0} ({1})", this.GetErrMsg(nErrNo), this.m_szId));
			return false;
		}

		// Token: 0x0600045B RID: 1115 RVA: 0x00010190 File Offset: 0x0000E390
		protected virtual void SetupOperator()
		{
			this.SetOp(QK3_OP.QK3_OP_NULL, Q_TOKEN.QTK_ERROR);
			this.SetOp(QK3_OP.QK3_OP_AND, Q_TOKEN.QTK_W_AND);
			this.SetOp(QK3_OP.QK3_OP_OR, Q_TOKEN.QTK_W_OR);
			this.SetOp(QK3_OP.QK3_OP_CMP_E, Q_TOKEN.QTK_W_EQU);
			this.SetOp(QK3_OP.QK3_OP_CMP_NE, Q_TOKEN.QTK_NOTEQU);
			this.SetOp(QK3_OP.QK3_OP_CMP_L, Q_TOKEN.QTK_LESS);
			this.SetOp(QK3_OP.QK3_OP_CMP_G, Q_TOKEN.QTK_GRAT);
			this.SetOp(QK3_OP.QK3_OP_CMP_LE, Q_TOKEN.QTK_LESS_EQU);
			this.SetOp(QK3_OP.QK3_OP_CMP_GE, Q_TOKEN.QTK_GRAT_EQU);
			this.SetOp(QK3_OP.QK3_OP_ADD, Q_TOKEN.QTK_PLUS);
			this.SetOp(QK3_OP.QK3_OP_SUB, Q_TOKEN.QTK_MINUS);
			this.SetOp(QK3_OP.QK3_OP_MUL, Q_TOKEN.QTK_ASTERISK);
			this.SetOp(QK3_OP.QK3_OP_DIV, Q_TOKEN.QTK_SLASH);
			this.SetOp(QK3_OP.QK3_OP_MOD, Q_TOKEN.QTK_PERCENT);
			this.SetOp(QK3_OP.QK3_OP_BLOCK_L, Q_TOKEN.QTK_PARENTH_L);
			this.SetOp(QK3_OP.QK3_OP_BLOCK_R, Q_TOKEN.QTK_PARENTH_R);
			this.SetOp(QK3_OP.QK3_OP_IDX_L, Q_TOKEN.QTK_BRACKET_L);
			this.SetOp(QK3_OP.QK3_OP_IDX_R, Q_TOKEN.QTK_BRACKET_R);
			this.SetOp(QK3_OP.QK3_OP_FUNC_L, Q_TOKEN.QTK_PARENTH_L);
			this.SetOp(QK3_OP.QK3_OP_FUNC_R, Q_TOKEN.QTK_PARENTH_R);
			this.SetOp(QK3_OP.QK3_OP_MINUS, Q_TOKEN.QTK_MINUS);
			this.SetOp(QK3_OP.QK3_OP_PLUS, Q_TOKEN.QTK_PLUS);
			this.SetOp(QK3_OP.QK3_OP_NOT, Q_TOKEN.QTK_EXCLAM);
			this.SetOp(QK3_OP.QK3_OP_SET, Q_TOKEN.QTK_EQU);
			this.SetOp(QK3_OP.QK3_OP_SET_ADD, Q_TOKEN.QTK_PLUS_EQU);
			this.SetOp(QK3_OP.QK3_OP_SET_SUB, Q_TOKEN.QTK_MINUS_EQU);
			this.SetOp(QK3_OP.QK3_OP_SET_MUL, Q_TOKEN.QTK_ASTERISK_EQU);
			this.SetOp(QK3_OP.QK3_OP_SET_DIV, Q_TOKEN.QTK_SLASH_EQU);
			this.SetOp(QK3_OP.QK3_OP_SET_MOD, Q_TOKEN.QTK_PER_EQU);
			this.SetOp(QK3_OP.QK3_OP_SET_INC, Q_TOKEN.QTK_W_PLUS);
			this.SetOp(QK3_OP.QK3_OP_SET_DEC, Q_TOKEN.QTK_W_MINUS);
			this.SetOp(QK3_OP.QK3_OP_PARAMS, Q_TOKEN.QTK_COMMA);
			this.SetOp(QK3_OP.QK3_OP_STR_BEGIN, Q_TOKEN.QTK_W_QUOT);
			this.SetOp(QK3_OP.QK3_OP_STR_END, Q_TOKEN.QTK_W_QUOT);
		}

		// Token: 0x0600045C RID: 1116 RVA: 0x000102E8 File Offset: 0x0000E4E8
		protected virtual QK3_IDTYPE GetIdType(string szName, ref QK3_ITEMINFO ItemInfo)
		{
			return QK3_IDTYPE.QK3_IDTYPE_NULL;
		}

		// Token: 0x0600045D RID: 1117 RVA: 0x000102EC File Offset: 0x0000E4EC
		protected virtual bool VarSetValue(QK3_ITEMINFO ItemInfo, CQK3Node nIndex, CQK3Node NodeValue)
		{
			return false;
		}

		// Token: 0x0600045E RID: 1118 RVA: 0x000102F0 File Offset: 0x0000E4F0
		protected virtual bool VarGetValue(QK3_ITEMINFO ItemInfo, CQK3Node nIndex, ref CQK3Node NodeRet)
		{
			return false;
		}

		// Token: 0x0600045F RID: 1119 RVA: 0x000102F4 File Offset: 0x0000E4F4
		protected virtual bool FuncCall(QK3_ITEMINFO ItemInfo, CQK3Node[] pParams, int nParamNum, ref CQK3Node NodeRet)
		{
			return false;
		}

		// Token: 0x06000460 RID: 1120 RVA: 0x000102F8 File Offset: 0x0000E4F8
		public virtual void Init()
		{
			this.SetupOperator();
		}

		// Token: 0x06000461 RID: 1121 RVA: 0x00010300 File Offset: 0x0000E500
		public virtual bool ExecCalc(string szExp, ref CQK3Node Node)
		{
			this.SetupScanner(szExp);
			this.m_nErr = QK3_ERROR.QK3_ERROR_NULL;
			Node.Zero();
			this.m_Tk = this.m_Scan.Scan();
			return this.Analysis_L(ref Node);
		}

		// Token: 0x06000462 RID: 1122 RVA: 0x00010348 File Offset: 0x0000E548
		public virtual void SetOp(QK3_OP nOp, Q_TOKEN nTk)
		{
			this.m_aOp[(int)nOp] = nTk;
		}

		// Token: 0x06000463 RID: 1123 RVA: 0x00010354 File Offset: 0x0000E554
		public QK3_ERROR GetErrorNo()
		{
			return this.m_nErr;
		}

		// Token: 0x06000464 RID: 1124 RVA: 0x0001035C File Offset: 0x0000E55C
		public string GetErrMsg(QK3_ERROR nNo)
		{
			switch (nNo)
			{
			case QK3_ERROR.QK3_ERROR_NULL:
				return "ERROR";
			case QK3_ERROR.QK3_ERROR_TOKEN:
				return "対応外の記号／名前です。";
			case QK3_ERROR.QK3_ERROR_ID:
				return "変数名／関数名ではありません。";
			case QK3_ERROR.QK3_ERROR_PARENTH_END:
				return "「 ) 」がありません。";
			case QK3_ERROR.QK3_ERROR_BRACKET_END:
				return "「 ] 」がありません。";
			case QK3_ERROR.QK3_ERROR_FUNC_BEGIN:
				return "関数の「 ( 」がありません。";
			case QK3_ERROR.QK3_ERROR_FUNC_END:
				return "関数の「 ) 」がありません。";
			case QK3_ERROR.QK3_ERROR_FUNC_PARAM_OVER:
				return "関数パラメータ数オーバーです。";
			case QK3_ERROR.QK3_ERROR_FUNC_CALLFUNC:
				return "関数呼び出し中にエラーが発生しました。";
			case QK3_ERROR.QK3_ERROR_SCAN_STRING:
				return "文字列取得中にエラーが発生しました。";
			case QK3_ERROR.QK3_ERROR_ARRAY_INDEX_INT:
				return "配列のインデックスが整数でありません。";
			default:
				return string.Empty;
			}
		}

		// Token: 0x0400031E RID: 798
		private const int ID_LEN_MAX = 40;

		// Token: 0x0400031F RID: 799
		private const int FUNC_PARAN_MAX = 32;

		// Token: 0x04000320 RID: 800
		protected Q_TOKEN m_Tk;

		// Token: 0x04000321 RID: 801
		protected QK3_ERROR m_nErr;

		// Token: 0x04000322 RID: 802
		protected string m_szId;

		// Token: 0x04000323 RID: 803
		protected CQK3Node[] m_aFuncParams;

		// Token: 0x04000324 RID: 804
		protected int m_nFuncParamNum;

		// Token: 0x04000325 RID: 805
		protected Q_TOKEN[] m_aOp;
	}
}
