﻿using System;

namespace NsQT
{
	// Token: 0x0200009B RID: 155
	public struct QK3_ITEMINFO
	{
		// Token: 0x0600044D RID: 1101 RVA: 0x0000F6D4 File Offset: 0x0000D8D4
		public void Clear()
		{
			this.nId = 0;
			this.nType = 0;
			this.nIndex = 0;
		}

		// Token: 0x0400031B RID: 795
		public int nType;

		// Token: 0x0400031C RID: 796
		public int nId;

		// Token: 0x0400031D RID: 797
		public int nIndex;
	}
}
