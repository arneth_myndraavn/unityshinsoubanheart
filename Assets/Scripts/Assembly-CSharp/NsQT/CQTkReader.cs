﻿using System;
using System.Collections.Generic;

namespace NsQT
{
	// Token: 0x020000A0 RID: 160
	public class CQTkReader
	{
		// Token: 0x170000A7 RID: 167
		// (get) Token: 0x06000480 RID: 1152 RVA: 0x00010770 File Offset: 0x0000E970
		private char NOW
		{
			get
			{
				return this.Buf[this.m_iRead];
			}
		}

		// Token: 0x170000A8 RID: 168
		// (get) Token: 0x06000481 RID: 1153 RVA: 0x00010784 File Offset: 0x0000E984
		private char AFTER
		{
			get
			{
				return (this.m_iRead + 1 < this.Buf.Length) ? this.Buf[this.m_iRead + 1] : '\0';
			}
		}

		// Token: 0x06000482 RID: 1154 RVA: 0x000107B8 File Offset: 0x0000E9B8
		private bool _NEXT()
		{
			this.ADD_ID();
			return this.INC_READPTR;
		}

		// Token: 0x06000483 RID: 1155 RVA: 0x000107C8 File Offset: 0x0000E9C8
		private bool IsEnd()
		{
			return this.m_iRead >= this.Buf.Length;
		}

		// Token: 0x06000484 RID: 1156 RVA: 0x000107E0 File Offset: 0x0000E9E0
		private bool IsAfterEnd()
		{
			return this.m_iRead + 1 >= this.Buf.Length;
		}

		// Token: 0x06000485 RID: 1157 RVA: 0x000107FC File Offset: 0x0000E9FC
		private bool _NEXT2()
		{
			return this._NEXT() || this._NEXT();
		}

		// Token: 0x170000A9 RID: 169
		// (get) Token: 0x06000486 RID: 1158 RVA: 0x00010814 File Offset: 0x0000EA14
		private bool INC_READPTR
		{
			get
			{
				this.m_iRead++;
				return this.IsEnd();
			}
		}

		// Token: 0x06000487 RID: 1159 RVA: 0x0001082C File Offset: 0x0000EA2C
		private bool IsIdChar(char c)
		{
			return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9') || c == '_';
		}

		// Token: 0x06000488 RID: 1160 RVA: 0x0001087C File Offset: 0x0000EA7C
		protected virtual Q_TOKEN ScanNum()
		{
			this.RESET_ID();
			while (char.IsDigit(this.NOW))
			{
				if (this._NEXT())
				{
					//IL_35:
					this.END_ID();
					return Q_TOKEN.QTK_NUM;
				}
			}
			//goto IL_35;
			this.END_ID();
			return Q_TOKEN.QTK_NUM;
		}

		// Token: 0x06000489 RID: 1161 RVA: 0x000108C8 File Offset: 0x0000EAC8
		protected virtual Q_TOKEN ScanId()
		{
			this.RESET_ID();
			do
			{
				char c = this.NOW;
				if (!this.IsIdChar(c))
				{
					break;
				}
				if (this.m_bUpper)
				{
					c = char.ToUpper(c);
				}
			}
			while (!this._NEXT());
			this.END_ID();
			if (this.m_pKeywords.Count > 0)
			{
				for (int num = 0; num != this.m_pKeywords.Count; num++)
				{
					if (this.m_pKeywords[num] == this.m_nowIdBuff)
					{
						return (Q_TOKEN)(num + this.m_nKeywordsTopNum);
					}
				}
			}
			return Q_TOKEN.QTK_ID;
		}

		// Token: 0x0600048A RID: 1162 RVA: 0x00010978 File Offset: 0x0000EB78
		protected virtual Q_TOKEN ScanWSym()
		{
			this.RESET_ID();
			if (!this.m_bWSym)
			{
				char now = this.NOW;
				switch (now)
				{
				case '%':
					this._NEXT();
					return Q_TOKEN.QTK_PERCENT;
				case '&':
					this._NEXT();
					return Q_TOKEN.QTK_AND;
				default:
					switch (now)
					{
					case ':':
						this._NEXT();
						return Q_TOKEN.QTK_COLON;
					default:
						if (now == '!')
						{
							this._NEXT();
							return Q_TOKEN.QTK_EXCLAM;
						}
						if (now == '|')
						{
							this._NEXT();
							return Q_TOKEN.QTK_OR;
						}
						break;
					case '<':
						this._NEXT();
						return Q_TOKEN.QTK_LESS;
					case '=':
						this._NEXT();
						return Q_TOKEN.QTK_EQU;
					case '>':
						this._NEXT();
						return Q_TOKEN.QTK_GRAT;
					}
					break;
				case '*':
					this._NEXT();
					return Q_TOKEN.QTK_ASTERISK;
				case '+':
					this._NEXT();
					return Q_TOKEN.QTK_PLUS;
				case '-':
					this._NEXT();
					return Q_TOKEN.QTK_MINUS;
				case '/':
					this._NEXT();
					return Q_TOKEN.QTK_SLASH;
				}
			}
			else
			{
				char now = this.NOW;
				switch (now)
				{
				case '%':
					if (this.AFTER == '=')
					{
						this._NEXT2();
						return Q_TOKEN.QTK_PER_EQU;
					}
					this._NEXT();
					return Q_TOKEN.QTK_PERCENT;
				case '&':
					if (this.AFTER == '&')
					{
						this._NEXT2();
						return Q_TOKEN.QTK_W_AND;
					}
					this._NEXT();
					return Q_TOKEN.QTK_AND;
				default:
					switch (now)
					{
					case ':':
						if (this.AFTER == ':')
						{
							this._NEXT2();
							return Q_TOKEN.QTK_W_COLON;
						}
						this._NEXT();
						return Q_TOKEN.QTK_COLON;
					default:
						if (now != '!')
						{
							if (now == '|')
							{
								if (this.AFTER == '|')
								{
									this._NEXT2();
									return Q_TOKEN.QTK_W_OR;
								}
								this._NEXT();
								return Q_TOKEN.QTK_OR;
							}
						}
						else
						{
							if (this.AFTER == '=')
							{
								this._NEXT2();
								return Q_TOKEN.QTK_NOTEQU;
							}
							this._NEXT();
							return Q_TOKEN.QTK_EXCLAM;
						}
						break;
					case '<':
						if (this.AFTER == '=')
						{
							this._NEXT2();
							return Q_TOKEN.QTK_LESS_EQU;
						}
						this._NEXT();
						return Q_TOKEN.QTK_LESS;
					case '=':
						if (this.AFTER == '=')
						{
							this._NEXT2();
							return Q_TOKEN.QTK_W_EQU;
						}
						this._NEXT();
						return Q_TOKEN.QTK_EQU;
					case '>':
						if (this.AFTER == '=')
						{
							this._NEXT2();
							return Q_TOKEN.QTK_GRAT_EQU;
						}
						this._NEXT();
						return Q_TOKEN.QTK_GRAT;
					}
					break;
				case '*':
					if (this.AFTER == '/')
					{
						this._NEXT2();
						return Q_TOKEN.QTK_REM_R;
					}
					if (this.AFTER == '=')
					{
						this._NEXT2();
						return Q_TOKEN.QTK_ASTERISK_EQU;
					}
					this._NEXT();
					return Q_TOKEN.QTK_ASTERISK;
				case '+':
					if (this.AFTER == '+')
					{
						this._NEXT2();
						return Q_TOKEN.QTK_W_PLUS;
					}
					if (this.AFTER == '=')
					{
						this._NEXT2();
						return Q_TOKEN.QTK_PLUS_EQU;
					}
					this._NEXT();
					return Q_TOKEN.QTK_PLUS;
				case '-':
					if (this.AFTER == '-')
					{
						this._NEXT2();
						return Q_TOKEN.QTK_W_MINUS;
					}
					if (this.AFTER == '=')
					{
						this._NEXT2();
						return Q_TOKEN.QTK_MINUS_EQU;
					}
					this._NEXT();
					return Q_TOKEN.QTK_MINUS;
				case '/':
					if (this.AFTER == '/')
					{
						this._NEXT2();
						return Q_TOKEN.QTK_W_SLASH;
					}
					if (this.AFTER == '*')
					{
						this._NEXT2();
						return Q_TOKEN.QTK_REM_L;
					}
					if (this.AFTER == '=')
					{
						this._NEXT2();
						return Q_TOKEN.QTK_SLASH_EQU;
					}
					this._NEXT();
					return Q_TOKEN.QTK_SLASH;
				}
			}
			return Q_TOKEN.QTK_ERROR;
		}

		// Token: 0x0600048B RID: 1163 RVA: 0x00010CF0 File Offset: 0x0000EEF0
		protected virtual Q_TOKEN ScanHex()
		{
			this.RESET_ID();
			for (;;)
			{
				switch (char.ToUpper(this.NOW))
				{
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9':
				case 'A':
				case 'B':
				case 'C':
				case 'D':
				case 'E':
				case 'F':
					if (!this._NEXT())
					{
						continue;
					}
					break;
				}
				break;
			}
			this.END_ID();
			return Q_TOKEN.QTK_HEXNUM;
		}

		// Token: 0x0600048C RID: 1164 RVA: 0x00010DA4 File Offset: 0x0000EFA4
		public virtual void SetSource(string text)
		{
			this.Buf = text;
			this.m_iRead = 0;
		}

		// Token: 0x0600048D RID: 1165 RVA: 0x00010DB4 File Offset: 0x0000EFB4
		public virtual void SetReadPtr(int i)
		{
			this.m_iRead = i;
		}

		// Token: 0x0600048E RID: 1166 RVA: 0x00010DC0 File Offset: 0x0000EFC0
		public virtual void Enable2byteSym(bool bWSym)
		{
			this.m_bWSym = bWSym;
		}

		// Token: 0x0600048F RID: 1167 RVA: 0x00010DCC File Offset: 0x0000EFCC
		public virtual void EnableUpperId(bool bUpper)
		{
			this.m_bUpper = bUpper;
		}

		// Token: 0x06000490 RID: 1168 RVA: 0x00010DD8 File Offset: 0x0000EFD8
		public virtual void SetKeywordList(string[] ppNames, int TopNum)
		{
			this.m_pKeywords.AddRange(ppNames);
			this.m_nKeywordsTopNum = TopNum;
		}

		// Token: 0x06000491 RID: 1169 RVA: 0x00010DF0 File Offset: 0x0000EFF0
		public virtual List<string> GetIdBuff()
		{
			return this.m_pIdBuff;
		}

		// Token: 0x06000492 RID: 1170 RVA: 0x00010DF8 File Offset: 0x0000EFF8
		public virtual string GetTokenChar(Q_TOKEN nTk)
		{
			switch (nTk)
			{
			case Q_TOKEN.QTK_SPACE:
				return " ";
			case Q_TOKEN.QTK_RETURN:
				return "\n";
			case Q_TOKEN.QTK_PARENTH_L:
				return "(";
			case Q_TOKEN.QTK_PARENTH_R:
				return ")";
			case Q_TOKEN.QTK_BRACE_L:
				return "{";
			case Q_TOKEN.QTK_BRACE_R:
				return "}";
			case Q_TOKEN.QTK_BRACKET_L:
				return "[";
			case Q_TOKEN.QTK_BRACKET_R:
				return "]";
			case Q_TOKEN.QTK_COLON:
				return ":";
			case Q_TOKEN.QTK_SEMICOLON:
				return ";";
			case Q_TOKEN.QTK_W_COLON:
				return "::";
			case Q_TOKEN.QTK_COMMA:
				return ",";
			case Q_TOKEN.QTK_POINT:
				return ".";
			case Q_TOKEN.QTK_W_QUOT:
				return "\"";
			case Q_TOKEN.QTK_S_QUOT:
				return "'";
			case Q_TOKEN.QTK_EQU:
				return "=";
			case Q_TOKEN.QTK_EXCLAM:
				return "!";
			case Q_TOKEN.QTK_PLUS:
				return "+";
			case Q_TOKEN.QTK_MINUS:
				return "-";
			case Q_TOKEN.QTK_ASTERISK:
				return "*";
			case Q_TOKEN.QTK_SLASH:
				return "/";
			case Q_TOKEN.QTK_W_PLUS:
				return "++";
			case Q_TOKEN.QTK_W_MINUS:
				return "--";
			case Q_TOKEN.QTK_PLUS_EQU:
				return "+=";
			case Q_TOKEN.QTK_MINUS_EQU:
				return "-=";
			case Q_TOKEN.QTK_ASTERISK_EQU:
				return "*=";
			case Q_TOKEN.QTK_SLASH_EQU:
				return "/=";
			case Q_TOKEN.QTK_W_EQU:
				return "==";
			case Q_TOKEN.QTK_NOTEQU:
				return "!=";
			case Q_TOKEN.QTK_LESS:
				return "<";
			case Q_TOKEN.QTK_GRAT:
				return ">";
			case Q_TOKEN.QTK_LESS_EQU:
				return "<=";
			case Q_TOKEN.QTK_GRAT_EQU:
				return ">=";
			case Q_TOKEN.QTK_OR:
				return "|";
			case Q_TOKEN.QTK_AND:
				return "&";
			case Q_TOKEN.QTK_W_AND:
				return "&&";
			case Q_TOKEN.QTK_W_OR:
				return "||";
			case Q_TOKEN.QTK_YEN:
				return "\\";
			case Q_TOKEN.QTK_AMARK:
				return "@";
			case Q_TOKEN.QTK_SHARP:
				return "#";
			case Q_TOKEN.QTK_QUESTION:
				return "?";
			case Q_TOKEN.QTK_DOLLAR:
				return "$";
			case Q_TOKEN.QTK_PERCENT:
				return "%";
			case Q_TOKEN.QTK_REM_L:
				return "/*";
			case Q_TOKEN.QTK_REM_R:
				return "*/";
			case Q_TOKEN.QTK_W_SLASH:
				return "//";
			case Q_TOKEN.QTK_PER_EQU:
				return "%=";
			default:
				return null;
			}
		}

		// Token: 0x06000493 RID: 1171 RVA: 0x00010FEC File Offset: 0x0000F1EC
		public virtual Q_TOKEN Scan()
		{
			this.RESET_ID();
			while (!this.IsEnd())
			{
				char now = this.NOW;
				switch (now)
				{
				case ' ':
					this._NEXT();
					break;
				default:
					switch (now)
					{
					case ';':
						this._NEXT();
						return Q_TOKEN.QTK_SEMICOLON;
					default:
						switch (now)
						{
						case '[':
							this._NEXT();
							return Q_TOKEN.QTK_BRACKET_L;
						case '\\':
							this._NEXT();
							return Q_TOKEN.QTK_YEN;
						case ']':
							this._NEXT();
							return Q_TOKEN.QTK_BRACKET_R;
						case '^':
							this._NEXT();
							return Q_TOKEN.QTK_OTHER_SYM;
						default:
							switch (now)
							{
							case '\t':
								this._NEXT();
								break;
							case '\n':
								this._NEXT();
								return Q_TOKEN.QTK_RETURN;
							default:
								switch (now)
								{
								case '{':
									this._NEXT();
									return Q_TOKEN.QTK_BRACE_L;
								default:
								{
									if (now == '\0')
									{
										return Q_TOKEN.QTK_END;
									}
									if (now == '･')
									{
										this._NEXT();
										return Q_TOKEN.QTK_OTHER_SYM;
									}
									Q_TOKEN q_TOKEN = this.ScanWSym();
									if (q_TOKEN != Q_TOKEN.QTK_ERROR)
									{
										return q_TOKEN;
									}
									if (!this.IsAfterEnd() && this.NOW == '0' && (this.AFTER == 'x' || this.AFTER == 'X'))
									{
										this._NEXT2();
										return this.ScanHex();
									}
									if (char.IsDigit(this.NOW))
									{
										return this.ScanNum();
									}
									return this.ScanId();
								}
								case '}':
									this._NEXT();
									return Q_TOKEN.QTK_BRACE_R;
								}
								break;
							case '\r':
								if (this.AFTER == '\n')
								{
									this._NEXT2();
									return Q_TOKEN.QTK_RETURN;
								}
								this._NEXT();
								return Q_TOKEN.QTK_RETURN;
							}
							break;
						case '`':
							this._NEXT();
							return Q_TOKEN.QTK_OTHER_SYM;
						}
						break;
					case '?':
						this._NEXT();
						return Q_TOKEN.QTK_QUESTION;
					case '@':
						this._NEXT();
						return Q_TOKEN.QTK_AMARK;
					}
					break;
				case '"':
					this._NEXT();
					return Q_TOKEN.QTK_W_QUOT;
				case '#':
					this._NEXT();
					return Q_TOKEN.QTK_SHARP;
				case '$':
					this._NEXT();
					return Q_TOKEN.QTK_DOLLAR;
				case '\'':
					this._NEXT();
					return Q_TOKEN.QTK_S_QUOT;
				case '(':
					this._NEXT();
					return Q_TOKEN.QTK_PARENTH_L;
				case ')':
					this._NEXT();
					return Q_TOKEN.QTK_PARENTH_R;
				case ',':
					this._NEXT();
					return Q_TOKEN.QTK_COMMA;
				case '.':
					this._NEXT();
					return Q_TOKEN.QTK_POINT;
				}
			}
			return Q_TOKEN.QTK_END;
		}

		// Token: 0x06000494 RID: 1172 RVA: 0x00011254 File Offset: 0x0000F454
		public virtual Q_TOKEN ScanString()
		{
			this.RESET_ID();
			while (this.NOW != '"')
			{
				if (this.IsEnd())
				{
					this.END_ID();
					return Q_TOKEN.QTK_ERROR;
				}
				if (this.NOW == '\n' || this.NOW == '\r' || this.NOW == '\0')
				{
					this.END_ID();
					return Q_TOKEN.QTK_ERROR;
				}
				if (this._NEXT())
				{
					//IL_7D:
					this.END_ID();
					return Q_TOKEN.QTK_STR;
				}
			}
			if (this.INC_READPTR)
			{
				//goto IL_7D;
				this.END_ID();
				return Q_TOKEN.QTK_STR;
			}
			//goto IL_7D;
			this.END_ID();
			return Q_TOKEN.QTK_STR;
		}

		// Token: 0x06000495 RID: 1173 RVA: 0x000112E8 File Offset: 0x0000F4E8
		public virtual Q_TOKEN ScanString(Q_TOKEN nEndTk)
		{
			string tokenChar = this.GetTokenChar(nEndTk);
			if (tokenChar == null)
			{
				return Q_TOKEN.QTK_ERROR;
			}
			this.RESET_ID();
			for (;;)
			{
				if (this.NOW == tokenChar[0])
				{
					if (tokenChar.Length <= 1)
					{
						goto IL_7B;
					}
					if (!this.IsAfterEnd() && this.AFTER == tokenChar[1])
					{
						break;
					}
				}
				if (this.IsEnd())
				{
					goto Block_9;
				}
				if (this.NOW == '\n' || this.NOW == '\r' || this.NOW == '\0')
				{
					goto IL_C8;
				}
				if (this._NEXT())
				{
					goto Block_12;
				}
			}
			if (this.INC_READPTR)
			{
				goto IL_E5;
			}
			if (this.INC_READPTR)
			{
				goto IL_E5;
			}
			goto IL_E5;
			IL_7B:
			if (this.INC_READPTR)
			{
				goto IL_E5;
			}
			goto IL_E5;
			Block_9:
			this.RESET_ID();
			return Q_TOKEN.QTK_ERROR;
			IL_C8:
			this.RESET_ID();
			return Q_TOKEN.QTK_ERROR;
			Block_12:
			IL_E5:
			this.RESET_ID();
			return Q_TOKEN.QTK_STR;
		}

		// Token: 0x06000496 RID: 1174 RVA: 0x000113E4 File Offset: 0x0000F5E4
		public virtual bool LineSkip()
		{
			while (this.NOW != '\n')
			{
				if (this.NOW == '\r')
				{
					if (!this.INC_READPTR)
					{
						if (this.NOW == '\n' && this.INC_READPTR)
						{
						}
					}
				}
				else if (this.NOW != '\0')
				{
					if (!this.INC_READPTR)
					{
						continue;
					}
				}
				return true;
			}
			if (this.INC_READPTR)
			{
				return true;
			}
			return true;
		}

		// Token: 0x06000497 RID: 1175 RVA: 0x00011478 File Offset: 0x0000F678
		public virtual Q_TOKEN GetLineText()
		{
			this.RESET_ID();
			while (this.NOW != '\n')
			{
				if (this.NOW == '\r')
				{
					if (!this.INC_READPTR)
					{
						if (this.NOW == '\n' && this.INC_READPTR)
						{
						}
					}
				}
				else if (this.NOW != '\0')
				{
					if (!this._NEXT())
					{
						continue;
					}
				}
				//IL_8C:
				this.RESET_ID();
				return Q_TOKEN.QTK_STR;
			}
			if (this.INC_READPTR)
			{
				//goto IL_8C;
				this.RESET_ID();
				return Q_TOKEN.QTK_STR;
			}
			//goto IL_8C;
			this.RESET_ID();
			return Q_TOKEN.QTK_STR;
		}

		// Token: 0x06000498 RID: 1176 RVA: 0x00011518 File Offset: 0x0000F718
		private void RESET_ID()
		{
			this.END_ID();
			this.m_nowIdBuff = string.Empty;
		}

		// Token: 0x06000499 RID: 1177 RVA: 0x0001152C File Offset: 0x0000F72C
		private void ADD_ID()
		{
			this.m_nowIdBuff += this.Buf[this.m_iRead];
			this.m_isAddYet = true;
		}

		// Token: 0x0600049A RID: 1178 RVA: 0x00011568 File Offset: 0x0000F768
		private void END_ID()
		{
			if (this.m_isAddYet)
			{
				this.m_pIdBuff.Add(this.m_nowIdBuff);
			}
			this.m_isAddYet = false;
		}

		// Token: 0x04000368 RID: 872
		private string Buf;

		// Token: 0x04000369 RID: 873
		protected int m_iRead;

		// Token: 0x0400036A RID: 874
		protected bool m_bWSym;

		// Token: 0x0400036B RID: 875
		protected bool m_bUpper;

		// Token: 0x0400036C RID: 876
		protected List<string> m_pIdBuff;

		// Token: 0x0400036D RID: 877
		private string m_nowIdBuff;

		// Token: 0x0400036E RID: 878
		private bool m_isAddYet;

		// Token: 0x0400036F RID: 879
		protected List<uint> m_pSum;

		// Token: 0x04000370 RID: 880
		protected List<string> m_pKeywords;

		// Token: 0x04000371 RID: 881
		protected int m_nKeywordsTopNum;
	}
}
