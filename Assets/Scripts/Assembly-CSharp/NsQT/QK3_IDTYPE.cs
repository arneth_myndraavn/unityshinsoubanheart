﻿using System;

namespace NsQT
{
	// Token: 0x0200009A RID: 154
	public enum QK3_IDTYPE
	{
		// Token: 0x04000318 RID: 792
		QK3_IDTYPE_NULL,
		// Token: 0x04000319 RID: 793
		QK3_IDTYPE_VAR,
		// Token: 0x0400031A RID: 794
		QK3_IDTYPE_FUNC
	}
}
