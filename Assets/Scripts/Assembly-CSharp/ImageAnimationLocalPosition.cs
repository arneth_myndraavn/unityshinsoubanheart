﻿using System;
using UnityEngine;

// Token: 0x02000197 RID: 407
public class ImageAnimationLocalPosition : IImageAnimation
{
	// Token: 0x06000BA8 RID: 2984 RVA: 0x00031464 File Offset: 0x0002F664
	public ImageAnimationLocalPosition(int repeat, float speed, float delay, MoveType moveType, Vector3 offsetBegin, Vector3 offsetEnd)
	{
		this.param = new AnimationRange_Vector3(repeat, speed, delay, moveType, offsetBegin, offsetEnd);
	}

	// Token: 0x06000BA9 RID: 2985 RVA: 0x0003148C File Offset: 0x0002F68C
	public override void Update()
	{
		this.param.Update();
	}

	// Token: 0x06000BAA RID: 2986 RVA: 0x0003149C File Offset: 0x0002F69C
	public override bool Active()
	{
		return this.param.Active;
	}

	// Token: 0x06000BAB RID: 2987 RVA: 0x000314AC File Offset: 0x0002F6AC
	public override void Restart()
	{
		this.param.Restart();
	}

	// Token: 0x06000BAC RID: 2988 RVA: 0x000314BC File Offset: 0x0002F6BC
	public override Vector3 CalcPosition(Vector3 position)
	{
		return position + new Vector3(this.param.Offset.x, -this.param.Offset.y, this.param.Offset.z);
	}

	// Token: 0x17000193 RID: 403
	// (get) Token: 0x06000BAD RID: 2989 RVA: 0x00031510 File Offset: 0x0002F710
	public Vector3 Offset
	{
		get
		{
			return this.param.Offset;
		}
	}

	// Token: 0x17000194 RID: 404
	// (get) Token: 0x06000BAE RID: 2990 RVA: 0x00031520 File Offset: 0x0002F720
	// (set) Token: 0x06000BAF RID: 2991 RVA: 0x00031530 File Offset: 0x0002F730
	public Vector3 OffsetBegin
	{
		get
		{
			return this.param.OffsetBegin;
		}
		set
		{
			this.param.OffsetBegin = value;
		}
	}

	// Token: 0x17000195 RID: 405
	// (get) Token: 0x06000BB0 RID: 2992 RVA: 0x00031540 File Offset: 0x0002F740
	// (set) Token: 0x06000BB1 RID: 2993 RVA: 0x00031550 File Offset: 0x0002F750
	public Vector3 OffsetEnd
	{
		get
		{
			return this.param.OffsetEnd;
		}
		set
		{
			this.param.OffsetEnd = value;
		}
	}

	// Token: 0x17000196 RID: 406
	// (get) Token: 0x06000BB2 RID: 2994 RVA: 0x00031560 File Offset: 0x0002F760
	// (set) Token: 0x06000BB3 RID: 2995 RVA: 0x00031570 File Offset: 0x0002F770
	public float Speed
	{
		get
		{
			return this.param.Speed;
		}
		set
		{
			this.param.Speed = value;
		}
	}

	// Token: 0x04000921 RID: 2337
	private AnimationRange_Vector3 param;
}
