﻿using System;
using System.Collections.Generic;
using PaymentGameApi;
using Qoo;

// Token: 0x02000123 RID: 291
public class PaymentDialogWindow : BaseWindow
{
	private Dictionary<string,int> dict = null;

	// Token: 0x060007D7 RID: 2007 RVA: 0x0002241C File Offset: 0x0002061C
	protected sealed override void OnAwake()
	{
		this.state = PaymentDialogWindow.STATE.INIT;
		this.tm = new PaymentDialogWindow.TanakaMain();
		this.purchaseList = new PaymentDialogWindow.PurchaseList();
		this.productId = UIValue.Payment_ProductId;
	}

	// Token: 0x060007D8 RID: 2008 RVA: 0x00022454 File Offset: 0x00020654
	protected sealed override string[] newSceneTextureNameArray()
	{
		return new string[]
		{
			"screen/common/sys_dialog"
		};
	}

	// Token: 0x060007D9 RID: 2009 RVA: 0x00022474 File Offset: 0x00020674
	protected sealed override void BeforeInit()
	{
	}

	// Token: 0x060007DA RID: 2010 RVA: 0x00022478 File Offset: 0x00020678
	protected sealed override void AfterInit()
	{
	}

	// Token: 0x060007DB RID: 2011 RVA: 0x0002247C File Offset: 0x0002067C
	protected sealed override BaseWindow.UIComponent[] newComponentArray()
	{
		return new BaseWindow.UIComponent[]
		{
			new BaseWindow.UICollision("Close", 0, 0, this.wndz, 960, 544),
			new BaseWindow.UIImage("MsgWnd", 137, 182, this.wndz + 1, "screen/common/sys_dialog", false, true),
			new BaseWindow.UIText("Text", 480, 272, this.wndz + 2, "購入処理中です。", 22, UnityTextSprite.PositionType.Center, UnityTextSprite.PositionType.Center, byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue)
		};
	}

	// Token: 0x060007DC RID: 2012 RVA: 0x00022514 File Offset: 0x00020714
	protected sealed override void OnBaseWindowOnButton(string obj)
	{
		if (obj != null)
		{
			if (this.dict == null)
			{
				this.dict = new Dictionary<string, int>(1)
				{
					{
						"Close",
						0
					}
				};
			}
			int num;
			if (this.dict.TryGetValue(obj, out num))
			{
				if (num == 0)
				{
					base.PlaySE_Cancel();
					this.OnClose();
				}
			}
		}
	}

	// Token: 0x060007DD RID: 2013 RVA: 0x0002257C File Offset: 0x0002077C
	protected override void OnBaseWindowUpdate()
	{
		switch (this.state)
		{
		case PaymentDialogWindow.STATE.INIT:
			this.tm.Init(this.productId);
			this.state = PaymentDialogWindow.STATE.EXEC;
			break;
		case PaymentDialogWindow.STATE.EXEC:
			if (this.tm.Exec())
			{
				switch (this.tm.Result())
				{
				case AMAppStore.ResultStatus.RESULT_SUCCESS:
					this.state = PaymentDialogWindow.STATE.PURCHASE_INIT;
					break;
				case AMAppStore.ResultStatus.RESULT_FAILD:
					this.state = PaymentDialogWindow.STATE.FAILD;
					base.StartTime();
					this.SetText(this.tm.ResultMessage());
					break;
				case AMAppStore.ResultStatus.RESULT_CANCEL:
					this.state = PaymentDialogWindow.STATE.CANCEL;
					break;
				}
			}
			break;
		case PaymentDialogWindow.STATE.PURCHASE_INIT:
			this.purchaseList.Init();
			this.state = PaymentDialogWindow.STATE.PURCHASE_EXEC;
			break;
		case PaymentDialogWindow.STATE.PURCHASE_EXEC:
			if (this.purchaseList.Exec())
			{
				base.StartTime();
				this.SetText(this.purchaseList.ResultMessage());
				this.state = ((this.purchaseList.Result() != AMAppStore.ResultStatus.RESULT_SUCCESS) ? PaymentDialogWindow.STATE.FAILD : PaymentDialogWindow.STATE.SUCCESS);
			}
			break;
		case PaymentDialogWindow.STATE.SUCCESS:
			if (base.CheckTime(1f))
			{
				this.OnClose();
			}
			break;
		case PaymentDialogWindow.STATE.FAILD:
			if (base.CheckTime(1f))
			{
				this.OnClose();
			}
			break;
		case PaymentDialogWindow.STATE.CANCEL:
			this.OnClose();
			break;
		case PaymentDialogWindow.STATE.EXIT:
			base.DeleteLastAddScene();
			this.state = PaymentDialogWindow.STATE.AFTER_EXIT;
			break;
		}
	}

	// Token: 0x060007DE RID: 2014 RVA: 0x0002270C File Offset: 0x0002090C
	private void OnClose()
	{
		switch (this.state)
		{
		case PaymentDialogWindow.STATE.SUCCESS:
			UIValue.Payment_Exit = PaymentExitType.SUCCESS;
			this.state = PaymentDialogWindow.STATE.EXIT;
			break;
		case PaymentDialogWindow.STATE.FAILD:
			UIValue.Payment_Exit = PaymentExitType.FAILD;
			this.state = PaymentDialogWindow.STATE.EXIT;
			break;
		case PaymentDialogWindow.STATE.CANCEL:
			UIValue.Payment_Exit = PaymentExitType.CANCEL;
			this.state = PaymentDialogWindow.STATE.EXIT;
			break;
		}
	}

	// Token: 0x060007DF RID: 2015 RVA: 0x00022770 File Offset: 0x00020970
	private void SetText(string message)
	{
		UnityTextSprite textSprite = base.GetTextSprite("Text");
		textSprite.ClearText();
		textSprite.AddText(message, 22);
		textSprite.SetPosition(480, 272, UnityTextSprite.PositionType.Center, UnityTextSprite.PositionType.Center);
		textSprite.Update(960, 544, (float)(-(float)(this.wndz + 2)));
	}

	// Token: 0x040006D7 RID: 1751
	private PaymentDialogWindow.STATE state;

	// Token: 0x040006D8 RID: 1752
	private PaymentDialogWindow.TanakaMain tm;

	// Token: 0x040006D9 RID: 1753
	private PaymentDialogWindow.PurchaseList purchaseList;

	// Token: 0x040006DA RID: 1754
	private string productId;

	// Token: 0x040006DB RID: 1755
	private readonly int wndz = 10;

	// Token: 0x02000124 RID: 292
	private class TanakaMain
	{
		// Token: 0x060007E0 RID: 2016 RVA: 0x000227C8 File Offset: 0x000209C8
		public TanakaMain()
		{
			this.m_eResut = AMAppStore.ResultStatus.RESULT_FAILD;
		}

		// Token: 0x060007E1 RID: 2017 RVA: 0x000227D8 File Offset: 0x000209D8
		public void Init(string productId)
		{
			AMAppStore.RequestPurchase(productId);
		}

		// Token: 0x060007E2 RID: 2018 RVA: 0x000227E4 File Offset: 0x000209E4
		public bool Exec()
		{
			return AMAppStore.ResultWaitPurchase(ref this.m_eResut);
		}

		// Token: 0x060007E3 RID: 2019 RVA: 0x000227F4 File Offset: 0x000209F4
		public AMAppStore.ResultStatus Result()
		{
			return this.m_eResut;
		}

		// Token: 0x060007E4 RID: 2020 RVA: 0x000227FC File Offset: 0x000209FC
		public string ResultMessage()
		{
			Debug.Print(AMAppStore.GetErrorPurchase());
			return (this.m_eResut != AMAppStore.ResultStatus.RESULT_SUCCESS) ? "通信に失敗しました。\n通信状態の良いところで再度接続を試みてください。" : " ";
		}

		// Token: 0x040006DD RID: 1757
		private AMAppStore.ResultStatus m_eResut;
	}

	// Token: 0x02000125 RID: 293
	private class PurchaseList
	{
		// Token: 0x060007E5 RID: 2021 RVA: 0x00022830 File Offset: 0x00020A30
		public PurchaseList()
		{
			this.m_eResut = AMAppStore.ResultStatus.RESULT_FAILD;
		}

		// Token: 0x060007E6 RID: 2022 RVA: 0x00022840 File Offset: 0x00020A40
		public void Init()
		{
			DatabaseProductID databaseProductID = new DatabaseProductID();
			AMAppStore.RequestAfterPurchase(databaseProductID.GetAllProductId());
		}

		// Token: 0x060007E7 RID: 2023 RVA: 0x00022860 File Offset: 0x00020A60
		public bool Exec()
		{
			return AMAppStore.ResultWaitPurchased(ref this.m_eResut);
		}

		// Token: 0x060007E8 RID: 2024 RVA: 0x00022870 File Offset: 0x00020A70
		public AMAppStore.ResultStatus Result()
		{
			return this.m_eResut;
		}

		// Token: 0x060007E9 RID: 2025 RVA: 0x00022878 File Offset: 0x00020A78
		public string ResultMessage()
		{
			Debug.Print(AMAppStore.GetErrorPurchased());
			return (this.m_eResut != AMAppStore.ResultStatus.RESULT_SUCCESS) ? "通信に失敗しました。\n通信状態の良いところで再度接続を試みてください。" : "アドオンの購入が完了しました。";
		}

		// Token: 0x040006DE RID: 1758
		private AMAppStore.ResultStatus m_eResut;
	}

	// Token: 0x02000126 RID: 294
	private enum STATE
	{
		// Token: 0x040006E0 RID: 1760
		INIT,
		// Token: 0x040006E1 RID: 1761
		EXEC,
		// Token: 0x040006E2 RID: 1762
		PURCHASE_INIT,
		// Token: 0x040006E3 RID: 1763
		PURCHASE_EXEC,
		// Token: 0x040006E4 RID: 1764
		SUCCESS,
		// Token: 0x040006E5 RID: 1765
		FAILD,
		// Token: 0x040006E6 RID: 1766
		CANCEL,
		// Token: 0x040006E7 RID: 1767
		EXIT,
		// Token: 0x040006E8 RID: 1768
		AFTER_EXIT
	}
}
