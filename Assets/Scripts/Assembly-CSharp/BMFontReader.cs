﻿using System;
using Qoo;
using UnityEngine;

// Token: 0x0200014C RID: 332
public static class BMFontReader
{
	// Token: 0x06000942 RID: 2370 RVA: 0x00028238 File Offset: 0x00026438
	private static string GetString(string s)
	{
		int num = s.IndexOf('=');
		return (num != -1) ? s.Substring(num + 1) : string.Empty;
	}

	// Token: 0x06000943 RID: 2371 RVA: 0x00028268 File Offset: 0x00026468
	private static int GetInt(string s)
	{
		int result = 0;
		string @string = BMFontReader.GetString(s);
		int.TryParse(@string, out result);
		return result;
	}

	// Token: 0x06000944 RID: 2372 RVA: 0x00028288 File Offset: 0x00026488
	public static void Load(BMFont font, string name, byte[] bytes)
	{
		font.Clear();
		if (bytes != null)
		{
			ByteReader byteReader = new ByteReader(bytes);
			char[] separator = new char[]
			{
				' '
			};
			while (byteReader.canRead)
			{
				string text = byteReader.ReadLine();
				if (string.IsNullOrEmpty(text))
				{
					break;
				}
				string[] array = text.Split(separator, StringSplitOptions.RemoveEmptyEntries);
				int num = array.Length;
				if (array[0] == "char")
				{
					int channel = (num <= 10) ? 15 : BMFontReader.GetInt(array[10]);
					if (num > 9 && BMFontReader.GetInt(array[9]) > 0)
					{
						UnityEngine.Debug.LogError("Your font was exported with more than one texture. Only one texture is supported by NGUI.\nYou need to re-export your font, enlarging the texture's dimensions until everything fits into just one texture.");
						break;
					}
					if (num <= 8)
					{
						UnityEngine.Debug.LogError(string.Concat(new object[]
						{
							"Unexpected number of entries for the 'char' field (",
							name,
							", ",
							array.Length,
							"):\n",
							text
						}));
						break;
					}
					int @int = BMFontReader.GetInt(array[1]);
					BMGlyph glyph = font.GetGlyph(@int, true);
					if (glyph != null)
					{
						glyph.x = BMFontReader.GetInt(array[2]);
						glyph.y = BMFontReader.GetInt(array[3]);
						glyph.width = BMFontReader.GetInt(array[4]);
						glyph.height = BMFontReader.GetInt(array[5]);
						glyph.offsetX = BMFontReader.GetInt(array[6]);
						glyph.offsetY = BMFontReader.GetInt(array[7]);
						glyph.advance = BMFontReader.GetInt(array[8]);
						glyph.channel = channel;
					}
					else
					{
						Qoo.Debug.Print(string.Concat(new object[]
						{
							"Char: ",
							array[1],
							" (",
							@int,
							") is NULL"
						}));
					}
				}
				else if (array[0] == "kerning")
				{
					if (num <= 3)
					{
						UnityEngine.Debug.LogError(string.Concat(new object[]
						{
							"Unexpected number of entries for the 'kerning' field (",
							name,
							", ",
							array.Length,
							"):\n",
							text
						}));
						break;
					}
					int int2 = BMFontReader.GetInt(array[1]);
					int int3 = BMFontReader.GetInt(array[2]);
					int int4 = BMFontReader.GetInt(array[3]);
					BMGlyph glyph2 = font.GetGlyph(int3, true);
					if (glyph2 != null)
					{
						glyph2.SetKerning(int2, int4);
					}
				}
				else if (array[0] == "common")
				{
					if (num <= 5)
					{
						UnityEngine.Debug.LogError(string.Concat(new object[]
						{
							"Unexpected number of entries for the 'common' field (",
							name,
							", ",
							array.Length,
							"):\n",
							text
						}));
						break;
					}
					font.charSize = BMFontReader.GetInt(array[1]);
					font.baseOffset = BMFontReader.GetInt(array[2]);
					font.texWidth = BMFontReader.GetInt(array[3]);
					font.texHeight = BMFontReader.GetInt(array[4]);
					int int5 = BMFontReader.GetInt(array[5]);
					if (int5 != 1)
					{
						UnityEngine.Debug.LogError(string.Concat(new object[]
						{
							"Font '",
							name,
							"' must be created with only 1 texture, not ",
							int5
						}));
						break;
					}
				}
				else if (array[0] == "page")
				{
					if (num > 2)
					{
						font.spriteName = BMFontReader.GetString(array[2]).Replace("\"", string.Empty);
						font.spriteName = font.spriteName.Replace(".png", string.Empty);
						font.spriteName = font.spriteName.Replace(".tga", string.Empty);
					}
				}
				else if (array[0] == "symbol" && num > 5)
				{
					BMSymbol symbol = font.GetSymbol(BMFontReader.GetString(array[1]), true);
					symbol.x = BMFontReader.GetInt(array[2]);
					symbol.y = BMFontReader.GetInt(array[3]);
					symbol.width = BMFontReader.GetInt(array[4]);
					symbol.height = BMFontReader.GetInt(array[5]);
				}
			}
		}
		font.Build();
		Qoo.Debug.Print(string.Format("Import Font:num={0}", font.glyphCount));
	}
}
