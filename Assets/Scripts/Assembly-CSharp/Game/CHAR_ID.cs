﻿using System;

namespace Game
{
	// Token: 0x020000C5 RID: 197
	public enum CHAR_ID
	{
		// Token: 0x04000490 RID: 1168
		NOTHING = -1,
		// Token: 0x04000491 RID: 1169
		BLOOD,
		// Token: 0x04000492 RID: 1170
		ELLIOT,
		// Token: 0x04000493 RID: 1171
		DEEDUM,
		// Token: 0x04000494 RID: 1172
		VIVALDI,
		// Token: 0x04000495 RID: 1173
		PETER,
		// Token: 0x04000496 RID: 1174
		ACE,
		// Token: 0x04000497 RID: 1175
		GOWLAND,
		// Token: 0x04000498 RID: 1176
		BORIS,
		// Token: 0x04000499 RID: 1177
		JULIUS,
		// Token: 0x0400049A RID: 1178
		NIGHTMARE,
		// Token: 0x0400049B RID: 1179
		MOB,
		// Token: 0x0400049C RID: 1180
		NUM
	}
}
