﻿using System;
using Qoo.AM.Game;
using Qoo.Def;
using Qoo.Game;
using Qoo.Message;
using Qoo.Table;
using UnityEngine;

namespace Game
{
	// Token: 0x020000DC RID: 220
	public static class GameInitilize
	{
		// Token: 0x06000605 RID: 1541 RVA: 0x00018F90 File Offset: 0x00017190
		public static bool Init()
		{
			CharaDef.InitCharIdTable();
			GameDef.InitGameDataTbl();
			SysData.Init();
			GameInitilize.SetMessageStyle();
			GameInitilize.InitDirScaleTable();
			GameInitilize.LoadSysSe();
			GameInitilize.LoadSystemData();
			SubPartCamera.Init(960f, 544f);
			return true;
		}

		// Token: 0x06000606 RID: 1542 RVA: 0x00018FD4 File Offset: 0x000171D4
		private static void SetMessageStyle()
		{
			MSGWND_STYLE_DATA msgwnd_STYLE_DATA = new MSGWND_STYLE_DATA();
			msgwnd_STYLE_DATA.posFrm = new Point3(0, 181, 800);
			msgwnd_STYLE_DATA.posName[0] = new Point3(86, 170, 803);
			msgwnd_STYLE_DATA.posName[1] = new Point3(126, 170, 802);
            if (!UnityApp.PlatformApp.isJapanese)
            {
                msgwnd_STYLE_DATA.posTxt = new Point3(104, 198, 801);
                msgwnd_STYLE_DATA.sizeTxt = new Size(704, 136);
            }
            else
            {
                msgwnd_STYLE_DATA.posTxt = new Point3(109, 198, 801);
                msgwnd_STYLE_DATA.sizeTxt = new Size(654, 136);
            }
            msgwnd_STYLE_DATA.posFace = new Point3(0, 173, 803);
			msgwnd_STYLE_DATA.posBrk = new Point3(459, 252, 802);
			msgwnd_STYLE_DATA.sizeBrkSplit = new Size(32, 1);
			msgwnd_STYLE_DATA.nBrkWait = 4;
			msgwnd_STYLE_DATA.nTxtWEx = 58;
			msgwnd_STYLE_DATA.bTxtAutoRet = true;
			msgwnd_STYLE_DATA.colTxt = new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue);
			msgwnd_STYLE_DATA.colRefWord = new Color32(byte.MaxValue, 105, 0, byte.MaxValue);
			msgwnd_STYLE_DATA.colRefBg = new Color32(23, 124, 0, 0);
			msgwnd_STYLE_DATA.colRefCur = new Color32(23, 124, 0, byte.MaxValue);
			msgwnd_STYLE_DATA.BrkCg = "pagebreak_a.png";
			msgwnd_STYLE_DATA.BrkAutoCg = "pagebreak_b.png";
			msgwnd_STYLE_DATA.SetPosScale(2f, 2f);
			MessageStyle.AddStyle(MSGWND_STYLE.NORMAL, "normal", "frame_togaki.png", msgwnd_STYLE_DATA);
			MessageStyle.AddStyle(MSGWND_STYLE.KYARA, "kyara", "frame_kyara.png", msgwnd_STYLE_DATA);
			MessageStyle.AddStyle(MSGWND_STYLE.TOGAKI, "togaki", "frame_togaki.png", msgwnd_STYLE_DATA);
			MessageStyle.AddStyle(MSGWND_STYLE.HEROINE, "heroine", "frame_heroine.png", msgwnd_STYLE_DATA);
			MessageStyle.AddStyle(MSGWND_STYLE.MONOLOGUE, "monologue", "frame_monologue.png", msgwnd_STYLE_DATA);
			MessageStyle.AddStyle(MSGWND_STYLE.TEST, "test", "frame_togaki.png", msgwnd_STYLE_DATA);
		}

		// Token: 0x06000607 RID: 1543 RVA: 0x000191A0 File Offset: 0x000173A0
		private static void InitDirScaleTable()
		{
			DirScaleTable.Clear();
			DirScaleTable.Base = new SizeF(480f, 272f);
			DirScaleTable.Default = new SizeF(1f, 1f);
			SizeF size = new SizeF(2f, 2f);
			DirScaleTable.Add("bytes/effect/", size);
			DirScaleTable.Add("bytes/event/bgimage/", size);
			DirScaleTable.Add("bytes/event/select/", size);
			DirScaleTable.Add("bytes/screen/cgmemory2/", size);
			DirScaleTable.Add("bytes/viewer/cgmode/", size);
			DirScaleTable.Add("bytes/viewer/memory/", size);
		}

		// Token: 0x06000608 RID: 1544 RVA: 0x00019234 File Offset: 0x00017434
		private static void LoadSysSe()
		{
			foreach (string name in SoundDef.SE_LOAD_DATA)
			{
				UnityApp.Sound.AddSeData(name);
			}
		}

		// Token: 0x06000609 RID: 1545 RVA: 0x0001926C File Offset: 0x0001746C
		private static bool LoadSystemData()
		{
			SysSaveData sysSaveData = new SysSaveData();
			return sysSaveData.Load(SaveLoadManager.LoadSystem());
		}
	}
}
