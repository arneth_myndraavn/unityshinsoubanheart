﻿using System;
using System.Collections.Generic;
using Qoo.Application;
using Qoo.Game;
using UnityEngine;

// Token: 0x02000135 RID: 309
public class TitleWindow : BaseWindow
{
	// Token: 0x06000848 RID: 2120 RVA: 0x0002534C File Offset: 0x0002354C
	protected sealed override string GetBGMName()
	{
		return "sysbgm_title";
	}

	// Token: 0x06000849 RID: 2121 RVA: 0x00025354 File Offset: 0x00023554
	protected sealed override void OnAwake()
	{
		SceneManager.ClearSceneStack();
		if (TitleWindow.titleCall == null)
		{
			TitleWindow.titleCall = new TitleWindow.TitleCall();
		}
		this.state = TitleWindow.STATE.WAIT_INPUT;
	}

	// Token: 0x0600084A RID: 2122 RVA: 0x00025384 File Offset: 0x00023584
	protected sealed override string[] newSceneTextureNameArray()
	{
		string str = (!base.IsTrial) ? string.Empty : "_tri";
		return new string[]
		{
			"screen/title/title_base",
			"screen/title/title_0",
			"screen/title/title_1" + str,
			"screen/title/title_2",
			"screen/title/title_3",
			"screen/title/title_4" + str,
			"screen/title/title_5",
			"screen/title/restore"
		};
	}

	// Token: 0x0600084B RID: 2123 RVA: 0x00025400 File Offset: 0x00023600
	protected sealed override void AfterInit()
	{
		if (base.IsTrial)
		{
			base.GetActionButton("Btn1").ActionEnable = false;
		}
		base.GameObjectShow("BtnGuard", false);
		GameData.ResetVoiceCheck();
	}

	// Token: 0x0600084C RID: 2124 RVA: 0x0002543C File Offset: 0x0002363C
	protected sealed override BaseWindow.UIComponent[] newComponentArray()
	{
		List<BaseWindow.UIComponent> components;
		string str = (!base.IsTrial) ? string.Empty : "_tri";
		components = new List<BaseWindow.UIComponent>
		{
			new BaseWindow.UIImage("BackGround", 0, 0, this.wndz, "screen/title/title_base", true, true),
			new BaseWindow.UIButton("Btn0", 171, 272, this.wndz + 1, "screen/title/title_0", true, true, 1, 2, 0),
			new BaseWindow.UIButton("Btn1", 378, 272, this.wndz + 1, "screen/title/title_1" + str, true, true, 1, 2, 0),
			new BaseWindow.UIButton("Btn2", 585, 272, this.wndz + 1, "screen/title/title_2", true, true, 1, 2, 0),
			new BaseWindow.UIButton("Btn3", 171, 404, this.wndz + 1, "screen/title/title_3", true, true, 1, 2, 0),
			new BaseWindow.UIButton("Btn4", 378, 404, this.wndz + 1, "screen/title/title_4" + str, true, true, 1, 2, 0),
			new BaseWindow.UIButton("Btn5", 585, 404, this.wndz + 1, "screen/title/title_5", true, true, 1, 2, 0),
			new BaseWindow.UIButton("BtnRestore", 814, 472, this.wndz + 1, "screen/title/restore", !base.IsTrial, !base.IsTrial, 1, 2, 0),
			new BaseWindow.UICollision("BtnGuard", 0, 0, this.wndz + 3, 960, 544),
			new BaseWindow.UIButton("BtnScreenRes", 16, 472, this.wndz + 1, "screen/title/restore", !base.IsTrial, !base.IsTrial, 1, 2, 0)
		};
        if (!UnityApp.Instance.isJapanese)
        {
			
			components.Add(new BaseWindow.UIButton("BtnCredits", 16, 402, this.wndz + 1, "screen/title/restore", !base.IsTrial, !base.IsTrial, 1, 2, 0));
		}
		return components.ToArray();
	}

	// Token: 0x0600084D RID: 2125 RVA: 0x000255E8 File Offset: 0x000237E8
	protected sealed override void OnBaseWindowUpdate()
	{
		switch (this.state)
		{
		case TitleWindow.STATE.WAIT_SE_STOP:
			if (!base.IsPlayerSEPlaying())
			{
				this.OnWaitSE();
			}
			break;
		}
	}

	// Token: 0x0600084E RID: 2126 RVA: 0x00025634 File Offset: 0x00023834
	protected sealed override void OnBaseWindowOnButton(string obj)
	{
		TitleWindow.STATE state = this.state;
		if (state != TitleWindow.STATE.WAIT_INPUT)
		{
			return;
		}
		switch (obj)
		{
		case "Btn0":
			this.OnButtonStartAdvMode(0);
			break;
		case "Btn1":
			if (!base.IsTrial)
			{
				this.OnButtonStartAdvMode(1);
			}
			break;
		case "Btn2":
			base.PlaySE_Ok();
			UIValue.SaveLoadType = SaveLoadType.LOAD;
			UIValue.SaveLoadCallType = SaveLoadCallType.TITLE;
			SceneManager.ChangeScene(UIValue.SCENE_SAVELOAD);
			break;
		case "Btn3":
			base.PlaySE_Ok();
			SceneManager.ChangeScene(UIValue.SCENE_GALLERYMENU);
			break;
		case "Btn4":
			base.PlaySE_Ok();
			UIValue.GalleryCharactor = 10;
			SceneManager.ChangeScene(UIValue.SCENE_INTRODUCTION);
			break;
		case "Btn5":
			base.PlaySE_Ok();
			SceneManager.ChangeScene(UIValue.SCENE_HELP);
			break;
		case "BtnRestore":
			Application.Quit();
			//base.PlaySE_Ok();
			//base.AddScene(UIValue.SCENE_TITLERESTORE);
			break;
		case "BtnCredits":
			base.PlaySE_Ok();
			SceneManager.ChangeScene(UIValue.SCENE_TITLECREDITS);
			break;
		case "BtnScreenRes":
			base.PlaySE_Ok();
			SceneManager.ChangeScene(UIValue.SCENE_SCREENRESSETTING);
			break;
		}
	}

	// Token: 0x0600084F RID: 2127 RVA: 0x000257A8 File Offset: 0x000239A8
	private void OnButtonStartAdvMode(int buttonId)
	{
		base.GameObjectShow("BtnGuard", true);
		base.PlaySE_Ok();
		base.PlayPlayerSE(TitleWindow.titleCall.getVoiceId().ToString("sysse_call00"));
		this.state = TitleWindow.STATE.WAIT_SE_STOP;
		App.SetAdvMode(buttonId);
	}

	// Token: 0x06000850 RID: 2128 RVA: 0x000257F4 File Offset: 0x000239F4
	private void OnWaitSE()
	{
		SceneManager.ChangeScene(UIValue.SCENE_ADVMODE);
		this.state = TitleWindow.STATE.END;
	}

	// Token: 0x04000742 RID: 1858
	private readonly int wndz;

	// Token: 0x04000743 RID: 1859
	private static TitleWindow.TitleCall titleCall;

	// Token: 0x04000744 RID: 1860
	private TitleWindow.STATE state;

	// Token: 0x02000136 RID: 310
	private class TitleCall
	{
		// Token: 0x06000851 RID: 2129 RVA: 0x00025808 File Offset: 0x00023A08
		public TitleCall()
		{
			this.table = new int[this.variation];
			this.index = 0;
			this.Init();
		}

		// Token: 0x06000852 RID: 2130 RVA: 0x00025840 File Offset: 0x00023A40
		public int getVoiceId()
		{
			int result = this.table[this.index++];
			if (this.index == this.variation)
			{
				this.Init();
			}
			return result;
		}

		// Token: 0x06000853 RID: 2131 RVA: 0x00025880 File Offset: 0x00023A80
		private void Init()
		{
			for (int i = 0; i < this.variation; i++)
			{
				this.table[i] = this.top + i;
			}
			this.Shuffle<int>(this.table);
			this.index = 0;
		}

		// Token: 0x06000854 RID: 2132 RVA: 0x000258C8 File Offset: 0x00023AC8
		private void Shuffle<T>(T[] array)
		{
			for (int i = array.Length; i > 1; i--)
			{
				int num = UnityEngine.Random.Range(0, i);
				T t = array[num];
				array[num] = array[i - 1];
				array[i - 1] = t;
			}
		}

		// Token: 0x04000746 RID: 1862
		private readonly int variation = 10;

		// Token: 0x04000747 RID: 1863
		private readonly int top = 1;

		// Token: 0x04000748 RID: 1864
		private int[] table;

		// Token: 0x04000749 RID: 1865
		private int index;
	}

	// Token: 0x02000137 RID: 311
	private enum STATE
	{
		// Token: 0x0400074B RID: 1867
		WAIT_INPUT,
		// Token: 0x0400074C RID: 1868
		WAIT_SE_STOP,
		// Token: 0x0400074D RID: 1869
		END
	}
}
