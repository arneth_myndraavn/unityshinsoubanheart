﻿using System;
using UnityEngine;

// Token: 0x0200019E RID: 414
public class UIActionToggle : UIAction
{
	// Token: 0x170001A6 RID: 422
	// (get) Token: 0x06000BFD RID: 3069 RVA: 0x000321FC File Offset: 0x000303FC
	// (set) Token: 0x06000BFE RID: 3070 RVA: 0x00032204 File Offset: 0x00030404
	public bool AutoSwitch
	{
		get
		{
			return this.m_Auto;
		}
		set
		{
			this.m_Auto = value;
		}
	}

	// Token: 0x06000BFF RID: 3071 RVA: 0x00032210 File Offset: 0x00030410
	public bool IsSwitchOn()
	{
		return this.m_Value == this.m_On;
	}

	// Token: 0x06000C00 RID: 3072 RVA: 0x00032220 File Offset: 0x00030420
	public static UIActionToggle Create(GameObject go, int on = 0, int off = 1, bool autoSwitch = false)
	{
		UIActionToggle uiactionToggle = go.AddComponent<UIActionToggle>();
		uiactionToggle.m_On = on;
		uiactionToggle.m_Off = off;
		uiactionToggle.m_Auto = autoSwitch;
		return uiactionToggle;
	}

	// Token: 0x06000C01 RID: 3073 RVA: 0x0003224C File Offset: 0x0003044C
	public override void Init()
	{
		base.Init();
	}

	// Token: 0x06000C02 RID: 3074 RVA: 0x00032254 File Offset: 0x00030454
	public void SetToggle(int value)
	{
		if (base.ActionEnable)
		{
			this.m_Value = value;
			this.UpdateTexture();
		}
	}

	// Token: 0x06000C03 RID: 3075 RVA: 0x00032270 File Offset: 0x00030470
	public void SwitchOn()
	{
		this.SetToggle(this.m_On);
	}

	// Token: 0x06000C04 RID: 3076 RVA: 0x00032280 File Offset: 0x00030480
	public void SwitchOff()
	{
		this.SetToggle(this.m_Off);
	}

	// Token: 0x06000C05 RID: 3077 RVA: 0x00032290 File Offset: 0x00030490
	public void SwitchToggle()
	{
		if (this.m_Value == this.m_On)
		{
			this.SwitchOff();
		}
		else
		{
			this.SwitchOn();
		}
	}

	// Token: 0x06000C06 RID: 3078 RVA: 0x000322C0 File Offset: 0x000304C0
	private void UpdateTexture()
	{
		this.imageObject.ImageBlockIndex = this.m_Value;
		this.imageObject.InitTextureUV();
	}

	// Token: 0x06000C07 RID: 3079 RVA: 0x000322E0 File Offset: 0x000304E0
	private void OnMouseDown()
	{
	}

	// Token: 0x06000C08 RID: 3080 RVA: 0x000322E4 File Offset: 0x000304E4
	private void OnMouseUp()
	{
		if (this.m_Auto)
		{
			this.SwitchToggle();
		}
	}

	// Token: 0x0400093E RID: 2366
	private int m_On;

	// Token: 0x0400093F RID: 2367
	private int m_Off;

	// Token: 0x04000940 RID: 2368
	private int m_Value;

	// Token: 0x04000941 RID: 2369
	private bool m_Auto;
}
