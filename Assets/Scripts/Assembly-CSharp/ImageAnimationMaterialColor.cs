﻿using System;
using UnityEngine;

// Token: 0x02000198 RID: 408
public class ImageAnimationMaterialColor : IImageAnimation
{
	// Token: 0x06000BB4 RID: 2996 RVA: 0x00031580 File Offset: 0x0002F780
	public ImageAnimationMaterialColor(int repeat, float speed, float delay, Color offsetBegin, Color offsetEnd)
	{
		this.param = new AnimationRange_Color(repeat, speed, delay, offsetBegin, offsetEnd);
	}

	// Token: 0x06000BB5 RID: 2997 RVA: 0x000315A8 File Offset: 0x0002F7A8
	public override void Update()
	{
		this.param.Update();
	}

	// Token: 0x06000BB6 RID: 2998 RVA: 0x000315B8 File Offset: 0x0002F7B8
	public override bool Active()
	{
		return this.param.Active;
	}

	// Token: 0x06000BB7 RID: 2999 RVA: 0x000315C8 File Offset: 0x0002F7C8
	public override void Restart()
	{
		this.param.Restart();
	}

	// Token: 0x06000BB8 RID: 3000 RVA: 0x000315D8 File Offset: 0x0002F7D8
	public override Color CalcColor(Color color)
	{
		return color * this.param.Offset;
	}

	// Token: 0x04000922 RID: 2338
	private AnimationRange_Color param;
}
