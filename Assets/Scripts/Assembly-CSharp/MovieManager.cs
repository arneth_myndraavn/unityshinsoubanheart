﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Video;

// Token: 0x0200017E RID: 382
public static class MovieManager
{
	//TODO fix movie playing. either make platform specific versions OR make platform agnostic

	// Token: 0x06000ACD RID: 2765 RVA: 0x0002F630 File Offset: 0x0002D830
	/*public static IEnumerator PlayMovie(string path, FullScreenMovieControlMode mode)
	{
		Handheld.PlayFullScreenMovie(path, Color.black, mode);
		yield return 0;
		yield break;
	}*/

	//public static IEnumerator PlayMovie(string path, string mode)

	public static IEnumerator PlayMovie(string filePath, GameObject gameObject)
	{
		int m_width = 960;
		int m_height = 544;
		yield return 0;
		yield return 0;
		UnitySprite movieFrame = new UnitySprite(false, gameObject, false);
		movieFrame.CalcRenderImageOffset(m_width, m_height);
		movieFrame.Update(m_width, m_height, 10f);
		movieFrame.SetName("Movie Frame");


		//NewCode
		string path = filePath;
		if (path.IndexOf('/') == 0)
		{
			path = path.Substring(1);
		}
		string pathExt = System.IO.Path.GetExtension(path);
		path = path.Substring(0, path.Length - pathExt.Length);

		VideoPlayer m_tex = movieFrame.obj.transform.gameObject.AddComponent<VideoPlayer>();
		m_tex.playOnAwake = true;
		Material newMat = new Material(Shader.Find("QO/Sprite") as Shader);
		m_tex.url = Application.streamingAssetsPath + "/" + path + ".webm";
		m_tex.isLooping = false;
		movieFrame.SetMaterial(newMat, 960, 544);
		m_tex.renderMode = UnityEngine.Video.VideoRenderMode.MaterialOverride;


		//OLD Code
		//movieFrame.SetMaterial(movie.Play(name, MOVIE_TEX_TYPE.ADD, false), 960, 544);
		//yield return 0;
		//while (movie.IsPlay)
		//m_tex.Play();
		yield return 0;
        while (!m_tex.isPrepared)
        {
			yield return 0;
		}
		yield return 0;
		while (m_tex.isPlaying || !m_tex.isPrepared)
		{
            if (Input.GetMouseButtonDown(0))
            {
				break;
            }
			movieFrame.CalcRenderImageOffset(m_width, m_height);
			movieFrame.Update(m_width, m_height, 10f);
			yield return 0;
		}
		//GameObject.Destroy(gameObject);
		//Handheld.PlayFullScreenMovie(path, Color.black, mode);
		yield return 0;
		yield break;
	}


}
