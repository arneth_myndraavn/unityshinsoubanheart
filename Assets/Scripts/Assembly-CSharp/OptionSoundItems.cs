﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x020000EE RID: 238
public class OptionSoundItems : MonoBehaviour
{
	// Token: 0x170000D9 RID: 217
	// (get) Token: 0x06000672 RID: 1650 RVA: 0x0001AC84 File Offset: 0x00018E84
	public List<GameObject> ListGameObjects
	{
		get
		{
			return this.UIGameObjects;
		}
	}

	// Token: 0x06000673 RID: 1651 RVA: 0x0001AC8C File Offset: 0x00018E8C
	public static OptionSoundItems Create(Transform parent = null)
	{
		return new GameObject("OptionSoundItems")
		{
			transform = 
			{
				parent = parent,
				localPosition = default(Vector3)
			}
		}.AddComponent<OptionSoundItems>();
	}

	// Token: 0x06000674 RID: 1652 RVA: 0x0001ACCC File Offset: 0x00018ECC
	public void ResetMeter()
	{
		this.m_MeterBGM.Init(373, 29, SoundOptionType.BGM);
		this.m_MeterSE.Init(373, 29, SoundOptionType.SE);
		this.m_MeterSystem.Init(373, 29, SoundOptionType.SYSTEM);
	}

	// Token: 0x06000675 RID: 1653 RVA: 0x0001AD14 File Offset: 0x00018F14
	public void AddItem(SoundOptionType type, GameObject label, GameObject meter, GameObject frame)
	{
		label.transform.parent = base.transform;
		meter.transform.parent = base.transform;
		frame.transform.parent = base.transform;
		OptionSoundMeter optionSoundMeter = meter.AddComponent<OptionSoundMeter>();
		switch (type)
		{
		case SoundOptionType.BGM:
			this.m_MeterBGM = optionSoundMeter;
			break;
		case SoundOptionType.SE:
			this.m_MeterSE = optionSoundMeter;
			break;
		case SoundOptionType.SYSTEM:
			this.m_MeterSystem = optionSoundMeter;
			break;
		}
	}

	// Token: 0x06000676 RID: 1654 RVA: 0x0001AD9C File Offset: 0x00018F9C
	public void Init()
	{
		this.ResetMeter();
	}

	// Token: 0x040005E3 RID: 1507
	private List<GameObject> UIGameObjects = new List<GameObject>();

	// Token: 0x040005E4 RID: 1508
	private OptionSoundMeter m_MeterBGM;

	// Token: 0x040005E5 RID: 1509
	private OptionSoundMeter m_MeterSE;

	// Token: 0x040005E6 RID: 1510
	private OptionSoundMeter m_MeterSystem;
}
