﻿using System;
using Qoo;
using Qoo.Graphics;
using UnityEngine;

// Token: 0x020000C3 RID: 195
public class DebugMovieWnd : UserBaseClass
{
	// Token: 0x060005E2 RID: 1506 RVA: 0x00017CC0 File Offset: 0x00015EC0
	public void Awake()
	{
		this.m_nCount = 0;
		Singleton<UnityGraph>.Instance.Disable(false);
	}

	// Token: 0x060005E3 RID: 1507 RVA: 0x00017CD4 File Offset: 0x00015ED4
	public void Update()
	{
		if (this.m_nCount < 100)
		{
			Qoo.Debug.Print("MovieTest:DisableVersion:Count=" + this.m_nCount);
			Movie.Play("quinrose_logo_8", true);
			this.m_nCount++;
		}
		else
		{
			Singleton<DebugMenuWnd>.Instance.enabled = true;
			UnityEngine.Object.Destroy(this);
			Singleton<UnityGraph>.Instance.Enable(false);
		}
	}

	// Token: 0x04000487 RID: 1159
	private int m_nCount;
}
