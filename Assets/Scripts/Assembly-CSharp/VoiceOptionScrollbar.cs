﻿using System;
using System.Collections;
using UnityEngine;

// Token: 0x020000F6 RID: 246
public class VoiceOptionScrollbar : MonoBehaviour
{
	// Token: 0x06000697 RID: 1687 RVA: 0x0001B4A8 File Offset: 0x000196A8
	public static VoiceOptionScrollbar Create(int height, ImageObject area, ImageObject slider, Transform parent)
	{
		GameObject gameObject = new GameObject("VoiceOptionScrollBar");
		gameObject.transform.parent = parent;
		gameObject.transform.localPosition = default(Vector3);
		VoiceOptionScrollbar voiceOptionScrollbar = gameObject.AddComponent<VoiceOptionScrollbar>();
		voiceOptionScrollbar.Init(height, area, slider, gameObject.transform);
		return voiceOptionScrollbar;
	}

	// Token: 0x06000698 RID: 1688 RVA: 0x0001B4F8 File Offset: 0x000196F8
	private void Init(int height, ImageObject area, ImageObject slider, Transform parent)
	{
		this.m_Scrollarea = area;
		this.m_Scrollbar = slider;
		this.m_Height = height;
		base.StartCoroutine(this.TextureLoadWait());
	}

	// Token: 0x06000699 RID: 1689 RVA: 0x0001B528 File Offset: 0x00019728
	private IEnumerator TextureLoadWait()
	{
		while (!GraphicManager.CheckLoadImageComplete("voice_ber1"))
		{
			yield return 0;
		}
		while (!GraphicManager.CheckLoadImageComplete("voice_ber2"))
		{
			yield return 0;
		}
		this.m_ScrollHeight = (int)(this.m_Scrollarea.transform.localScale.y - this.m_Scrollbar.transform.localScale.y);
		this.m_Scrollbar.gameObject.GetComponent<VoiceOptionScrollbarSlider>().SetHeight(this.m_ScrollHeight);
		this.m_Scrollbar.gameObject.GetComponent<VoiceOptionScrollbarSlider>().SetListHeight(this.m_Height);
		yield break;
	}

	// Token: 0x0600069A RID: 1690 RVA: 0x0001B544 File Offset: 0x00019744
	public void SetSlideObject(GameObject slideObject)
	{
		VoiceOptionScrollbarSlider voiceOptionScrollbarSlider = this.m_Scrollbar.gameObject.AddComponent<VoiceOptionScrollbarSlider>();
		voiceOptionScrollbarSlider.SetSlideObject(slideObject);
	}

	// Token: 0x0600069B RID: 1691 RVA: 0x0001B56C File Offset: 0x0001976C
	public void SetValue(int value)
	{
		this.m_Value = value;
		this.UpdateBar();
	}

	// Token: 0x0600069C RID: 1692 RVA: 0x0001B57C File Offset: 0x0001977C
	private void UpdateBar()
	{
		this.m_Rate = (float)this.m_Value / (float)this.m_Height;
		if (this.m_Rate < -1f)
		{
			this.m_Rate = -1f;
		}
		if (this.m_Rate > 0f)
		{
			this.m_Rate = 0f;
		}
		this.m_Scrollbar.OnViewPosition.y = this.m_Scrollbar.OriginalPosition.y + this.m_Rate * (float)this.m_ScrollHeight;
		if (this.m_Scrollbar.OnViewPosition.y > this.m_Scrollbar.OriginalPosition.y)
		{
			this.m_Scrollbar.OnViewPosition.y = this.m_Scrollbar.OriginalPosition.y;
		}
	}

	// Token: 0x040005FF RID: 1535
	private ImageObject m_Scrollbar;

	// Token: 0x04000600 RID: 1536
	private ImageObject m_Scrollarea;

	// Token: 0x04000601 RID: 1537
	private int m_Height;

	// Token: 0x04000602 RID: 1538
	private int m_Value;

	// Token: 0x04000603 RID: 1539
	private float m_Rate;

	// Token: 0x04000604 RID: 1540
	private int m_ScrollHeight;
}
