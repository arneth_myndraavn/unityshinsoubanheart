﻿using System;

// Token: 0x0200019F RID: 415
public class UIObjectInfo
{
	// Token: 0x06000C09 RID: 3081 RVA: 0x000322F8 File Offset: 0x000304F8
	public UIObjectInfo(string name_, int x_, int y_, int z_, string fpath_, int xImgCnt_ = 1, int yImgCnt_ = 1, int defaultIndex_ = 0)
	{
		this.ObjectName = name_;
		this.ScreenX = x_;
		this.ScreenY = y_;
		this.ScreenZ = z_;
		this.UITexture = new UITextureInfo(xImgCnt_, yImgCnt_, fpath_);
		this.DefaultTextureIndex = defaultIndex_;
		this.ScaleRate = 1;
	}

	// Token: 0x04000942 RID: 2370
	public string ObjectName;

	// Token: 0x04000943 RID: 2371
	public int ScreenX;

	// Token: 0x04000944 RID: 2372
	public int ScreenY;

	// Token: 0x04000945 RID: 2373
	public int ScreenZ;

	// Token: 0x04000946 RID: 2374
	public int ScaleRate;

	// Token: 0x04000947 RID: 2375
	public UITextureInfo UITexture;

	// Token: 0x04000948 RID: 2376
	public int DefaultTextureIndex;
}
