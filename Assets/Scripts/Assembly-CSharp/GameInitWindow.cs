﻿using System;
using Qoo;

// Token: 0x02000112 RID: 274
public class GameInitWindow : BaseWindow
{
	// Token: 0x06000744 RID: 1860 RVA: 0x0001F414 File Offset: 0x0001D614
	protected override void OnBaseWindowUpdate()
	{
		if (this.isChangeScene)
		{
			SceneManager.ChangeScene(UIValue.SCENE_TITLE);
			this.isChangeScene = false;
		}
		else
		{
			Debug.Print("＊＊＊GameInitおわた＊＊＊");
			this.isChangeScene = true;
		}
	}

	// Token: 0x040006A7 RID: 1703
	private bool isChangeScene;
}
