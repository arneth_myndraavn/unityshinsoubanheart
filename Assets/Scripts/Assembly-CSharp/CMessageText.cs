﻿using Qoo.Def;
using Qoo.Game;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;

// Token: 0x020000B2 RID: 178
public class CMessageText
{
	// Token: 0x0600052A RID: 1322 RVA: 0x00013B8C File Offset: 0x00011D8C
	public CMessageText(UIFont font_ = null, bool IsKerning = true)
	{
		this.font = font_;
		this.m_Anim.nCnt = 0;
		this.m_Anim.nWait = 0;
		this.m_Anim.nSpeed = 1;
		this.m_isOver = false;
		this.m_DrawPos.SetPos(0, 0);
		this.m_CurPos.SetPos(0, 0);
		this.m_DispPos.SetPos(0, 0);
		this.m_ChNum.SetSize(0, 0);
		this.m_CurSize.SetSize(0, 0);
		this.m_Clip.SetSize(0, 0);
		this.m_nChNum = 0;
		this.m_nClipWEx = 0;
		this.m_isAutoRet = true;
		this.m_anDefCol = new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue);
		this.m_anKeyCol = new Color32(byte.MaxValue, 192, 0, byte.MaxValue);
		this.m_KeyCursor.BgColor = new Color32(0, 0, 0, 0);
		this.m_KeyCursor.CursorColor = new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, 128);
		this.SpCode.AddRange(MsgDef.SP_CODES);
		this.LineCode.AddRange(MsgDef.LINETOP_CODES);
		this.m_isKerning = IsKerning;

    }

	// Token: 0x170000BD RID: 189
	// (get) Token: 0x0600052C RID: 1324 RVA: 0x00013D1C File Offset: 0x00011F1C
	// (set) Token: 0x0600052D RID: 1325 RVA: 0x00013D24 File Offset: 0x00011F24
	public static bool IsHyphenation
	{
		get
		{
			return CMessageText.m_isHyphenation;
		}
		set
		{
			CMessageText.m_isHyphenation = value;
		}
	}

	// Token: 0x0600052E RID: 1326 RVA: 0x00013D2C File Offset: 0x00011F2C
	public void Init(int nX, int nY, int nZ, int nColum, int nLine, int nClipW, int nClipH, int nClipWEx)
	{
		if (this.m_ChrData != null)
		{
			this.Release();
		}
		this.m_DrawPos.SetPos(nX, nY);
		this.m_nZ = nZ;
		this.m_ChNum.SetSize(nColum, nLine);
		this.m_nChNum = nColum * nLine;
		this.m_Clip.nCx = nClipW;
		this.m_Clip.nCy = nClipH;
		this.m_nClipWEx = nClipWEx;
		this.m_Anim.nCnt = 0;
		this.m_Anim.nWait = 0;
		this.m_Anim.nSpeed = 1;
		this.m_CurPos.SetPos(0, 0);
		this.m_DispPos.SetPos(0, 0);
		this.m_CurSize.SetSize(0, 0);
		this.m_ChrData = new CharObject[this.m_nChNum];
		this.m_ExtData = new CMessageText.EXTINFO[this.m_nChNum];
		for (int num = 0; num != this.m_nChNum; num++)
		{
			this.m_ChrData[num] = new CharObject();
			this.m_ExtData[num] = new CMessageText.EXTINFO();
		}
		this.m_KeyCursor.Init();
	}

	// Token: 0x0600052F RID: 1327 RVA: 0x00013E44 File Offset: 0x00012044
	public void Release()
	{
		this.m_ChrData = null;
		this.m_ExtData = null;
		this.m_KeyCursor.Release();
	}

	// Token: 0x06000530 RID: 1328 RVA: 0x00013E60 File Offset: 0x00012060
	public void UpdateAnim()
	{
		if (this.m_DispPos.nX == this.m_CurPos.nX && this.m_DispPos.nY == this.m_CurPos.nY)
		{
			return;
		}
		if (this.m_Anim.nCnt < this.m_Anim.nWait)
		{
			this.m_Anim.nCnt = this.m_Anim.nCnt + 1;
			return;
		}
		this.m_Anim.nCnt = 0;
		int i = 0;
		IL_1CB:
		while (i < this.m_Anim.nSpeed)
		{
			this.m_DispPos.nX = this.m_DispPos.nX + 1;
			while (this.m_DispPos.nY < this.m_ChNum.nCy)
			{
				for (int j = 0; j <= this.m_DispPos.nX; j++)
				{
					if (j < this.m_ChNum.nCx)
					{
						CharObject chrInfo = this.GetChrInfo(j, this.m_DispPos.nY);
						if (chrInfo.code != '\u3000')
						{
							if (chrInfo.code != ' ')
							{
								if (chrInfo.code != '\0')
								{
									chrInfo.color.a = byte.MaxValue;
								}
							}
						}
					}
				}
				CharObject chrInfo2 = this.GetChrInfo(this.m_DispPos.nX, this.m_DispPos.nY);
				if (this.m_DispPos.nX < this.m_ChNum.nCx && chrInfo2.code != '\0')
				{
					i++;
					goto IL_1CB;
				}
				this.m_DispPos.nX = 0;
				this.m_DispPos.nY = this.m_DispPos.nY + 1;
			}
			this.m_DispPos.nX = this.m_CurPos.nX;
			this.m_DispPos.nY = this.m_CurPos.nY;
			return;
		}
	}

	// Token: 0x06000531 RID: 1329 RVA: 0x0001404C File Offset: 0x0001224C
	public void Show(bool bShow)
	{
		this.m_KeyCursor.Show(bShow);
	}

	// Token: 0x06000532 RID: 1330 RVA: 0x0001405C File Offset: 0x0001225C
	public void ResetMessage()
	{
		if (this.m_ChrData == null)
		{
			return;
		}
		for (int i = 0; i < this.m_nChNum; i++)
		{
			this.m_ChrData[i].code = '\0';
			this.m_ChrData[i].size = 0;
			this.m_ExtData[i].nKeywordNo = -1;
			this.m_ChrData[i].color.r = this.m_anDefCol.r;
			this.m_ChrData[i].color.g = this.m_anDefCol.g;
			this.m_ChrData[i].color.b = this.m_anDefCol.b;
			this.m_ChrData[i].color.a = 0;
		}
		this.m_CurPos.SetPos(0, 0);
		this.m_DispPos.SetPos(0, 0);
		this.m_CurSize.SetSize(0, 0);
		this.m_Anim.nCnt = 0;
		this.m_LineTop.bFlag = false;
		this.m_isOver = false;
		this.m_KeyCursor.Reset(true);
	}

	// Token: 0x06000533 RID: 1331 RVA: 0x00014174 File Offset: 0x00012374
	public void AddMessage(string szMsg, Color32 color, MSGFONTSIZE nFontType = MSGFONTSIZE.NORMAL, int nKeywordNo = -1)
	{
        
        int testWidth = 0;
        if (!UnityApp.PlatformApp.isJapanese)
        {
            for (int i = 0; i < szMsg.Length; i++)
            {
                //Force diacritics to closest letter with no accent mark
                if (Regex.IsMatch(szMsg[i].ToString(), "[À-ž]"))
                {
                    szMsg = szMsg.Remove(i, 1).Insert(i, szMsg[i].ToString().Normalize(NormalizationForm.FormC).Normalize(NormalizationForm.FormD));
                }
                //Auto line breaker based on character widths
                BMGlyph glyph = this.font.Font.GetGlyph((int)szMsg[i], true);
                if (glyph != null)
                {
                    testWidth += glyph.width;
                }
                else
                {
                    break;
                }
                if (szMsg[i] == '\n')
                {
                    testWidth = 0;
                }
                if (testWidth > 960)
                {
                    for (int j = i; j >= 0; j--)
                    {
                        if (szMsg[j] == ' ')
                        {
                            szMsg = szMsg.Remove(j, 1).Insert(j, "\n");
                            i = j;
                            testWidth = 0;


                            break;
                        }
                    }
                }
            }
        }

		foreach (char c in szMsg)
		{
			char c2 = '\0';
			if (c != '\r')
			{
				if (c == '\n')
				{
					this.m_CurPos.nX = 0;
					this.m_CurPos.nY = this.m_CurPos.nY + 1;
					this.Normalize();
				}
				else if (c >= ' ')
				{
					c2 = c;
				}
				else
				{
					c2 = c;
				}
				if (CMessageText.EnabelLINETOP_PROC && this.m_LineTop.bOn && this.m_CurPos.nX == 0 && this.m_CurPos.nY == 0)
				{
					this.m_LineTop.bFlag = this.CheckLineTopCode(c2);
					if (this.m_LineTop.bFlag)
					{
						this.m_LineTop.nFontType = nFontType;
					}
				}
				if (c2 != '\0')
				{
					if (this.m_LineTop.bFlag && this.m_CurPos.nX == 0 && this.m_CurPos.nY > 0)
					{
						this.AddChar('\u3000', new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, 0), this.m_LineTop.nFontType, false, -1);
					}
					bool bUdrExt = this.CheckSpCode(c2);
					if (!this.AddChar(c2, color, nFontType, bUdrExt, nKeywordNo) && this.m_CurSize.nCx >= 0 && this.m_CurSize.nCy >= 0)
					{
						if (this.m_isAutoRet)
						{
							this.m_CurPos.nX = 0;
							this.m_CurPos.nY = this.m_CurPos.nY + 1;
							this.Normalize();
							if (this.m_LineTop.bFlag)
							{
								this.AddChar('\u3000', new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, 0), this.m_LineTop.nFontType, false, -1);
							}
							this.AddChar(c2, color, nFontType, false, nKeywordNo);
						}
						else
						{
							this.m_isOver = true;
						}
					}
				}
			}
		}
		this.Normalize();
	}

	// Token: 0x06000534 RID: 1332 RVA: 0x00014390 File Offset: 0x00012590
	public CharObject[] GetArray()
	{
		return this.m_ChrData;
	}

	// Token: 0x06000535 RID: 1333 RVA: 0x00014398 File Offset: 0x00012598
	public void Full()
	{
		this.m_DispPos.nX = this.m_CurPos.nX;
		this.m_DispPos.nY = this.m_CurPos.nY;
		for (int i = 0; i < this.m_nChNum; i++)
		{
			if (this.m_ChrData[i].code != '\u3000')
			{
				if (this.m_ChrData[i].code != ' ')
				{
					if (this.m_ChrData[i].code != '\0')
					{
						this.m_ChrData[i].color.a = byte.MaxValue;
					}
				}
			}
		}
		this.NormalizeKeyCursor();
		for (int j = 0; j < this.m_nChNum; j++)
		{
			if (this.m_ExtData[j].nKeywordNo >= 0)
			{
				this.m_ChrData[j].color.r = this.m_anKeyCol.r;
				this.m_ChrData[j].color.g = this.m_anKeyCol.g;
				this.m_ChrData[j].color.b = this.m_anKeyCol.b;
			}
		}
		this.m_KeyCursor.UpdateKeyCursor();
		this.SetWordFlag();
	}

	// Token: 0x06000536 RID: 1334 RVA: 0x000144E0 File Offset: 0x000126E0
	public bool IsFull()
	{
		return this.m_DispPos.nX == this.m_CurPos.nX && this.m_DispPos.nY == this.m_CurPos.nY;
	}

	// Token: 0x06000537 RID: 1335 RVA: 0x0001451C File Offset: 0x0001271C
	public int GetCursorPosX()
	{
		return this.m_DrawPos.nX + this.m_CurSize.nCx;
	}

	// Token: 0x06000538 RID: 1336 RVA: 0x00014538 File Offset: 0x00012738
	public int GetCursorPosY()
	{
		return this.m_DrawPos.nY + this.m_CurSize.nCy;
	}

	// Token: 0x06000539 RID: 1337 RVA: 0x00014554 File Offset: 0x00012754
	public void SetAnimSpeed(int nSpeed, int nWait)
	{
		this.m_Anim.nSpeed = nSpeed;
		this.m_Anim.nWait = nWait;
	}

	// Token: 0x0600053A RID: 1338 RVA: 0x00014570 File Offset: 0x00012770
	public string SaveText()
	{
		string text = string.Empty;
		for (int i = 0; i < this.m_ChNum.nCy; i++)
		{
			if (this.GetChrInfo(0, i).code != '\0')
			{
				for (int j = 0; j < this.m_ChNum.nCx; j++)
				{
					CharObject chrInfo = this.GetChrInfo(j, i);
					if (chrInfo.code == '\0')
					{
						break;
					}
					text += chrInfo.code;
				}
				text += '\n';
			}
		}
		return text;
	}

	// Token: 0x0600053B RID: 1339 RVA: 0x0001460C File Offset: 0x0001280C
	public void ChangeClipSize(int nClipW, int nClipH, int nClipWEx)
	{
		this.m_Clip.nCx = nClipW;
		this.m_Clip.nCy = nClipH;
		this.m_nClipWEx = nClipWEx;
	}

	// Token: 0x0600053C RID: 1340 RVA: 0x00014630 File Offset: 0x00012830
	public void EnableAutoReturn(bool bOn)
	{
		this.m_isAutoRet = bOn;
	}

	// Token: 0x0600053D RID: 1341 RVA: 0x0001463C File Offset: 0x0001283C
	public void ChangePos(int nX, int nY, int nZ, bool bUpdate = false)
	{
		this.m_DrawPos.SetPos(nX, nY);
		this.m_nZ = nZ;
		if (bUpdate)
		{
			this.Normalize();
		}
	}

	// Token: 0x0600053E RID: 1342 RVA: 0x00014660 File Offset: 0x00012860
	public void SetColor(byte nR, byte nG, byte nB)
	{
		this.m_anDefCol = new Color32(nR, nG, nB, byte.MaxValue);
		for (int i = 0; i < this.m_nChNum; i++)
		{
			if (this.m_ExtData[i].nKeywordNo < 0)
			{
				this.m_ChrData[i].color.r = this.m_anDefCol.r;
				this.m_ChrData[i].color.g = this.m_anDefCol.g;
				this.m_ChrData[i].color.b = this.m_anDefCol.b;
			}
			else
			{
				this.m_ChrData[i].color.r = this.m_anKeyCol.r;
				this.m_ChrData[i].color.g = this.m_anKeyCol.g;
				this.m_ChrData[i].color.b = this.m_anKeyCol.b;
			}
		}
	}

	// Token: 0x0600053F RID: 1343 RVA: 0x00014760 File Offset: 0x00012960
	public bool IsOver()
	{
		return this.m_isOver;
	}

	// Token: 0x06000540 RID: 1344 RVA: 0x00014768 File Offset: 0x00012968
	public void EnableLineTopProc(bool bOn)
	{
		this.m_LineTop.bOn = bOn;
	}

	// Token: 0x06000541 RID: 1345 RVA: 0x00014778 File Offset: 0x00012978
	public bool MoveKeyCursor(int nVec)
	{
		this.m_KeyCursor.MoveKeyCursor(nVec);
		return true;
	}

	// Token: 0x06000542 RID: 1346 RVA: 0x00014788 File Offset: 0x00012988
	public int GetKeyNum()
	{
		return this.m_KeyCursor.Num;
	}

	// Token: 0x06000543 RID: 1347 RVA: 0x00014798 File Offset: 0x00012998
	public void ResetKeySel()
	{
		this.m_KeyCursor.ResetKeySel();
	}

	// Token: 0x06000544 RID: 1348 RVA: 0x000147A8 File Offset: 0x000129A8
	public string GetWordString(int nIndex)
	{
		Qoo.Debug.Assert(nIndex < this.GetKeyNum());
		string text = string.Empty;
		for (int i = 0; i < this.m_nChNum; i++)
		{
			if ((int)this.m_ExtData[i].nKeywordNo == nIndex)
			{
				text += this.m_ChrData[i].code;
			}
		}
		return text;
	}

	// Token: 0x06000545 RID: 1349 RVA: 0x00014810 File Offset: 0x00012A10
	public string GetSelectedWordString()
	{
		if (this.m_KeyCursor.Select < 0)
		{
			return string.Empty;
		}
		return this.GetWordString(this.m_KeyCursor.Select);
	}

	// Token: 0x06000546 RID: 1350 RVA: 0x00014848 File Offset: 0x00012A48
	public void SetWordFlag()
	{
		for (int i = 0; i < this.m_KeyCursor.Num; i++)
		{
			SysData.SetReadRef(this.GetWordString(i));
		}
	}

	// Token: 0x06000547 RID: 1351 RVA: 0x00014880 File Offset: 0x00012A80
	public void SetKeyTxtColor(byte nR, byte nG, byte nB)
	{
		this.m_anKeyCol = new Color32(nR, nG, nB, byte.MaxValue);
	}

	// Token: 0x06000548 RID: 1352 RVA: 0x00014898 File Offset: 0x00012A98
	public void SetKeyCurColor(byte r, byte g, byte b, byte a)
	{
		this.m_KeyCursor.CursorColor = new Color32(r, g, b, a);
	}

	// Token: 0x06000549 RID: 1353 RVA: 0x000148B0 File Offset: 0x00012AB0
	public void SetKeyBgColor(byte r, byte g, byte b, byte a)
	{
		this.m_KeyCursor.BgColor = new Color32(r, g, b, a);
	}

	// Token: 0x0600054A RID: 1354 RVA: 0x000148C8 File Offset: 0x00012AC8
	private bool AddChar(char chCode, Color32 color, MSGFONTSIZE nFontType, bool bUdrExt, int nKeywordNo)
	{
		if (this.m_CurSize.nCx < 0 || this.m_CurSize.nCy < 0)
		{
			return false;
		}
		BMGlyph glyph = this.font.Font.GetGlyph((int)chCode);
		if (glyph == null)
		{
			return false;
		}
		int nSize = FontDef.GetFontInfo(nFontType).nSize;
		float num = (float)glyph.advance / (float)this.font.Font.charSize;
		int num2 = (int)((float)nSize * num);
		int num3 = (int)((float)nSize * ((float)glyph.height / (float)this.font.Font.charSize));
		num2 = (int)((float)num2 * FontDef.GetFontInfo(nFontType).fStretch);
		num3 = (int)((float)num3 * FontDef.GetFontInfo(nFontType).fStretch);
		if (this.m_CurPos.nY >= this.m_ChNum.nCy)
		{
			this.m_isOver = true;
			return true;
		}
		if (bUdrExt)
		{
			if (this.m_CurSize.nCx + num2 > this.m_Clip.nCx + this.m_nClipWEx)
			{
				return false;
			}
		}
		else if (this.m_CurSize.nCx + num2 > this.m_Clip.nCx)
		{
			if (this.IsRunHyphenation(chCode))
			{
				this.AddChar('-', color, nFontType, true, nKeywordNo);
			}
            return false;
		}
		if (this.m_CurPos.nX >= this.m_ChNum.nCx)
		{
			this.m_isOver = true;
			return true;
		}
		if (this.m_isKerning && this.m_CurPos.nX > 0)
		{
			CharObject chrInfo = this.GetChrInfo(this.m_CurPos.nX - 1, this.m_CurPos.nY);
			if (chrInfo != null)
			{
				this.m_DrawPos.nX = this.m_DrawPos.nX + (int)((float)glyph.GetKerning((int)chrInfo.code, true) * num);
			}
		}
		int x = this.m_DrawPos.nX + this.m_CurSize.nCx;
		CharObject chrInfo2 = this.GetChrInfo();
		chrInfo2.code = chCode;
		chrInfo2.color = color;
		chrInfo2.X = x;
		chrInfo2.Y = 0;
		chrInfo2.height = nSize;
		chrInfo2.width = num2;
		chrInfo2.size = nSize;
		chrInfo2.glyph = glyph;
		chrInfo2.color.r = this.m_anDefCol.r;
		chrInfo2.color.g = this.m_anDefCol.g;
		chrInfo2.color.b = this.m_anDefCol.b;
		chrInfo2.color.a = 0;
		CMessageText.EXTINFO extInfo = this.GetExtInfo();
		extInfo.nKeywordNo = (short)nKeywordNo;
		this.m_CurPos.nX = this.m_CurPos.nX + 1;
		this.m_CurSize.nCx = this.m_CurSize.nCx + num2;
		return true;
	}

	// Token: 0x0600054B RID: 1355 RVA: 0x00014B94 File Offset: 0x00012D94
	private bool IsRunHyphenation(char chCode)
	{
		if (CMessageText.IsHyphenation)
		{
			CharObject lastChrInfo = this.GetLastChrInfo(this.m_CurPos.nY);
            return Regex.IsMatch(lastChrInfo.code.ToString(), "^[a-zA-Z0-9]+$") && Regex.IsMatch(chCode.ToString(), "^[a-zA-Z0-9]+$");
		}
		return false;
	}

	// Token: 0x0600054C RID: 1356 RVA: 0x00014BF0 File Offset: 0x00012DF0
	private void Normalize()
	{
		if (this.m_CurSize.nCx < 0 || this.m_CurSize.nCy < 0)
		{
			return;
		}
		this.m_CurSize.nCy = 0;
		for (int i = 0; i <= this.m_CurPos.nY; i++)
		{
			if (i >= this.m_ChNum.nCy)
			{
				break;
			}
			this.m_CurSize.nCx = 0;
			if (this.GetChrInfo(0, i).code != '\0')
			{
				int num = 0;
				for (int j = 0; j < this.m_ChNum.nCx; j++)
				{
					CharObject chrInfo = this.GetChrInfo(j, i);
					if (chrInfo.code == '\0')
					{
						break;
					}
					if (num < chrInfo.Height)
					{
						num = chrInfo.Height;
					}
				}
				if (this.m_CurSize.nCy + num > this.m_Clip.nCy)
				{
					for (int k = 0; k < this.m_ChNum.nCx; k++)
					{
						this.GetChrInfo(k, i).code = '\0';
					}
					this.m_CurSize.nCx = -1;
					this.m_CurSize.nCy = -1;
					this.m_isOver = true;
					break;
				}
				for (int l = 0; l < this.m_ChNum.nCx; l++)
				{
					CharObject chrInfo2 = this.GetChrInfo(l, i);
					if (chrInfo2.code == '\0')
					{
						break;
					}
					chrInfo2.Y = this.m_DrawPos.nY + this.m_CurSize.nCy + (num - chrInfo2.Height);
					this.m_CurSize.nCx = this.m_CurSize.nCx + chrInfo2.Width;
				}
				this.m_CurSize.nCy = this.m_CurSize.nCy + num;
				this.m_CurSize.nCy = this.m_CurSize.nCy + 4;
			}
		}
	}

	// Token: 0x0600054D RID: 1357 RVA: 0x00014DE0 File Offset: 0x00012FE0
	private void NormalizeKeyCursor()
	{
		this.m_KeyCursor.Reset(false);
		if (this.m_CurSize.nCx < 0 || this.m_CurSize.nCy < 0)
		{
			return;
		}
		short num = -1;
		for (int i = 0; i <= this.m_CurPos.nY; i++)
		{
			if (i >= this.m_ChNum.nCy)
			{
				break;
			}
			if (this.GetChrInfo(0, i).code != '\0')
			{
				for (int j = 0; j < this.m_ChNum.nCx; j++)
				{
					CharObject chrInfo = this.GetChrInfo(j, i);
					if (chrInfo.code == '\0')
					{
						break;
					}
					CMessageText.EXTINFO extInfo = this.GetExtInfo(j, i);
					if (extInfo.nKeywordNo != num && num >= 0)
					{
						num = -1;
						this.m_KeyCursor.Add();
					}
					else if (num >= 0)
					{
						this.m_KeyCursor.SetCursorSize(chrInfo.X + chrInfo.Width, chrInfo.Height, false);
					}
					if (extInfo.nKeywordNo >= 0 && num < 0)
					{
						this.m_KeyCursor.SetCursorPos(chrInfo.X, chrInfo.Y, this.m_nZ);
						this.m_KeyCursor.SetCursorSize(chrInfo.Width, chrInfo.Height, true);
						num = extInfo.nKeywordNo;
						this.m_KeyCursor.SetCursorGroup((int)extInfo.nKeywordNo);
						if (this.m_KeyCursor.Num < (int)(extInfo.nKeywordNo + 1))
						{
							this.m_KeyCursor.Num = (int)(extInfo.nKeywordNo + 1);
						}
					}
				}
				if (num >= 0)
				{
					num = -1;
					this.m_KeyCursor.Add();
				}
			}
		}
	}

	// Token: 0x0600054E RID: 1358 RVA: 0x00014F98 File Offset: 0x00013198
	private CharObject GetChrInfo(int x, int y)
	{
		Qoo.Debug.Assert(x < this.m_ChNum.nCx && y < this.m_ChNum.nCy);
		return this.m_ChrData[y * this.m_ChNum.nCx + x];
	}

	// Token: 0x0600054F RID: 1359 RVA: 0x00014FD8 File Offset: 0x000131D8
	private CharObject GetChrInfo()
	{
		return this.GetChrInfo(this.m_CurPos.nX, this.m_CurPos.nY);
	}

	// Token: 0x06000550 RID: 1360 RVA: 0x00014FF8 File Offset: 0x000131F8
	private CharObject GetLastChrInfo(int y)
	{
        for (int i = this.m_ChNum.nCx - 1; i >= 0; i--)
		{
			CharObject chrInfo = this.GetChrInfo(i, y);
			if (chrInfo != null && chrInfo.code != '\0')
			{
				return chrInfo;
			}
		}
		return null;
	}

	// Token: 0x06000551 RID: 1361 RVA: 0x00015040 File Offset: 0x00013240
	private CMessageText.EXTINFO GetExtInfo(int x, int y)
	{
		Qoo.Debug.Assert(x < this.m_ChNum.nCx && y < this.m_ChNum.nCy);
		return this.m_ExtData[y * this.m_ChNum.nCx + x];
	}

	// Token: 0x06000552 RID: 1362 RVA: 0x00015080 File Offset: 0x00013280
	private CMessageText.EXTINFO GetExtInfo()
	{
		return this.GetExtInfo(this.m_CurPos.nX, this.m_CurPos.nY);
	}

	// Token: 0x06000553 RID: 1363 RVA: 0x000150A0 File Offset: 0x000132A0
	private bool CheckSpCode(char code)
	{
		return this.SpCode.Contains(code);
	}

	// Token: 0x06000554 RID: 1364 RVA: 0x000150B0 File Offset: 0x000132B0
	private bool CheckLineTopCode(char code)
	{
		return this.LineCode.Contains(code);
	}

	// Token: 0x06000555 RID: 1365 RVA: 0x000150C0 File Offset: 0x000132C0
	internal string GetMessage()
	{
		string text = string.Empty;
		for (int i = 0; i < this.m_ChNum.nCy; i++)
		{
			for (int j = 0; j < this.m_ChNum.nCx; j++)
			{
				CharObject chrInfo = this.GetChrInfo(j, i);
				if (chrInfo.code != '\0')
				{
					text += chrInfo.code;
				}
			}
			if (i < this.m_ChNum.nCy - 1)
			{
				text += "\n";
			}
		}
		return text;
	}

	// Token: 0x040003D4 RID: 980
	private const int CURSP_MAX = 8;

	// Token: 0x040003D5 RID: 981
	private const char ZEN_SPACE = '\u3000';

	// Token: 0x040003D6 RID: 982
	public static bool EnabelLINETOP_PROC = true;

	// Token: 0x040003D7 RID: 983
	private UIFont font;

	// Token: 0x040003D8 RID: 984
	private CharObject[] m_ChrData;

	// Token: 0x040003D9 RID: 985
	private CMessageText.EXTINFO[] m_ExtData;

	// Token: 0x040003DA RID: 986
	private int m_nZ;

	// Token: 0x040003DB RID: 987
	private CMessageText.INT_POS m_DrawPos;

	// Token: 0x040003DC RID: 988
	private CMessageText.INT_SIZE m_ChNum;

	// Token: 0x040003DD RID: 989
	private CMessageText.INT_POS m_CurPos;

	// Token: 0x040003DE RID: 990
	private CMessageText.INT_SIZE m_CurSize;

	// Token: 0x040003DF RID: 991
	private CMessageText.INT_POS m_DispPos;

	// Token: 0x040003E0 RID: 992
	private CMessageText.INT_SIZE m_Clip;

	// Token: 0x040003E1 RID: 993
	private int m_nClipWEx;

	// Token: 0x040003E2 RID: 994
	private int m_nChNum;

	// Token: 0x040003E3 RID: 995
	private CMessageText.ANIMDATA m_Anim;

	// Token: 0x040003E4 RID: 996
	private bool m_isAutoRet;

	// Token: 0x040003E5 RID: 997
	private Color32 m_anDefCol = new Color32(0, 0, 0, byte.MaxValue);

	// Token: 0x040003E6 RID: 998
	private Color32 m_anKeyCol = new Color32(0, 0, 0, byte.MaxValue);

	// Token: 0x040003E7 RID: 999
	private bool m_isOver;

	// Token: 0x040003E8 RID: 1000
	private CMessageText.LINETOPINFO m_LineTop;

	// Token: 0x040003E9 RID: 1001
	private CursorData m_KeyCursor = new CursorData();

	// Token: 0x040003EA RID: 1002
	private List<char> SpCode = new List<char>();

	// Token: 0x040003EB RID: 1003
	private List<char> LineCode = new List<char>();

	// Token: 0x040003EC RID: 1004
	private bool m_isKerning;

	// Token: 0x040003ED RID: 1005
	private static bool m_isHyphenation;

	// Token: 0x020000B3 RID: 179
	private struct INT_POS
	{
		// Token: 0x06000556 RID: 1366 RVA: 0x00015154 File Offset: 0x00013354
		public void SetPos(int x, int y)
		{
			this.nX = x;
			this.nY = y;
		}

		// Token: 0x040003EE RID: 1006
		public int nX;

		// Token: 0x040003EF RID: 1007
		public int nY;
	}

	// Token: 0x020000B4 RID: 180
	private struct INT_SIZE
	{
		// Token: 0x06000557 RID: 1367 RVA: 0x00015164 File Offset: 0x00013364
		public void SetSize(int x, int y)
		{
			this.nCx = x;
			this.nCy = y;
		}

		// Token: 0x040003F0 RID: 1008
		public int nCx;

		// Token: 0x040003F1 RID: 1009
		public int nCy;
	}

	// Token: 0x020000B5 RID: 181
	private class EXTINFO
	{
		// Token: 0x040003F2 RID: 1010
		public short nKeywordNo;
	}

	// Token: 0x020000B6 RID: 182
	private struct ANIMDATA
	{
		// Token: 0x040003F3 RID: 1011
		public int nSpeed;

		// Token: 0x040003F4 RID: 1012
		public int nWait;

		// Token: 0x040003F5 RID: 1013
		public int nCnt;
	}

	// Token: 0x020000B7 RID: 183
	private struct LINETOPINFO
	{
		// Token: 0x040003F6 RID: 1014
		public bool bOn;

		// Token: 0x040003F7 RID: 1015
		public bool bFlag;

		// Token: 0x040003F8 RID: 1016
		public MSGFONTSIZE nFontType;
	}
}
