﻿using System;
using UnityEngine;

// Token: 0x0200019D RID: 413
public class UIActionButton : UIAction
{
	// Token: 0x06000BF7 RID: 3063 RVA: 0x00032178 File Offset: 0x00030378
	public static UIActionButton Create(GameObject go, int normal = 0, int press = 1)
	{
		UIActionButton uiactionButton = go.AddComponent<UIActionButton>();
		uiactionButton.m_NormalIndex = normal;
		uiactionButton.m_PressIndex = press;

		return uiactionButton;
	}

	// Token: 0x06000BF8 RID: 3064 RVA: 0x0003219C File Offset: 0x0003039C
	public override void Init()
	{
		base.Init();
	}

	// Token: 0x06000BF9 RID: 3065 RVA: 0x000321A4 File Offset: 0x000303A4
	private void SetImageIndex(int index)
	{
		if (base.ActionEnable)
		{
			this.imageObject.ImageBlockIndex = index;
			this.imageObject.InitTextureUV();
		}
	}

	// Token: 0x06000BFA RID: 3066 RVA: 0x000321D4 File Offset: 0x000303D4
	private void OnMouseDown()
	{

		this.SetImageIndex(this.m_PressIndex);
	}

	// Token: 0x06000BFB RID: 3067 RVA: 0x000321E4 File Offset: 0x000303E4
	private void OnMouseUp()
	{

		this.SetImageIndex(this.m_NormalIndex);
	}

	// Token: 0x0400093C RID: 2364
	private int m_NormalIndex;

	// Token: 0x0400093D RID: 2365
	private int m_PressIndex;
}
