﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x0200018F RID: 399
public class TaskManager : Singleton<TaskManager>
{
	// Token: 0x17000186 RID: 390
	// (get) Token: 0x06000B72 RID: 2930 RVA: 0x00030E00 File Offset: 0x0002F000
	// (set) Token: 0x06000B73 RID: 2931 RVA: 0x00030E08 File Offset: 0x0002F008
	public bool IsTaskRunnning { get; private set; }

	// Token: 0x06000B74 RID: 2932 RVA: 0x00030E14 File Offset: 0x0002F014
	private void Awake()
	{
		this.IsTaskRunnning = false;
		base.StartCoroutine("UpdateTask");
	}

	// Token: 0x06000B75 RID: 2933 RVA: 0x00030E2C File Offset: 0x0002F02C
	private void Update()
	{
	}

	// Token: 0x06000B76 RID: 2934 RVA: 0x00030E30 File Offset: 0x0002F030
	public bool AddMainTask(UnityTask task_)
	{
		this.MainTaskList.Add(task_);
		return true;
	}

	// Token: 0x06000B77 RID: 2935 RVA: 0x00030E40 File Offset: 0x0002F040
	public bool AddSubTask(UnityTask task_)
	{
		this.SubTaskList.Add(task_);
		return true;
	}

	// Token: 0x06000B78 RID: 2936 RVA: 0x00030E50 File Offset: 0x0002F050
	public bool AddReadTask(UnityTask task_)
	{
		this.ReadTaskList.Add(task_);
		return true;
	}

	// Token: 0x06000B79 RID: 2937 RVA: 0x00030E60 File Offset: 0x0002F060
	public void UpdateReadTask()
	{
		base.StartCoroutine(this.RunReadTask());
	}

	// Token: 0x06000B7A RID: 2938 RVA: 0x00030E70 File Offset: 0x0002F070
	public IEnumerator UpdateTask()
	{
		for (;;)
		{
			if (this.MainTaskList.Count == 0 && this.SubTaskList.Count == 0)
			{
				yield return 0;
			}
			while (this.SubTaskList.Count > 0)
			{
				this.IsTaskRunnning = true;
				List<UnityTask> nowList = new List<UnityTask>(this.SubTaskList);
				this.SubTaskList.Clear();
				foreach (UnityTask item2 in nowList)
				{
					yield return base.StartCoroutine(this.RunReadTask());
					switch (item2.type)
					{
					case TASK_TYPE.NEXT_FRAME:
						yield return 0;
						break;
					case TASK_TYPE.TIME_WAIT:
						yield return new WaitForSeconds(item2.param_float);
						break;
					case TASK_TYPE.EFFECT:
					{
						IEnumerator task = Singleton<UnityGraph>.Instance.Effect(item2.param_string, item2.param_float);
						if (task != null)
						{
							yield return base.StartCoroutine(task);
						}
						break;
					}
					case TASK_TYPE.COROUTINE:
						if (item2.task != null)
						{
							yield return base.StartCoroutine(item2.task);
						}
						break;
					case TASK_TYPE.COROUTINE_NOUNITY:
						if (item2.task != null)
						{
							while (item2.MoveNext())
							{
								yield return base.StartCoroutine(this.RunReadTask());
							}
						}
						break;
					}
				}
				nowList.Clear();
				this.IsTaskRunnning = false;
			}
			if (this.MainTaskList.Count > 0)
			{
				List<UnityTask> nowList2 = new List<UnityTask>(this.MainTaskList);
				this.MainTaskList.Clear();
				nowList2.ForEach(delegate(UnityTask item)
				{
					item.IsTaskEnd = !item.MoveNext();
				});
				nowList2.RemoveAll((UnityTask item) => item.IsTaskEnd);
				this.MainTaskList.InsertRange(0, nowList2.GetRange(0, nowList2.Count));
				nowList2.Clear();
			}
			yield return base.StartCoroutine(this.RunReadTask());
		}
		yield break;
	}

	// Token: 0x06000B7B RID: 2939 RVA: 0x00030E8C File Offset: 0x0002F08C
	public IEnumerator RunReadTask()
	{
		List<UnityTask> nowList = new List<UnityTask>(this.ReadTaskList);
		this.ReadTaskList.Clear();
		foreach (UnityTask item in nowList)
		{
			yield return base.StartCoroutine(item.task);
		}
		yield break;
	}

	// Token: 0x04000903 RID: 2307
	private List<UnityTask> MainTaskList = new List<UnityTask>();

	// Token: 0x04000904 RID: 2308
	private List<UnityTask> SubTaskList = new List<UnityTask>();

	// Token: 0x04000905 RID: 2309
	private List<UnityTask> ReadTaskList = new List<UnityTask>();
}
