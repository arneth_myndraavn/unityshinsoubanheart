﻿using System;
using UnityEngine;

// Token: 0x02000143 RID: 323
public class ScriptEventSelfHandler : ScriptEvent
{
	// Token: 0x060008F4 RID: 2292 RVA: 0x00027544 File Offset: 0x00025744
	public static ScriptEvent Create(GameObject go, ScriptEventTriger triger, Action action)
	{
		ScriptEventSelfHandler scriptEventSelfHandler = go.AddComponent<ScriptEventSelfHandler>();
		scriptEventSelfHandler.Triger = triger;
		scriptEventSelfHandler.handler = new ScriptEventSelfHandler.HandlerWithoutParam(action);
		return scriptEventSelfHandler;
	}

	// Token: 0x060008F5 RID: 2293 RVA: 0x0002756C File Offset: 0x0002576C
	public static ScriptEvent Create(GameObject go, ScriptEventTriger triger, Action<int> action, int param)
	{
		ScriptEventSelfHandler scriptEventSelfHandler = go.AddComponent<ScriptEventSelfHandler>();
		scriptEventSelfHandler.Triger = triger;
		scriptEventSelfHandler.handler = new ScriptEventSelfHandler.HandlerWithInt(action, param);
		return scriptEventSelfHandler;
	}

	// Token: 0x060008F6 RID: 2294 RVA: 0x00027598 File Offset: 0x00025798
	public static ScriptEvent Create(GameObject go, ScriptEventTriger triger, Action<string> action, string param)
	{
		ScriptEventSelfHandler scriptEventSelfHandler = go.AddComponent<ScriptEventSelfHandler>();
		scriptEventSelfHandler.Triger = triger;
		scriptEventSelfHandler.handler = new ScriptEventSelfHandler.HandlerWithString(action, param);
		return scriptEventSelfHandler;
	}

	// Token: 0x060008F7 RID: 2295 RVA: 0x000275C4 File Offset: 0x000257C4
	public override void Action()
	{
		this.handler.Action();
	}

	// Token: 0x04000793 RID: 1939
	private ScriptEventSelfHandler.IHandler handler;

	// Token: 0x02000144 RID: 324
	public abstract class IHandler
	{
		// Token: 0x060008F9 RID: 2297
		public abstract void Action();
	}

	// Token: 0x02000145 RID: 325
	public class HandlerWithoutParam : ScriptEventSelfHandler.IHandler
	{
		// Token: 0x060008FA RID: 2298 RVA: 0x000275DC File Offset: 0x000257DC
		public HandlerWithoutParam(Action action_)
		{
			this.action = action_;
		}

		// Token: 0x060008FB RID: 2299 RVA: 0x000275EC File Offset: 0x000257EC
		public override void Action()
		{
			this.action();
		}

		// Token: 0x04000794 RID: 1940
		private Action action;
	}

	// Token: 0x02000146 RID: 326
	public class HandlerWithInt : ScriptEventSelfHandler.IHandler
	{
		// Token: 0x060008FC RID: 2300 RVA: 0x000275FC File Offset: 0x000257FC
		public HandlerWithInt(Action<int> action_, int param_)
		{
			this.action = action_;
			this.param = param_;
		}

		// Token: 0x060008FD RID: 2301 RVA: 0x00027614 File Offset: 0x00025814
		public override void Action()
		{
			this.action(this.param);
		}

		// Token: 0x04000795 RID: 1941
		private Action<int> action;

		// Token: 0x04000796 RID: 1942
		private int param;
	}

	// Token: 0x02000147 RID: 327
	public class HandlerWithString : ScriptEventSelfHandler.IHandler
	{
		// Token: 0x060008FE RID: 2302 RVA: 0x00027628 File Offset: 0x00025828
		public HandlerWithString(Action<string> action_, string param_)
		{
			this.action = action_;
			this.param = param_;
		}

		// Token: 0x060008FF RID: 2303 RVA: 0x0002764C File Offset: 0x0002584C
		public override void Action()
		{
			this.action(this.param);
		}

		// Token: 0x04000797 RID: 1943
		private Action<string> action;

		// Token: 0x04000798 RID: 1944
		private string param = string.Empty;
	}
}
