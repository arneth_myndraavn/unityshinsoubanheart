﻿using System;
using System.Collections;
using Game;

// Token: 0x020000F3 RID: 243
public class PaymentTask : SubPartTask
{
	// Token: 0x06000687 RID: 1671 RVA: 0x0001B07C File Offset: 0x0001927C
	public IEnumerator Open(CHAR_ID charaId)
	{
		UIValue.GalleryCharactor = (int)charaId;
		yield return base.Open(UIValue.SCENE_PAYMENT, true);
		yield break;
	}
}
