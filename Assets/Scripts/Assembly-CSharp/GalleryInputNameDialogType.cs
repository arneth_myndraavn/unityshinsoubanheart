﻿using System;

// Token: 0x020000F9 RID: 249
public enum GalleryInputNameDialogType
{
	// Token: 0x04000610 RID: 1552
	INVALID,
	// Token: 0x04000611 RID: 1553
	EMPTY,
	// Token: 0x04000612 RID: 1554
	OVER,
	// Token: 0x04000613 RID: 1555
	INVALIDCHAR,
	// Token: 0x04000614 RID: 1556
	CONFIRM
}
