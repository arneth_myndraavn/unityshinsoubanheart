﻿using System;

// Token: 0x020000FF RID: 255
public class UIValue
{
	// Token: 0x0400062C RID: 1580
	public static readonly string SCENE_GAMEINIT = "SceneGameInit";

	// Token: 0x0400062D RID: 1581
	public static readonly string SCENE_INIT = "SceneInit";

	// Token: 0x0400062E RID: 1582
	public static readonly string SCENE_TITLE = "SceneTitle";

	// Token: 0x0400062F RID: 1583
	public static readonly string SCENE_TITLERESTORE = "SceneTitleRestore";

	public static readonly string SCENE_TITLECREDITS = "SceneTitleCredits";

	public static readonly string SCENE_SCREENRESSETTING = "SceneScreenResSettings";

	// Token: 0x04000630 RID: 1584
	public static readonly string SCENE_GALLERYMENU = "SceneGalleryMenu";

	// Token: 0x04000631 RID: 1585
	public static readonly string SCENE_GALLERYMOVIE = "SceneGalleryMovie";

	// Token: 0x04000632 RID: 1586
	public static readonly string SCENE_GALLERYCG = "SceneGalleryCG";

	// Token: 0x04000633 RID: 1587
	public static readonly string SCENE_GALLERYINPUTNAME = "SceneGalleryInputName";

	// Token: 0x04000634 RID: 1588
	public static readonly string SCENE_GALLERYINPUTNAMEKEYBOARD = "SceneGalleryInputNameKeyboard";

	// Token: 0x04000635 RID: 1589
	public static readonly string SCENE_GALLERYINPUTNAMEDIALOG = "SceneGalleryInputNameDialog";

	// Token: 0x04000636 RID: 1590
	public static readonly string SCENE_CGCHARASELECT = "SceneCGCharaSelect";

	// Token: 0x04000637 RID: 1591
	public static readonly string SCENE_CGSELECT = "SceneCGSelect";

	// Token: 0x04000638 RID: 1592
	public static readonly string SCENE_CGVIEWER = "SceneCGViewer";

	// Token: 0x04000639 RID: 1593
	public static readonly string SCENE_MEMORYCHARASELECT = "SceneMemoryCharaSelect";

	// Token: 0x0400063A RID: 1594
	public static readonly string SCENE_MEMORYSELECT = "SceneMemorySelect";

	// Token: 0x0400063B RID: 1595
	public static readonly string SCENE_ADVMENU = "SceneAdvMenu";

	// Token: 0x0400063C RID: 1596
	public static readonly string SCENE_STORYSELECT = "SceneStorySelect";

	// Token: 0x0400063D RID: 1597
	public static readonly string SCENE_INTRODUCTION = "SceneIntroduction";

	// Token: 0x0400063E RID: 1598
	public static readonly string SCENE_PAYMENT = "ScenePayment";

	// Token: 0x0400063F RID: 1599
	public static readonly string SCENE_PAYMENTDIALOG = "ScenePaymentDialog";

	// Token: 0x04000640 RID: 1600
	public static readonly string SCENE_PAYMENTLIST = "ScenePaymentList";

	// Token: 0x04000641 RID: 1601
	public static readonly string SCENE_SAVELOAD = "SceneSaveLoad";

	// Token: 0x04000642 RID: 1602
	public static readonly string SCENE_HELP = "SceneHelp";

	// Token: 0x04000643 RID: 1603
	public static readonly string SCENE_ADVMODE = "SceneAdvMode";

	// Token: 0x04000644 RID: 1604
	public static readonly string SCENE_LOGOMOVIE = "SceneLogoMovie";

	// Token: 0x04000645 RID: 1605
	public static readonly string SCENE_OPTIONTEXT = "SceneOptionText";

	// Token: 0x04000646 RID: 1606
	public static readonly string SCENE_OPTIONSOUND = "SceneOptionSound";

	// Token: 0x04000647 RID: 1607
	public static readonly string SCENE_OPTIONVOICE = "SceneOptionVoice";

	// Token: 0x04000648 RID: 1608
	public static readonly string SCENE_OPTIONSCREEN = "SceneOptionScreen";

	// Token: 0x04000649 RID: 1609
	public static readonly string SCENE_OPTIONSKIP = "SceneOptionSkip";

	// Token: 0x0400064A RID: 1610
	public static readonly string SCENE_OPTIONBACK = "SceneOptionBack";

	// Token: 0x0400064B RID: 1611
	public static int GalleryCharactor;

	// Token: 0x0400064C RID: 1612
	public static int GalleryPage;

	// Token: 0x0400064D RID: 1613
	public static int GalleryIndex;

	// Token: 0x0400064E RID: 1614
	public static GalleryInputNameDialogType GalleryInputNameDialog_Type;

	// Token: 0x0400064F RID: 1615
	public static string GalleryInputNameDialog_Name;

    public static string GalleryInputNameDialog_LastName;

    // Token: 0x04000650 RID: 1616
    public static GalleryInputNameDialogExitType GalleryInputNameDialog_Exit;

	// Token: 0x04000651 RID: 1617
	public static SaveLoadType SaveLoadType;

	// Token: 0x04000652 RID: 1618
	public static SaveLoadCallType SaveLoadCallType;

	// Token: 0x04000653 RID: 1619
	public static int SaveDataIndex;

	// Token: 0x04000654 RID: 1620
	public static int VolumeBGM;

	// Token: 0x04000655 RID: 1621
	public static int VolumeSE;

	// Token: 0x04000656 RID: 1622
	public static int VolumeSystem;

	// Token: 0x04000657 RID: 1623
	public static PaymentCallType Payment_Call;

	// Token: 0x04000658 RID: 1624
	public static PaymentExitType Payment_Exit;

	// Token: 0x04000659 RID: 1625
	public static string Payment_ProductId;

	// Token: 0x0400065A RID: 1626
	public static int[] CharaVoiceVolume = new int[11];
}
