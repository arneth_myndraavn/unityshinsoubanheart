﻿using System;
using System.Collections;
using UnityEngine;

// Token: 0x02000008 RID: 8
public class ScreenMovieSample : MonoBehaviour
{
	// Token: 0x06000011 RID: 17 RVA: 0x00002238 File Offset: 0x00000438
	private IEnumerator Start()
	{
		//TODO fix movie playing. either make platform specific versions OR make platform agnostic
		//yield return base.StartCoroutine(MovieManager.PlayMovie("mp4/high/op_movie.mp4", FullScreenMovieControlMode.CancelOnInput));
		yield return base.StartCoroutine(MovieManager.PlayMovie("mp4/high/op_movie.webm", this.gameObject));
		Debug.Log(" #DEBUGLOG::MOVIE OWATA ");
		yield break;
	}

	// Token: 0x06000012 RID: 18 RVA: 0x00002254 File Offset: 0x00000454
	private void Update()
	{
	}
}
