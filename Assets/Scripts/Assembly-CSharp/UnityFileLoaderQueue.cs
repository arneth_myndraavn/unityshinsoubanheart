﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x0200014A RID: 330
public class UnityFileLoaderQueue
{
	// Token: 0x06000920 RID: 2336 RVA: 0x00027A70 File Offset: 0x00025C70
	public bool LoadImage(string name)
	{
		UnityFile unityFile = Singleton<UnityFileLoader>.Instance.LoadImageFile(name);
		if (unityFile != null)
		{
			this.Add(name, unityFile);
			return true;
		}
		return false;
	}

	// Token: 0x06000921 RID: 2337 RVA: 0x00027A9C File Offset: 0x00025C9C
	public bool LoadKsFile(string name)
	{
		UnityFile unityFile = Singleton<UnityFileLoader>.Instance.LoadKsFile(name);
		if (unityFile != null)
		{
			this.Add(name, unityFile);
			return true;
		}
		return false;
	}

	// Token: 0x06000922 RID: 2338 RVA: 0x00027AC8 File Offset: 0x00025CC8
	public bool LoadOther(string name, string path)
	{
		UnityFile unityFile = Singleton<UnityFileLoader>.Instance.LoadFile(name, path, false);
		if (unityFile != null)
		{
			this.Add(name, unityFile);
			return true;
		}
		return false;
	}

	// Token: 0x06000923 RID: 2339 RVA: 0x00027AF4 File Offset: 0x00025CF4
	public bool LoadResource(string name, string path)
	{
		UnityFile unityFile = Singleton<UnityFileLoader>.Instance.LoadResource(name, path);
		if (unityFile != null)
		{
			this.Add(name, unityFile);
			return true;
		}
		return false;
	}

	// Token: 0x06000924 RID: 2340 RVA: 0x00027B20 File Offset: 0x00025D20
	public void Add(string name, UnityFile file)
	{
		this.m_Dic.Add(name, file);
	}

	// Token: 0x06000925 RID: 2341 RVA: 0x00027B30 File Offset: 0x00025D30
	public bool IsReadEnd(string name)
	{
		return this.m_Dic.ContainsKey(name) && this.m_Dic[name].IsReadEnd;
	}

	// Token: 0x06000926 RID: 2342 RVA: 0x00027B64 File Offset: 0x00025D64
	public bool IsReadEndAll()
	{
		Dictionary<string, UnityFile>.ValueCollection values = this.m_Dic.Values;
		foreach (UnityFile unityFile in values)
		{
			if (!unityFile.IsReadEnd)
			{
				return false;
			}
		}
		return true;
	}

	// Token: 0x06000927 RID: 2343 RVA: 0x00027BE0 File Offset: 0x00025DE0
	public byte[] GetData(string name)
	{
		if (this.m_Dic.ContainsKey(name) && this.IsReadEnd(name))
		{
			return this.m_Dic[name].Data;
		}
		return null;
	}

	// Token: 0x06000928 RID: 2344 RVA: 0x00027C20 File Offset: 0x00025E20
	public UnityEngine.Object GetObject(string name)
	{
		if (this.m_Dic.ContainsKey(name) && this.IsReadEnd(name))
		{
			return this.m_Dic[name].Obj;
		}
		return null;
	}

	// Token: 0x06000929 RID: 2345 RVA: 0x00027C60 File Offset: 0x00025E60
	public bool Remove(string name)
	{
		return this.m_Dic.ContainsKey(name) && this.m_Dic.Remove(name);
	}

	// Token: 0x040007A6 RID: 1958
	private Dictionary<string, UnityFile> m_Dic = new Dictionary<string, UnityFile>();
}
