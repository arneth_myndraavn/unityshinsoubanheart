﻿using System;
using System.Collections;
using Qoo;
using UnityEngine;
using UnityEngine.Networking;

// Token: 0x02000148 RID: 328
public class UnityFile
{
	// Token: 0x1700010E RID: 270
	// (get) Token: 0x06000901 RID: 2305 RVA: 0x0002768C File Offset: 0x0002588C
	public bool IsReadEnd
	{
		get
		{
			return this.m_isReadEnd;
		}
	}

	// Token: 0x1700010F RID: 271
	// (get) Token: 0x06000902 RID: 2306 RVA: 0x00027694 File Offset: 0x00025894
	public string Name
	{
		get
		{
			return this.m_szName;
		}
	}

	// Token: 0x17000110 RID: 272
	// (get) Token: 0x06000903 RID: 2307 RVA: 0x0002769C File Offset: 0x0002589C
	public string Path
	{
		get
		{
			return this.m_szPath;
		}
	}

	// Token: 0x17000111 RID: 273
	// (get) Token: 0x06000904 RID: 2308 RVA: 0x000276A4 File Offset: 0x000258A4
	public byte[] Data
	{
		get
		{
			return this.m_ReadData;
		}
	}

	// Token: 0x17000112 RID: 274
	// (get) Token: 0x06000905 RID: 2309 RVA: 0x000276AC File Offset: 0x000258AC
	public AssetBundle Asset
	{
		get
		{
			return this.m_asset;
		}
	}

	// Token: 0x17000113 RID: 275
	// (get) Token: 0x06000906 RID: 2310 RVA: 0x000276B4 File Offset: 0x000258B4
	public UnityEngine.Object Obj
	{
		get
		{
			return this.m_object;
		}
	}

	// Token: 0x17000114 RID: 276
	// (get) Token: 0x06000907 RID: 2311 RVA: 0x000276BC File Offset: 0x000258BC
	public bool IsError
	{
		get
		{
			return this.m_IsError;
		}
	}

	// Token: 0x06000908 RID: 2312 RVA: 0x000276C4 File Offset: 0x000258C4
	public bool Create(WWW loader, string name, string path, bool IsAssetBandle)
	{
		this.m_szName = name;
		this.m_szPath = path;
		this.m_IsAssetBandle = IsAssetBandle;
		UnityTask.SetReadTask(this.LoadWait());

		return true;
	}

	// Token: 0x06000909 RID: 2313 RVA: 0x000276F4 File Offset: 0x000258F4
	public bool CreateFromAssetBundle(AssetBundle asset, string name)
	{
		this.m_szName = name;
		this.m_ReadData = (asset.LoadAsset(name) as TextAsset).bytes;
		this.m_isReadEnd = true;
		return true;
	}

	// Token: 0x0600090A RID: 2314 RVA: 0x00027728 File Offset: 0x00025928
	public bool LoadResource(string name_, string path_)
	{
		this.m_szName = name_;
		this.m_szPath = path_;
		this.m_object = Resources.Load(path_ + name_);
		if (this.m_object != null)
		{
			this.m_isReadEnd = true;
		}
		return this.m_isReadEnd;
	}

	// Token: 0x0600090B RID: 2315 RVA: 0x00027774 File Offset: 0x00025974
	public bool CreateAssetBandle(WWW loader, string name, string path)
	{
		this.m_szName = name;
		this.m_szPath = path;
		this.m_isLoadAssetBandle = true;
		this.m_IsAssetBandle = true;
		UnityTask.SetReadTask(this.LoadWait());
		return true;
	}

	// Token: 0x0600090C RID: 2316 RVA: 0x000277AC File Offset: 0x000259AC
	private string CreateLoader(string name, string localpath, bool IsAssetBandle)
	{
		localpath = localpath.Replace('\\', '/');
		name = name.Replace('\\', '/');
		if (IsAssetBandle)
		{
			name = Pathing.ToPlatformAssetBundleName(name);
		}
		string text = Pathing.appContentDataPath + localpath + name;
        Qoo.Debug.Print("first possible spot:");
		return text;
	}

	// Token: 0x0600090D RID: 2317 RVA: 0x00027800 File Offset: 0x00025A00
	private IEnumerator LoadWait()
	{
		if (this.m_isLoadPackFile)
		{
			AssetBundleCreateRequest req = AssetBundle.LoadFromMemoryAsync(this.m_ReadData);
			yield return req;
			this.m_object = req.assetBundle.mainAsset;
			req.assetBundle.Unload(false);
			req = null;
			this.m_isReadEnd = true;
			Resources.UnloadUnusedAssets();
			yield return new WaitForEndOfFrame();
		}
		else
		{
			WWW loader = null;
			string path = this.CreateLoader(this.m_szName, this.m_szPath, this.m_IsAssetBandle);
			for (int i = 0; i <= 10; i++)
			{
				loader = new WWW(path);
				yield return loader;
				/*this.m_szErrorMsg = loader.error;
				if (this.m_szErrorMsg != null && this.m_szErrorMsg.Length > 0)
				{
					Qoo.Debug.Print("Error:" + this.m_szErrorMsg);
					if (i == 10)
					{
						this.m_IsError = true;
						break;
					}
					if (this.m_isLoadAssetBandle)
					{
						this.m_IsError = true;
						break;
					}
					loader = null;
				}
				else if (loader.isDone)
				{
					break;
				}*/
				break;
				yield return 0;
			}
			if (loader.isDone)
			{
				if (this.m_isLoadAssetBandle)
				{
					this.m_asset = loader.assetBundle;
				}
				else if (this.m_IsAssetBandle)
				{
					if (Application.platform == RuntimePlatform.Android)
					{
                        string url;
                        if (Application.platform == RuntimePlatform.Android)
                        {
                            // On Android, use "jar:file://" URL to load file from StreamingAssets
                            url = ("jar:file://" + Application.dataPath + "!/assets" + m_szPath + m_szName + ".png").TrimStart('/');
                            UnityEngine.Debug.LogWarning(url);
                        }
                        else
                        {
                            // On other platforms, use file:// URL to load file from StreamingAssets
                            url = Application.streamingAssetsPath + m_szPath + m_szName + ".png";
                        }

                        //UnityEngine.Debug.LogWarning(url);
                        UnityWebRequest request = UnityWebRequest.Get(url);
                        yield return request.SendWebRequest();

                        if (request.result == UnityWebRequest.Result.Success)
                        {
                            byte[] data = request.downloadHandler.data;
                            this.m_ReadData = data;
                            // Use the loaded image data here
                        }
                        else
                        {
                            UnityEngine.Debug.LogError("Failed to load image: " + request.error);
                        }

                    }
					else
					{
                        UnityEngine.Debug.Log(Application.streamingAssetsPath + m_szPath + m_szName + ".png");
                        byte[] data = System.IO.File.ReadAllBytes(Application.streamingAssetsPath + m_szPath + m_szName + ".png");
                        this.m_ReadData = data;
                    }

                    

                    /*UnityEngine.Debug.Log (Application.streamingAssetsPath + m_szPath + m_szName + ".png");
					byte[] data = System.IO.File.ReadAllBytes(Application.streamingAssetsPath + m_szPath + m_szName + ".png");
					this.m_ReadData = data;*/
                }
				else
				{
					this.m_ReadData = loader.bytes;
				}
				loader.Dispose();
				this.m_isReadEnd = true;
			}
			else
			{
				UnityEngine.Debug.LogError("Error:FileRead Failed" + this.m_szName);
				this.m_IsError = true;
			}
			loader = null;
		}
		yield break;
	}

	// Token: 0x04000799 RID: 1945
	private const int RETRY_COUNT = 10;

	// Token: 0x0400079A RID: 1946
	private bool m_isReadEnd;

	// Token: 0x0400079B RID: 1947
	private string m_szName = string.Empty;

	// Token: 0x0400079C RID: 1948
	private string m_szPath = string.Empty;

	// Token: 0x0400079D RID: 1949
	private bool m_IsAssetBandle;

	// Token: 0x0400079E RID: 1950
	private bool m_IsError;

	// Token: 0x0400079F RID: 1951
	private string m_szErrorMsg = string.Empty;

	// Token: 0x040007A0 RID: 1952
	private byte[] m_ReadData;

	// Token: 0x040007A1 RID: 1953
	private AssetBundle m_asset;

	// Token: 0x040007A2 RID: 1954
	private UnityEngine.Object m_object;

	// Token: 0x040007A3 RID: 1955
	private bool m_isLoadAssetBandle;

	// Token: 0x040007A4 RID: 1956
	private bool m_isLoadPackFile;
}
