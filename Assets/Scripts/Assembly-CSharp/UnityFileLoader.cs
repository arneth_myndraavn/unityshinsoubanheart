﻿using System;
using Qoo;
using Qoo.File;
using UnityEngine;

// Token: 0x02000149 RID: 329
public class UnityFileLoader : Singleton<UnityFileLoader>
{
	// Token: 0x0600090F RID: 2319 RVA: 0x00027824 File Offset: 0x00025A24
	private void Awake()
	{
	}

	// Token: 0x06000910 RID: 2320 RVA: 0x00027828 File Offset: 0x00025A28
	private void Update()
	{
	}

	// Token: 0x06000911 RID: 2321 RVA: 0x0002782C File Offset: 0x00025A2C
	public UnityFile LoadImageFile(string name)
	{
		name = this.NormalizeImageName(name);
		string imageDir = this.GetImageDir(name);
		return this.LoadFile(name, imageDir, true);
	}

	// Token: 0x06000912 RID: 2322 RVA: 0x00027854 File Offset: 0x00025A54
	public UnityFile LoadSoundFile(string name)
	{
		name = this.NormalizeImageName(name);
		return this.LoadFile(name, string.Empty, true);
	}

	// Token: 0x06000913 RID: 2323 RVA: 0x0002786C File Offset: 0x00025A6C
	public UnityFile LoadKsFile(string name)
	{
		name = name.ToLower();
		return this.LoadFile(name, "/ks/", false);
	}

	// Token: 0x06000914 RID: 2324 RVA: 0x00027884 File Offset: 0x00025A84
	public UnityFile LoadFile(string name, string localpath, bool IsAssetBandle = false)
	{
		UnityEngine.Debug.LogWarning("in Load File");
		if (this.IsLoadedAssetBundle())
		{
			string name2 = this.NormalizeImageName(name);
			if (this.m_Asset.Asset.Contains(name2))
			{
				return this.GetAssetBandleFile(name);
			}
		}
		UnityFile unityFile = new UnityFile();
		unityFile.Create(null, name, localpath, IsAssetBandle);
		return unityFile;
	}

	// Token: 0x06000915 RID: 2325 RVA: 0x000278D4 File Offset: 0x00025AD4
	public UnityFile LoadResource(string name, string path)
	{
		UnityFile unityFile = new UnityFile();
		unityFile.LoadResource(name, path);
		return unityFile;
	}

	// Token: 0x06000916 RID: 2326 RVA: 0x000278F4 File Offset: 0x00025AF4
	public static WWW CreateLoader(string name, string localpath, bool IsAssetBandle)
	{
		localpath = localpath.Replace('\\', '/');
		name = name.Replace('\\', '/');
		if (IsAssetBandle)
		{
			name = Pathing.ToPlatformAssetBundleName(name);
		}
		string text = Pathing.appContentDataPath + localpath + name;
        Qoo.Debug.Print("FileLoad:" + text);
		return new WWW(text);
	}

	// Token: 0x06000917 RID: 2327 RVA: 0x0002794C File Offset: 0x00025B4C
	private string NormalizeImageName(string name)
	{
		name = name.ToLower();
		int num = name.LastIndexOf('.');
		if (num != -1)
		{
			name = name.Substring(0, num);
		}
		return name;
	}

	// Token: 0x06000918 RID: 2328 RVA: 0x0002797C File Offset: 0x00025B7C
	public string GetImageDir(string name)
	{
		name = this.NormalizeImageName(name);
		NMB_FILEINFO fileInfo = Nmb.GetFileInfo(name);
		return '/' + fileInfo.DirName;
	}

	// Token: 0x06000919 RID: 2329 RVA: 0x000279AC File Offset: 0x00025BAC
	public int GetPackNo(string name)
	{
		return Nmb.GetPackNo(name);
	}

	// Token: 0x0600091A RID: 2330 RVA: 0x000279B4 File Offset: 0x00025BB4
	public int NameToNo(string name)
	{
		return Nmb.NameToNo(name);
	}

	// Token: 0x0600091B RID: 2331 RVA: 0x000279BC File Offset: 0x00025BBC
	public void LoadAssetBundle(string name)
	{
		this.UnloadAssetBundle();
		this.m_Asset = new UnityFile();
		this.m_Asset.CreateAssetBandle(null, name, string.Empty);
	}

	// Token: 0x0600091C RID: 2332 RVA: 0x000279F0 File Offset: 0x00025BF0
	public bool IsLoadedAssetBundle()
	{
		return this.m_Asset != null && this.m_Asset.IsReadEnd;
	}

	// Token: 0x0600091D RID: 2333 RVA: 0x00027A0C File Offset: 0x00025C0C
	public void UnloadAssetBundle()
	{
		if (this.m_Asset != null)
		{
			this.m_Asset.Asset.Unload(true);
		}
		this.m_Asset = null;
	}

	// Token: 0x0600091E RID: 2334 RVA: 0x00027A34 File Offset: 0x00025C34
	private UnityFile GetAssetBandleFile(string name)
	{
		UnityFile unityFile = new UnityFile();
		unityFile.CreateFromAssetBundle(this.m_Asset.Asset, name);
		return unityFile;
	}

	// Token: 0x040007A5 RID: 1957
	private UnityFile m_Asset;
}
