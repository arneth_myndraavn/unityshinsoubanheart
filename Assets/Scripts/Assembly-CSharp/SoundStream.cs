﻿using System;
using UnityEngine;

// Token: 0x0200018B RID: 395
public class SoundStream : MonoBehaviour
{
	// Token: 0x17000179 RID: 377
	// (get) Token: 0x06000B3E RID: 2878 RVA: 0x0003055C File Offset: 0x0002E75C
	public SoundVoice Current
	{
		get
		{
			return this.voice[this.current];
		}
	}

	// Token: 0x1700017A RID: 378
	// (get) Token: 0x06000B3F RID: 2879 RVA: 0x0003056C File Offset: 0x0002E76C
	public bool IsPlay
	{
		get
		{
			return this.Current.IsPlay;
		}
	}

	// Token: 0x06000B40 RID: 2880 RVA: 0x0003057C File Offset: 0x0002E77C
	public static SoundStream CreateObject(GameObject parent, string name)
	{
		return new GameObject("SoundStream." + name)
		{
			transform = 
			{
				parent = parent.transform
			}
		}.gameObject.AddComponent<SoundStream>();
	}

	// Token: 0x06000B41 RID: 2881 RVA: 0x000305B8 File Offset: 0x0002E7B8
	private void Awake()
	{
		for (int i = 0; i < this.voice.Length; i++)
		{
			this.voice[i] = base.gameObject.AddComponent<SoundVoice>();
		}
	}

	// Token: 0x06000B42 RID: 2882 RVA: 0x000305F4 File Offset: 0x0002E7F4
	private void Update()
	{
	}

	// Token: 0x06000B43 RID: 2883 RVA: 0x000305F8 File Offset: 0x0002E7F8
	public void Play(string category, string name_, bool isLoop = true, float volume = 1f, int timeIn = 1000, int timeOut = 1000)
	{
		bool flag = true;
		if (category == "bgm" || (category == "se" && isLoop))
		{
			flag = (!this.Current.IsPlay || this.Current.Name != name_);
		}
		if (flag)
		{
			this.Stop(timeOut);
			this.Change();
			this.Current.SetSource(category, name_);
			this.Current.FadeInTime = timeIn;
			this.Current.Loop = isLoop;
			this.Current.Volume = volume;
			this.Current.Play();
		}
		else
		{
			this.Current.FadeInTime = timeIn;
			this.Current.Loop = isLoop;
			this.Current.Volume = volume;
		}
	}

	// Token: 0x06000B44 RID: 2884 RVA: 0x000306D0 File Offset: 0x0002E8D0
	public void Stop(int timeOut = 1000)
	{
		if (this.Current.IsPlay)
		{
			this.Current.FadeOutTime = timeOut;
			this.Current.Stop();
		}
	}

	// Token: 0x06000B45 RID: 2885 RVA: 0x00030704 File Offset: 0x0002E904
	public void Pause(bool bPause)
	{
		this.Current.Pause(bPause);
	}

	// Token: 0x06000B46 RID: 2886 RVA: 0x00030714 File Offset: 0x0002E914
	private void Change()
	{
		this.current++;
		if (this.current >= this.voice.Length)
		{
			this.current = 0;
		}
	}

	// Token: 0x040008E9 RID: 2281
	private const int SOUND_STREAM_NUM = 2;

	// Token: 0x040008EA RID: 2282
	private SoundVoice[] voice = new SoundVoice[2];

	// Token: 0x040008EB RID: 2283
	private int current;
}
