﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x020000F5 RID: 245
public class VoiceOptionList : MonoBehaviour
{
	// Token: 0x170000DA RID: 218
	// (get) Token: 0x06000691 RID: 1681 RVA: 0x0001B304 File Offset: 0x00019504
	public List<GameObject> ListGameObjects
	{
		get
		{
			return this.UIGameObjects;
		}
	}

	// Token: 0x06000692 RID: 1682 RVA: 0x0001B30C File Offset: 0x0001950C
	public static VoiceOptionList Create(int x, int y, int z, int width, int height, int itemCount, Transform parent)
	{
		GameObject gameObject = new GameObject("VoiceOptionList");
		VoiceOptionList voiceOptionList = gameObject.AddComponent<VoiceOptionList>();
		voiceOptionList.Init(x, y, z, width, height, itemCount, parent);
		int num = -480;
		int num2 = 272;
		voiceOptionList.m_ColliderScript = new GameObject("VoiceOptionCollider")
		{
			transform = 
			{
				parent = parent,
				localPosition = new Vector3((float)(num + width / 2 + x), (float)(num2 - height / 2 - y), -(parent.localPosition.z + (float)z + 2f)),
				localScale = new Vector3((float)width, (float)height, 1f)
			}
		}.AddComponent<VoiceOptionCollider>();
		voiceOptionList.m_ColliderScript.SetInfo(gameObject, 600);
		return voiceOptionList;
	}

	// Token: 0x06000693 RID: 1683 RVA: 0x0001B3D8 File Offset: 0x000195D8
	public void AddListItem(ImageObject itemObject)
	{
		itemObject.transform.parent = base.transform;
		ListItemObject listItemObject = ListItemObject.Create(itemObject);
		listItemObject.SetArea((float)this.ListTop, (float)this.ListBottom);
	}

	// Token: 0x06000694 RID: 1684 RVA: 0x0001B414 File Offset: 0x00019614
	public void SetScrollbar(GameObject area, GameObject slider)
	{
		VoiceOptionScrollbar voiceOptionScrollbar = VoiceOptionScrollbar.Create(600, area.GetComponent<ImageObject>(), slider.GetComponent<ImageObject>(), base.transform);
		voiceOptionScrollbar.SetSlideObject(base.gameObject);
		this.m_ColliderScript.AddScrollBar(voiceOptionScrollbar);
	}

	// Token: 0x06000695 RID: 1685 RVA: 0x0001B458 File Offset: 0x00019658
	public void Init(int x, int y, int z, int width, int height, int itemCount, Transform parent)
	{
		this.ListTop = y;
		this.ListBottom = y + height;
		base.transform.parent = parent;
		base.transform.localPosition = new Vector3(0f, 0f, (float)(-(float)z));
	}

	// Token: 0x040005F0 RID: 1520
	private const int VOICE_CHIP_HEIGHT = 66;

	// Token: 0x040005F1 RID: 1521
	private const int VOICE_BASE_X = 250;

	// Token: 0x040005F2 RID: 1522
	private const int VOICE_BASE_Y = 155;

	// Token: 0x040005F3 RID: 1523
	private const int VOICE_DISTANCE = 75;

	// Token: 0x040005F4 RID: 1524
	private const int FRAME_BASE_X = 434;

	// Token: 0x040005F5 RID: 1525
	private const int FRAME_BASE_Y = 185;

	// Token: 0x040005F6 RID: 1526
	private const int FRAME_DISTANCE = 75;

	// Token: 0x040005F7 RID: 1527
	private const int METER_BASE_X = 436;

	// Token: 0x040005F8 RID: 1528
	private const int METER_BASE_Y = 187;

	// Token: 0x040005F9 RID: 1529
	private const int METER_DISTANCE = 75;

	// Token: 0x040005FA RID: 1530
	private int ListTop;

	// Token: 0x040005FB RID: 1531
	private int ListBottom;

	// Token: 0x040005FC RID: 1532
	private VoiceOptionCollider m_ColliderScript;

	// Token: 0x040005FD RID: 1533
	private List<GameObject> UIGameObjects = new List<GameObject>();

	// Token: 0x040005FE RID: 1534
	private List<OptionVoiceMeter> m_MeterList;
}
