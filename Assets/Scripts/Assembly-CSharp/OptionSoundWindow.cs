﻿using System;
using System.Collections.Generic;
using Qoo.Game;
using UnityEngine;

// Token: 0x02000120 RID: 288
public class OptionSoundWindow : OptionBaseWindow
{
	// Token: 0x060007B6 RID: 1974 RVA: 0x00020F34 File Offset: 0x0001F134
	private void MeterUpdate()
	{
		GameObject gameObject = base.GetGameObject("OptionSoundItems");
		OptionSoundItems component = gameObject.GetComponent<OptionSoundItems>();
		component.ResetMeter();
	}

	// Token: 0x060007B7 RID: 1975 RVA: 0x00020F5C File Offset: 0x0001F15C
	protected override OptionBaseWindow.OptionType GetOptionType()
	{
		return OptionBaseWindow.OptionType.Sound;
	}

	// Token: 0x060007B8 RID: 1976 RVA: 0x00020F60 File Offset: 0x0001F160
	protected sealed override string[] newSceneTextureNameArray()
	{
		string[] collection = base.newSceneTextureNameArray();
		string[] collection2 = new string[]
		{
			"screen/sysmenu/sys_plate",
			"screen/sysmenu/sys_btn_10",
			"screen/sysmenu/sys_btn_11",
			"screen/screen/sysmenu/sys_volume_bgm",
			"screen/screen/sysmenu/sys_volume_se",
			"screen/screen/sysmenu/sys_volume_system",
			"screen/screen/sysmenu/sys_volume_bar1",
			"screen/screen/sysmenu/sys_volume_meter1",
			"screen/common/cancel"
		};
		List<string> list = new List<string>();
		list.AddRange(collection);
		list.AddRange(collection2);
		return list.ToArray();
	}

	// Token: 0x060007B9 RID: 1977 RVA: 0x00020FE0 File Offset: 0x0001F1E0
	protected sealed override void BeforeInit()
	{
		Vector3 localPosition = base.transform.parent.localPosition;
		localPosition.z = -50f;
		base.transform.parent.localPosition = localPosition;
		UIValue.VolumeBGM = SysData.GetVolumeBgm();
		UIValue.VolumeSE = SysData.GetVolumeSe();
		UIValue.VolumeSystem = SysData.GetVolumeSys();
	}

	// Token: 0x060007BA RID: 1978 RVA: 0x0002103C File Offset: 0x0001F23C
	protected sealed override void AfterInit()
	{
		OptionSoundItems optionSoundItems = OptionSoundItems.Create(base.transform);
		optionSoundItems.AddItem(SoundOptionType.BGM, base.GetGameObject("BGM_Label"), base.GetGameObject("BGM_Meter"), base.GetGameObject("BGM_Bar"));
		optionSoundItems.AddItem(SoundOptionType.SE, base.GetGameObject("SE_Label"), base.GetGameObject("SE_Meter"), base.GetGameObject("SE_Bar"));
		optionSoundItems.AddItem(SoundOptionType.SYSTEM, base.GetGameObject("SYS_Label"), base.GetGameObject("SYS_Meter"), base.GetGameObject("SYS_Bar"));
		optionSoundItems.Init();
		base.AddGameObject("OptionSoundItems", optionSoundItems.gameObject);
	}

	// Token: 0x060007BB RID: 1979 RVA: 0x000210E4 File Offset: 0x0001F2E4
	protected override void OnGraphicLoadComplete()
	{
		base.OnGraphicLoadComplete();
	}

	// Token: 0x060007BC RID: 1980 RVA: 0x000210EC File Offset: 0x0001F2EC
	protected sealed override BaseWindow.UIComponent[] newComponentArray()
	{
		BaseWindow.UIComponent[] collection = base.newComponentArray();
		BaseWindow.UIComponent[] collection2 = new BaseWindow.UIComponent[]
		{
			new BaseWindow.UICollision("Close", 0, 0, this.wndz, 960, 544),
			new BaseWindow.UIImage("BackGround", 153, 68, this.wndz + 1, "screen/sysmenu/sys_plate", true, true),
			new BaseWindow.UIButton("Clear", 208, 380, this.wndz + 2, "screen/sysmenu/sys_btn_10", true, true, 1, 2, 0),
			new BaseWindow.UIButton("Decide", 686, 380, this.wndz + 2, "screen/sysmenu/sys_btn_11", true, true, 1, 2, 0),
			new BaseWindow.UIButton("Cancel", 715, 73, this.wndz + 2, "screen/common/cancel", true, true, 1, 2, 0),
			new BaseWindow.UIImage("BGM_Label", 222, 141, this.wndz + 5, "sys_volume_bgm", false, true),
			new BaseWindow.UIImage("SE_Label", 222, 213, this.wndz + 5, "sys_volume_se", false, true),
			new BaseWindow.UIImage("SYS_Label", 222, 285, this.wndz + 5, "sys_volume_system", false, true),
			new BaseWindow.UIImage("BGM_Meter", 354, 146, this.wndz + 6, "sys_volume_meter1", false, true),
			new BaseWindow.UIImage("SE_Meter", 354, 219, this.wndz + 6, "sys_volume_meter1", false, true),
			new BaseWindow.UIImage("SYS_Meter", 354, 292, this.wndz + 6, "sys_volume_meter1", false, true),
			new BaseWindow.UIImage("BGM_Bar", 354, 146, this.wndz + 7, "sys_volume_bar1", false, true),
			new BaseWindow.UIImage("SE_Bar", 354, 219, this.wndz + 7, "sys_volume_bar1", false, true),
			new BaseWindow.UIImage("SYS_Bar", 354, 292, this.wndz + 7, "sys_volume_bar1", false, true)
		};
		List<BaseWindow.UIComponent> list = new List<BaseWindow.UIComponent>();
		list.AddRange(collection);
		list.AddRange(collection2);
		return list.ToArray();
	}

	// Token: 0x060007BD RID: 1981 RVA: 0x00021338 File Offset: 0x0001F538
	protected sealed override void OnBaseWindowOnButton(string obj)
	{
		base.OnBaseWindowOnButton(obj);
		this.MeterUpdate();
	}

	// Token: 0x040006CE RID: 1742
	private readonly int wndz;
}
