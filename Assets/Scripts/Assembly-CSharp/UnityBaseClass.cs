﻿using System;
using UnityEngine;

// Token: 0x020001AC RID: 428
public class UnityBaseClass : MonoBehaviour
{
	// Token: 0x06000C5D RID: 3165 RVA: 0x00032D2C File Offset: 0x00030F2C
	private void Begin()
	{
	}

	// Token: 0x06000C5E RID: 3166 RVA: 0x00032D30 File Offset: 0x00030F30
	private void Update()
	{
	}
}
