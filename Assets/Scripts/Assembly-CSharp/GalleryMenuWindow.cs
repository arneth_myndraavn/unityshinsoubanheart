﻿using System;

// Token: 0x0200010E RID: 270
public class GalleryMenuWindow : BaseWindow
{
	// Token: 0x06000719 RID: 1817 RVA: 0x0001E56C File Offset: 0x0001C76C
	protected sealed override string GetBGMName()
	{
		return "sysbgm_cgmemory";
	}

	// Token: 0x0600071A RID: 1818 RVA: 0x0001E574 File Offset: 0x0001C774
	protected sealed override void OnAwake()
	{
		CSVManager.Instance.UpdateGallery();
	}

	// Token: 0x0600071B RID: 1819 RVA: 0x0001E580 File Offset: 0x0001C780
	protected sealed override string[] newSceneTextureNameArray()
	{
		return new string[]
		{
			"screen/title/title_base",
			"screen/title/memory02",
			"screen/title/memory01",
			"screen/title/memory03",
			"screen/common/cancel"
		};
	}

	// Token: 0x0600071C RID: 1820 RVA: 0x0001E5C0 File Offset: 0x0001C7C0
	protected sealed override void AfterInit()
	{
		if (base.IsTrial)
		{
			base.GetGameObject("Btn0").GetComponent<UIActionButton>().ActionEnable = false;
			base.GetGameObject("Btn2").GetComponent<UIActionButton>().ActionEnable = false;
		}
	}

	// Token: 0x0600071D RID: 1821 RVA: 0x0001E604 File Offset: 0x0001C804
	protected sealed override BaseWindow.UIComponent[] newComponentArray()
	{
		return new BaseWindow.UIComponent[]
		{
			new BaseWindow.UIImage("BackGround", 0, 0, this.wndz, "screen/title/title_base", false, true),
			new BaseWindow.UIButton("Btn0", 293, 280, this.wndz + 1, "screen/title/memory02", true, true, 1, 2, 0),
			new BaseWindow.UIButton("Btn1", 534, 280, this.wndz + 1, "screen/title/memory01", true, true, 1, 2, 0),
			new BaseWindow.UIButton("Btn2", 393, 402, this.wndz + 1, "screen/title/memory03", true, true, 1, 2, 0),
			new BaseWindow.UIButton("Cancel", 887, 6, this.wndz + 3, "screen/common/cancel", true, true, 1, 2, 0)
		};
	}

	// Token: 0x0600071E RID: 1822 RVA: 0x0001E6D8 File Offset: 0x0001C8D8
	protected sealed override void OnBaseWindowOnButton(string obj)
	{
		switch (obj)
		{
		case "Btn0":
			if (!base.IsTrial)
			{
				base.PlaySE_Ok();
				SceneManager.ChangeScene(UIValue.SCENE_MEMORYCHARASELECT);
			}
			break;
		case "Btn1":
			base.PlaySE_Ok();
			SceneManager.ChangeScene(UIValue.SCENE_CGCHARASELECT);
			break;
		case "Btn2":
			if (!base.IsTrial)
			{
				base.PlaySE_Ok();
				SceneManager.ChangeScene(UIValue.SCENE_GALLERYMOVIE);
			}
			break;
		case "Cancel":
			base.PlaySE_Cancel();
			SceneManager.BackScene();
			break;
		}
	}

	// Token: 0x04000695 RID: 1685
	private readonly int wndz;
}
