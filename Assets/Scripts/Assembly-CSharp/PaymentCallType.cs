﻿using System;

// Token: 0x020000FC RID: 252
public enum PaymentCallType
{
	// Token: 0x04000621 RID: 1569
	INVALID,
	// Token: 0x04000622 RID: 1570
	TITLE,
	// Token: 0x04000623 RID: 1571
	ADV
}
