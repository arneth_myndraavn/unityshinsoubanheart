﻿using System;

// Token: 0x02000168 RID: 360
public class CSVManager
{
	// Token: 0x06000A59 RID: 2649 RVA: 0x0002DDC4 File Offset: 0x0002BFC4
	private CSVManager()
	{
		this.csvCGListHolder = new CSVCGListHolder();
		this.csvSceneMemoryHolder = new CSVSceneMemoryHolder();
	}

	// Token: 0x1700014D RID: 333
	// (get) Token: 0x06000A5A RID: 2650 RVA: 0x0002DDE4 File Offset: 0x0002BFE4
	public static CSVManager Instance
	{
		get
		{
			return CSVManager.instance;
		}
	}

	// Token: 0x06000A5B RID: 2651 RVA: 0x0002DDEC File Offset: 0x0002BFEC
	public static void init()
	{
		if (CSVManager.instance == null)
		{
			CSVManager.instance = new CSVManager();
		}
	}

	// Token: 0x06000A5C RID: 2652 RVA: 0x0002DE04 File Offset: 0x0002C004
	public static int calcCharaIndex(string charaid_)
	{
		switch (charaid_)
		{
		case "blood":
			return 0;
		case "elliot":
			return 1;
		case "deedum":
			return 2;
		case "vivaldi":
			return 3;
		case "peter":
			return 4;
		case "ace":
			return 5;
		case "gowland":
			return 6;
		case "boris":
			return 7;
		case "julius":
			return 8;
		case "nightmare":
			return 9;
		case "other":
			return 10;
		}
		return -1;
	}

	// Token: 0x1700014E RID: 334
	// (get) Token: 0x06000A5D RID: 2653 RVA: 0x0002DF1C File Offset: 0x0002C11C
	public CSVCGListHolder CsvCGListHolder
	{
		get
		{
			return this.csvCGListHolder;
		}
	}

	// Token: 0x1700014F RID: 335
	// (get) Token: 0x06000A5E RID: 2654 RVA: 0x0002DF24 File Offset: 0x0002C124
	public CSVSceneMemoryHolder CsvSceneMemoryHolder
	{
		get
		{
			return this.csvSceneMemoryHolder;
		}
	}

	// Token: 0x06000A5F RID: 2655 RVA: 0x0002DF2C File Offset: 0x0002C12C
	public void UpdateGallery()
	{
		this.csvCGListHolder.UpdateGallery();
		this.csvSceneMemoryHolder.UpdateGallery();
	}

	// Token: 0x04000860 RID: 2144
	private static CSVManager instance;

	// Token: 0x04000861 RID: 2145
	private CSVCGListHolder csvCGListHolder;

	// Token: 0x04000862 RID: 2146
	private CSVSceneMemoryHolder csvSceneMemoryHolder;
}
