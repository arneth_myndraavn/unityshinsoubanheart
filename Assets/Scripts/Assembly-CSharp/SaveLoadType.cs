﻿using System;

// Token: 0x020000FD RID: 253
public enum SaveLoadType
{
	// Token: 0x04000625 RID: 1573
	INVALID,
	// Token: 0x04000626 RID: 1574
	SAVE,
	// Token: 0x04000627 RID: 1575
	LOAD
}
