﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x02000111 RID: 273
public abstract class GallerySelectWindow : BaseWindow
{
	private Dictionary<string,int> dict = null;

	// Token: 0x06000725 RID: 1829
	protected abstract void BeforeInitGallerySelect();

	// Token: 0x06000726 RID: 1830
	protected abstract int GetCollect();

	// Token: 0x06000727 RID: 1831
	protected abstract string GetThumbnailName(int index);

	// Token: 0x06000728 RID: 1832
	protected abstract int GetThumbnailLength();

	// Token: 0x06000729 RID: 1833
	protected abstract string GetOnArrowSceneName();

	// Token: 0x0600072A RID: 1834
	protected abstract bool IsFont();

	// Token: 0x0600072B RID: 1835
	protected abstract string GetFontText(int index);

	// Token: 0x0600072C RID: 1836
	protected abstract void OnSelect(int index);

	// Token: 0x0600072D RID: 1837
	protected abstract bool IsSelectable(int index);

	// Token: 0x170000DC RID: 220
	// (get) Token: 0x0600072E RID: 1838 RVA: 0x0001E880 File Offset: 0x0001CA80
	// (set) Token: 0x0600072F RID: 1839 RVA: 0x0001E888 File Offset: 0x0001CA88
	protected int Chara
	{
		get
		{
			return this.m_chara;
		}
		set
		{
			this.m_chara = value;
		}
	}

	// Token: 0x170000DD RID: 221
	// (get) Token: 0x06000730 RID: 1840 RVA: 0x0001E894 File Offset: 0x0001CA94
	// (set) Token: 0x06000731 RID: 1841 RVA: 0x0001E89C File Offset: 0x0001CA9C
	protected int Page
	{
		get
		{
			return this.m_page;
		}
		set
		{
			this.m_page = value;
		}
	}

	// Token: 0x06000732 RID: 1842 RVA: 0x0001E8A8 File Offset: 0x0001CAA8
	private int pageCountLoop(int page)
	{
		int num = (this.GetThumbnailLength() + this.MAX_THUMBNAILS_COUNT - 1) / this.MAX_THUMBNAILS_COUNT;
		return (page + num) % num;
	}

	// Token: 0x06000733 RID: 1843 RVA: 0x0001E8D4 File Offset: 0x0001CAD4
	protected sealed override string GetBGMName()
	{
		return "sysbgm_cgmemory";
	}

	// Token: 0x06000734 RID: 1844 RVA: 0x0001E8DC File Offset: 0x0001CADC
	protected sealed override void OnAwake()
	{
		this.m_chara = UIValue.GalleryCharactor;
		this.m_page = UIValue.GalleryPage;
		this.BeforeInitGallerySelect();
		if (this.GetThumbnailLength() <= this.m_page * this.MAX_THUMBNAILS_COUNT)
		{
			this.m_page = 0;
		}
		this.m_collect = this.GetCollect();
		this.loading = false;
		this.m_dir = 0;
	}

	// Token: 0x06000735 RID: 1845 RVA: 0x0001E940 File Offset: 0x0001CB40
	protected sealed override string[] newSceneTextureNameArray()
	{
		this.m_chara = UIValue.GalleryCharactor;
		int num = this.m_page * this.MAX_THUMBNAILS_COUNT;
		return new string[]
		{
			"screen/cgmemory2/cgm_bg_1",
			this.GetFilenameId(this.m_chara).ToString("screen/cgmemory/cgm_bnr_cha00"),
			"screen/cgmemory/cgm_kumi0",
			"screen/cgmemory/cgm_kumi1",
			"screen/cgmemory/cgm_kumi2",
			"screen/cgmemory/cgm_kumi3",
			"screen/cgmemory/cgm_kumi4",
			"screen/cgmemory/cgm_kumi5",
			"screen/cgmemory/cgm_kumi6",
			"screen/cgmemory/cgm_kumi7",
			"screen/cgmemory/cgm_kumi8",
			"screen/cgmemory/cgm_kumi9",
			"screen/common/cancel",
			"screen/cgmemory/cgm_arwl",
			"screen/cgmemory/cgm_arwr",
			this.GetThumbnailName(num),
			this.GetThumbnailName(num + 1),
			this.GetThumbnailName(num + 2),
			this.GetThumbnailName(num + 3),
			this.GetThumbnailName(num + 4),
			this.GetThumbnailName(num + 5),
			this.GetThumbnailName(num + 6),
			this.GetThumbnailName(num + 7),
			this.GetThumbnailName(num + 8),
			this.GetThumbnailName(num + 9),
			this.GetThumbnailName(num + 10),
			this.GetThumbnailName(num + 11)
		};
	}

	// Token: 0x06000736 RID: 1846 RVA: 0x0001EAA0 File Offset: 0x0001CCA0
	protected sealed override void BeforeInit()
	{
	}

	// Token: 0x06000737 RID: 1847 RVA: 0x0001EAA4 File Offset: 0x0001CCA4
	protected sealed override void AfterInit()
	{
		this.SetArrowAnimation("ArrowL", -1f);
		this.SetArrowAnimation("ArrowR", 1f);
	}

	// Token: 0x06000738 RID: 1848 RVA: 0x0001EAD4 File Offset: 0x0001CCD4
	protected sealed override BaseWindow.UIComponent[] newComponentArray()
	{
		List<BaseWindow.UIComponent> list = new List<BaseWindow.UIComponent>();
		list.Add(new BaseWindow.UIImage("BackGround", 0, 0, this.wndz, "screen/cgmemory2/cgm_bg_1", false, true));
		list.Add(new BaseWindow.UIImage("Header", 0, 0, this.wndz + 1, this.GetFilenameId(this.m_chara).ToString("screen/cgmemory/cgm_bnr_cha00"), false, true));
		int num = this.m_collect / 100;
		int num2 = (this.m_collect - num * 100) / 10;
		int num3 = this.m_collect - num * 100 - num2 * 10;
		list.Add(new BaseWindow.UIImage("Per100", 581, 17, this.wndz + 2, num.ToString("screen/cgmemory/cgm_kumi0"), false, true));
		list.Add(new BaseWindow.UIImage("Per010", 611, 17, this.wndz + 2, num2.ToString("screen/cgmemory/cgm_kumi0"), false, true));
		list.Add(new BaseWindow.UIImage("Per001", 641, 17, this.wndz + 2, num3.ToString("screen/cgmemory/cgm_kumi0"), false, true));
		int num4 = this.m_page + 1;
		int num5 = num4 / 10;
		int num6 = num4 - num5 * 10;
		list.Add(new BaseWindow.UIImage("Page010", 792, 17, this.wndz + 2, num5.ToString("screen/cgmemory/cgm_kumi0"), false, true));
		list.Add(new BaseWindow.UIImage("Page001", 822, 17, this.wndz + 2, num6.ToString("screen/cgmemory/cgm_kumi0"), false, true));
		for (int i = 0; i < this.MAX_THUMBNAILS_COUNT; i++)
		{
			int x_ = 33 + i % 4 * 224;
			int y_ = 122 + i / 4 * 140;
			int num7 = i + this.m_page * this.MAX_THUMBNAILS_COUNT;
			string text = i.ToString("Thumbnail00");
			list.Add(new BaseWindow.UIImage(text, x_, y_, this.wndz + 2, this.GetThumbnailName(num7), true, num7 < this.GetThumbnailLength()));
			list.Add(new BaseWindow.UIImage("Dummy" + text, x_, y_, this.wndz + 2, this.GetThumbnailName(num7), false, false));
		}
		list.Add(new BaseWindow.UIButton("Cancel", 887, 6, this.wndz + 3, "screen/common/cancel", true, true, 1, 2, 0));
		list.Add(new BaseWindow.UIImage("ArrowL", 33, 80, this.wndz + 3, "screen/cgmemory/cgm_arwl", false, true));
		list.Add(new BaseWindow.UIImage("ArrowR", 848, 80, this.wndz + 3, "screen/cgmemory/cgm_arwr", false, true));
		if (this.IsFont())
		{
			for (int j = 0; j < this.MAX_THUMBNAILS_COUNT; j++)
			{
				int x_2 = 245 + j % 4 * 224;
				int y_2 = 238 + j / 4 * 140;
				int index = j + this.m_page * this.MAX_THUMBNAILS_COUNT;
				string fontText = this.GetFontText(index);
				list.Add(new BaseWindow.UIText(j.ToString("Text00"), x_2, y_2, this.wndz + 3, fontText, 22, UnityTextSprite.PositionType.BottomRight, UnityTextSprite.PositionType.BottomRight, byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue));
			}
		}
		return list.ToArray();
	}

	// Token: 0x06000739 RID: 1849 RVA: 0x0001EE30 File Offset: 0x0001D030
	protected override void OnBaseWindowUpdate()
	{
		if (this.loading)
		{
			if (!this.LoadCompleted())
			{
				return;
			}
			this.OnLoadCompleted();
			this.loading = false;
			this.m_dir = 0;
		}
		if (this.action)
		{
			return;
		}
		INPUT_STATUS status = UnityApp.Input.Status;
		if (status == INPUT_STATUS.DRAG || status == INPUT_STATUS.FLICK)
		{
			SLIDE_VECTOR slideDragVector = UnityApp.Input.SlideDragVector;
			if (slideDragVector != SLIDE_VECTOR.LEFT)
			{
				if (slideDragVector == SLIDE_VECTOR.RIGHT)
				{
					base.PlaySE_Select();
					this.OnAllow(-1);
				}
			}
			else
			{
				base.PlaySE_Select();
				this.OnAllow(1);
			}
		}
	}

	// Token: 0x0600073A RID: 1850 RVA: 0x0001EEDC File Offset: 0x0001D0DC
	protected sealed override void OnBaseWindowOnButton(string obj)
	{
		if (this.loading)
		{
			return;
		}
		if (obj != null)
		{
			if (this.dict == null)
			{
				this.dict = new Dictionary<string, int>(13)
				{
					{
						"Thumbnail00",
						0
					},
					{
						"Thumbnail01",
						0
					},
					{
						"Thumbnail02",
						0
					},
					{
						"Thumbnail03",
						0
					},
					{
						"Thumbnail04",
						0
					},
					{
						"Thumbnail05",
						0
					},
					{
						"Thumbnail06",
						0
					},
					{
						"Thumbnail07",
						0
					},
					{
						"Thumbnail08",
						0
					},
					{
						"Thumbnail09",
						0
					},
					{
						"Thumbnail10",
						0
					},
					{
						"Thumbnail11",
						0
					},
					{
						"Cancel",
						1
					}
				};
			}
			int num;
			if (this.dict.TryGetValue(obj, out num))
			{
				if (num != 0)
				{
					if (num == 1)
					{
						base.PlaySE_Cancel();
						SceneManager.BackScene();
					}
				}
				else
				{
					int index = this.m_page * this.MAX_THUMBNAILS_COUNT + this.ButtonNameToThumbnailId(obj);
					if (this.IsSelectable(index))
					{
						this.action = true;
						base.PlaySE_Ok();
						this.OnSelect(index);
					}
				}
			}
		}
	}

	// Token: 0x0600073B RID: 1851 RVA: 0x0001F020 File Offset: 0x0001D220
	private void SetArrowAnimation(string name, float dir)
	{
		float delay = 0f;
		int repeat = 0;
		float speed = 0.5f;
		ImageObject component = base.GetGameObject(name).GetComponent<ImageObject>();
		component.AddImageAnimation(new ImageAnimationLocalPosition(repeat, speed, delay, MoveType.LoopSin000to180, new Vector3(0f, 0f, 0f), new Vector3(25f * dir, 0f, 0f)));
	}

	// Token: 0x0600073C RID: 1852 RVA: 0x0001F084 File Offset: 0x0001D284
	private void OnAllow(int dir)
	{
		this.loading = true;
		this.m_dir = dir;
		int num = this.pageCountLoop(this.m_page + this.m_dir);
		for (int i = 0; i < this.MAX_THUMBNAILS_COUNT; i++)
		{
			int index = i + num * this.MAX_THUMBNAILS_COUNT;
			string name = i.ToString("DummyThumbnail00");
			base.SetTexture(name, this.GetThumbnailName(index));
		}
	}

	// Token: 0x0600073D RID: 1853 RVA: 0x0001F0F0 File Offset: 0x0001D2F0
	private bool LoadCompleted()
	{
		for (int i = 0; i < this.MAX_THUMBNAILS_COUNT; i++)
		{
			string name = i.ToString("DummyThumbnail00");
			if (!this.LoadCompleted(base.GetGameObject(name)))
			{
				return false;
			}
		}
		return true;
	}

	// Token: 0x0600073E RID: 1854 RVA: 0x0001F138 File Offset: 0x0001D338
	private void OnLoadCompleted()
	{
		this.m_page = this.pageCountLoop(this.m_page + this.m_dir);
		UIValue.GalleryPage = this.m_page;
		int num = this.m_page + 1;
		int num2 = num / 10;
		int num3 = num - num2 * 10;
		base.SetTexture("Page010", num2.ToString("screen/cgmemory/cgm_kumi0"));
		base.SetTexture("Page001", num3.ToString("screen/cgmemory/cgm_kumi0"));
		for (int i = 0; i < this.MAX_THUMBNAILS_COUNT; i++)
		{
			int num4 = i + this.m_page * this.MAX_THUMBNAILS_COUNT;
			string name = i.ToString("Thumbnail00");
			base.SetTexture(name, this.GetThumbnailName(num4));
			base.GameObjectShow(name, num4 < this.GetThumbnailLength());
			if (this.IsFont())
			{
				int x = 245 + i % 4 * 224;
				int y = 238 + i / 4 * 140;
				this.SetFontStr(base.GetTextSprite(i.ToString("Text00")), this.GetFontText(num4), x, y);
			}
		}
	}

	// Token: 0x0600073F RID: 1855 RVA: 0x0001F258 File Offset: 0x0001D458
	private bool LoadCompleted(GameObject go)
	{
		ImageObject component = go.GetComponent<ImageObject>();
		return component == null || component.LoadCompleted;
	}

	// Token: 0x06000740 RID: 1856 RVA: 0x0001F280 File Offset: 0x0001D480
	private void SetFontStr(UnityTextSprite text, string str, int x, int y)
	{
		text.ClearText();
		text.AddText(str, 22);
		text.CalcSize();
		text.x = x - text.w;
		text.y = y - text.h;
		text.Update(960, 544, -3f);
	}

	// Token: 0x06000741 RID: 1857 RVA: 0x0001F2D8 File Offset: 0x0001D4D8
	private int GetFilenameId(int i)
	{
		return i + 1;
	}

	// Token: 0x06000742 RID: 1858 RVA: 0x0001F2E0 File Offset: 0x0001D4E0
	private int ButtonNameToThumbnailId(string obj)
	{
		switch (obj)
		{
		case "Thumbnail00":
			return 0;
		case "Thumbnail01":
			return 1;
		case "Thumbnail02":
			return 2;
		case "Thumbnail03":
			return 3;
		case "Thumbnail04":
			return 4;
		case "Thumbnail05":
			return 5;
		case "Thumbnail06":
			return 6;
		case "Thumbnail07":
			return 7;
		case "Thumbnail08":
			return 8;
		case "Thumbnail09":
			return 9;
		case "Thumbnail10":
			return 10;
		case "Thumbnail11":
			return 11;
		}
		return -1;
	}

	// Token: 0x0400069D RID: 1693
	protected readonly int MAX_THUMBNAILS_COUNT = 12;

	// Token: 0x0400069E RID: 1694
	private int m_chara;

	// Token: 0x0400069F RID: 1695
	private int m_page;

	// Token: 0x040006A0 RID: 1696
	private int m_collect;

	// Token: 0x040006A1 RID: 1697
	private bool loading;

	// Token: 0x040006A2 RID: 1698
	private bool action;

	// Token: 0x040006A3 RID: 1699
	private int m_dir;

	// Token: 0x040006A4 RID: 1700
	private readonly int wndz;
}
