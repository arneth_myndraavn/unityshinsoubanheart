﻿using System;
using UnityEngine;

// Token: 0x02000103 RID: 259
public class GalleryCGWindow : BaseWindow
{
	// Token: 0x060006C9 RID: 1737 RVA: 0x0001C594 File Offset: 0x0001A794
	protected sealed override void OnAwake()
	{
		this.arrowAnimationHolder = new GalleryCGWindow.ArrowAnimationHolder();
		this.cgLink = CSVManager.Instance.CsvCGListHolder.GetCharaIndex(UIValue.GalleryCharactor).CgLink;
		this.cgLink.SetPageByIndex(UIValue.GalleryIndex);
		this.moveBackGround = new GalleryCGWindow.MoveBackGround();
		this.action = false;
	}

	// Token: 0x060006CA RID: 1738 RVA: 0x0001C5F0 File Offset: 0x0001A7F0
	protected sealed override void OnBaseWindowDestroy()
	{
		this.moveBackGround.Release();
	}

	// Token: 0x060006CB RID: 1739 RVA: 0x0001C600 File Offset: 0x0001A800
	protected sealed override string[] newSceneTextureNameArray()
	{
		string text = "event/cg/" + this.cgLink.GetCurrentFPath();
		string text2 = "event/cg/" + this.cgLink.GetLeftFPath();
		string text3 = "event/cg/" + this.cgLink.GetRightFPath();
		string text4 = "event/cg/" + this.cgLink.GetUpFPath();
		string text5 = "event/cg/" + this.cgLink.GetDownFPath();
		return new string[]
		{
			text,
			text2,
			text3,
			text4,
			text5,
			"screen/cgmemory/cg_cursor_l",
			"screen/cgmemory/cg_cursor_r",
			"screen/cgmemory/cg_cursor_u",
			"screen/cgmemory/cg_cursor_d"
		};
	}

	// Token: 0x060006CC RID: 1740 RVA: 0x0001C6BC File Offset: 0x0001A8BC
	protected sealed override void AfterInit()
	{
		this.moveBackGround.Init(base.GetGameObject("BGHolder"));
		bool lr = this.cgLink.lr;
		bool ud = this.cgLink.ud;
		this.arrowAnimationHolder.SetAnimation(base.GetGameObject("ArrowL"), 0, lr);
		this.arrowAnimationHolder.SetAnimation(base.GetGameObject("ArrowR"), 1, lr);
		this.arrowAnimationHolder.SetAnimation(base.GetGameObject("ArrowU"), 2, ud);
		this.arrowAnimationHolder.SetAnimation(base.GetGameObject("ArrowD"), 3, ud);
		base.GetGameObject("BGHolder").transform.localScale = new Vector3(1f, 1f, 1f);
		base.SetParent("BGC", "BGHolder");
		base.SetParent("BGL", "BGHolder");
		base.SetParent("BGR", "BGHolder");
		base.SetParent("BGU", "BGHolder");
		base.SetParent("BGD", "BGHolder");
	}

	// Token: 0x060006CD RID: 1741 RVA: 0x0001C7D0 File Offset: 0x0001A9D0
	protected override void OnGraphicLoadComplete()
	{
		this.arrowAnimationHolder.SetRendererEnable(this.cgLink.lr, this.cgLink.ud);
		this.arrowAnimationHolder.Restart();
	}

	// Token: 0x060006CE RID: 1742 RVA: 0x0001C80C File Offset: 0x0001AA0C
	protected sealed override BaseWindow.UIComponent[] newComponentArray()
	{
		string fpath_ = "event/cg/" + this.cgLink.GetCurrentFPath();
		string fpath_2 = "event/cg/" + this.cgLink.GetLeftFPath();
		string fpath_3 = "event/cg/" + this.cgLink.GetRightFPath();
		string fpath_4 = "event/cg/" + this.cgLink.GetUpFPath();
		string fpath_5 = "event/cg/" + this.cgLink.GetDownFPath();
		return new BaseWindow.UIComponent[]
		{
			new BaseWindow.UIImage("BGC", 0, 0, this.wndz, fpath_, false, true),
			new BaseWindow.UIImage("BGL", -965, 0, this.wndz, fpath_2, false, true),
			new BaseWindow.UIImage("BGR", 965, 0, this.wndz, fpath_3, false, true),
			new BaseWindow.UIImage("BGU", 0, -549, this.wndz, fpath_4, false, true),
			new BaseWindow.UIImage("BGD", 0, 549, this.wndz, fpath_5, false, true),
			new BaseWindow.UICollision("BGHolder", 0, 0, this.wndz, 960, 544),
			new BaseWindow.UIImage("ArrowL", 2, 232, this.wndz + 1, "screen/cgmemory/cg_cursor_l", false, true),
			new BaseWindow.UIImage("ArrowR", 896, 232, this.wndz + 1, "screen/cgmemory/cg_cursor_r", false, true),
			new BaseWindow.UIImage("ArrowU", 439, 2, this.wndz + 1, "screen/cgmemory/cg_cursor_u", false, true),
			new BaseWindow.UIImage("ArrowD", 439, 481, this.wndz + 1, "screen/cgmemory/cg_cursor_d", false, true)
		};
	}

	// Token: 0x060006CF RID: 1743 RVA: 0x0001C9CC File Offset: 0x0001ABCC
	protected override void OnBaseWindowUpdate()
	{
		if (this.moveBackGround.Moving)
		{
			if (!this.moveBackGround.Update())
			{
				this.LoadBG();
				this.arrowAnimationHolder.SetRendererEnable(this.cgLink.lr, this.cgLink.ud);
				this.arrowAnimationHolder.Restart();
				this.action = false;
			}
			return;
		}
		if (this.action)
		{
			return;
		}
		switch (UnityApp.Input.Status)
		{
		case INPUT_STATUS.CLICK:
			this.action = true;
			this.OnClick();
			break;
		case INPUT_STATUS.DRAG_ON:
		{
			Point2 delta = UnityApp.Input.Delta;
			this.OnDraging(new Vector3((float)((!this.cgLink.lr) ? 0 : (-(float)delta.x)), (float)((!this.cgLink.ud) ? 0 : (-(float)delta.y)), 0f));
			break;
		}
		case INPUT_STATUS.DRAG:
		{
			Point2 delta2 = UnityApp.Input.Delta;
			this.OnDrag(new Vector3((float)((!this.cgLink.lr) ? 0 : (-(float)delta2.x)), (float)((!this.cgLink.ud) ? 0 : (-(float)delta2.y)), 0f));
			break;
		}
		case INPUT_STATUS.FLICK:
			this.OnFlick(UnityApp.Input.SlideDragVector);
			break;
		}
	}

	// Token: 0x060006D0 RID: 1744 RVA: 0x0001CB48 File Offset: 0x0001AD48
	private void OnClick()
	{
		int nowIndex = this.cgLink.GetNowIndex();
		UIValue.GalleryIndex = nowIndex;
		UIValue.GalleryPage = nowIndex / this.MAX_THUMBNAILS_COUNT;
		base.PlaySE_Cancel();
		SceneManager.BackScene();
	}

	// Token: 0x060006D1 RID: 1745 RVA: 0x0001CB80 File Offset: 0x0001AD80
	private void OnDraging(Vector3 v)
	{
		this.moveBackGround.SetBackGroundPosition(v);
	}

	// Token: 0x060006D2 RID: 1746 RVA: 0x0001CB90 File Offset: 0x0001AD90
	private void OnDrag(Vector3 v)
	{
		this.moveBackGround.SetBackGroundPosition(v);
		SLIDE_VECTOR slideVector = this.moveBackGround.GetSlideVector();
		switch (slideVector)
		{
		case SLIDE_VECTOR.LEFT:
		case SLIDE_VECTOR.RIGHT:
		case SLIDE_VECTOR.UP:
		case SLIDE_VECTOR.DOWN:
			this.OnFlick(slideVector);
			break;
		default:
			this.moveBackGround.ResetBackGroundPosition();
			break;
		}
	}

	// Token: 0x060006D3 RID: 1747 RVA: 0x0001CBF0 File Offset: 0x0001ADF0
	private bool checkDirLR(SLIDE_VECTOR slide)
	{
		return slide == SLIDE_VECTOR.LEFT || slide == SLIDE_VECTOR.RIGHT;
	}

	// Token: 0x060006D4 RID: 1748 RVA: 0x0001CC18 File Offset: 0x0001AE18
	private bool checkDir(SLIDE_VECTOR slide, SLIDE_VECTOR flick)
	{
		switch (slide)
		{
		case SLIDE_VECTOR.LEFT:
		case SLIDE_VECTOR.RIGHT:
		case SLIDE_VECTOR.UP:
		case SLIDE_VECTOR.DOWN:
			return this.checkDirLR(slide) == this.checkDirLR(flick);
		default:
			return true;
		}
	}

	// Token: 0x060006D5 RID: 1749 RVA: 0x0001CC58 File Offset: 0x0001AE58
	private void OnFlick(SLIDE_VECTOR slide)
	{
		if (!this.checkDir(this.moveBackGround.GetDirFixSlideVector(), slide))
		{
			this.moveBackGround.ResetBackGroundPosition();
			return;
		}
		string currentFPath = this.cgLink.GetCurrentFPath();
		switch (slide)
		{
		case SLIDE_VECTOR.LEFT:
			this.cgLink.Right();
			break;
		case SLIDE_VECTOR.RIGHT:
			this.cgLink.Left();
			break;
		case SLIDE_VECTOR.UP:
			this.cgLink.Down();
			break;
		case SLIDE_VECTOR.DOWN:
			this.cgLink.Up();
			break;
		}
		string currentFPath2 = this.cgLink.GetCurrentFPath();
		if (currentFPath2 != currentFPath)
		{
			base.PlaySE_Select();
			this.action = true;
			this.ChangeBG("event/cg/" + currentFPath2, "event/cg/" + this.cgLink.GetLeftFPath(), "event/cg/" + this.cgLink.GetRightFPath(), "event/cg/" + this.cgLink.GetUpFPath(), "event/cg/" + this.cgLink.GetDownFPath(), slide);
		}
	}

	// Token: 0x060006D6 RID: 1750 RVA: 0x0001CD80 File Offset: 0x0001AF80
	private void ChangeBG(string cpath, string lpath, string rpath, string upath, string dpath, SLIDE_VECTOR slide)
	{
		this.moveBackGround.ChangeBG(cpath, lpath, rpath, upath, dpath, slide);
	}

	// Token: 0x060006D7 RID: 1751 RVA: 0x0001CD98 File Offset: 0x0001AF98
	private void LoadBG()
	{
		base.SetTexture("BGC", this.cgLink.GetCurrentFPath());
		base.SetTexture("BGL", this.cgLink.GetLeftFPath());
		base.SetTexture("BGR", this.cgLink.GetRightFPath());
		base.SetTexture("BGU", this.cgLink.GetUpFPath());
		base.SetTexture("BGD", this.cgLink.GetDownFPath());
	}

	// Token: 0x04000661 RID: 1633
	private const int SCREEN_W = 960;

	// Token: 0x04000662 RID: 1634
	private const int SCREEN_H = 544;

	// Token: 0x04000663 RID: 1635
	private const int CG_POS_X = 965;

	// Token: 0x04000664 RID: 1636
	private const int CG_POS_Y = 549;

	// Token: 0x04000665 RID: 1637
	protected readonly int MAX_THUMBNAILS_COUNT = 12;

	// Token: 0x04000666 RID: 1638
	private readonly int wndz;

	// Token: 0x04000667 RID: 1639
	private GalleryCGWindow.ArrowAnimationHolder arrowAnimationHolder;

	// Token: 0x04000668 RID: 1640
	private CSVCGListHolder.CGLink cgLink;

	// Token: 0x04000669 RID: 1641
	private GalleryCGWindow.MoveBackGround moveBackGround;

	// Token: 0x0400066A RID: 1642
	private bool action;

	// Token: 0x02000104 RID: 260
	private class ArrowAnimation
	{
		// Token: 0x060006D8 RID: 1752 RVA: 0x0001CE14 File Offset: 0x0001B014
		public ArrowAnimation(Vector2 direction)
		{
			float num = 0f;
			int num2 = 3;
			float num3 = 0.8f;
			this.lp = new ImageAnimationLocalPosition(num2, num3, num, MoveType.LoopSin000to180, new Vector3(0f, 0f, 0f), new Vector3(25f * direction.x, 25f * direction.y, 0f));
			this.mc = new ImageAnimationMaterialColor(1, num3, num + (float)(num2 - 1) * num3, new Color(1f, 1f, 1f, 1f), new Color(1f, 1f, 1f, 0f));
		}

		// Token: 0x060006D9 RID: 1753 RVA: 0x0001CEC4 File Offset: 0x0001B0C4
		public void Restart()
		{
			this.lp.Restart();
			this.mc.Restart();
		}

		// Token: 0x0400066B RID: 1643
		public ImageAnimationLocalPosition lp;

		// Token: 0x0400066C RID: 1644
		public ImageAnimationMaterialColor mc;
	}

	// Token: 0x02000105 RID: 261
	private class ArrowAnimationHolder
	{
		// Token: 0x060006DA RID: 1754 RVA: 0x0001CEDC File Offset: 0x0001B0DC
		public ArrowAnimationHolder()
		{
			this.animation = new GalleryCGWindow.ArrowAnimation[4];
			this.animation[0] = new GalleryCGWindow.ArrowAnimation(new Vector2(1f, 0f));
			this.animation[1] = new GalleryCGWindow.ArrowAnimation(new Vector2(-1f, 0f));
			this.animation[2] = new GalleryCGWindow.ArrowAnimation(new Vector2(0f, 1f));
			this.animation[3] = new GalleryCGWindow.ArrowAnimation(new Vector2(0f, -1f));
			this.gameObject = new GameObject[4];
		}

		// Token: 0x060006DB RID: 1755 RVA: 0x0001CF78 File Offset: 0x0001B178
		public void SetAnimation(GameObject go, int index, bool enabled)
		{
			ImageObject component = go.GetComponent<ImageObject>();
			component.AddImageAnimation(this.animation[index].lp);
			component.AddImageAnimation(this.animation[index].mc);
			component.RendererEnableAfterLoad = enabled;
			this.gameObject[index] = go;
		}

		// Token: 0x060006DC RID: 1756 RVA: 0x0001CFC4 File Offset: 0x0001B1C4
		public void Restart()
		{
			foreach (GalleryCGWindow.ArrowAnimation arrowAnimation in this.animation)
			{
				arrowAnimation.Restart();
			}
		}

		// Token: 0x060006DD RID: 1757 RVA: 0x0001CFF8 File Offset: 0x0001B1F8
		public void SetRendererEnable(bool lr, bool ud)
		{
			this.gameObject[0].GetComponent<Renderer>().enabled = lr;
			this.gameObject[1].GetComponent<Renderer>().enabled = lr;
			this.gameObject[2].GetComponent<Renderer>().enabled = ud;
			this.gameObject[3].GetComponent<Renderer>().enabled = ud;
		}

		// Token: 0x0400066D RID: 1645
		private GalleryCGWindow.ArrowAnimation[] animation;

		// Token: 0x0400066E RID: 1646
		private GameObject[] gameObject;
	}

	// Token: 0x02000106 RID: 262
	private class MovableObjectHolder
	{
		// Token: 0x060006DE RID: 1758 RVA: 0x0001D054 File Offset: 0x0001B254
		public MovableObjectHolder()
		{
			this.go = null;
			this.slide = null;
			this.drag = null;
			this.golocal = new Vector3(0f, 0f, 0f);
			this.move = this.golocal;
			this.dirFix = false;
			this.dirHorizon = false;
		}

		// Token: 0x060006DF RID: 1759 RVA: 0x0001D0C8 File Offset: 0x0001B2C8
		public void Init(GameObject go_)
		{
			this.go = go_;
			this.slide = new ImageAnimationLocalPosition(1, this.asSlide, 0f, MoveType.OneshotSin000to090, new Vector3(0f, 0f, 0f), new Vector3(0f, 0f, 0f));
			this.drag = new ImageAnimationLocalPosition(1, this.asDrag, 0f, MoveType.OneshotSin000to090, new Vector3(0f, 0f, 0f), new Vector3(0f, 0f, 0f));
			this.golocal = this.go.transform.localPosition;
		}

		// Token: 0x060006E0 RID: 1760 RVA: 0x0001D174 File Offset: 0x0001B374
		public bool Update()
		{
			this.slide.Update();
			this.go.transform.localPosition = this.slide.CalcPosition(this.golocal);
			return this.slide.Active();
		}

		// Token: 0x060006E1 RID: 1761 RVA: 0x0001D1B8 File Offset: 0x0001B3B8
		public void SetBackGroundPosition(Vector3 v)
		{
			if (!this.dirFix)
			{
				float num = Mathf.Abs(v.x);
				float num2 = Mathf.Abs(v.y);
				bool flag = num > num2;
				float num3 = (!flag) ? num2 : num;
				if (num3 > 32f)
				{
					this.dirFix = true;
					this.dirHorizon = flag;
				}
			}
			if (this.dirFix)
			{
				Vector3 rhs = new Vector3((!this.dirHorizon) ? 0f : v.x, (!this.dirHorizon) ? v.y : 0f, 0f);
				if (this.move != rhs)
				{
					this.move = rhs;
					this.SetAnimation(this.drag, this.drag.Offset, this.move, this.asDrag, true);
				}
				this.drag.Update();
				this.go.transform.localPosition = this.drag.CalcPosition(this.golocal);
			}
		}

		// Token: 0x060006E2 RID: 1762 RVA: 0x0001D2D0 File Offset: 0x0001B4D0
		public void ResetBackGroundPosition()
		{
			this.MoveStart(new Vector3(0f, 0f, 0f));
		}

		// Token: 0x060006E3 RID: 1763 RVA: 0x0001D2EC File Offset: 0x0001B4EC
		public SLIDE_VECTOR GetDirFixSlideVector()
		{
			if (!this.dirFix)
			{
				return SLIDE_VECTOR.NONE;
			}
			if (this.dirHorizon)
			{
				return (this.move.x >= 0f) ? SLIDE_VECTOR.RIGHT : SLIDE_VECTOR.LEFT;
			}
			return (this.move.y >= 0f) ? SLIDE_VECTOR.DOWN : SLIDE_VECTOR.UP;
		}

		// Token: 0x060006E4 RID: 1764 RVA: 0x0001D34C File Offset: 0x0001B54C
		public SLIDE_VECTOR GetSlideVector()
		{
			if (this.dirFix)
			{
				float num = Mathf.Abs(this.move.x);
				float num2 = Mathf.Abs(this.move.y);
				float num3 = (!this.dirHorizon) ? num2 : num;
				float num4 = (float)((!this.dirHorizon) ? 272 : 480);
				if (num3 > num4)
				{
					if (this.dirHorizon)
					{
						return (this.move.x >= 0f) ? SLIDE_VECTOR.RIGHT : SLIDE_VECTOR.LEFT;
					}
					return (this.move.y >= 0f) ? SLIDE_VECTOR.DOWN : SLIDE_VECTOR.UP;
				}
			}
			return SLIDE_VECTOR.NONE;
		}

		// Token: 0x060006E5 RID: 1765 RVA: 0x0001D404 File Offset: 0x0001B604
		public void MoveStart(Vector3 end)
		{
			this.dirFix = false;
			this.dirHorizon = false;
			float num = this.calcRangeRate(this.drag.Offset, end);
			this.SetAnimation(this.slide, this.drag.Offset, end, this.asSlide * num, true);
		}

		// Token: 0x060006E6 RID: 1766 RVA: 0x0001D454 File Offset: 0x0001B654
		public void ResetOffsetEnd()
		{
			Vector3 vector = new Vector3(0f, 0f, 0f);
			this.SetAnimation(this.slide, vector, vector, this.asSlide, true);
			this.slide.Update();
			this.SetAnimation(this.drag, vector, vector, this.asDrag, true);
			this.drag.Update();
			this.go.transform.localPosition = this.golocal;
		}

		// Token: 0x060006E7 RID: 1767 RVA: 0x0001D4D0 File Offset: 0x0001B6D0
		private void SetAnimation(ImageAnimationLocalPosition animation, Vector3 begin, Vector3 end, float speed, bool restart)
		{
			animation.OffsetBegin = begin;
			animation.OffsetEnd = end;
			animation.Speed = speed;
			if (restart)
			{
				animation.Restart();
			}
		}

		// Token: 0x060006E8 RID: 1768 RVA: 0x0001D4F8 File Offset: 0x0001B6F8
		private float calcRangeRate(Vector3 begin, Vector3 end)
		{
			float num = this.calcRangeRate(begin.x - end.x, 965f);
			if (num != 0f)
			{
				return num;
			}
			float num2 = this.calcRangeRate(begin.y - end.y, 549f);
			if (num2 != 0f)
			{
				return num2;
			}
			return 0.1f;
		}

		// Token: 0x060006E9 RID: 1769 RVA: 0x0001D55C File Offset: 0x0001B75C
		private float calcRangeRate(float d, float m)
		{
			return (d != 0f) ? Math.Max(0.2f, Mathf.Abs(d) / m) : 0f;
		}

		// Token: 0x0400066F RID: 1647
		private readonly float asDrag = 0.05f;

		// Token: 0x04000670 RID: 1648
		private readonly float asSlide = 1f;

		// Token: 0x04000671 RID: 1649
		private GameObject go;

		// Token: 0x04000672 RID: 1650
		private ImageAnimationLocalPosition slide;

		// Token: 0x04000673 RID: 1651
		private ImageAnimationLocalPosition drag;

		// Token: 0x04000674 RID: 1652
		private Vector3 golocal;

		// Token: 0x04000675 RID: 1653
		private Vector3 move;

		// Token: 0x04000676 RID: 1654
		private bool dirFix;

		// Token: 0x04000677 RID: 1655
		private bool dirHorizon;
	}

	// Token: 0x02000107 RID: 263
	private class GraphicLoader
	{
		// Token: 0x060006EA RID: 1770 RVA: 0x0001D588 File Offset: 0x0001B788
		public GraphicLoader()
		{
			this.graphicHolder = new GraphicManager.SceneGraphicHolder(new string[0]);
			this.stanbyPath = new string[0];
		}

		// Token: 0x060006EB RID: 1771 RVA: 0x0001D5B0 File Offset: 0x0001B7B0
		public void Standby(string[] path)
		{
			this.stanbyPath = path;
		}

		// Token: 0x060006EC RID: 1772 RVA: 0x0001D5BC File Offset: 0x0001B7BC
		public void Create(string[] path)
		{
			this.graphicHolder.Create(path);
		}

		// Token: 0x060006ED RID: 1773 RVA: 0x0001D5CC File Offset: 0x0001B7CC
		public void Release()
		{
			this.graphicHolder.Release();
		}

		// Token: 0x060006EE RID: 1774 RVA: 0x0001D5DC File Offset: 0x0001B7DC
		public bool isDone()
		{
			if (this.stanbyPath.Length != 0)
			{
				this.graphicHolder.Create(this.stanbyPath);
				this.stanbyPath = new string[0];
			}
			return this.graphicHolder.isDone();
		}

		// Token: 0x04000678 RID: 1656
		private GraphicManager.SceneGraphicHolder graphicHolder;

		// Token: 0x04000679 RID: 1657
		private string[] stanbyPath;
	}

	// Token: 0x02000108 RID: 264
	private class MoveBackGround
	{
		// Token: 0x060006EF RID: 1775 RVA: 0x0001D614 File Offset: 0x0001B814
		public MoveBackGround()
		{
			this.movableObjectHolder = new GalleryCGWindow.MovableObjectHolder();
			this.moving = false;
			this.graphicLoader = new GalleryCGWindow.GraphicLoader();
		}

		// Token: 0x060006F0 RID: 1776 RVA: 0x0001D63C File Offset: 0x0001B83C
		public void Release()
		{
			this.graphicLoader.Release();
		}

		// Token: 0x170000DB RID: 219
		// (get) Token: 0x060006F1 RID: 1777 RVA: 0x0001D64C File Offset: 0x0001B84C
		public bool Moving
		{
			get
			{
				return this.moving;
			}
		}

		// Token: 0x060006F2 RID: 1778 RVA: 0x0001D654 File Offset: 0x0001B854
		public void Init(GameObject movableObject)
		{
			this.movableObjectHolder.Init(movableObject);
		}

		// Token: 0x060006F3 RID: 1779 RVA: 0x0001D664 File Offset: 0x0001B864
		public void SetBackGroundPosition(Vector3 v)
		{
			this.movableObjectHolder.SetBackGroundPosition(v);
		}

		// Token: 0x060006F4 RID: 1780 RVA: 0x0001D674 File Offset: 0x0001B874
		public void ResetBackGroundPosition()
		{
			this.movableObjectHolder.ResetBackGroundPosition();
			this.moving = true;
		}

		// Token: 0x060006F5 RID: 1781 RVA: 0x0001D688 File Offset: 0x0001B888
		public SLIDE_VECTOR GetDirFixSlideVector()
		{
			return this.movableObjectHolder.GetDirFixSlideVector();
		}

		// Token: 0x060006F6 RID: 1782 RVA: 0x0001D698 File Offset: 0x0001B898
		public SLIDE_VECTOR GetSlideVector()
		{
			return this.movableObjectHolder.GetSlideVector();
		}

		// Token: 0x060006F7 RID: 1783 RVA: 0x0001D6A8 File Offset: 0x0001B8A8
		public void ChangeBG(string cpath, string lpath, string rpath, string upath, string dpath, SLIDE_VECTOR slide)
		{
			string[] path = new string[]
			{
				cpath,
				lpath,
				rpath,
				upath,
				dpath
			};
			this.graphicLoader.Standby(path);
			Vector3 vector = new Vector3(965f, 549f, 0f);
			Vector3 vector2 = this.calcVector(slide);
			Vector3 end = new Vector3(vector2.x * vector.x, vector2.y * vector.y, 0f);
			this.movableObjectHolder.MoveStart(end);
			this.moving = true;
		}

		// Token: 0x060006F8 RID: 1784 RVA: 0x0001D73C File Offset: 0x0001B93C
		public bool Update()
		{
			if (this.moving && !this.movableObjectHolder.Update() && this.graphicLoader.isDone())
			{
				this.movableObjectHolder.ResetOffsetEnd();
				this.moving = false;
			}
			return this.moving;
		}

		// Token: 0x060006F9 RID: 1785 RVA: 0x0001D790 File Offset: 0x0001B990
		public Vector3 calcVector(SLIDE_VECTOR slide)
		{
			switch (slide)
			{
			case SLIDE_VECTOR.LEFT:
				return new Vector3(-1f, 0f, 0f);
			case SLIDE_VECTOR.RIGHT:
				return new Vector3(1f, 0f, 0f);
			case SLIDE_VECTOR.UP:
				return new Vector3(0f, -1f, 0f);
			case SLIDE_VECTOR.DOWN:
				return new Vector3(0f, 1f, 0f);
			default:
				return new Vector3(0f, 0f, 0f);
			}
		}

		// Token: 0x0400067A RID: 1658
		private GalleryCGWindow.MovableObjectHolder movableObjectHolder;

		// Token: 0x0400067B RID: 1659
		private bool moving;

		// Token: 0x0400067C RID: 1660
		private GalleryCGWindow.GraphicLoader graphicLoader;
	}
}
