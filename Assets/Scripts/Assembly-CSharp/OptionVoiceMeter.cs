﻿using System;
using System.Collections;
using Game;
using Qoo.Game;
using Qoo.SoundSystem;
using UnityEngine;

// Token: 0x020000F0 RID: 240
public class OptionVoiceMeter : Meter
{
	// Token: 0x0600067E RID: 1662 RVA: 0x0001AF0C File Offset: 0x0001910C
	public void Init(int width, int height, int index)
	{
		this.m_Index = index;
		this.m_Value = SysData.GetVoiceVolume((CHAR_ID)this.m_Index);
		base.Init();
	}

	// Token: 0x0600067F RID: 1663 RVA: 0x0001AF2C File Offset: 0x0001912C
	protected override IEnumerator WaitLoadTextureCoroutine()
	{
		while (base.gameObject.GetComponent<Renderer>().material.mainTexture == null)
		{
			yield return 0;
		}
		this.UpdateMeter((float)this.m_Value / 5f);
		yield break;
	}

	// Token: 0x06000680 RID: 1664 RVA: 0x0001AF48 File Offset: 0x00019148
	public override void OnEdit(float value)
	{
		float num = value * 5f;
		if (num > 0f && num < 1f)
		{
			num = 1f;
		}
		else
		{
			num = Mathf.Round(num);
		}
		if (this.m_Value != (int)num)
		{
			this.m_Value = (int)num;
			SysData.SetVoiceVolume((CHAR_ID)this.m_Index, (int)num);
		}
	}

	// Token: 0x06000681 RID: 1665 RVA: 0x0001AFA8 File Offset: 0x000191A8
	public override void OnEditEnd()
	{
		string text = Chara.GetVoiceFromId((CHAR_ID)this.m_Index);
		this.UpdateMeter((float)this.m_Value / 5f);
		Sound.VoiceStop();
		if (text == null)
		{
			text = "mob";
		}
		Sound.VoicePlay(text + "_v", 1f);
	}

	// Token: 0x040005E8 RID: 1512
	private int m_Index;
}
