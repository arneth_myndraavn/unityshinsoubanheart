﻿using System;

// Token: 0x020001A0 RID: 416
public class UITextureInfo
{
	// Token: 0x06000C0A RID: 3082 RVA: 0x00032348 File Offset: 0x00030548
	public UITextureInfo(int blockX, int blockY, string texturePath)
	{
		this.TexturePath = texturePath;
		this.BlockX = blockX;
		this.BlockY = blockY;
	}

	// Token: 0x170001A7 RID: 423
	// (get) Token: 0x06000C0B RID: 3083 RVA: 0x00032368 File Offset: 0x00030568
	public int BlockNum
	{
		get
		{
			return this.BlockX * this.BlockY;
		}
	}

	// Token: 0x04000949 RID: 2377
	public string TexturePath;

	// Token: 0x0400094A RID: 2378
	public int BlockX;

	// Token: 0x0400094B RID: 2379
	public int BlockY;
}
