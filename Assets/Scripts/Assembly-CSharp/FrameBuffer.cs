﻿using System;
using UnityEngine;

// Token: 0x02000153 RID: 339
public class FrameBuffer
{
	// Token: 0x0600096E RID: 2414 RVA: 0x00029D7C File Offset: 0x00027F7C
	public FrameBuffer(int w, int h, string name)
	{
		this.m_Tex = new RenderTexture(w, h, 16);
		this.m_Tex.name = name;
		this.m_Tex.antiAliasing = 1;
		this.m_isReady = false;
	}

	// Token: 0x17000127 RID: 295
	// (get) Token: 0x0600096F RID: 2415 RVA: 0x00029DC0 File Offset: 0x00027FC0
	// (set) Token: 0x06000970 RID: 2416 RVA: 0x00029DC8 File Offset: 0x00027FC8
	public RenderTexture Tex
	{
		get
		{
			return this.m_Tex;
		}
		set
		{
			this.m_Tex = value;
		}
	}

	// Token: 0x17000128 RID: 296
	// (get) Token: 0x06000971 RID: 2417 RVA: 0x00029DD4 File Offset: 0x00027FD4
	// (set) Token: 0x06000972 RID: 2418 RVA: 0x00029DDC File Offset: 0x00027FDC
	public bool IsReady
	{
		get
		{
			return this.m_isReady;
		}
		set
		{
			this.m_isReady = value;
		}
	}

	// Token: 0x040007D1 RID: 2001
	private RenderTexture m_Tex;

	// Token: 0x040007D2 RID: 2002
	private bool m_isReady;
}
