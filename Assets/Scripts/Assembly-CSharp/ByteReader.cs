﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

// Token: 0x0200014F RID: 335
public class ByteReader
{
	// Token: 0x0600094B RID: 2379 RVA: 0x000288C8 File Offset: 0x00026AC8
	public ByteReader(byte[] bytes)
	{
		this.mBuffer = bytes;
	}

	// Token: 0x0600094C RID: 2380 RVA: 0x000288D8 File Offset: 0x00026AD8
	public ByteReader(TextAsset asset)
	{
		this.mBuffer = asset.bytes;
	}

	// Token: 0x1700011E RID: 286
	// (get) Token: 0x0600094D RID: 2381 RVA: 0x000288EC File Offset: 0x00026AEC
	public bool canRead
	{
		get
		{
			return this.mBuffer != null && this.mOffset < this.mBuffer.Length;
		}
	}

	// Token: 0x0600094E RID: 2382 RVA: 0x0002890C File Offset: 0x00026B0C
	private static string ReadLine(byte[] buffer, int start, int count)
	{
		return Encoding.UTF8.GetString(buffer, start, count);
	}

	// Token: 0x0600094F RID: 2383 RVA: 0x0002891C File Offset: 0x00026B1C
	public string ReadLine()
	{
		int num = this.mBuffer.Length;
		while (this.mOffset < num && this.mBuffer[this.mOffset] < 32)
		{
			this.mOffset++;
		}
		int i = this.mOffset;
		if (i < num)
		{
			while (i < num)
			{
				int num2 = (int)this.mBuffer[i++];
				if (num2 == 10 || num2 == 13)
				{
					//IL_81:
					string result = ByteReader.ReadLine(this.mBuffer, this.mOffset, i - this.mOffset - 1);
					this.mOffset = i;
					return result;
				}
			}
			i++;
			//goto IL_81;
			string result1 = ByteReader.ReadLine(this.mBuffer, this.mOffset, i - this.mOffset - 1);
			this.mOffset = i;
			return result1;
		}
		this.mOffset = num;
		return null;
	}

	// Token: 0x06000950 RID: 2384 RVA: 0x000289DC File Offset: 0x00026BDC
	public Dictionary<string, string> ReadDictionary()
	{
		Dictionary<string, string> dictionary = new Dictionary<string, string>();
		char[] separator = new char[]
		{
			'='
		};
		while (this.canRead)
		{
			string text = this.ReadLine();
			if (text == null)
			{
				break;
			}
			string[] array = text.Split(separator, 2, StringSplitOptions.RemoveEmptyEntries);
			if (array.Length == 2)
			{
				string key = array[0].Trim();
				string value = array[1].Trim();
				dictionary[key] = value;
			}
		}
		return dictionary;
	}

	// Token: 0x040007C0 RID: 1984
	private byte[] mBuffer;

	// Token: 0x040007C1 RID: 1985
	private int mOffset;
}
