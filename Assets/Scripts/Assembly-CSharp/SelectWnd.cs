﻿using System;
using System.Collections;
using System.Collections.Generic;
using Qoo;
using Qoo.Application;
using Qoo.Def;
using Qoo.Game;
using Qoo.Input;
using Qoo.Select;
using Qoo.SoundSystem;
using UnityEngine;

// Token: 0x020000BD RID: 189
public class SelectWnd : Singleton<SelectWnd>
{
	// Token: 0x170000CF RID: 207
	// (get) Token: 0x060005A2 RID: 1442 RVA: 0x000169CC File Offset: 0x00014BCC
	// (set) Token: 0x060005A3 RID: 1443 RVA: 0x000169D4 File Offset: 0x00014BD4
	private static int SelectNo
	{
		get
		{
			return SelectWnd.m_nSelectNo;
		}
		set
		{
			SelectWnd.m_nSelectNo = value;
		}
	}

	// Token: 0x170000D0 RID: 208
	// (get) Token: 0x060005A4 RID: 1444 RVA: 0x000169DC File Offset: 0x00014BDC
	// (set) Token: 0x060005A5 RID: 1445 RVA: 0x000169E4 File Offset: 0x00014BE4
	public bool IsRun { get; private set; }

	// Token: 0x060005A6 RID: 1446 RVA: 0x000169F0 File Offset: 0x00014BF0
	private void Awake()
	{
		this.Init();
		base.enabled = false;
	}

	// Token: 0x060005A7 RID: 1447 RVA: 0x00016A00 File Offset: 0x00014C00
	private void Update()
	{
		if (SelectWnd.m_SelectBtn.Count == 0)
		{
			return;
		}
		if (this.IsSelectEnd())
		{
			return;
		}
		if (KsInput.IsMenu)
		{
			return;
		}
		if (base.QooGraph.IsFade)
		{
			return;
		}
		if (KsInput.State == INPUT_STATE.JUMP_NEXT)
		{
			return;
		}
		foreach (SelectBtn selectBtn in SelectWnd.m_SelectBtn)
		{
			selectBtn.Restore();
			selectBtn.Show(true);
		}
		switch (UnityApp.Input.Status)
		{
		case INPUT_STATUS.PUSH:
			foreach (SelectBtn selectBtn2 in SelectWnd.m_SelectBtn)
			{
				selectBtn2.Select(selectBtn2.IsHit(UnityApp.Input.Start));
			}
			break;
		case INPUT_STATUS.CLICK:
			for (int num = 0; num != SelectWnd.m_SelectBtn.Count; num++)
			{
				SelectWnd.m_SelectBtn[num].Select(false);
			}
			for (int num2 = 0; num2 != SelectWnd.m_SelectBtn.Count; num2++)
			{
				SelectBtn selectBtn3 = SelectWnd.m_SelectBtn[num2];
				if (selectBtn3.IsHit(UnityApp.Input.End))
				{
					this.Select(num2);
					return;
				}
			}
			break;
		case INPUT_STATUS.DRAG_ON:
			break;
		default:
			foreach (SelectBtn selectBtn4 in SelectWnd.m_SelectBtn)
			{
				selectBtn4.Select(false);
			}
			break;
		}
		if (Qoo.Debug.IsAutoKsDebug)
		{
			this.Select(App.GetRandom(SelectWnd.m_SelectBtn.Count));
		}
	}

	// Token: 0x060005A8 RID: 1448 RVA: 0x00016C44 File Offset: 0x00014E44
	private void Select(int index)
	{
		Sound.SE_Ok();
		SelectBtn selectBtn = SelectWnd.m_SelectBtn[index];
		selectBtn.Select(true);
		Qoo.Debug.Print("Select:" + index);
		base.enabled = false;
		if (this.m_isAuto)
		{
			KsInput.State = INPUT_STATE.AUTO;
			Singleton<MsgWnd>.Instance.IsAutoSkip = true;
		}
		base.StartCoroutine(this.WaitEnd(index, App.QooSelect.IsAnim(index)));
	}

	// Token: 0x060005A9 RID: 1449 RVA: 0x00016CBC File Offset: 0x00014EBC
	private IEnumerator WaitEnd(int select, bool IsAnim)
	{
		if (IsAnim)
		{
			this.m_LoveAnim = Man2D.Sprite("sys_love.png");
			Point2 pos = SelectWnd.m_SelectBtn[select].GetPos();
			if (App.QooSelect.IsGrp())
			{
				pos.x += 50;
				pos.y += 186;
			}
			else
			{
				pos.x += 364;
				pos.y += -18;
			}
			this.m_LoveAnim.SetPos(pos.x, pos.y, 783);
			this.m_LoveAnim.SetPtnNum(7, 2);
			this.m_LoveAnim.SetPtnPos(0, 0);
			this.m_LoveAnim.RepeatUVAnim(false);
			this.m_LoveAnim.SetUVAnimWait(4);
			this.m_LoveAnim.EnableUVAnim(true);
			while (this.m_LoveAnim.IsEnableUVAnim())
			{
				yield return 0;
				if (this.m_LoveAnim == null)
				{
					IL_198:
					Singleton<Man2D>.Instance.RemoveSprite(this.m_LoveAnim);
					this.m_LoveAnim = null;
					goto IL_1DB;
				}
			}
			//goto IL_198;
			Singleton<Man2D>.Instance.RemoveSprite(this.m_LoveAnim);
			this.m_LoveAnim = null;
			goto IL_1DB;
		}
		yield return new WaitForSeconds(1f);
		IL_1DB:
		SelectWnd.SelectNo = select;
		yield break;
	}

	// Token: 0x060005AA RID: 1450 RVA: 0x00016CF4 File Offset: 0x00014EF4
	public void OnDestroy()
	{
		this.Init();
	}

	// Token: 0x060005AB RID: 1451 RVA: 0x00016CFC File Offset: 0x00014EFC
	public void Init()
	{
		this.ReleaseBtn();
		SelectWnd.SelectNo = -1;
		if (Singleton<Man2D>.IsReady && this.m_LoveAnim != null)
		{
			Singleton<Man2D>.Instance.RemoveSprite(this.m_LoveAnim);
		}
		this.m_LoveAnim = null;
	}

	// Token: 0x060005AC RID: 1452 RVA: 0x00016D44 File Offset: 0x00014F44
	public void ReleaseBtn()
	{
		foreach (SelectBtn selectBtn in SelectWnd.m_SelectBtn)
		{
			selectBtn.Release();
		}
		SelectWnd.m_SelectBtn.Clear();
	}

	// Token: 0x060005AD RID: 1453 RVA: 0x00016DB4 File Offset: 0x00014FB4
	public void Begin(bool p, bool bReaded, string nameTable)
	{
		Qoo.Debug.Assert(App.QooSelect.Count > 0, "選択肢が登録されていません");
		this.Init();
		this.m_isAuto = KsInput.IsAuto;
		if (this.m_isAuto)
		{
			Singleton<MsgWnd>.Instance.IsAutoSkip = false;
			KsInput.Clear();
		}
		UnityApp.Input.Clear();
		UnityApp.Input.IsOnePushDelete = true;
		UnityTask.SetSubTask(this.CreateSelectBtn(bReaded, nameTable));
		this.IsRun = true;
	}

	// Token: 0x060005AE RID: 1454 RVA: 0x00016E30 File Offset: 0x00015030
	private IEnumerator CreateSelectBtn(bool bReaded, string nameTable)
	{
		foreach (SelectItem item in App.QooSelect.SelectAr)
		{
			if (item.IsGrpFile)
			{
				Man2D.ReadyTexture(item.Select, true);
			}
		}
		yield return 0;
		foreach (SelectItem item2 in App.QooSelect.SelectAr)
		{
			if (item2.IsGrpFile)
			{
				while (!Man2D.IsUseTexture(item2.Select))
				{
					yield return base.StartCoroutine(Singleton<TaskManager>.Instance.RunReadTask());
				}
			}
		}
		Point2[] pos = SelectDef.GetBtnPos(nameTable, App.QooSelect.IsGrp(), App.QooSelect.Count);
		int count = 0;
		foreach (SelectItem item3 in App.QooSelect.SelectAr)
		{
			SelectBtn btn = new SelectBtn();
			if (item3.IsGrpFile)
			{
				btn.CreateGrpBtn(item3.Select, bReaded && SysData.IsReadedMark(item3.Ks, item3.Label), count);
			}
			else
			{
				btn.CreateTxtBtn(item3.Select, bReaded && SysData.IsReadedMark(item3.Ks, item3.Label));
			}
			btn.SetPos(pos[SelectWnd.m_SelectBtn.Count]);
			SelectWnd.m_SelectBtn.Add(btn);
			count++;
		}
		base.enabled = true;
		yield break;
	}

	// Token: 0x060005AF RID: 1455 RVA: 0x00016E68 File Offset: 0x00015068
	public void End()
	{
		Qoo.Debug.Assert(App.QooSelect.SelectAr.Count > 0, "選択肢が登録されていません");
		App.QooSelect.Clear();
		this.ReleaseBtn();
		if (Singleton<Man2D>.IsReady && this.m_LoveAnim != null)
		{
			Singleton<Man2D>.Instance.RemoveSprite(this.m_LoveAnim);
		}
		this.m_LoveAnim = null;
		this.IsRun = false;
	}

	// Token: 0x060005B0 RID: 1456 RVA: 0x00016ED8 File Offset: 0x000150D8
	public int GetSelectNo()
	{
		return SelectWnd.SelectNo;
	}

	// Token: 0x060005B1 RID: 1457 RVA: 0x00016EE0 File Offset: 0x000150E0
	public int GetSelectNum()
	{
		return App.QooSelect.SelectAr.Count;
	}

	// Token: 0x060005B2 RID: 1458 RVA: 0x00016EF4 File Offset: 0x000150F4
	public bool IsGrp()
	{
		return App.QooSelect.IsGrp();
	}

	// Token: 0x060005B3 RID: 1459 RVA: 0x00016F00 File Offset: 0x00015100
	public string GetSelectKs()
	{
		Qoo.Debug.Assert(App.QooSelect.SelectAr.Count > 0, "選択肢が登録されていません");
		Qoo.Debug.Assert(SelectWnd.SelectNo >= 0 && SelectWnd.SelectNo < App.QooSelect.SelectAr.Count, "選択結果が選択肢の範囲に収まっていません");
		return App.QooSelect.SelectAr[SelectWnd.SelectNo].Ks;
	}

	// Token: 0x060005B4 RID: 1460 RVA: 0x00016F70 File Offset: 0x00015170
	public string GetSelectLabel()
	{
		Qoo.Debug.Assert(App.QooSelect.SelectAr.Count > 0, "選択肢が登録されていません");
		Qoo.Debug.Assert(SelectWnd.SelectNo >= 0 && SelectWnd.SelectNo < App.QooSelect.SelectAr.Count, "選択結果が選択肢の範囲に収まっていません");
		return App.QooSelect.SelectAr[SelectWnd.SelectNo].Label;
	}

	// Token: 0x060005B5 RID: 1461 RVA: 0x00016FE0 File Offset: 0x000151E0
	public bool IsSelectEnd()
	{
		return SelectWnd.SelectNo >= 0;
	}

	// Token: 0x04000422 RID: 1058
	private static List<SelectBtn> m_SelectBtn = new List<SelectBtn>();

	// Token: 0x04000423 RID: 1059
	private static int m_nSelectNo = -1;

	// Token: 0x04000424 RID: 1060
	private bool m_isAuto;

	// Token: 0x04000425 RID: 1061
	private UnitySprite m_LoveAnim;
}
