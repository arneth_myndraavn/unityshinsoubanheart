﻿using System;
using System.Collections.Generic;

// Token: 0x02000155 RID: 341
public class ManSprite
{
	// Token: 0x1700012B RID: 299
	// (get) Token: 0x06000994 RID: 2452 RVA: 0x0002A540 File Offset: 0x00028740
	public List<UnitySprite> SpriteList
	{
		get
		{
			return this.m_Array;
		}
	}

	// Token: 0x06000995 RID: 2453 RVA: 0x0002A548 File Offset: 0x00028748
	public void SetZ(float near_, float far_)
	{
		this.m_fNear = near_;
		this.m_fFar = far_;
	}

	// Token: 0x06000996 RID: 2454 RVA: 0x0002A558 File Offset: 0x00028758
	public void SetScreenSize(int screenW_, int screenH_)
	{
		this.m_nScreenW = screenW_;
		this.m_nScreenH = screenH_;
	}

	// Token: 0x06000997 RID: 2455 RVA: 0x0002A568 File Offset: 0x00028768
	public void Enable()
	{
		foreach (UnitySprite unitySprite in this.m_Array)
		{
			if (unitySprite.Show)
			{
				unitySprite.obj.SetActive(true);
			}
		}
	}

	// Token: 0x06000998 RID: 2456 RVA: 0x0002A5E0 File Offset: 0x000287E0
	public void Disable()
	{
		foreach (UnitySprite unitySprite in this.m_Array)
		{
			if (!unitySprite.Show)
			{
				unitySprite.obj.SetActive(false);
			}
		}
	}

	// Token: 0x06000999 RID: 2457 RVA: 0x0002A658 File Offset: 0x00028858
	public void Update()
	{
		this.Sort();
		float num = this.m_fFar - this.m_fNear;
		int num2 = 0;
		float num3 = this.m_fNear + num * 0.5f;
		foreach (UnitySprite unitySprite in this.m_Array)
		{
			unitySprite.Update(this.m_nScreenW, this.m_nScreenH, num3 + (float)num2);
			num2++;
		}
	}

	// Token: 0x0600099A RID: 2458 RVA: 0x0002A6F8 File Offset: 0x000288F8
	public UnitySprite Add(UnitySprite sprite)
	{
		this.m_Array.Add(sprite);
		return sprite;
	}

	// Token: 0x0600099B RID: 2459 RVA: 0x0002A708 File Offset: 0x00028908
	public UnitySprite Get(uint id)
	{
		return this.m_Array.Find((UnitySprite item) => item.id == id);
	}

	// Token: 0x0600099C RID: 2460 RVA: 0x0002A73C File Offset: 0x0002893C
	public bool Remove(UnitySprite sprite)
	{
		if (this.m_Array.Remove(sprite))
		{
			if (sprite is UnityTextSprite)
			{
				UnityTextSprite unityTextSprite = sprite as UnityTextSprite;
				unityTextSprite.Clear();
			}
			sprite.Reset();
			return true;
		}
		return false;
	}

	// Token: 0x0600099D RID: 2461 RVA: 0x0002A77C File Offset: 0x0002897C
	public void Sort()
	{
		this.m_Array.Sort((UnitySprite x, UnitySprite y) => (x.z <= y.z) ? ((x.z != y.z) ? 1 : 0) : -1);
	}

	// Token: 0x0600099E RID: 2462 RVA: 0x0002A7B4 File Offset: 0x000289B4
	public void ResetFx(int mask)
	{
		if ((mask & 2) != 0)
		{
			foreach (UnitySprite unitySprite in this.m_Array)
			{
				if ((unitySprite.Effect & 2) != 0)
				{
					unitySprite.Effect &= -3;
					unitySprite.Brend = SPRITE_DRAW_MODE.MUL;
				}
			}
		}
		if ((mask & 1) != 0)
		{
			foreach (UnitySprite unitySprite2 in this.m_Array)
			{
				if ((unitySprite2.Effect & 1) != 0)
				{
					unitySprite2.Effect &= -2;
					unitySprite2.SetFxPos(0, 0);
				}
			}
		}
	}

	// Token: 0x040007DB RID: 2011
	private List<UnitySprite> m_Array = new List<UnitySprite>();

	// Token: 0x040007DC RID: 2012
	private float m_fNear;

	// Token: 0x040007DD RID: 2013
	private float m_fFar;

	// Token: 0x040007DE RID: 2014
	private int m_nScreenW;

	// Token: 0x040007DF RID: 2015
	private int m_nScreenH;
}
