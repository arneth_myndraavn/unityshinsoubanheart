﻿using System;

// Token: 0x0200014E RID: 334
[Serializable]
public class BMSymbol
{
	// Token: 0x1700011D RID: 285
	// (get) Token: 0x0600094A RID: 2378 RVA: 0x00028898 File Offset: 0x00026A98
	public int length
	{
		get
		{
			if (this.mLength == 0)
			{
				this.mLength = this.sequence.Length;
			}
			return this.mLength;
		}
	}

	// Token: 0x040007BA RID: 1978
	public string sequence;

	// Token: 0x040007BB RID: 1979
	public int x;

	// Token: 0x040007BC RID: 1980
	public int y;

	// Token: 0x040007BD RID: 1981
	public int width;

	// Token: 0x040007BE RID: 1982
	public int height;

	// Token: 0x040007BF RID: 1983
	private int mLength;
}
