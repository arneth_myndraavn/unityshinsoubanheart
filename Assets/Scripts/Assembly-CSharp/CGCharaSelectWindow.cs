﻿using System;

// Token: 0x02000101 RID: 257
public class CGCharaSelectWindow : GalleryCharaSelectWindow
{
	// Token: 0x060006B9 RID: 1721 RVA: 0x0001C4A0 File Offset: 0x0001A6A0
	protected sealed override int GetCollect()
	{
		return CSVManager.Instance.CsvCGListHolder.AllCharaCollect;
	}

	// Token: 0x060006BA RID: 1722 RVA: 0x0001C4B4 File Offset: 0x0001A6B4
	protected sealed override void OnName()
	{
	}

	// Token: 0x060006BB RID: 1723 RVA: 0x0001C4B8 File Offset: 0x0001A6B8
	protected sealed override bool IsNameButton()
	{
		return false;
	}

	// Token: 0x060006BC RID: 1724 RVA: 0x0001C4BC File Offset: 0x0001A6BC
	protected sealed override string GetHeaderPath()
	{
		return "screen/cgmemory/cgm_head_cg";
	}

	// Token: 0x060006BD RID: 1725 RVA: 0x0001C4C4 File Offset: 0x0001A6C4
	protected sealed override string GetCharaButtonSceneName()
	{
		return UIValue.SCENE_CGSELECT;
	}
}
