﻿using System;
using System.IO;
using UnityEngine;

// Token: 0x02000172 RID: 370
public static class Pathing
{
	// Token: 0x17000157 RID: 343
	// (get) Token: 0x06000A89 RID: 2697 RVA: 0x0002E7C8 File Offset: 0x0002C9C8
	public static string appDataPath
	{
		get
		{
			return Application.dataPath;
		}
	}

	// Token: 0x17000158 RID: 344
	// (get) Token: 0x06000A8A RID: 2698 RVA: 0x0002E7D0 File Offset: 0x0002C9D0
	public static string appContentDataPath
	{
		get
		{
			string path = Application.streamingAssetsPath.TrimStart('/');
			if(Application.platform == RuntimePlatform.Android)
			{
				string url = path;
                //UnityEngine.Debug.LogWarning(url);
                return url;
            }
			else
			{
                return "file://localhost/" + path;
            }
            
		}
	}

	// Token: 0x17000159 RID: 345
	// (get) Token: 0x06000A8B RID: 2699 RVA: 0x0002E7DC File Offset: 0x0002C9DC
	public static Uri appContentDataUri
	{
		get
		{
			if (Application.platform == RuntimePlatform.IPhonePlayer)
			{
				return new UriBuilder
				{
					Scheme = "file",
					Path = Path.Combine(Pathing.appDataPath, "Raw")
				}.Uri;
			}
			if (Application.platform == RuntimePlatform.Android)
			{
				return new Uri("jar:file://" + Application.dataPath + "!/assets");
			}
			return new UriBuilder
			{
				Scheme = "file",
				Path = Path.Combine(Pathing.appDataPath, Pathing.STREAMING_ASSETS)
			}.Uri;
		}
	}

	// Token: 0x06000A8C RID: 2700 RVA: 0x0002E874 File Offset: 0x0002CA74
	public static string ToPlatformAssetBundleName(string fileName)
	{
		return fileName;
		//return fileName + Pathing.EXTENSION_ASSETBUNDLE_IOS;
	}

	// Token: 0x06000A8D RID: 2701 RVA: 0x0002E890 File Offset: 0x0002CA90
	public static string GetAppSandboxPathIPhone()
	{
		return Path.GetDirectoryName(Path.GetDirectoryName(Application.dataPath));
	}

	// Token: 0x06000A8E RID: 2702 RVA: 0x0002E8A4 File Offset: 0x0002CAA4
	public static string GetAppPersistencePathIPhone()
	{
		return Path.Combine(Pathing.GetAppSandboxPathIPhone(), "Documents");
	}

	// Token: 0x04000882 RID: 2178
	private static readonly string STREAMING_ASSETS = "StreamingAssets";

	// Token: 0x04000883 RID: 2179
	private static readonly string EXTENSION_ASSETBUNDLE = ".unity3d";

	// Token: 0x04000884 RID: 2180
	private static readonly string EXTENSION_ASSETBUNDLE_IOS = ".ios" + Pathing.EXTENSION_ASSETBUNDLE;
}
