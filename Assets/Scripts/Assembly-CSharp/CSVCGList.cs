﻿using System;
using Qoo.Game;

// Token: 0x02000162 RID: 354
public class CSVCGList
{
	// Token: 0x06000A2A RID: 2602 RVA: 0x0002D4EC File Offset: 0x0002B6EC
	public CSVCGList(string[] values)
	{
		int num = values.Length - 2;
		this.charaIndex = -1;
		this.charaId = values[0];
		this.thumbnail = values[1];
		this.cgs = new CSVCGList.CG[num];
		for (int i = 0; i < num; i++)
		{
			this.cgs[i] = new CSVCGList.CG(values[i + 2]);
		}
		this.collectCount = 0;
		this.collectTotal = num;
		this.charaIndex = CSVManager.calcCharaIndex(this.charaId);
	}

	// Token: 0x06000A2B RID: 2603 RVA: 0x0002D570 File Offset: 0x0002B770
	public void Update()
	{
		this.collectCount = 0;
		foreach (CSVCGList.CG cg in this.cgs)
		{
			cg.Update();
			if (cg.read)
			{
				this.collectCount++;
			}
		}
	}

	// Token: 0x0400084B RID: 2123
	public int charaIndex;

	// Token: 0x0400084C RID: 2124
	public string charaId;

	// Token: 0x0400084D RID: 2125
	public string thumbnail;

	// Token: 0x0400084E RID: 2126
	public CSVCGList.CG[] cgs;

	// Token: 0x0400084F RID: 2127
	public int collectCount;

	// Token: 0x04000850 RID: 2128
	public int collectTotal;

	// Token: 0x02000163 RID: 355
	public class CG
	{
		// Token: 0x06000A2C RID: 2604 RVA: 0x0002D5C4 File Offset: 0x0002B7C4
		public CG(string fpath_)
		{
			this.fpath = fpath_;
			this.read = false;
		}

		// Token: 0x06000A2D RID: 2605 RVA: 0x0002D5DC File Offset: 0x0002B7DC
		public void Update()
		{
            this.read = SysData.IsReadCG(this.fpath);
		}

		// Token: 0x04000851 RID: 2129
		public string fpath;

		// Token: 0x04000852 RID: 2130
		public bool read;
	}
}
