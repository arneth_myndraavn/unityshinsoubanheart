﻿using System;

// Token: 0x02000159 RID: 345
public enum SPRITE_FX_FLAG
{
	// Token: 0x040007F4 RID: 2036
	SHAKE = 1,
	// Token: 0x040007F5 RID: 2037
	FLASH
}
