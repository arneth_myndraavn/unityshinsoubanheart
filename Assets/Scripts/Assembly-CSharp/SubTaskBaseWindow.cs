﻿using System;

// Token: 0x020001AB RID: 427
public abstract class SubTaskBaseWindow : BaseWindow
{
	// Token: 0x170001A9 RID: 425
	// (get) Token: 0x06000C5A RID: 3162 RVA: 0x00032D10 File Offset: 0x00030F10
	// (set) Token: 0x06000C5B RID: 3163 RVA: 0x00032D18 File Offset: 0x00030F18
	public bool IsTaskEnd { get; set; }
}
