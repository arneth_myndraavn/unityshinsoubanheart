﻿using Qoo.Application;
using Qoo.Input;
using Qoo.Message;
using Qoo.SoundSystem;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x020000A9 RID: 169
public class CBackLogWnd : UserBaseClass
{
	// Token: 0x060004FD RID: 1277 RVA: 0x000126D8 File Offset: 0x000108D8
	private void Awake()
	{
		KsInput.Pause = false;
		this.m_aLine.Capacity = 512;
		this.m_aMes.Capacity = 64;
		for (int num = 0; num != this.m_sp.Length; num++)
		{
			this.m_sp[num] = Man2D.Sprite(string.Empty);
		}
		for (int num2 = 0; num2 != this.m_txt.Length; num2++)
		{
			this.m_txt[num2] = Man2D.TextSprite(null);
		}
		UnitySprite unitySprite = this.m_sp[0];
		unitySprite.x = 0;
		unitySprite.y = 0;
		unitySprite.w = 960;
		unitySprite.h = 544;
		unitySprite.z = 1300;
		unitySprite.SetColor(new Color32(0, 0, 0, 192));
		unitySprite = this.m_sp[1];
		unitySprite.SetImage(Man2D.Texture("cancel"));
		unitySprite.x = 902;
		unitySprite.y = 5;
		unitySprite.z = 1302;
		unitySprite.SetPtnNum(1, 2);
		unitySprite.SetPtnPos(0, 0);
		unitySprite.ResetUVAnim();
		unitySprite.SetUVAnimWait(1);
		for (int i = 0; i < 16; i++)
		{
			UnityTextSprite unityTextSprite = this.m_txt[0 + i];
			unityTextSprite.x = 0;
			unityTextSprite.y = 4 + i * 32;
			unityTextSprite.z = 1301;
			unityTextSprite.SetColor(new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue));
		}
		for (int j = 0; j < 16; j++)
		{
			UnityTextSprite unityTextSprite = this.m_txt[16 + j];
			unityTextSprite.x = 64;
			unityTextSprite.y = 4 + j * 32;
			unityTextSprite.z = 1301;
			unityTextSprite.SetColor(new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue));
			unityTextSprite.SetTextColor(new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue));
            if (!UnityApp.PlatformApp.isJapanese)
			{
				UnityEngine.Debug.LogWarning(UnityApp.PlatformApp.preferredFontSize);
                unityTextSprite.AddText("♪", UnityApp.PlatformApp.preferredFontSize);
            }
            else
			{
                unityTextSprite.AddText("♪", 28);
            }
            
			unityTextSprite.Show = false;
		}
		this.InitLog();
		this.m_nDispLine = this.m_nScrollMax;
		this.m_nCursor = this.m_nMesNum - 1;
		this.m_nScrollPos = this.m_nDispLine * 32;
		this.UpdateText();
		Sound.SeSlotStopAll();
		if (this.m_bFadeIn)
		{
		}
		this.m_bUpdate = false;
		this.m_bVoiceStop = false;
		this.m_nOldDelta = 0;
	}

	// Token: 0x060004FE RID: 1278 RVA: 0x00012950 File Offset: 0x00010B50
	private void OnDestroy()
	{
		if (Singleton<Man2D>.IsReady)
		{
			if (this.m_sp != null)
			{
				for (int num = 0; num != this.m_sp.Length; num++)
				{
					if (this.m_sp[num] != null)
					{
						Singleton<Man2D>.Instance.RemoveSprite(this.m_sp[num]);
					}
					this.m_sp[num] = null;
				}
			}
			if (this.m_txt != null)
			{
				for (int num2 = 0; num2 != this.m_txt.Length; num2++)
				{
					if (this.m_txt[num2] != null)
					{
						Singleton<Man2D>.Instance.RemoveSprite(this.m_txt[num2]);
					}
					this.m_txt[num2] = null;
				}
			}
		}
		KsInput.Pause = true;
		if (Singleton<UnityApp>.IsReady)
		{
			UnityApp.Input.IsOnePushDelete = true;
		}
	}

	// Token: 0x060004FF RID: 1279 RVA: 0x00012A1C File Offset: 0x00010C1C
	private void Update()
	{
		if (!this.m_bUpdate)
		{
			this.m_bUpdate = true;
			return;
		}
		switch (UnityApp.Input.Status)
		{
		case INPUT_STATUS.PUSH:
			this.ResetAutoScroll();
			this.m_nOldDelta = 0;
			if (this.m_sp[1].IsHit(UnityApp.Input.Start))
			{
				this.m_sp[1].EnableUVAnim(true);
			}
			goto IL_1CF;
		case INPUT_STATUS.CLICK:
			if (this.m_sp[1].IsHit(UnityApp.Input.End))
			{
				Sound.SE_Cancel();
				if (this.m_bVoiceStop)
				{
					Sound.VoiceStop();
				}
				if (this.m_bFadeOut)
				{
				}
				KsInput.ClearMenu();
				UnityEngine.Object.Destroy(this);
				base.enabled = false;
			}
			else
			{
				this.SelectItem(UnityApp.Input.End);
			}
			goto IL_1CF;
		case INPUT_STATUS.DRAG_ON:
			if (this.m_nOldDelta != UnityApp.Input.Delta.y)
			{
				this.Move(-(this.m_nOldDelta - UnityApp.Input.Delta.y));
				this.m_nOldDelta = UnityApp.Input.Delta.y;
			}
			goto IL_1CF;
		case INPUT_STATUS.FLICK:
		{
			this.m_nOldDelta = 0;
			Qoo.Debug.Print("FLICK START");
			this.m_bAutoScroll = true;
			SLIDE_VECTOR slide = UnityApp.Input.Slide;
			if (slide != SLIDE_VECTOR.UP)
			{
				if (slide != SLIDE_VECTOR.DOWN)
				{
					this.m_bAutoScroll = false;
				}
				else
				{
					this.m_nScrollSpeed = -32;
				}
			}
			else
			{
				this.m_nScrollSpeed = 32;
			}
			goto IL_1CF;
		}
		}
		this.m_nOldDelta = 0;
		this.m_sp[1].SetPtnPos(0, 0);
		this.m_sp[1].ResetUVAnim();
		this.m_sp[1].EnableUVAnim(false);
		IL_1CF:
		if (this.m_bAutoScroll && !this.Move(this.m_nScrollSpeed))
		{
			this.ResetAutoScroll();
		}
	}

	// Token: 0x06000500 RID: 1280 RVA: 0x00012C1C File Offset: 0x00010E1C
	private bool SelectItem(Point2 pos)
	{
		int num = this.SearchItem(pos);
		Qoo.Debug.Print(string.Format("SelectItem={0}", num));
		if (this.m_nCursor == num)
		{
			this.PlayCurItem();
			return true;
		}
		if (num >= 0)
		{
			this.m_nCursor = num;
			this.UpdateText();
			this.PlayCurItem();
			return true;
		}
		return false;
	}

	// Token: 0x06000501 RID: 1281 RVA: 0x00012C78 File Offset: 0x00010E78
	private void PlayCurItem()
	{
		CBackLogWnd.MESDATA mesdata = this.m_aMes[this.m_nCursor];
		if (mesdata.pszVoice.Length > 0)
		{
			Sound.VoicePlay(mesdata.pszVoice, 1f);
			this.m_bVoiceStop = true;
		}
	}

	// Token: 0x06000502 RID: 1282 RVA: 0x00012CC4 File Offset: 0x00010EC4
	private int SearchItem(Point2 pos)
	{
		Qoo.Debug.Print(string.Format("point x={0} y={1}", pos.x, pos.y));
		if (128 <= pos.x)
		{
			for (int num = 0; num != this.m_txt.Length; num++)
			{
				if (this.m_txt[num] != null && this.m_txt[num].Show)
				{
					this.m_txt[num].CalcSize();
					if (this.m_txt[num].w != 0 || this.m_txt[num].h != 0)
					{
						Point2 point = new Point2(this.m_txt[num].x, this.m_txt[num].y);
						Point2 point2 = new Point2(point.x + 960, point.y + 32);
						if (point.x <= pos.x && pos.x < point2.x && point.y <= pos.y && pos.y < point2.y)
						{
							int num2 = num;
							if (num >= 16)
							{
								num2 -= 16;
							}
							for (int num3 = 0; num3 != this.m_aMes.Count; num3++)
							{
								CBackLogWnd.MESDATA mesdata = this.m_aMes[num3];
								if (this.m_nDispLine + num2 >= mesdata.nTop && this.m_nDispLine + num2 < mesdata.nTop + mesdata.nLineNum)
								{
									Qoo.Debug.Print(string.Format("HIT={0} MESNo={1} x={2} y={3}", new object[]
									{
										num,
										num3,
										pos.x,
										pos.y
									}));
									return num3;
								}
							}
							return -1;
						}
					}
				}
			}
		}
		return -1;
	}

	// Token: 0x06000503 RID: 1283 RVA: 0x00012EAC File Offset: 0x000110AC
	private void ResetAutoScroll()
	{
		this.m_bAutoScroll = false;
		this.m_nScrollSpeed = 0;
	}

	// Token: 0x06000504 RID: 1284 RVA: 0x00012EBC File Offset: 0x000110BC
	private bool Move(int addY)
	{
		if (this.m_nScrollMax == 0)
		{
			return false;
		}
		if (this.m_aLine.Count < 16)
		{
			return false;
		}
		if (addY > 0 && this.m_nDispLine + 16 >= this.m_aLine.Count)
		{
			return false;
		}
		if (addY < 0 && this.m_nDispLine == 0)
		{
			return false;
		}
		this.SetScroll(this.m_nScrollPos + addY);
		return true;
	}

	// Token: 0x06000505 RID: 1285 RVA: 0x00012F30 File Offset: 0x00011130
	private bool SetUp(bool bFadeIn = false, bool bFadeOut = false)
	{
		this.m_bFadeIn = bFadeIn;
		this.m_bFadeOut = bFadeOut;
		return true;
	}

	// Token: 0x06000506 RID: 1286 RVA: 0x00012F44 File Offset: 0x00011144
	private void InitLog()
	{
		this.m_aLine.Clear();
		this.m_aMes.Clear();
		this.m_nMesNum = 0;
		this.m_nLineNum = 0;
		BACKLOG_ITEM[] renge = App.QooBackLog.GetRenge(64);
		this.m_nMesNum = renge.Length;
		bool flag = false;
		for (int i = 0; i < this.m_nMesNum; i++)
		{
			BACKLOG_ITEM backlog_ITEM = renge[i];
			CBackLogWnd.MESDATA item = default(CBackLogWnd.MESDATA);
			item.nLineNum = 0;
			if (backlog_ITEM.Name.Length > 0 && flag)
			{
				this.AddSpaceLine(i);
			}
			item.nTop = this.m_nLineNum;
			item.pszVoice = backlog_ITEM.Voice;
			this.AddNameLine(i, backlog_ITEM);
			item.nLineNum++;
			string text = string.Empty;
			foreach (char c in backlog_ITEM.Txt)
			{
				if (c == '\n')
				{
					this.AddMesLine(i, text);
					item.nLineNum++;
					text = string.Empty;
				}
				else
				{
					text += c;
				}
			}
			if (text.Length > 0)
			{
				this.AddMesLine(i, text);
				item.nLineNum++;
			}
			this.m_aMes.Add(item);
			flag = true;
		}
		this.m_nScrollMax = ((16 >= this.m_nLineNum) ? 0 : (this.m_nLineNum - 16));
	}

	// Token: 0x06000507 RID: 1287 RVA: 0x000130D4 File Offset: 0x000112D4
	private void UpdateText()
	{
		CBackLogWnd.MESDATA mesdata = this.m_aMes[this.m_nCursor];
		for (int i = 0; i < 16; i++)
		{
			UnityTextSprite unityTextSprite = this.m_txt[0 + i];
			UnityTextSprite unityTextSprite2 = this.m_txt[16 + i];
			if (this.m_nDispLine + i < this.m_nLineNum)
			{
				CBackLogWnd.LINEDATA linedata = this.m_aLine[this.m_nDispLine + i];
				if (linedata.nType == CBackLogWnd.LINETYPE.LINETYPE_SPACE)
				{
					unityTextSprite.Show = false;
					unityTextSprite2.Show = false;
				}
				else
				{
					unityTextSprite.ClearText();
					unityTextSprite2.ClearText();
					if (this.m_nDispLine + i >= mesdata.nTop && this.m_nDispLine + i < mesdata.nTop + mesdata.nLineNum)
					{
						unityTextSprite.SetTextColor(new Color32(byte.MaxValue, byte.MaxValue, 0, byte.MaxValue));
						unityTextSprite2.SetTextColor(new Color32(byte.MaxValue, 0, 0, byte.MaxValue));
						Qoo.Debug.Print(string.Format("Change Color ={0}", i));
					}
					else
					{
						unityTextSprite.SetTextColor(new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue));
						unityTextSprite2.SetTextColor(new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue));
					}
					if (!UnityApp.PlatformApp.isJapanese)
					{
                        UnityEngine.Debug.LogWarning(UnityApp.PlatformApp.preferredFontSize);
                        unityTextSprite.AddText(linedata.pszTxt, UnityApp.PlatformApp.preferredFontSize);
                        unityTextSprite2.AddText("♪", UnityApp.PlatformApp.preferredFontSize);
                        
                    }
					else
					{
                        unityTextSprite.AddText(linedata.pszTxt, 28);
                        unityTextSprite2.AddText("♪", 28);
                    }
					if (linedata.nType == CBackLogWnd.LINETYPE.LINETYPE_MES)
					{
						unityTextSprite.x = 204;
					}
					else
					{
						unityTextSprite.x = 128;
					}
					unityTextSprite.Show = true;
					CBackLogWnd.MESDATA mesdata2 = this.m_aMes[linedata.nNo];
					if (linedata.nType == CBackLogWnd.LINETYPE.LINETYPE_NAME && mesdata2.pszVoice.Length > 0)
					{
						unityTextSprite2.Show = true;
					}
					else
					{
						unityTextSprite2.Show = false;
					}
					UnitySprite unitySprite = unityTextSprite;
					int y = -this.m_nMoveY + 4 + i * 32;
					unityTextSprite2.y = y;
					unitySprite.y = y;
				}
			}
			else
			{
				unityTextSprite.Show = false;
				unityTextSprite2.Show = false;
			}
		}
	}

	// Token: 0x06000508 RID: 1288 RVA: 0x000132F0 File Offset: 0x000114F0
	private void SetScroll(int nPos)
	{
		if (nPos < 0)
		{
			if (this.m_nScrollPos == 0)
			{
				return;
			}
			nPos = 0;
		}
		if (nPos / 32 + 16 >= this.m_aLine.Count)
		{
			if (this.m_nDispLine + 16 == this.m_aLine.Count)
			{
				return;
			}
			nPos = (this.m_aLine.Count - 16) * 32;
		}
		this.m_nScrollPos = nPos;
		this.m_nMoveY = nPos % 32;
		this.m_nDispLine = this.m_nScrollPos / 32;
		this.UpdateText();
	}

	// Token: 0x06000509 RID: 1289 RVA: 0x00013380 File Offset: 0x00011580
	private void MoveCursor(int nCur, int nAlign)
	{
		int nCursor = this.m_nCursor;
		int nDispLine = this.m_nDispLine;
		if (nAlign == 0)
		{
			if (this.m_aMes[this.m_nCursor].nTop < this.m_nDispLine)
			{
				this.SetScroll(this.m_aMes[this.m_nCursor].nTop * 32);
			}
		}
		else if (this.m_aMes[this.m_nCursor].nTop + this.m_aMes[this.m_nCursor].nLineNum > this.m_nDispLine + 16)
		{
			this.SetScroll((this.m_aMes[this.m_nCursor].nTop - (16 - this.m_aMes[this.m_nCursor].nLineNum)) * 32);
		}
		if (nDispLine != this.m_nDispLine || nCursor != this.m_nCursor)
		{
			Sound.SE_Select();
			this.UpdateText();
		}
	}

	// Token: 0x0600050A RID: 1290 RVA: 0x00013494 File Offset: 0x00011694
	private void AddSpaceLine(int i)
	{
		CBackLogWnd.LINEDATA item = default(CBackLogWnd.LINEDATA);
		item.nNo = i;
		item.nType = CBackLogWnd.LINETYPE.LINETYPE_SPACE;
		item.pszTxt = string.Empty;
		this.m_aLine.Add(item);
		this.m_nLineNum++;
	}

	// Token: 0x0600050B RID: 1291 RVA: 0x000134E0 File Offset: 0x000116E0
	private void AddNameLine(int i, BACKLOG_ITEM item)
	{
		CBackLogWnd.LINEDATA item2 = default(CBackLogWnd.LINEDATA);
		item2.nNo = i;
		item2.nType = CBackLogWnd.LINETYPE.LINETYPE_NAME;
		item2.pszTxt = item.Name;
		this.m_aLine.Add(item2);
		this.m_nLineNum++;
	}

	// Token: 0x0600050C RID: 1292 RVA: 0x0001352C File Offset: 0x0001172C
	private void AddMesLine(int i, string line_str)
	{
		CBackLogWnd.LINEDATA item = default(CBackLogWnd.LINEDATA);
		item.nNo = i;
		item.nType = CBackLogWnd.LINETYPE.LINETYPE_MES;
		item.pszTxt = line_str;
		this.m_aLine.Add(item);
		this.m_nLineNum++;
	}

	// Token: 0x0400038C RID: 908
	private const int TEXTBUFFSIZE = 65536;

	// Token: 0x0400038D RID: 909
	private const int MES_MAX = 64;

	// Token: 0x0400038E RID: 910
	private const int LINE_MAX = 512;

	// Token: 0x0400038F RID: 911
	private const int DISPLINE_NUM = 16;

	// Token: 0x04000390 RID: 912
	private const int Z_BG = 1300;

	// Token: 0x04000391 RID: 913
	private const int Z_TEXT = 1301;

	// Token: 0x04000392 RID: 914
	private const int LINE_HEIGHT = 32;

	// Token: 0x04000393 RID: 915
	private UnitySprite[] m_sp = new UnitySprite[2];

	// Token: 0x04000394 RID: 916
	private UnityTextSprite[] m_txt = new UnityTextSprite[32];

	// Token: 0x04000395 RID: 917
	private List<CBackLogWnd.MESDATA> m_aMes = new List<CBackLogWnd.MESDATA>();

	// Token: 0x04000396 RID: 918
	private int m_nMesNum;

	// Token: 0x04000397 RID: 919
	private List<CBackLogWnd.LINEDATA> m_aLine = new List<CBackLogWnd.LINEDATA>();

	// Token: 0x04000398 RID: 920
	private int m_nLineNum;

	// Token: 0x04000399 RID: 921
	private int m_nDispLine;

	// Token: 0x0400039A RID: 922
	private int m_nScrollMax;

	// Token: 0x0400039B RID: 923
	private int m_nCursor;

	// Token: 0x0400039C RID: 924
	private bool m_bUpdate;

	// Token: 0x0400039D RID: 925
	private bool m_bFadeIn;

	// Token: 0x0400039E RID: 926
	private bool m_bFadeOut;

	// Token: 0x0400039F RID: 927
	private bool m_bVoiceStop;

	// Token: 0x040003A0 RID: 928
	private int m_nScrollPos;

	// Token: 0x040003A1 RID: 929
	private int m_nMoveY;

	// Token: 0x040003A2 RID: 930
	private int m_nOldDelta;

	// Token: 0x040003A3 RID: 931
	private bool m_bAutoScroll;

	// Token: 0x040003A4 RID: 932
	private int m_nScrollSpeed;

	// Token: 0x020000AA RID: 170
	private enum SP_NO
	{
		// Token: 0x040003A6 RID: 934
		SP_FILTER,
		// Token: 0x040003A7 RID: 935
		SP_END_BTN,
		// Token: 0x040003A8 RID: 936
		SP_MAX
	}

	// Token: 0x020000AB RID: 171
	private enum SPTEXT_NO
	{
		// Token: 0x040003AA RID: 938
		TXT_LINE_TOP,
		// Token: 0x040003AB RID: 939
		TXT_LINE_END = 15,
		// Token: 0x040003AC RID: 940
		TXT_VOICEMARK_TOP,
		// Token: 0x040003AD RID: 941
		TXT_VOICEMARK_END = 31,
		// Token: 0x040003AE RID: 942
		TXT_MAX
	}

	// Token: 0x020000AC RID: 172
	private enum LINETYPE
	{
		// Token: 0x040003B0 RID: 944
		LINETYPE_MES,
		// Token: 0x040003B1 RID: 945
		LINETYPE_NAME,
		// Token: 0x040003B2 RID: 946
		LINETYPE_SPACE
	}

	// Token: 0x020000AD RID: 173
	private struct LINEDATA
	{
		// Token: 0x040003B3 RID: 947
		public int nNo;

		// Token: 0x040003B4 RID: 948
		public CBackLogWnd.LINETYPE nType;

		// Token: 0x040003B5 RID: 949
		public string pszTxt;
	}

	// Token: 0x020000AE RID: 174
	private struct MESDATA
	{
		// Token: 0x040003B6 RID: 950
		public int nTop;

		// Token: 0x040003B7 RID: 951
		public int nLineNum;

		// Token: 0x040003B8 RID: 952
		public string pszVoice;
	}
}
