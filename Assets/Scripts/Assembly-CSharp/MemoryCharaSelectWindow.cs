﻿using System;

// Token: 0x0200011B RID: 283
public class MemoryCharaSelectWindow : GalleryCharaSelectWindow
{
	// Token: 0x06000783 RID: 1923 RVA: 0x000204C0 File Offset: 0x0001E6C0
	protected sealed override int GetCollect()
	{
		return CSVManager.Instance.CsvSceneMemoryHolder.AllCharaCollect;
	}

	// Token: 0x06000784 RID: 1924 RVA: 0x000204D4 File Offset: 0x0001E6D4
	protected sealed override void OnName()
	{
		SceneManager.ChangeScene(UIValue.SCENE_GALLERYINPUTNAME);
	}

	// Token: 0x06000785 RID: 1925 RVA: 0x000204E0 File Offset: 0x0001E6E0
	protected sealed override bool IsNameButton()
	{
		return true;
	}

	// Token: 0x06000786 RID: 1926 RVA: 0x000204E4 File Offset: 0x0001E6E4
	protected sealed override string GetHeaderPath()
	{
		return "screen/cgmemory/cgm_head_mem";
	}

	// Token: 0x06000787 RID: 1927 RVA: 0x000204EC File Offset: 0x0001E6EC
	protected sealed override string GetCharaButtonSceneName()
	{
		return UIValue.SCENE_MEMORYSELECT;
	}
}
