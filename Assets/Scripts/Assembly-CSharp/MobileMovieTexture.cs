﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;

// Token: 0x0200013A RID: 314
public class MobileMovieTexture : MonoBehaviour
{
	// Token: 0x14000001 RID: 1
	// (add) Token: 0x0600085C RID: 2140 RVA: 0x000259D0 File Offset: 0x00023BD0
	// (remove) Token: 0x0600085D RID: 2141 RVA: 0x000259EC File Offset: 0x00023BEC
	public event MobileMovieTexture.OnFinished onFinished;

	// Token: 0x170000EC RID: 236
	// (get) Token: 0x0600085E RID: 2142 RVA: 0x00025A08 File Offset: 0x00023C08
	public string Path
	{
		get
		{
			return this.m_path;
		}
	}

	// Token: 0x170000ED RID: 237
	// (set) Token: 0x0600085F RID: 2143 RVA: 0x00025A10 File Offset: 0x00023C10
	public bool playAutomatically
	{
		set
		{
			this.m_playAutomatically = value;
		}
	}

	// Token: 0x170000EE RID: 238
	// (get) Token: 0x06000860 RID: 2144 RVA: 0x00025A1C File Offset: 0x00023C1C
	public Material MovieMaterial
	{
		get
		{
			return this.m_movieMaterial;
		}
	}

	// Token: 0x170000EF RID: 239
	// (get) Token: 0x06000861 RID: 2145 RVA: 0x00025A24 File Offset: 0x00023C24
	public int width
	{
		get
		{
			return this.m_picWidth;
		}
	}

	// Token: 0x170000F0 RID: 240
	// (get) Token: 0x06000862 RID: 2146 RVA: 0x00025A2C File Offset: 0x00023C2C
	public int height
	{
		get
		{
			return this.m_picHeight;
		}
	}

	// Token: 0x170000F1 RID: 241
	// (get) Token: 0x06000863 RID: 2147 RVA: 0x00025A34 File Offset: 0x00023C34
	public float aspectRatio
	{
		get
		{
			if (this.m_nativeContext != IntPtr.Zero)
			{
				return MobileMovieTexture.GetAspectRatio(this.m_nativeContext);
			}
			return 1f;
		}
	}

	// Token: 0x170000F2 RID: 242
	// (get) Token: 0x06000864 RID: 2148 RVA: 0x00025A68 File Offset: 0x00023C68
	public bool isPlaying
	{
		get
		{
			return this.m_nativeContext != IntPtr.Zero && !this.m_hasFinished && this.m_advance;
		}
	}

	// Token: 0x170000F3 RID: 243
	// (get) Token: 0x06000865 RID: 2149 RVA: 0x00025A94 File Offset: 0x00023C94
	// (set) Token: 0x06000866 RID: 2150 RVA: 0x00025AA0 File Offset: 0x00023CA0
	public bool pause
	{
		get
		{
			return !this.m_advance;
		}
		set
		{
			this.m_advance = !value;
		}
	}

	// Token: 0x06000867 RID: 2151
	[DllImport("__Internal")]
	private static extern IntPtr OpenStream(string path, int offset, int size, bool loop, bool pot);

	// Token: 0x06000868 RID: 2152
	[DllImport("__Internal")]
	private static extern int GetPicWidth(IntPtr context);

	// Token: 0x06000869 RID: 2153
	[DllImport("__Internal")]
	private static extern int GetPicHeight(IntPtr context);

	// Token: 0x0600086A RID: 2154
	[DllImport("__Internal")]
	private static extern int GetPicX(IntPtr context);

	// Token: 0x0600086B RID: 2155
	[DllImport("__Internal")]
	private static extern int GetPicY(IntPtr context);

	// Token: 0x0600086C RID: 2156
	[DllImport("__Internal")]
	private static extern bool DecodeFrame(IntPtr context);

	// Token: 0x0600086D RID: 2157
	[DllImport("__Internal")]
	private static extern int GetYStride(IntPtr context);

	// Token: 0x0600086E RID: 2158
	[DllImport("__Internal")]
	private static extern int GetYHeight(IntPtr context);

	// Token: 0x0600086F RID: 2159
	[DllImport("__Internal")]
	private static extern int GetUVStride(IntPtr context);

	// Token: 0x06000870 RID: 2160
	[DllImport("__Internal")]
	private static extern int GetUVHeight(IntPtr context);

	// Token: 0x06000871 RID: 2161
	[DllImport("__Internal")]
	private static extern bool DecodeAndUpload(IntPtr context, int yHandle, int uHandle, int vHandle);

	// Token: 0x06000872 RID: 2162
	[DllImport("__Internal")]
	private static extern void GetPlane(IntPtr context, int planeIndex, IntPtr buffer);

	// Token: 0x06000873 RID: 2163
	[DllImport("__Internal")]
	private static extern double GetNextFrameTime(IntPtr context);

	// Token: 0x06000874 RID: 2164
	[DllImport("__Internal")]
	private static extern void CloseStream(IntPtr context);

	// Token: 0x06000875 RID: 2165
	[DllImport("__Internal")]
	private static extern float GetAspectRatio(IntPtr context);

	// Token: 0x06000876 RID: 2166 RVA: 0x00025AAC File Offset: 0x00023CAC
	private void Start()
	{
		if (this.m_playAutomatically)
		{
			this.Play();
		}
	}

	// Token: 0x06000877 RID: 2167 RVA: 0x00025AC0 File Offset: 0x00023CC0
	public void Play()
	{
		this.Close();
		this.m_elapsedTime = 0.0;
		this.m_nextFrameTime = 0.0;
		this.Open();
		this.m_advance = true;
		this.m_hasFinished = false;
	}

	// Token: 0x06000878 RID: 2168 RVA: 0x00025B08 File Offset: 0x00023D08
	public void Stop()
	{
		this.Close();
	}

	// Token: 0x06000879 RID: 2169 RVA: 0x00025B10 File Offset: 0x00023D10
	private void Open()
	{
		bool flag = Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer;
		bool pot = !flag;
		RuntimePlatform platform = Application.platform;
		string path;
		long num;
		long num2;
		if (platform != RuntimePlatform.Android)
		{
			path = Application.streamingAssetsPath + "/" + this.m_path;
			num = 0L;
			num2 = 0L;
		}
		else
		{
			path = Application.dataPath;
			if (!AssetStream.GetZipFileOffsetLength(Application.dataPath, this.m_path, out num, out num2))
			{
				throw new Exception("problem opening movie");
			}
		}
		this.m_nativeContext = MobileMovieTexture.OpenStream(path, (int)num, (int)num2, this.m_loop, pot);
		if (this.m_nativeContext != IntPtr.Zero)
		{
			this.m_picWidth = MobileMovieTexture.GetPicWidth(this.m_nativeContext);
			this.m_picHeight = MobileMovieTexture.GetPicHeight(this.m_nativeContext);
			this.m_picX = MobileMovieTexture.GetPicX(this.m_nativeContext);
			this.m_picY = MobileMovieTexture.GetPicY(this.m_nativeContext);
			int ystride = MobileMovieTexture.GetYStride(this.m_nativeContext);
			int yheight = MobileMovieTexture.GetYHeight(this.m_nativeContext);
			int uvstride = MobileMovieTexture.GetUVStride(this.m_nativeContext);
			int uvheight = MobileMovieTexture.GetUVHeight(this.m_nativeContext);
			if (ystride != this.m_yStride || yheight != this.m_yHeight || uvstride != this.m_uvStride || uvheight != this.m_uvHeight)
			{
				this.m_yStride = ystride;
				this.m_yHeight = yheight;
				this.m_uvStride = uvstride;
				this.m_uvHeight = uvheight;
				this.DestroyTextures();
				this.AllocateTexures();
			}
			this.Decode(true);
			this.m_nextFrameTime = MobileMovieTexture.GetNextFrameTime(this.m_nativeContext);
			return;
		}
		throw new Exception("problem opening movie");
	}

	// Token: 0x0600087A RID: 2170 RVA: 0x00025CD0 File Offset: 0x00023ED0
	private void AllocateTexures()
	{
		this.m_ChannelTextures = new ChannelTexture[3];
		TextureFormat format = TextureFormat.Alpha8;
		this.m_ChannelTextures[0] = new ChannelTexture(this.m_yStride, this.m_yHeight, format);
		this.m_ChannelTextures[1] = new ChannelTexture(this.m_uvStride, this.m_uvHeight, format);
		this.m_ChannelTextures[2] = new ChannelTexture(this.m_uvStride, this.m_uvHeight, format);
		Vector2 scale = new Vector2((float)this.m_picWidth / (float)this.m_yStride, -((float)this.m_picHeight / (float)this.m_yHeight));
		Vector2 vector = new Vector2((float)this.m_picX / (float)this.m_yStride, ((float)this.m_picHeight + (float)this.m_picY) / (float)this.m_yHeight);
		Vector2 scale2 = default(Vector2);
		Vector2 offset = default(Vector2);
		if (this.m_uvStride == this.m_yStride)
		{
			scale2.x = scale.x;
		}
		else
		{
			scale2.x = (float)this.m_picWidth / 2f / (float)this.m_uvStride;
		}
		if (this.m_uvHeight == this.m_yHeight)
		{
			scale2.y = scale.y;
			offset = vector;
		}
		else
		{
			scale2.y = -((float)this.m_picHeight / 2f / (float)this.m_uvHeight);
			offset = new Vector2((float)this.m_picX / 2f / (float)this.m_uvStride, ((float)this.m_picHeight + (float)this.m_picY) / 2f / (float)this.m_uvHeight);
		}
		this.m_movieMaterial.SetTexture("_YTex", this.m_ChannelTextures[0].tex);
		this.m_movieMaterial.SetTexture("_CrTex", this.m_ChannelTextures[1].tex);
		this.m_movieMaterial.SetTexture("_CbTex", this.m_ChannelTextures[2].tex);
		this.m_movieMaterial.SetTextureScale("_YTex", scale);
		this.m_movieMaterial.SetTextureOffset("_YTex", vector);
		this.m_movieMaterial.SetTextureScale("_CbTex", scale2);
		this.m_movieMaterial.SetTextureOffset("_CbTex", offset);
	}

	// Token: 0x0600087B RID: 2171 RVA: 0x00025EF8 File Offset: 0x000240F8
	private void DestroyTextures()
	{
		this.m_reallocTextures = false;
		if (this.m_ChannelTextures != null)
		{
			for (int i = 0; i < 3; i++)
			{
				if (this.m_ChannelTextures[i] != null)
				{
					this.m_ChannelTextures[i].Destroy();
				}
			}
		}
		this.m_ChannelTextures = null;
	}

	// Token: 0x0600087C RID: 2172 RVA: 0x00025F4C File Offset: 0x0002414C
	private void Close()
	{
		if (this.m_nativeContext != IntPtr.Zero)
		{
			MobileMovieTexture.CloseStream(this.m_nativeContext);
			this.m_nativeContext = IntPtr.Zero;
		}
	}

	// Token: 0x0600087D RID: 2173 RVA: 0x00025F7C File Offset: 0x0002417C
	private void OnDestroy()
	{
		this.Close();
		this.DestroyTextures();
	}

	// Token: 0x0600087E RID: 2174 RVA: 0x00025F8C File Offset: 0x0002418C
	private void OnApplicationPause(bool pause)
	{
		if (!pause)
		{
			this.m_reallocTextures = true;
		}
	}

	// Token: 0x0600087F RID: 2175 RVA: 0x00025F9C File Offset: 0x0002419C
	private void Update()
	{
		if (this.m_reallocTextures && this.m_ChannelTextures != null)
		{
			for (int i = 0; i < 3; i++)
			{
				this.m_ChannelTextures[i].ReAlloc();
			}
			this.m_reallocTextures = false;
		}
		if (this.m_nativeContext != IntPtr.Zero && !this.m_hasFinished && this.m_advance)
		{
			this.m_elapsedTime += (double)Time.deltaTime;
			while (this.m_elapsedTime > this.m_nextFrameTime)
			{
				bool upload = false;
				if (this.m_nextFrameTime + this.m_lastFrameDelta >= this.m_elapsedTime)
				{
					upload = true;
				}
				this.Decode(upload);
				double nextFrameTime = MobileMovieTexture.GetNextFrameTime(this.m_nativeContext);
				if (nextFrameTime == this.m_nextFrameTime)
				{
					this.m_hasFinished = true;
					if (this.onFinished != null)
					{
						this.onFinished(this);
					}
					break;
				}
				this.m_lastFrameDelta = nextFrameTime - this.m_nextFrameTime;
				this.m_nextFrameTime = nextFrameTime;
			}
		}
	}

	// Token: 0x06000880 RID: 2176 RVA: 0x000260AC File Offset: 0x000242AC
	private void Decode(bool upload)
	{
		if (upload)
		{
			MobileMovieTexture.DecodeAndUpload(this.m_nativeContext, this.m_ChannelTextures[0].tex.GetNativeTextureID(), this.m_ChannelTextures[1].tex.GetNativeTextureID(), this.m_ChannelTextures[2].tex.GetNativeTextureID());
			GL.InvalidateState();
		}
		else
		{
			MobileMovieTexture.DecodeAndUpload(this.m_nativeContext, -1, -1, -1);
		}
	}

	// Token: 0x04000750 RID: 1872
	private const int CHANNELS = 3;

	// Token: 0x04000751 RID: 1873
	private const string PLATFORM_DLL = "__Internal";

	// Token: 0x04000752 RID: 1874
	[SerializeField]
	public string m_path;

	// Token: 0x04000753 RID: 1875
	[SerializeField]
	public Material m_movieMaterial;

	// Token: 0x04000754 RID: 1876
	[SerializeField]
	private bool m_playAutomatically = true;

	// Token: 0x04000755 RID: 1877
	[SerializeField]
	public bool m_loop;

	// Token: 0x04000756 RID: 1878
	private IntPtr m_nativeContext = IntPtr.Zero;

	// Token: 0x04000757 RID: 1879
	private int m_picWidth;

	// Token: 0x04000758 RID: 1880
	private int m_picHeight;

	// Token: 0x04000759 RID: 1881
	private int m_picX;

	// Token: 0x0400075A RID: 1882
	private int m_picY;

	// Token: 0x0400075B RID: 1883
	private int m_yStride;

	// Token: 0x0400075C RID: 1884
	private int m_yHeight;

	// Token: 0x0400075D RID: 1885
	private int m_uvStride;

	// Token: 0x0400075E RID: 1886
	private int m_uvHeight;

	// Token: 0x0400075F RID: 1887
	private ChannelTexture[] m_ChannelTextures;

	// Token: 0x04000760 RID: 1888
	private bool m_advance = true;

	// Token: 0x04000761 RID: 1889
	private double m_elapsedTime;

	// Token: 0x04000762 RID: 1890
	private double m_nextFrameTime;

	// Token: 0x04000763 RID: 1891
	private double m_lastFrameDelta = double.MaxValue;

	// Token: 0x04000764 RID: 1892
	private bool m_reallocTextures;

	// Token: 0x04000765 RID: 1893
	private bool m_hasFinished = true;

	// Token: 0x020001AF RID: 431
	// (Invoke) Token: 0x06000C68 RID: 3176
	public delegate void OnFinished(MobileMovieTexture sender);
}
