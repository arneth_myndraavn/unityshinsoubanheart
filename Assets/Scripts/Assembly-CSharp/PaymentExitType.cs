﻿using System;

// Token: 0x020000FB RID: 251
public enum PaymentExitType
{
	// Token: 0x0400061C RID: 1564
	INVALID,
	// Token: 0x0400061D RID: 1565
	CANCEL,
	// Token: 0x0400061E RID: 1566
	SUCCESS,
	// Token: 0x0400061F RID: 1567
	FAILD
}
