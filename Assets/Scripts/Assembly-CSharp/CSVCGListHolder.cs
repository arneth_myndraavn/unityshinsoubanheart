﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x02000164 RID: 356
public class CSVCGListHolder
{
	// Token: 0x06000A2E RID: 2606 RVA: 0x0002D5F0 File Offset: 0x0002B7F0
	public CSVCGListHolder()
	{
		this.charaData = CSVCGListHolder.SplitChara();
		this.allCharaCollect = 0;
	}

	// Token: 0x17000145 RID: 325
	// (get) Token: 0x06000A2F RID: 2607 RVA: 0x0002D60C File Offset: 0x0002B80C
	public int AllCharaCollect
	{
		get
		{
			return this.allCharaCollect;
		}
	}

	// Token: 0x06000A30 RID: 2608 RVA: 0x0002D614 File Offset: 0x0002B814
	public CSVCGListHolder.CharaData GetCharaIndex(int charaIndex)
	{
		return this.charaData[charaIndex];
	}

	// Token: 0x06000A31 RID: 2609 RVA: 0x0002D620 File Offset: 0x0002B820
	public void UpdateGallery()
	{
		int num = 0;
		int num2 = 0;
		foreach (CSVCGListHolder.CharaData charaData in this.charaData)
		{
			charaData.UpdateGallery();
			num += charaData.CollectCount;
			num2 += charaData.CollectTotal;
		}
		this.allCharaCollect = ((num2 == 0) ? 0 : (num * 100 / num2));
	}

	// Token: 0x06000A32 RID: 2610 RVA: 0x0002D688 File Offset: 0x0002B888
	private static List<CSVCGList> Read()
	{
		List<CSVCGList> list = new List<CSVCGList>();
		TextAsset textAsset = Resources.Load("CSV/CGLIST") as TextAsset;
		string[] array = textAsset.text.Split(new char[]
		{
			'\n'
		});
		for (int i = 1; i < array.Length; i++)
		{
			string text = array[i];
			string text2 = text.Replace("\"", string.Empty).Replace("\r", string.Empty);
			string[] array2 = text2.Split(new char[]
			{
				','
			});
			if (array2.Length >= 2)
			{
				list.Add(new CSVCGList(array2));
			}
		}
		Resources.UnloadAsset(textAsset);
		return list;
	}

	// Token: 0x06000A33 RID: 2611 RVA: 0x0002D734 File Offset: 0x0002B934
	private static CSVCGListHolder.CharaData[] SplitChara()
	{
		List<CSVCGList> list = CSVCGListHolder.Read();
		List<CSVCGListHolder.CharaData> list2 = new List<CSVCGListHolder.CharaData>();
		int charaIndex;
		for (charaIndex = 0; charaIndex < 11; charaIndex++)
		{
			list2.Add(new CSVCGListHolder.CharaData(list.FindAll((CSVCGList obj) => obj.charaIndex == charaIndex)));
		}
		return list2.ToArray();
	}

	// Token: 0x04000853 RID: 2131
	private CSVCGListHolder.CharaData[] charaData;

	// Token: 0x04000854 RID: 2132
	private int allCharaCollect;

	// Token: 0x02000165 RID: 357
	public class CGSameThumbnail
	{
		// Token: 0x06000A34 RID: 2612 RVA: 0x0002D7A0 File Offset: 0x0002B9A0
		public CGSameThumbnail(int index_, CSVCGList data)
		{
			this.index = index_;
			this.fpath = new string[data.collectCount];
			this.current = 0;
			int num = 0;
			foreach (CSVCGList.CG cg in data.cgs)
			{
				if (cg.read)
				{
					this.fpath[num++] = cg.fpath;
				}
			}
		}

		// Token: 0x06000A35 RID: 2613 RVA: 0x0002D810 File Offset: 0x0002BA10
		public void ResetCurrent()
		{
			this.current = 0;
		}

		// Token: 0x06000A36 RID: 2614 RVA: 0x0002D81C File Offset: 0x0002BA1C
		public string GetCurrentFPath()
		{
			return this.fpath[this.current];
		}

		// Token: 0x06000A37 RID: 2615 RVA: 0x0002D82C File Offset: 0x0002BA2C
		public string GetUpFPath()
		{
			return this.fpath[this.calcUp()];
		}

		// Token: 0x06000A38 RID: 2616 RVA: 0x0002D83C File Offset: 0x0002BA3C
		public string GetDownFPath()
		{
			return this.fpath[this.calcDown()];
		}

		// Token: 0x06000A39 RID: 2617 RVA: 0x0002D84C File Offset: 0x0002BA4C
		public void Up()
		{
			this.current = this.calcUp();
		}

		// Token: 0x06000A3A RID: 2618 RVA: 0x0002D85C File Offset: 0x0002BA5C
		public void Down()
		{
			this.current = this.calcDown();
		}

		// Token: 0x06000A3B RID: 2619 RVA: 0x0002D86C File Offset: 0x0002BA6C
		private int calcUp()
		{
			return (this.current != 0) ? (this.current - 1) : (this.fpath.Length - 1);
		}

		// Token: 0x06000A3C RID: 2620 RVA: 0x0002D89C File Offset: 0x0002BA9C
		private int calcDown()
		{
			return (this.current != this.fpath.Length - 1) ? (this.current + 1) : 0;
		}

		// Token: 0x04000855 RID: 2133
		public int index;

		// Token: 0x04000856 RID: 2134
		public string[] fpath;

		// Token: 0x04000857 RID: 2135
		public int current;
	}

	// Token: 0x02000166 RID: 358
	public class CGLink
	{
		// Token: 0x06000A3D RID: 2621 RVA: 0x0002D8C4 File Offset: 0x0002BAC4
		public CGLink(CSVCGList[] datas)
		{
			this.thumbnails = CSVCGListHolder.CGLink.makeThumbnails(datas);
			this.page = 0;
		}

		// Token: 0x06000A3E RID: 2622 RVA: 0x0002D8E0 File Offset: 0x0002BAE0
		private static CSVCGListHolder.CGSameThumbnail[] makeThumbnails(CSVCGList[] datas)
		{
			List<CSVCGListHolder.CGSameThumbnail> list = new List<CSVCGListHolder.CGSameThumbnail>();
			int num = 0;
			foreach (CSVCGList csvcglist in datas)
			{
				if (csvcglist.collectCount != 0)
				{
					list.Add(new CSVCGListHolder.CGSameThumbnail(num, csvcglist));
				}
				num++;
			}
			return list.ToArray();
		}

		// Token: 0x06000A3F RID: 2623 RVA: 0x0002D938 File Offset: 0x0002BB38
		public void SetPageByIndex(int index)
		{
			int num = 0;
			foreach (CSVCGListHolder.CGSameThumbnail cgsameThumbnail in this.thumbnails)
			{
				if (cgsameThumbnail.index == index)
				{
					this.page = num;
					return;
				}
				num++;
			}
		}

		// Token: 0x06000A40 RID: 2624 RVA: 0x0002D980 File Offset: 0x0002BB80
		public int GetNowIndex()
		{
			return this.thumbnails[this.page].index;
		}

		// Token: 0x06000A41 RID: 2625 RVA: 0x0002D994 File Offset: 0x0002BB94
		public string GetCurrentFPath()
		{
			return this.thumbnails[this.page].GetCurrentFPath();
		}

		// Token: 0x06000A42 RID: 2626 RVA: 0x0002D9A8 File Offset: 0x0002BBA8
		public string GetUpFPath()
		{
			return (!this.ud) ? this.GetCurrentFPath() : this.thumbnails[this.page].GetUpFPath();
		}

		// Token: 0x06000A43 RID: 2627 RVA: 0x0002D9E0 File Offset: 0x0002BBE0
		public string GetDownFPath()
		{
			return (!this.ud) ? this.GetCurrentFPath() : this.thumbnails[this.page].GetDownFPath();
		}

		// Token: 0x06000A44 RID: 2628 RVA: 0x0002DA18 File Offset: 0x0002BC18
		public string GetLeftFPath()
		{
			return (!this.lr) ? this.GetCurrentFPath() : this.thumbnails[this.calcLeft()].GetCurrentFPath();
		}

		// Token: 0x06000A45 RID: 2629 RVA: 0x0002DA50 File Offset: 0x0002BC50
		public string GetRightFPath()
		{
			return (!this.lr) ? this.GetCurrentFPath() : this.thumbnails[this.calcRight()].GetCurrentFPath();
		}

		// Token: 0x06000A46 RID: 2630 RVA: 0x0002DA88 File Offset: 0x0002BC88
		public void Up()
		{
			this.thumbnails[this.page].Up();
		}

		// Token: 0x06000A47 RID: 2631 RVA: 0x0002DA9C File Offset: 0x0002BC9C
		public void Down()
		{
			this.thumbnails[this.page].Down();
		}

		// Token: 0x06000A48 RID: 2632 RVA: 0x0002DAB0 File Offset: 0x0002BCB0
		public void Left()
		{
			int num = this.page;
			this.page = this.calcLeft();
			if (num != this.page)
			{
				this.thumbnails[num].ResetCurrent();
			}
		}

		// Token: 0x06000A49 RID: 2633 RVA: 0x0002DAEC File Offset: 0x0002BCEC
		public void Right()
		{
			int num = this.page;
			this.page = this.calcRight();
			if (num != this.page)
			{
				this.thumbnails[num].ResetCurrent();
			}
		}

		// Token: 0x06000A4A RID: 2634 RVA: 0x0002DB28 File Offset: 0x0002BD28
		private int calcLeft()
		{
			return (this.page != 0) ? (this.page - 1) : (this.thumbnails.Length - 1);
		}

		// Token: 0x06000A4B RID: 2635 RVA: 0x0002DB58 File Offset: 0x0002BD58
		private int calcRight()
		{
			return (this.page != this.thumbnails.Length - 1) ? (this.page + 1) : 0;
		}

		// Token: 0x17000146 RID: 326
		// (get) Token: 0x06000A4C RID: 2636 RVA: 0x0002DB80 File Offset: 0x0002BD80
		public bool lr
		{
			get
			{
				return this.thumbnails.Length > 1;
			}
		}

		// Token: 0x17000147 RID: 327
		// (get) Token: 0x06000A4D RID: 2637 RVA: 0x0002DB90 File Offset: 0x0002BD90
		public bool ud
		{
			get
			{
				return this.thumbnails[this.page].fpath.Length > 1;
			}
		}

		// Token: 0x04000858 RID: 2136
		public CSVCGListHolder.CGSameThumbnail[] thumbnails;

		// Token: 0x04000859 RID: 2137
		public int page;
	}

	// Token: 0x02000167 RID: 359
	public class CharaData
	{
		// Token: 0x06000A4E RID: 2638 RVA: 0x0002DBAC File Offset: 0x0002BDAC
		public CharaData(List<CSVCGList> dataList)
		{
			this.datas = dataList.ToArray();
			this.reads = new int[this.datas.Length, 2];
			this.collectCount = 0;
			this.collectTotal = 1;
			this.openCount = 0;
			this.cgLink = null;
		}

		// Token: 0x17000148 RID: 328
		// (get) Token: 0x06000A4F RID: 2639 RVA: 0x0002DBFC File Offset: 0x0002BDFC
		public int Collect
		{
			get
			{
				return this.collectCount * 100 / this.collectTotal;
			}
		}

		// Token: 0x17000149 RID: 329
		// (get) Token: 0x06000A50 RID: 2640 RVA: 0x0002DC10 File Offset: 0x0002BE10
		public int CollectCount
		{
			get
			{
				return this.collectCount;
			}
		}

		// Token: 0x1700014A RID: 330
		// (get) Token: 0x06000A51 RID: 2641 RVA: 0x0002DC18 File Offset: 0x0002BE18
		public int CollectTotal
		{
			get
			{
				return this.collectTotal;
			}
		}

		// Token: 0x1700014B RID: 331
		// (get) Token: 0x06000A52 RID: 2642 RVA: 0x0002DC20 File Offset: 0x0002BE20
		public int Length
		{
			get
			{
				return this.datas.Length;
			}
		}

		// Token: 0x1700014C RID: 332
		// (get) Token: 0x06000A53 RID: 2643 RVA: 0x0002DC2C File Offset: 0x0002BE2C
		public CSVCGListHolder.CGLink CgLink
		{
			get
			{
				return this.cgLink;
			}
		}

		// Token: 0x06000A54 RID: 2644 RVA: 0x0002DC34 File Offset: 0x0002BE34
		public bool Read(int index)
		{
			return index < this.reads.GetLength(0) && this.reads[index, 0] != 0;
		}

		// Token: 0x06000A55 RID: 2645 RVA: 0x0002DC6C File Offset: 0x0002BE6C
		public string Thumbnail(int index)
		{
			return (!this.Read(index)) ? "screen/cgmemory2/cgm_nothm" : ("viewer/cgmode/" + this.datas[index].thumbnail);
		}

		// Token: 0x06000A56 RID: 2646 RVA: 0x0002DC9C File Offset: 0x0002BE9C
		public string CollectString(int index)
		{
			return (!this.Read(index)) ? string.Empty : string.Format("{0}/{1}", this.reads[index, 0], this.reads[index, 1]);
		}

		// Token: 0x06000A57 RID: 2647 RVA: 0x0002DCF0 File Offset: 0x0002BEF0
		public void UpdateGallery()
		{
			int num = 0;
			this.collectCount = 0;
			this.collectTotal = 0;
			foreach (CSVCGList csvcglist in this.datas)
			{
				csvcglist.Update();
				this.collectCount += (this.reads[num, 0] = csvcglist.collectCount);
				this.collectTotal += (this.reads[num, 1] = csvcglist.collectTotal);
				if (csvcglist.collectCount != 0)
				{
					this.openCount++;
				}
				num++;
			}
			if (this.collectTotal == 0)
			{
				this.collectTotal = 1;
			}
			this.cgLink = new CSVCGListHolder.CGLink(this.datas);
		}

		// Token: 0x06000A58 RID: 2648 RVA: 0x0002DDBC File Offset: 0x0002BFBC
		public void OnSelect(int index)
		{
			UIValue.GalleryIndex = index;
		}

		// Token: 0x0400085A RID: 2138
		private CSVCGList[] datas;

		// Token: 0x0400085B RID: 2139
		private int[,] reads;

		// Token: 0x0400085C RID: 2140
		private int collectCount;

		// Token: 0x0400085D RID: 2141
		private int collectTotal;

		// Token: 0x0400085E RID: 2142
		private int openCount;

		// Token: 0x0400085F RID: 2143
		private CSVCGListHolder.CGLink cgLink;
	}
}
