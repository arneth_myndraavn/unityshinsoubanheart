﻿using System;
using System.Collections.Generic;
using Qoo.Game;
using UnityEngine;

// Token: 0x0200010D RID: 269
public class GalleryInputNameWindow : BaseWindow
{
	private Dictionary<string,int> dict = null;
	// Token: 0x06000711 RID: 1809 RVA: 0x0001E2CC File Offset: 0x0001C4CC
	protected sealed override void OnAwake()
	{
		UIValue.GalleryInputNameDialog_Exit = GalleryInputNameDialogExitType.INVALID;
		UIValue.GalleryInputNameDialog_Name = GameData.GetParamString("firstname", string.Empty);
        UIValue.GalleryInputNameDialog_LastName = GameData.GetParamString("familyname", string.Empty);
        this.edit = true;
		this.wait = 0f;
	}

	// Token: 0x06000712 RID: 1810 RVA: 0x0001E308 File Offset: 0x0001C508
	protected sealed override string[] newSceneTextureNameArray()
	{
		return new string[]
		{
			"screen/common/sys_name_bg"
		};
	}

	// Token: 0x06000713 RID: 1811 RVA: 0x0001E328 File Offset: 0x0001C528
	protected sealed override BaseWindow.UIComponent[] newComponentArray()
	{
		return new BaseWindow.UIComponent[]
		{
			new BaseWindow.UIImage("BackGround", 0, 0, this.wndz, "screen/common/sys_name_bg", true, true)
		};
	}

	// Token: 0x06000714 RID: 1812 RVA: 0x0001E35C File Offset: 0x0001C55C
	protected sealed override void OnBaseWindowOnButton(string obj)
	{
		if (obj != null)
		{
			if (this.dict == null)
			{
				this.dict = new Dictionary<string, int>(1)
				{
					{
						"BackGround",
						0
					}
				};
			}
			int num;
			if (this.dict.TryGetValue(obj, out num))
			{
				if (num != 0)
				{
				}
			}
		}
	}

	// Token: 0x06000715 RID: 1813 RVA: 0x0001E3B8 File Offset: 0x0001C5B8
	protected override void OnBaseWindowUpdate()
	{
		if (this.edit)
		{
			this.wait += Time.deltaTime;
			if (this.wait > 1f)
			{
				base.AddScene(UIValue.SCENE_GALLERYINPUTNAMEKEYBOARD);
				this.edit = false;
				this.wait = 0f;
			}
		}
		else
		{
			switch (UIValue.GalleryInputNameDialog_Exit)
			{
			case GalleryInputNameDialogExitType.INPUT:
			{
				UIValue.GalleryInputNameDialog_Exit = GalleryInputNameDialogExitType.INVALID;
                string galleryInputNameDialog_Name = UIValue.GalleryInputNameDialog_Name;
                string galleryInputNameDialog_LastName = UIValue.GalleryInputNameDialog_LastName;
				if (galleryInputNameDialog_Name.Length == 0 || this.checkSpaceOnly(galleryInputNameDialog_Name) || galleryInputNameDialog_LastName.Length == 0 || this.checkSpaceOnly(galleryInputNameDialog_LastName))
				{
					UIValue.GalleryInputNameDialog_Type = GalleryInputNameDialogType.EMPTY;
				}
				else if (galleryInputNameDialog_Name.Length > 7 || galleryInputNameDialog_LastName.Length > 7)
				{
					UIValue.GalleryInputNameDialog_Type = GalleryInputNameDialogType.OVER;
				}
				else if (!this.checkValidString(galleryInputNameDialog_Name) || !this.checkValidString(galleryInputNameDialog_LastName))
				{
					UIValue.GalleryInputNameDialog_Type = GalleryInputNameDialogType.INVALIDCHAR;
				}
				else
				{
					UIValue.GalleryInputNameDialog_Type = GalleryInputNameDialogType.CONFIRM;
				}
				base.AddScene(UIValue.SCENE_GALLERYINPUTNAMEDIALOG);
				break;
			}
			case GalleryInputNameDialogExitType.YES:
				UIValue.GalleryInputNameDialog_Exit = GalleryInputNameDialogExitType.INVALID;
				GameData.SetParamString("firstname", string.Empty, UIValue.GalleryInputNameDialog_Name);
				GameData.SetParamString("familyname", string.Empty, UIValue.GalleryInputNameDialog_LastName);
				SceneManager.BackScene();
				break;
			case GalleryInputNameDialogExitType.NO:
				UIValue.GalleryInputNameDialog_Exit = GalleryInputNameDialogExitType.INVALID;
				this.edit = true;
				this.wait = 0f;
				break;
			case GalleryInputNameDialogExitType.RETRY:
				UIValue.GalleryInputNameDialog_Exit = GalleryInputNameDialogExitType.INVALID;
				this.edit = true;
				this.wait = 0f;
				break;
			}
		}
	}

	// Token: 0x06000716 RID: 1814 RVA: 0x0001E508 File Offset: 0x0001C708
	private bool checkValidString(string firstname)
	{
		return Singleton<UnityGraph>.Instance.Font.Font.CheckGlyph(firstname);
	}

	// Token: 0x06000717 RID: 1815 RVA: 0x0001E520 File Offset: 0x0001C720
	private bool checkSpaceOnly(string firstname)
	{
		foreach (char c in firstname)
		{
			if (c != ' ' && c != '\u3000')
			{
				return false;
			}
		}
		return true;
	}

	// Token: 0x04000691 RID: 1681
	private readonly int wndz;

	// Token: 0x04000692 RID: 1682
	private bool edit;

	// Token: 0x04000693 RID: 1683
	private float wait;
}
