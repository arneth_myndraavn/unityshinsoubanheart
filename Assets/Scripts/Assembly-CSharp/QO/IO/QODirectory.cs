﻿using System;
using System.Collections.Generic;
using System.IO;

namespace QO.IO
{
	// Token: 0x02000173 RID: 371
	public class QODirectory
	{
		// Token: 0x06000A90 RID: 2704 RVA: 0x0002E8C0 File Offset: 0x0002CAC0
		public static List<string> CollectFile(string directory, string extension, bool isWithChild = false)
		{
			List<string> list = new List<string>();
			if (!Directory.Exists(directory))
			{
				return list;
			}
			foreach (string text in Directory.GetFiles(directory))
			{
				if (text.IndexOf(".meta") < 0)
				{
					if (text.IndexOf(extension) >= 0)
					{
						list.Add(text);
					}
				}
			}
			if (isWithChild)
			{
				string[] directories = Directory.GetDirectories(directory);
				foreach (string directory2 in directories)
				{
					foreach (string item in QODirectory.CollectFile(directory2, extension, isWithChild))
					{
						list.Add(item);
					}
				}
			}
			return list;
		}

		// Token: 0x06000A91 RID: 2705 RVA: 0x0002E9BC File Offset: 0x0002CBBC
		public static void CreatePathToFileDirectory(string filePath)
		{
			string text = string.Empty;
			string[] array = filePath.Split(new char[]
			{
				'/'
			});
			for (int i = 0; i < array.Length - 1; i++)
			{
				text = text + "/" + array[i];
			}
			if (text != string.Empty)
			{
				text = text.Substring(1);
				if (!Directory.Exists(text))
				{
					Directory.CreateDirectory(text);
				}
			}
		}
	}
}
