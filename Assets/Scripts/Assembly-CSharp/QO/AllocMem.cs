﻿using System;
using System.Text;
using UnityEngine;

namespace QO
{
	// Token: 0x0200015F RID: 351
	[ExecuteInEditMode]
	public class AllocMem : MonoBehaviour
	{
		// Token: 0x06000A22 RID: 2594 RVA: 0x0002D02C File Offset: 0x0002B22C
		public void Start()
		{
			base.useGUILayout = false;
		}

		// Token: 0x06000A23 RID: 2595 RVA: 0x0002D038 File Offset: 0x0002B238
		public void OnGUI()
		{
			if (!this.show || (!Application.isPlaying && !this.showInEditor))
			{
				return;
			}
			int num = GC.CollectionCount(0);
			if (this.lastCollectNum != (float)num)
			{
				this.lastCollectNum = (float)num;
				this.delta = Time.realtimeSinceStartup - this.lastCollect;
				this.lastCollect = Time.realtimeSinceStartup;
				this.lastDeltaTime = Time.deltaTime;
				this.collectAlloc = this.allocMem;
			}
			this.allocMem = (int)GC.GetTotalMemory(false);
			this.peakAlloc = ((this.allocMem <= this.peakAlloc) ? this.peakAlloc : this.allocMem);
			if (Time.realtimeSinceStartup - this.lastAllocSet > 0.3f)
			{
				int num2 = this.allocMem - this.lastAllocMemory;
				this.lastAllocMemory = this.allocMem;
				this.lastAllocSet = Time.realtimeSinceStartup;
				if (num2 >= 0)
				{
					this.allocRate = num2;
				}
			}
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("usedHeapSize  ");
			stringBuilder.Append(string.Concat(new object[]
			{
				UnityEngine.Profiling.Profiler.usedHeapSize / 1024f,
				" KB / ",
				(float)SystemInfo.systemMemorySize * 1024f,
				" KB"
			}));
			stringBuilder.Append("\r\n");
			stringBuilder.Append("Currently allocated  ");
			stringBuilder.Append(((float)this.allocMem / 1024f).ToString("0"));
			stringBuilder.Append("KB\r\n");
			stringBuilder.Append("Peak allocated  ");
			stringBuilder.Append(((float)this.peakAlloc / 1024f).ToString("0"));
			stringBuilder.Append("KB (last collect ");
			stringBuilder.Append(((float)this.collectAlloc / 1024f).ToString("0"));
			stringBuilder.Append(" KB)\r\n");
			stringBuilder.Append("Allocation rate  ");
			stringBuilder.Append(((float)this.allocRate / 1024f).ToString("0.0"));
			stringBuilder.Append("KB\r\n");
			stringBuilder.Append("Collection frequency  ");
			stringBuilder.Append(this.delta.ToString("0.00"));
			stringBuilder.Append("s\r\n");
			stringBuilder.Append("Last collect delta  ");
			stringBuilder.Append(this.lastDeltaTime.ToString("0.000"));
			stringBuilder.Append("s (");
			stringBuilder.Append((1f / this.lastDeltaTime).ToString("0.0"));
			stringBuilder.Append(" fps)");
			if (this.showFPS)
			{
				stringBuilder.Append("\r\n" + (1f / Time.deltaTime).ToString("0.0") + " fps");
			}
			GUI.Box(new Rect(5f, 5f, 310f, (float)(80 + ((!this.showFPS) ? 0 : 16))), string.Empty);
			GUI.Label(new Rect(10f, 5f, 2000f, 400f), stringBuilder.ToString());
		}

		// Token: 0x0400083E RID: 2110
		public bool show = true;

		// Token: 0x0400083F RID: 2111
		public bool showFPS;

		// Token: 0x04000840 RID: 2112
		public bool showInEditor;

		// Token: 0x04000841 RID: 2113
		private float lastCollect;

		// Token: 0x04000842 RID: 2114
		private float lastCollectNum;

		// Token: 0x04000843 RID: 2115
		private float delta;

		// Token: 0x04000844 RID: 2116
		private float lastDeltaTime;

		// Token: 0x04000845 RID: 2117
		private int allocRate;

		// Token: 0x04000846 RID: 2118
		private int lastAllocMemory;

		// Token: 0x04000847 RID: 2119
		private float lastAllocSet = -9999f;

		// Token: 0x04000848 RID: 2120
		private int allocMem;

		// Token: 0x04000849 RID: 2121
		private int collectAlloc;

		// Token: 0x0400084A RID: 2122
		private int peakAlloc;
	}
}
