﻿using System;
using Qoo.Game;
using UnityEngine;

// Token: 0x020000A6 RID: 166
public class ParamWnd : Singleton<ParamWnd>
{
	// Token: 0x060004DD RID: 1245 RVA: 0x000120B8 File Offset: 0x000102B8
	private void Awake()
	{
		this.m_Tp[0] = Man2D.TextSprite(null);
		this.m_Tp[0].SetName("Param");
		this.m_Tp[0].SetPos(0, 0, 8000);
		this.m_Tp[1] = Man2D.TextSprite(null);
		this.m_Tp[1].SetName("Param");
		this.m_Tp[1].SetPos(480, 0, 8000);
		this.m_log[0] = string.Empty;
		this.m_log[1] = string.Empty;
		base.enabled = false;
	}

	// Token: 0x060004DE RID: 1246 RVA: 0x00012154 File Offset: 0x00010354
	private void OnDestroy()
	{
		if (Singleton<Man2D>.IsReady)
		{
			Singleton<Man2D>.Instance.RemoveSprite(this.m_Tp[0]);
			this.m_Tp[0] = null;
			Singleton<Man2D>.Instance.RemoveSprite(this.m_Tp[1]);
			this.m_Tp[1] = null;
		}
	}

	// Token: 0x060004DF RID: 1247 RVA: 0x000121A4 File Offset: 0x000103A4
	private void OnEnable()
	{
		this.m_Tp[0].Show = true;
		this.m_Tp[1].Show = true;
	}

	// Token: 0x060004E0 RID: 1248 RVA: 0x000121C4 File Offset: 0x000103C4
	private void OnDisable()
	{
		this.m_Tp[0].Show = false;
		this.m_Tp[1].Show = false;
	}

	// Token: 0x060004E1 RID: 1249 RVA: 0x000121E4 File Offset: 0x000103E4
	private void Update()
	{
		this.UpdateParam();
	}

	// Token: 0x060004E2 RID: 1250 RVA: 0x000121EC File Offset: 0x000103EC
	private void UpdateParam()
	{
		this.Clear();
		this.AddLine(string.Concat(new object[]
		{
			"fps:",
			(1f / Time.deltaTime).ToString("0.0"),
			" heap:",
			UnityEngine.Profiling.Profiler.usedHeapSize / 1048576U,
			"MB all=",
			SystemInfo.systemMemorySize,
			"MB"
		}));
		for (int num = 0; num != GameData.Param.Num; num++)
		{
			this.AddLine("value=" + GameData.Param.GetName(num) + " param=" + GameData.Param.GetParamString(num));
		}
		this.m_Tp[0].AddText(this.m_log[0], 18);
		this.m_Tp[1].AddText(this.m_log[1], 18);
	}

	// Token: 0x060004E3 RID: 1251 RVA: 0x000122E4 File Offset: 0x000104E4
	private void Clear()
	{
		this.m_Tp[0].ClearText();
		this.m_Tp[1].ClearText();
		this.count = 0;
		this.m_log[0] = string.Empty;
		this.m_log[1] = string.Empty;
	}

	// Token: 0x060004E4 RID: 1252 RVA: 0x00012324 File Offset: 0x00010524
	private void AddLine(string str)
	{
		if (this.count < 20)
		{
			string[] log = this.m_log;
			int num = 0;
			log[num] = log[num] + str + "\n";
		}
		else
		{
			string[] log2 = this.m_log;
			int num2 = 1;
			log2[num2] = log2[num2] + str + "\n";
		}
		this.count++;
	}

	// Token: 0x0400037E RID: 894
	private UnityTextSprite[] m_Tp = new UnityTextSprite[2];

	// Token: 0x0400037F RID: 895
	private string[] m_log = new string[2];

	// Token: 0x04000380 RID: 896
	private int count;
}
