﻿using System;
using UnityEngine;

// Token: 0x0200016F RID: 367
public class ImageBytesInfo : MonoBehaviour
{
	// Token: 0x17000156 RID: 342
	// (get) Token: 0x06000A84 RID: 2692 RVA: 0x0002E6EC File Offset: 0x0002C8EC
	public string Path
	{
		get
		{
			string text = string.Empty;
			switch (this.MainCategory)
			{
			case ImageBytesInfo.ImageCategory.EVENT:
				text = "event";
				break;
			case ImageBytesInfo.ImageCategory.EFFECT:
				text = "effect";
				break;
			case ImageBytesInfo.ImageCategory.VIEWER:
				text = "viewer";
				break;
			case ImageBytesInfo.ImageCategory.SCREEN:
				text = "screen";
				break;
			}
			return string.Concat(new string[]
			{
				text,
				"/",
				this.SubCategory,
				"/",
				this.FileName
			});
		}
	}

	// Token: 0x0400087A RID: 2170
	public ImageBytesInfo.ImageCategory MainCategory;

	// Token: 0x0400087B RID: 2171
	public string SubCategory;

	// Token: 0x0400087C RID: 2172
	public string FileName;

	// Token: 0x02000170 RID: 368
	public enum ImageCategory
	{
		// Token: 0x0400087E RID: 2174
		EVENT,
		// Token: 0x0400087F RID: 2175
		EFFECT,
		// Token: 0x04000880 RID: 2176
		VIEWER,
		// Token: 0x04000881 RID: 2177
		SCREEN
	}
}
