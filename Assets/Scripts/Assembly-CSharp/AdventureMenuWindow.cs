﻿using System;
using Qoo;
using Qoo.Game;
using Qoo.Input;
using UnityEngine;

// Token: 0x02000100 RID: 256
public class AdventureMenuWindow : SubTaskBaseWindow
{
	// Token: 0x060006A8 RID: 1704 RVA: 0x0001B954 File Offset: 0x00019B54
	public static void SetSubTaskParam(Texture bgTex, bool isNormal)
	{
		//TODO troubleshoot sprite stretching
		//AdventureMenuWindow.BgTex = bgTex;
		AdventureMenuWindow.IsNormal = isNormal;
	}

	// Token: 0x060006A9 RID: 1705 RVA: 0x0001B964 File Offset: 0x00019B64
	private void OnClose()
	{
		SaveLoadManager.SaveSystem();
		if (DispAssert.IsAssert())
		{
			return;
		}
		base.IsTaskEnd = true;
		this.OnPastWindow();
	}

	// Token: 0x060006AA RID: 1706 RVA: 0x0001B984 File Offset: 0x00019B84
	protected override void OnBaseWindowDestroy()
	{
		base.IsTaskEnd = true;
	}

	// Token: 0x060006AB RID: 1707 RVA: 0x0001B990 File Offset: 0x00019B90
	private void OnAuto()
	{
		KsInput.State = INPUT_STATE.AUTO;
	}

	// Token: 0x060006AC RID: 1708 RVA: 0x0001B998 File Offset: 0x00019B98
	private void OnTitle()
	{
		GameData.IsMoveTitle = true;
	}

	// Token: 0x060006AD RID: 1709 RVA: 0x0001B9A0 File Offset: 0x00019BA0
	private void MakeTextureObject(string name_, int x_, int y_, int z_, Texture tex, float scale_w = 1f, float scale_h = 1f)
	{
		int num = -480;
		int num2 = 272;
		GameObject gameObject = GameObject.CreatePrimitive(PrimitiveType.Quad);
		//gameObject.renderer.material = new Material(Resources.Load("Shader/Sprite/Sprite") as Shader);
		gameObject.GetComponent<Renderer>().material = new Material(Shader.Find("QO/Sprite") as Shader);
		gameObject.GetComponent<Renderer>().material.mainTexture = tex;
		gameObject.transform.parent = base.transform;
		float num3 = (float)tex.width * scale_w;
		float num4 = (float)tex.height * scale_h;
		gameObject.transform.localPosition = new Vector3((float)(num + (int)(num3 / 2f) + x_), (float)(num2 - (int)(num4 / 2f) - y_), (float)(-(float)z_));
		gameObject.transform.localScale = new Vector3(num3, num4, 1f);
		gameObject.name = name_;
		base.AddGameObject(gameObject.name, gameObject);
	}

	// Token: 0x060006AE RID: 1710 RVA: 0x0001BA78 File Offset: 0x00019C78
	protected void CreateScreenBackEffect()
	{
		Color black = Color.black;
		black.a = 0.5f;
		ScreenEffect.Init(0, black, base.transform);
	}

	// Token: 0x060006AF RID: 1711 RVA: 0x0001BAA4 File Offset: 0x00019CA4
	protected sealed override string[] newSceneTextureNameArray()
	{
		return new string[]
		{
			"screen/sysmenu/sys_pop",
			"screen/sysmenu/sys_pop2",
			"screen/sysmenu/sys_btn_0",
			"screen/sysmenu/sys_btn_1",
			"screen/sysmenu/sys_btn_2",
			"screen/sysmenu/sys_btn_3",
			"screen/sysmenu/sys_btn_4",
			"screen/sysmenu/sys_btn_5",
			"screen/sysmenu/sys_btn_6",
			"screen/sysmenu/sys_btn_7",
			"screen/sysmenu/sys_btn_8",
			"screen/sysmenu/sys_btn_9",
			"screen/sysmenu/sys_btn_12"
		};
	}

	// Token: 0x060006B0 RID: 1712 RVA: 0x0001BB28 File Offset: 0x00019D28
	protected sealed override void BeforeInit()
	{
		Vector3 localPosition = base.transform.parent.localPosition;
		localPosition.z = -30f;
		base.transform.parent.localPosition = localPosition;
		//TODO troubleshoot sprite stretching
		//Causes sprite stretching
		//this.MakeTextureObject("BackGround", 0, 0, -1, AdventureMenuWindow.BgTex, 960f / (float)AdventureMenuWindow.BgTex.width, 544f / (float)AdventureMenuWindow.BgTex.height);
	}

	// Token: 0x060006B1 RID: 1713 RVA: 0x0001BB98 File Offset: 0x00019D98
	protected override void AfterInit()
	{
		this.ConfirmShow(false);
	}

	// Token: 0x060006B2 RID: 1714 RVA: 0x0001BBA4 File Offset: 0x00019DA4
	protected override void OnGraphicLoadComplete()
	{
		this.CreateScreenBackEffect();
	}

	// Token: 0x060006B3 RID: 1715 RVA: 0x0001BBAC File Offset: 0x00019DAC
	protected sealed override BaseWindow.UIComponent[] newComponentArray()
	{
		if (AdventureMenuWindow.IsNormal)
		{
			return new BaseWindow.UIComponent[]
			{
				new BaseWindow.UICollision("Close", 0, 0, this.wndz, 960, 544),
				new BaseWindow.UIImage("Popup", 5, 373, this.wndz + 1, "screen/sysmenu/sys_pop", true, true),
				new BaseWindow.UIButton("Btn0", 55, 401, this.wndz + 2, "screen/sysmenu/sys_btn_0", true, true, 1, 2, 0),
				new BaseWindow.UIButton("Btn1", 55, 471, this.wndz + 2, "screen/sysmenu/sys_btn_1", true, true, 1, 2, 0),
				new BaseWindow.UIButton("Btn2", 227, 401, this.wndz + 2, "screen/sysmenu/sys_btn_2", true, true, 1, 2, 0),
				new BaseWindow.UIButton("Btn3", 227, 471, this.wndz + 2, "screen/sysmenu/sys_btn_3", true, true, 1, 2, 0),
				new BaseWindow.UIButton("Btn4", 399, 401, this.wndz + 2, "screen/sysmenu/sys_btn_4", true, true, 1, 2, 0),
				new BaseWindow.UIButton("Btn5", 399, 471, this.wndz + 2, "screen/sysmenu/sys_btn_5", true, true, 1, 2, 0),
				new BaseWindow.UIButton("Btn6", 571, 401, this.wndz + 2, "screen/sysmenu/sys_btn_6", true, true, 1, 2, 0),
				new BaseWindow.UIButton("Btn7", 571, 471, this.wndz + 2, "screen/sysmenu/sys_btn_7", true, true, 1, 2, 0),
				new BaseWindow.UIButton("Btn8", 743, 401, this.wndz + 2, "screen/sysmenu/sys_btn_8", true, true, 1, 2, 0),
				new BaseWindow.UIButton("Btn9", 743, 471, this.wndz + 2, "screen/sysmenu/sys_btn_9", true, true, 1, 2, 0),
				new BaseWindow.UICollision("CofirmCollision", 0, 0, this.wndz + 3, 960, 544),
				new BaseWindow.UIImage("Confirm", 136, 182, this.wndz + 3, "screen/common/sys_dialog", true, false),
				new BaseWindow.UIButton("Confirm_YES", 312, 277, this.wndz + 4, "screen/common/dlog_btn_y", true, false, 1, 2, 0),
				new BaseWindow.UIButton("Confirm_NO", 513, 277, this.wndz + 4, "screen/common/dlog_btn_n", true, false, 1, 2, 0),
				new BaseWindow.UIText("Confirm_MSG", 480, 230, this.wndz + 4, UnityApp.Instance.isJapanese ? "タイトル画面へ戻りますか？" : "Return to Title Screen？", 28, UnityTextSprite.PositionType.Center, UnityTextSprite.PositionType.TopLeft, byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue)
			};
		}
		return new BaseWindow.UIComponent[]
		{
			new BaseWindow.UICollision("Close", 0, 0, this.wndz, 960, 544),
			new BaseWindow.UIImage("Popup", 5, 373, this.wndz + 1, "screen/sysmenu/sys_pop2", true, true),
			new BaseWindow.UIButton("Btn2", 227, 401, this.wndz + 2, "screen/sysmenu/sys_btn_2", true, true, 1, 2, 0),
			new BaseWindow.UIButton("Btn3", 227, 471, this.wndz + 2, "screen/sysmenu/sys_btn_3", true, true, 1, 2, 0),
			new BaseWindow.UIButton("Btn4", 399, 401, this.wndz + 2, "screen/sysmenu/sys_btn_4", true, true, 1, 2, 0),
			new BaseWindow.UIButton("Btn5", 399, 471, this.wndz + 2, "screen/sysmenu/sys_btn_5", true, true, 1, 2, 0),
			new BaseWindow.UIButton("Btn6", 571, 401, this.wndz + 2, "screen/sysmenu/sys_btn_6", true, true, 1, 2, 0),
			new BaseWindow.UIButton("Btn7", 571, 471, this.wndz + 2, "screen/sysmenu/sys_btn_7", true, true, 1, 2, 0),
			new BaseWindow.UIButton("Btn8", 743, 401, this.wndz + 2, "screen/sysmenu/sys_btn_8", true, true, 1, 2, 0),
			new BaseWindow.UIButton("Btn12", 743, 471, this.wndz + 2, "screen/sysmenu/sys_btn_12", true, true, 1, 2, 0),
			new BaseWindow.UICollision("CofirmCollision", 0, 0, this.wndz + 3, 960, 544),
			new BaseWindow.UIImage("Confirm", 136, 182, this.wndz + 3, "screen/common/sys_dialog", true, false),
			new BaseWindow.UIButton("Confirm_YES", 312, 277, this.wndz + 4, "screen/common/dlog_btn_y", true, false, 1, 2, 0),
			new BaseWindow.UIButton("Confirm_NO", 513, 277, this.wndz + 4, "screen/common/dlog_btn_n", true, false, 1, 2, 0),
			new BaseWindow.UIText("Confirm_MSG", 480, UnityApp.Instance.isJapanese ? 230 : 210, this.wndz + 4, UnityApp.Instance.isJapanese ? "回想を停止しますか？" : "Do you want to stop\nreplaying this event？", 28, UnityTextSprite.PositionType.Center, UnityTextSprite.PositionType.TopLeft, byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue)
		};
	}

	// Token: 0x060006B4 RID: 1716 RVA: 0x0001C0EC File Offset: 0x0001A2EC
	protected sealed override void OnBaseWindowOnButton(string obj)
	{
		if (base.IsTaskEnd)
		{
			return;
		}
		switch (obj)
		{
		case "CofirmCollision":
		case "Popup":
		case "Confirm":
		case "Btn8":
			goto IL_C2;
		case "Confirm_NO":
			base.PlaySE_Cancel();
			goto IL_C2;
		case "Close":
			base.PlaySE_Cancel();
			goto IL_C2;
		}
		base.PlaySE_Ok();
		IL_C2:
		switch (obj)
		{
		case "Close":
			this.OnClose();
			break;
		case "Btn0":
			UIValue.SaveLoadType = SaveLoadType.SAVE;
			UIValue.SaveLoadCallType = SaveLoadCallType.ADVMENU;
			base.AddScene(UIValue.SCENE_SAVELOAD);
			break;
		case "Btn1":
			UIValue.SaveLoadType = SaveLoadType.LOAD;
			UIValue.SaveLoadCallType = SaveLoadCallType.ADVMENU;
			base.AddScene(UIValue.SCENE_SAVELOAD);
			break;
		case "Btn2":
			base.AddScene(UIValue.SCENE_OPTIONTEXT);
			break;
		case "Btn3":
			base.AddScene(UIValue.SCENE_OPTIONSCREEN);
			break;
		case "Btn4":
			base.AddScene(UIValue.SCENE_OPTIONSOUND);
			break;
		case "Btn5":
			base.AddScene(UIValue.SCENE_OPTIONVOICE);
			break;
		case "Btn6":
			base.AddScene(UIValue.SCENE_OPTIONSKIP);
			break;
		case "Btn7":
			base.AddScene(UIValue.SCENE_OPTIONBACK);
			break;
		case "Btn8":
			this.OnAuto();
			this.OnClose();
			break;
		case "Btn9":
			this.ConfirmShow(true);
			break;
		case "Btn12":
			this.ConfirmShow(true);
			break;
		case "Confirm_YES":
			this.OnTitle();
			this.ConfirmShow(false);
			this.OnClose();
			break;
		case "Confirm_NO":
			this.ConfirmShow(false);
			break;
		}
	}

	// Token: 0x060006B5 RID: 1717 RVA: 0x0001C3D0 File Offset: 0x0001A5D0
	public override void OnCurrentWindow()
	{
		base.OnCurrentWindow();
		this.CreateScreenBackEffect();
	}

	// Token: 0x060006B6 RID: 1718 RVA: 0x0001C3E0 File Offset: 0x0001A5E0
	private void ConfirmShow(bool show = true)
	{
		base.GameObjectShow("Confirm", show);
		base.GameObjectShow("Confirm_YES", show);
		base.GameObjectShow("Confirm_NO", show);
		base.GameObjectShow("CofirmCollision", show);
		if (show)
		{
			if (UnityApp.Instance.isJapanese)
			{
				this.SetText("Confirm_MSG", (!AdventureMenuWindow.IsNormal) ? "回想を停止しますか？" : "タイトル画面へ戻りますか？");
			}
			else
			{
				this.SetText("Confirm_MSG", (!AdventureMenuWindow.IsNormal) ? "Do you want to stop\nreplaying this event？" : "Return to Title Screen？");
			}
			
		}
		else
		{
			this.SetText("Confirm_MSG", string.Empty);
		}
	}

	// Token: 0x060006B7 RID: 1719 RVA: 0x0001C45C File Offset: 0x0001A65C
	private void SetText(string name, string text)
	{
		UnityTextSprite textSprite = base.GetTextSprite(name);
		textSprite.ClearText();
		textSprite.AddText(text, 28);
		textSprite.Update(960, 544, -10f);
	}

	// Token: 0x0400065B RID: 1627
	private static Texture BgTex;

	// Token: 0x0400065C RID: 1628
	private static bool IsNormal;

	// Token: 0x0400065D RID: 1629
	private readonly int wndz;
}
