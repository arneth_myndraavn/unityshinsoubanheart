﻿using System;

// Token: 0x02000175 RID: 373
public enum SLIDE_VECTOR
{
	// Token: 0x04000889 RID: 2185
	NONE,
	// Token: 0x0400088A RID: 2186
	LEFT,
	// Token: 0x0400088B RID: 2187
	RIGHT,
	// Token: 0x0400088C RID: 2188
	UP,
	// Token: 0x0400088D RID: 2189
	DOWN
}
