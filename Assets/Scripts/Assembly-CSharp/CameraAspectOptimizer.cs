﻿using System;
using UnityEngine;

// Token: 0x02000191 RID: 401
public class CameraAspectOptimizer : MonoBehaviour
{
	// Token: 0x06000B85 RID: 2949 RVA: 0x00030F58 File Offset: 0x0002F158
	private void Awake()
	{
	}

	// Token: 0x06000B86 RID: 2950 RVA: 0x00030F5C File Offset: 0x0002F15C
	private void Start()
	{
		this.Target = base.GetComponent<Camera>();
		Rect rect = this.calcAspect(this.widthAspect, this.heightAspect);
		this.Target.rect = rect;
	}

	// Token: 0x06000B87 RID: 2951 RVA: 0x00030F94 File Offset: 0x0002F194
	private Rect calcAspect(float width, float height)
	{
		float num = width / height;
		float num2 = (float)Screen.width / (float)Screen.height;
		float num3 = num2 / num;
		Rect result = new Rect(0f, 0f, 1f, 1f);
		if (1f > num3)
		{
			result.x = 0f;
			result.y = (1f - num3) / 2f;
			result.width = 1f;
			result.height = num3;
		}
		else
		{
			float num4 = 1f / num3;
			result.x = (1f - num4) / 2f;
			result.y = 0f;
			result.width = num4;
			result.height = 1f;
		}
		return result;
	}

	// Token: 0x06000B88 RID: 2952 RVA: 0x00031058 File Offset: 0x0002F258
	private void Update()
	{
	}

	// Token: 0x0400090A RID: 2314
	public float widthAspect = 16f;

	// Token: 0x0400090B RID: 2315
	public float heightAspect = 9f;

	// Token: 0x0400090C RID: 2316
	private Camera Target;
}
