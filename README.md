# README #
Unity 2021.2.10.f1 project for the Shinsouban Heart app

##3/31/23 Project assets are being revamped and Unity version updated, currently referencecd versions will not work.##

### Setup ###

* Install Unity 2021.2.10.f1: [Unity Download Archive](https://unity3d.com/get-unity/download/archive) \(**Important!** project has been updated to 2021.2.10.f1. older versions of unity should not be used\)
* Clone repo to your desired folder
* Download game assets here: [Folder contians Japanese and English assets, only download the one you want. The English assets aren't complete](https://drive.google.com/drive/folders/1niz8gwMY_-tXHnT97IH5lZOWV1n9w0Qn?usp=sharing) 
* Extract game assets to the assets folder from the cloned repo.
* Open cloned repo folder as a unity project. It may take a few hours to load all of the asset files!
* Open the Init scene and press play. The project is working correctly if the title scene loads additively into the Init scene.

### Trello Board ###

* [Trello](https://trello.com/b/BDeHwsNe/shinsouban-alice-unity-project)
* keep track of the project's progress here

### Contribution guidelines ###

* Merging to main can only be done through PR. All PRs will be reviewed by the project lead (Arneth Myndraavn).

### How do I join/contribute? ###

* Project Discord: [Invite link](https://href.li/?https://discord.gg/HD7rGgZz4c)
* Contact ArnethMyndraavn@gmail.com or ArnethMyndraavn#3438 on discord.
* Currently recruiting Script Editors, Translators, Image Editors, Playtesters, and Devs